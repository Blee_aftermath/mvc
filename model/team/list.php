<?php
class ModelTeamList extends Model {
  
  function getTeam($teamID) {
    $q = "SELECT * FROM team";
        $q.= " WHERE team_id = '$teamID'";
        return $this->db->query($q)->row;
  }


  function getTeamsByRegion() {

    $result = [];

    $q  = "SELECT * FROM region";
    $q .= " LEFT JOIN emp ON emp_id = region_emp_rm";
    $q .= " WHERE region_is_field = 1 AND region_is_act = 1 ORDER BY region_order";
    //echo '<pre>'; print_r($q); echo '</pre>'; exit;

    $count = 0; // Hold the # of teams across the page -- when 4 do a break and new row

    if ($rows = $this->db->query($q)->rows) {
      if (!empty($rows)) {

        foreach ($rows as $k => $myRegion) {

          $count++;
          $result[$k]['valRegion']     = $valRegion = $myRegion['region_id'];
          $result[$k]['webRegionCode'] = $this->util->prepareWeb($myRegion['region_code']);
          $result[$k]['webRegionName'] = $this->util->prepareWeb($myRegion['region_name']);
          $result[$k]['webColor1']     = $myRegion['region_color1'];
          $result[$k]['webColor2']     = $myRegion['region_color2'];
          $result[$k]['webRMName']     = $myRegion['emp_fname'] . " " . $myRegion['emp_lname'];
          $result[$k]['webRMTelc']     = $this->util->formatPhone($myRegion['emp_telc']);

          $sqlTeam  = "SELECT *";
          $sqlTeam .= ", (SELECT COUNT(*) FROM res_partner_office WHERE rpo_office = team_office) AS count_rp";
          $sqlTeam .= " FROM team";
          $sqlTeam .= " JOIN team_status ON team_status = team_status_id";
          $sqlTeam .= " JOIN office ON team_office = office_id";
          $sqlTeam .= " JOIN region ON office_region = region_id";
          $sqlTeam .= " JOIN truck ON team_truck = truck_id";
          $sqlTeam .= " JOIN truck_status ON truck_status = truck_status_id";
          $sqlTeam .= " LEFT JOIN geotab ON truck_geo = geo_id";
          $sqlTeam .= " WHERE region_id = " . $valRegion . " AND team_is_act = 1 AND office_is_act = 1 ORDER BY office_city";

          $aTeam = [];
          if ($rowTeam = $this->db->query($sqlTeam)->rows) {
            if (!empty($rowTeam)) {
              foreach ($rowTeam as $t => $myTeam) {

                $result[$k]['team'][$t]['valTeam']            = $valTeam = $myTeam['team_id'];
                $result[$k]['team'][$t]['valOffice']          = $myTeam['office_id'];
                $result[$k]['team'][$t]['webOfficeName']      = $this->util->prepareWeb($myTeam['office_name']);
                $result[$k]['team'][$t]['webOfficeCity']      = $this->util->prepareWeb($myTeam['office_city']);
                $result[$k]['team'][$t]['webOfficeState']     = $this->util->prepareWeb($myTeam['office_state']);
                $result[$k]['team'][$t]['webTeamInfo']        = $this->util->prepareWeb($myTeam['team_info']);
                $result[$k]['team'][$t]['webOfficeInfo']      = $this->util->prepareWeb($myTeam['office_info']);
                $result[$k]['team'][$t]['webTruckInfo']       = $this->util->prepareWeb($myTeam['truck_info']);
                $result[$k]['team'][$t]['webTruckStatus']     = $this->util->prepareWeb($myTeam['truck_status']);
                $result[$k]['team'][$t]['webTruckStatusDsca'] = $this->util->prepareWeb($myTeam['truck_status_dsca']);
                $result[$k]['team'][$t]['valTruck']           = $myTeam['truck_id'];
                $result[$k]['team'][$t]['webLatTruck']        = $myTeam['geo_lat'];
                $result[$k]['team'][$t]['webLonTruck']        = $myTeam['geo_lon'];
                $result[$k]['team'][$t]['valGeoDTLoc']        = $myTeam['geo_dt_loc'];
                $result[$k]['team'][$t]['valGeoComm']         = $myTeam['geo_is_comm'];
                $result[$k]['team'][$t]['valCountRP']         = $myTeam['count_rp'];
                $result[$k]['team'][$t]['valHasHydroxyl']     = $myTeam['office_has_hydroxyl'];

                /* TODO.GEO seperatedly besso
                $webGeoIsNotComm = '';
                if (!$myTeam['geo_is_comm']) {
                  $webGeoIsNotComm = "<img class='icon16' src='icons/error16.png' title='Geotab offline";
                  if ($valGeoDTLoc) {
                    $i = date_diff( new DateTime($valGeoDTLoc), new DateTime() );
                    $webGeoIsNotComm .= ": " . ($i->days > 0 ? $i->format('%a days') : 'Less than a day');
                  }
                  $webGeoIsNotComm .= "' />";
                }
                */

                $aEmp = [];
                $sqlTeamEmp  = "SELECT *, DATEDIFF(" . $this->db->escape(T) . ", emp_dt_hire) AS emp_days FROM team_emp";
                $sqlTeamEmp .= " JOIN emp ON te_emp = emp_id";
                $sqlTeamEmp .= " JOIN team ON te_team = team_id";
                $sqlTeamEmp .= " JOIN team_status ON team_status = team_status_id";
                $sqlTeamEmp .= " JOIN title ON emp_title = title_id";
                $sqlTeamEmp .= " WHERE te_team = " . $valTeam;
                $sqlTeamEmp .= " ORDER BY te_is_sup DESC, title_id, emp_fname";
                if ($rowEmp = $this->db->query($sqlTeamEmp)->rows) {
                  if (!empty($rowEmp)) {
                    foreach ($rowEmp as $e => $myTeamEmp) {

                      $result[$k]['team'][$t]['emp'][$e]['valIsSup']      = $myTeamEmp['te_is_sup'];
                      $result[$k]['team'][$t]['emp'][$e]['valEmp']        = $myTeamEmp['te_emp'];
                      $result[$k]['team'][$t]['emp'][$e]['valIsTrn']      = $myTeamEmp['emp_is_trn'];
                      $result[$k]['team'][$t]['emp'][$e]['valDays']       = $myTeamEmp['emp_days'];
                      $result[$k]['team'][$t]['emp'][$e]['webTitleShort'] = $this->util->prepareWeb($myTeamEmp['title_short']);
                      $result[$k]['team'][$t]['emp'][$e]['webEmpFName']   = $this->util->prepareWeb($myTeamEmp['emp_fname']);
                      $result[$k]['team'][$t]['emp'][$e]['webEmpLName']   = $this->util->prepareWeb($myTeamEmp['emp_lname']);
                      $result[$k]['team'][$t]['emp'][$e]['webEmpTelc']    = $this->util->formatPhone($myTeamEmp['emp_telc']);
                      $result[$k]['team'][$t]['emp'][$e]['webColor']      = $this->util->prepareWeb($myTeamEmp['team_status_color']);

                    }
                  }
                }
              } // foreach Team  
            }
          } // check if Team

          
          $result[$k]['region'] = $aTeam;
        }
      }
    }

    //echo '<pre>'; print_r($result); echo '</pre>';
    return $result;
  }

  function getTeamContackList($teams) {
    $result = [];
    foreach( $teams as $code => $title ) {
      $q  = "SELECT emp_id, concat(emp_fname, ' ', emp_lname) as emp_name, emp_telp, emp_telc FROM emp";
      $q .= " JOIN title ON emp_title = title_id";
      $q .= " WHERE emp_is_act = 1 AND title_short = '$code' AND emp_id NOT IN " . $this->config->get('strEmpHide') . " ORDER BY emp_fname, emp_lname";

      $rows = $this->db->query($q)->rows;
      if ( !empty($rows) ) $result[$title] = $rows;

      
    }
    //echo '<pre>'; print_r($result); echo '</pre>'; exit;

    return $result;
  }

}