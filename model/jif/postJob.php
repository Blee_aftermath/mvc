<?php

/* model
	database
	classify methods
	also prepare best set of data
*/
class ModelJifPostjob extends Model {

	public function getPostJobs($jobID) {

		$query  = "SELECT * FROM post_job p";
  		$query .= "  LEFT JOIN job_pj j on p.post_job_id = j.jpj_post and j.jpj_job = $jobID";
  		$query .= " ORDER BY p.post_job_id";

  		$rows = $this->db->query($query)->rows;

  		return $rows;

  	}

  	public function addPostJob($req) {
    	$jpjID       = $req['jpjID'];
    	$jobID       = $req['jobID'];
    	$postJobID   = $req['postJobID'];
    	$postJobNote = $req['postJobNote'];

    	$query  = "INSERT INTO job_pj SET";
  		$query .= " jpj_job = $jobID,";
  		$query .= " jpj_post = $postJobID,";
  		$query .= " jpj_note = '$postJobNote'";
  		$query .= " ON DUPLICATE KEY UPDATE jpj_note = '$postJobNote'";	

  		return $this->db->query($query);
  	}

  
}
