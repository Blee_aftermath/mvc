<?php

class ModelEmployeeList extends Model {
	// by emp_title
	public function getEmpByTitle($emp_title) {
		$q = "SELECT * FROM emp ";
		$q.= "  JOIN title t on t.title_id = emp.emp_title";
		$q.= " WHERE emp_is_act = 1 AND emp_title = '$emp_title'";
		$rows = $this->db->query($q)->rows;
		return $rows;
	}

	public function getEmpByTitles($aTitle) {
		$q = "SELECT * FROM emp ";
		$q.= "  JOIN title t on t.title_id = emp.emp_title";
		$q.= " WHERE emp_is_act = 1 AND emp_title in (" . implode(',',$aTitle) . ")";
		$rows = $this->db->query($q)->rows;
		return $rows;
	}

	// 
	public function getMyteamByTitle($emp_title) {
		$q = "SELECT * FROM emp";
		$q.= "  JOIN title t on t.title_id = emp.emp_title";
		$q.= "  JOIN team_emp ON emp_id = te_emp ";
		$q.= " WHERE emp_is_act = 1 AND emp_title = '$emp_title'";
		$rows = $this->db->query($q)->rows;
		return $rows;
	}

	public function getMyteamByTitles($aTitle) {
		$q = "SELECT * FROM emp ";
		$q.= "  JOIN title t on t.title_id = emp.emp_title";
		$q.= "  JOIN team_emp ON emp_id = te_emp ";
		$q.= " WHERE emp_is_act = 1 AND emp_title in (" . implode(',',$aTitle) . ")";
		$rows = $this->db->query($q)->rows;
		return $rows;
	}

	public function getHireStat() {
	    $q  = "SELECT";
	    $q .= " SUM(IF(YEAR(emp_dt_hire) = YEAR(NOW()),1,0)) AS 'ytd_hire',";
	    $q .= " SUM(IF(YEAR(emp_dt_term) = YEAR(NOW()),1,0)) AS 'ytd_term',";
	    $q .= " SUM(IF(year(emp_dt_hire) = YEAR(NOW()) AND MONTH(emp_dt_hire) = MONTH(NOW()),1,0)) AS 'mtd_hire',";
	    $q .= " SUM(IF(year(emp_dt_term) = YEAR(NOW()) AND MONTH(emp_dt_term) = MONTH(NOW()),1,0)) AS 'mtd_term'";
	    $q .= " FROM emp";
		$row = $this->db->query($q)->row;
		return $row;
	}
}