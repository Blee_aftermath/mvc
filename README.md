# Mvc

For mvc prototype

https://www.freecodecamp.org/news/the-model-view-controller-pattern-mvc-architecture-and-frameworks-explained/

## Pull the code

git clone https://gitlab.com/Blee_aftermath/mvc.git

## Add new service at IISManager

## DB

```
create database mvc
grant all privileges on mvc.* to 'root'@'localhost'
flush privileges
```

Edit setting in config/config.php

## Direct structure

- controller : request handling, lookup Model, Business Logic, prepare variables and transfer into view
- model : DB layer
- view : html template with variables from Controller
- view/asset : contain all javascript, style, icon, js plugin etc
- system / engine : core MVC classes
- system / library : core library classes like db, config, request, session, util etc
- system / vendor : 3rd party classes like securimage etc
- system / logs : php logs on application level
- system / cache : defind cache storage


## Route

http://url/{directory}/{file}/{method}?{query string}

http://url/jif/postjob/greeting?name={your name}

- the route is controller location as it. so Above route looking for 
    1. controller/jif folder
    2. controller/jif/postJob.php file having 'ControllerJifPostJob' class
    3. greeting(), the member method of the class
    4. Query param $_GET['name'] be sent and can take it in the method


## Controller

Controller play a role of mediator.
    1. request handle
    2. get data from model class (layer)
    3. business logic
    4. transfer best variables to view layer and get HTML
    5. response HTML

```
        // request handle
        $jobID = (null !== $this->request->get['job_id']) ? $this->request->get['job_id'] : '0';

        // get data from model
        $this->load->model('jif/postjob');
        $jobID = '12345';
        $postJobs = [
          'jobID' => $jobID,
          'itemTemplate' => $this->load->view('jif/postJobItem.tpl'),
          'postJob' => $this->model_jif_postjob->getPostJobs($jobID)
        ];
        // prepare best set of variables for view layer
        $basket['postJobs'] = json_encode($postJobs);

        // call independent controller as module 
        $header = $this->load->controller('common/header');
        $left = $this->load->controller('jif/left');
        $footer = $this->load->controller('common/footer');

        $basket['header'] = $header;
        $basket['footer'] = $footer;

        // pass the basket array into view
        $view = $this->load->view('jif/postJob.tpl', $basket);

        // response
        $this->response->setOutput($view);
```

## Call Model inside Controller

In controller, the 'jif/postjob' model class is loaded and called
```
        // this code inside ControllerJifPostJob class

        $this->load->model('jif/postjob');
        $postJobs = [
          'jobID' => $jobID,
          'itemTemplate' => $this->load->view('jif/postJobItem.tpl'),
          'postJob' => $this->model_jif_postjob->getPostJobs($jobID)
        ];
        // prepare best set of variables for view layer
        $basket['postJobs'] = json_encode($postJobs);
```

It works as like routing, $this->load->model('jif/postjob') 

Try to looking for jif folder / postJob.php model file.

Check the postJobs.php file and confirm the 'ModelJifPostJob' class.

So now you can lookup any method of it with this way 

$this->model_jif_postjob->getPostJobs($jobID)

if there is other method like addPostJob($var) then we can call the model with 

$this->model_jif_postjob->addPostJob($var)


## Model

Model is quite simple.

It fetch the data from database by using SQL queries and return the data back to controller.

```
        $query  = "SELECT * FROM post_job p";
        $query .= "  LEFT JOIN job_pj j on p.post_job_id = j.jpj_post and j.jpj_job = $jobID";
        $query .= " ORDER BY p.post_job_id";

        $rows = $this->db->query($query)->rows;
        return $rows;
```

## View

View receives the resultant data from controller file.

Do you remember $basket var at controller ?

In view, we take each variable one by one and can use it in view.

(controller) $basket['postJobs'] is just $postJobs var in view.

View file is located as same manner.

Please check below code in view/jif/postjob.tpl

```
    <script type='module'>
       import {PostJob} from '/view/asset/js/jif/postJob.js?$scriptUUID';
       new PostJob(<?php echo $postJobs ?>)
    </script>
```

## Bootstrap

There are so many devices and browser resolutions.

We can easily develop the responsive design with maden css bundles.

https://getbootstrap.com/docs/5.1/examples/

In our resository, you can pull the bootstrap v5.1.1 (Sep.2021)

git pull https://gitlab.com/Blee_aftermath/bootstrap511.git bootstrap

Just call bootstrap/index.html locally in your browser and check the example pages

It's common to look around and think it before you do page design 


## Bootstrap in Team page

http://url/team/list?bootstrap=true#

It could be best examples for MVC, DB handling, code resuability, Bootstrap etc.

Especially for web design, we can think about the bootstrap related on our portal.


## Define Javascript, Css in Portal

Our portal use standard javascript, jQuery and module js so we need a logic to assign proper JS and CSS per page.

TODO. figure out exact use case for each services


## Configuration

MVC conf are available thru file and DB both.

```
$config->load('variables.inc');
$registry->set('config', $config);

// then use in Controller and Model like
$this->config->get('foo');

```

We dont have admin console where one can CRUD any configuration variables.
So we can think about if there are configurations editable out of IT.

```

// get data from form and
$this->config->get('foo', $var);

``` 

## Util, common functions

On system/library/util.php, I moved functions.php logics and we can use easily like below

```

// call existing methods as class method manner
$this->util->print_array($arrFields);

```

## With Module javascript

http://url/jif/postjob

Please check below codes

Controller/jif/postJob.php

Model/jif/postJob.php

and View/jif/postJob.tpl

postJob.tpl show how we preserve most Module JS codes

```

                    <div name="postJob" style="background-color: white;padding-bottom: 15px">
                        <div class="block-header">Post Job Recommendation</div>
                        <p style="padding:10px">Select all applicable recommendations for additional work needed after completing our scope. <br/><span style="font-style: italic">Write one in Other if not on the list.</span></br> Customer must sign that they understand post job recommendation(s).</p>
                        <div>
                           <table class="data data-alt1" width="100%">
                              <thead>
                                 <tr>
                                    <th style="width:25%">Recommendation</th>
                                    <th style="width:3%"></th>
                                    <th style="width:66%;">Notes</th>
                                    <th style="width:6%;"></th>
                                 </tr>
                              </thead>
                              <tbody name="postJobBody">
                              </tbody>
                           </table>
                        </div>
                     </div>

                     <script type='module'>
                           import {PostJob} from '/view/asset/js/jif/postJob.js?$scriptUUID';
                           new PostJob(<?php echo $postJobs ?>)
                     </script>

```


## Auth

TBD
Just consist basic login/logout then need more job.


## More modular

TBD
header,footer is good example of module. Then no custom module yet. 


## Request, Response

$_GET[] is equivalent with $this->response->get[]
$_POST[] is equivalent with $this->response->post[]
php://input for API json param is equivalent with $this->response->json[]





