<?php
class Employee {
	private $emp_id;
	private $emp_fname;
	private $emp_lname;
	private $emp_email;
	private $emp_telc;
	private $fax;
	private $newsletter;
	private $employee_group_id;
	private $address_id;

	public function __construct($registry) {
		$this->config = $registry->get('config');
		$this->db = $registry->get('db');
		$this->request = $registry->get('request');
		$this->session = $registry->get('session');

		if (isset($this->session->data['emp'])) {
			$employee_query = $this->db->query("SELECT * FROM emp WHERE emp_id = '" . (int)$this->session->data['emp'] . "' AND emp_is_act = '1'");

			if ($employee_query->num_rows) {
				$this->emp_id = $employee_query->row['emp_id'];
				$this->emp_fname = $employee_query->row['emp_fname'];
				$this->emp_lname = $employee_query->row['emp_lname'];
				$this->emp_email = $employee_query->row['emp_email'];
				$this->emp_telc = $employee_query->row['emp_telc'];

			} else {
				$this->logout();
			}
		}
	}

	public function login($emp_email, $password, $override = false) {
		if ($override) {
			$employee_query = $this->db->query("SELECT * FROM employee WHERE LOWER(emp_email) = '" . $this->db->escape(utf8_strtolower($emp_email)) . "' AND status = '1'");
		} else {
			$employee_query = $this->db->query("SELECT * FROM employee WHERE LOWER(emp_email) = '" . $this->db->escape(utf8_strtolower($emp_email)) . "' AND (password = SHA1(CONCAT(salt, SHA1(CONCAT(salt, SHA1('" . $this->db->escape($password) . "'))))) OR password = '" . $this->db->escape(md5($password)) . "') AND status = '1' AND approved = '1'");
		}

		if ($employee_query->num_rows) {
			$this->session->data['emp_id'] = $employee_query->row['emp_id'];

			if ($employee_query->row['cart'] && is_string($employee_query->row['cart'])) {
				$cart = unserialize($employee_query->row['cart']);

				foreach ($cart as $key => $value) {
					if (!array_key_exists($key, $this->session->data['cart'])) {
						$this->session->data['cart'][$key] = $value;
					} else {
						$this->session->data['cart'][$key] += $value;
					}
				}
			}

			if ($employee_query->row['wishlist'] && is_string($employee_query->row['wishlist'])) {
				if (!isset($this->session->data['wishlist'])) {
					$this->session->data['wishlist'] = array();
				}

				$wishlist = unserialize($employee_query->row['wishlist']);

				foreach ($wishlist as $product_id) {
					if (!in_array($product_id, $this->session->data['wishlist'])) {
						$this->session->data['wishlist'][] = $product_id;
					}
				}
			}

			$this->emp_id = $employee_query->row['emp_id'];
			$this->emp_fname = $employee_query->row['emp_fname'];
			$this->emp_lname = $employee_query->row['emp_lname'];
			$this->emp_email = $employee_query->row['emp_email'];
			$this->emp_telc = $employee_query->row['emp_telc'];
			$this->fax = $employee_query->row['fax'];
			$this->newsletter = $employee_query->row['newsletter'];
			$this->employee_group_id = $employee_query->row['employee_group_id'];
			$this->address_id = $employee_query->row['address_id'];

			$this->db->query("UPDATE employee SET ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "' WHERE emp_id = '" . (int)$this->emp_id . "'");

			return true;
		} else {
			return false;
		}
	}

	public function logout() {
		$this->db->query("UPDATE employee SET cart = '" . $this->db->escape(isset($this->session->data['cart']) ? serialize($this->session->data['cart']) : '') . "', wishlist = '" . $this->db->escape(isset($this->session->data['wishlist']) ? serialize($this->session->data['wishlist']) : '') . "' WHERE emp_id = '" . (int)$this->emp_id . "'");

		unset($this->session->data['emp_id']);

		$this->emp_id = '';
		$this->emp_fname = '';
		$this->emp_lname = '';
		$this->emp_email = '';
		$this->emp_telc = '';
		$this->fax = '';
		$this->newsletter = '';
		$this->employee_group_id = '';
		$this->address_id = '';
	}

	public function isLogged() {
		return $this->emp_id;
	}

	public function getId() {
		return $this->emp_id;
	}

	public function getFirstName() {
		return $this->emp_fname;
	}

	public function getLastName() {
		return $this->emp_lname;
	}

	public function getEmail() {
		return $this->emp_email;
	}

	public function getTelephone() {
		return $this->emp_telc;
	}

	public function getFax() {
		return $this->fax;
	}

	public function getNewsletter() {
		return $this->newsletter;
	}

	public function getGroupId() {
		return $this->employee_group_id;
	}

	public function getAddressId() {
		return $this->address_id;
	}

	public function getBalance() {
		$query = $this->db->query("SELECT SUM(amount) AS total FROM employee_transaction WHERE emp_id = '" . (int)$this->emp_id . "'");

		return $query->row['total'];
	}

	public function getRewardPoints() {
		$query = $this->db->query("SELECT SUM(points) AS total FROM employee_reward WHERE emp_id = '" . (int)$this->emp_id . "'");

		return $query->row['total'];
	}




	// custom methods for emp lookup

	
}