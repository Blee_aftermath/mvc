<?php

class Util {
	
	private $registry ;
	
	public function __construct($registry) {
		$this->db = $registry->get('db');
	}
	
	function print_array($array) {
	  echo "<p style='text-align:left'>";
	  foreach ($array as $key => $value) { echo "[$key] => $value<br />"; }
	  echo "</p>";
	}

	function fnRedirect($site) {
	  echo "<script type=\"text/javascript\"><!--\r\n";
	  echo "window.location = \"$site\"\r\n";
	  echo "//--></script>\r\n";
	}

	function showAlert($text) { ?>
	  <script language="javascript">
	      <!-- Hide Script
	      alert("<?php echo $text; ?>")
	      //End Hide Script-->
	  </script> <?php
	}

	function prepareTitle($txt) {    // Prepare a new title for insert            **** TO BE DELETED ****
	  $txt = trim($txt);             // Strip white space beginning and end
	  $txt = strip_tags($txt);       // Remove all <> delimited strings - prevents even a single < symbol
	  $txt = htmlspecialchars($txt); // Convert any remaining &" (phpbb did this)
	  return $txt;
	}

	function prepareFrm($txt) {
	  $txt = str_replace(chr(160)," ", $txt);  // chr(160) (non-breaking space) is sent by Chrome if a trailing space is left in a textarea.  Strip these out or it crashes the mysql update.
	  $txt = trim($txt);
	  $txt = htmlspecialchars ($txt, ENT_QUOTES); // Convert symbols to &amp; &quot; &lt; &gt; &#039 (ent_quotes) for web display // Put back in 10/29/18 because single apostrophe in an input field was cutting off the field (10' Pole)
	  return $txt;
	}

	function prepareWeb($txt) {
	  $txt = trim($txt);
	  $txt = htmlspecialchars ($txt, ENT_QUOTES); // Convert symbols to &amp; &quot; &lt; &gt; &#039 (ent_quotes) for web display
	  $txt = nl2br($txt);
	  return $txt;
	}

	function prepareSaf($txt) {             // Prepare text from form fields for database insert
	  
	  $txt = str_replace(chr(160)," ", $txt);  // chr(160) (non-breaking space) is sent by Chrome if a trailing space is left in a textarea.  Strip these out or it crashes the mysql update.
	  $txt = htmlspecialchars_decode ($txt, ENT_QUOTES); // Convert &amp; &quot; &lt; &gt; &#039 (ent_quotes) to symbols for storage.
	  $txt = strip_tags($txt);               // Remove all <> delimited strings - prevents even a single < symbol
	  $txt = trim($txt);
	  $txt = "'" . $this->db->escape($txt) . "'";  // Escape and Quote
	  return $txt;
	}

	/**
	 * Trim end quotes off of prepareSaf so $txt can be used in a prepared "placeholder" statement
	 * @param string $txt
	 */
	function prepareSan($txt) {
	  return trim($this->prepareSaf($txt), "'");
	}

	function prepareHtm($txt) {             // 
	  
	  global $allowedTags;
	  $txt = htmlspecialchars_decode ($txt, ENT_QUOTES); // Convert &amp; &quot; &lt; &gt; &#039 (ent_quotes) to symbols for storage.
	  $txt = strip_tags($txt, $allowedTags);       // Remove all <> delimited strings - prevents even a single < symbol
	  $txt = trim($txt);
	  if (!is_numeric($txt)) { $txt = "'" . $this->db->escape($txt) . "'"; }  // Escape and Quote
	  return $txt;
	}

	function prepareJS($str) {
		// remove carriage return
		// $str = str_replace("\r", '', (string) $str);  // Taken out 2/18/20 MSims.  Textareas were not retaining new lines.  Office -> Addl Info
		// escape all characters with ASCII code between 0 and 31
		$str = addcslashes($str, "\0..\37'\\");
		// escape double quotes
		$str = str_replace('"', '\"', $str);
		// replace \n with double quotes
		$str = str_replace("\n", '\n', $str); 
		return $str;
	}

	function prepareFil($txt) {                                 // Prepare file names
	  $txt = trim($txt);                                        // Remove any leading & trailing spaces
	  $txt = preg_replace("/\s/", "-", $txt);                   // Spaces to dashes
	  $txt = preg_replace("/-+/", "-", $txt);                   // Dashes to dash

	  $extPos = strrpos($txt, ".");
	  $ext    = strtolower(substr($txt, $extPos + 1));          // Get and lowercase extension
	  $base   = substr($txt, 0, $extPos);                       // Get the base

	  $ext = preg_replace("/[^a-z0-9]/", "", $ext);             // Remove non-alphanumeric chars
	  if ( strlen($ext) < 2 || strlen($ext) > 4 ) return false; // Return false if ext is not 2 or 4 chars

	  $base = preg_replace("/[\\\\<>:\"\/|?*]/", "", $base);    // Remove Windows illegal file characters
	  $base = preg_replace("/^\./", "", $base);                 // Remove first character period
	  if ( strlen($base) < 1 ) return false;                    // Return false if base name is not at least 1 char

	  return $base . "." . $ext;
	}

	function prepareXSS($txt) {
	  $txt = str_replace(chr(160)," ", $txt);  // chr(160) (non-breaking space) is sent by Chrome if a trailing space is left in a textarea.  Strip these out or it crashes the mysql update.
	  $txt = strip_tags($txt);
	  return htmlspecialchars($txt, ENT_QUOTES);
	}

	function escape_and_quote($value) {
	  
	  global $pdo;
	  if (isset($mysqli)) {
	    return  "'" . $this->db->escape($value) . "'";
	  } else if (isset($pdo)) {
	    return $pdo->quote($value);
	  } else {
	    return false;
	  }
	}

	function truncate($txt, $len) {
	  RETURN ((strlen($txt) > $len + 2) ? substr($txt,0,$len) . "..." : $txt);
	}

	function formatBytes($bytes, $precision = 2) { 
	  $units = array('B', 'KB', 'MB', 'GB', 'TB'); 
	  $bytes = max($bytes, 0); 
	  $pow = floor(($bytes ? log($bytes) : 0) / log(1024)); 
	  $pow = min($pow, count($units) - 1); 
	  $bytes /= pow(1024, $pow);
	  return round($bytes, $precision) . ' ' . $units[$pow];
	}

	function formatDTMysql($time) { if (is_object($time)) return ($time->format('Y-m-d H:i:s'));
	                                else                  return (date('Y-m-d H:i:s', $time)); } // 2009-11-23 14:23:30

	function formatDMysql($time)  { if (is_object($time)) return ($time->format('Y-m-d'));
	                                else                  return (date('Y-m-d', $time)); } // 2009-11-23

	function formatDText($time)  { if (empty($time)) return ""; else return date("m/d/y", $time); } // date input fields and display on pages
	function formatDYText($time) { if (empty($time)) return ""; else return date("m/d/Y", $time); } // date input fields and display on pages - 4 digit year
	function formatTText($time)  { if (empty($time)) return ""; else return date("h:ia", $time); }
	function formatDTText($time) { if (empty($time)) return ""; else return str_replace(" ", "&nbsp;", date("m/d/y h:ia", $time)); }
	function formatDLong($time)  { if (empty($time)) return ""; else return date("M d, Y", $time); } // date input fields and display on pages

	function formatDAct($time)  { return date("n/j/y", $time); }   // 9/1/08
	function formatTAct($time)  { return date("g:i A", $time); } // 6:29 AM Took off the :s seconds 8/19/2016 MS because 12:00:00 PM was importing to ACT as 12:00 AM

	function convertStringD($string) {
	  if (strtotime($string)) {
	    $d = date('Y-m-d', strtotime($string));
	    return $d;
	  } else {
	    return FALSE;
	  }
	}

	function convertStringT($string) {
	  if (strtotime($string)) {
	    $t = date('g:i A', strtotime($string));
	    return $t;
	  } else {
	    return FALSE;
	  }
	}

	/**
	 * Provides an array of 24 hours, 15 minute increments in time
	 */
	function timeArr() {
	  $arrTime = [];
	  for ($ampm=0; $ampm<2; $ampm++) for ($hours=0; $hours<12; $hours++) for ($mins=0; $mins<60; $mins+=15) {
	    $arrTime[] = ($hours=='00'?"12":sprintf("%02d", $hours)) . ":" . str_pad($mins, 2, '0', STR_PAD_LEFT) . "" . ["am","pm"][$ampm];
	  }
	  return $arrTime;
	}

	function fnHlThresh($value, $thresh = 10) {
	  if ($value <= -$thresh) return " hlneg";
	  if ($value >=  $thresh) return " hlpos";
	}

	function changeTimeZone($dateString, $timeZoneSource = null, $timeZoneTarget = null) {
	  if (empty($timeZoneSource)) {
	    $timeZoneSource = date_default_timezone_get();
	  }
	  if (empty($timeZoneTarget)) {
	    $timeZoneTarget = date_default_timezone_get();
	  }

	  $dt = new DateTime($dateString, new DateTimeZone($timeZoneSource));
	  $dt->setTimezone(new DateTimeZone($timeZoneTarget));

	  return $dt->format("Y-m-d H:i:s");
	}

	function changeTimeToZip($dateString, $zip) {
	  
	  $safZip = $this->prepareSaf($zip);
	  $sqlZip = "SELECT * FROM zip LEFT JOIN timezone ON zip_tz = tz_zone WHERE zip_code = $safZip";
	  if ($resZip = $this->db->query($sqlZip)) {
	    if ($resZip->num_rows == 1) {
	      $myZip = $resZip->rows;
	      $tzName = $myZip['tz_name'];
	      return changeTimeZone($dateString, "", $tzName);
	    }
	  }
	}

	function fnYrMo($yr, $mo) {
	  $strYrMo  = $yr . ($mo < 10 ? "0" : "") . $mo;
	  return $strYrMo;
	}

	function formatAddr($fname, $lname, $title, $company, $line1, $line2, $city, $state, $zip) {
	  if (!empty($fname))     $arrAddrName[] = prepareWeb($fname);
	  if (!empty($lname))     $arrAddrName[] = prepareWeb($lname);

	  if (!empty($city))      $arrCityState[] = prepareWeb($city);
	  if (!empty($state))     $arrCityState[] = prepareWeb($state);
	  if (isset($arrCityState))              $arrCityStateZip[] = implode(", ", $arrCityState);
	  if (!empty($zip))       $arrCityStateZip[] = prepareWeb($zip);

	  if (isset($arrAddrName))               $arrAddr[] = "<b>" . implode(" ", $arrAddrName) . "</b>";
	  if (!empty($title))     $arrAddr[] = "<i>" . prepareWeb($title) . "</i>";
	  if (!empty($company))   $arrAddr[] = prepareWeb($company);
	  if (!empty($line1))     $arrAddr[] = prepareWeb($line1);
	  if (!empty($line2))     $arrAddr[] = prepareWeb($line2);
	  if (isset($arrCityStateZip))           $arrAddr[] = implode(" ", $arrCityStateZip);
	  return implode("<br />", $arrAddr);
	}

	function formatPhone($phone) {
	  switch (strlen($phone)) {
	  case 7:
	    $webPhone = substr($phone, 0, 3) . "-" . substr($phone, 3, 4);
	  break;
	  case 10:
	    $webPhone = "(" . substr($phone, 0, 3) . ") " . substr($phone, 3, 3). "-" . substr($phone, 6, 4);
	  break;
	  default:
	    $webPhone = $phone;
	  break;
	  }
	  return $webPhone;
	}


	function formatPhoneExt($phone, $ext) {
	  $webPhone = formatPhone($phone);
	  if ($ext) $webPhone .= " x" . $ext;
	  return $webPhone;
	}

	function formatPhoneDash($phone) {
	  switch (strlen($phone)) {
	  case 7:
	    $webPhone = substr($phone, 0, 3) . "-" . substr($phone, 3, 4);
	  break;
	  case 10:
	    $webPhone = substr($phone, 0, 3) . "-" . substr($phone, 3, 3). "-" . substr($phone, 6, 4);
	  break;
	  default:
	    $webPhone = $phone;
	  break;
	  }
	  return $webPhone;
	}

	function strtophone($s, $r) {
	  $rx = "/
	      (1)?\D*     # optional country code
	      (\d{3})?\D* # optional area code
	      (\d{3})\D*  # first three
	      (\d{4})     # last four
	      (?:\D+|$)   # extension delimiter or EOL
	      (\d*)       # optional extension
	  /x";
	  preg_match($rx, $s, $matches);
	  if(!isset($matches[0])) return false;

	  $country = $matches[1];
	  $area = $matches[2];
	  $three = $matches[3];
	  $four = $matches[4];
	  $ext = $matches[5];

	  $out = "$three-$four";
	  if(!empty($area)) $out = "($area) $out";
	  //if(!empty($country)) $out = "+$country-$out";
	  //if(!empty($ext)) $out .= " x$ext";

	  switch ($r) {
	  case "tel":
	    return $out;
	  break;
	  case "ext":
	    return $ext;
	  break;
	  }
	}

	function stripNumber($str) {
	  $num = str_replace(array(',','$','%'), '', $str );
	  return $num;
	}

	function stripPhone($phone) {
	  $phone = preg_replace('/\D/', '', $phone); // Strip everything that is not a digit
	  return $phone;
	}

	function format_price($price) {
	  if (round($price,2) == round($price,3)) {
	    return number_format($price,2);
	  } else {
	    return number_format($price,3);
	  }  
	}

	function sanityCheck($string, $type, $maxl, $minl) {
	  $type = 'is_'.$type;
	  if(!$type($string))                 { return FALSE; }
	//  elseif($minl > 0 && empty($string)) { return FALSE; }  // Took this out otherwise a string can't be 0
	  elseif(strlen($string) > $maxl)     { return FALSE; }
	  elseif(strlen($string) < $minl)     { return FALSE; }
	  return TRUE;
	}

	function is_numint0($int) {
	  if (is_numeric($int) && (floor($int) == $int) && ($int >= 0)) { return TRUE; } else { return FALSE; }
	}

	function is_numint($int) {
	  if (is_numeric($int) && (floor($int) == $int) && ($int > 0)) { return TRUE; } else { return FALSE; }
	}

	function is_numwho($int) {
	  if (is_numeric($int) && (floor($int) == $int) && ($int >= 0)) { return TRUE; } else { return FALSE; }
	}

	function is_numint_pn($int) {
	  if (is_numeric($int) && (floor($int) == $int)) { return TRUE; } else { return FALSE; }
	}

	function ok_numeric($num) {
	  if (is_numeric($num) && ($num > 0)) { return TRUE; } else { return FALSE; }
	}

	function is_currency($string) {
	  if (preg_match('/^[0-9]+(?:\.[0-9]{1,2})?$/', $string)) { return TRUE; } else { return FALSE; }
	// for negative currency, allow /^-?[0-9]+(?:\.[0-9]{1,2})?$/
	// http://stackoverflow.com/questions/4982291/how-to-check-if-an-entered-value-is-currency
	}


	function is_date($date) {
	  $date = str_replace(array(" ", "-", ",", "."), "/", $date);
	  $arr = explode('/', $date);
	  if (count($arr) != 3) { return FALSE; }
	  $mm = $arr[0];
	  $dd = $arr[1];
	  $yy = $arr[2];
	  if (($yy > 100 & $yy < 1900) || ($yy >= 2079)) { return FALSE; } // Per smalldatetime valid Jan 1 1900 - June 6 2079
	  if ($yy == 0) { $yy = 2000; }
	  if (!is_numeric($mm) || !is_numeric($dd) || !is_numeric($yy) || !checkdate($mm, $dd, $yy)) { return FALSE; } else { return TRUE; }
	}

	  /**
	   * @deprecated See validateField function
	   */
	  function fnValidate($inpField, $dbField, $is_req, $type, $min, $max, $modalId = "") { // docPRG, addressPRG, itemPRG

	    global $arrFields;
	    global $errCount;

	    if (strlen($_POST[$inpField]) == 0) {
	      if (!$is_req) {
	        $arrFields[] = $dbField . " = NULL";
	        return true;
	      }
	    } else {
	      switch ($type) {
	      case "string":
	        if (sanityCheck($_POST[$inpField], $type, $max, $min)==TRUE) {
	          $arrFields[] = $dbField . " = " . $this->prepareSaf($_POST[$inpField]);
	          return true;
	        }
	      break;
	      case "phone":
	        $tmpTel = stripPhone($_POST[$inpField]);
	        if (is_numint($tmpTel) && sanityCheck($tmpTel, 'string', $max, $min)==TRUE) {
	          $arrFields[] = $dbField . " = " . $this->prepareSaf($tmpTel);
	          return true;
	        }
	      break;
	      case "digits":
	        if (is_numint0($_POST[$inpField]) && sanityCheck($_POST[$inpField], 'string', $max, $min)==TRUE) {
	          $arrFields[] = $dbField . " = " . $this->prepareSaf($_POST[$inpField]);
	          return true;
	        }
	      break;
	      case "decimal":
	        if (ok_numeric($_POST[$inpField]) && sanityCheck($_POST[$inpField], 'string', $max, $min)==TRUE) {
	          $arrFields[] = $dbField . " = " . $this->prepareSaf($_POST[$inpField]);
	          return true;
	        }
	      break;
	      case "decimal_0":
	        if (is_numeric($_POST[$inpField]) && sanityCheck($_POST[$inpField], 'string', $max, $min)==TRUE) {
	          $arrFields[] = $dbField . " = " . $this->prepareSaf($_POST[$inpField]);
	          return true;
	        }
	      break;
	      case "email":
	        if (sanityCheck($_POST[$inpField], 'string', $max, $min)==TRUE && filter_var($_POST[$inpField], FILTER_VALIDATE_EMAIL)) {
	          $arrFields[] = $dbField . " = " . $this->prepareSaf(strtolower($_POST[$inpField]));
	          return true;
	        }
	      break;
	      case "date":
	        if (sanityCheck($_POST[$inpField], 'string', $max, $min)==TRUE && strtotime($_POST[$inpField])) {
	          $arrFields[] = $dbField . " = " . $this->prepareSaf(formatDTMysql(strtotime($_POST[$inpField])));
	          return true;
	        }
	      break;
	      } // switch
	    }
	    $_SESSION["err" . $modalId . "Field"][]=$inpField;
	    $errCount++;
	    return false;
	  }

	    /**
	   * A variation of fnValidate to be used in AJAX PRGs. 
	   * This function does not use the globals errCount or arrFields, 
	   * and instead relies on its input/return values. 
	   * It also does not use the session, expanding its use case
	   * without interfering with session values older PRGs use.
	   * 
	   * @param array $fields Array that sql safe values will be appended to
	   * @param array $errors Array that error messages will be appended to
	   *  
	   * @return mixed If the validation is passed, the SQL safe value is returned.
	   * On fail, false is returned.
	   */
	  function validateField($dbField, $value, $is_req, $type, $min, $max, &$fields, &$errors){
	    if ($type != 'bool' && strlen($value) == 0) {
	      if (!$is_req) {
	        $fields[] = sprintf('%s = NULL', $dbField);
	        return true;
	      }
	    } else {
	      switch ($type) {
	      case "string":
	        if (sanityCheck($value, $type, $max, $min)==TRUE) {
	          $fields[] = sprintf('%s = %s', $dbField, $this->prepareSaf($value));
	          return true;
	        }
	      break;
	      case "phone":
	        $tmpTel = stripPhone($value);
	        if (is_numint($tmpTel) && strlen($tmpTel) == 10 && sanityCheck($tmpTel, 'string', $max, $min)==TRUE) {
	          $fields[] = sprintf('%s = %s', $dbField, $this->prepareSaf($tmpTel));
	          return true;
	        }
	      break;
	      case "digits":
	        if (is_numint0($value) && sanityCheck($value, 'string', $max, $min)==TRUE) {
	          $fields[] = sprintf('%s = %s', $dbField, $this->prepareSaf($value));
	          return true;
	        }
	      break;
	      case "decimal":
	        if (ok_numeric($value) && sanityCheck($value, 'string', $max, $min)==TRUE) {
	          $fields[] = sprintf('%s = %s', $dbField, $this->prepareSaf($value));
	          return true;
	        }
	      break;
	      case "decimal_0":
	        if (is_numeric($value) && sanityCheck($value, 'string', $max, $min)==TRUE) {
	          $arrFields[] = $dbField . " = " . $this->prepareSaf($value);
	          return true;
	        }
	      break;
	      case "email":
	        if (sanityCheck($value, 'string', $max, $min)==TRUE && filter_var($value, FILTER_VALIDATE_EMAIL)) {
	          $fields[] = sprintf('%s = %s', $dbField, $this->prepareSaf(strtolower($value)));
	          return true;
	        }
	      break;
	      case "date":
	        if (sanityCheck($value, 'string', $max, $min)==TRUE && strtotime($value)) {
	          $fields[] = sprintf('%s = %s', $dbField, $this->prepareSaf(formatDTMysql(strtotime($value))));
	          return true;
	        }
	      break;
	      case 'bool':
	        if(is_bool($value)){
	          $fields[] = sprintf('%s = %s', $dbField, $value ? 1 : 0);
	          return true;
	        }
	      break;
	      } 
	    }

	    $errors[] = $dbField;
	    return false;
	  }

	function fnRandomString($length) {
	    $key = '';
	    $keys = array_merge(range(0, 9), range('a', 'z'));
	    for ($i = 0; $i < $length; $i++) $key .= $keys[array_rand($keys)];
	    return $key;
	}

	function pregCheck($string, $type) {
	  switch ($type) {
	  case "name":   if (preg_match('/^[a-zA-Z0-9][a-zA-Z0-9_\- ]*[a-zA-Z0-9]$/', $string)) { return TRUE; }
	                 // Must begin and end with a letter or number, but may also contain _ - or spaces in the middle - Used for Store Name and Wanted List Name
	  break;
	  case "member": if (preg_match('/^[a-zA-Z][a-zA-Z0-9_*]*[a-zA-Z0-9]$/', $string)) { return TRUE; }
	                 // Must begin and end with a letter and end with a letter or number, but may also contain _ * in the middle - Used for Username and Passwords
	  break;
	  case "color":  if (preg_match('/^[A-Fa-f0-9]{6}$/i', $string)) { return TRUE; }
	  break;
	  }
	  return FALSE;
	}

	function getUsersOnline() {
	  
	  $sqlOnline = "SELECT COUNT(*) AS on_count FROM online";
	  $resOnline = $this->db->query($sqlOnline);
	  $myOnline  = $resOnline->rows;
	  return $myOnline['on_count'];
	}

	function getUploadErrorDetail($error) {
	  switch($error) {

	  case 0:  $errDetailed = "Success";             break;
	  case 1:  $errDetailed = "File is too large";   break;
	  case 2:  $errDetailed = "File is too large";   break;
	  case 3:  $errDetailed = "Partial upload";      break;
	  case 4:  $errDetailed = "No file selected";    break;
	  case 6:  $errDetailed = "Missing temp folder"; break;
	  case 7:  $errDetailed = "Failed disk write";   break;
	  case 8:  $errDetailed = "Invalid extension";   break;

	  }
	  return $errDetailed;
	}

	function updVisitorNum() {
	  
	  $sql_upd_visitor = "UPDATE statistic SET stat_visitor = stat_visitor + 1";
	  $res_upd_visitor = $this->db->query($sql_upd_visitor);
	}

	function getVisitorNum() {
	  
	  $sql_get_visitor = "SELECT stat_visitor FROM statistic";
	  $res_get_visitor = $this->db->query($sql_get_visitor);
	  $my_visitor = $res_get_visitor->rows;
	  return $my_visitor['stat_visitor'];
	}

	function getFormattedAddress($webLine1, $webLine2, $webLine3, $webCity, $webState, $webZip, $delim) {

	  if (!isset($delim)) $delim = "<br />";

	  $strAddress = $webLine1 . $delim;
	  if (strlen($webLine2) > 0) { $strAddress .= $webLine2 . $delim; }
	  if (strlen($webLine3) > 0) { $strAddress .= $webLine3 . $delim; }
	  if (strlen($webCity) > 0)  { $strAddress .= $webCity  . ", "; }
	  if (strlen($webState) > 0) { $strAddress .= $webState . " "; }
	  if (strlen($webZip) > 0)   { $strAddress .= $webZip; }

	  return $strAddress;
	}

	function fnModalMessages() {

	  if (isset($_SESSION['errModal'])) {

	    if (is_array($_SESSION['errModal'])) {
	      echo "<div class='content error';><p>" . implode("<br />", $_SESSION['errModal']) . "</p></div>";
	    } else {
	      echo "<div class='content error';><p>" . $_SESSION['errModal'] . "</p></div>";
	    }
	  }

	  if (isset($_SESSION['errModal'])) {
	    echo "<div class='body_stripe'></div>";
	  }
	  unset($_SESSION['errModal']);
	}

	function fnMessages() {

	  if (isset($_SESSION['msgInfo'])) {
	    if (is_array($_SESSION['msgInfo'])) {
	      $_SESSION['msgInfo'] = array_unique($_SESSION['msgInfo']);
	      echo "<div class='body_content_slim message';><p>" . implode("<br />", $_SESSION['msgInfo']) . "</p></div>";
	    } else {
	      echo "<div class='body_content_slim message';><p>" . $_SESSION['msgInfo'] . "</p></div>";
	    }
	  }
	  if (isset($_SESSION['errInfo'])) {
	    if (is_array($_SESSION['errInfo'])) {
	      $_SESSION['errInfo'] = array_unique($_SESSION['errInfo']);
	      echo "<div class='body_content_slim error';><p>" . implode("<br />", $_SESSION['errInfo']) . "</p></div>";
	    } else {
	      echo "<div class='body_content_slim error';><p>" . $_SESSION['errInfo'] . "</p></div>";
	    }
	  }
	  if (isset($_SESSION['altInfo'])) {
	    if (is_array($_SESSION['altInfo'])) {
	      $_SESSION['altInfo'] = array_unique($_SESSION['altInfo']);
	      echo "<div class='body_content_slim alert';><p>" . implode("<br />", $_SESSION['altInfo']) . "</p></div>";
	    } else {
	      echo "<div class='body_content_slim alert';><p>" . $_SESSION['altInfo'] . "</p></div>";
	    }
	  }
	  if (isset($_SESSION['drkInfo'])) {
	    if (is_array($_SESSION['drkInfo'])) {
	      $_SESSION['drkInfo'] = array_unique($_SESSION['drkInfo']);
	      echo "<div class='body_content_slim dark';><p>" . implode("<br />", $_SESSION['drkInfo']) . "</p></div>";
	    } else {
	      echo "<div class='body_content_slim dark';><p>" . $_SESSION['drkInfo'] . "</p></div>";
	    }
	  }

	  if (isset($_SESSION['msgInfo']) || isset($_SESSION['errInfo']) || isset($_SESSION['altInfo']) || isset($_SESSION['drkInfo'])) {
	    echo "<div class='body_stripe'></div>";
	  }
	  unset($_SESSION['msgInfo']);
	  unset($_SESSION['errInfo']);
	  unset($_SESSION['altInfo']);
	  unset($_SESSION['drkInfo']);
	}

	function fnSetPageFilter($page, $field) {

	  if (isset($_SESSION[$page][$field]) && $_SESSION[$page][$field] == $_GET[$field]) {
	    unset($_SESSION[$page][$field]);
	    if (count($_SESSION[$page]) == 0) { unset($_SESSION[$page]); }
	  } else {
	    $_SESSION[$page][$field] = $_GET[$field];
	  }
	}

	function fnSortArrows($field, $persistentGetHeader = false) {
	  global $PHP_SELF;
	  global $sort;
	  global $desc;
	  if ($field == $sort) {
	    if ($desc) {
	      $srcUp = "icons/up16.png";
	      $srcDn = "icons/dn16on.png";
	    } else {
	      $srcUp = "icons/up16on.png";
	      $srcDn = "icons/dn16.png";
	    }
	  } else {
	    $srcUp = "icons/up16.png";
	    $srcDn = "icons/dn16.png";
	  }      
	  return "<a href='$PHP_SELF?" . ($persistentGetHeader ? ($_SERVER['QUERY_STRING'] . "&") : "") ."sort=$field'><img src='$srcUp' /></a><a href='$PHP_SELF?" . ($persistentGetHeader ? ($_SERVER['QUERY_STRING'] . "&") : "") ."sort=$field&desc'><img src='$srcDn' /></a>";
	}

	function encode_ip($dotquad_ip) {	
	  $ip_sep = explode('.', $dotquad_ip);
	  if ( isset($ip_sep[3]) ) {
	    return sprintf('%02x%02x%02x%02x', $ip_sep[0], $ip_sep[1], $ip_sep[2], $ip_sep[3]);
	  }
	}

	function decode_ip($int_ip)
	{	$hexipbang = explode('.', chunk_split($int_ip, 2, '.'));
		return hexdec($hexipbang[0]). '.' . hexdec($hexipbang[1]) . '.' . hexdec($hexipbang[2]) . '.' . hexdec($hexipbang[3]);
	}

	function fnLogin($fnLoginID, $fnPassword, $fnRememberMe) {
			
	  global $safIPHex;
	  global $safIPAddr;
	  global $safBrowser;
	  global $maxDaysPass;

	  $safLoginID  = $this->prepareSaf($fnLoginID);
	  $safPassword = $this->prepareSaf($fnPassword);

	  $sqlLogin = "SELECT *, DATEDIFF(CURDATE(), emp_dt_pass) AS emp_days_pass FROM emp JOIN office ON emp_office = office_id WHERE emp_user = $safLoginID AND emp_pass = $safPassword AND emp_is_ena = 1";
	  $resLogin = $this->db->query($sqlLogin);
	//  $resLogin = $this->db->query($sqlLogin) or die($sqlLogin);

	  if ($resLogin->num_rows == 1) { 

	    $myLogin = $resLogin->row;

	    $_SESSION['emp']   = $myLogin['emp_id'];
	    $_SESSION['user']  = $myLogin['emp_user'];
	    $_SESSION['email'] = $myLogin['emp_email'];
	    $_SESSION['office']= $myLogin['emp_office'];
	    $_SESSION['region']= $myLogin['office_region'];
	    $_SESSION['fname'] = ucfirst($myLogin['emp_fname']);
	    $_SESSION['lname'] = ucfirst($myLogin['emp_lname']);
	    $_SESSION['role']  = array();
	    $_SESSION['perm']  = array();
	    if ($myLogin['emp_days_pass'] > $maxDaysPass) { $_SESSION['altInfo'][] = "Your password is <b>" . $myLogin['emp_days_pass'] . " day" . ($myLogin['emp_days_pass'] == 1 ? "" : "s") . "</b> old.  [<a href='account.php?action=password'>Change it now</a>]"; }

	    $sqlPerm  = "SELECT * FROM emp_role JOIN role_list ON er_role_code = role_code";
	    $sqlPerm .= " LEFT JOIN role_perm ON er_role_code = rp_role_code";
	    $sqlPerm .= " WHERE er_emp = " . $_SESSION['emp'];
	    $resPerm  = $this->db->query($sqlPerm);

	    if ($resPerm->num_rows >=1) {
	      
	      $rows = $resPerm->rows;
	      foreach( $rows as $myPerm ) {
	        if (!in_array($myPerm['rp_role_code'], $_SESSION['role'])) $_SESSION['role'][] = $myPerm['rp_role_code'];
	        if (!in_array($myPerm['rp_perm_code'], $_SESSION['perm'])) $_SESSION['perm'][] = $myPerm['rp_perm_code'];

	      }
	    }

	    $inTwoMonths = 60 * 60 * 24 * 60 + time(); 
	    if(isset($_COOKIE['lastLoginDate']) && isset($_COOKIE['lastLoginTime'])) {
	      $_SESSION['last_login_date'] = $_COOKIE['lastLoginDate'];
	      $_SESSION['last_login_time'] = $_COOKIE['lastLoginTime'];
	    }

	    setcookie("lastLoginDate", date("m/d/y"), $inTwoMonths);
	    setcookie("lastLoginTime", date("G:i"), $inTwoMonths);

	    if ($fnRememberMe) {
	      setcookie("login_id", $fnLoginID, $inTwoMonths); 
	      setcookie("password", $fnPassword, $inTwoMonths); 
	    }

/* TODO later
	    $sqlLog  = "INSERT INTO log_login SET ";
	    $sqlLog .= "log_user = " . $this->prepareSaf($fnLoginID) . ", ";
	    $sqlLog .= "log_success = 1, ";
	    $sqlLog .= "log_ip_addr = $safIPAddr, ";
	    $sqlLog .= "log_ip_hex = $safIPHex, ";
	    $sqlLog .= "log_browser = $safBrowser";
	    $resLog = $this->db->query($sqlLog);
**/
	    session_write_close();
	    return true;

	  } else {
/**
	    $sqlLog  = "INSERT INTO log_login SET ";
	    $sqlLog .= "log_user = " . $this->prepareSaf($fnLoginID) . ", ";
	    $sqlLog .= "log_success = 0, ";
	    $sqlLog .= "log_ip_addr = $safIPAddr, ";
	    $sqlLog .= "log_ip_hex = $safIPHex, ";
	    $sqlLog .= "log_browser = $safBrowser";
	    $resLog = $this->db->query($sqlLog);
**/
	    return false;
	  }
	}

	function fnUpdOnline() {

	  
	  global $safIPHex;
	  global $safDTNow;

	  $safDTExp = escape_and_quote(formatDTmysql(strtotime('- 15 minutes')));

	  $safIPv4   = escape_and_quote($_SERVER["REMOTE_ADDR"]);

	  if (isset($_SESSION['emp'])) { if (is_numint($_SESSION['emp'])) { $valEmp = $_SESSION['emp']; } else { $valEmp = 0; } } else { $valEmp = 0; }

	  $sqlChkOnline = "SELECT * FROM online WHERE on_iphex = $safIPHex";
	  $resChkOnline = $this->db->query($sqlChkOnline);
	  $numChkOnline = $resChkOnline->num_rows;

	  if ($numChkOnline == 0) { updVisitorNum(); } // If this is a new visitor, update the statistic table.

	  $sqlUpdOnline = "INSERT INTO online VALUES ($safIPHex, $valEmp, $safIPv4, $safDTNow, $safDTNow)
	                   ON DUPLICATE KEY UPDATE on_last = $safDTNow";

	  $resUpdOnline = $this->db->query($sqlUpdOnline);

	  $sqlDelOnline = "DELETE FROM online WHERE on_last < $safDTExp";
	  $resDelOnline = $this->db->query($sqlDelOnline);

	}

	function fnShowRecordNav($query, $field, $table, $current, $key, $count, $arrIcons = NULL, $listPage = NULL) {

	  
	  global $PHP_SELF;

	  // First check whether the current record being viewed is one of the records in the current filter

	  $current = $this->prepareSaf($current); // added to make the record nav work when on an A-File

	  //Added table name to preceed field 2021-06-08 - SK
	  $sqlCheck   = "SELECT $table.$field FROM $table $query AND $table.$field = $current";
	  if ($resCheck   = $this->db->query($sqlCheck)) {
	    if ($resCheck->num_rows == 1) {

	      $showNavButtons = 1;

	      $sqlFirst    = "SELECT $table.$field FROM $table $query AND $table.$field < $current ORDER BY $table.$field ASC LIMIT 1";
	      $resFirst    = $this->db->query($sqlFirst);
	      if ($myFirst = $resFirst->rows) { $first = $myFirst[$field]; }

	      $sqlPrev     = "SELECT $table.$field FROM $table $query AND $table.$field < $current ORDER BY $table.$field DESC LIMIT 1";
	      $resPrev     = $this->db->query($sqlPrev);
	      if ($myPrev  = $resPrev->rows)  { $prev  = $myPrev[$field];  }

	      $sqlNext     = "SELECT $table.$field FROM $table $query AND $table.$field > $current ORDER BY $table.$field ASC LIMIT 1";
	      $resNext     = $this->db->query($sqlNext);
	      if ($myNext  = $resNext->rows)  { $next  = $myNext[$field];  }

	      $sqlLast     = "SELECT $table.$field FROM $table $query AND $table.$field > $current ORDER BY $table.$field DESC LIMIT 1";
	      $resLast     = $this->db->query($sqlLast);
	      if ($myLast  = $resLast->rows)  { $last  = $myLast[$field];  }

	      $sqlBelow    = "SELECT COUNT(*) AS count FROM $table $query AND $table.$field <= $current";
	      $myBelow     = $this->db->query($sqlBelow)->rows;
	      $numBelow    = $myBelow['count'];
	    }
	  }

	  echo "<table class='data'><tr><th style='width:60px;'>";
	  echo "<a href='" . (is_null($listPage) ? $PHP_SELF : $listPage) . "'><img class='icon16' alt='List' title='List' src='icons/list16.png' /></a>";

	  if (isset($showNavButtons)) {

	    if (isset($first))    { echo "<a href='$PHP_SELF?$key=$first'><img class='icon16' alt='First' title='First' src='icons/nav_first16.png' /></a>"; }
	                     else { echo "<img class='icon16' alt='Spacer' title='Spacer' src='icons/spacer.gif' />"; }
	    if (isset($prev))     { echo "<a href='$PHP_SELF?$key=$prev'><img class='icon16' alt='Previous' title='Previous' src='icons/nav_prev16.png' /></a>"; }
	                     else { echo "<img class='icon16' alt='Spacer' title='Spacer' src='icons/spacer.gif' />"; }

	    echo "</th><th style='width:140px;'>Record " . $numBelow . " of " . $count . "</th><th style='width:60px;'>";

	    if (isset($next))     { echo "<a href='$PHP_SELF?$key=$next'><img class='icon16' alt='Next' title='Next' src='icons/nav_next16.png' /></a>"; }
	                     else { echo "<img class='icon16' alt='Spacer' title='Spacer' src='icons/spacer.gif' />"; }
	    if (isset($last))     { echo "<a href='$PHP_SELF?$key=$last'><img class='icon16' alt='Last' title='Last' src='icons/nav_last16.png' /></a>"; }
	                     else { echo "<img class='icon16' alt='Spacer' title='Spacer' src='icons/spacer.gif' />"; }
	  }
	  echo "</th><th style='text-align:right;'>";

	  if (isset($arrIcons)) { foreach ($arrIcons AS $value) echo $value; }

	  echo "</th></tr></table>";
	}

	function fnShowSearch($basename="", $defDateClass="inp06") {

	  
	  global $webSearch;
	  global $webDFr;
	  global $webDTo;
	  global $filterSup;
	  global $filterOff;
	  global $filterReg;
	  global $filterPartner;
	  global $maxSearch;
	  global $maxDate;
	  global $lenTruncate1;

	  echo "<form method='post' name='frmSearch' action='searchPRG.php' autocomplete='off'>";

	  if (in_array($basename, array("job.php", "csr.php", "invoice.php", "call.php", "ldir.php", "crem.php", "market.php"))) {
	    $inpField = "inpSearch";
	    $defValue = (isset($_SESSION['inpValue'][$inpField]) ? $_SESSION['inpValue'][$inpField] : (isset($webSearch) ? $webSearch : ""));
	    $defClass = "inp12 " . (isset($_SESSION['errField']) ? (in_array($inpField, $_SESSION['errField']) ? "err" : "wht") : "wht");
	    echo "Keyword:<br /><input class='" . $defClass . "' type='text' name='$inpField' id='$inpField' value='$defValue' maxlength='$maxSearch' />";
	  }

	  if (in_array($basename, array("inventory.php"))) {
	    global $yrToday;
	    global $noToday;
	    global $arrMo;
	    global $filterOffice;

	    echo "Month/Year:<br />";

	    $inpField = "filMo";
	    $inpLabel = "Month";
	    $defValue = (isset($_SESSION[$basename][$inpField]) ? $_SESSION[$basename][$inpField] : "");
	    $defClass = "inp06 " . (isset($_SESSION['errField']) ? (in_array($inpField, $_SESSION['errField']) ? "err" : "wht") : "wht");
	    echo "<select class='$defClass' name='$inpField'>";
	    echo "<option value='' selected='selected'>--All--</option>";
	    for ($i=1; $i<=12; $i++) {
	      echo "<option value='" . $i . "'";
	      if ($i == $defValue) echo " selected";
	      echo ">" . $arrMo[$i] . "</option>"; }
	    echo "</select>";

	    $inpField = "filYr";
	    $inpLabel = "Year";
	    $defValue = (isset($_SESSION[$basename][$inpField]) ? $_SESSION[$basename][$inpField] : "");
	    $defClass = "inp06 " . (isset($_SESSION['errField']) ? (in_array($inpField, $_SESSION['errField']) ? "err" : "wht") : "wht");
	    echo "<select class='$defClass' name='$inpField'>";
	    echo "<option value='' selected='selected'>--All--</option>";
	    for ($yr=$yrToday-1; $yr<=$yrToday+1; $yr++) {
	      echo "<option value='" . $yr . "'";
	      if ($yr == $defValue) echo " selected";
	      echo ">" . $yr . "</option>"; }
	    echo "</select>";
	  }

	  if (in_array($basename, array("inventory.php", "market.php"))) {
	    echo "Office:<br />";

	    $sqlSelOffice = "SELECT * FROM office WHERE office_is_act = 1 AND office_stock_level IS NOT NULL ORDER BY office_city";
	    $resSelOffice = $this->db->query($sqlSelOffice);

	    $inpField = "filOffice";
	    $inpLabel = "Office";
	    $defValue = (isset($_SESSION[$basename][$inpField]) ? $_SESSION[$basename][$inpField] : "");
	    $defClass = "inp12 " . (isset($_SESSION['errField']) ? (in_array($inpField, $_SESSION['errField']) ? "err" : "wht") : "wht");
	    echo "<select class='$defClass' name='$inpField'>";
	    echo "<option value='' selected='selected'>--All--</option>";
	    while ($mySelOffice = $resSelOffice->rows) {
	      echo "<option value='" . $mySelOffice['office_id'] . "'";
	      if ($mySelOffice['office_id'] == $defValue) { echo " selected='selected'"; }
	      echo ">" . prepareWeb($mySelOffice['office_name']) . "</option>"; }
	    echo "</select>";
	  }

	  if (in_array($basename, array("job.php", "csr.php", "invoice.php", "cash.php", "promise.php", "call.php", "lgen.php", "ldir.php", "lead.php"))) {
	    echo "Date (From-To):<br />";

	    $inpField = "inpDFr";
	    $defValue = (isset($_SESSION['inpValue'][$inpField]) ? $_SESSION['inpValue'][$inpField] : (isset($webDFr) ? $webDFr : ""));
	    $defClass = "$defDateClass " . (isset($_SESSION['errField']) ? (in_array($inpField, $_SESSION['errField']) ? "err" : "wht") : "wht");
	    echo "<input class='" . $defClass . "' type='text' name='$inpField' id='$inpField' value='$defValue' maxlength='$maxDate' />";
	    echo "<script>$(function() { $('#".  $inpField . "').datepicker(); }); </script>";

	    $inpField = "inpDTo";
	    $defValue = (isset($_SESSION['inpValue'][$inpField]) ? $_SESSION['inpValue'][$inpField] : (isset($webDTo) ? $webDTo : ""));
	    $defClass = "$defDateClass " . (isset($_SESSION['errField']) ? (in_array($inpField, $_SESSION['errField']) ? "err" : "wht") : "wht");
	    echo "<input class='" . $defClass . "' type='text' name='$inpField' id='$inpField' value='$defValue' maxlength='$maxDate' />";
	    echo "<script>$(function() { $('#".  $inpField . "').datepicker(); }); </script>";
	  }

	  if (in_array($basename, array("job.php", "market.php"))) {
	    echo "Supervisor:<br />";

	    $sqlSelSup = "SELECT emp_id, emp_is_act, emp_fname, emp_lname FROM emp JOIN emp_role ON emp_id = er_emp JOIN role_list ON er_role_code = role_code WHERE role_code IN ('SUP', 'BAC') ORDER BY emp_is_act DESC, emp_fname, emp_lname";
	    $resSelSup = $this->db->query($sqlSelSup);

	    $arrSelSup = array('ACTIVE' => array(), 'INACTIVE' => array());
	    while ($res = $resSelSup->rows) {
	      $label = $res['emp_is_act'] == 1 ? 'ACTIVE' : 'INACTIVE';
	      array_push($arrSelSup[$label], $res);
	    }

	    $inpField = "selSup";
	    $defValue = isset($filterSup) ? $filterSup : "";
	    $defClass = "inp12";

	    echo "<select class='$defClass' name='$inpField'>";
	    echo "<option value='0' selected='selected'>--All--</option>";
	    echo "<optgroup label='ME'>";
	    echo "<option value='" . $_SESSION['emp'] . "'";
	    if ($_SESSION['emp'] == $defValue) { echo " selected='selected'"; }
	    echo ">" . substr($_SESSION['fname'], 0, 1) . "." . $_SESSION['lname'] . "</option>";
	    echo "</optgroup>";

	    foreach($arrSelSup AS $label => $arrSup) {
	      if (sizeof($arrSup)) {
	        echo "<optgroup label='$label'" . ($label == "INACTIVE" ? "  style='background-color:#ccc;'" : "") . ">";
	        foreach($arrSup AS $mySup) {
	          $webName = prepareWeb(truncate(substr($mySup['emp_fname'],0,1) . "." . $mySup['emp_lname'], $lenTruncate1));
	          echo "<option value='" . $mySup['emp_id'] . "'";
	          if ($mySup['emp_id'] == $defValue) { echo " selected='selected'"; }
	          if ($mySup['emp_is_act'] == 0) echo " style='background-color:#ccc;'";
	          echo ">" . $webName . "</option>";
	        }
	      echo "</optgroup>";
	      }
	    }

	    echo "</select>";
	  }

	  if (in_array($basename, array("job.php", "opportunity.php"))) {
	    echo "Office:<br />";

	    $sqlSelOff = "SELECT office_id, office_name, office_is_act FROM office WHERE office_is_field = 1 ORDER BY office_is_act DESC, office_name";
	    $resSelOff = $this->db->query($sqlSelOff);

	    $arrSelOff = array('ACTIVE' => array(), 'INACTIVE' => array());
	    while ($res = $resSelOff->rows) {
	      $label = $res['office_is_act'] == 1 ? 'ACTIVE' : 'INACTIVE';
	      array_push($arrSelOff[$label], $res);
	    }

	    $inpField = "selOff";
	    $defValue = isset($filterOff) ? $filterOff : "";
	    $defClass = "inp12";

	    echo "<select class='$defClass' name='$inpField'>";
	    echo "<option value='0' selected='selected'>--All--</option>";

	    foreach($arrSelOff AS $label => $arrOff) {
	      if (sizeof($arrOff)) {
	        echo "<optgroup label='$label'>";
	        foreach($arrOff AS $myOff) {
	          $webName = prepareWeb($myOff['office_name']);
	          echo "<option value='" . $myOff['office_id'] . "'";
	          if ($myOff['office_id'] == $defValue) { echo " selected='selected'"; }
	          if ($myOff['office_is_act'] == 0) echo " style='background-color:#ccc;'";
	          echo ">" . $webName . "</option>";
	        }
	        echo "</optgroup>";
	      }
	    }

	    echo "</select>";
	  }

	  if (in_array($basename, array("job.php", "inventory.php", "opportunity.php"))) {
	    echo "Region:<br />";

	    $sqlSelReg = "SELECT region_id, region_name, region_is_act FROM region WHERE region_is_field = 1 ORDER BY region_is_act DESC, region_name";
	    $resSelReg = $this->db->query($sqlSelReg);

	    $arrSelReg = array('ACTIVE' => array(), 'INACTIVE' => array());
	    while ($res = $resSelReg->rows) {
	      $label = $res['region_is_act'] == 1 ? 'ACTIVE' : 'INACTIVE';
	      array_push($arrSelReg[$label], $res);
	    }

	    $inpField = "selReg";
	    $defValue = isset($filterReg) ? $filterReg : "";
	    $defClass = "inp12";

	    echo "<select class='$defClass' name='$inpField'>";
	    echo "<option value='0' selected='selected'>--All--</option>";

	    foreach($arrSelReg AS $label => $arrReg) {
	      if (sizeof($arrReg)) {
	        echo "<optgroup label='$label'>";
	        foreach($arrReg AS $myReg) {
	          $webName = prepareWeb($myReg['region_name']);
	          echo "<option value='" . $myReg['region_id'] . "'";
	          if ($myReg['region_id'] == $defValue) { echo " selected='selected'"; }
	          if ($myReg['region_is_act'] == 0) echo " style='background-color:#ccc;'";
	          echo ">" . $webName . "</option>";
	        }
	        echo "</optgroup>";
	      }
	    }

	    echo "</select>";
	  }

	  if (in_array($basename, array("job.php"))) {
	    echo "Partner Pricing:<br />";

	    $sql = "SELECT partner_id, partner_dsca FROM partner WHERE 1 ORDER BY partner_dsca";
	    $res = $this->db->query($sql);

	    $inpField = "selPartner";
	    $defValue = isset($filterPartner) ? $filterPartner : "";
	    $defClass = "inp12";

	    echo "<select class='$defClass' name='$inpField'>";
	    echo "<option value='0' selected='selected'>--All--</option>";

	    while ($row = $res->rows) {

	      $webDsca = prepareWeb($row['partner_dsca']);
	      echo "<option value='" . $row['partner_id'] . "'";
	      if ($row['partner_id'] == $defValue) { echo " selected='selected'"; }
	      echo ">" . $webDsca . "</option>";

	    }
	    echo "</select>";
	  }

	  echo "<input class='inp06' type='submit' name='subGoSearch' value='Search' />";
	  echo "<input class='inp06' type='submit' name='subClearSearch' value='Clear' />";

	  echo "</form>";
	}

	function fnShowRPP($curRPP) {

	  $arrRPP = array(20,40,60,100,200,400,800);

	  echo "<form method='post' name='frmRPP' action='RPPPRG.php'>";

	  $inpField = "selRPP";
	  echo "Rows Per Page:<br />";
	  echo "<select class='inp06' name='$inpField'>";
	  foreach ($arrRPP AS $value) {
	    echo "<option value='$value'";
	    if ($value == $curRPP) { echo " selected='selected'"; }
	    echo ">" . $value . "</option>";
	  }
	  echo "</select>";
	  echo "<input class='inp06' type='submit' name='subGoRPP' value='Set' />";

	  echo "</form>";
	}

	function radTeamBoxes($state, $zipTo) {

	  
	  global $PHP_SELF;
	  global $strURL;
	  global $valTeam;
	  global $lenTruncate2;
	  global $safDTNow;
	  $safState = $this->prepareSaf($state);
	  $strTeam = "";

	  $sqlJobLoc = "SELECT * FROM zip WHERE zip_code = '$zipTo'";
	  if ($resJobLoc = $this->db->query($sqlJobLoc)) {
	    if ($resJobLoc->num_rows == 1) {
	      $myJobLoc = $resJobLoc->rows;
	      $safJobLat = $this->prepareSaf($myJobLoc['zip_lat']);
	      $safJobLon = $this->prepareSaf($myJobLoc['zip_lon']);
	    }
	  }

	  $sqlMatrix = "SELECT *,";
	  $sqlMatrix .= " CEIL((3959 * acos(cos(radians($safJobLat) ) * cos( radians( zip_lat ) ) * cos( radians(zip_lon) - radians($safJobLon)) + sin(radians($safJobLat)) * sin( radians(zip_lat)))) + (-1 * office_cov_adj_mi)) AS adjdist,"; 
	  $sqlMatrix .= " CEIL(3959 * acos(cos(radians($safJobLat) ) * cos( radians( zip_lat ) ) * cos( radians(zip_lon) - radians($safJobLon)) + sin(radians($safJobLat)) * sin( radians(zip_lat)))) AS zipdist,"; 
	  $sqlMatrix .= " CEIL(3959 * acos(cos(radians($safJobLat) ) * cos( radians( geo_lat ) ) * cos( radians(geo_lon) - radians($safJobLon)) + sin(radians($safJobLat)) * sin( radians(geo_lat)))) AS geodist"; 
	  $sqlMatrix .= " FROM office_state_matrix";
	  $sqlMatrix .= " JOIN team ON osm_office = team_office";
	  $sqlMatrix .= " JOIN office ON team_office = office_id";
	  $sqlMatrix .= " JOIN region ON office_region = region_id";
	  $sqlMatrix .= " JOIN truck ON team_truck = truck_id";
	  $sqlMatrix .= " JOIN truck_status ON truck_status_id = truck_status";
	  $sqlMatrix .= " LEFT JOIN geotab ON truck_geo = geo_id";
	  $sqlMatrix .= " JOIN zip ON office_zip = zip_code";
	  $sqlMatrix .= " WHERE team_is_act = 1 AND osm_state = $safState";
	  $sqlMatrix .= " ORDER BY adjdist, office_city";

	  if ($resMatrix = $this->db->query($sqlMatrix)) {

	    if ($resMatrix->num_rows > 0) {

	      while ($myMatrix = $resMatrix->rows) {
	        $webColor1       = $myMatrix['region_color1'];
	        $webColor2       = $myMatrix['region_color2'];
	        $webOfficeName   = prepareWeb($myMatrix['office_name']);
	        $webOfficeCity   = prepareWeb($myMatrix['office_city']);
	        $webOfficeState  = prepareWeb($myMatrix['office_state']);
	        $webOfficeZip    = prepareWeb($myMatrix['office_zip']);
	        $webTeamInfo     = prepareWeb($myMatrix['team_info']);
	        $webOfficeInfo   = prepareWeb($myMatrix['office_info']);
	        $webTruckInfo    = prepareWeb($myMatrix['truck_info']);
	        $webTruckStatus  = prepareWeb($myMatrix['truck_status']);
	        $webTruckDsca    = prepareWeb($myMatrix['truck_status_dsca']);
	        $valTruck        = $myMatrix['truck_id'];
	        $valCovAdjMi     = $myMatrix['office_cov_adj_mi'];
	        //$valDistAdj      = $myMatrix['adjdist'];
	        $valDistZip      = $myMatrix['zipdist'];
	        $valDistGeo      = $myMatrix['geodist'];
	        $webLatTruck     = $myMatrix['geo_lat'];
	        $webLonTruck     = $myMatrix['geo_lon'];

	        $inpField = "radTeam";
	        $defValue = (isset($_SESSION['inpValue'][$inpField]) ? $_SESSION['inpValue'][$inpField] : (isset($valTeam) ? $valTeam : ""));
	        $defClass = "title " . (isset($_SESSION['errField']) ? (in_array($inpField, $_SESSION['errField']) ? "#fc6" : $webColor2) : $webColor2);

	        $strTeam .= "\n<br /><table class='team'>";

	        $strTeam .= "\n<tr class='title'><td colspan='3' style='background-color:#" . (isset($_SESSION['errField']) ? (in_array($inpField, $_SESSION['errField']) ? "f66" : $webColor2) : $webColor2) . ";'>";
	        $strTeam .= "\n<span style='float:left;margin-top:2px;'><b>$webOfficeName</b></span>";
	        $strTeam .= "\n<span style='float:right;'><input type='radio' name='$inpField' id='$inpField" . $myMatrix['team_id'] . "' value='" . $myMatrix['team_id'] . "'" . (($myMatrix['team_id'] == $defValue) ? " checked='checked'" : "") . " /></span>";
	        $strTeam .= "\n<span style='float:right;'><a href='team.php?team=" . $myMatrix['team_id'] . "&action=edit'><img class='icon16' src='icons/edit16.png' title='Edit Team'/></a></span>";
	        $strTeam .= "\n<span style='float:right;margin-top:2px;' title='Truck ($webTruckDsca)'>Truck $valTruck  &nbsp;</span>";
	        $strTeam .= "</td></tr>";

	        $sqlTeamEmp  = "SELECT *, DATEDIFF($safDTNow, emp_dt_hire) AS emp_days FROM team_emp";
	        $sqlTeamEmp .= " JOIN team ON te_team = team_id";
	        $sqlTeamEmp .= " JOIN emp ON te_emp = emp_id";
	        $sqlTeamEmp .= " JOIN team_status ON team_status = team_status_id";
	        $sqlTeamEmp .= " JOIN title ON emp_title = title_id";
	        $sqlTeamEmp .= " WHERE te_team = " . $myMatrix['team_id'];
	        $sqlTeamEmp .= " ORDER BY te_is_sup DESC, title_id, emp_fname";
	        if ($resTeamEmp = $this->db->query($sqlTeamEmp)) {
	          if ($resTeamEmp->num_rows > 0) {
	            while ($myTeamEmp = $resTeamEmp->rows) {

	              $valIsSup      = $myTeamEmp['te_is_sup'];
	              $valEmp        = $myTeamEmp['te_emp'];
	              $valIsTrn      = $myTeamEmp['emp_is_trn'];
	              $valDays       = $myTeamEmp['emp_days'];
	              $webTitleShort = prepareWeb($myTeamEmp['title_short']);
	              $webEmpFName   = prepareWeb($myTeamEmp['emp_fname']);
	              $webEmpLName   = prepareWeb($myTeamEmp['emp_lname']);
	              $webEmpTelc    = formatPhone($myTeamEmp['emp_telc']);
	              $webColor      = prepareWeb($myTeamEmp['team_status_color']);

	              $strName = (in_array('MASTER', $_SESSION['perm']) ? "<a href='employee.php?emp=$valEmp'>$webEmpFName $webEmpLName</a>" : "$webEmpFName $webEmpLName");

	              if ($valIsTrn) { $strName = "(" . $strName . ")"; }
	              if ($valDays < 30) { $strName = "<i>" . $strName . "</i>"; }

	              if ($valIsSup) {
	                $sqlLastAppt = "SELECT * FROM job WHERE job_emp_sup = $valEmp ORDER BY job_dt_appt DESC LIMIT 0,1";
	                $resLastAppt = $this->db->query($sqlLastAppt);
	                if ($resLastAppt->num_rows == 1) {
	                  $myLastAppt = $resLastAppt->rows;
	                  $webDTLastAppt = formatDTText(strtotime($myLastAppt['job_dt_appt']));
	                } else {
	                  $webDTLastAppt = "Never";
	                }
	              }

	              $strTeam .= "\n<tr class='content" . ($webTitleShort == "R"? " regional" : "") . "' style='background-color:#$webColor;'><td>$webTitleShort:</td><td" . ($valIsSup == 1 ? " style='font-weight:bold;'" : "") . ">";
	              $strTeam .= $strName;
	              $strTeam .= "\n</td><td class='tel'>$webEmpTelc</td></tr>";
	              if ($valIsSup) $strTeam .= "\n<tr class='content' style='background-color:#$webColor;'><td>&nbsp;</td><td colspan='2'>Last Appt: $webDTLastAppt</td></tr>";
	            }
	          }
	        }

	        $strTeam .= "\n<tr class='heading'><td colspan='3'>";
	        $strTeam .= "\n<span style='float:left;margin-top:2px;'>Office is <b>$valDistZip miles</b> from property</span>";
	        $strTeam .= "\n<span style='float:right;'><a href='https://www.google.com/maps/dir/$webOfficeZip/$zipTo' target='map'><img class='icon16' src='icons/map16.png' title='Directions'/></a></span>";
	        $strTeam .= "<div style='clear:both;'></div>";

	        if ($valCovAdjMi != 0) {
	          $strTeam .= "\n<div class='notebox " . ($valCovAdjMi < 0 ? "notebox-neg" : "notebox-pos") . "'><span><img class='icon16' src='icons/question16.png' alt='Help' title='The office range " . ($valCovAdjMi > 0 ? 'increased' : 'reduced') . " by " . abs($valCovAdjMi) . " miles.'></span><span>A coverage adjustment has " . ($valCovAdjMi < 0 ? "de" : "") . "prioritized this office.</span></div>";
	        }

	        if (is_null($webLatTruck)) {
	          $strTeam .= "\n<span style='float:left;margin-top:2px;'>Truck position is unknown.</span>";
	        } else {
	          $strTeam .= "\n<span style='float:left;margin-top:2px;'>Truck is <b>$valDistGeo miles</b> from property</span>";
	          $strTeam .= "\n<span style='float:right;'><a href='https://www.google.com/maps/dir/$webLatTruck+$webLonTruck/$zipTo' target='map'><img class='icon16' src='" . $strURL . "/icons/map16.png' title='Directions'/></a></span>";
	//          $strTeam .= "\n<span style='float:right;'><a href='https://www.google.com/maps?t=m&q=loc:$webLatTruck+$webLonTruck' target='map'><img class='icon16' src='" . $strURL . "/icons/map16.png' title='Directions'/></a></span>";
	        }
	        $strTeam .= "</td></tr>";

	        $sqlSchedule  = "SELECT *,";
	        $sqlSchedule .= " CEIL(3959 * acos(cos(radians($safJobLat) ) * cos( radians( zip_lat ) ) * cos( radians(zip_lon) - radians($safJobLon)) + sin(radians($safJobLat)) * sin( radians(zip_lat)))) AS dist"; 
	        $sqlSchedule .= " FROM job";
	        $sqlSchedule .= " JOIN job_status ON job_status = job_status_id";
	        $sqlSchedule .= " JOIN zip ON job_prop_zip = zip_code";
	        $sqlSchedule .= " WHERE job_team = " . $myMatrix['team_id'] . " AND job_status IN (3,4)";

	//        $_SESSION['msgInfo'] = $sqlSchedule;

	        if ($resSchedule = $this->db->query($sqlSchedule)) { 
	          if ($resSchedule->num_rows > 0) {
	            while ($mySchedule = $resSchedule->rows) {
	              $valJob        = $mySchedule['job_id'];
	              $valStatus     = $mySchedule['job_status'];
	              $webStatusCode = prepareWeb($mySchedule['job_status_code']);
	              $valYrNum      = $mySchedule['job_yrnum'];
	              $valDist       = $mySchedule['dist'];
	              $webSchedZip   = $mySchedule['job_prop_zip'];

	              switch ($valStatus) {
	              case 3:
	                $webDTSched = formatDTText(strtotime($mySchedule['job_dt_appt']));
	                $webDOW     = date("D", strtotime($mySchedule['job_dt_appt']));
	              break;
	              case 4:
	                $webDTSched = formatDTText(strtotime($mySchedule['job_dt_start']));
	                $webDOW     = date("D", strtotime($mySchedule['job_dt_start']));
	              break;
	              }

	              if (in_array($valStatus, array(3,4))) {
	                $strTeam .= "\n<tr class='heading'><td colspan='3' class='highlight'>";
	                $strTeam .= "\n<span style='float:left;margin-top:2px;'><a href='job.php?job=$valJob'>$webStatusCode: $webDOW $webDTSched (<b>$valDist mi</b>)</a></span>";
	                $strTeam .= "\n<span style='float:right;'><a href='https://www.google.com/maps/dir/$webSchedZip/$zipTo' target='map'><img class='icon16' src='icons/map16.png' title='Directions'/></a></span>";
	                $strTeam .= "</td></tr>";
	              }
	            }
	          } else {
	            $strTeam .= "\n<tr class='heading'><td colspan='3' class='highlight'>Nothing scheduled.</td></tr>";
	          }
	        }

	        if (!empty($webTeamInfo))  { $strTeam .= "<tr class='heading'><td colspan='3' class='alert'>$webTeamInfo</td></tr>"; }

	        if (!empty($webTruckInfo))  { $strTeam .= "<tr class='heading'><td colspan='3' class='alert'>Truck $valTruck: $webTruckInfo</td></tr>"; }

	        if ($webTruckStatus!='3')  { $strTeam .= "<tr class='heading'><td colspan='3'>Truck $valTruck status: <b>$webTruckDsca </b></td></tr>"; }

	        if (!empty($webOfficeInfo)) { $strTeam .= "<tr class='heading'><td colspan='3' class='alert'>$webOfficeInfo</td></tr>"; }

	        $strTeam .= "</table>";
	      } // while $myMatrix
	    }
	  }
	  return (isset($strTeam) ? $strTeam : FALSE);
	}

	function fnContactBox($contact) {

	  
	  global $valJobID;

	  $sqlContactBox = "SELECT * FROM contact WHERE con_id = $contact";
	  if ($resContactBox = $this->db->query($sqlContactBox)) {
	    if ($resContactBox->num_rows == 1) {

	      $myContactBox = $resContactBox->rows;

	      $webFName        = prepareWeb($myContactBox['con_fname']);
	      $webLName        = prepareWeb($myContactBox['con_lname']);
	      $webEmail        = prepareWeb($myContactBox['con_email']);
	      $webTelp         = formatPhone($myContactBox['con_telp']);
	      $webTelc         = formatPhone($myContactBox['con_telc']);
	      $webTelf         = formatPhone($myContactBox['con_telf']);

	      if (!is_null($myContactBox['con_telpx'])) $webTelpx        = prepareWeb($myContactBox['con_telpx']);
	      if (!is_null($myContactBox['con_info']))  $webInfo         = prepareWeb($myContactBox['con_info']);

	      echo "<table class='team' style='table-layout:fixed;'>";
	      echo "<col width='75px'>";
	      echo "<col width='200px'>";
	      echo "<tr class='heading gray'><td colspan='2'>";
	      echo "<span style='float:left;margin-top:2px;'><b>$webFName $webLName</b></span>";
	      echo "<span style='float:right;'><a href='contact.php?contact=$contact&job=$valJobID'><img class='icon16' src='icons/remove16.png' title='Remove Contact'/></a></span>";
	      echo "<span style='float:right;'><a href='contact.php?contact=$contact&action=edit'><img class='icon16' src='icons/edit16.png' title='Edit Contact'/></a></span>";
	      if (!empty($webEmail)) echo "<span style='float:right;'><a href='mailto:$webEmail'><img class='icon16' src='icons/email16.png' title='Email Contact'/></a></span>";
	      echo "</td></tr>";

	      echo "\n<tr class='content'><td>Email:</td><td class='tel'>$webEmail</td></tr>";
	      echo "\n<tr class='content'><td>Phone:</td><td class='tel'>$webTelp</td></tr>";
	      if (isset($webTelpx)) echo "\n<tr class='content'><td>Ext:</td><td class='tel'>$webTelpx</td></tr>";
	      echo "\n<tr class='content'><td>Cell:</td><td class='tel'>$webTelc</td></tr>";
	      echo "\n<tr class='content'><td>Fax:</td><td class='tel'>$webTelf</td></tr>";
	      if (isset($webInfo)) echo "\n<tr class='heading gray'><td colspan='2'>$webInfo</td></tr>";

	      echo "</table>";
	    }
	  }
	}

	function teamBox($team, $zipTo) {

	  
	  global $PHP_SELF;
	  global $strURL;
	  global $lenTruncate2;
	  global $safDTNow;

	  $sqlJobLoc = "SELECT * FROM zip WHERE zip_code = '$zipTo'";
	  if ($resJobLoc = $this->db->query($sqlJobLoc)) {
	    if ($resJobLoc->num_rows == 1) {
	      $myJobLoc = $resJobLoc->rows;
	      $safJobLat = $this->prepareSaf($myJobLoc['zip_lat']);
	      $safJobLon = $this->prepareSaf($myJobLoc['zip_lon']);
	    }
	  }

	  $sqlTeamBox = "SELECT *";
	  $sqlTeamBox .= ", CEIL(3959 * acos(cos(radians($safJobLat) ) * cos( radians( zip_lat ) ) * cos( radians(zip_lon) - radians($safJobLon)) + sin(radians($safJobLat)) * sin( radians(zip_lat)))) AS zipdist"; 
	  $sqlTeamBox .= ", CEIL(3959 * acos(cos(radians($safJobLat) ) * cos( radians( geo_lat ) ) * cos( radians(geo_lon) - radians($safJobLon)) + sin(radians($safJobLat)) * sin( radians(geo_lat)))) AS geodist"; 
	  $sqlTeamBox .= ", (SELECT COUNT(*) FROM res_partner_office WHERE rpo_office = team_office) AS count_rp";
	  $sqlTeamBox .= " FROM team";
	  $sqlTeamBox .= " JOIN office ON team_office = office_id";
	  $sqlTeamBox .= " JOIN truck ON team_truck = truck_id";
	  $sqlTeamBox .= " JOIN truck_status ON truck_status_id = truck_status";
	  $sqlTeamBox .= " LEFT JOIN geotab ON truck_geo = geo_id";
	  $sqlTeamBox .= " JOIN region ON office_region = region_id";
	  $sqlTeamBox .= " JOIN zip ON office_zip = zip_code";
	  $sqlTeamBox .= " WHERE team_id = $team";

	  if ($resTeamBox = $this->db->query($sqlTeamBox)) {

	    if ($resTeamBox->num_rows == 1) {

	              $myTeamBox = $resTeamBox->rows;

	              $webColor2       = $myTeamBox['region_color2'];
	              $webOfficeName   = prepareWeb($myTeamBox['office_name']);
	              $webOfficeCity   = prepareWeb($myTeamBox['office_city']);
	              $webOfficeState  = prepareWeb($myTeamBox['office_state']);
	              $webOfficeZip    = prepareWeb($myTeamBox['office_zip']);
	              $valOffice       = $myTeamBox['team_office'];
	              $valTruck        = $myTeamBox['team_truck'];
	              $valDistZip      = $myTeamBox['zipdist'];
	              $valDistGeo      = $myTeamBox['geodist'];
	              $webLatTruck     = $myTeamBox['geo_lat'];
	              $webLonTruck     = $myTeamBox['geo_lon'];
	              $webTeamInfo     = prepareWeb($myTeamBox['team_info']);
	              $webOfficeInfo   = prepareWeb($myTeamBox['office_info']);
	              $webTruckInfo    = prepareWeb($myTeamBox['truck_info']);
	              $webTruckStatus  = prepareWeb($myTeamBox['truck_status']);
	              $webTruckDsca    = prepareWeb($myTeamBox['truck_status_dsca']);

	              $strTeam = "\n<br /><table class='team'>";
	              $strTeam .= "\n<tr class='title'><td colspan='3' style='background-color:#" . $webColor2 . ";'>";
	              $strTeam .= "\n<span style='float:left;margin-top:2px;'>";
	              $strTeam .= "\n";
	              $strTeam .= "<b><a href='office.php?office=$valOffice'>$webOfficeName</a></b></span>";
	              $strTeam .= "\n<span style='float:right;margin-top:2px;'><a href='truck.php?truck=$valTruck' title='Truck $valTruck ($webTruckDsca)'>Truck $valTruck</a>&nbsp;</span>";
	              $strTeam .= "</td></tr>";

	              $sqlTeamEmp  = "SELECT *, DATEDIFF($safDTNow, emp_dt_hire) AS emp_days FROM team_emp";
	              $sqlTeamEmp .= " JOIN emp ON te_emp = emp_id";
	              $sqlTeamEmp .= " JOIN team ON te_team = team_id";
	              $sqlTeamEmp .= " JOIN team_status ON team_status = team_status_id";
	              $sqlTeamEmp .= " JOIN title ON emp_title = title_id";
	              $sqlTeamEmp .= " WHERE te_team = $team";
	              $sqlTeamEmp .= " ORDER BY te_is_sup DESC, title_id, emp_fname";
	              if ($resTeamEmp = $this->db->query($sqlTeamEmp)) {
	                if ($resTeamEmp->num_rows > 0) {
	                  while ($myTeamEmp = $resTeamEmp->rows) {

	                    $valIsSup      = $myTeamEmp['te_is_sup'];
	                    $valEmp        = $myTeamEmp['te_emp'];
	                    $valIsTrn      = $myTeamEmp['emp_is_trn'];
	                    $valDays       = $myTeamEmp['emp_days'];
	                    $webTitleShort = prepareWeb($myTeamEmp['title_short']);
	                    $webEmpFName   = prepareWeb($myTeamEmp['emp_fname']);
	                    $webEmpLName   = prepareWeb($myTeamEmp['emp_lname']);
	                    $webEmpTelc    = formatPhone($myTeamEmp['emp_telc']);
	                    $webColor      = prepareWeb($myTeamEmp['team_status_color']);

	                    $strName = (in_array('MASTER', $_SESSION['perm']) ? "<a href='employee.php?emp=$valEmp'>$webEmpFName $webEmpLName</a>" : "$webEmpFName $webEmpLName");

	                    if ($valIsTrn) { $strName = "(" . $strName . ")"; }
	                    if ($valDays < 30) { $strName = "<i>" . $strName . "</i>"; }

	                    $strTeam .= "\n<tr class='content" . ($webTitleShort == "R"? " regional" : "") . "' style='background-color:#$webColor;'><td>$webTitleShort:</td><td" . ($valIsSup == 1 ? " style='font-weight:bold;'" : "") . ">";
	                    $strTeam .= $strName;
	                    $strTeam .= "\n</td><td class='tel'>$webEmpTelc</td></tr>";
	                  }
	                }
	              }

	              $strTeam .= "\n<tr class='heading'><td colspan='3'>";
	              $strTeam .= "\n<span style='float:left;margin-top:2px;'>Office is <b>$valDistZip miles</b> from property</span>";
	              $strTeam .= "\n<span style='float:right;'><a href='https://www.google.com/maps/dir/$webOfficeZip/$zipTo' target='map'><img class='icon16' src='" . $strURL . "/icons/map16.png' title='Directions'/></a></span>";
	              $strTeam .= "<div style='clear:both;'></div>";
	              if (is_null($webLatTruck)) {
	                $strTeam .= "\n<span style='float:left;margin-top:2px;'>Truck position is unknown.</span>";
	              } else {
	                $strTeam .= "\n<span style='float:left;margin-top:2px;'>Truck is <b>$valDistGeo miles</b> from property</span>";
	                $strTeam .= "\n<span style='float:right;'><a href='https://www.google.com/maps/dir/$webLatTruck+$webLonTruck/$zipTo' target='map'><img class='icon16' src='" . $strURL . "/icons/map16.png' title='Directions'/></a></span>";
	              }
	              $strTeam .= "</td></tr>";

	              $sqlSchedule  = "SELECT *,";
	              $sqlSchedule .= " CEIL(3959 * acos(cos(radians($safJobLat) ) * cos( radians( zip_lat ) ) * cos( radians(zip_lon) - radians($safJobLon)) + sin(radians($safJobLat)) * sin( radians(zip_lat)))) AS dist"; 
	              $sqlSchedule .= " FROM job";
	              $sqlSchedule .= " JOIN job_status ON job_status = job_status_id";
	              $sqlSchedule .= " JOIN zip ON job_prop_zip = zip_code";
	              $sqlSchedule .= " WHERE job_team = $team AND job_status IN (3,4)";

	              if ($resSchedule = $this->db->query($sqlSchedule)) { 
	                if ($resSchedule->num_rows > 0) {
	                  while ($mySchedule = $resSchedule->rows) {
	                    $valJob        = $mySchedule['job_id'];
	                    $valStatus     = $mySchedule['job_status'];
	                    $webStatusCode = prepareWeb($mySchedule['job_status_code']);
	                    $valYrNum      = $mySchedule['job_yrnum'];
	                    $valDist       = $mySchedule['dist'];
	                    $webSchedZip   = $mySchedule['job_prop_zip'];

	                    switch ($valStatus) {
	                    case 3:
	                      $webDTSched = formatDTText(strtotime($mySchedule['job_dt_appt']));
	                      $webDOW     = date("D", strtotime($mySchedule['job_dt_appt']));
	                    break;
	                    case 4:
	                      $webDTSched = formatDTText(strtotime($mySchedule['job_dt_start']));
	                      $webDOW     = date("D", strtotime($mySchedule['job_dt_start']));
	                    break;
	                    }

	                    $strTeam .= "\n<tr class='heading'><td colspan='3'>";
	                    $strTeam .= "\n<span style='float:left;margin-top:2px;'><a href='job.php?job=$valJob'>$webStatusCode: $webDOW $webDTSched (<b>$valDist mi</b>)</a></span>";
	                    $strTeam .= "\n<span style='float:right;'><a href='https://www.google.com/maps/dir/$webSchedZip/$zipTo' target='map'><img class='icon16' src='" . $strURL . "/icons/map16.png' title='Directions'/></a></span>";
	                    $strTeam .= "</td></tr>";

	                  }
	                } else {
	                  $strTeam .= "\n<tr class='heading'><td colspan='3' class='highlight'>Nothing scheduled.</td></tr>";
	                }
	              }
	              if (!empty($webTeamInfo))   { $strTeam .= "<tr class='heading'><td colspan='3' class='alert'>$webTeamInfo</td></tr>"; }

	              if (!empty($webTruckInfo))  { $strTeam .= "<tr class='heading'><td colspan='3' class='alert'>Truck $valTruck: $webTruckInfo</td></tr>"; }

	              if ($webTruckStatus!='3')  { $strTeam .= "<tr class='heading'><td colspan='3'>Truck $valTruck status: <b>$webTruckDsca </b></td></tr>"; }

	              if (!empty($webOfficeInfo)) { $strTeam .= "<tr class='heading'><td colspan='3' class='alert'>$webOfficeInfo</td></tr>"; }

	              $strTeam .= "</table>";
	            }
	          }

	  return (isset($strTeam) ? $strTeam : FALSE);
	}

	function fnMessage($subject, $messagePlain, $messageHTML) {

	  global $messageHead;
	  global $messageStyle;
	  global $messageEnd;
	  global $messageBoundaryPlain;
	  global $messageBoundaryHTML;
	  global $messageBoundaryEnd;

	  $message  = $messageBoundaryPlain;
	  $message .= $messagePlain . "\r\n";
	  $message .= $messageBoundaryHTML;
	  $message .= $messageHead;
	  $message .= "<title>$subject</title>";
	  $message .= $messageStyle;
	  $message .= $messageHTML;
	  $message .= $messageEnd;
	  $message .= $messageBoundaryEnd;

	  return $message;

	}

	function fnSetWaiting($valJobNum) {

	  

	  $sqlUpd = "UPDATE job SET job_is_waiting = 1 WHERE job_yrnum = $valJobNum";
	  if ($resUpd = $this->db->query($sqlUpd)) { 
	    if ($this->db->affected_rows == 1) {
	      $_SESSION['msgInfo'][] = "Job $valJobNum set to Waiting for Paperwork.";
	    }
	  }
	}

	function fnSendError($txt) {

	  global $headers;
	  global $emailAdmin;

	  $subject       = "Portal Error";

	  $messagePlain  = $txt . "\r\n";

	  $messageHTML   = "<p>$txt</p>";

	  $messageHTML  .= "<table>";
	  $messageHTML  .= "<tr style='font-weight:bold;'><td>POST</td><td>VALUE</td></tr>";
	  foreach ($_POST AS $key => $value) {
	    $messageHTML .= "<tr><td>$key</td><td>$value</td></tr>";
	  }
	  $messageHTML  .= "<tr><td>&nbsp;</td><td>&nbsp;</td></tr>";
	  $messageHTML  .= "<tr style='font-weight:bold;'><td>GET</td><td>VALUE</td></tr>";
	  foreach ($_GET AS $key => $value) {
	    $messageHTML .= "<tr><td>$key</td><td>$value</td></tr>";
	  }
	  $messageHTML  .= "<tr><td>&nbsp;</td><td>&nbsp;</td></tr>";
	  $messageHTML  .= "<tr style='font-weight:bold;'><td>SESSION</td><td>VALUE</td></tr>";
	  foreach ($_SESSION AS $key => $value) {
	    $messageHTML .= "<tr><td>$key</td><td>$value</td></tr>";
	  }
	  $messageHTML  .= "<tr><td>&nbsp;</td><td>&nbsp;</td></tr>";
	  $messageHTML  .= "<tr style='font-weight:bold;'><td>SERVER</td><td>VALUE</td></tr>";
	  foreach ($_SERVER AS $key => $value) {
	    $messageHTML .= "<tr><td>$key</td><td>$value</td></tr>";
	  }
	  $messageHTML  .= "</table>";

	  $message = fnMessage($subject, $messagePlain, $messageHTML);

	  $to = $emailAdmin;
	  @mail("$to", "$subject", "$message", "$headers");
	}

	function fnEmailScherer($valJob) {

	  global $testingEmail;
	  
	  global $headers;
	  global $emailScherer;

	  $sqlJob  = "SELECT * FROM job";
	  $sqlJob .= " JOIN emp ON job_emp_cm = emp_id";
	  $sqlJob .= " JOIN job_status ON job_status = job_status_id";
	  $sqlJob .= " JOIN job_type ON job_type = job_type_id";
	  $sqlJob .= " JOIN prop_type ON job_prop_type = prop_type_id";
	  $sqlJob .= " WHERE job_id = $valJob";
	  $resJob = $this->db->query($sqlJob);
	  $myJob  = $resJob->rows;

	  $valCM           = $myJob['job_emp_cm'];
	  $webCMName       = prepareWeb($myJob['emp_fname'] . " " . $myJob['emp_lname']);
	  $webJobStatusDsca= prepareWeb($myJob['job_status_dsca']);
	  $webJobTypeDsca  = prepareWeb($myJob['job_type_dsca']);
	  $webJobLostInfo  = prepareWeb($myJob['job_lost_info']);
	  $webPropTypeDsca = prepareWeb($myJob['prop_type_dsca']);
	  $webCustFName    = prepareWeb($myJob['job_cust_fname']);
	  $webCustLName    = prepareWeb($myJob['job_cust_lname']);
	  $webPropAddr1    = prepareWeb($myJob['job_prop_addr1']);
	  $webPropAddr2    = prepareWeb($myJob['job_prop_addr2']);
	  $webPropCity     = prepareWeb($myJob['job_prop_city']);
	  $webPropState    = prepareWeb($myJob['job_prop_state']);
	  $webPropZip      = prepareWeb($myJob['job_prop_zip']);
	  $webSummary      = prepareWeb($myJob['job_summary']);
	  $webDTStart      = (is_null($myJob['job_dt_start']) ? "" : formatDTText(strtotime($myJob['job_dt_start'])));
	  $webSOAmt        = (is_null($myJob['job_so_amt']) ? "" : "$" . number_format($myJob['job_so_amt']/100));

	  $subject       = "Scherer Notify :: $webPropCity, $webPropState";

	  $messagePlain  = "This message is best viewed as HTML.\r\n";

	  $messageHTML   = "<p><b>$webCustFName $webCustLName</b>";
	  $messageHTML  .= (empty($webPropAddr1) ? "" : "<br />$webPropAddr1") ;
	  $messageHTML  .= (empty($webPropAddr2) ? "" : "<br />$webPropAddr2");
	  $messageHTML  .= "<br />$webPropCity, $webPropState $webPropZip";

	  $sqlNote = "SELECT * FROM job_note JOIN emp ON job_note_emp = emp_id WHERE job_note_job = $valJob ORDER BY job_note_dt_add DESC";
	  if ($resNote = $this->db->query($sqlNote)) {
	    if ($resNote->num_rows > 0) {
	      while ($myNote = $resNote->rows) {

	        $webNoteBody  = prepareWeb($myNote['job_note_body']);
	        $webNoteDTAdd = formatDTText(strtotime($myNote['job_note_dt_add']));
	        $webEmpCM     = prepareWeb(substr($myNote['emp_fname'],0,1) . "." . $myNote['emp_lname']);
	        $messageHTML  .= "<p><b>$webNoteDTAdd ($webEmpCM):</b><br />$webNoteBody</p>";
	      }
	    }
	  }

	  $messageHTML  .= "<p><b>Summary:</b><br />$webSummary</p>";

	  if (!empty($webJobLostInfo)) { $messageHTML  .= "<p>Info: <b>$webJobLostInfo</b></p>"; }

	  $messageHTML  .= "<table>";
	  $messageHTML  .= "<tr><td>Status: </td><td><b>$webJobStatusDsca</b></td></tr>";
	  $messageHTML  .= "<tr><td>Job Type: </td><td><b>$webJobTypeDsca</b></td></tr>";
	  $messageHTML  .= "<tr><td>Prop Type: </td><td><b>$webPropTypeDsca</b></td></tr>";
	  $messageHTML  .= "<tr><td>Client Mgr: </td><td><b>$webCMName</b></td></tr>";
	  $messageHTML  .= "<tr><td>Start:</td><td><b>$webDTStart</b></td></tr>";
	  $messageHTML  .= "<tr><td>Amount:</td><td><b>$webSOAmt</b></td></tr>";
	  $messageHTML  .= "</table>";

	  $message = fnMessage($subject, $messagePlain, $messageHTML);

	  if ($testingEmail) {
	    $sqlEmail = "SELECT emp_email FROM emp WHERE emp_id = 1";
	  } else {
	    $sqlEmail = "SELECT emp_email FROM emp WHERE emp_id = $valCM";
	  }

	  $resEmail = $this->db->query($sqlEmail);
	  while ($myEmail = $resEmail->rows) {
	    $arrEmail[] = $myEmail['emp_email'];
	  }

	  $to = implode(", ", $arrEmail);

	  if (!$testingEmail) {
	    $to .= ", " . $emailScherer;
	  }

	  if (@mail("$to", "$subject", "$message", "$headers")) {
	    $_SESSION['altInfo'][] = "Email sent to Scherer.";
	  } else {
	    $_SESSION['errInfo'][] = "Email not sent.  Please notify administrator.";
	  }

	}
	function fnEmailSchererCall($valCall) {

	  global $testingEmail;
	  
	  global $headers;
	  global $emailScherer;

	  $sqlCall  = "SELECT * FROM asd";
	  $sqlCall .= " JOIN emp ON call_emp_cm = emp_id";
	  $sqlCall .= " JOIN asd_disp ON call_disp = asd_disp_id";
	  $sqlCall .= " WHERE call_id = $valCall";
	  $resCall = $this->db->query($sqlCall);
	  $myCall  = $resCall->rows;

	  $valCM           = $myCall['call_emp_cm'];
	  $webCMName       = prepareWeb($myCall['emp_fname'] . " " . $myCall['emp_lname']);
	  $webDispName     = prepareWeb($myCall['asd_disp_name']);
	  $webCaller       = prepareWeb($myCall['call_caller']);
	  $webCity         = prepareWeb($myCall['call_city']);
	  $webState        = prepareWeb($myCall['call_state']);
	  $webMsgPad       = prepareWeb($myCall['call_asd_msg_pad']);
	  $webSummary      = prepareWeb($myCall['call_summary']);

	  $subject       = "Scherer Notify :: $webCity, $webState";

	  $messagePlain  = "This message is best viewed as HTML.\r\n";

	  $messageHTML   = "<p><b>$webCaller</b>";
	  $messageHTML  .= "<br />" . (empty($webPropCity) ? "" : "$webPropCity, ") . $webPropState;

	  $messageHTML  .= "<p>Summary: <b>$webSummary</b></p>";

	  $messageHTML  .= "<table>";
	  $messageHTML  .= "<tr><td>Status: </td><td><b>$webDispName</b></td></tr>";
	  $messageHTML  .= "<tr><td>Client Mgr: </td><td><b>$webCMName</b></td></tr>";
	  $messageHTML  .= "<tr><td>Message Pad: </td><td><b>$webMsgPad</b></td></tr>";
	  $messageHTML  .= "</table>";

	  $message = fnMessage($subject, $messagePlain, $messageHTML);

	  if ($testingEmail) {
	    $sqlEmail = "SELECT emp_email FROM emp WHERE emp_id = 1";
	  } else {
	    $sqlEmail = "SELECT emp_email FROM emp WHERE emp_id = $valCM";
	  }

	  $resEmail = $this->db->query($sqlEmail);
	  while ($myEmail = $resEmail->rows) {
	    $arrEmail[] = $myEmail['emp_email'];
	  }

	  $to = implode(", ", $arrEmail);

	  if (!$testingEmail) {
	    $to .= ", " . $emailScherer;
	  }

	  if (@mail("$to", "$subject", "$message", "$headers")) {
	    $_SESSION['altInfo'][] = "Email sent to Scherer.";
	  } else {
	    $_SESSION['errInfo'] = "Email not sent.  Please notify administrator.";
	  }

	}

	function fnEmailSchererGen($valCall) {

	  global $testingEmail;
	  
	  global $headers;
	  global $emailScherer;

	  $sqlCall  = "SELECT * FROM lead_gen";
	  $sqlCall .= " JOIN emp ON lgen_emp_cm = emp_id";
	  $sqlCall .= " JOIN asd_disp ON lgen_disp = asd_disp_id";
	  $sqlCall .= " WHERE lgen_id = $valCall";
	  $resCall = $this->db->query($sqlCall);
	  $myCall  = $resCall->rows;

	  $valCM           = $myCall['lgen_emp_cm'];
	  $webCMName       = prepareWeb($myCall['emp_fname'] . " " . $myCall['emp_lname']);
	  $webDispName     = prepareWeb($myCall['asd_disp_name']);
	  $webCaller       = prepareWeb($myCall['lgen_caller']);
	  $webCity         = prepareWeb($myCall['lgen_city']);
	  $webState        = prepareWeb($myCall['lgen_state']);
	  $webMsgPad       = prepareWeb($myCall['lgen_asd_msg_pad']);
	  $webSummary      = prepareWeb($myCall['lgen_summary']);

	  $subject       = "Scherer Notify :: $webCity, $webState";

	  $messagePlain  = "This message is best viewed as HTML.\r\n";

	  $messageHTML   = "<p><b>$webCaller</b>";
	  $messageHTML  .= "<br />" . (empty($webPropCity) ? "" : "$webPropCity, ") . $webPropState;

	  $messageHTML  .= "<p>Summary: <b>$webSummary</b></p>";

	  $messageHTML  .= "<table>";
	  $messageHTML  .= "<tr><td>Status: </td><td><b>$webDispName</b></td></tr>";
	  $messageHTML  .= "<tr><td>Client Mgr: </td><td><b>$webCMName</b></td></tr>";
	  $messageHTML  .= "<tr><td>Message Pad: </td><td><b>$webMsgPad</b></td></tr>";
	  $messageHTML  .= "</table>";

	  $message = fnMessage($subject, $messagePlain, $messageHTML);

	  if ($testingEmail) {
	    $sqlEmail = "SELECT emp_email FROM emp WHERE emp_id = 1";
	  } else {
	    $sqlEmail = "SELECT emp_email FROM emp WHERE emp_id = $valCM";
	  }

	  $resEmail = $this->db->query($sqlEmail);
	  while ($myEmail = $resEmail->rows) {
	    $arrEmail[] = $myEmail['emp_email'];
	  }

	  $to = implode(", ", $arrEmail);

	  if (!$testingEmail) {
	    $to .= ", " . $emailScherer;
	  }

	  if (@mail("$to", "$subject", "$message", "$headers")) {
	    $_SESSION['altInfo'][] = "Email sent to Scherer.";
	  } else {
	    $_SESSION['errInfo'] = "Email not sent.  Please notify administrator.";
	  }

	}

	function fnNotifyEmail($valJob, $strType) {

	  global $testingEmail;
	  
	  global $headers;
	  global $strURL;

	  $sqlJob  = "SELECT * FROM job";
	  $sqlJob .= " JOIN emp ON job_emp_cm = emp_id";
	  $sqlJob .= " JOIN job_status ON job_status = job_status_id";
	  $sqlJob .= " JOIN job_type ON job_type = job_type_id";
	  $sqlJob .= " JOIN prop_type ON job_prop_type = prop_type_id";
	  $sqlJob .= " JOIN ref_source ON job_ref_source = ref_id";
	  $sqlJob .= " JOIN state ON job_prop_state = st_id";
	  $sqlJob .= " WHERE job_id = $valJob";
	  $resJob = $this->db->query($sqlJob);
	  $myJob  = $resJob->rows;

	  $valCM           = $myJob['job_emp_cm'];
	  $webCMName       = prepareWeb($myJob['emp_fname'] . " " . $myJob['emp_lname']);
	  $webJobStatusDsca= prepareWeb($myJob['job_status_dsca']);
	  $webJobTypeDsca  = prepareWeb($myJob['job_type_dsca']);
	  $webJobLostInfo  = prepareWeb($myJob['job_lost_info']);
	  $webPropTypeDsca = prepareWeb($myJob['prop_type_dsca']);
	  $webCustFName    = prepareWeb($myJob['job_cust_fname']);
	  $webCustLName    = prepareWeb($myJob['job_cust_lname']);
	  $webCustTelp     = formatPhoneDash($myJob['job_cust_telp']);
	  $valCustExt      = $myJob['job_cust_ext'];
	  $webPropAddr1    = prepareWeb($myJob['job_prop_addr1']);
	  $webPropAddr2    = prepareWeb($myJob['job_prop_addr2']);
	  $webPropCity     = prepareWeb($myJob['job_prop_city']);
	  $webPropState    = prepareWeb($myJob['job_prop_state']);
	  $webPropZip      = prepareWeb($myJob['job_prop_zip']);
	  $webSummary      = prepareWeb($myJob['job_summary']);
	  $webRefName      = prepareWeb($myJob['ref_name']);
	  $webRefContact   = prepareWeb($myJob['job_ref_contact']);
	  $webRefCompany   = prepareWeb($myJob['job_ref_company']);
	  $webRefInfo      = prepareWeb($myJob['job_ref_info']);
	  $valEmpMkt       = $myJob['st_emp_mkt'];
	  $valEmpFun       = $myJob['st_emp_fun'];

	  switch ($strType) {
	  case "Mkt":
	    $subject       = "Marketing Notify :: $webPropCity, $webPropState";
	  break;
	  case "Fun":
	    $subject       = "Funeral Notify :: $webPropCity, $webPropState";
	  break;
	  }

	  $messagePlain  = "This message is best viewed as HTML.\r\n";

	  $messageHTML   = "<p><b>$webCustFName $webCustLName</b>";
	  $messageHTML  .= (empty($webPropAddr1) ? "" : "<br />$webPropAddr1") ;
	  $messageHTML  .= (empty($webPropAddr2) ? "" : "<br />$webPropAddr2");
	  $messageHTML  .= "<br />$webPropCity, $webPropState $webPropZip";

	  $sqlNote = "SELECT * FROM job_note JOIN emp ON job_note_emp = emp_id WHERE job_note_job = $valJob ORDER BY job_note_dt_add DESC";
	  if ($resNote = $this->db->query($sqlNote)) {
	    if ($resNote->num_rows > 0) {
	      while ($myNote = $resNote->rows) {

	        $webNoteBody  = prepareWeb($myNote['job_note_body']);
	        $webNoteDTAdd = formatDTText(strtotime($myNote['job_note_dt_add']));
	        $webEmpCM     = prepareWeb(substr($myNote['emp_fname'],0,1) . "." . $myNote['emp_lname']);
	        $messageHTML  .= "<p>$webNoteDTAdd ($webEmpCM): <b>$webNoteBody</b></p>";
	      }
	    }
	  }

	  $messageHTML  .= "<p>Summary: <b>$webSummary</b></p>";

	  if (!empty($webJobLostInfo)) { $messageHTML  .= "<p>Info: <b>$webJobLostInfo</b></p>"; }

	  $messageHTML  .= "<table>";
	  $messageHTML  .= "<tr><td>Status: </td><td><b>$webJobStatusDsca</b></td></tr>";
	  $messageHTML  .= "<tr><td>Job Type: </td><td><b>$webJobTypeDsca</b></td></tr>";
	  $messageHTML  .= "<tr><td>Prop Type: </td><td><b>$webPropTypeDsca</b></td></tr>";
	  $messageHTML  .= "<tr><td>CM: </td><td><b>$webCMName</b></td></tr>";
	  $messageHTML  .= "<tr><td>Referral Source: </td><td><b>$webRefName</b></td></tr>";
	  $messageHTML  .= "<tr><td>Referral Contact: </td><td><b>" . (isset($webRefContact) ? $webRefContact : "None") . "</b></td></tr>";
	  $messageHTML  .= "<tr><td>Referral Company: </td><td><b>" . (isset($webRefCompany) ? $webRefCompany : "None") . "</b></td></tr>";
	  $messageHTML  .= "<tr><td>Referral Info: </td><td><b>" . (isset($webRefInfo) ? $webRefInfo : "None") . "</b></td></tr>";
	  $messageHTML  .= "</table>";

	  $messageHTML  .= "<p><b>$webCustFName $webCustLName</b>";
	  $messageHTML  .= (empty($webPropAddr1) ? "" : "<br />$webPropAddr1") ;
	  $messageHTML  .= (empty($webPropAddr2) ? "" : "<br />$webPropAddr2");
	  $messageHTML  .= "<br />$webPropCity, $webPropState $webPropZip";
	  $messageHTML  .= "<br />$webCustTelp" . (empty($valCustExt) ? "" : " ext " . $valCustExt);
	  $messageHTML  .= "</p>";

	  $messageHTML  .= "<p><a href='" . $strURL . "/job.php?job=$valJob'>View this job on the Portal</a></p>";

	  $message = fnMessage($subject, $messagePlain, $messageHTML);

	  switch ($strType) {
	  case "Mkt":

	    $sqlEmail = "SELECT emp_email FROM emp WHERE emp_id = $valEmpMkt";

	    $resEmail = $this->db->query($sqlEmail);
	    while ($myEmail = $resEmail->rows) {
	      $arrEmail[] = $myEmail['emp_email'];
	    }
	    $to = implode(", ", $arrEmail);

	    if (@mail("$to", "$subject", "$message", "$headers")) {
	      $_SESSION['altInfo'][] = "Email sent to Marketing / Law Enforcement.";
	    } else {
	      $_SESSION['errInfo'] = "Email not sent.  Please notify administrator.";
	    }

	  break;
	  case "Fun":

	    $sqlEmail = "SELECT emp_email FROM emp WHERE emp_id = $valEmpFun";

	    $resEmail = $this->db->query($sqlEmail);
	    while ($myEmail = $resEmail->rows) {
	      $arrEmail[] = $myEmail['emp_email'];
	    }
	    $to = implode(", ", $arrEmail);

	    if (@mail("$to", "$subject", "$message", "$headers")) {
	      $_SESSION['altInfo'][] = "Email sent to Marketing / Funeral Home.";
	    } else {
	      $_SESSION['errInfo'] = "Email not sent.  Please notify administrator.";
	    }
	  break;
	  }
	}

	function restructureFilesArray($files)
	{
	    $output = [];
	    foreach ($files as $attrName => $valuesArray) {
	        foreach ($valuesArray as $key => $value) {
	            $output[$key][$attrName] = $value;
	        }
	    }
	    return $output;
	}

	function fnField($inpName, $inpType, $defValue, $defClass, $inpReq, $maxLength) {
	// only known to be used in jobTabTM as of 5/12/21 MSims

	  $value = (isset($_SESSION['inpValue'][$inpName]) ? $_SESSION['inpValue'][$inpName] : (isset($defValue) ? $defValue : ""));
	  $class = implode(" ", array((empty($defClass) ? "" : $defClass), (isset($_SESSION['errField']) ? (in_array($inpName, $_SESSION['errField']) ? "err" : $inpReq) : $inpReq)));
	  return "<input class='r " . $class . "' type='" . $inpType . "' name='" . $inpName . "' id='" . $inpName . "' value='" . $defValue . "' maxlength='$maxLength'" . ($inpReq == "reo" ? " readonly" : "") . " onChange='calcTot()' onKeyUp='calcTot()' />";

	}

	function returnTo($page = "index.php", $msg = null) {
	  if (isset($msg)) {
	    if (is_array($msg)) {
	      if ( isset($msg['msg']) ) $_SESSION['msgInfo'] = $msg['msg'];
	      if ( isset($msg['err']) ) $_SESSION['errInfo'] = $msg['err'];
	      if ( isset($msg['alt']) ) $_SESSION['altInfo'] = $msg['alt'];
	      if ( isset($msg['drk']) ) $_SESSION['drkInfo'] = $msg['drk'];
	    } else {
	       $_SESSION['msgInfo'] = $msg;
	    }
	  }
	  header("location:$page");
	  exit();
	}

	function returnJson($json = []) {
	  echo json_encode($json);
	  exit();
	}

	function console_log($output, $with_script_tags = true) {
	  $js_code = 'console.log(' . json_encode($output, JSON_HEX_TAG) . 
	');';
	  if ($with_script_tags) {
	      $js_code = '<script>' . $js_code . '</script>';
	  }
	  echo $js_code;
	}

	function fnSetting($arrIn) {

	  foreach($arrIn AS $value) {
	    $arrSaf[] = $this->prepareSaf($value);
	  }

	  
	  $safStrKey = implode(", ", $arrSaf);

	  $sql = "SELECT set_key, set_val FROM setting WHERE set_key IN ($safStrKey)";

	  if ($res = $this->db->query($sql)) {
	    while ($my = $res->rows) {
	      $arrOut[$my['set_key']] = $my['set_val'];
	    }
	    return $arrOut;
	  }
	  return FALSE;
	}

	/**
	 * Checks if a string starts with a given substring
	 * @param string $string
	 * @param string $startString
	 * @return bool True if string $starts with $startString
	 */
	function startsWith($string, $startString){
	  return substr($string, 0, strlen($startString)) === $startString;
	}

	/**
	 * Prepares an associative array for using implode in a sql query
	 */
	function prepareSQLArray($ary, $disclude = []){
	  $retAry = [];
	  foreach ($ary as $field => $value) {
	    if(!in_array($field, $disclude))
	      $retAry[] = "$field = $value";
	  }
	  return $retAry;
	}

	/**
	 * Prepares a sql insert array like prepareSQLArray but santizes the fields to safe first
	 * @param array $ary
	 * @param array $disclude
	 * @return array
	 */
	function prepareSafSQLArray($ary, $disclude = []){
	  $retAry = [];
	  foreach($ary as $field => $value){
	    if(!in_array($field, $disclude))
	      //$ary[$field] = $this->prepareSaf($value);
	      $ary[$field] = is_null($value) ? 'NULL' : $this->prepareSaf($value);
	  }

	  return prepareSQLArray($ary, $disclude);
	}

	/**
	 * DEV tool to show array values to the screen and exit (default)
	 * @param array $var
	 * @param boolean $exit
	 */
	function dump($var, $exit = false) {
	  echo '<pre>';
	  print_r($var);
	  echo '</pre>';
	  if ( !$exit ) exit;
	}

	/**
	 * DEV tool to echo a string value to the screen and exit (default)
	 * @param string $var
	 * @param boolean $exit
	 */
	function say($var, $exit = false) {
	  echo '<pre>' . $var . '</pre>';
	  if ( !$exit ) exit;
	}

	/**
	 * DEV tool to log to the PHP error log and continue (default)
	 * @param string $var
	 * @param boolean $stop
	 */
	function oops($var, $stop = false) {
	  error_log($var);
	  if ( $stop ) exit;
	}

	//str_contains polyfill
  function str_contains($haystack, $needle) {
    return $needle !== '' && mb_strpos($haystack, $needle) !== false;
  }

}