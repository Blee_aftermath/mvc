<?php
class Config {
	private $data = array();

	public function get($key) {
		return (isset($this->data[$key]) ? $this->data[$key] : null);
	}

	public function set($key, $value) {
		$this->data[$key] = $value;
	}

	public function has($key) {
		return isset($this->data[$key]);
	}

	public function load($filename) {

		$file = DIR_CONFIG . $filename;

		if (file_exists($file)) {
			$_ = array();

			require($file);
			$vars = get_defined_vars();
			//$this->data = array_merge($this->data, $vars);

			foreach( $vars as $k => $v) {
				$this->set($k,$v);
			}

			//echo '<pre>'; print_r(	$this->data); echo '</pre>'; exit;
		} else {
			trigger_error('Error: Could not load config ' . $filename . '!');
			exit();
		}
	}
	
}