<?php
// Version
define('VERSION', '1.0');

// Configuration
if (is_file('config/config.php')) {
    require_once('config/config.php');
}

// autoload libraries
require_once( DIR_SYSTEM . 'startup.php' );

// Class Container : Registry
$registry = new Registry();
$loader = new Loader($registry); // Dependency Injection Parttern 
$registry->set('load', $loader);



// Config
$config = new Config();

// Database
$db = new DB(DB_DRIVER, DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
$registry->set('db', $db);


$util = new Util($registry);
$registry->set('util',$util);

// configuration from DB
$query = $db->query("SELECT * FROM config");
foreach ($query->rows as $result) {
    if (!$result['serialized']) {
        $config->set($result['key'], $result['value']);
    } else {
        $config->set($result['key'], unserialize($result['value']));
    }
}

// configuration with file name
$config->load('variables.inc');
$registry->set('config', $config);

// define url
$url = new Url(HTTP_SERVER);
$registry->set('url', $url);

// Log
$config->set('logfile','error.log');
$log = new Log($config->get('logfile'));
$registry->set('log', $log);

// Request
$request = new Request();
$registry->set('request', $request);

// Response
$response = new Response();
$response->addHeader('Content-Type: text/html; charset=utf-8');
$response->setCompression($config->get('config_compression'));
$registry->set('response', $response);

// Cache
$cache = new Cache('file');
$registry->set('cache', $cache);

// Session
$session = new Session();
$registry->set('session', $session);

// Employee
$employee = new Employee($registry);
$registry->set('emp', $employee);



// Employee Group
if ($employee->isLogged()) {
    $config->set('employee_group_id', $employee->getGroupId());
} elseif (isset($session->data['employee']) && isset($session->data['employee']['employee_group_id'])) {
    // For API calls
    $config->set('employee_group_id', $session->data['employee']['employee_group_id']);
}

// Document
$registry->set('doc', new Document());

// Front Controller
$controller = new Front($registry);


/*
 TODO. check permission for all route except common/home and auth/login
*/


/*
// Login
$controller->addPreAction(new Action('common/login/check'));

// Permission
$controller->addPreAction(new Action('error/permission/check'));
*/

// Route
if (isset($request->get['route'])) {
    $action = new Action($request->get['route']);
} else {
    $action = new Action('common/home');
}

// Dispatch
$controller->dispatch($action, new Action('error/not_found'));

// Output
$response->output();
