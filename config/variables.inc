<?php

// Server

//if (isset($_SESSION['emp'])) $_SESSION['altInfo'][] = "ALERT: Cell carriers seem to be returning to normal.  Continue to watch for new developments and rely on texting, email, notes in the Portal and contact via landlines. [<a href='alert.php'>More Info</a>]";

//if (!isset($_SESSION['drkInfo'])) $_SESSION['drkInfo'][] = "<img class='icon16' src='icons/cupcake16.png' /> Happy Birthday Melinda!";

  $testing = 0;

  if (isset($_SERVER['PATH_INFO'])) die;

  $PHP_SELF     = htmlspecialchars($_SERVER['PHP_SELF'], ENT_QUOTES);  // both single and double quotes
  $phpSelf      = basename($PHP_SELF);
  $phpRefr      = isset($_SERVER['HTTP_REFERER']) ? basename(preg_replace('/\&amp;/', '&', htmlspecialchars($_SERVER['HTTP_REFERER'], ENT_QUOTES))) : null;
  //$safIPAddr    = escape_and_quote($_SERVER["REMOTE_ADDR"]);
  //$safIPHex     = escape_and_quote(encode_ip($_SERVER["REMOTE_ADDR"]));
  //$safBrowser   = (isset($_SERVER["HTTP_USER_AGENT"]) ? escape_and_quote($_SERVER["HTTP_USER_AGENT"]) : "Error HTTP_USER_AGENT");

//  $_SESSION['altInfo'][] = "Special alert.";

// Session

  if (isset($_SESSION['timeout'])) $_SESSION['inactivity'] = time() - $_SESSION['timeout'];
  $_SESSION['timeout'] = time();

  foreach ($_SESSION as $key=>$value) { if (isset($GLOBALS[$key])) unset($GLOBALS[$key]); }

  if (isset($_GET['PHPSESSID'])) {
    $newURI = preg_replace('/[.|?|&]PHPSESSID=[^&]+/',"",$_SERVER['REQUEST_URI']);
    header("HTTP/1.1 301 Moved Permanently");  
    header("Location: https://".$_SERVER['HTTP_HOST'].$newURI);  
    exit;  // Strip PHPSESSID from a URL and redirect to prevent browsers from manually sending.  Cookies required.
  }

  if (isset($_SESSION['emp']) && $_SESSION['emp'] == 305) { 
    session_destroy();
  }

// Dates

  $my_date_format = "Y-m-d";
  $my_date_long_format = "l, F j, Y";
  $format_MdY = "M d, Y";
  $format_Hi = "H:i";
  $my_datetime_format = "Y-m-d H:i:s";

  $today     = date($my_date_format); // Deprecated, replace all with $dToday
  $dToday    = date($my_date_format);
  $dEpoch    = date($my_date_format, 0); //unix-epoch timestamp (00:00:00 UTC on 1 January 1970)
  $dLToday   = date($my_date_long_format);
  /*
  $dYest     = convertStringD('-1 Days');
  $d3Ago     = convertStringD('-3 Days');
  $d7Ago     = convertStringD('-7 Days');
  $d1DOM     = convertStringD('first day of this month');
  $d1DLM     = convertStringD('first day of last month');
  $dlDLM     = convertStringD('last day of last month');
  */
  $tsNow     = time();
  $tsToday   = strtotime('Today', time());
  //$safDTNow  = escape_and_quote(formatDTmysql($tsNow));
  //$safDNow   = escape_and_quote(formatDmysql($tsNow));

  $dtToday   = new DateTime($dToday);

  $yrStart   = 2016;
  $yrToday   = date('Y'); // 2018
  $moToday   = date('m'); // 01-12
  $noToday   = date('n'); // 1-12
  $qrToday   = ceil(date('m')/3); // 1-4
  $yrMoToday = $yrToday . $moToday;

// Strings

  $jqueryFolder   = "includes/jquery-ui-1.11.4/";
  $fusionFolder   = "includes/fusioncharts/";
  $strEmpHide     = "(642, 646, 669)"; // Amer Assist, B.Legal, AMI Auditor
  $strJSCurrency  = "'en-US', {style: 'currency', currency: 'USD'}";
  $allowedTags    = "<p><b><i><u><a><hr><img><table><tr><th><td><ul><ol><lh><li><h1><h2><h3><h4><h4><h5>";
  $srcBlank       = "images/blank.jpg";
  $strOops        = "Oops! Something went wrong!";
  $strPmtAddress  = "75 Executive Drive, Suite 200, Aurora, IL 60504";
  $strNE          = "N/E";
  $strBBBReview   = "https://bit.ly/aftermathbbb";    

// Validation

  $minUser     = 4;       $maxUser     = 20;
  $minPass     = 5;       $maxPass     = 20;
  $minEmail    = 5;       $maxEmail    = 45;
  $minMD5PW    = 1;       $maxMD5PW    = 60;
  $minName     = 2;       $maxName     = 45; // Name, Title
  $minPWHint   = 1;       $maxPWHint   = 20;
  $minAddr     = 2;       $maxAddr     = 45; // Address, City, County
  $minPermit   = 2;       $maxPermit   = 90; // Permits on office
  $minState    = 2;       $maxState    = 30;
  $minZip      = 5;       $maxZip      = 5;
  $minTel      = 10;      $maxTel      = 14; // (xxx) xxx-xxxx
  $minExt      = 1;       $maxExt      = 10;  // Extension
  $minValidation = 1;     $maxValidation = 7;
  $minSubject  = 2;       $maxSubject  = 80;   // Contact, Message, Forum, Feedback, Annc
  $minBody     = 5;       $maxBody     = 9999; // Contact, Message, Forum
  $minHtm      = 0;       $maxHtm      = 9999; // Splash
  $minTyp      = 1;       $maxTyp      = 4;
  $minItem     = 1;       $maxItem     = 20;
  $minDsca     = 3;       $maxDsca     = 150;
  $minText     = 4;       $maxText     = 7000; // Text fields (Note) Defined as TEXT in MySQL
  $minInfo     = 4;       $maxInfo     = 999;  // Info and Summary fields
  $minUM       = 1;       $maxUM       = 10;
  $minSearch   = 1;       $maxSearch   = 20;
  $minTyp      = 1;       $maxTyp      = 4;
  $minInvNum   = 9;       $maxInvNum   = 11;
  $minCodeInsCo= 1;       $maxCodeInsCo= 5;
  $minVIN      = 17;      $maxVIN      = 17;
  $minLic      = 5;       $maxLic      = 10;  // License plate number
  $minDLNum    = 8;       $maxDLNum    = 12;  // Drivers license number
  $maxDate     = 20;      $minDate     = 6;   // Form field dates
  $minURL      = 5;       $maxURL      = 45;  // Google review
  $minTrans    = 5;       $maxTrans    = 40;  // Transponder number

// Numerics

  $maxTime            = 8;        // Form field times
  $maxSource          = 45;       // Lead Source
  $maxStatus          = 360;      // Status
  $maxNum             = 4;        // Maximum length of numbers entered (member ID, order ID)
  $maxAmt             = 10;       // Maximum length of amounts (sign-off) Changed from 7 to 10 to allow for .00 3/13/16
  $maxAmtK            = 5;        // Maximum length of amounts in thousands (smallint)
  $maxDaysPass        = 365;      // Maximum Password age
  $maxValue           = 10000000; // 10 million
  $maxlengthValue     = 13;
  $maxOrder           = 999;
  $maxSizePost        = 12582912; // 12MB -- post_max_size in php.ini enforces this. Stored here for reference in pages and errors.
  $maxSizePhoto       = 307200;   // 300KB Max of an individual image
  $maxSizeDoc         = 10485760; // 12MB  Max of an individual pdf document
  $maxSizeImage       = 10485760; // 10MB  Max of total upload
  $maxSizeXML         = 4194304;  // 4MB
  $maxSizeAvatar      = 20480;    // 20KB
  $numCharExcerpt     = 300;
  $lenCodeScope       = 2;
  $lenCodeType        = 4;
  $lenCodeITyp        = 3;
  $lenCodeInvActivity = 3;
  $lenCodeInvStatus   = 3;
  $lenCodeAuditE      = 3;
  $lenCodeAuditM      = 7;
  $lenTruncate1       = 9;
  $lenTruncate2       = 12;
  $lenTruncate3       = 20; // Rst Partner
  $lenJobNum          = 7;
  $lenJCNum           = 11;
  $lenTel             = 10;
  $lenTruck           = 3;
  $lenPCat            = 3;
  $lenGeotab          = 4;
  $lenPayID           = 4;
  $lenStockLevel      = 1;
  $lenHrs             = 7; // 0000.00 DECIMAL(6,2)
  $lenUOM             = 2;
  $lenSku             = 9;
  $lenUPC             = 12;
  $lenEntUnit         = 6;
  $maxTinyintUn       = 255;
  $maxSmallint        = 32767;
  $maxSmallintUn      = 65535;
  $maxMediumintUn     = 16777215;
  $schererAcct        = 5610;
  $perSIFAuth         = 82; // raised from 79 210108 MSims
  $maxPhotoPerEmail   = 24; // used in Send-To-Me on jobTabPhoto
  $defOffset          = -6;
  $defTZ              = "Central";
  $convKMperMI        = 1.609;
  $usdot              = 1407143;
  $taxIDNum           = "83-4302492"; // 46-1509720
  $maxDeclination     = 4;
  $lenJC              = 11; // Length of a JC Job number, xx-xxxx

// Arrays
  
  $arrWidthDBL         = array(1 => 170, 285, 345, 435);
  $arrWidthDBR         = array(1 => 985, 870, 810, 720);
  $arrMo               = array(1 => "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
  $arrKeywords         = array("aftermath", "suicide", "biological remediation");
  $arrValidImgTypes    = array("image/jpeg", "image/pjpeg", "image/gif", "image/png");
  $arrValidImgExt      = array("gif", "jpg", "png");
  $arrYN               = array("N", "Y");
  $arrHTMLYN           = array("&cross;", "&check;");
  $arrNoYes            = array("No", "Yes");
  $arrNoYesPend        = array("No", "Yes", "Pending");
  $arrAMPM             = array("am", "pm");
  $arrIsAct            = array("INACTIVE", "ACTIVE");
  $arrYesNo            = array(1 => "Yes", "No");
  $arrABC              = array(1 => "A", "B", "C");
  $arrPmtMethod        = array(1 => "Cash", "Check", "Charge");
  $arrPayByInsNon      = array(1 => "Insurance", "Customer");
  $arrPayByInsCusUnk   = array(1 => "Insurance", "Customer", "Unknown");
  $arrClaimFiledBy     = array(1 => "Employee", "Customer");
  $arrOpenClosed       = array(1 => "Open", "Closed");
  $arrSafTiming        = array(1 => "Current", "Post");
  $arrSafCat           = array(1 => "Safety", "P&P", "CallBack", "Vehicle");
  $arrSafSever         = array(1 => "Major", "Moderate", "Minor");
  $arrCoverage         = array(0 => "Open", "Confirmed", "Denied");
  $arrSafValid         = array(0 => "Open", "Confirmed", "Invalid");
  $arrActInactBoth     = array(1 => "Active", "Inactive", "Both");
  $arrFileError        = array(0 => "Success", 1 => "File size exceeds php limit", 2 => "File exceeds per-file limit", 3 => "File partially failed", 4 => "No file selected", 6 => "Missing temp folder", 7 => "Failed disk write", 8 => "PHP extension failed");
  $arrAFile            = array(1 => "Billable", "Waived", "N/A");
  $arrShipTo           = array(1 => "Home", "Shop");
  $arrOfficeView       = array(1 => "Main Approach", "Inside Office", "Inside Warehouse", "Parking Lot", "Team Fun", "EEOO Poster");
  $arrTruckView        = array(1 => "Main", "Right", "Front", "Left", "Back");
  $arrTruckFuel        = array(1 => "Diesel", "Gas");
  $arrQr               = array(1 => "Q1", "Q2", "Q3", "Q4");
  $arrMoQr             = array(1 => 1,1,1,2,2,2,3,3,3,4,4,4);
  $arrCCStatus         = array(0 => "None", "Active", "Suspended", "Cancelled");
  $arrEmpPay           = array(0 => "None", "Salary", "Hourly", "Contract");
  $arrTrailing         = array(0 => "RANGE", 1 => "MTD", 2 => "YTD", 30 => "T-30", 60 => "T-60", 90 => "T-90", 180 => "T-180", 360 => "T-360");
  $arrClassCode        = array(1 => "B", 2 => "C", 3 => "J", 4 => "E", 5 => "T", 6 => "R");
  $arrClassName        = array(1 => "Aftermath", 2 => "Coronavirus", 3 => "Jail Cell", 4 => "Air Quality", 9 => "Development");
  $arrIncIgn           = array(1 => "Include", "Ignore");
  $arrLeadType         = array(1 => "ASD Call", 2 => "General Call", 8 => "Web Lead", 9 => "Direct Lead");
  $arrWorkType         = array(1 => "Job", "JC", "EQS");
  $arrOccType          = array(1 => "Owner-Occupied", "Renter-Occupied", "Unknown");
  $arrVerifyLevel      = array(1 => "CM", "SUP");

  // 20 resusable colors, renamed with prefix "arr", $hiliteColors to be searched and replaced
  $hiliteColors = $arrColors = ['db79f3','79f3b8','f39479','7981f3','a5f379','f379c8','79ecf3','f3d679','b379f3','79f38f','f37986','79aaf3','cdf379','f379f1','79f3d1','f3ae79','8a79f3','8cf379','f379af','79d3f3'];

// Defaults

  $delim          = " &#183; ";
  $arrowR         = "&#11166;";
  $arrowL         = "&#11164;";
  $arrowU         = "&#11165;";
  $arrowD         = "&#11167;";
  $save           = "&#128190;";
  $page           = 1;
  $defRPP         = 40;
  $pagesToShowFwd = 4;
  $pagesToShowBck = 4;
  $defWriteoff    = 0.25;
//  $defPWO[1]      = 0.25; // 200424 changed to PWO by Class in dbReal // moved to class table 210325
//  $defPWO[2]      = 0.15; // 200424 changed to PWO by Class in dbReal // moved to class table 210325
  $scriptRandom   = date('s');
  $scriptUUID     = time();
  $emptyDate      = "&#183;&#183; / &#183;&#183; / &#183;&#183;";

// Fusion

  $canvasBorderColor     = "#242";
  $canvasBorderThickness = 2;
  $colorHighlight        = "99cc33"; // tabOfficeArea

// Email

//  ini_set('SMTP','aftermath-com.mail.protection.outlook.com');

  $emailAdmin       = "msims@aftermath.com, jcirino@aftermath.com, skiehn@aftermath.com";
  $emailFrom        = "portal@aftermath.com";
  $emailHelp        = "msims@aftermath.com";
  $emailScherer     = "aftermathcalls@gmail.com, jjoseph@aftermath.com, aftermath90@gmail.com";
  $emailAudit       = "tbao@aftermath.com, jdana@aftermath.com, tmontalbano@aftermath.com, cdecker@aftermath.com, bwarcholek@aftermath.com, kbrown@aftermath.com, bciaccio@aftermath.com, mbroom@aftermath.com";
  $emailInsurance   = "tsmith@aftermath.com";
  $emailLeadGrabber = "elg@aftermath.com";
  $emailMktLog      = "tbao@aftermath.com, bdirks@aftermath.com, sbrown@aftermath.com";
  $emailSafety      = "msims@aftermath.com, mbroom@aftermath.com, cdecker@aftermath.com, bciaccio@aftermath.com, kbrown@aftermath.com, bmarks@aftermath.com, tcoleman@aftermath.com, kbosse@aftermath.com, jdevries@aftermath.com, nspears@aftermath.com, jlemon@aftermath.com";
  $emailCrem        = "tsmith@aftermath.com";
  $emailCSR         = "csr@aftermath.com";
  $boundary = uniqid('np'); 

  $headers = "MIME-Version: 1.0" . "\r\n";
  $headers .= "Content-Type: multipart/alternative; boundary=" . $boundary . "\r\n";
  $headers .= "From: " . $emailFrom . "\r\n";
  $headers .= "X-Mailer: PHP/" . phpversion() . "\r\n";
  $headers .= "Return-Path: " . $emailFrom . "\r\n";
  $headers .= "Bcc: " . $emailAdmin . "\r\n";

  $headersFile = "MIME-Version: 1.0" . "\r\n";
  $headersFile .= "Content-Type: multipart/mixed; boundary=" . $boundary . "\r\n";
  $headersFile .= "From: " . $emailFrom . "\r\n";
  $headersFile .= "X-Mailer: PHP/" . phpversion() . "\r\n";
  $headersFile .= "Return-Path: " . $emailFrom . "\r\n";
  $headersFile .= "Bcc: " . $emailAdmin . "\r\n";

  $messageHead   = "\r\n<html><head>";
  $messageStyle  = "\r\n<style type='text/css'>";
  $messageStyle .= "\r\nbody                        { font-family: verdana, sans-serif; font-size:12px; }";
  $messageStyle .= "\r\ntable                       { cellpadding: 0;border-spacing: 0;border-collapse:collapse; }";
  $messageStyle .= "\r\ntd                          { font-size:12px; }";
  $messageStyle .= "\r\ntd l                        { text-align:left; }";
  $messageStyle .= "\r\ntable.team                  { width:285px; background-color:#fff; border:3px inset #003; }";
  $messageStyle .= "\r\ntable.team tr.heading td    { padding:2px 2px 2px 5px; }";
  $messageStyle .= "\r\ntable.team tr.content td    { padding:2px 0 2px 5px; }";
  $messageStyle .= "\r\ntable.team tr.content td.tel{ text-align:right;padding-right:5px;white-space:nowrap; }";
  $messageStyle .= "\r\ntd.highlight                { background-color:#ff6; }";
  $messageStyle .= "\r\n</style></head><body>";
  $messageEnd    = "\r\n</body></html>\r\n";
  $messageBoundaryPlain = "\r\n--" . $boundary . "\r\nContent-Type: text/plain; charset=\"iso-8859-1\"\r\nContent-Transfer-Encoding: 8bit\r\n\r\n";
  $messageBoundaryHTML  = "\r\n--" . $boundary . "\r\nContent-Type: text/html; charset=\"iso-8859-1\"\r\nContent-Transfer-Encoding: 8bit\r\n\r\n";
  $messageBoundaryFile  = "\r\n--" . $boundary . "\r\n";
  $messageBoundaryEnd   = "\r\n--" . $boundary . "--\r\n";

  $templateStyle  = "\r\n<style type='text/css'>";
  $templateStyle .= "\r\nbody                        { font-family: verdana, sans-serif; font-size:12px; }";
  $templateStyle .= "\r\ntable                       { cellpadding: 0;border-spacing: 0;border-collapse:collapse; }";
  $templateStyle .= "\r\ntd                          { padding:2px 6px; font-size:12px;}";
  $templateStyle .= "\r\np                           { padding:2px 6px; }";
  $templateStyle .= "\r\n</style></head><body>";

  /**
   * Time between javascript auto-updates in milliseconds
   */
  $jsAutoUpdate = 60*1000;


  // besso append
  // TODO project - page mapping with role permission need
  $arrMenu = array(
    array('MASTER', 'admin',     'Adm',       '/dir/admin',      'Administration'),
    array('COMMON', 'my',        'My',        '/dir/my',         'My Status'),
    array('JOB_E',  'call',      'Call',      '/dir/call',       'Call Leads'),
    array('JOB_E',  'lgen',      'Gen',       '/dir/lgen',       'General Calls'),
    array('JOB_E',  'lead',      'Web',       '/dir/lead',       'Website Leads'),
    array('JOB_E',  'ldir',      'Dir',       '/dir/ldir',       'Direct Leads'),
    array('JOB_PG', 'job',       'Job',       '/dir/job',        'Jobs'),
    array('JOB_PG', 'jc',        'JC',        '/dir/jcJob',      'Jail Cell Jobs'),
    array('JOB_PG', 'res',       'RJ',        '/dir/resDB',      'Restoration Jobs'),
    array('JOB_PG', 'eqs',       'EQ',        '/dir/dbEnv',      'Environmental Jobs'),
    array('FIN_E',  'invoice',   'Inv',       '/dir/invoice',    'Invoices'),
    array('FIN_E',  'csr',       'CSR',       '/dir/csr',        'Invoices'),
    array('FIN_E',  'collection', 'Collect',  '/dir/cash',       'Collections'),
    array('COMMON', 'team',      'Team',      '/dir/team',       'Teams'),
    array('JOB_E',  'insurance', 'Ins',       '/dir/insurance',  'Insurance Companies'),
    array('JOB_E',  'partner',   'Cont',      '/dir/partnerIns', 'Partners'),
    array('IV_E',   'inventory', 'Invy',      '/dir/inventory',  'Inventory'),
    array('JOB_E',  'opportunity','Opp',      '/dir/opportunity','Opportunity Logger'),
    array('COMMON', 'resources', 'Res',       '/dir/resources',  'Resources'),
    array('RPT',    'report',    'Rpt',       '/dir/report',     'Reports'),
    array('RDB',    'db1',       'DB1',       '/dir/db1',        'Dashboard 1'),
    //array('RDB',    'dbAR',      'DB2',       '/dir/dbAR',       'AR Dashboard'),
    //array('SCH_R',  'cm4',       'CM',        '/dir/cm4',        'CM Schedule'),
    //array('COMMON', 'help',      'Help',      '/dir/help',       'Help')
  );

?>
