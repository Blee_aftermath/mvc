<?php
// global variables. seperate for deploy purpose

ini_set('display_errors',1);
error_reporting(E_ALL);

define('HTTP_SERVER', 'http://mvc.local/');

// DIR
define('DIR_APPLICATION', 'C:\inetpub\mvc/');
define('DIR_SYSTEM', 'C:\inetpub\mvc\system/');
define('DIR_CONFIG', 'C:\inetpub\mvc\config/');
define('DIR_VIEW', 'C:\inetpub\mvc\view/');
define('DIR_TEMPLATE', 'C:\inetpub\mvc\view/');
define('DIR_IMAGE', 'C:\inetpub\mvc\image/');
define('DIR_CACHE', 'C:\inetpub\mvc\system/cache/');
define('DIR_DOWNLOAD', 'C:\inetpub\mvc\system/download/');
define('DIR_UPLOAD', 'C:\inetpub\mvc\system/upload/');
define('DIR_LOGS', 'C:\inetpub\mvc\system/logs/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '0000');
define('DB_DATABASE', 'mvc');

// Local
define('SITE_NAME', 'Dev Portal');
define('T', time());