<?php 

/* 
controller
	request handle
	data handle with model class (layer)
	business logic
	actions
	transfer best variables to view layer
*/
class ControllerCommonHome extends Controller {

	public function index() {

		$data['title'] = "Home";
		
		$data['header'] = $this->load->controller('common/header');
        $data['left']   = $this->load->controller('common/left');
        $data['footer'] = $this->load->controller('common/footer');

        $view = $this->load->view('common/home.tpl', $data);
        $this->response->setOutput($view);

	}

}