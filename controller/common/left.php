<?php
class ControllerCommonLeft extends Controller {
    public function index() {
        $basket = [];
        $viewTpl = ( isset($this->request->get['bootstrap']) ) ? 'common/left_bootstrap.tpl' : 'common/left.tpl';
        return $this->load->view($viewTpl, $basket);
    }
}