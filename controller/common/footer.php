<?php
class ControllerCommonFooter extends Controller {
    public function index() {
        $basket['text_footer'] = 'footer';
        
        $viewTpl = ( isset($this->request->get['bootstrap']) ) ? 'common/footer_bootstrap.tpl' : 'common/footer.tpl';
        return $this->load->view($viewTpl, $basket);
    }
}