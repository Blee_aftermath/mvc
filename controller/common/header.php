<?php
class ControllerCommonHeader extends Controller {
    public function index() {
        
        $basket = array();

        // dynamic varialbes for different css, js use  
        $args = func_get_args()[0];
        if ( !empty($args) ) {
            foreach ( $args as $key => $val ) {
                $basket[$key] = $val;
            }
        }

        $basket['arrMenu'] = $this->config->get('arrMenu');

        $viewTpl = ( isset($this->request->get['bootstrap']) ) ? 'common/header_bootstrap.tpl' : 'common/header.tpl';
        return $this->load->view($viewTpl, $basket);
    }

}