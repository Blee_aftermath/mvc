<?php 

/* 
controller
	request handle
	basket handle with model class (layer)
	business logic
	actions
	transfer best variables to view layer and get HTML
	response HTML
*/
/* router
	http://127.0.0.1:88/jif/postjob?foo=var
	1st dir "jif" is folder
	2nd postjob "postjob" is PostJob class in postjob.php file ( no case sensitive )
	
	http://127.0.0.1:88/index.php?route=jif/postjob/callMethod
	3rd a method inside the class
	
	query string foo=var
*/
class ControllerJifPostJob extends Controller {

	public function greeting() {
		//$this->util->showAlert('hello');
		$name = (null !== $this->request->get['name']) ? $this->request->get['name'] : 'No-name';
		echo '<pre>'; print_r('Hello, ' . $name); echo '</pre>';
	}

	public function index() {

		/*
			we can include any mvc set as variable inside controller
			It has own MVC pattern and load-> return just HTML for *this* view
		*/
		$basketHeader['incJQueryCSS'] = '/view/asset/js/jquery/jquery-ui-1.11.4/jquery-ui.css';
		$basketHeader['incJifStyles'] = '/view/asset/css/jifStyles.css?v=' . time() ;
		$basketHeader['title'] = SITE_NAME . ' Test';
	
		$header = $this->load->controller('common/header', $basketHeader);
	    //$left = $this->load->controller('jif/left');
	    $footer = $this->load->controller('common/footer');

	    $basket['header'] = $header;
	    $basket['footer'] = $footer;



		//echo '<pre>'; print_r($this->request->json['foo']); echo '</pre>'; exit;

		$this->load->model('jif/postjob');
		$jobID = '12345';
		$postJobs = [
		  'jobID' => $jobID,
		  'itemTemplate' => $this->load->view('jif/postJobItem.tpl'),
		  'postJob' => $this->model_jif_postjob->getPostJobs($jobID)
		];
		//convert basket to json
		$basket['postJobs'] = json_encode($postJobs);



	    $view = $this->load->view('jif/postJob.tpl', $basket);

	    $this->response->setOutput($view);

	}

	/* it works as like API then same MVC pattern with easy */
	public function addPostJob() {

		// $request from Module javascript fetch call
		$req = $this->request->json;

		/*
			call model layer
				load->model to initiate that model class and generate model_folder_phpfile class dynamically
				follwing the same conventional routing
		*/
		$this->load->model('jif/postjob');
		$status = $this->model_jif_postjob->addPostJob($req);
		
		echo json_encode(['ok' => $status]);
	}

	public function deletePostJob() {

		$req = $this->request->json;
		
		/* 
			for simple query, we can call DB on Controller as easy
		 */	
    	$jobID       = $req['jobID'];
    	$postJobID   =  $req['postJobID'];
   	
    	$query  = "DELETE FROM job_pj";
  		$query .= " WHERE jpj_job = $jobID and jpj_post = $postJobID";
  		$status = $this->db->query($query);

		echo json_encode(['ok' => $status]);
  	}

  	public function besso() {
  		if ( isset($this->request->get['foo'])) {
  			$arg = $this->request->get['foo'];
  		}
  		echo '<pre>'; print_r("Hi Besso : $arg"); echo '</pre>';
  	}

}