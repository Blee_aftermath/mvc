<?php 

class ControllerAdminlteIndex extends Controller {

	public function index() {

		$data['title'] = "AdminLte";

		$data['header'] = $this->load->controller('common/header');
        $data['left']   = $this->load->controller('common/left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('adminlte/index.tpl', $data));

	}

}