<?php 

/* 
controller
	request handle
	data handle with model class (layer)
	business logic
	actions
	transfer best variables to view layer
*/
class ControllerAuthLogin extends Controller {

	public function index() {

		$data['title'] = "Auth";
		
		if ($this->emp->isLogged() && isset($this->request->get['user_token']) && isset($this->session->data['user_token']) && ($this->request->get['user_token'] == $this->session->data['user_token'])) {
			$this->response->redirect($this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token']));
		}

		if ( !empty($this->request->post) ) {
			/*
			    [inpUsername] => blee
    			[inpPassword] => Computer22
    			[inpCaptcha] => ssssss
    			[subLogin] => Login
			*/
			$this->login();
		}


		$data['header'] = $this->load->controller('common/header');
        $data['footer'] = $this->load->controller('common/footer');

        $data['maxUser'] = $this->config->get('maxUser');
        $data['maxPass'] = $this->config->get('maxPass');

        $view = $this->load->view('auth/login.tpl', $data);

        $this->response->setOutput($view);
	}

	public function showCaptcha() {
		// 3rd party captcha
		$captcha = new Securimage();
		if (!empty($this->request->get['namespace'])) $captcha->setNamespace($this->request->get['namespace']);
		$captcha->show();
	}

	public function login() {
		$errCount = 0;
		$maxPass = $this->config->get('maxPass');
		$minPass = $this->config->get('minPass');


		$maxUser = $this->config->get('maxUser');
		$minUser = $this->config->get('minUser');

		if (isset($_POST['inpPassword']) && $this->util->sanityCheck($_POST['inpPassword'], 'string', $maxPass, $minPass)==TRUE) {
	      $md5_pw = hash('md5', $_POST['inpPassword']);
	    } else {
	      $errCount++;
	    }

	    if (isset($_POST['inpUsername']) && $this->util->sanityCheck($_POST['inpUsername'], 'string', $maxUser, $minUser)==TRUE) {
	      $valUsername = $_POST['inpUsername'];
	    } else {
	      $errCount++;
	    }

	    if (isset($_POST['chkRememberMe']) && ($_POST['chkRememberMe'] == true)) { $safRememberMe = 1; } else { $safRememberMe = 0; }

	    $securimage = new Securimage();

	    $inpField = "inpCaptcha";
	    if ($securimage->check($_POST[$inpField]) == false) {
	      $errCaptcha = "Invalid Captcha.";
	      //$errCount++;
	    }
		
	    if ($errCount == 0) { // If there are no errors in the form, then attempt to log in
	      if ($this->util->fnLogin($valUsername, $md5_pw, $safRememberMe)) {
	      	if (isset($_SESSION['goAfterLogin'])) {
	          //$goAfterLogin = $_SESSION['goAfterLogin'];
	          $goAfterLogin = '/jif/postjob';
	          unset($_SESSION['goAfterLogin']);
	          header("Location: $goAfterLogin"); exit();
	        } else {
	          $goAfterLogin = '/jif/postjob';
	          header("Location: $goAfterLogin"); exit();
	          //header("Location: index.php"); exit();
	        }
	      } else {
	      	echo '<pre>'; print_r(	'a'); echo '</pre>'; exit;
	        $_SESSION['errInfo'] = "The username and password combination you have entered is not valid.";
	      }
	    } else {
	      $_SESSION['errInfo'] = "Login error.  Please check your username and password or validate the captcha code correctly."; 
	    }
	}

}