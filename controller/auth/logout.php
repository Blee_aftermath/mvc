<?php

class ControllerAuthLogout extends Controller {

    function index() {

        // TODO. test code
        $location = '/common/home?bootstrap=true';

        $_SESSION = array(); // Unsets all session variables

        setcookie(session_name(), '', time() - 42000); // Remove the session cookie from the user browser
        session_destroy();

        setcookie("login_id", "", -1); 
        setcookie("password", "", -1); 

        $this->response->redirect($location);
    }
}