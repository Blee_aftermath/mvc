<?php 
class ControllerTeamList extends Controller {
	
	function index() {

		$this->load->model('team/list');
		
		$basket['title']  = 'Team';
		$basket['header'] = $this->load->controller('common/header');
        $basket['left']   = $this->load->controller('common/left');
        $basket['footer'] = $this->load->controller('common/footer');
        $viewTpl = 'team/list.tpl';

        // ==================================================================================================== //
		// === REQUEST === //
		// ==================================================================================================== //

        $arrValidAction = array('box', 'list', 'add', 'edit', 'delete');
        if (isset($_GET['action'])) if (in_array($_GET['action'], $arrValidAction)) $action = $_GET['action'];
        if (empty($action)) { $action = 'box'; }


		// ==================================================================================================== //
		// === QUERY === //
		// ==================================================================================================== //

        // single team
        if (isset($_GET['team']) && is_numint($_GET['team'])) {
        	$teamID = $valTeam = $_GET['team'];
        	$myTeam = $this->model_team_list->getTeam($teamID);
        	if ( !empty($myTeam) ) {
	        	if (empty($action)) $action = 'edit';
	            $valOffice = $myTeam['team_office'];
	            //$viewTpl = 'team/my.tpl';
	        } else {
	        	$_SESSION['errInfo'] = 'Team not found.';
	        }
      	}

      	// a line global record
        
		switch ($action) {
			case 'box':
				$empStatHtml = $this->htmlEmpStat();

		    	// getTeamsByRegion
		    	$regions = $this->model_team_list->getTeamsByRegion();

			break;
		}

		// TODO more proper var name
		$basket['empStatHtml'] = $empStatHtml;
		$basket['regions'] = $regions;


		// ==================================================================================================== //
		// === Team Contact List === //
		// ==================================================================================================== //
		$teamList = [
			'M' => 'CM',
			'I' => 'IT',
			'F' => 'Funeral',
			'L' => 'LESR',
			'N' => 'Indoor Science',
			'R' => 'Regional',
			'C' => 'CSR',
			'H' => 'Home Office',
		];

		// vue trial
		$teamContactList = $this->model_team_list->getTeamContackList($teamList);
		$basket['teamContactList'] = json_encode($teamContactList, JSON_HEX_APOS);

        $view = $this->load->view($viewTpl, $basket);
        $this->response->setOutput($view);
	}



	// TODO. it's used in several services. consider common moudle
	public function htmlEmpStat() {

		$html = '';
		$arrStats = [];

		$cond = [
			'5' => 'RM',
			'6' => 'Sub',
			'7' => 'Bac',
			'8' => 'Tec'
		];

		$this->load->model('employee/list');
		// new ()

		foreach ( $cond as $teamID => $name ) {
			$myEmp  = $this->model_employee_list->getEmpByTitle($teamID);
			$myTeam = $this->model_employee_list->getMyteamByTitle($teamID);
			$arrStats[] = "$name: <b>" . count($myTeam)	 . '/' . count($myEmp) . '</b>';
		}

		$teamIDs = array_keys($cond);
		//$teamIDs = http_build_query(array_keys($cond), '', ',');;
		$myEmp  = $this->model_employee_list->getEmpByTitles($teamIDs);
		$myTeam = $this->model_employee_list->getMyteamByTitles($teamIDs);
		$arrStats['Total'] = 'Total: <b>' . count($myTeam)	 . '/' . count($myEmp) . '</b>';
		
		// Total
		$myEmp  = $this->model_employee_list->getEmpByTitles([5,6,7,8]);		
		$myTeam = $this->model_employee_list->getMyteamByTitles([5,6,7,8]);
		$arrStats[] = 'Total: <b>' . count($myTeam)	 . '/' . count($myEmp) . '</b>';

    	$myStats = $this->model_employee_list->getHireStat();
		$arrStats[] = 'MTD Hire/Term: <b>' . $myStats['mtd_hire'] . '/' . $myStats['mtd_term'] . '</b>' . $this->config->get('delim') . 'YTD Hire/Term: <b>' . $myStats['ytd_hire'] . '/' . $myStats['ytd_term'] . '</b>'; 

    	$html  = '(Assigned/Active)' . $this->config->get('delim');
    	$html .= implode($this->config->get('delim'), $arrStats);


//$this->load->model('jif/list');


    	return $html;

	}


	function htmlTeamContact($teamContact) {

	}
}