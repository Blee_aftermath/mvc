
<?php echo $header ?>

    <!-- test mockup -->
    <!--
    TODO. modulize each section to share
    ......
    -->
    <style>
    table.team {
        table-layout: auto;
        width: 100%;
        background-color: #fff;
        border: 2px inset #069;
        margin-bottom: 12px;
    }
    .col {
        float: left;
        padding: 10px 0px 12px 12px;
    }
    </style>

    <!-- TODO. vue for test -->
    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.14"></script>

    <div class="container-fluid">
      <div class="row">
        
        <?php
        echo $left;
        ?>

        <main class="col-md-12 ms-sm-auto col-lg-12 px-md-4 border-primary m-2" id="content">
          
          <div class="container">

            <div class="border p-2 text-center">
              <?php echo $empStatHtml; ?>
            </div>

            
            
            <?php
            if ( !empty($regions )) {
              //echo '<pre>'; print_r($regions); echo '</pre>';


              foreach ($regions as $i => $region) {

                if ( $i%4 == 0 ) echo "<div class='row'>";

                $valRegion     = $region['valRegion'];
                $webRegionCode = $region['webRegionCode'];
                $webRegionName = $region['webRegionName'];
                $webColor1     = $region['webColor1'];
                $webColor2     = $region['webColor2'];
                $webRMName     = $region['webRMName'];
                $webRMTelc     = $region['webRMTelc'];

                echo "<div class='col'>";

                echo "<table class='team table'>";
                echo "<tr class='title'><td colspan='3' style='padding:5px;font-size:2em;text-align:center;color:#fff;background-color:#" . $webColor1 . "'>$webRegionName"."</td></tr>";
                echo "<tr class='content'><td style='width:15px;'>R:</td><td>$webRMName</td><td style='width:105px;'>$webRMTelc</td></tr>";
                echo "</table>";

                if ( isset($region['team']) ) {
                foreach ( $region['team'] as $myTeam) {
                  
                  //echo '<pre>'; print_r($myTeam); echo '</pre>';

                  if ( empty($myTeam) ) continue;

                  $valTeam         = $myTeam['valTeam'];
                  $valOffice       = $myTeam['valOffice'];
                  $webOfficeName   = $myTeam['webOfficeName'];
                  $webOfficeCity   = $myTeam['webOfficeCity'];
                  $webOfficeState  = $myTeam['webOfficeState'];
                  $webTeamInfo     = $myTeam['webTeamInfo'];
                  $webOfficeInfo   = $myTeam['webOfficeInfo'];
                  $webTruckInfo    = $myTeam['webTruckInfo'];
                  $webTruckStatus  = $myTeam['webTruckStatus'];
                  $webTruckStatusDsca = $myTeam['webTruckStatusDsca'];
                  $valTruck        = $myTeam['valTruck'];
                  $webLatTruck     = $myTeam['webLatTruck'];
                  $webLonTruck     = $myTeam['webLonTruck'];
                  $valGeoDTLoc     = $myTeam['valGeoDTLoc'];
                  $valGeoComm      = $myTeam['valGeoComm'];
                  $valCountRP      = $myTeam['valCountRP'];
                  $valHasHydroxyl  = $myTeam['valHasHydroxyl'];

                  /* TODO. later
                  $webGeoIsNotComm = '';
                  if (!$myTeam['geo_is_comm']) {
                    $webGeoIsNotComm = "<img class='icon16' src='/view/asset/icons/error16.png' title='Geotab offline";
                    if ($valGeoDTLoc) {
                      $i = date_diff( new DateTime($valGeoDTLoc), new DateTime() );
                      $webGeoIsNotComm .= ": " . ($i->days > 0 ? $i->format('%a days') : 'Less than a day');
                    }
                    $webGeoIsNotComm .= "' />";
                  }
                  */

                  echo "<table class='team table'>";
                  echo "<tr class='title'><td colspan='3' style='background-color:#" . $webColor2 . "'>";

                  echo "<span class='span16' style='float:left;margin-right:4px;'>";
                  echo "<a href='state.php?state=$webOfficeState'><img class='sprite $webOfficeState' src='/view/asset/img/trans1x1.png' title='$webOfficeState'/></a>&nbsp;";
                  echo "</span>";

                  echo "<span style='float:left;margin-top:2px;'>";
                  echo "<b><a href='office.php?office=$valOffice'>$webOfficeName</a></b>";
                  echo "</span>";

                  // EDIT BUTTON
                  echo (in_array('TEAM', $_SESSION['perm']) ? "<span style='float:right;margin-left:5px;'><a href='" . $_SERVER['PHP_SELF'] . "?team=$valTeam&action=edit'><img class='icon16' src='/view/asset/icons/edit16.png' title='Edit Team'/></a></span>" : "");

                  // HYDROXYL ICON
                  if ($valHasHydroxyl == 1) { echo "<span style='float:right;margin-left:5px;'><img class='icon16' src='/view/asset/icons/hydroxyl16.png' title='This office has a Hydroxyl Generator'/></span>"; }

                  // RESTORATION BUTTON
                  if ($valCountRP > 0) { echo "<span style='float:right;margin-left:5px;'><a href='office.php?office=$valOffice&tab=tabPartnerRes'><img class='icon16' src='/view/asset/icons/restoration16.png' title='$valCountRP Restoration Partner" . ($valCountRP == 1 ? "" : "s") . "'/></a></span>"; }

                  /*
                  // GEOTAB BUTTON
                  if ($webGeoIsNotComm) {
                      echo "<span style='float:right;margin-left:5px;'>$webGeoIsNotComm</span>";
                  } else {
                    if (!is_null($webLonTruck)) {
                      echo "<span style='float:right;' class='ml-1'><a href='https://www.google.com/maps?t=m&q=$webLatTruck+$webLonTruck' target='map'><img class='icon16' src='/view/asset/icons/map16.png' title='Truck Location'/></a></span>";
                    }
                  }
                  */


                  echo "<span style='float:right;margin-top:2px;'>";
                  echo "<a href='truck.php?truck=$valTruck' title='Truck $valTruck ($webTruckStatusDsca)'>$valTruck </a>";
                  echo "&nbsp;</span>";
                  echo "</td></tr>";

                  if ( isset($myTeam['emp']) ) {

                    foreach ( $myTeam['emp'] as $myTeamEmp ) {
                      if ( empty($myTeamEmp) ) continue;
                      $valIsSup      = $myTeamEmp['valIsSup'];
                      $valEmp        = $myTeamEmp['valEmp'];
                      $valIsTrn      = $myTeamEmp['valIsTrn'];
                      $valDays       = $myTeamEmp['valDays'];
                      $webTitleShort = $myTeamEmp['webTitleShort'];
                      $webEmpFName   = $myTeamEmp['webEmpFName'];
                      $webEmpLName   = $myTeamEmp['webEmpLName'];
                      $webEmpTelc    = $myTeamEmp['webEmpTelc'];
                      $webColor      = $myTeamEmp['webColor'];

                      $strName = (in_array('TEAM', $_SESSION['perm']) ? "<a href='employee.php?emp=$valEmp'>$webEmpFName $webEmpLName</a>" : "$webEmpFName $webEmpLName");

                      if ($valIsTrn) { $strName = "(" . $strName . ")"; }
                      if ($valDays < 30) { $strName = "<i>" . $strName . "</i>"; }

                      echo "<tr class='content" . ($webTitleShort == "R"? " regional" : "") . "' style='background-color:#$webColor;'><td>$webTitleShort:</td><td" . ($valIsSup == 1 ? " style='font-weight:bold;'" : "") . ">";
                      echo $strName;
                      echo "</td><td class='tel'>$webEmpTelc</td></tr>";

                      
                    }  // foreach for my Emp
                    if (!empty($webTeamInfo))   { echo "<tr class='heading'><td colspan='3'>$webTeamInfo</td></tr>"; }
                    if (!empty($webTruckInfo))  { echo "<tr class='heading'><td colspan='3'>Truck $valTruck: $webTruckInfo" . ($valGeoComm ? "" : " Geotab not reporting.") . "</td></tr>"; }
                    if ($webTruckStatus!='3')  { echo "<tr class='heading'><td colspan='3'>Truck $valTruck status: <b>$webTruckStatusDsca </b></td></tr>"; }
                    if (!empty($webOfficeInfo)) { echo "<tr class='heading' besso><td colspan='3'>$webOfficeCity: $webOfficeInfo</td></tr>"; }
                  }
                    
                  echo "</table>";

                } // foreach for my Team
                } // isset
                echo "</div>"; // col

                if ( $i%4 == 3 ) echo "</div>";
              } // foreach for region
            //echo "</div>"; // body_fixed
            } // not empty if
            ?>


            <div class="container" id="contacts">
              <h2 class="text-center border-top p-2 mt-5">Corporate Directory</h2>

              <div class="row">

                  <!-- for template -->
                  <div class="col-3" v-for="(contact, title) in contacts">

                    <div>
                      <h5 class="text-center border border-warning p-2">{{ title }}</h5>
                      <table class="table">
                        <tr v-for="emp in contact">
                          <td>{{ emp.emp_name }}</td>
                          <td>
                            <div v-if="emp.emp_telp">O: {{ emp.emp_telp }}<div> 
                            <div v-if="emp.emp_telc">C: {{ emp.emp_telc }}<div>
                          </td>
                        </tr>
                      </table>
                    </div>

                    
                  </div>
                  <!-- end for template -->

                  <div class="w-100"></div>
                  
              </div>
            </div>

            <script>
              new Vue({
                el: "#contacts",
                data: {
                  contactsPerRow: 4,
                  contacts: JSON.parse('<?php echo $teamContactList ?>')
                }
              })

            </script>
            
            
            

            <div class='foot_fixed no-print'>
               
               <div class='foot_content'>
                  <table style='width:100%'>
                     <tr>
                        <td style='width:40%;text-align:center;'>
                           <p>There are <span class='highlighted'>67</span> users online &#183; You are visitor <span class='highlighted'>938374</span> &#183; Page loaded in <span class='highlighted'>0.088</span> seconds by <span class='highlighted'>NAP-WEB-003</span> &#183; All times are <span class='highlighted'>Central</span> time zone &#183; &copy;2021 Aftermath Services LLC</p>
                        </td>
                     </tr>
                  </table>
               </div>
            </div>
         




            <!-- END OF Mockup -->
          </div>
        </main>
      </div>
    </div>

    <script src="http://127.0.0.1:85/assets/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/feather-icons@4.28.0/dist/feather.min.js" integrity="sha384-uO3SXW5IuS1ZpFPKugNNWqTZRRglnUJK6UAZ/gxOX80nxEkN9NcGZTftn6RzhGWE" crossorigin="anonymous"></script>
    
<?php echo $footer ?>