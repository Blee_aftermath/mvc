<?php echo $header; ?>
    <div class="container-fluid">
      <div class="row">
        
        <main role="main" class="col-md-12 ml-sm-auto col-lg-112 pt-3 px-4">
          
  <?php        
  $frmName = "frmLogin";
  $action  = '/auth/login';
  $display = 'login';

  echo "<form method='post' name='$frmName' action='$action' autocomplete='off'>";

  echo "<div class='body_content'>";

  switch ($display) {

  case "login":

    echo "<table class='info'>";
    echo   "<tr class='content'>";
    echo   "<td class='l'>Username:</td><td><input class='inp20' type='text' name='inpUsername' maxlength='$maxUser' autofocus='autofocus' value='blee'/></td>";
    echo   "</tr>";

    echo   "<tr class='content'>";
    echo   "<td class='l'>Password:</td><td><input class='inp20' type='password' name='inpPassword'  maxlength='$maxPass' value='Computer22' /></td>";
    echo   "</tr>";

    echo   "<tr class='content'><td class='l'>6 Characters:</td>";
    echo   "<td><input class='inp20' type='text' name='inpCaptcha' maxlength='6' />"; if (isset($errCaptcha)) { echo "&nbsp;&nbsp;$errCaptcha"; }
    echo   "</td></tr>";

    echo   "<tr class='content'>";
    echo   "<td class='l'>Can't read it?<br /><a href='#' onclick=\"document.getElementById('captcha').src = '/auth/login/showCaptcha?' + Math.random(); return false\">Load a new Image</a></td>";
    echo   "<td class='l'><p><img id='captcha' src='/auth/login/showCaptcha' alt='CAPTCHA Image' /></p></td>";
    echo   "</tr>";

    echo   "<tr class='content'>";
    echo   "<td>&nbsp;</td><td class='l'><input type='checkbox' name='chkRememberMe' /> Remember me &nbsp;<input type='submit' class='inp08' name='subLogin' value='Login' /></td>";
    echo   "</tr>";

    echo   "<tr class='content'>";
    echo   "<td>&nbsp;</td><td class='l'><a href='login.php?display=help'>Login Help</a></td>";
    echo   "</tr>";

    echo "</table>";

  break;
  case "help":

    echo "<h3>Login Help</h3>";
    echo "<p><a href='login.php?display=username'>Recover My Username</a></p>";
    echo "<p><a href='login.php?display=password'>Reset My Password</a></p>";

  break;
  case "username":

    echo "<h3>Recover My Username</h3>";
    echo "<p>Enter your email address.  If the address is a valid address on this sytem,<br />your username will be emailed to you.</p>";
    echo "<p>&nbsp;</p>";
    echo "<p>Email: <input type='text' name='inpEmail' maxlength='$maxEmail' />&nbsp;<input type='submit' class='inp04' name='subUsername' value='Go!' /></p>";

  break;
  case "password":

    echo "<h3>Reset My Password</h3>";
    echo "<p>Enter your email address.  If the address is a valid address on this sytem,<br />a link to reset your password will be emailed to you.</p>";
    echo "<p>&nbsp;</p>";
    echo "<p>Email: <input type='text' name='inpEmail' maxlength='$maxEmail' />&nbsp;<input type='submit' class='inp04' name='subPassword' value='Go!' /></p>";

  break;
  } // switch ($display)

  echo "</div>"; // body_content
  echo "</form>";

?>

        </main>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="https://getbootstrap.com/docs/4.0/assets/js/vendor/popper.min.js"></script>
    <script src="https://getbootstrap.com/docs/4.0/dist/js/bootstrap.min.js"></script>

<?php echo $footer; ?>