import { default as formModal } from './modules/ui/formModal.js'

/**
 * Class for Insurance Contacts admin page
 * @author Scott Kiehn
 */

export class PartnerIns {
  /**
   * Creates a new PartnerIns class
   */
  constructor(opts = {}) {
    this.dom  = {}

    this.dom.contacts = document.querySelector('#insContact')
    this.dom.saveMdl  = document.querySelector('#modal-inscontact-save-form')
    this.dom.saveBtn  = document.querySelector('#modal-save-button')

    this.dom.contacts.addEventListener('click', (ev) => {
      if (ev.target.matches('[data-opts]'))
        formModal(ev.target)
    })

    this.dom.saveBtn.addEventListener('click', (ev) => this.savePartner(ev.target.closest('form')))
  }
 
  /**
   * Save insurance contact information
   * @param {Element} form
   */
  savePartner = async (form) => {

    const inpData = {}

    let errors = 0

    for (const inp of form.querySelectorAll('[name]')) {
      inpData[inp.name] = inp.value
      if (inp.classList.contains('req')) if (inp.value.length == 0) errors++
    }

    if (errors) return this.dom.saveMdl.querySelector('.error').innerHTML = `<p>Values for the required items were not provided.</p>`;

    const post = await fetch('partnerInsPRG.php', {
      method: 'post',
      headers: { 'Content-Type': 'application/json;charset=utf-8' },
      body: JSON.stringify(inpData)
    })

    const res = await post.json()

    if ( res.ok ) {
      this.dom.saveMdl.querySelector('.error').innerHTML = '';
      this.dom.saveMdl.querySelector('.message').innerHTML = `<p>${res.msg}</p>`;
      setTimeout(() => {
        location.assign(res.ref)
        if (res.ref.match(/#/)) location.reload()
      }, 750)
    } else {
      const error = []

      if (typeof(res.msg) == 'object') {
        for (const m of res.msg) error.push(m.msg)
      } else {
        error[0] = res.msg
      }

      this.dom.saveMdl.querySelector('.error').innerHTML = `<p>${error.join('<br />')}</p>`;
    }
  }
}
