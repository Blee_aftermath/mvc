/**
 * Allows for a scheduled queue of async functions to fire at once, and waits for every
 * result to be returned.
 * @author Jake Cirino
 */
export class AsyncScheduler{
  /**
   * Creates a new async controller
   * @param {Function} callback Callback on success
   */
  constructor(callback){
    this.callback = callback
    this.queue = [] //queue of async functions
    this.completed = 0 //the amount of items that have completed execution 
  }

  /**
   * Adds a function to the queue
   * @param {Function} func
   */
  add(func){
    this.queue.push(func)
  }

  /**
   * Executes the queue of functions
   */
  execute(){
    for (const key in this.queue) {
      if (this.queue.hasOwnProperty(key)) {
        const element = this.queue[key];
        element(this.complete.bind(this))
      }
    }
  }

  /**
   * Called by the async function when execution is complete
   */
  complete(){
    this.completed += 1

    if(this.completed == this.queue.length) this.callback()
  }

}