<?php

echo "<script type='module'>\n";
echo "import {default as formModal} from './scripts/modules/ui/formModal.js?$scriptRandom';\n";
echo "window.jsClkEvt['formModal'] = formModal;\n";
echo "</script>\n";

echo "<div class='modal' id='modal-service-addr'>\n";
echo "<form action='serviceAddrPRG.php' method='post'>";
echo "<table class='info'>";

//=== FORM CONTENT ===

echo "<tr class='heading'><th colspan='2'>Service Address</th></tr>";

$inpField = "hidJobID";
$inpLabel = "Job ID";
$defClass = "inp06 reo";
echo "<tr style='display:none' class='content'>";
echo "<td><label for='$inpField'>$inpLabel:</label></td>";
echo "<td class='r'><input class='$defClass' type='text' name='$inpField' id='$inpField' readonly /></td>";
echo "</tr>\n";

$sqlJobType = "SELECT job_type_id, job_type_dsca FROM job_type";
$resJobType = $mysqli->query($sqlJobType) or die('Get job type failed');
$inpField = "selJobType";
$inpLabel = "Job Type:";
$defClass = "req";
echo "<tr class='content'><td class='l'>$inpLabel</td><td class='r'>";
echo "<select class='$defClass' name='$inpField' id='$inpField'>";
echo "<option value='' selected='selected'>--- Select ---</option>";
while ($myJobType = $resJobType->fetch_assoc()) {
  echo "<option value='" . $myJobType['job_type_id'] . "'";
  echo ">" . $myJobType['job_type_dsca'] . "</option>";
}
echo "</select>";

$sqlPropType = "SELECT prop_type_id, prop_type_dsca FROM prop_type";
$resPropType = $mysqli->query($sqlPropType) or die('Get property type failed');
$inpField = "selPropType";
$inpLabel = "Property Type:";
$defClass = "req";
echo "<tr class='content'><td class='l'>$inpLabel</td><td class='r'>";
echo "<select class='$defClass' name='$inpField' id='$inpField'>";
echo "<option value='' selected='selected'>--- Select ---</option>";
while ($myPropType = $resPropType->fetch_assoc()) {
  echo "<option value='" . $myPropType['prop_type_id'] . "'";
  echo ">" . $myPropType['prop_type_dsca'] . "</option>";
}
echo "</select>";

$inpField = "inpLine1";
$inpLabel = "Address Line 1:";
$defClass = "req";
echo "<tr class='content'><td class='l'>$inpLabel</td><td class='r'>";
echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' maxlength='$maxAddr' />";
echo "<span id='spanGetPropName'></span>";
echo "</td></tr>";

$inpField = "inpLine2";
$inpLabel = "Address Line 2:";
$defClass = "opt";
echo "<tr class='content'><td class='l'>$inpLabel</td><td class='r'>";
echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' maxlength='$maxAddr' />";
echo "</td></tr>";

$inpField = "inpCity";
$inpLabel = "City:";
$defClass = "req";
echo "<tr class='content'><td class='l'>$inpLabel</td><td class='r'>";
echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' maxlength='$maxAddr' />";
echo "</td></tr>";

$sqlState = "SELECT * FROM state ORDER BY st_name";
$resState = $mysqli->query($sqlState) or die('Get state failed');
$inpField = "selState";
$inpLabel = "State / Zip";
$defClass = "inp12 req";
echo "<tr class='content'><td class='l'>$inpLabel</td><td class='r'>";
echo "<select class='$defClass' name='$inpField' id='$inpField'>";
echo "<option value='' selected='selected'>--- Select ---</option>";
while ($myState = $resState->fetch_assoc()) {
  echo "<option value='" . $myState['st_id'] . "'";
  echo ">" . $myState['st_name'] . "</option>";
}
echo "</select>";

$inpField = "inpZip";
$defClass = "inp08 " . "req";
echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' maxlength='$maxZip' />";
echo "</td></tr>";

$inpField = "inpYrBuilt";
$inpLabel = "Year Built:";
$defClass = "inp04 opt";
echo "<tr class='content'><td class='l'>$inpLabel</td><td class='r'>";
echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' maxlength='$maxAddr' />";
echo "</td></tr>";

echo "<tr class='heading'><th colspan='2'>Service Info</th></tr>";

$inpField = "inpDecedent";
$inpLabel = "Injured / Victim:";
$defClass = "opt";
echo "<tr class='content'><td class='l'>$inpLabel</td><td class='r'>";
echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' maxlength='$maxAddr' />";
echo "</td></tr>";

$inpField = "inpCause";
$inpLabel = "Cause of Death:";
$defClass = "opt";
echo "<tr class='content'><td class='l'>$inpLabel</td><td class='r'>";
echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' maxlength='$maxAddr' />";
echo "</td></tr>";

$inpField = "inpUnattDays";
$inpLabel = "Unatt. Days:";
$defClass = "inp04 opt";
echo "<tr class='content'><td class='l'>$inpLabel</td><td class='r'>";
echo "<input class='$defClass' maxlength='4' type='text' name='$inpField' id='$inpField' maxlength='$maxAddr' />";
echo "</td></tr>";

$inpField = "chkIsPolice";
$inpLabel = "Police Involved:";
echo "<tr class='content'><td>$inpLabel</td><td class='l' colspan='2'>";
echo "<input type='checkbox' name='$inpField' id='$inpField'/>";
echo "</td></tr>";

$inpField = "inpPoliceName";
$inpLabel = "Police Contact:";
$defId = 'trPoliceName';
$defClass = "opt";
echo "<tr class='content' name='$defId' id='$defId'><td class='l'>$inpLabel</td><td class='r'>";
echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' maxlength='$maxAddr' />";
echo "</td></tr>";

$inpField = "inpPoliceDept";
$inpLabel = "Police Dept:";
$defId = 'trPoliceDept';
$defClass = "opt";
echo "<tr class='content' name='$defId' id='$defId'><td class='l'>$inpLabel</td><td class='r'>";
echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' maxlength='$maxAddr' />";
echo "</td></tr>";

$inpField = "chkIsBusiness";
$inpLabel = "Business:";
echo "<tr class='content'><td>$inpLabel</td><td class='l' colspan='2'>";
echo "<input type='checkbox' name='$inpField' id='$inpField'/>";
echo "</td></tr>";

$inpField = "inpCompanyName";
$inpLabel = "Business Name:";
$defId = 'trCompanyName';
$defClass = "opt";
echo "<tr class='content' name='$defId' id='$defId'><td class='l'>$inpLabel</td><td class='r'>";
echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' maxlength='$maxAddr' />";
echo "</td></tr>";

$inpField = "chkIsNonprofit";
$inpLabel = "Tax Exempt:";
echo "<tr class='content'><td>$inpLabel</td><td class='l' colspan='2'>";
echo "<input type='checkbox' name='$inpField' id='$inpField'/>";
echo "</td></tr>";

echo "<tr class='heading'><th colspan='2'></th></tr>";

$arrServiceAddrButtons[] = "<input class='inp12' id='subSave' type='submit' name='subSave' value='Save' />";
$arrServiceAddrButtons[] = "<input class='inp12 modal-close' style='float: none' type='button' value='Cancel' />";
echo "<tr class='content'><td></td><td>" . implode("", $arrServiceAddrButtons) . "</td></tr>";

echo '<script language="JavaScript">';

echo 'function checkPoliceInvolved() {';
echo '  let contName = document.getElementById("trPoliceName");';
echo '  let dept = document.getElementById("trPoliceDept");';
echo '  contName.style.display = chkPoliceInvolved.checked ? "table-row" : "none";';
echo '  dept.style.display = chkPoliceInvolved.checked ? "table-row" : "none";';
echo '}';

echo 'var chkPoliceInvolved = document.getElementById("chkIsPolice");';
echo 'chkPoliceInvolved.addEventListener("change", checkPoliceInvolved);';

echo 'function checkCompany() {';
echo '  let compName = document.getElementById("trCompanyName");';
echo '  compName.style.display = chkIsBusiness.checked ? "table-row" : "none";';
echo '}';

echo 'var chkIsBusiness = document.getElementById("chkIsBusiness");';
echo 'chkIsBusiness.addEventListener("change", checkCompany);';

echo 'function checkAll() {';
echo '  checkPoliceInvolved();';
echo '  checkCompany();';
echo '}';

echo 'checkAll()';
echo '</script>';

echo "</table>\n";
echo "</form>\n";
echo "</div>\n";
