
var arrSkuOb   = []; // Array of SKU objects
var arrSkuID   = []; // Array of SKU IDs
var arrSkuEr   = []; // Array of errored fields to tag in red

function fnToggleAggrReq() {
  const isAggr  = document.getElementById('chkIsAggr'),
        skuPart = document.getElementById('inpSkuPart');

  skuPart.classList.remove('opt');
  skuPart.classList.add('req');

  if (isAggr.checked) {
    skuPart.classList.remove('req');
    skuPart.classList.add('opt');
  }
}

function fnToggleConsReq() {
  const isCons = document.getElementById('chkIsCons'),
        uomJif = document.getElementById('selUOMJif'),
        qtyJif = document.getElementById('inpQtyPerJif');

  uomJif.classList.add('opt');
  uomJif.classList.remove('req');
  qtyJif.classList.add('opt');
  qtyJif.classList.remove('req');

  if (isCons.checked) {
    uomJif.classList.add('req');
    qtyJif.classList.add('req');
  }
}

function fnAddSku() {
  document.getElementById("spanSkuHead").innerHTML     = "Add SKU";
  document.getElementById("hidSkuID").disabled         = true;
  document.getElementById("tblSku").style.display      = "block";
  document.getElementById("subAddSku").style.display   = "inline";
  document.getElementById("subChgSku").style.display   = "none";
  document.getElementById("butConSku").style.display   = "none";
  document.getElementById("myModalSku").style.display  = "block";
}

function fnChgSku(id) {
  skuIndex = arrSkuID.indexOf(id);
  document.getElementById("spanSkuHead").innerHTML     = "Edit SKU";
  document.getElementById("hidSkuID").disabled         = false;
  document.getElementById("tblSku").style.display      = "block";
  document.getElementById("subAddSku").style.display   = "none";
  document.getElementById("subChgSku").style.display   = "inline";
  document.getElementById("butConSku").style.display   = "inline";
  document.getElementById("myModalSku").style.display  = "block";
  fnFillSku(skuIndex);
}

function fnFillSku(skuIndex) {
  document.getElementById("hidSkuID").value            = arrSkuID[skuIndex];
  document.getElementById("inpSkuNum").value           = arrSkuOb[skuIndex].num;
  document.getElementById("selUOM").value              = arrSkuOb[skuIndex].uom_code;
  document.getElementById("inpQtyPer").value           = arrSkuOb[skuIndex].qty_per;
  document.getElementById("inpSkuNama").value          = arrSkuOb[skuIndex].nama;
  document.getElementById("inpSkuPart").value          = arrSkuOb[skuIndex].part;
  document.getElementById("inpSkuUPC").value           = arrSkuOb[skuIndex].upc;
  document.getElementById("chkIsAggr").checked         = (arrSkuOb[skuIndex].is_aggr == 1 ? true : false);
  document.getElementById("chkIsCons").checked         = (arrSkuOb[skuIndex].is_consumable == 1 ? true : false);
  document.getElementById("selUOMJif").value           = arrSkuOb[skuIndex].uom_code_jif;
  document.getElementById("inpQtyPerJif").value        = arrSkuOb[skuIndex].uom_qty_per_jif;
  fnToggleAggrReq()
  fnToggleConsReq();
}

function fnConfirmDelSku() {
  strConfirm  = "<div class='content alert'>";
  strConfirm += "Delete this SKU?&nbsp;&nbsp;";
  strConfirm += "<input class='inp08' type='submit' name='subDelSkuY' value='Yes' />";
  strConfirm += "<input class='inp08' type='button' name='butDelSkuN' value='No' onclick='fnDelSkuN()' />";
  strConfirm += "</div>";
  document.getElementById("spanSkuConfirm").innerHTML = strConfirm;
}

function fnDelSkuN() {
  document.getElementById("spanSkuConfirm").innerHTML = "";
}

function fnCloseModalSku() {
  document.getElementById("hidSkuID").value            = "";
  document.getElementById("inpSkuNum").value           = "";
  document.getElementById("selUOM").value              = "";
  document.getElementById("inpQtyPer").value           = "";
  document.getElementById("inpSkuNama").value          = "";
  document.getElementById("inpSkuPart").value          = "";
  document.getElementById("inpSkuUPC").value           = "";
  document.getElementById("chkIsAggr").checked         = false;
  document.getElementById("chkIsCons").checked         = false;
  document.getElementById("selUOMJif").value           = "";
  document.getElementById("inpQtyPerJif").value        = "";
  document.getElementById("spanSkuHead").innerHTML     = "";
  document.getElementById("spanSkuMessages").innerHTML = "";
  document.getElementById("spanSkuConfirm").innerHTML  = "";
  document.getElementById("myModalSku").style.display  = "none";
  for(var i=0; i<arrSkuEr.length; i++) {
    document.getElementById(arrSkuEr[i]).classList.remove("err");
  }
  fnToggleAggrReq()
  fnToggleConsReq();
}
