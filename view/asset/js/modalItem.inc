<?php

// ==================================================================================================== //
// === MODAL === //
// ==================================================================================================== //

  echo "<div class='modal' id='myModalItem'>";
  echo "<div class='fixed'>";
  echo "<div class='body_stripe'></div>";

  echo "<div class='head'>";
  echo "<div class='content'>";

  echo "<span id='spanItemHead'></span>";
  echo "<span style='float:right;'><a href='javascript:fnCloseModalItem();'><img class='icon16' src='icons/no16.png' alt='Close' title='Close' /></a></span>";

  echo "</div>"; // content
  echo "</div>"; // head

  echo "<span id='spanItemMessages'>";
  fnModalMessages();
  echo "</span>";

  echo "<div class='body'>";
  echo "\n<form method='post' name='frmModalItem' action='modalPRG.php'>";
  echo "<div class='content'>";

  echo "<table class='info' id='tblItem'>";

  $inpField = "reoItemID";
  $inpLabel = "Item ID";
  $defClass = "inp06 reo";
  echo "<tr class='content' id='rowItemID'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' readonly />";
  echo "</td></tr>";

  $inpField = "inpItemName";
  $inpLabel = "Name";
  $defClass = "inp24 req";
  $inpHelp  = "Enter a short name for this Item.";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' maxlength='$maxName' list='off' />";
  echo "&nbsp;<img class='icon16' title='$inpHelp' alt='Help' src='icons/question16.png' />";
  echo "</td></tr>";

  $sqlSelTyp = "SELECT ityp_code, ityp_dsca FROM item_type ORDER BY ityp_dsca";
  $resSelTyp = $mysqli->query($sqlSelTyp);

  $inpField = "selTyp";
  $inpLabel = "Type";
  $defClass = "req";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<select class='$defClass' name='$inpField' id='$inpField'>";
  echo "<option value='' selected='selected'>--- Select ---</option>";
  while ($mySelTyp = $resSelTyp->fetch_assoc()) {
    echo "<option value='" . $mySelTyp['ityp_code'] . "'>" . prepareFrm($mySelTyp['ityp_dsca']) . "</option>";
  }
  echo "</select>";
  echo "</td></tr>";

  $sqlSelCat = "SELECT icat_code, icat_dsca FROM item_cat ORDER BY icat_code";
  $resSelCat = $mysqli->query($sqlSelCat);

  $inpField = "selCat";
  $inpLabel = "Category";
  $defClass = "req";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<select class='$defClass' name='$inpField' id='$inpField'>";
  echo "<option value='' selected='selected'>--- Select ---</option>";
  while ($mySelCat = $resSelCat->fetch_assoc()) {
    echo "<option value='" . $mySelCat['icat_code'] . "'>" . prepareFrm($mySelCat['icat_dsca']) . "</option>";
  }
  echo "</select>";
  echo "</td></tr>";

  $sqlSelVendor = "SELECT vendor_code, vendor_dsca FROM vendor ORDER BY vendor_code";
  $resSelVendor = $mysqli->query($sqlSelVendor);

  $inpField = "selVendor";
  $inpLabel = "Vendor";
  $defClass = "inp12 req";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<select class='$defClass' name='$inpField' id='$inpField'>";
  echo "<option value='' selected='selected'>--- Select ---</option>";
  while ($mySelVendor = $resSelVendor->fetch_assoc()) {
    echo "<option value='" . $mySelVendor['vendor_code'] . "'>" . prepareFrm($mySelVendor['vendor_dsca']) . "</option>";
  }
  echo "</select>";
  echo "</td></tr>";

  $sqlSelUOM = "SELECT uom_code, uom_dsca FROM uom ORDER BY uom_code";
  $resSelUOM = $mysqli->query($sqlSelUOM);

  $inpField = "selItemUOM";
  $inpLabel = "Unit of Measure";
  $defClass = "inp12 req";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<select class='$defClass' name='$inpField' id='$inpField'>";
  echo "<option value='' selected='selected'>--- Select ---</option>";
  while ($mySelUOM = $resSelUOM->fetch_assoc()) {
    echo "<option value='" . $mySelUOM['uom_code'] . "'>" . prepareFrm($mySelUOM['uom_dsca']) . "</option>";
  }
  echo "</select>";
  echo "</td></tr>";

  $inpField = "inpItemQtyPer";
  $inpLabel = "Qty Per";
  $defClass = "inp08 req";
  $inpHelp  = "Quantity per unit when ordering this item.";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' maxlength='" . strlen($maxSmallintUn) . "' list='off' />";
  echo "&nbsp;<img class='icon16' title='$inpHelp' alt='Help' src='icons/question16.png' />";
  echo "</td></tr>";

  $inpField = "inpItemInfo";
  $inpLabel = "Additional<br />Information";
  $defClass = "opt";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<textarea class='$defClass' name='$inpField' id='$inpField' rows='6' cols='48'></textarea>";
  echo "</td></tr>";

  $inpField = "chkItemIsAct";
  $inpLabel = "Active";
  $inpHelp  = "An active Item (checked) may be used to create SKUs.";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='l' colspan='2'>";
  echo "<input type='checkbox' name='$inpField' id='$inpField' />";
  echo "&nbsp;<img class='icon16' title='$inpHelp' alt='Help' src='icons/question16.png' />";
  echo "</td></tr>";

  $arrButtonRowItem[] = "<input class='inp12' type='submit' name='subAddItem' id='subAddItem' value='Add' />";
  $arrButtonRowItem[] = "<input class='inp12' type='submit' name='subChgItem' id='subChgItem' value='Save' />";
  $arrButtonRowItem[] = "<input class='inp12' type='button' name='butConItem' id='butConItem' value='Delete' onclick='fnConfirmDelItem();' />";
  $arrButtonRowItem[] = "<input class='inp12' type='button' name='butCanItem' id='butCanItem' value='Cancel' onclick='fnCloseModalItem();' />";

  echo "<tr class='content'><td></td><td>" . implode("", $arrButtonRowItem) . "</td></tr>";

  echo "</table>";

  echo "</div>"; // content
  echo "<span id='spanItemConfirm'></span>";
  echo "</form>";
  echo "</div>"; // body

  echo "<div class='body_stripe'></div>";
  echo "</div>"; // fixed
  echo "</div>"; // modal

// ==================================================================================================== //
// === LOAD === //
// ==================================================================================================== //

  if (isset($_SESSION['inpValue']['subAddItem']) || isset($_SESSION['inpValue']['subChgItem'])) {

    echo "\n<script language=\"JavaScript\">";
    echo "\n<!-- Hide the script from old browsers --";

    echo "\nvar objItem = {};"; // Create a new object to hold the posted values.

    if (isset($_SESSION['inpValue']['reoItemID'])) {
      echo "\narrItemID.push(" . $_SESSION['inpValue']['reoItemID'] . ");";
      echo "\nobjItem.id     = \"" . $_SESSION['inpValue']['reoItemID'] . "\";";
    }

    echo "\nobjItem.typ_code    = \"" . prepareJS($_SESSION['inpValue']['selTyp'])             . "\";";
    echo "\nobjItem.cat_code    = \"" . prepareJS($_SESSION['inpValue']['selCat'])             . "\";";
    echo "\nobjItem.vendor_code = \"" . prepareJS($_SESSION['inpValue']['selVendor'])          . "\";";
    echo "\nobjItem.uom_code    = \"" . prepareJS($_SESSION['inpValue']['selItemUOM'])         . "\";";
    echo "\nobjItem.name        = \"" . prepareJS($_SESSION['inpValue']['inpItemName'])        . "\";";
    echo "\nobjItem.info        = \"" . prepareJS($_SESSION['inpValue']['inpItemInfo'])        . "\";";
    echo "\nobjItem.qty_per     = \"" . $_SESSION['inpValue']['inpItemQtyPer']                 . "\";";
    echo "\nobjItem.is_act      = \"" . (isset($_SESSION['inpValue']['chkItemIsAct']) ? 1 : 0) . "\";";

    echo "\narrItemOb.push(objItem);"; // Tack this onto the end of the array of Item objects.

    foreach($_SESSION['errField'] AS $field) {

      echo "\narrItemEr.push(\"" . $field    . "\");";
      echo "\ndocument.getElementById(\"" . $field . "\").classList.add(\"err\");";

    }

    // Show the modal dialog box

    if (isset($_SESSION['inpValue']['subAddItem'])) echo "\nfnAddItem();";
    if (isset($_SESSION['inpValue']['subChgItem'])) echo "\nfnChgItem(" . $_SESSION['inpValue']['reoItemID'] . ");";
    echo "\nfnFillItem(arrItemOb.length-1);";

    echo "// --End Hiding Here -->\n";
    echo "\n</script>\n";

  }

?>