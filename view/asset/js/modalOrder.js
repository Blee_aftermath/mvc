
var arrOrderOb   = []; // Array of Order objects
var arrOrderID   = []; // Array of Order IDs
var arrOrderEr   = []; // Array of errored fields to tag in red

function fnAddOrder() {
  document.getElementById("spanOrderHead").innerHTML     = "Add Order";
  document.getElementById("rowOrderID").hidden           = true;
  document.getElementById("reoOrderID").disabled         = true;
  document.getElementById("subAddOrder").style.display   = "inline";
  document.getElementById("subChgOrder").style.display   = "none";
  document.getElementById("butConOrder").style.display   = "none";
  document.getElementById("myModalOrder").style.display  = "block";
}

function fnChgOrder(id) {
  jsIndex = arrOrderID.indexOf(id);
  document.getElementById("spanOrderHead").innerHTML     = "Edit Order";
  document.getElementById("rowOrderID").hidden           = false;
  document.getElementById("reoOrderID").disabled         = false;
  document.getElementById("subAddOrder").style.display   = "none";
  document.getElementById("subChgOrder").style.display   = "inline";
  document.getElementById("butConOrder").style.display   = "inline";
  document.getElementById("myModalOrder").style.display  = "block";
  fnFillOrder(jsIndex);
}

function fnFillOrder(jsIndex) {
  document.getElementById("reoOrderID").value            = arrOrderID[jsIndex];
  document.getElementById("selOrderOffice").value        = arrOrderOb[jsIndex].office;
  document.getElementById("selOrderITyp").value          = arrOrderOb[jsIndex].ityp;
  document.getElementById("selArrShipTo").value          = arrOrderOb[jsIndex].shipto;
  document.getElementById("inpOrderInfo").value          = arrOrderOb[jsIndex].info;
}

function fnConfirmDelOrder() {
  strConfirm  = "<div class='content alert'>";
  strConfirm += "Delete this Order?&nbsp;&nbsp;";
  strConfirm += "<input class='inp08' type='submit' name='subDelOrderY' value='Yes' />";
  strConfirm += "<input class='inp08' type='button' name='butDelOrderN' value='No' onclick='fnDelOrderN()' />";
  strConfirm += "</div>";
  document.getElementById("spanOrderConfirm").innerHTML = strConfirm;
}

function fnDelOrderN() {
  document.getElementById("spanOrderConfirm").innerHTML = "";
}

function fnCloseModalOrder() {
  document.getElementById("reoOrderID").value            = "";
  document.getElementById("selOrderOffice").value        = "";
  document.getElementById("selOrderITyp").value          = "";
  document.getElementById("selArrShipTo").value          = "";
  document.getElementById("inpOrderInfo").value          = "";
  document.getElementById("myModalOrder").style.display  = "none";
  for(var i=0; i<arrOrderEr.length; i++) {
    document.getElementById(arrOrderEr[i]).classList.remove("err");
  }
}