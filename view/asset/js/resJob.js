import { default as formModal } from './modules/ui/formModal.js'
import { default as note } from './modules/lib/note.js'

/**
 * Class for Restoration Jobs admin page
 * @author Scott Kiehn
 */

export class ResJob {
  /**
   * Creates a new ResJob class
   */
  constructor() {
    this.dom = {}
  
    // Parent of Restoration Job DOM
    this.dom.base = document.querySelector('#resJob')

    // DOM for new customer selection
    this.dom.newJob = document.querySelector('#newJob')

    this.dom.base.addEventListener('click', (ev) => {

      if (ev.target.matches('#add-icon')) {
        if (this.dom.newJob.value.match(/^\d+$/)) {
          this.addJob(ev.target, this.dom.newJob.value)
        } else {
          note({class: 'error', text: 'Please provide a proper Job number.'})
        }
      }

      if (ev.target.matches('[data-opts]'))
        formModal(ev.target)
    })

    for (const save of document.querySelectorAll('.save-form'))
      save.addEventListener('click', () => this.saveForm(save.closest('form')))
  }

  /**
   * Add a restoration job
   * @param {Element} el
   * @param {Int} id
   */

  addJob = async (el, id) => {
    const inpData = {
      action: 'createResJob',
      jobID:  id
    }

    const post = await fetch('restorationPRG.php', {
      method: 'post',
      headers: { 'Content-Type': 'application/json;charset=utf-8' },
      body: JSON.stringify(inpData)
    })

    const res = await post.json()

    if ( res.ok ) {
      location.assign(`/resJob.php?job=${res.jobID}`)
    } else {
      note({class: 'error', text: 'An error has occurred.'})
    }
  }

  /**
   * Save modal forms
   * @param {Element} form
   */

  saveForm = async (form) => {

    const inpData = {},
      modal = form.closest('.modal')

    let errors = 0

    for (const inp of form.querySelectorAll('[name]')) {
      if (inp.type == 'checkbox') {
        inpData[inp.name] = inp.checked ? 1 : 0
      } else {
        inpData[inp.name] = inp.value
      }
      if (inp.classList.contains('req')) if (inp.value.length == 0) errors++
    }

    if (errors) return modal.querySelector('.error').innerHTML = '<p>Values for the required items were not provided.</p>';

    const post = await fetch('resJobPRG.php', {
      method: 'post',
      headers: { 'Content-Type': 'application/json;charset=utf-8' },
      body: JSON.stringify(inpData)
    })

    const res = await post.json()

    if ( res.ok ) {
      modal.querySelector('.error').innerHTML = '';
      modal.querySelector('.message').innerHTML = `<p>${res.msg}</p>`;
      setTimeout(() => {
        location.assign(res.referer)
        if (res.referer.match(/#/)) location.reload()
      }, 750)
    } else {
      const error = []

      if (typeof(res.msg) == 'object') {
        for (const m of res.msg) error.push(m.msg)
      } else {
        error[0] = res.msg
      }

      modal.querySelector('.error').innerHTML = `<p>${error.join('<br />')}</p>`;
    }

  }
}
