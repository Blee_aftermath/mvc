import {Util} from "../util.js"
import {RestorationDetails} from "./restorationDetails.js"
import note from "../modules/lib/note.js"

/**
 * Class that handles the JS section on the restoration job details tab
 * @author Jake Cirino
 */
export class RestorationDetailsJS{
  /**
   * Creates a new restoration details js section
   * @param {RestorationDetails} parent
   * @param {Object} data
   */
  constructor(parent, data) {
    this.parent = parent
    this.data = data
    
    this.baseElement = parent.dom.jsBody
    this.dom = Util.parseDOM(this.baseElement)
    
    //init change events
    let changeFunc = () => {
      this.dom.saveButton.disabled = false
    }
    RestorationDetails.initiateChangeEvents(this.dom, changeFunc)
    RestorationDetails.initiateDatePicker(this.dom.dateAppt, changeFunc)
    RestorationDetails.initiateDatePicker(this.dom.jres_d_quote, changeFunc)
    
    //initialize save button events
    this.dom.saveButton.addEventListener('click', this.save.bind(this))
    this.populate()
  }
  
  /**
   * Populates data from this.data
   */
  populate(){
    try{
      const resData = this.data.resData
      const dateTime = resData.job_dt_appt.split(' ')
      this.dom.dateAppt.value = dateTime[0]
      this.dom.timeAppt.value = Util.parseTime24(dateTime[1])
  
      let onSite = resData.job_yn_onsite != null && resData.job_yn_onsite != 0
      this.dom.job_yn_onsite.checked = onSite
  
      this.dom.jres_amt_quote_est.value = (Number(resData.jres_amt_quote_est)/100).toFixed(2)
      this.dom.jres_amt_quote_act.value = (Number(resData.jres_amt_quote_act)/100).toFixed(2)
  
      this.dom.jres_d_quote.value = resData.jres_d_quote
  
      this.dom.jres_info_js.value = resData.jres_info_js
    }catch(ex){}
    
    //set res section disabled
    this.updateSectionDisabled()
    this.dom.saveButton.disabled = true
  }
  
  /**
   * Sets the restoration time disabled/enabled based on the job status
   */
  updateSectionDisabled(){
    const status = this.data.resData.job_status_code
    const jobDisabled = status == 'JL'
    const jresDisabled = status == 'JN' || status == 'JL'
    
    this.dom.dateAppt.disabled = jobDisabled
    this.dom.timeAppt.disabled = jobDisabled
    this.dom.job_yn_onsite.disabled = jobDisabled
    
    this.dom.jres_amt_quote_est.disabled = jresDisabled
    this.dom.jres_amt_quote_act.disabled = jresDisabled
    this.dom.jres_d_quote.disabled = jresDisabled
    this.dom.jres_info_js.disabled = jresDisabled
  }
  
  /**
   * Saves the job data for this item
   */
  async save(){
    if(this.data.resData.job_status_code == 'JN'){
      //save job data
      this.saveJob()
    }else{
      //save jres data
      if(await this.saveJob(false))
        this.saveResJob()
    }
  }
  
  /**
   * Saves the job data for this section
   * @param {boolean} populate
   * @returns {Promise<boolean>}
   */
  async saveJob(populate = true){
    return new Promise((resolve, reject) => {
      try{
        let data = {
          action: 'updateJobJS',
          jobID: this.data.resData.job_id,
          job_dt_appt: Util.dateToString(Util.parseDate(this.dom.dateAppt.value, this.dom.timeAppt.value)),
          job_yn_onsite: this.dom.job_yn_onsite.checked
        }
    
        fetch('restorationDetailsPRG.php', {
          method: 'post',
          headers: {'Content-Type': 'application/json;charset=utf8'},
          body: JSON.stringify(data)
        })
          .then(res => res.json())
          .then(body => {
            if(body.ok){
              if(populate) this.parent.populate(body.msg, body.comp)
            }else{
              note({class: 'error', text: 'Error inserting data'})
            }
        
            resolve(body.ok)
          })
      }catch(ex){
        note({class: 'error', text: 'Invalid data entered'})
        resolve(false)
      }
    })
  }
  
  /**
   * Saves the restoration job data for this section
   * @param {boolean} populate
   * @returns {Promise<boolean>}
   */
  async saveResJob(populate = true){
    return new Promise((resolve, reject) => {
      try{
        let dQuote = null
        try{
          dQuote = Util.dateToString(Util.parseDate(this.dom.jres_d_quote.value))
        }catch(e){}
    
        let data = {
          action: 'updateResJS',
          jobID: this.data.resData.job_id,
          resJobID: this.data.resData.jres_job,
          jres_amt_quote_est: Number(this.dom.jres_amt_quote_est.value).toFixed(2) * 100,
          jres_amt_quote_act: Number(this.dom.jres_amt_quote_act.value).toFixed(2) * 100,
          jres_d_quote: dQuote,
          jres_info_js: this.dom.jres_info_js.value
        }
    
        fetch('restorationDetailsPRG.php', {
          method: 'post',
          headers: {'Content-Type': 'application/json;charset=utf8'},
          body: JSON.stringify(data)
        })
          .then(res => res.json())
          .then(body => {
            if(body.ok){
              if(populate) this.parent.populate(body.msg, body.comp)
            }else{
              note({class: 'error', text: 'Error inserting data'})
            }
        
            resolve(body.ok)
          })
      }catch(ex){
        note({class: 'error', text: 'Invalid data entered'})
        resolve(false)
      }
    })
  }
  
}