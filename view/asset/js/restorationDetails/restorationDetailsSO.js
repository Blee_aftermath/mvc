import {Util} from "../util.js"
import {RestorationDetails} from "./restorationDetails.js"
import note from "../modules/lib/note.js"

/**
 * Class that handles the SO section on the restoration job details tab
 * @author Jake Cirino
 */
export class RestorationDetailsSO{
  /**
   * Creates a new SO section
   * @param {RestorationDetails} parent
   * @param {Object} data
   */
  constructor(parent, data) {
    this.parent = parent
    this.data = data
    
    this.baseElement = parent.dom.soBody
    this.dom = Util.parseDOM(this.baseElement)
    
    let changeFunc = () => {
      this.dom.saveButton.disabled = false
    }
    RestorationDetails.initiateChangeEvents(this.dom, changeFunc)
    RestorationDetails.initiateDatePicker(this.dom.dateStart, changeFunc)
    RestorationDetails.initiateDatePicker(this.dom.jres_d_work_start_est, changeFunc)
    RestorationDetails.initiateDatePicker(this.dom.jres_d_work_done_est, changeFunc)
    RestorationDetails.initiateDatePicker(this.dom.jres_d_work_start_act, changeFunc)
    RestorationDetails.initiateDatePicker(this.dom.jres_d_work_done_act, changeFunc)
    
    this.dom.saveButton.addEventListener('click', this.save.bind(this))
    this.populate()
  }
  
  /**
   * Populates the form with data from this.data
   */
  populate(){
    try{
      const resData = this.data.resData
      const dateTime = resData.job_dt_start.split(' ')
      this.dom.dateStart.value = dateTime[0]
      this.dom.timeStart.value = Util.parseTime24(dateTime[1])
      
      this.dom.jres_amt_so_ini.value = (Number(resData.jres_amt_so_ini)/100).toFixed(2)
      
      this.dom.jres_d_work_start_est.value = resData.jres_d_work_start_est
      this.dom.jres_d_work_done_est.value = resData.jres_d_work_done_est
      this.dom.jres_d_work_start_act.value = resData.jres_d_work_start_act
      this.dom.jres_d_work_done_act.value = resData.jres_d_work_done_act
      
      this.dom.jres_amt_so_fin.value = (Number(resData.jres_amt_so_fin)/100).toFixed(2)
      
      this.dom.jres_info_so.value = resData.jres_info_so
    }catch(ex){
      //TODO
    }
    
    this.updateSectionDisabled()
    this.dom.saveButton.disabled = true
  }
  
  /**
   * Sets different sections to be enabled based on the job status
   */
  updateSectionDisabled(){
    let status = this.data.resData.job_status_code
    let jobDisabled = status == 'JN' || status == 'JL'
    let jresDisabled = status == 'JN' || status == 'JS' || status == 'JL'
    
    this.dom.dateStart.disabled = jobDisabled
    this.dom.timeStart.disabled = jobDisabled
    
    this.dom.jres_amt_so_ini.disabled = jresDisabled
    this.dom.jres_d_work_start_est.disabled = jresDisabled
    this.dom.jres_d_work_done_est.disabled = jresDisabled
    this.dom.jres_d_work_start_act.disabled = jresDisabled
    this.dom.jres_d_work_done_act.disabled = jresDisabled
    this.dom.jres_amt_so_fin.disabled = jresDisabled
    this.dom.jres_info_so.disabled = jresDisabled
  }
  
  /**
   * Saves the form
   */
  async save(){
    if(this.data.resData.job_status_code == 'JS'){
      await this.saveJob()
    }else{
      if(await this.saveJob(false))
        this.saveResJob()
    }
  }
  
  /**
   * Saves the job section
   * @param {boolean} populate
   * @returns {Promise<boolean>}
   */
  async saveJob(populate = true){
    return new Promise((resolve, reject) => {
      try{
        const data = {
          action: 'updateJobSO',
          jobID: this.data.resData.job_id,
          job_dt_start: Util.dateToString(Util.parseDate(this.dom.dateStart.value, this.dom.timeStart.value))
        }
      
        fetch('restorationDetailsPRG.php', {
          method: 'post',
          headers: {'Content-Type': 'application/json;charset=utf8'},
          body: JSON.stringify(data)
        })
          .then(res => res.json())
          .then(body => {
            if(body.ok){
              if(populate) this.parent.populate(body.msg, body.comp)
            }else{
              note({class: 'error', text: 'Error inserting data'})
            }
          
            resolve(body.ok)
          })
      }catch(ex){
        note({class: 'error', text: 'Invalid data entered'})
        resolve(false)
      }
    })
  }
  
  /**
   * Saves the restoration section of this job
   * @param {boolean} populate
   * @returns {Promise<boolean>}
   */
  async saveResJob(populate = true){
    return new Promise((resolve, reject) => {
      try{
        let soIni = null
        try{
          soIni = Number(this.dom.jres_amt_so_ini.value).toFixed(2) * 100
        }catch(e){}
    
        let startEst = null
        try{
          startEst = Util.dateToString(Util.parseDate(this.dom.jres_d_work_start_est.value))
        }catch(e){}
    
        let doneEst = null
        try{
          doneEst = Util.dateToString(Util.parseDate(this.dom.jres_d_work_done_est.value))
        }catch(e){}
    
        let startAct = null
        try{
          startAct = Util.dateToString(Util.parseDate(this.dom.jres_d_work_start_act.value))
        }catch(e){}
    
        let doneAct = null
        try{
          doneAct = Util.dateToString(Util.parseDate(this.dom.jres_d_work_done_act.value))
        }catch(e){}
    
        let soFin = null
        try{
          soFin = Number(this.dom.jres_amt_so_fin.value).toFixed(2) * 100
        }catch(e){}
    
        const data = {
          action: 'updateResSO',
          jobID: this.data.resData.job_id,
          resJobID: this.data.resData.jres_job,
          jres_amt_so_ini: soIni,
          jres_d_work_start_est: startEst,
          jres_d_work_done_est: doneEst,
          jres_d_work_start_act: startAct,
          jres_d_work_done_act: doneAct,
          jres_amt_so_fin: soFin,
          jres_info_so: this.dom.jres_info_so.value
        }
    
        fetch('restorationDetailsPRG.php', {
          method: 'post',
          headers: {'Content-Type': 'application/json;charset=utf8'},
          body: JSON.stringify(data)
        })
          .then(res => res.json())
          .then(body => {
            if(body.ok){
              if(populate) this.parent.populate(body.msg, body.comp)
            }else{
              note({class: 'error', text: 'Error inserting data'})
            }
        
            resolve(body.ok)
          })
      }catch(ex){
        note({class: 'error', text: 'Invalid data entered'})
          resolve(false)
      }
    })
  }
}