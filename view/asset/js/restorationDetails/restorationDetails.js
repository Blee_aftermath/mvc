/**
 * Details tab on the restoration job page
 * @author Jake Cirino
 */
import {Util} from "../util.js"
import {ResPartnerSelector} from "./resPartnerSelector.js"
import {RestorationDetailsJS} from "./restorationDetailsJS.js"
import {RestorationDetailsSO} from "./restorationDetailsSO.js"
import {RestorationDetailsCO} from "./restorationDetailsCO.js"
import {SelectModal} from "../selectModal.js"
import {YNModal} from "../ynModal.js"
import note from "../modules/lib/note.js"

export class RestorationDetails {
  /**
   * Creates a new restoration details tab controller
   * @param {Object} data
   */
  constructor(data) {
    this.data = data
    console.log(data)
    
    this.baseElement = document.querySelector('[name=tabDetails]')
    this.dom = Util.parseDOM(this.baseElement)
    
    //create section objects
    this.js = new RestorationDetailsJS(this, this.data)
    this.so = new RestorationDetailsSO(this, this.data)
    this.co = new RestorationDetailsCO(this, this.data)
    
    //hide sections if partners is null
    if(this.data.partners.length == 0){
      Util.hide(this.dom.normalSection)
    }
    
    //create partner selector
    this.partnerSelector = new ResPartnerSelector(this.data.partners, window.partnerTemplate)
    this.partnerSelector.setSelectedPartnerID(this.data.resData.jres_partner)
    this.partnerSelector.addChangeListener(this.onPartnerSelected.bind(this))
    
    this.dom.jobLostButton.addEventListener('click', this.setResJobLost.bind(this))
    this.dom.jobRevertButton.addEventListener('click', this.unsetResJobLost.bind(this))
    
    this.baseElement.prepend(this.partnerSelector.baseElement)
    this.updateLostVisibility()
  }
  
  /**
   * Repopulates the data of all fields with a new data object
   * @param {object} data
   */
  populate(data, compAmt){
    //set data objects
    this.data.resData = data
    this.data.compAmt = compAmt
    this.js.populate()
    this.so.populate()
    this.co.populate()
    
    document.querySelector('#comp-amt-header').innerText = Util.formatCurrency(compAmt)
    
    this.updateLostVisibility()
  }
  
  /**
   * Updates the visibility of the lost section and its values
   */
  updateLostVisibility(){
    //update normal/lost visibility
    const isLost = this.data.resData.job_status_code == 'JL'
    const normalDisplay = !isLost ? '' : 'none'
    const lostDisplay = isLost ? '' : 'none'
  
    this.dom.jobRevertButton.style.display = lostDisplay
    this.dom.lostInfo.style.display = lostDisplay
    this.dom.jobLostButton.style.display = normalDisplay
  
    this.dom.job_reason_dsca.innerText = this.data.resData.job_reason_dsca
  }
  
  /**
   * Sets this restoration job as lost
   */
  setResJobLost(){
    //prepare reasons key
    let selectData = []
    for (const reasonsKey in this.data.reasons) {
      const item = this.data.reasons[reasonsKey]
      
      selectData.push({
        value: item.job_reason_id,
        text: item.job_reason_dsca
      })
    }
    
    let options = {
      title: 'Set Job Lost',
      label: 'Reason:',
      okText: 'Set Lost'
    }
    
    new SelectModal(selectData, options, ((result, resultID) => {
      if(result, resultID){
        fetch('restorationDetailsPRG.php', {
          method: 'post',
          headers: {'Content-Type':'application/json;charset=utf8'},
          body: JSON.stringify({
            action: 'setLost',
            jobID: this.data.resData.job_id,
            reason: resultID
          })
        })
          .then(res => res.json())
          .then(body => {
            if(body.ok){
              this.populate(body.msg)
            }else{
              note({class: 'error', text: 'Error updating data'})
            }
          })
      }
    }).bind(this))
  }
  
  /**
   * Unsets this restoration job as lost
   */
  unsetResJobLost(){
    new YNModal('Revert Job Lost', 'Are you sure you wish to unmark this job as lost?', ((result) => {
      if(result){
        fetch('restorationDetailsPRG.php', {
          method: 'post',
          headers: {'Content-Type':'application/json;charset=utf8'},
          body: JSON.stringify({
            action: 'unsetLost',
            jobID: this.data.resData.job_id
          })
        })
          .then(res => res.json())
          .then(body => {
            if(body.ok){
              this.populate(body.msg)
            }
          })
      }
    }).bind(this))
  }
  
  /**
   * Event handler for when a new partner is selected
   */
  onPartnerSelected(){
    const partnerID = this.partnerSelector.getSelectedPartnerID()
    fetch('restorationDetailsPRG.php', {
      method: 'post',
      headers: {'Content-Type': 'application/json;charset=utf8'},
      body: JSON.stringify({
        action: 'setResPartner',
        jobID: this.data.resData.job_id,
        resJobID: this.data.resData.jres_job,
        partnerID: partnerID
      })
    })
      .then(res => res.json())
      .then(body => {
        if(body.ok){
          this.populate(body.msg)
        }else{
          note({class: 'error', text: 'Error updating data'})
        }
      })
  }
  
  /**
   * Updates the data by matching dom elements
   * @param dom
   * @param data
   */
  updateData(dom){
    for (const domKey in dom) {
      if(!dom.hasOwnProperty(domKey) || this.data[domKey] === undefined) continue
      const elem = dom[domKey]
      this.data[domKey] = elem.value
    }
  }
  
  /**
   * Initiates change events for all of the input items in the dom
   * @param {Object} dom
   * @param {Function} callback
   */
  static initiateChangeEvents(dom, callback) {
    for (const domKey in dom) {
      if (!dom.hasOwnProperty(domKey)) continue
      let elem = dom[domKey]
      
      const tagName = elem.tagName.toLowerCase()
      if (tagName != 'input' && tagName != 'textarea') continue
      switch (elem.type) {
        case 'button':
          continue
        default:
          elem.addEventListener('change', callback)
      }
    }
  }
  
  /**
   * Initiates a datepicker and datepicker change event
   * @param {HTMLElement} dp
   * @param {Function} event
   */
  static initiateDatePicker(dp, event){
    let jqueryObj = $(dp)
    jqueryObj.datepicker({
      onSelect: event
    })
    jqueryObj.datepicker('option', 'dateFormat', 'mm/dd/y')
  }
}