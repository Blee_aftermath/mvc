import { ResPartner } from "./resPartner.js"
/**
 * A selector box that controls selecting a restoration partner
 * @author Jake Cirino
 */
export class ResPartnerSelector{
  /**
   * Creates a new restoration partner selector
   * @param {JSON} data JSON containing the list of res partner data
   * @param {String} resTemplate
   * @param {Number} initialPartnerID
   */
  constructor(data, resTemplate, selectedPartnerID = -1){
    this.data = data
    this.resTemplate = resTemplate
    this.changeListeners = []

    //create base element
    this.baseElement = document.createElement('div')
    this.baseElement.className = 'partner-selector'

    //populate items
    this.items = []
    if(this.data.length > 0){
      for (const key in this.data) {
        if (Object.hasOwnProperty.call(this.data, key)) {
          const resData = this.data[key];

          //alert(resData['rp_is_nonpay']);

          //create the partner object
          let newPartner = new ResPartner(resData, this.resTemplate, true)

          //add click listener to the partner object
          newPartner.addClickListener(((resPartner) => {
            if(resPartner.selected){
              this.setSelectedPartner(null)
            }else{
              this.setSelectedPartner(resPartner)
            }
          }).bind(this))
  
          //push the item to html/partner list
          this.items.push(newPartner)
          this.baseElement.appendChild(newPartner.baseElement)
        }
      }
    }else{
      this.baseElement.innerText = 'There are no Restoration Partners available to service this office or state.'
    }

    this.setSelectedPartnerID(selectedPartnerID)
  }

  /**
   * Sets the currently selected partner
   * @param {ResPartner} partner 
   */
  setSelectedPartner(partner){
    this.selectedPartner = partner

    //set item to be selected
    for (const key in this.items) {
      if (Object.hasOwnProperty.call(this.items, key)) {
        const element = this.items[key];
        
        if(this.selectedPartner != null && element.equals(this.selectedPartner)){
          element.setSelected(true)
        }else{
          element.setSelected(false)
        }
      }
    }

    this.changed()
  }

  /**
   * Sets the currently selected partner by their partner ID
   * @param {Number} partnerID 
   */
  setSelectedPartnerID(partnerID){
    for (const key in this.items) {
      if (Object.hasOwnProperty.call(this.items, key)) {
        const element = this.items[key];
        
        if(Number(element.data.rp_id == partnerID)){
          this.setSelectedPartner(element)
          return
        }
      }
    }

    //partner ID doesnt exist, set to null
    this.setSelectedPartner(null)
  }

  /**
   * Gets the currently selected partner
   * @returns {ResPartner}
   */
  getSelectedPartner(){
    return this.selectedPartner
  }

  /**
   * Gets the partner ID of the currently selected partner
   * @returns {ResPartnerID}
   */
  getSelectedPartnerID(){
    let partner = this.getSelectedPartner()

    if(partner == null) return partner
    else return partner.data.rp_id
  }

  /**
   * Adds an event listener for when the selected partner is changed
   * @param {Function} callback 
   */
  addChangeListener(callback){
    this.changeListeners.push(callback)
  }

  /**
   * Calls all of the change listeners for this selector
   */
  changed(){
    for (const key in this.changeListeners) {
      if (Object.hasOwnProperty.call(this.changeListeners, key)) {
        const callback = this.changeListeners[key];
        callback()
      }
    }
  }
  
  /**
   * Sets this partner selector visible or not
   * @param {boolean} visible
   */
  setVisible(visible){
    this.baseElement.style.display = visible ? '' : 'none'
  }
  
  /**
   * Deletes this partner selector
   */
  delete(){
    this.baseElement.remove()
    delete this
  }
}