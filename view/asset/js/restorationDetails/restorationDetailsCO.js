import {Util} from "../util.js"
import {RestorationDetails} from "./restorationDetails.js"
import note from "../modules/lib/note.js"

/**
 * Class that handles the CO section on the restoration job details tab
 * @author Jake Cirino
 */
export class RestorationDetailsCO{
  /**
   * Creates a new CO section
   * @param {RestorationDetails} parent
   * @param {Object} data
   */
  constructor(parent, data) {
    this.parent = parent
    this.data = data
    
    this.baseElement = parent.dom.coBody
    this.dom = Util.parseDOM(this.baseElement)
    
    let changeFunc = () => {
      this.dom.saveButton.disabled = false
    }
    RestorationDetails.initiateChangeEvents(this.dom, changeFunc)
    RestorationDetails.initiateChangeEvents(this.dom.job_dt_comp, changeFunc)
    RestorationDetails.initiateDatePicker(this.dom.compDate, changeFunc)
    
    this.dom.saveButton.addEventListener('click', this.save.bind(this))
    this.populate()
  }
  
  /**
   * Populates this form with this.data
   */
  populate(){
    try{
      const resData = this.data.resData
      const dateTime = resData.job_dt_comp.split(' ')
      this.dom.compDate.value = dateTime[0]
      this.dom.compTime.value = Util.parseTime24(dateTime[1])
      
      this.dom.jres_amt_paid.value = (resData.jres_amt_paid/100).toFixed(2)
      this.dom.jres_info_co.value = resData.jres_info_co
    }catch(e){}
  
    this.dom.amsValue.value = (this.data.compAmt/100).toFixed(2)
    this.dom.saveButton.disabled = true
    this.updateSectionDisabled()
  }
  
  /**
   * Updates the disabled sections
   */
  updateSectionDisabled(){
    let status = this.data.resData.job_status_code
    let jobDisabled = status != 'SO' && status != 'CO'
    let jresDisabled = status != 'CO'
    
    this.dom.compDate.disabled = jobDisabled
    this.dom.compTime.disabled = jobDisabled
    
    this.dom.jres_amt_paid.disabled = jresDisabled
    this.dom.jres_info_co.disabled = jresDisabled
  }
  
  calculateAMS(){
    //TODO
  }
  
  /**
   * Saves both sections of this body
   * @returns {Promise<void>}
   */
  async save(){
    if(this.data.resData.job_status_code == 'SO'){
      this.saveJob()
    }else{
      if(await this.saveJob(false))
        this.saveResJob()
    }
  }
  
  /**
   * Saves the job section
   * @param populate
   * @returns {boolean}
   */
  saveJob(populate = true){
    return new Promise((resolve, reject) => {
      try{
        const data = {
          action: 'updateJobCO',
          jobID: this.data.resData.job_id,
          job_dt_comp: Util.dateToString(Util.parseDate(this.dom.compDate.value, this.dom.compTime.value))
        }
    
        fetch('restorationDetailsPRG.php', {
          method: 'post',
          headers: {'Content-Type': 'application/json;charset=utf8'},
          body: JSON.stringify(data)
        })
          .then(res => res.json())
          .then(body => {
            if(body.ok){
              if(populate) this.parent.populate(body.msg, body.comp)
            }else{
              note({class: 'error', text: 'Error inserting data'})
            }
        
            resolve(body.ok)
          })
      }catch(e){
        note({class: 'error', text: 'Invalid data entered'})
        resolve(false)
      }
    })
  }
  
  /**
   * Saves the restoration job section
   * @param populate
   * @returns {boolean}
   */
  saveResJob(populate = true){
    return new Promise((resolve, reject) => {
      try{
        const data = {
          action: 'updateResCO',
          jobID: this.data.resData.job_id,
          resJobID: this.data.resData.jres_job,
          jres_amt_paid: Number(this.dom.jres_amt_paid.value).toFixed(2) * 100,
          jres_info_co: this.dom.jres_info_co.value
        }
    
        fetch('restorationDetailsPRG.php', {
          method: 'post',
          headers: {'Content-Type': 'application/json;charset=utf8'},
          body: JSON.stringify(data)
        })
          .then(res => res.json())
          .then(body => {
            if(body.ok){
              if(populate) this.parent.populate(body.msg, body.comp)
            }else{
              note({class: 'error', text: 'Error inserting data'})
            }
        
            resolve(body.ok)
          })
      }catch(e){
        note({class: 'error', text: 'Invalid data entered'})
        resolve(false)
      }
    })
  }
  
}