import { Util } from '../util.js'

/**
 * Creates and manages the state of a restoration partner object.
 * @author Jake Cirino
 */
export class ResPartner {
  /**
   * Creates a new res partner object
   * @param {JSON} data
   * @param {String} template The html template
   * @param {Boolean} radio Whether or not to include the radio selector for this item
   */
  constructor(data, template, radio = false) {
    this.data = data
    this.template = template
    this.radio = radio
    this.selectCallbacks = []

    //create the base element
    this.baseElement = document.createElement('div')
    this.baseElement.className = 'partner-box'
    this.baseElement.innerHTML = template

    //parse the dom for this object
    this.dom = Util.parseDOM(this.baseElement)

    //setup clicking
    if (this.radio) {
      this.dom.radio.style.display = ''

      this.dom.radio.addEventListener('click', (event) => {
        //call all of the selected callbacks
        for (const key in this.selectCallbacks) {
          if (Object.hasOwnProperty.call(this.selectCallbacks, key)) {
            const callback = this.selectCallbacks[key]
            callback(this)
          }
        }
      })
    }

    //set initial selected state
    this.setSelected(false)
    this.populate()
  }

  /**
   * Populates the data fields of the html based on this.data
   */
  populate() {
    for (const name in this.dom) {
      if (Object.hasOwnProperty.call(this.dom, name)) {
        const element = this.dom[name]
        const elementData = this.data[name]

        if (name.includes('rp_')) {
          if(elementData == null){ //hide the item if there is no data
            element.style.display = 'none'
            continue
          }

          if(name == 'rp_price_base')
            element.innerText = `Base: $${(elementData/100).toFixed(2)}`
          else if(name == 'rp_price_rate')
            element.innerText = `Rate: ${elementData}%`
          else if(name == 'rp_is_nonpay') {
            if(Number(elementData) == 1) { element.style.display = 'block' }
          } else
            element.innerText = elementData
        }
      }
    }
    
    //setup edit button link
    this.dom.rpEditButton.setAttribute('href', `resPartner.php?rp=${this.data.rp_id}`)
  }

  /**
   * Sets the selected state of this partner object
   * @param {Boolean} selected
   */
  setSelected(selected) {
    this.selected = selected
    
    this.dom.radio.checked = selected
    if (selected) {
      this.baseElement.classList.add('box-selected')
    } else {
      this.baseElement.classList.remove('box-selected')
    }
  }

  /**
   * Adds a listener event for when this res partner is clicked on
   * @param {Function} callback
   */
  addClickListener(callback) {
    this.selectCallbacks.push(callback)
  }

  /**
   * Checks if this res partner object is equal to another
   * @param {ResPartner} other
   * @returns {Boolean}
   */
  equals(other) {
    return this.data.rp_id == other.data.rp_id
  }
}
