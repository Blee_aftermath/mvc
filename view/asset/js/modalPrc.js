var arrPrcOb   = []; // Array of Price objects
var arrPrcID   = []; // Array of Price IDs
var arrPrcEr   = []; // Array of errored fields to tag in red

function fnAddPrc() {
  document.getElementById("spanPrcHead").innerHTML     = "Add Price";
  document.getElementById("hidPrcID").disabled         = true;
  document.getElementById("tblPrc").style.display      = "block";
  document.getElementById("subAddPrc").style.display   = "inline";
  document.getElementById("subChgPrc").style.display   = "none";
  document.getElementById("butConPrc").style.display   = "none";
  document.getElementById("myModalPrc").style.display  = "block";
}

function fnChgPrc(id) {
  prcIndex = arrPrcID.indexOf(id);
  document.getElementById("spanPrcHead").innerHTML     = "Edit Price";
  document.getElementById("hidPrcID").disabled         = false;
  document.getElementById("tblPrc").style.display      = "block";
  document.getElementById("subAddPrc").style.display   = "none";
  document.getElementById("subChgPrc").style.display   = "inline";
  document.getElementById("butConPrc").style.display   = "inline";
  document.getElementById("myModalPrc").style.display  = "block";
  fnFillPrc(prcIndex);
}

function fnFillPrc(prcIndex) {
  document.getElementById("hidPrcID").value            = arrPrcID[prcIndex];
  document.getElementById("inpAmtCost").value          = (arrPrcOb[prcIndex].penCost / 100).toFixed(2);
  document.getElementById("inpDEff").value             = arrPrcOb[prcIndex].dtEff;
}

function fnConfirmDelPrc() {
  strConfirm  = "<div class='content alert'>";
  strConfirm += "Delete this Price?&nbsp;&nbsp;";
  strConfirm += "<input class='inp08' type='submit' name='subDelPrcY' value='Yes' />";
  strConfirm += "<input class='inp08' type='button' name='butDelPrcN' value='No' onclick='fnDelPrcN()' />";
  strConfirm += "</div>";
  document.getElementById("spanPrcConfirm").innerHTML = strConfirm;
}

function fnDelPrcN() {
  document.getElementById("spanPrcConfirm").innerHTML = "";
}

function fnCloseModalPrc() {
  document.getElementById("hidPrcID").value            = "";
  document.getElementById("inpAmtCost").value          = "";
  document.getElementById("inpDEff").value             = "";
  document.getElementById("spanPrcHead").innerHTML     = "";
  document.getElementById("spanPrcMessages").innerHTML = "";
  document.getElementById("spanPrcConfirm").innerHTML  = "";
  document.getElementById("myModalPrc").style.display  = "none";
  for(var i=0; i<arrPrcEr.length; i++) {
    document.getElementById(arrPrcEr[i]).classList.remove("err");
  }
}