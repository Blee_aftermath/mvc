<?php
 echo "\n\n<script language=\"JavaScript\">";
  echo "\n<!-- Hide the script from old browsers --";

  echo "\nvar arrAddressP = [];"; // Array of address objectss for this job
  echo "\nvar arrErrorPr   = [];"; // Array of errored fields to tag in red

// Store some info about the caller and property for fnGetCaller and fnGetProperty.
// Make two attempts to do this -- from a possible $myJob or $myInv on the including page.

  if (isset($myJob)) {
    echo "\nvar objCallerAddressPr = {};";
    echo "\nvar objPropAddressPr   = {};";
    echo "\nobjCallerAddressPr.fname    = \"" . prepareJS($myJob['job_cust_fname'])    . "\";";
    echo "\nobjCallerAddressPr.lname    = \"" . prepareJS($myJob['job_cust_lname'])    . "\";";
    echo "\nobjCallerAddressPr.company  = \"" . prepareJS($myJob['job_cust_other'])  . "\";";
    echo "\nobjCallerAddressPr.email    = \"" . prepareJS($myJob['job_cust_email'])    . "\";";
    echo "\nobjCallerAddressPr.telp     = \"" . formatPhone($myJob['job_cust_telp'])   . "\";";
    echo "\nobjCallerAddressPr.extp     = \"" . prepareJS($myJob['job_cust_ext'])      . "\";";
    echo "\nobjPropAddressPr.addr1      = \"" . prepareJS($myJob['job_prop_addr1'])    . "\";";
    echo "\nobjPropAddressPr.addr2      = \"" . prepareJS($myJob['job_prop_addr2'])    . "\";";
    echo "\nobjPropAddressPr.city       = \"" . prepareJS($myJob['job_prop_city'])     . "\";";
    echo "\nobjPropAddressPr.state      = \"" . prepareJS($myJob['job_prop_state'])    . "\";";
    echo "\nobjPropAddressPr.zip        = \"" . prepareJS($myJob['job_prop_zip'])      . "\";";

  } elseif (isset($myInv)) {
    echo "\nvar objCallerAddressPr = {};";
    echo "\nvar objPropAddressPr   = {};";
    echo "\nobjCallerAddressPr.fname    = \"" . prepareJS($myInv['job_cust_fname'])    . "\";";
    echo "\nobjCallerAddressPr.lname    = \"" . prepareJS($myInv['job_cust_lname'])    . "\";";
    echo "\nobjCallerAddressPr.company  = \"" . prepareJS($myInv['job_cust_other'])  . "\";";
    echo "\nobjCallerAddressPr.email    = \"" . prepareJS($myInv['job_cust_email'])    . "\";";
    echo "\nobjCallerAddressPr.telp     = \"" . formatPhone($myInv['job_cust_telp'])   . "\";";
    echo "\nobjCallerAddressPr.extp     = \"" . prepareJS($myInv['job_cust_ext'])      . "\";";
    echo "\nobjPropAddressPr.addr1      = \"" . prepareJS($myInv['job_prop_addr1'])    . "\";";
    echo "\nobjPropAddressPr.addr2      = \"" . prepareJS($myInv['job_prop_addr2'])    . "\";";
    echo "\nobjPropAddressPr.city       = \"" . prepareJS($myInv['job_prop_city'])     . "\";";
    echo "\nobjPropAddressPr.state      = \"" . prepareJS($myInv['job_prop_state'])    . "\";";
    echo "\nobjPropAddressPr.zip        = \"" . prepareJS($myInv['job_prop_zip'])      . "\";";
  }

  echo "\nfunction fnGetCallerPr() {";
  echo "\n  document.getElementById(\"inpFNamePr\").value    = objCallerAddressPr.fname;";
  echo "\n  document.getElementById(\"inpLNamePr\").value    = objCallerAddressPr.lname;";
  echo "\n  document.getElementById(\"inpRelationPr\").value = objCallerAddressPr.company;";
  echo "\n  document.getElementById(\"inpEmailPr\").value    = objCallerAddressPr.email;";
  echo "\n  document.getElementById(\"inpTelpPr\").value     = objCallerAddressPr.telp;";
  echo "\n  document.getElementById(\"inpExtpPr\").value     = objCallerAddressPr.extp;";
  echo "\n}";

  echo "\nfunction fnGetPropertyPr() {";
  echo "\n  document.getElementById(\"inpLine1Pr\").value   = objPropAddressPr.addr1;";
  echo "\n  document.getElementById(\"inpLine2\").value    = objPropAddressPr.addr2;";
  echo "\n  document.getElementById(\"inpCity\").value     = objPropAddressPr.city;";
  echo "\n  document.getElementById(\"selState\").value    = objPropAddressPr.state;";
  echo "\n  document.getElementById(\"inpZip\").value      = objPropAddressPr.zip;";
  echo "\n}";

  echo "\nfunction fnFillFormPr(id) {";
  echo "\n  document.getElementById(\"inpFNamePr\").value   =  arrAddressP[id].fname;";
  echo "\n  document.getElementById(\"inpLNamePr\").value    = arrAddressP[id].lname;";
  echo "\n  document.getElementById(\"inpTitlePr\").value    = arrAddressP[id].title;";
  echo "\n  document.getElementById(\"inpCompanyPr\").value  = arrAddressP[id].company;";
  echo "\n  document.getElementById(\"inpLine1Pr\").value    = arrAddressP[id].line1;";
  echo "\n  document.getElementById(\"inpLine2Pr\").value    = arrAddressP[id].line2;";
  echo "\n  document.getElementById(\"inpCityPr\").value     = arrAddressP[id].city;";
  echo "\n  document.getElementById(\"selStatePr\").value    = arrAddressP[id].state;";
  echo "\n  document.getElementById(\"inpZipPr\").value      = arrAddressP[id].zip;";
  echo "\n  document.getElementById(\"inpEmailPr\").value    = arrAddressP[id].email;";
  echo "\n  document.getElementById(\"inpTelpPr\").value     = arrAddressP[id].telp;";
  echo "\n  document.getElementById(\"inpTelcPr\").value     = arrAddressP[id].telc;";
  echo "\n  document.getElementById(\"inpTelwPr\").value     = arrAddressP[id].telw;";
  echo "\n  document.getElementById(\"inpExtpPr\").value     = arrAddressP[id].extp;";
  echo "\n  document.getElementById(\"inpExtcPr\").value     = arrAddressP[id].extc;";
  echo "\n  document.getElementById(\"inpExtwPr\").value     = arrAddressP[id].extw;";
  echo "\n  document.getElementById(\"inpRelationPr\").value = arrAddressP[id].relation;";
  echo "\n  document.getElementById(\"inpInfoPr\").value     = arrAddressP[id].info;";
  echo "\n}";

  echo "\nfunction fnAddAddressPr() {";
  echo "\n  document.getElementById(\"spanAddressIDPr\").innerHTML = \" " . $delim . " Add\";";
  echo "\n  document.getElementById(\"hidAddressIDPr\").disabled   = true;";
  echo "\n  document.getElementById(\"hidAddressIDPr\").value      = \"\";";
  echo "\n  document.getElementById(\"subAddAddrPr\").style.display    = \"inline\";";
  echo "\n  document.getElementById(\"subChgAddrPr\").style.display    = \"none\";";
  echo "\n  document.getElementById(\"subCan\").style.display    = \"inline\";";
  echo "\n  document.getElementById(\"myModalAddressPr\").style.display   = \"block\";";
  echo "\n  if(window.objCallerAddressPr) {"; // Only show the Get Caller option if the objCallerAddressPr was created earlier.
  echo "\n    document.getElementById(\"spanGetCallerNamePr\").innerHTML    = \"&nbsp;[<a href='javascript:fnGetCallerPr();'>Get Caller</a>]\";";
  echo "\n  }";
  echo "\n  if(window.objPropAddressPr) {"; // Only show the Get Property option if the objCallerAddressPr was created earlier.
  echo "\n    document.getElementById(\"spanGetPropNamePr\").innerHTML    = \"&nbsp;[<a href='javascript:fnGetPropertyPr();'>Get Property</a>]\";";
  echo "\n  }";
  echo "\n  document.getElementById(\"spanConfirm\").innerHTML = \"\";";
  echo "\n}";

  echo "\nfunction fnEditAddressPr(id) {";
  echo "\n  fnFillFormPr(id);";
  echo "\n  document.getElementById(\"spanAddressIDPr\").innerHTML = arrAddressP[id].id + \" " . $delim . " Edit\";";
  echo "\n  document.getElementById(\"hidAddressIDPr\").disabled   = false;";
  echo "\n  document.getElementById(\"hidAddressIDPr\").value      = arrAddressP[id].id;";
  echo "\n  document.getElementById(\"subAddAddrPr\").style.display  = \"none\";";
  echo "\n  document.getElementById(\"subChgAddrPr\").style.display  = \"inline\";";
  echo "\n  document.getElementById(\"subCan\").style.display  = \"inline\";";
  echo "\n  document.getElementById(\"myModalAddressPr\").style.display = \"block\";";
  echo "\n  document.getElementById(\"spanGetCallerNamePr\").innerHTML  = \"\";";
  echo "\n  document.getElementById(\"spanGetPropNamePr\").innerHTML    = \"\";";
  echo "\n}";

  echo "\nfunction fnCloseAddressPr() {";
  echo "\n  document.getElementById(\"hidAddressIDPr\").value      = \"\";";
  echo "\n  document.getElementById(\"spanAddressIDPr\").innerHTML = \"\";";
  echo "\n  document.getElementById(\"inpFNamePr\").value          = \"\";";
  echo "\n  document.getElementById(\"inpLNamePr\").value          = \"\";";
  echo "\n  document.getElementById(\"inpTitlePr\").value          = \"\";";
  echo "\n  document.getElementById(\"inpCompanyPr\").value        = \"\";";
  echo "\n  document.getElementById(\"inpLine1Pr\").value          = \"\";";
  echo "\n  document.getElementById(\"inpLine2Pr\").value          = \"\";";
  echo "\n  document.getElementById(\"inpCityPr\").value           = \"\";";
  echo "\n  document.getElementById(\"selStatePr\").value          = \"\";";
  echo "\n  document.getElementById(\"inpZipPr\").value            = \"\";";
  echo "\n  document.getElementById(\"inpEmailPr\").value          = \"\";";
  echo "\n  document.getElementById(\"inpTelpPr\").value           = \"\";";
  echo "\n  document.getElementById(\"inpExtpPr\").value           = \"\";";
  echo "\n  document.getElementById(\"inpTelcPr\").value           = \"\";";
  echo "\n  document.getElementById(\"inpExtcPr\").value           = \"\";";
  echo "\n  document.getElementById(\"inpTelwPr\").value           = \"\";";
  echo "\n  document.getElementById(\"inpExtwPr\").value           = \"\";";
  echo "\n  document.getElementById(\"inpRelationPr\").value       = \"\";";
  echo "\n  document.getElementById(\"inpInfoPr\").value           = \"\";";
  echo "\n  for(var i=0; i<arrErrorPr.length; i++) {";
  echo "\n    document.getElementById(arrErrorPr[i]).classList.remove(\"err\");";
  echo "\n  }";
  echo "\n  document.getElementById(\"spanMessagesPr\").innerHTML  = \"\";";
  echo "\n  document.getElementById(\"myModalAddressPr\").style.display   = \"none\";";
  echo "\n}";

  echo "\n// --End Hiding Here -->";
  echo "\n</script>\n";

  if (isset($valJobID)) {
    $sqlAddr = "SELECT address.* FROM address WHERE addr_job = $valJobID and addr_is_prow =1";

    if ($resAddr = $mysqli->query($sqlAddr)) {
      $numAddr = $resAddr->num_rows;
      if ($numAddr > 0) {

        while ($myAddr = $resAddr->fetch_assoc()) {

          echo "\n<script type=\"text/javascript\">";
          echo "\n<!-- Hide the script from old browsers --";

          echo "\nvar objAddressP = {};"; // Object of address fields for this address.  Can also = new Object();
          $statePr = $myAddr['addr_state'];

          echo "\nobjAddressP.id       = \"" . $myAddr['addr_id']                  . "\";";
          echo "\nobjAddressP.fname    = \"" . prepareJS($myAddr['addr_fname'])    . "\";";
          echo "\nobjAddressP.lname    = \"" . prepareJS($myAddr['addr_lname'])    . "\";";
          echo "\nobjAddressP.title    = \"" . prepareJS($myAddr['addr_title'])    . "\";";
          echo "\nobjAddressP.company  = \"" . prepareJS($myAddr['addr_company'])  . "\";";
          echo "\nobjAddressP.line1    = \"" . prepareJS($myAddr['addr_line1'])    . "\";";
          echo "\nobjAddressP.line2    = \"" . prepareJS($myAddr['addr_line2'])    . "\";";
          echo "\nobjAddressP.city     = \"" . prepareJS($myAddr['addr_city'])     . "\";";
          echo "\nobjAddressP.state    = \"" . $myAddr['addr_state']               . "\";";
          echo "\nobjAddressP.zip      = \"" . $myAddr['addr_zip']                 . "\";";
          echo "\nobjAddressP.email    = \"" . prepareJS($myAddr['addr_email'])    . "\";";
          echo "\nobjAddressP.telp     = \"" . formatPhone($myAddr['addr_telp'])   . "\";";
          echo "\nobjAddressP.telc     = \"" . formatPhone($myAddr['addr_telc'])   . "\";";
          echo "\nobjAddressP.telw     = \"" . formatPhone($myAddr['addr_telw'])   . "\";";
          echo "\nobjAddressP.extp     = \"" . prepareJS($myAddr['addr_extp'])     . "\";";
          echo "\nobjAddressP.extc     = \"" . prepareJS($myAddr['addr_extc'])     . "\";";
          echo "\nobjAddressP.extw     = \"" . prepareJS($myAddr['addr_extw'])     . "\";";
          echo "\nobjAddressP.relation = \"" . prepareJS($myAddr['addr_relation']) . "\";";
          echo "\nobjAddressP.info     = \"" . prepareJS($myAddr['addr_info'])     . "\";";
          echo "\nobjAddressP.is_cosi  = \"" . prepareJS($myAddr['addr_is_cosi'])  . "\";";
          echo "\nobjAddressP.is_prow  = \"" . prepareJS($myAddr['addr_is_prow'])  . "\";";

          echo "\narrAddressP[" . $myAddr['addr_id'] . "] = objAddressP;"; // Push this object into the array of addresses

          echo "\n// --End Hiding Here -->";
          echo "\n</script>\n";

        } // while
      } // if ($numAddr > 0)
    } // if ($resAddr)
  } // if ($valJobID)

  $strIcons  = "<span style='float:right;'>";
  $strIcons .= "<a href='javascript:fnAddAddressPr();'><img class='icon16' src='icons/add16.png' alt='Add' title='Add' /></a>";
  $strIcons .= "</span>";
  echo "<div class='modal' id='myModalAddressPr'>";
  echo "<div class='fixed'>";
  echo "<div class='body_stripe'></div>";

  echo "<div class='head'>";
  echo "<div class='content'>";

  echo "Property Owner Information <span id='spanAddressIDPr'></span>";
  echo "<span style='float:right;'><a href='javascript:fnCloseAddressPr();'><img class='icon16' src='icons/no16.png' alt='Close' title='Close' /></a></span>";

  echo "</div>"; // content
  echo "</div>"; // head

  echo "<span id='spanMessagesPr'>";
  fnModalMessagesBox('Property');
  echo "</span>";

  echo "<div class='body'>";
  echo "\n<form method='post' name='frmAddress1' id='frmAddress1' action='jifPRG.php'>";
  echo "<div class='content'>";

  $inpField = "hidAddressIDPr";
  $defValue = ""; // set by JS
  echo "<input type='hidden' name='$inpField' id='$inpField' value='" . $defValue . "' />";

  if (isset($valJobID)) {
    $inpField = "hidJobID";
    $defValue = $valJobID;
    echo "<input type='hidden' name='$inpField' id='$inpField' value='" . $defValue . "' />";
  }
  echo "<table class='info'>";

  $defValue = ""; // set by JS for all fields in the modal table
  $checked  = ""; // set by JS for all fields in the modal table

  echo "<tr class='heading'><th colspan='2'>Printable Address</th></tr>";
 
  $inpField = "inpFNamePr";
  $inpLabel = "Name:";
  $defClass = "inp10 " . "req";
  echo "<tr class='content'><td>$inpLabel</td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' value='$defValue' maxlength='$maxName' />";

  $inpField = "inpLNamePr";
  $defClass = "inp10 " . "req";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' value='$defValue' maxlength='$maxName' />";
  echo "<span id='spanGetCallerNamePr'></span>";
  echo "</td></tr>";

  $inpField = "inpTitlePr";
  $inpLabel = "Title:";
  $inpHelp  = "Title should be a real employment title such as Director or Attorney.  It should be entered as you would expect it to show on print materials like an Aftermath invoice.";
  $defClass = "opt";
  echo "<tr class='content'><td>$inpLabel</td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' value='" . $defValue . "' maxlength='$maxName' />";
  echo "&nbsp;<img class='icon16' title='$inpHelp' alt='Help' src='icons/question16.png' />";
  echo "</td></tr>";

  $inpField = "inpCompanyPr";
  $inpLabel = "Company:";
  $inpHelp  = "Company should be the company this person works for.  It could be something like BNSF or the proper name of a management company.  Do not put an informal description like Management Company.  Spell out the actual company name as this may appear on print materials like an Aftermath invoice.";
  $defClass = "opt";
  echo "<tr class='content'><td class='l'>$inpLabel</td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' value='$defValue' maxlength='$maxName' />";
  echo "&nbsp;<img class='icon16' title='$inpHelp' alt='Help' src='icons/question16.png' />";
  echo "</td></tr>";

  $inpField = "inpLine1Pr";
  $inpLabel = "Address Line 1:";
  $defClass = "req";
  echo "<tr class='content'><td class='l'>$inpLabel</td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' value='$defValue' maxlength='$maxAddr' />";
  echo "<span id='spanGetPropNamePr'></span>";
  echo "</td></tr>";

  $inpField = "inpLine2Pr";
  $inpLabel = "Address Line 2:";
  $defClass = "opt";
  echo "<tr class='content'><td class='l'>$inpLabel</td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' value='$defValue' maxlength='$maxAddr' />";
  echo "</td></tr>";

  $inpField = "inpCityPr";
  $inpLabel = "City:";
  $defClass = "req";
  echo "<tr class='content'><td class='l'>$inpLabel</td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' value='$defValue' maxlength='$maxAddr' />";
  echo "</td></tr>";

  $sqlState = "SELECT * FROM state ORDER BY st_name";
  $resState = $mysqli->query($sqlState) or die('Get state failed');
  $inpField = "selStatePr";
  $inpLabel = "State / Zip:";
  $defClass = "inp12 " . "req";
  echo "<tr class='content'><td class='l'>$inpLabel</td><td class='r'>";
  echo "<select class='$defClass' name='$inpField' id='$inpField'>";
  echo "<option value='' selected='selected'>--- Select ---</option>";
  while ($myState = $resState->fetch_assoc()) {
    echo "<option value='" . $myState['st_id'] . "'";
    if(isset($statePr)) { 
      if ($myState['st_id'] == $statePr) { echo " selected='selected'"; }
    }
    echo ">" . $myState['st_name'] . "</option>";
  } echo "</select>";

  $inpField = "inpZipPr";
  $defClass = "inp08 " . "req";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' value='$defValue' maxlength='$maxZip' />";
  echo "</td></tr>";

  echo "<tr class='heading'><th colspan='2'>Contact Details</th></tr>";

  $inpField = "inpEmailPr";
  $inpLabel = "Email:";
  $defClass = "opt";
  echo "<tr class='content'><td>$inpLabel</td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' value='$defValue' maxlength='$maxEmail' />";
  echo "</td></tr>";

  $inpLabel = "Phone (Home):";
  echo "<tr class='content'><td class='l'>$inpLabel</td>";
  echo "<td class='r'>";
  $inpField = "inpTelpPr";
  $defClass = "inp12 " . "opt";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' value='$defValue' maxlength='$maxTel' />";

  $inpField = "inpExtpPr";
  $defClass = "inp08 " . "opt";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' value='$defValue' maxlength='$maxExt' />";
  echo "</td></tr>";

  $inpLabel = "Phone (Cell):";
  echo "<tr class='content'><td class='l'>$inpLabel</td>";
  echo "<td class='r'>";
  $inpField = "inpTelcPr";
  $defClass = "inp12 " . "opt";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' value='$defValue' maxlength='$maxTel' />";

  $inpField = "inpExtcPr";
  $defClass = "inp08 " . "opt";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' value='$defValue' maxlength='$maxExt' />";
  echo "</td></tr>";

  $inpLabel = "Phone (Work):";
  echo "<tr class='content'><td class='l'>$inpLabel</td>";
  echo "<td class='r'>";
  $inpField = "inpTelwPr";
  $defClass = "inp12 " . "opt";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' value='$defValue' maxlength='$maxTel' />";

  $inpField = "inpExtwPr";
  $defClass = "inp08 " . "opt";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' value='$defValue' maxlength='$maxExt'/>";
  echo "</td></tr>";

  $inpField = "inpRelationPr";
  $inpLabel = "Relationship:";
  $inpHelp  = "Relation should indicate the relationship between this person and the job.  Potential entries might be Father of decedent, Executor, Neighbor, Contractor or Property Manager.  Do not enter Owner or Property Owner here.  If this person is the property owner, check the appropriate box below.  Relation is an internal field and will not show on customer correspondence.";
  $defClass = "opt";
  echo "<tr class='content'><td class='l'>$inpLabel</td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' value='$defValue' maxlength='$maxName' />";
  echo "&nbsp;<img class='icon16' title='$inpHelp' alt='Help' src='icons/question16.png' />";
//  echo "&nbsp;Relationships like &quot;Brother&quot; should go here.";
  echo "</td></tr>";

  $inpField = "inpInfoPr";
  $inpLabel = "Additional<br />Information:";
  $inpHelp  = "Additional Information should indicate why this person is relevant to this job.  If there are multiple billable addresses, specify which components to bill to this person.  Additional Information is an internal field and will not show on customer correspondence.";
  $defClass = "opt";
  echo "<tr class='content'><td class='l'>$inpLabel</td><td class='r'>";
  echo "<textarea class='$defClass' name='$inpField' id='$inpField' rows='4' cols='48'>$defValue</textarea>";
  echo "&nbsp;<img class='icon16' title='$inpHelp' alt='Help' src='icons/question16.png' />";
  echo "</td></tr>";


  $arrButtonRow[] = "<input class='inp12' type='submit' name='subAddAddrPr' id='subAddAddrPr' value='Add' />";
  $arrButtonRow[] = "<input class='inp12' type='submit' name='subChgAddrPr' id='subChgAddrPr' value='Change' />";
  $arrButtonRow[] = "<input class='inp08' type='button' name='subCan' id='subCan' value='Cancel' onclick='fnCloseAddressPr();' />";

  echo "<tr><td></td><td>" . implode("", $arrButtonRow) . "</td></tr>";

  echo "</table>";

  echo "</div>"; // content
  echo "<span id='spanConfirm'></span>";
  echo "</form>";
  echo "</div>"; // body

  echo "<div class='body_stripe'></div>";
  echo "</div>"; // fixed
  echo "</div>"; // modal

  if (isset($_SESSION['inpValue']['subAddAddrPr']) || isset($_SESSION['inpValue']['subChgAddrPr'])) {
    echo "\n<script language=\"JavaScript\">";
    echo "\n<!-- Hide the script from old browsers --";

    echo "\nvar objAddressP = {};"; // Create a new object to hold the posted values, otherwise it updates the last one displayed earlier

    if (isset($_SESSION['inpValue']['hidAddressIDPr'])) {
      echo "\nobjAddressP.id     = \"" . prepareJS($_SESSION['inpValue']['hidAddressIDPr']) . "\";";
    }
    echo "\nobjAddressP.fname    = \"" . prepareJS($_SESSION['inpValue']['inpFNamePr'])     . "\";";
    echo "\nobjAddressP.lname    = \"" . prepareJS($_SESSION['inpValue']['inpLNamePr'])     . "\";";
    echo "\nobjAddressP.title    = \"" . prepareJS($_SESSION['inpValue']['inpTitlePr'])     . "\";";
    echo "\nobjAddressP.company  = \"" . prepareJS($_SESSION['inpValue']['inpCompanyPr'])   . "\";";
    echo "\nobjAddressP.line1    = \"" . prepareJS($_SESSION['inpValue']['inpLine1Pr'])     . "\";";
    echo "\nobjAddressP.line2    = \"" . prepareJS($_SESSION['inpValue']['inpLine2Pr'])     . "\";";
    echo "\nobjAddressP.city     = \"" . prepareJS($_SESSION['inpValue']['inpCityPr'])      . "\";";
    echo "\nobjAddressP.state    = \"" . $_SESSION['inpValue']['selStatePr']   . "\";";
    echo "\nobjAddressP.zip      = \"" . $_SESSION['inpValue']['inpZipPr']     . "\";";
    echo "\nobjAddressP.email    = \"" . prepareJS($_SESSION['inpValue']['inpEmailPr'])     . "\";";
    echo "\nobjAddressP.telp     = \"" . formatPhone($_SESSION['inpValue']['inpTelpPr'])    . "\";";
    echo "\nobjAddressP.telc     = \"" . formatPhone($_SESSION['inpValue']['inpTelcPr'])    . "\";";
    echo "\nobjAddressP.telw     = \"" . formatPhone($_SESSION['inpValue']['inpTelwPr'])    . "\";";
    echo "\nobjAddressP.extp     = \"" . prepareJS($_SESSION['inpValue']['inpExtpPr'])      . "\";";
    echo "\nobjAddressP.extc     = \"" . prepareJS($_SESSION['inpValue']['inpExtcPr'])      . "\";";
    echo "\nobjAddressP.extw     = \"" . prepareJS($_SESSION['inpValue']['inpExtwPr'])      . "\";";
    echo "\nobjAddressP.relation = \"" . prepareJS($_SESSION['inpValue']['inpRelationPr'])  . "\";";
    echo "\nobjAddressP.info     = \"" . prepareJS($_SESSION['inpValue']['inpInfoPr'])      . "\";";

    foreach($_SESSION['errField'] AS $field) {

      echo "\narrErrorPr.push(\"" . $field    . "\");";
      echo "\ndocument.getElementById(\"" . $field . "\").classList.add(\"err\");";

    }

    echo "\narrAddressP[0] = objAddressP;"; // Put this object into the array of addresses

    if (isset($_SESSION['inpValue']['subAddAddrPr'])) { echo "\nfnFillFormPr(0);";
                                                  echo "\nfnAddAddressPr();";   }
    if (isset($_SESSION['inpValue']['subChgAddrPr'])) { echo "\nfnEditAddressPr(0);"; }

    echo "// --End Hiding Here -->\n";
    echo "\n</script>\n";
  }
 unset($arrButtonRow);
?>