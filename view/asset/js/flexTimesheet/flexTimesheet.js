import {Util} from "../util.js"
import {FlexTimesheetItem} from "./flexTimesheetItem.js"
import note from "../modules/lib/note.js"

const OPTS_DEFAULT = {
  infoSection: false,
  breakSection: false,
  billableUpdate: true,
  roleInsert: false,
  limit: true,
  crew: false,
  columns: {
    link: true,
    linkJob: true,
    invoice: false,
    role: false,
    resp: true
  }
}

/**
 * A more flexible version of scripts/jif/timesheet.js (which is only used on the jif timesheet tabs)
 * @author Jake Cirino
 */
export class FlexTimesheet{
  /**
   *
   * @param {Object} data
   * @param {Number} id The id of this page, whether its an emp id, jc job, ect.
   * @param {Object} [data.emp] The employee data
   * @param {Object} [data.crew] Data on the crew for this job
   * @param {Object} data.items
   *
   * @param {Object} opts
   * @param {boolean} opts.infoSection Whether to include the info section
   * @param {boolean} opts.breakSection Whether to include the lunch/breaks section
   * @param {boolean} opts.billableUpdate If true, billable checkbox for activities will be checked
   * based on the value of act_def_billable, otherwise it will always default to unchecked
   * @param {boolean} opts.roleInsert Allow select of role on insert, or to pull roles from crew table
   * @param {boolean} opts.navigator Whether to show/use the filters/navigator
   * @param {boolean} opts.crew Whether to show/use the crew selector
   * @param {Object} opts.columns List of visible columns
   * @param {boolean} opts.columns.link Link type selector to link a ts card to a job
   * @param {boolean} opts.columns.linkJob JobID for a linked job
   * @param {boolean} opts.columns.invoice Select invoice for the job this item is on
   * @param {boolean} opts.columns.role Choose the role for this emp on the job
   * @param {boolean} opts.columns.resp The respirator checkbox
   */
  constructor(data, opts) {
    this.data = data
    this.items = []
    
    //add stylesheet to header
    let link = document.createElement('link')
    link.rel = 'stylesheet'
    link.type = 'text/css'
    link.href = 'styles/flexTimesheetStyles.css'
    document.head.appendChild(link)
    
    //combine provided options and defaults
    this.opts = Object.assign({}, OPTS_DEFAULT, opts)
    this.opts.columns = Object.assign({}, OPTS_DEFAULT.columns, opts.columns)
    
    //retrieve base element
    this.baseElement = document.querySelector('#flexTimesheet')
    this.dom = Util.parseDOM(this.baseElement)
  
    //initiate options and selectors
    this.initiateOptions()
    this.initiateDatepickers()
    this.initiateSelectors()
    this.initiateButtons()
    
    //add items
    for (const itemsKey in this.data.items) {
      const itemData = this.data.items[itemsKey]
      this.addItem(itemData)
    }

    document.addEventListener('crewChangeEvent', ev => {

      const crewBlock = document.querySelector('[name=blockCrewSelector]')
      if (!crewBlock) return
      const assigned = crewBlock.querySelector('[name=assigned]')
      
      fetch('timesheetPRG.php', {
        method: 'post',
        headers: {'Content-Type': 'application/json;charset=utf8'},
        body: JSON.stringify({
          action: 'getTimesheetEmployees',
          jobID: this.data.id
        })
      })
      .then(res => res.json())
      .then(body => {
        if(body.ok){
          this.dom.ts_emp_multi.setItems(body.data)
        }
      })
    })
  }
  
  /**
   * Initiates the UI based off the options provided
   */
  initiateOptions(){
    //include info section at the top
    if(this.opts.infoSection){
      //TODO info section functionality
    }else{
      //Util.hide(this.dom.infoSection)
      //Util.hide(this.dom.infoSeparator)
    }
  
    //include the break/lunch section on the bottom
    if(this.opts.breakSection){
      //TODO break section functionality
    }else{
      Util.hide(this.dom.breakSection)
    }
    
    //include the navigator/date if relevant
    if(this.opts.navigator){
      this.currentDate = new Date()
      this.currentDate.setHours(0, 0, 0, 0)
      this.navDebounce = true //allows navigator buttons to be used
      this.initiateNavigator()
    }else{
      Util.hide(this.dom.pageNavigator)
    }
    
    //show role selector on insert
    if(!this.opts.roleInsert){
      Util.hide(this.dom.ts_crew_type)
    }
    
    //hide specified columns
    for (const columnsKey in this.opts.columns) {
      const val = this.opts.columns[columnsKey]
      
      if(!val){
        Util.hide(this.dom[columnsKey + "Head"])
        Util.hide(this.dom[columnsKey + "Insert"])
      }
    }
  }
  
  /**
   * Initiates the datepickers
   */
  initiateDatepickers(){
    let startDate = $("[name|='startDate']")
    startDate.datepicker({
      onSelect: this.updateDuration.bind(this)
    })
    startDate.datepicker('option', 'dateFormat', 'mm/dd/y')
    this.dom.startDate.name = ''
  
    let endDate = $("[name|='endDate']")
    endDate.datepicker({
      onSelect: this.updateDuration.bind(this)
    })
    endDate.datepicker('option', 'dateFormat', 'mm/dd/y')
    this.dom.endDate.name = ''
    
    //set default dates to current date
    let currentDate = Util.dateToString(new Date()).split(' ')[0]
    this.dom.startDate.value = currentDate
    this.dom.endDate.value = currentDate
  }
  
  /**
   * Initiates the selectors with their default values
   */
  initiateSelectors(){
    //populate activities
    for (const activitiesKey in window.activities) {
      const item = window.activities[activitiesKey]
      let opt = document.createElement('option')
      opt.value = item.act_id
      opt.innerText = item.act_dsca
      this.dom.ts_activity.appendChild(opt)
    }
    
    //populate role insert field if applicable
    if(this.opts.columns.role && this.opts.roleInsert) {
      //check if employee can be a sup or tech
      let isSup = false,
        isTec = false
      for (const empRolesKey in window.emp_roles) {
        const val = window.emp_roles[empRolesKey].role_code
        if (val == 'SUP' || val == 'BAC') {
          isSup = true
          isTec = true
          break
        }
        if (val == 'TEC') isTec = true
      }
      if (isSup) {
        let opt = document.createElement('option')
        opt.value = '1'
        opt.innerText = 'S'
        this.dom.ts_crew_type.appendChild(opt)
      }
      if (isTec) {
        let opt = document.createElement('option')
        opt.value = '2'
        opt.innerText = 'T'
        this.dom.ts_crew_type.appendChild(opt)
      }
    }
    
    //populate employee multi-select if applicable
    if(this.data.emp !== undefined){
      //populate with a single employee
      this.dom.ts_emp_multi.setItems(this.data.emp)
    }else if(this.data.crew !== undefined){
      //populate with crew data
      this.dom.ts_emp_multi.setItems(this.data.crew)
    }
    
    //update bill/resp checkboxes on activity update
    this.dom.ts_activity.addEventListener('change', this.updateActivityOptions.bind(this))
    
    //change events for time
    this.dom.startTime.addEventListener('change', this.updateEndtime.bind(this))
    this.dom.endTime.addEventListener('change', this.updateDuration.bind(this))
  }
  
  /**
   * Initiates the buttons on the page
   */
  initiateButtons(){
    this.dom.linkJobInput.disabled = true
    this.dom.linkInput.addEventListener('change', ((event) => {
      console.log('checked')
      this.setJobLinkEnabled(event.target.checked)
    }).bind(this))
    
    this.dom.addButton.addEventListener('click', this.insertItem.bind(this))
  }
  
  /**
   * Initiates the navigator section
   */
  initiateNavigator(){
    //initiate date picker
    let currentDate = Util.dateToString(new Date())
    $("[name|='navigateDate']").datepicker({
      onSelect: (date) => {
        this.filterDate(new Date(date))
      }
    })
    $("[name|='navigateDate']").datepicker('option', 'dateFormat', 'mm/dd/y')
    this.dom.navigateDate.value = currentDate.split(' ')[0]
    
    //navigate buttons
    this.dom.navigatePrev.addEventListener('click', () => {
      if(this.navDebounce){
        let newDate = new Date(this.currentDate.getTime())
        newDate.setDate(newDate.getDate() - 1)
        this.filterDate(new Date(newDate))
      }
    })
    this.dom.navigateNext.addEventListener('click', () => {
      if(this.navDebounce){
        let newDate = new Date(this.currentDate.getTime())
        newDate.setDate(newDate.getDate() + 1)
        this.filterDate(newDate)
      }
    })
  }
  
  /**
   * Adds a new item to the end of the timesheet table
   * @param {Object} data
   */
  addItem(data){
    let item = new FlexTimesheetItem(this, data)
    this.items.push(item)
  }
  
  /**
   * Clears all the items in the array
   */
  clear(){
    while(this.items.length > 0){
      this.items.pop().remove();
    }
  }
  
  /**
   * Inserts an item when the user hits the add button
   * @param {Object} emp The employee to add
   */
  insertItem(emp){
    //collect data from insert fields
    let startDT = Util.parseDate(this.dom.startDate.value, this.dom.startTime.value),
      endDT = Util.parseDate(this.dom.endDate.value, this.dom.endTime.value)
    
    //get employees
    let employees = [];
    let selected = this.dom.ts_emp_multi.getSelected()
    for (const selectedKey in selected) {
      employees.push(JSON.parse(selected[selectedKey]))
    }
    
    let data = {
      action: 'insertMultiItems',
      ts_job: null,
      ts_seq: this.opts.columns.invoice ? this.dom.ts_seq.value : 1,
      ts_activity: this.dom.ts_activity.value,
      employees: employees,
      ts_emp: this.data.emp !== undefined ? this.data.id : null,
      ts_is_billable: this.dom.ts_is_billable.checked,
      ts_respirator_worn: this.dom.ts_respirator_worn.checked,
      ts_dt_fr: Util.dateToString(startDT),
      ts_dt_to: Util.dateToString(endDT),
      ts_duration: this.dom.duration.value.split(' ')[0]
    }
    
    //if this is the employee page, we need to append the current crew type select
    if(employees[0].ts_crew_type === undefined){
      employees[0].ts_crew_type = this.dom.ts_crew_type.value
    }
  
    //set job link values if applicable
    if(this.opts.columns.link && this.opts.columns.linkJob && this.dom.linkInput.checked){
      let jobID = this.dom.linkJobInput.value //pull job id from insert field
      data.ts_job = jobID
    }else{
      data.ts_job = this.data.id
    }
  
    fetch('flexTimesheetPRG.php', {
      method: 'post',
      headers: {'Content-Type': 'application/json;charset=utf8'},
      body: JSON.stringify(data)
    })
      .then(res => res.json())
      .then(body => {
        if(body.ok){
          //filter date to ts added if the date navigator is enabled
          if(this.opts.navigator){
            startDT.setHours(0, 0, 0, 0)
            this.filterDate(startDT)
          }else{
            for (const msgKey in body.msg) {
              const item = body.msg[msgKey]
              this.addItem(item)
            }
          }
        
          this.clearForm()
        }else{
          note({
            text: 'An error occurred trying to add this timesheet item',
            class: 'error',
            time: 4000
          })
        }
      })
  }
  
  /**
   * Sets the date filte r on this timesheet
   * @param {Date} date
   */
  filterDate(date){
    //return early if same as current date or debounce is false
    if(this.currentDate == date) return
    this.navDebounce = false
    this.currentDate = date
    this.dom.navigateDate.value = Util.dateToString(date).split(' ')[0]
    
    //get start/end dates
    let startDate = Util.dateToString(date.getTime()),
      endDate = Util.dateToString(new Date(date.getTime() + 1000*60*60*24))
    
    //separate prev/new items
    let prevItems = this.items
    this.items = []
    
    //fetch new data
    fetch('flexTimesheetPRG.php', {
      method: 'post',
      headers: {'Content-Type': 'application/json;charset=utf8'},
      body: JSON.stringify({
        action: 'getEmpItemsDate',
        empID: this.data.id,
        startDate: startDate,
        endDate: endDate
      })
    })
      .then(res => res.json())
      .then(body => {
        if(body.ok){
          //add new items
          for (const dataKey in body.data) {
            this.addItem(body.data[dataKey])
          }
  
          //remove previous page items
          for (const prevItemsKey in prevItems) {
            prevItems[prevItemsKey].remove()
          }
        }else{
          this.items = prevItems
          note({
            text: 'An error occurred loading the timesheet items',
            class: 'error',
            time:4000
          })
        }
  
        //reset debounce
        this.navDebounce = true
      })
  }
  
  /**
   * Sets whether the job link input is enabled
   * @param {Boolean} enabled
   */
  setJobLinkEnabled(enabled){
    if(enabled){
      this.dom.linkJobInput.disabled = false
    }else{
      this.dom.linkJobInput.disabled = true
      this.dom.linkJobInput.value = ""
    }
  }
  
  /**
   * Updates billable and respirator fields depending on the activity chosen
   */
  updateActivityOptions(){
    try{
      let actData = this.getActivityData(this.dom.ts_activity.value)
      if(actData == null) return //return early if there is no activity data
      
      //billable disabled on emp page for now
      if(this.opts.billableUpdate)
        this.dom.ts_is_billable.checked = actData.act_def_billable == 1 ? true : false
      this.dom.ts_respirator_worn.checked = actData.act_def_respirator == 1 ? true : false
    }catch(ex) {}
  }
  
  /**
   * Updates the end date/time to be ahead of the currently selected dt/time
   */
  updateEndtime(){
    try{
      //get start DT
      let startDT = Util.parseDate(this.dom.startDate.value, this.dom.startTime.value)
      
      //calculate next time
      const actID = this.dom.ts_activity.value
      let timeframe = 15
      try{
        if(Number.parseInt(actID) == 92){ //lunch is 30m
          timeframe = 30
        }
      }catch(ex){}
  
      let nextTime = new Date(startDT.getTime() + timeframe*60000) //increase time by timeframe
      nextTime = Util.dateToString(nextTime).split(' ')
      
      //set datetime values and recalc duration
      this.dom.endDate.value = nextTime[0]
      this.dom.endTime.value = nextTime[1]
      this.updateDuration()
    }catch(ex){}
  }
  
  /**
   * Recalculates and updates the duration field
   * @returns {boolean}
   */
  updateDuration() {
    try{
      let startDate = this.dom.startDate.value,
        startTime = this.dom.startTime.value,
        endDate = this.dom.endDate.value,
        endTime = this.dom.endTime.value,
        startDatetime = Util.parseDate(startDate, startTime),
        endDatetime = Util.parseDate(endDate, endTime)
      
      let duration = (endDatetime.getTime() - startDatetime.getTime()) / 1000 / 60 / 60
      
      //hightlight based off duration
      if(duration >= 12){
        this.baseElement.style.backgroundColor = '#f99'
      }else if(duration >= 8){
        this.baseElement.style.backgroundColor = '#ff9'
      }
      
      duration = duration.toFixed(2)
      duration = `${duration} Hr`
      this.dom.duration.value = duration
    } catch (err) {
      this.dom.duration.value = '0.00 Hr'
      return false
    }
  }
  
  /**
   * Clears all the values in the form, also increments the start/end datetime
   */
  clearForm(){
    this.dom.ts_seq.value = ''
    this.dom.linkJobInput.value = ''
    this.dom.ts_activity.value = ''
    this.dom.ts_crew_type.value = ''
    this.dom.ts_is_billable.checked = false
    this.dom.ts_respirator_worn.checked = false
    
    //TODO variable timing, specific defaults
    let nextEnd = Util.parseDate(this.dom.endDate.value, this.dom.endTime.value)
    nextEnd = new Date(nextEnd.getTime() + 15*60000) //advance 15 mins
    nextEnd = Util.dateToString(nextEnd).split(' ')
    this.dom.startDate.value = this.dom.endDate.value
    this.dom.startTime.value = this.dom.endTime.value
    this.dom.endDate.value = nextEnd[0]
    this.dom.endTime.value = nextEnd[1]
    
    //focus on first element
    this.dom.ts_activity.focus()
  }
  
  /**
   * Gets activity data from window.activites for a given act id
   * @param {Number} actID The activity id
   * @returns {null|Object}
   */
  getActivityData(actID){
    for (const activitiesKey in window.activities) {
      const data = window.activities[activitiesKey]
      if(data.act_id == actID) return data
    }
    
    return null
  }
}