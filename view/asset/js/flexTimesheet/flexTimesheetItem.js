/**
 * A row in the flex timesheet item table
 * @author Jake Cirino
 */
import {Util} from "../util.js"
import note from "../modules/lib/note.js"

export class FlexTimesheetItem {
  /**
   * Creates a new flex timesheet item
   * @param {FlexTimesheet} parent
   * @param {Object} data Contains the data for this object
   */
  constructor(parent, data) {
    this.parent = parent
    this.data = data
    this.opts = this.parent.opts

    console.log(this.data)
    
    //create baseElement
    this.baseElement = document.createElement('tr')
    this.baseElement.innerHTML = window.itemTemplate
    this.dom = Util.parseDOM(this.baseElement)
    
    //initialize dom items
    this.initiateButtons()
    this.initiateSelectors()
    
    //populate data and set edit mode intitially to false
    this.setEditMode(false)
    
    //append base element to parent
    let tableBody = this.parent.dom.data_body
    tableBody.insertBefore(this.baseElement, tableBody.lastElementChild)
    
    //initiate datepicker and populate
    this.initiateDatepickers()
    this.populate()
  }
  
  /**
   * Initiates the datepicker objects
   */
  initiateDatepickers(){
    let dateStartField = $("[name|='dateStartField']")
    dateStartField.datepicker({
      onSelect: this.updateDuration.bind(this)
    })
    dateStartField.datepicker('option', 'dateFormat', 'mm/dd/y')
    this.dom.dateStartField.name = ''
    
    let dateEndField = $("[name|='dateEndField']")
    dateEndField.datepicker({
      onSelect: this.updateDuration.bind(this)
    })
    dateEndField.datepicker('option', 'dateFormat', 'mm/dd/y')
    this.dom.dateEndField.name = ''
  }
  
  /**
   * Adds functionality to the buttons of this item
   */
  initiateButtons() {
    this.dom.editButton.addEventListener('click', () => {
      this.setEditMode(true)
    })
    
    this.dom.deleteButton.addEventListener('click', this.delete.bind(this))
    this.dom.saveButton.addEventListener('click', this.save.bind(this))
  }
  
  /**
   * Populates the selector elements with their respective values
   */
  initiateSelectors() {
    //populate link selector
    let linkText = 'None',
      linkValue = ''
    if(this.opts.columns.link){
      if(this.data.ts_job != null) {
        linkValue = 'job'
        linkText = 'Job'
      }
      
      this.dom.link.innerText = linkText
      this.dom.linkField.value = linkValue
    }
    
    //populate activity selector
    for (const activitiesKey in window.activities) {
      const item = window.activities[activitiesKey]
      let opt = document.createElement('option')
      opt.value = item.act_id
      opt.innerText = item.act_dsca
      this.dom.activityField.appendChild(opt)
    }
  
    //populate role if applicable
    if(this.opts.columns.role){
      //check if employee can be a sup or tech
      let isSup = false,
        isTec = false
      for (const empRolesKey in window.emp_roles) {
        const val = window.emp_roles[empRolesKey].role_code
        if(val == 'SUP' || val == 'BAC') {
          isSup = true
          isTec = true
          break
        }
        if(val == 'TEC') isTec = true
      }
      if(isSup){
        let opt = document.createElement('option')
        opt.value = '1'
        opt.innerText = 'S'
        this.dom.roleField.appendChild(opt)
      }
      if(isTec){
        let opt = document.createElement('option')
        opt.value = '2'
        opt.innerText = 'T'
        this.dom.roleField.appendChild(opt)
      }
    }
    
    //populate emp selector
    let opt = document.createElement('option')
    opt.value = this.data.ts_emp
    opt.innerText = `${this.data.emp_fname} ${this.data.emp_lname}`
    this.dom.empField.appendChild(opt)
  }
  
  /**
   * Populates the form with data from this.data
   */
  populate() {
    //split datetime values
    let dateTimeStart = this.data.ts_dt_fr.split(' '),
      dateTimeEnd = this.data.ts_dt_to.split(' ')
    
    //setup link
    if (!this.opts.columns.link) {
      Util.hide(this.dom.link)
      Util.hide(this.dom.linkEdit)
    }
    
    //setup link job
    if (this.opts.columns.linkJob) {
      if(this.data.ts_job != null){
        this.dom.linkJobHref.innerText = this.data.job_yrnum
        let link = this.data.act_dsca == "Jail Cell" ? 'jcJob' : 'job'
        this.dom.linkJobHref.href = `${link}.php?job=${this.data.ts_job}`
        this.dom.linkJobField.value = this.data.job_yrnum
      }else{
        this.dom.linkJob.innerText = ''
        this.dom.linkJobField.value = ''
      }
    } else {
      Util.hide(this.dom.linkJob)
      Util.hide(this.dom.linkJobEdit)
    }
    
    //setup ts_scope
    if (this.opts.columns.invoice) {
      this.dom.ts_scope.innerText = this.data.ts_scope
      this.dom.scopeField.value = this.data.ts_scope
    } else {
      Util.hide(this.dom.ts_scope)
      Util.hide(this.dom.scopeEdit)
    }
    
    //setup ts_crew_type/role
    if (this.opts.columns.role) {
      let typeText
      if(this.data.ts_crew_type == 1) typeText = 'S'
      else if(this.data.ts_crew_type == 2) typeText = 'T'
      else typeText = ''
      this.dom.ts_type.innerText = typeText
      this.dom.roleField.value = this.data.ts_crew_type
    } else {
      Util.hide(this.dom.ts_type)
      Util.hide(this.dom.roleEdit)
    }
    
    if(!this.opts.columns.resp){
      Util.hide(this.dom.ts_respirator_worn);
      Util.hide(this.dom.respiratorEdit);
    }
    
    //setup constant fields
    let activityItem = this.getActivityItem()
    this.dom.ts_activity.innerText = activityItem.act_dsca
    this.dom.activityField.value = this.data.ts_activity
    
    this.dom.ts_emp.innerText = `${this.data.emp_fname} ${this.data.emp_lname}`
    this.dom.empField.value = this.data.ts_emp
    
    this.dom.ts_is_billable.innerText = this.data.ts_is_billable == 1 ? 'Y' : 'N'
    this.dom.billableField.checked = this.data.ts_is_billable == 1
    
    this.dom.ts_respirator_worn.innerText = this.data.ts_respirator_worn == 1 ? 'Y' : 'N'
    this.dom.respiratorField.checked = this.data.ts_respirator_worn == 1
    
    this.dom.dateStart.innerText = dateTimeStart[0]
    this.dom.dateStartField.value = dateTimeStart[0]
    
    this.dom.timeStart.innerText = dateTimeStart[1]
    this.dom.timeStartField.value = dateTimeStart[1]
    
    this.dom.dateEnd.innerText = dateTimeEnd[0]
    this.dom.dateEndField.value = dateTimeEnd[0]
    
    this.dom.timeEnd.innerText = dateTimeEnd[1]
    this.dom.timeEndField.value = dateTimeEnd[1]
    
    this.updateDuration()
  }
  
  /**
   * Sets if this item is in edit mode
   * @param {boolean} editMode
   */
  setEditMode(editMode) {
    this.editMode = editMode
    
    //we either need to call Util.hide or Util.show on elements
    let view = editMode ? Util.hide : Util.show,
      edit = editMode ? Util.show : Util.hide
    
    this.baseElement.className = editMode ? 'table-input' : ''
    
    //show optional fields
    if (this.opts.columns.invoice) {
      view(this.dom.ts_scope)
      edit(this.dom.scopeEdit)
    }
    if (this.opts.columns.link) {
      view(this.dom.link)
      edit(this.dom.linkEdit)
    }
    if (this.opts.columns.linkJob) {
      view(this.dom.linkJob)
      edit(this.dom.linkJobEdit)
    }
    if (this.opts.columns.role) {
      view(this.dom.ts_type)
      edit(this.dom.roleEdit)
    }
    if (this.opts.columns.resp){
      view(this.dom.ts_respirator_worn)
      edit(this.dom.respiratorEdit)
    }
    
    //show static fields
    view(this.dom.ts_activity)
    view(this.dom.ts_emp)
    view(this.dom.ts_is_billable)
    view(this.dom.dateStart)
    view(this.dom.timeStart)
    view(this.dom.dateEnd)
    view(this.dom.timeEnd)
    view(this.dom.duration)
    view(this.dom.iconButtons)
    
    edit(this.dom.activityEdit)
    edit(this.dom.empEdit)
    edit(this.dom.billableEdit)
    edit(this.dom.dateStartEdit)
    edit(this.dom.timeStartEdit)
    edit(this.dom.dateEndEdit)
    edit(this.dom.timeEndEdit)
    edit(this.dom.durationEdit)
    edit(this.dom.saveEdit)
  }
  
  /**
   * Gets the activity item for this timesheet item
   * @returns {null|Object}
   */
  getActivityItem() {
    for (const activitiesKey in window.activities) {
      const item = window.activities[activitiesKey]
      if (item.act_id == this.data.ts_activity)
        return item
    }
    
    return null
  }
  
  
  /**
   * Recalculates and updates the duration field
   * @returns {boolean}
   */
  updateDuration() {
    try{
      let startDate = this.dom.dateStartField.value,
        startTime = this.dom.timeStartField.value,
        endDate = this.dom.dateEndField.value,
        endTime = this.dom.timeEndField.value,
        startDatetime = Util.parseDate(startDate, startTime),
        endDatetime = Util.parseDate(endDate, endTime)
      
      let duration = (endDatetime.getTime() - startDatetime.getTime()) / 1000 / 60 / 60
      this.data.ts_duration = duration
      
      //hightlight based off duration
      if(duration >= 12){
        this.baseElement.style.backgroundColor = '#f99'
      }else if(duration >= 8){
        this.baseElement.style.backgroundColor = '#ff9'
      }
      
      duration = duration.toFixed(2)
      duration = `${duration} Hr`
      this.dom.durationField.value = duration
      this.dom.duration.innerText = duration
    } catch (err) {
      this.dom.durationField.value = '0.00 Hr'
      this.dom.duration.innerText = '0.00 Hr'
      return false
    }
  }
  
  /**
   * Deletes the item from the dom and frees its memory
   */
  remove() {
    this.baseElement.remove()
    delete this
  }
  
  /**
   * Deletes the database record of this item and then calls {@link remove}
   */
  delete() {
    fetch('flexTimesheetPRG.php', {
      method: 'post',
      headers: {'Content-Type': 'application/json;charset=utf8'},
      body: JSON.stringify({
        action: 'deleteItem',
        tsID: this.data.ts_id
      })
    })
      .then(res => res.json())
      .then(body => {
        if (body.ok) {
          this.remove()
        } else {
          note({
            text: 'Error deleting timesheet item.',
            class: 'error',
            time: 4000
          })
        }
      })
  }
  
  save(){
    //TODO optional fields
    // this.data.ts_crew_type = this.dom.roleField.value
    // this.data.ts_emp = this.dom.empField.value
    // this.data.ts_seq = this.dom.scopeField.value
    
    //update data values
    this.data.ts_activity = this.dom.activityField.value
    this.data.ts_is_billable = this.dom.billableField.checked
    this.data.ts_respirator_worn = this.dom.respiratorField.checked
    let startDT = Util.parseDate(this.dom.dateStartField.value, this.dom.timeStartField.value)
    let endDT = Util.parseDate(this.dom.dateEndField.value, this.dom.timeEndField.value)
    this.data.ts_dt_fr = Util.dateToString(startDT)
    this.data.ts_dt_to = Util.dateToString(endDT)
    this.data.ts_duration = this.dom.durationField.value.split(' ')[0]
    if(this.opts.columns.role){
      this.data.ts_crew_type = this.dom.roleField.value
    }
    if(this.opts.columns.link && this.opts.columns.linkJob){
      let jobType = this.dom.linkField.value
      let jobID = this.dom.linkJobField.value
      
      if(jobType == 'job'){
        this.data.ts_job = jobID
      }else{
        this.data.ts_job = null
      }
    }
  
    //TODO verify fields
    
    //adjust endDatetime to next day if needed
    if (endDT.getTime() - startDT.getTime() < 0)
      endDT.setTime(endDT.getTime() + 1000 * 60 * 60 * 24)
    
    //prepare data and post
    fetch('flexTimesheetPRG.php', {
      method: 'post',
      headers: {'Content-Type': 'application/json;charset=utf8'},
      body: JSON.stringify({
        action: 'updateItem',
        ts_job: this.data.ts_job,
        ts_id: this.data.ts_id,
        ts_activity: this.data.ts_activity,
        ts_crew_type: this.data.ts_crew_type,
        ts_emp: this.data.ts_emp,
        ts_seq: this.data.ts_seq,
        ts_is_billable: this.data.ts_is_billable,
        ts_dt_fr: this.data.ts_dt_fr,
        ts_dt_to: this.data.ts_dt_to,
        ts_duration: this.data.ts_duration
      })
    })
      .then(res => res.json())
      .then(body => {
        if(body.ok){
          this.populate()
          this.setEditMode(false)
          //TODO highlight overlaps?
          if(window.jifController !== undefined)
            window.jifController.update()
        }else{
          note({
            text: 'An error occurred editing the timesheet item',
            class: 'error',
            time: 4000
          })
        }
      })
  }
}