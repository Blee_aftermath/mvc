<?php

// *************************** MODAL ***************************

  echo "<div class='modal' id='myModalPrc'>";
  echo "<div class='fixed'>";
  echo "<div class='body_stripe'></div>";

  echo "<div class='head'>";
  echo "<div class='content'>";

  echo "<span id='spanPrcHead'></span>";
  echo "<span style='float:right;'><a href='javascript:fnCloseModalPrc();'><img class='icon16' src='icons/no16.png' alt='Close' title='Close' /></a></span>";

  echo "</div>"; // content
  echo "</div>"; // head

  echo "<span id='spanPrcMessages'>";
  fnModalMessages();
  echo "</span>";

  echo "<div class='body'>";
  echo "\n<form method='post' name='frmModal' action='modalPRG.php'>";
  echo "<div class='content'>";

  echo "<input type='hidden' name='hidPrcID' id='hidPrcID' />";

  echo "<table class='info' id='tblPrc' style='display:none;'>";

  $inpField = "reoSkuNum";
  $inpLabel = "SKU";
  $defValue = "$frmSkuNum";
  $defClass = "inp16 reo";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' value='" . $defValue . "' readonly />";
  echo "</td></tr>";

  $inpField = "inpAmtCost";
  $inpLabel = "Price";
  $defValue = "";
  $defClass = "inp08 req";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td>";
  echo "$&nbsp;<input class='$defClass' type='text' name='$inpField' id='$inpField' value='$defValue' maxlength='" . $maxAmt . "' />";
  echo "</td></tr>";

  $inpField = "inpDEff";
  $inpLabel = "Effective";
  $defValue = "";
  $defClass = "inp16 req";
  $inpHelp  = "Enter the date this price will take effect.";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' value='" . $defValue . "' maxlength='$maxName' list='off' />";
  echo "&nbsp;<img class='icon16' title='$inpHelp' alt='Help' src='icons/question16.png' />";
  echo "</td></tr>";

  echo "<script>$(function() { $('#".  $inpField . "').datepicker(); }); </script>";

  $arrButtonRowPrc[] = "<input class='inp12' type='submit' name='subAddPrc' id='subAddPrc' value='Add' />";
  $arrButtonRowPrc[] = "<input class='inp12' type='submit' name='subChgPrc' id='subChgPrc' value='Save' />";
  $arrButtonRowPrc[] = "<input class='inp12' type='button' name='butConPrc' id='butConPrc' value='Delete' onclick='fnConfirmDelPrc();' />";
  $arrButtonRowPrc[] = "<input class='inp12' type='button' name='butCanPrc' id='butCanPrc' value='Cancel' onclick='fnCloseModalPrc();' />";

  echo "<tr class='content'><td></td><td>" . implode("", $arrButtonRowPrc) . "</td></tr>";

  echo "</table>";

  echo "</div>"; // content
  echo "<span id='spanPrcConfirm'></span>";
  echo "</form>";
  echo "</div>"; // body

  echo "<div class='body_stripe'></div>";
  echo "</div>"; // fixed
  echo "</div>"; // modal

// *************************** LOAD ***************************

  if (isset($_SESSION['inpValue']['subAddPrc']) || isset($_SESSION['inpValue']['subChgPrc'])) {

    echo "\n<script language=\"JavaScript\">";
    echo "\n<!-- Hide the script from old browsers --";

    echo "\nvar objPrc = {};"; // Create a new object to hold the posted values.

    if (isset($_SESSION['inpValue']['hidPrcID'])) {
      echo "\narrPrcID.push(" . $_SESSION['inpValue']['hidPrcID'] . ");";
      echo "\nobjPrc.id     = \"" . $_SESSION['inpValue']['hidPrcID'] . "\";";
    }

    echo "\nobjPrc.skuNum   = \"" . prepareJS($_SESSION['inpValue']['reoSkuNum'])  . "\";";
    echo "\nobjPrc.penCost  = \"" . prepareJS($_SESSION['inpValue']['inpAmtCost']) . "\";";
    echo "\nobjPrc.dtEff    = \"" . prepareJS($_SESSION['inpValue']['inpDEff'])    . "\";";

    foreach($_SESSION['errField'] AS $field) {

      echo "\narrPrcEr.push(\"" . $field    . "\");";
      echo "\ndocument.getElementById(\"" . $field . "\").classList.add(\"err\");";

    }

    echo "\narrPrcOb.push(objPrc);"; // Tack this onto the end of the array of SKU objects.

    if (isset($_SESSION['inpValue']['subAddPrc'])) echo "\nfnAddPrc();";
    if (isset($_SESSION['inpValue']['subChgPrc'])) echo "\nfnChgPrc(" . $_SESSION['inpValue']['hidPrcID'] . ");";
    echo "\nfnFillPrc(arrPrcOb.length-1);";

    echo "// --End Hiding Here -->\n";
    echo "\n</script>\n";
  }

?>