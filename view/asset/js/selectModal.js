const DEFAULT_OPTS = {
  title: 'Select Modal',
  okText: 'Ok',
  label: 'Select:',
  required: true
}

/**
 * Generic modal with a select box
 * @author Jake Cirino
 */
export class SelectModal{
  /**
   * Creates a new select modal
   * @param {Object} items List of the items in the select
   * @param {Object} opts Array of options
   * @param {Function} callback The callback function
   * @param {String} [opts.title] The title for the modal
   * @param {String} [opts.label] The text for the label of the select field
   * @param {String} [opts.okText] The text value for the 'ok'/'submit' button
   */
  constructor(items, opts = DEFAULT_OPTS, callback) {
    this.items = items
    this.callback = callback
    this.opts = opts
    
    //populate unfilled default opts
    for (const defaultoptsKey in DEFAULT_OPTS) {
      if(this.opts[defaultoptsKey] == undefined){
        this.opts[defaultoptsKey] = DEFAULT_OPTS[defaultoptsKey]
      }
    }
    
    this.baseElement = document.createElement('div')
    this.baseElement.className = 'modal'
    this.baseElement.id = 'modal-sel-tmp'
    this.baseElement.style.display = 'block'
   
    this.populate()
  }
  
  /**
   * Populates the modal with html and data
   * @returns {Promise<void>}
   */
  async populate(){
    let template = await window.templateController.getTemplate('selectModal')
    this.baseElement.innerHTML = template
    
    this.title = this.baseElement.querySelector('.title')
    this.selectLabel = this.baseElement.querySelector("[for='selectModal']")
    this.select = this.baseElement.querySelector('#selectModal')
    
    this.title.innerText = this.opts.title
    this.selectLabel.innerText = this.opts.label
    
    for (const itemsKey in this.items) {
      const item = this.items[itemsKey]
      
      let option = document.createElement('option')
      option.value = item.value
      option.innerText = item.text
      this.select.appendChild(option)
    }
    
    //setup buttons
    this.baseElement.querySelector('[name=action-yes]').addEventListener('click', (() => {
      if(this.select.value == ''){
        this.select.style.backgroundColor = '#f66'
      }else{
        this.close(true, this.select.value)
      }
    }).bind(this))
    let closeButtons = this.baseElement.querySelectorAll('[name=action-close]')
    for (const closeButtonsKey in closeButtons) {
      if(closeButtons.hasOwnProperty(closeButtonsKey)){
        const button = closeButtons[closeButtonsKey]
  
        button.addEventListener('click', (() => {
          this.close(false, null)
        }).bind(this))
      }
    }
    
    document.body.appendChild(this.baseElement)
  }
  
  /**
   * Closes the window and calls the callback function
   * @param {Boolean} result
   * @param {Object} selected
   */
  close(result, selected){
    this.baseElement.remove()
    this.callback(result, selected)
    delete this
  }
}