/**
 * A custom HTML element consisting of a dropdown menu with checkboxes
 */
class MultiSelectElement extends HTMLElement {
  constructor() {
    super()
    this.items = []

    //setup button
    this.innerText = this.getAttribute('header')
    this.tabIndex = 0

    //setup expander element
    this.expander = document.createElement('div')
    this.expander.className = 'multi-select-expander'
    this.expander.innerText = 'Testing testing testing'
    this.expander.style.top = this.style.bottom
    this.appendChild(this.expander)
    this.setExpanded(false)

    //append expander icon
    this.addEventListener(
      'click',
      ((event) => {
        if (event.target === this) this.setExpanded(!this.expanded)
      }).bind(this)
    )

    this.onkeydown = (event) => {
      if (event.key == 'Enter') {
        this.setExpanded(!this.expanded)
      }

      if(event.target === this && event.key == ' '){
        this.setExpanded(!this.expanded)
      }
    }
  }

  /**
   * Sets the items to be displayed in this event
   * @param {Array} items
   */
  setItems(items) {
    //clear html
    this.expander.innerHTML = ''

    //append new elements
    items.forEach((item) => {
      let itemElement = document.createElement('input')
      itemElement.type = 'checkbox'
      itemElement.id = item.crew_emp
      itemElement.value = JSON.stringify({
        emp_id: item.emp_id,
        ts_crew_type: item.crew_type})
      itemElement.checked = true
      itemElement.tabIndex = 0

      let itemLabel = document.createElement('label')
      let crewText = item.crew_type === undefined ? '' : item.crew_type == 1 ? ' - S' : ' - T'
      let labelText = `${item.emp_fname} ${item.emp_lname}${crewText}`
      itemLabel.innerText = labelText
      itemLabel.setAttribute('for', item.crew_emp)

      this.expander.appendChild(itemElement)
      this.expander.appendChild(itemLabel)
      this.expander.appendChild(document.createElement('br'))
    })
  }

  isFocused() {
    let focused = document.activeElement === this
    for (const key in this.expander.children) {
      if (this.expander.children.hasOwnProperty(key)) {
        const element = this.expander.children[key]
        focused = focused || document.activeElement === element
      }
    }
    console.log(focused)
  }

  setExpanded(expanded) {
    this.expanded = expanded

    this.expander.style.minWidth = this.getBoundingClientRect().width + 'px'
    this.expander.style.visibility = expanded ? '' : 'hidden'

    if (expanded) this.expander.children[0].focus()
    else this.focus()
  }

  /**
   * @returns {Array} A list of the values of the selected items
   */
  getSelected() {
    let values = []
    for (const key in this.expander.children) {
      if (this.expander.children.hasOwnProperty(key)) {
        const element = this.expander.children[key]
        if (element.checked) values.push(element.value)
      }
    }
    console.log(values)
    return values
  }
}

//define custom element
customElements.define('multi-select', MultiSelectElement)
