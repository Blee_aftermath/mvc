<?php
  echo "<div class='modal' id='modal-gsa-buyer' title='GSA Buyer'>\n";
  echo "<form id='form-gsa-buyer' action='gsaPRG.php' method='post'>\n";

  if (in_array('MASTER', $_SESSION['perm'])) {
    echo "<div style='text-align: right;'><img style='margin: -10px -14px 0 0; display: inline-block; cursor: pointer;' class='icon16 form-toggle' title='Edit' alt='Edit' src='icons/edit16.png' /></div>\n";
  }

  echo "<input type='hidden' name='inpGSAType' value='Buyer' />\n";

  echo "<p id='gsaBuyerID' style='margin-bottom: 8px;'>GSA Buyer ID: <span id='inpGSAID' style='font-weight: 700;'></span></p>\n";
  echo "<label class='hide'>GSA Buyer ID:&nbsp;<input class='inp04 reo' type='text' name='inpGSAID' readonly /></label>\n";

  echo "<p id='inpGSABuyerTitle1' style='text-decoration: underline;'></p>\n";
  echo "<label class='hide' style='margin-top: 8px;'>GSA Buyer Title 1:<br /><input class='req inp32' type='text' name='inpGSABuyerTitle1' /></label>\n";

  echo "<p id='inpGSABuyerDsca1' style='margin-bottom: 8px;'></p>\n";
  echo "<label class='hide' style='margin-top: 8px;'>GSA Buyer Description 1:<br /><textarea style='width: 500px; height: 50px;' class='req' name='inpGSABuyerDsca1'></textarea></label>\n";

  echo "<p id='inpGSABuyerTitle2' style='text-decoration: underline;'></p>\n";
  echo "<label class='hide' style='margin-top: 8px;'>GSA Buyer Title 2:<br /><input class='req inp32' type='text' name='inpGSABuyerTitle2' /></label>\n";

  echo "<p id='inpGSABuyerDsca2' style='margin-bottom: 8px;'></p>\n";
  echo "<label class='hide' style='margin-top: 8px;'>GSA Buyer Description 2:<br /><textarea style='width: 500px; height: 50px;' class='req' name='inpGSABuyerDsca2'></textarea></label>\n";

  echo "<p id='inpGSABuyerTitle3' style='text-decoration: underline;'></p>\n";
  echo "<label class='hide' style='margin-top: 8px;'>GSA Buyer Title 3:<br /><input class='req inp32' type='text' name='inpGSABuyerTitle3' /></label>\n";

  echo "<p id='inpGSABuyerDsca3' style='margin-bottom: 8px;'></p>\n";
  echo "<label class='hide' style='margin-top: 8px;'>GSA Buyer Description 3:<br /><textarea style='width: 500px; height: 50px;' class='req' name='inpGSABuyerDsca3'></textarea></label>\n";

  echo "<p id='inpGSABuyerDsca4' style='margin-bottom: 8px;'></p>\n";
  echo "<label class='hide' style='margin-top: 8px;'>GSA Buyer Description 4:<br /><textarea style='width: 500px; height: 50px;' class='req' name='inpGSABuyerDsca4'></textarea></label>\n";

  echo "<input class='inp12 hide form-submit' type='button' value='Save' />\n";
  echo "<input class='inp08 hide modal-close' type='button' value='Cancel' />\n";

  echo "<div class='tip' style='margin-top: 16px; box-sizing: border-box; width: 100%;'><p class='gsaStates'></p></div>\n";

  echo "</form>\n";
  echo "</div>\n";

  echo "<div class='modal' id='modal-gsa-cancel' title='GSA Cancel'>\n";
  echo "<form id='form-gsa-cancel' action='gsaPRG.php' method='post'>\n";

  if (in_array('MASTER', $_SESSION['perm'])) {
    echo "<div style='text-align: right;'><img style='margin: -10px -14px 0 0; display: inline-block; cursor: pointer;' class='icon16 form-toggle' title='Edit' alt='Edit' src='icons/edit16.png' /></div>\n";
  }

  echo "<input type='hidden' name='inpGSAType' value='Cancel' />\n";

  echo "<p id='gsaCancelID' style='margin-bottom: 8px;'>GSA Cancel ID: <span id='inpGSAID' style='font-weight: 700;'></span></p>\n";
  echo "<label class='hide'>GSA Cancel ID:&nbsp;<input class='inp04 reo' type='text' name='inpGSAID' readonly /></label>\n";

  echo "<p id='inpGSACancelDsca' style='margin-bottom: 8px;'></p>\n";
  echo "<label class='hide' style='margin-top: 8px;'>GSA Cancel Description:<br /><textarea style='width: 500px;  height: 50px;' class='req' name='inpGSACancelDsca'></textarea></label>\n";

  echo "<input class='inp12 hide form-submit' type='button' value='Save' />\n";
  echo "<input class='inp08 hide modal-close' type='button' value='Cancel' />\n";

  echo "<div class='tip' style='margin-top: 16px; box-sizing: border-box; width: 100%;'><p class='gsaStates'></p></div>\n";

  echo "</form>\n";
  echo "</div>\n";
?>