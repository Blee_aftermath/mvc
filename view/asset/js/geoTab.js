import {modal} from './modules/lib/modal.js'
import { default as note } from './modules/lib/note.js'

/*
The constructor is normally passed a callback, for when a
"call" is made the callback then authenticates. The login
info normally gets supplied via a form field login. For seamless
usage, the server can login and provide the sessionId that
is assigned so it can be set into local storage. The Geotab
api checks local storage for these values and authenticates
without the need for the authenticate callback
*/

/**
 * Class to connect with the Geotab API webservice
 * @author Scott Kiehn
 */

export class GeoTab {
  /**
   * Creates a new GeoTab class
   */
  constructor(data) {
    this.api   = GeotabApi() //GeotabApi() is a global loaded with a script tag
    this.login = data.login

    document.addEventListener('click', (ev) => {
      const el = ev.target
      if (el.matches('[data-geo]')) {
        this.geoInfo(JSON.parse(el.dataset.geo))
      }
    })
  }

  /**
   * Gets returned credentials from a successful
   * server (PHP) login with the Geotab web service
   */
  geoLogin = async () => {

    if ( !this.needCred() ) return 'Credentials from storage'

    const get = await fetch(this.login),
          res = await get.json()

    if (res.ok) {
      localStorage.setItem('geotabAPI_credentials', JSON.stringify({
        database:  res.credentials.database,
        sessionId: res.credentials.sessionId,
        userName:  res.credentials.userName
      }))
      localStorage.setItem('geotabAPI_server', res.credentials.server)
      return 'Credentials retrieved'
    } else {
      return false
    }
  }

  /**
   * 
   */
  geoInfo = async (geo) => {
    const login = await this.geoLogin()

    if (!login) {
      modal({title: 'Error', body: `Sorry, access denied.`})
      return
    }

    console.log(login)

    this.api.call('Get', {
      typeName: 'DeviceStatusInfo',
      search: {
        DeviceSearch: {
          id: geo.geo
        }
      }
    }, (res) => {
      if ( res.length ) {
        res[0].info = geo.truck
        this.truckContent( res[0] )
      } else {
        modal({title: 'Not Found', body: `Sorry, there has been no information found for that truck.`})
      }
    }, (rej) => {
      modal({title: 'Error', body: `Sorry, there has been an error retrieving the information.`})
    })
  }

  /**
   * Directional label
   */
  bearing = cardinal => ['North', 'Northeast', 'East', 'Southeast', 'South', 'Southwest', 'West', 'Northwest', 'North'][ parseInt(cardinal/45) ]

  /**
   * km to mph
   */
  mph = km => parseInt( km * 0.62137 )

  /**
   * Boolean check for the stored credentials
   */
  needCred = () => {
    if ( localStorage.getItem('geotabAPI_credentials') === null || localStorage.getItem('geotabAPI_server') === null )
      return true
    else
      return false
  }

  /**
   * Formatted truck info in a modal
   */
  truckContent = truck => {
    if (truck.device.id) {

      let title = `Truck ${truck.info}`,
        content = `<h1>${title}</h1>`

      if ( truck.isDeviceCommunicating ) {

        content += truck.isDriving
          ? `<p>The vehicle is driving ${this.bearing(truck.bearing)} at ${this.mph(truck.speed)} mph.</p>`
          : `<p>The vehicle is currently not driving anywhere.</p>`

        content += `<p><a href="https://www.google.com/maps?t=m&q=${truck.latitude}+${truck.longitude}" target="map">Google map location</a></p>`

      } else {
        content += `<p>The device is not communicating.</p>`
      }

      modal({title: title, body: content})
    } else {
      modal({title: truck.info, body: `<p>The has been an error as the Geotab service did not return any information for that truck.</p>`})
    }
  }
}
