<?php

// ==================================================================================================== //
// === MODAL === //
// ==================================================================================================== //

  echo "<div class='modal' id='myModalOrder'>";
  echo "<div class='fixed'>";
  echo "<div class='body_stripe'></div>";

  echo "<div class='head'>";
  echo "<div class='content'>";

  echo "<span id='spanOrderHead'></span>";
  echo "<span style='float:right;'><a href='javascript:fnCloseModalOrder();'><img class='icon16' src='icons/no16.png' alt='Close' title='Close' /></a></span>";

  echo "</div>"; // content
  echo "</div>"; // head

  echo "<span id='spanOrderMessages'>";
  fnModalMessages();
  echo "</span>";

  echo "<div class='body'>";
  echo "\n<form method='post' name='frmModalOrder' action='modalPRG.php'>";
  echo "<div class='content'>";

  echo "<table class='info' id='tblOrder'>";

  $inpField = "reoOrderID";
  $inpLabel = "Order ID";
  $defClass = "inp06 reo";
  echo "<tr class='content' id='rowOrderID'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' readonly />";
  echo "</td></tr>";

  $sqlOffice = "SELECT * FROM office ORDER BY office_id";
  $resOffice = $mysqli->query($sqlOffice);

  $inpField = "selOrderOffice";
  $inpLabel = "Office";
  $defClass = "req";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<select class='$defClass' name='$inpField' id='$inpField'>";
  echo "<option value='' selected='selected'>--- Select ---</option>";
  while ($myOffice = $resOffice->fetch_assoc()) {
    echo "<option value='" . $myOffice['office_id'] . "'>" . prepareFrm($myOffice['office_name']) . "</option>";
  }
  echo "</select>";
  echo "</td></tr>";

  $sqlITyp = "SELECT * FROM item_type WHERE ityp_is_orderable = 1 ORDER BY ityp_dsca";
  $resITyp = $mysqli->query($sqlITyp);

  $inpField = "selOrderITyp";
  $inpLabel = "Type";
  $defClass = "inp12 req";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<select class='$defClass' name='$inpField' id='$inpField'>";
  echo "<option value='' selected='selected'>--- Select ---</option>";
  while ($myITyp = $resITyp->fetch_assoc()) {
    echo "<option value='" . $myITyp['ityp_code'] . "'>" . prepareFrm($myITyp['ityp_dsca']) . "</option>";
  }
  echo "</select>";
  echo "</td></tr>";

  $inpField = "selArrShipTo";
  $inpLabel = "Ship To";
  $defClass = "inp12 req";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<select class='$defClass' name='$inpField' id='$inpField'>";
  echo "<option value='' selected='selected'>--- Select ---</option>";
  foreach ($arrShipTo AS $key => $value) {
    echo "<option value='" . $key . "'>" . $value . "</option>";
  }
  echo "</select>";
  echo "</td></tr>";

  $inpField = "inpOrderInfo";
  $inpLabel = "Additional<br />Information";
  $defClass = "opt";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<textarea class='$defClass' name='$inpField' id='$inpField' rows='6' cols='48'></textarea>";
  echo "</td></tr>";

  $arrButtonRowOrder[] = "<input class='inp12' type='submit' name='subAddOrder' id='subAddOrder' value='Add' />";
  $arrButtonRowOrder[] = "<input class='inp12' type='submit' name='subChgOrder' id='subChgOrder' value='Save' />";
  $arrButtonRowOrder[] = "<input class='inp12' type='button' name='butConOrder' id='butConOrder' value='Delete' onclick='fnConfirmDelOrder();' />";
  $arrButtonRowOrder[] = "<input class='inp12' type='button' name='butCanOrder' id='butCanOrder' value='Cancel' onclick='fnCloseModalOrder();' />";

  echo "<tr class='content'><td></td><td>" . implode("", $arrButtonRowOrder) . "</td></tr>";

  echo "</table>";

  echo "</div>"; // content
  echo "<span id='spanOrderConfirm'></span>";
  echo "</form>";
  echo "</div>"; // body

  echo "<div class='body_stripe'></div>";
  echo "</div>"; // fixed
  echo "</div>"; // modal

// ==================================================================================================== //
// === LOAD === //
// ==================================================================================================== //

  if (isset($_SESSION['inpValue']['subAddOrder']) || isset($_SESSION['inpValue']['subChgOrder'])) {

    echo "\n<script language=\"JavaScript\">";
    echo "\n<!-- Hide the script from old browsers --";

    echo "\nvar objOrder = {};"; // Create a new object to hold the posted values.

    if (isset($_SESSION['inpValue']['reoOrderID'])) {
      echo "\narrOrderID.push(" . $_SESSION['inpValue']['reoOrderID'] . ");";
      echo "\nobjOrder.id     = \"" . $_SESSION['inpValue']['reoOrderID'] . "\";";
    }

    echo "\nobjOrder.office      = \"" . prepareJS($_SESSION['inpValue']['selOrderOffice'])      . "\";";
    echo "\nobjOrder.ityp        = \"" . prepareJS($_SESSION['inpValue']['selOrderITyp'])        . "\";";
    echo "\nobjOrder.shipto      = \"" . prepareJS($_SESSION['inpValue']['selArrShipTo'])        . "\";";
    echo "\nobjOrder.info        = \"" . prepareJS($_SESSION['inpValue']['inpOrderInfo'])        . "\";";

    echo "\narrOrderOb.push(objOrder);"; // Tack this onto the end of the array of Order objects.

    foreach($_SESSION['errField'] AS $field) {

      echo "\narrOrderEr.push(\"" . $field    . "\");";
      echo "\ndocument.getElementById(\"" . $field . "\").classList.add(\"err\");";

    }

    // Show the modal dialog box

    if (isset($_SESSION['inpValue']['subAddOrder'])) echo "\nfnAddOrder();";
    if (isset($_SESSION['inpValue']['subChgOrder'])) echo "\nfnChgOrder(" . $_SESSION['inpValue']['reoOrderID'] . ");";
    echo "\nfnFillOrder(arrOrderOb.length-1);";

    echo "// --End Hiding Here -->\n";
    echo "\n</script>\n";

  }

?>