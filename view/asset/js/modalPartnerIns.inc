<?php
  //delete modal
  echo "<div class='modal modal-close' id='modal-inscontact-delete-form' title='Delete an Insurance Contact'>\n";
  echo "<form action='partnerInsPRG.php' method='post'>\n";
  echo "<input type='hidden' name='action' />\n";

  $inpField = "con_id";
  echo "<input type='hidden' name='$inpField' />";

  echo "<table class='info'>\n";

  echo "<tr class='content'>";
  echo "<td>Click Yes to confirm the contact deletion.</td>";
  echo "</tr>\n";

  $arrButtonRowCnty   = [];
  $arrButtonRowCnty[] = "<input class='inp12' type='submit' value='Yes' />";
  $arrButtonRowCnty[] = "<input class='inp12 modal-close' type='button' value='No' />";
  echo "<tr class='content'><td style='padding-top: 8px;'>" . implode("", $arrButtonRowCnty) . "</td></tr>";

  echo "</table>\n";
  echo "</form>\n";
  echo "</div>\n";

  //update & insert modal
  echo "<div class='modal modal-close' id='modal-inscontact-save-form' title='Insurance Contact'>\n";
  echo "<form action='partnerInsPRG.php' method='post' novalidate='novalidate'>\n";
  echo "<input type='hidden' name='action' />\n";

  $inpField = "con_id";
  echo "<input type='hidden' name='$inpField' />";

  echo "<table class='info'>\n";

  $inpLabel = "Name";
  $defClass = "inp12 req";
  echo "<tr class='content'>";
  echo "<td><label>$inpLabel:</label></td>";
  echo "<td class='r'>";
  $inpField = "con_fname";
  echo "  <input class='$defClass' type='text' name='$inpField' id='$inpField' />";
  $inpField = "con_lname";
  echo "  <input class='$defClass' type='text' name='$inpField' id='$inpField' />";
  echo "</td>";
  echo "</tr>\n";

  $inpField = "con_title";
  $inpLabel = "Title";
  $defClass = "inp24 opt";
  echo "<tr class='content'>";
  echo "<td><label for='$inpField'>$inpLabel:</label></td>";
  echo "<td class='r'><input class='$defClass' type='text' name='$inpField' id='$inpField' /></td>";
  echo "</tr>\n";

  $inpField = "con_email";
  $inpLabel = "Email";
  $defClass = "inp24 opt";
  echo "<tr class='content'>";
  echo "<td><label for='$inpField'>$inpLabel:</label></td>";
  echo "<td class='r'><input class='$defClass' type='text' name='$inpField' id='$inpField' /></td>";
  echo "</tr>\n";

  $inpField = "con_telp";
  $inpLabel = "Phone";
  $defClass = "opt inp12";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' maxlength='$maxTel' />";
  $inpField = "con_telpx";
  $defClass = "opt inp08";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' maxlength='$maxExt' />";
  echo "</td></tr>";

  $inpField = "con_telc";
  $inpLabel = "Cell";
  $defClass = "opt inp12";
  echo "<tr class='content'>";
  echo "<td><label for='$inpField'>$inpLabel:</label></td>";
  echo "<td class='r'><input class='$defClass' type='text' name='$inpField' id='$inpField' maxlength='$maxTel' /></td>";
  echo "</tr>";

  $inpField = "con_telf";
  $inpLabel = "Fax";
  $defClass = "opt inp12";
  echo "<tr class='content'>";
  echo "<td><label for='$inpField'>$inpLabel:</label></td>";
  echo "<td class='r'><input class='$defClass' type='text' name='$inpField' id='$inpField' maxlength='$maxTel' /></td>";
  echo "</tr>";

  $sqlInsco = "SELECT * FROM insco ORDER BY insco_name";
  $resInsco = $mysqli->query($sqlInsco) or die('Get insurance company failed');

  $inpField = "con_insco";
  $inpLabel = "Carrier";
  $defClass = "inp16 opt";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<select class='$defClass' name='$inpField' id='$inpField'>";
  echo "<option value='' selected='selected'>--- Select Carrier ---</option>";
  while ($myInsco = $resInsco->fetch_assoc()) {
    echo "<option value='" . prepareFrm($myInsco['insco_id']) . "'>" . prepareFrm($myInsco['insco_name']) . "</option>";
  }
  echo "</select>";

  $inpField = "con_info";
  $inpLabel = "Notes";
  $defClass = "opt";
  echo "<tr class='content'><td class='l'>Additional<br />Information:</td>";
  echo "<td class='r'><textarea class='$defClass' name='$inpField' rows='6' cols='48'></textarea>";
  echo "</td></tr>";

  $arrButtonRowCnty   = [];
  $arrButtonRowCnty[] = "<button id='modal-save-button' class='inp12' type='button'>Save</button>";
  $arrButtonRowCnty[] = "<input class='inp12 modal-close' type='button' value='Cancel' />";
  echo "<tr class='content'><td style='padding-top: 8px;'></td><td style='padding-top: 8px;'>" . implode("", $arrButtonRowCnty) . "</td></tr>";

  echo "</table>\n";
  echo "</form>\n";
  echo "</div>\n";
?>