import {message} from './modules/lib/util.js'
import {default as note} from './modules/lib/note.js'
import {Util} from './util.js'

/**
 * Class for verifying job audit checks
 * @author Scott Kiehn
 */

export class JobVerify {
  /**
   * Creates a new JobVerify class
   */
  constructor(data) {
    this.data        = data
    this.data.verify = {}
    this.dom         = {}
    this.changeEvent = new CustomEvent('JobVerifyChangeEvent')

    // Set up a permission array
    this.perms       = ['job_v1', 'job_v2']

    // Get verify items for this phase
    fetch('jobVerifyPRG.php', {
      method: 'post',
      headers: { 'Content-Type': 'application/json;charset=utf-8' },
      body: JSON.stringify({action: 'verify', jver_job: this.data.job, jver_class: this.data.class})
    })
    .then(res => res.json())
    .then(body => {
      if ( body.ok ) {

        // Set global jifVerifyCount to actual
        window.aftermath.jifVerifyCount = body.count

        // Collect the data-verify DOM elements
        // 1. Set data-edit with the correct permissions
        // 2. Place into the class DOM object
        for (const el of document.querySelectorAll('[data-verify]')) {
          if (el.dataset.perm && this.hasPerm(el.dataset.perm)) {
            el.setAttribute('data-edit', 1)
          }
          this.dom[el.dataset.verify] = el
        }

        // Iterate through the DB fetch
        for (const ver of body.data) {

          const verifyEl = this.dom[ver.ver_ref]

          if (!verifyEl) continue

          // Fill verify & confirm objects with codes as keys containing IDs/confirm questions
          this.data.verify[ver.ver_ref] = ver

          // If the verify item has a job_verify record, add 'verified' css class to the DOM item
          if (ver.jver_verify) {
            this.domTweaks(verifyEl, {confirm: 'yes', data: ver})
          } else {
            this.domTweaks(verifyEl, {confirm: 'no', data: ver})
          }

          // Make the verify selectors visible
          verifyEl.style.display = 'inline-block'
        }

        document.addEventListener('click', (ev) => {

          const verifyEl = ev.target,
                key      = verifyEl.dataset.verify

          if (verifyEl.matches('[data-edit]') && Number(verifyEl.dataset.edit) === 1) {

            if (Number(verifyEl.dataset.confirmed) === 1) {
              note({text: 'Unconfirm the estimate for this phase prior to revising.', class: 'alert', time: 2000})
              return
            }

            if (window.aftermath.hasOwnProperty('jifSaveState') && window.aftermath.jifSaveState === false) {
              note({text: 'Save the page data prior to the verification checks.', class: 'alert'})
              return
            }

            if (this.perms.includes(verifyEl.dataset.perm)) {
              if ( !this.hasPerm(verifyEl.dataset.perm) ) {
                note({text: 'Permission denied.', class: 'error'})
                return
              }
            } else {
              note({text: 'Access denied.', class: 'error'})
              return
            }

            const msg = verifyEl.classList.contains('jobVerified') 
              ? 'Are you sure that you want to remove this verification?' 
              : this.data.verify[key].ver_confirm
                  ? `<b>${this.data.verify[key].ver_level == 1 ? 'SUP' : 'CM'}:</b> ${this.data.verify[key].ver_confirm}`
                  : 'I verify that this item is accurately recorded.'

            message({
              title: 'Verification Step',
              text: this.modalContent(msg),
              time: 10000,
              confirm: () => {this.toggleVerified(verifyEl)}
            })
          }
        })

        this.reportState()
      } else {
        //TODO if not OK
        this.reportState()
      }
    })
  }

  /**
   * Tweak the verify DOMs
   * @param {Element} verifyEl
   * @param {Object} data
   */
  domTweaks = (verifyEl, data) => {
    /*
      1. Assign HTML to the verify DOM
      2. Revise the jobVerify DOM classList
      3. Revise usage of optional verified DOM form input
    */

    if (data.confirm == 'yes') {

      verifyEl.innerHTML = this.hoverContentYes(data.data)
      verifyEl.classList.remove('jobUnverified')
      verifyEl.classList.add('jobVerified')
      this.adjustInputUsage(verifyEl.dataset.verify)

    } else if (data.confirm == 'no') {

      if (verifyEl.dataset.perm && this.hasPerm(verifyEl.dataset.perm))
        verifyEl.innerHTML = this.hoverContentNo(data.data)
      else
        verifyEl.innerHTML = this.hoverContentBase(data.data)

      verifyEl.classList.remove('jobVerified')
      verifyEl.classList.add('jobUnverified')
      this.adjustInputUsage(verifyEl.dataset.verify)

    }
  }

  /**
   * Disable/Enable associated form field
   * @param {String} tag
   */
  adjustInputUsage = (tag) => {

    tag = tag.replace(/\d$/, '')

    const verifiedEl = document.querySelector(`[data-verified=${tag}]`)

    if (verifiedEl) {

      let x = 0

      for (const el of document.querySelectorAll(`[data-verify^=${tag}]`))
        if (el.classList.contains('jobVerified')) x++

      if (x > 0)
        verifiedEl.disabled = true
      else 
        verifiedEl.disabled = false
    }
  }

  /**
   * Checks user role permission
   * @param {String} perm
   */
  hasPerm = (perm) => {
    if (this.perms.includes(perm)) {
      if (window.aftermath.hasPerm(perm)) {
        return true
      }
    }
    return false
  }

  /**
   * Produces HTML hover content for the base item without permissions
   * @param {Object} data
   */
  hoverContentBase = data => `<em>Contact the ${data.ver_level == 1 ? 'SUP' : 'CM'} to have this item verified.</em>`

  /**
   * Produces HTML hover content for unverified
   * @param {Object} data
   */
  hoverContentNo = data => `<em><b>${data.ver_level == 1 ? 'SUP' : 'CM'}:</b> ${data.ver_confirm}</em>`

  /**
   * Produces HTML hover content for verified
   * @param {Object} data
   */
  hoverContentYes = data => `<em>${data.emp_name} verified on ${Util.dateToString( new Date(data.jver_dt) )}</em>`

  /**
   * Produces HTML modal content
   * @param {String} text
   */
  modalContent = text => `${text}<br /><br /><button data-yes="1" class="modal-close">Yes</button><button class="modal-close">No</button>`

  /**
   * Console logs state and dispatches the custom change event.
   */
  reportState = () => {
    document.dispatchEvent(this.changeEvent)
    //console.log(this.data.verify)
    console.log('**********************************************************************')
    console.log(`jifVerifyCount: ${JSON.stringify(window.aftermath.jifVerifyCount)}`)
    console.log(`jifSaveState: ${JSON.stringify(window.aftermath.jifSaveState)}`)
    console.log('**********************************************************************')
  }

  /**
   * Toggle (add or remove) a verification item
   */
  toggleVerified = async (verifyEl) => {

    const job_id   = this.data.job,
          ver_id   = this.data.verify[verifyEl.dataset.verify].ver_id,
          class_id = this.data.verify[verifyEl.dataset.verify].ver_class

    const post = await fetch('jobVerifyPRG.php', {
      method: 'post',
      headers: { 'Content-Type': 'application/json;charset=utf-8' },
      body: JSON.stringify({action: 'toggle', jver_job: job_id, jver_verify: ver_id, jver_class: class_id})
    })

    const data = await post.json()

    if ( data.ok ) {
      this.domTweaks(verifyEl, data)
      window.aftermath.jifVerifyCount = data.count
      this.reportState()
    } else {
      //TODO if not OK
      note({text: data.msg})
    }
  }
}
