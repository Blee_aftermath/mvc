const currencyFormatter = new Intl.NumberFormat('en-US', {
  style: 'currency',
  currency: 'USD',
})

const htmlMap = {
  '&': '&amp;',
  '<': '&lt;',
  '>': '&gt;',
  '"': '&quot;',
  "'": '&#039;',
}

/**
 * Class that holds a bunch of various utility functions for use all around the portal
 * @author Jake Cirino
 */
export class Util {
  /**
   * Sets an html element to visible
   * @param {HTMLElement} element
   */
  static show(element){
    element.style.display = ''
  }
  
  /**
   * Hides an HTML element
   * @param {HTMLElement} element
   */
  static hide(element){
    element.style.display = 'none'
  }
  
  /**
   * Sleeps for a certain amount of time
   * @param {Number} time 
   */
  static sleep (time) {
    return new Promise((resolve) => setTimeout(resolve, time));
  }  

  /**
   * Parses the dom inside an element, for all elements that have a name
   * @param {HTMLElement} element
   * @returns {JSON} A key/item map of html elements and their names
   */
  static parseDOM(element) {
    let elements = element.querySelectorAll('[name]')
    let collection = {}
    for (const key in elements) {
      if (elements.hasOwnProperty(key)) {
        const elem = elements[key]
        collection[elem.getAttribute('name')] = elem
      }
    }

    return collection
  }

  /**
   * Parses the dom inside an element, for all elements that have an id
   * @param {HTMLElement} element
   * @returns {JSON} A key/item map of html elements and their ids
   */
  static parseDOMID(element) {
    const collection = {}
    for (const elem of element.querySelectorAll('[id]')) {
      collection[elem.getAttribute('id')] = elem
    }
    return collection
  }

  /**
   * Strips the prefixes off the fields of an array
   * @param {JSON} array
   * @returns {JSON}
   */
  static stripPrefixes(array) {
    let ary = {}
    for (const key in array) {
      if (array.hasOwnProperty(key)) {
        const element = array[key]
        ary[key.substring(key.indexOf('_') + 1)] = element
      }
    }

    return ary
  }

  /**
   * The opposite of stripPrefixes, appends a prefix to each item
   * @param {JSON} array
   * @param {String} prefix
   * @returns {JSON}
   */
  static addPrefixes(array, prefix) {
    let ary = {}
    for (const key in array) {
      if (array.hasOwnProperty(key)) {
        const element = array[key]
        ary[prefix + '_' + key] = element
      }
    }

    return ary
  }

  /**
   * Sums equivalently named fields between two JSON arrays and returns the result in a new JSON array
   * TODO I feel like this should be moved elsewhere or given some more functionality
   * @param {JSON} array1
   * @param {JSON} array2
   * @param {Boolean} stripPrefixes True if you want to strip the prefix off field names.
   * For example, if you wanted to add jcur_* items with jfin_* items.
   * @returns {JSON} All of the shared fields between the two arrays, added together
   */
  static sumArrays(array1, array2, stripPrefixes = false) {
    //make copy of input arrays so they can be manipulated
    let ary1 = JSON.parse(JSON.stringify(array1)),
      ary2 = JSON.parse(JSON.stringify(array2))

    if(ary2 == null) return ary1

    //strip prefixes if defined
    if (stripPrefixes) {
      //strip array 1
      let stripped1 = {}
      for (const key in ary1) {
        if (ary1.hasOwnProperty(key)) {
          const element = ary1[key]
          stripped1[key.substring(key.indexOf('_') + 1)] = element
        }
      }
      ary1 = stripped1

      //strip array 2
      let stripped2 = {}
      for (const key in ary2) {
        if (ary2.hasOwnProperty(key)) {
          const element = ary2[key]
          stripped2[key.substring(key.indexOf('_') + 1)] = element
        }
      }
      ary2 = stripped2
    }

    //sum the arrays
    let finalAry = {}
    for (const key in ary1) {
      if (ary1.hasOwnProperty(key)) {
        const value1 = ary1[key]
        const value2 = ary2[key]

        if (value1 != undefined && value2 != undefined) {
          finalAry[key] = Number(value1) + Number(value2)
        }
      }
    }

    return finalAry
  }

  /**
   * Performs array1 - array2 in the same manner
   * @param {*} array1
   * @param {*} array2
   * @param {*} stripPrefixes
   * @see sumArrays
   */
  static subtractArrays(array1, array2, stripPrefixes = false) {
    let ary2 = {}
    for (const key in array2) {
      if (array2.hasOwnProperty(key)) {
        const element = array2[key]
        ary2[key] = -Number(element)
      }
    }

    return this.sumArrays(array1, ary2, stripPrefixes)
  }

  /**
   * Prepares text for view in a form
   * @param {String} text
   */
  static prepareForm(text) {
    //str_replace
    let regExp = new RegExp(String.fromCharCode(160), 'g')
    let formatted = text.replace(regExp, ' ')

    //trim
    formatted = formatted.trim()

    //htmlspecialchars
    return this.htmlSpecialChars(formatted)
  }

  /**
   * Prepares text for web display
   * @param {String} text
   */
  static prepareWeb(text) {
    //trim
    let formatted = text.trim()

    //htmlspecialchars
    formatted = this.htmlSpecialChars(formatted)

    //nl2br
    return this.nl2br(formatted)
  }

  /**
   * Converts newlines to breaks in the string
   * @param {String} str
   * @param {Boolean} is_xhtml
   */
  static nl2br(str, is_xhtml) {
    if (typeof str === 'undefined' || str === null) {
      return ''
    }
    var breakTag = is_xhtml || typeof is_xhtml === 'undefined' ? '<br />' : '<br>'
    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2')
  }

  /**
   * An equivalent of the php function htmlspecialchars
   * @param {String} text
   */
  static htmlSpecialChars(text) {
    return text.replace(/[&<>"']/g, (m) => {
      return htmlMap[m]
    })
  }

  /**
   * Converts a variable from MySQL bool to a javascript bool
   * @param {Number} item
   * @returns {Boolean}
   */
  static fromMysqlBool(item) {
    return item == 1 ? true : false
  }

  /**
   * Formats a phone number for display
   * @param {String} phone The unformatted phone number
   * @returns {String} The formatted phone number
   */
  static formatPhone(phone) {
    let match = phone.match(/^(\d{3})(\d{3})(\d{4})$/)
    return `(${match[1]}) ${match[2]}-${match[3]}`
  }

  /**
   * Filters null results to an empty string
   * @param {JSON} data
   * @returns {JSON} The filtered JSON array
   */
  static filterNulls(data) {
    let result = {}

    for (var key in data) {
      if (data[key] === null) result[key] = ''
      else result[key] = data[key]
    }

    return result
  }

  /**
   * Converts date and time display/input fields into a javascript date object
   * @param {String} date
   * @param {String} time
   * @returns {Date}
   */
  static parseDate(date, time = "0:00") {
    let splitDate = date.split('/'),
      splitTime = time.split(':')

    let yearAddition = splitDate[2].length == 4 ? 0 : 2000
    let year = parseInt(splitDate[2]) + yearAddition, //is there a better way to do this? dont want Y2.1K 79 years from now...
      month = parseInt(splitDate[0]) - 1,
      day = parseInt(splitDate[1]),
      hour = parseInt(splitTime[0]),
      minute = parseInt(splitTime[1].substr(0, 2))

    //convert time from ampm to 24hours
    if (splitTime[1].includes('am')) {
      hour = hour == 12 ? 0 : hour
    } else if(splitTime[1].includes('pm')) {
      hour = hour == 12 ? hour : hour + 12
    }

    return new Date(year, month, day, hour, minute)
  }
  
  /**
   * Parses 12 hour time format to a 24 hour time format
   * @param {string} time
   * @returns {string}
   */
  static parseTime24(time){
    let hrMins = time.split(':')
    
    let hour = Number(hrMins[0])
    if(hrMins[1].includes('am')) {
      if (hour == 12) hour = 0
    } else if(hrMins[1].includes('pm')) {
      if(hour != 12) hour += 12
    }
    
    return `${hour.toString().padStart(2, '0')}:${hrMins[1].substr(0, 2)}`
  }

  /**
   * Converts a datetime string to a date object
   * @param {String} dateTime
   * @returns {Date}
   */
  static parseDateTime(dateTime) {
    let dateTimeStrings = dateTime.split(' '),
      date = dateTimeStrings[0],
      time = dateTimeStrings[1]

    return this.parseDate(date, time)
  }

  /**
   * Converts a date to a SQL/web display ready string
   * @param {Date} date
   * @param {String} yearDigits
   * @returns {String}
   */
  static dateToString(date, yearDigits = '2-digit') {
    const year = new Intl.DateTimeFormat('en', { year: yearDigits }).format(date)
    const month = new Intl.DateTimeFormat('en', { month: '2-digit' }).format(date)
    const day = new Intl.DateTimeFormat('en', { day: '2-digit' }).format(date)
    const time = new Intl.DateTimeFormat('en', {
      hour: '2-digit',
      minute: 'numeric',
    })
      .format(date)
      .replace(' ', '')
      .toLowerCase()

    return `${month}/${day}/${year} ${time}`
  }

  /**
   * Shifts the timezone from central time to a local time.
   * This is very hacky as our date/time format doesnt align
   * with javascripts Date object standards, please improve it if you can.
   * @param {String} date
   * @param {Number} timezoneShift
   */
  static shiftTimezone(date, timezoneShift) {
    let splitDateTime = date.split(' ')
    let splitTime = splitDateTime[1].split(':')

    let adjustedDate = new Date(splitDateTime[0])
    //convert hours to 24 hour format
    let adjustedHours = parseInt(splitTime[0])
    if (splitTime[1].includes('am')) {
      adjustedHours = adjustedHours == 12 ? 0 : adjustedHours
    } else {
      adjustedHours += 12
    }
    adjustedHours -= -6 - timezoneShift
    adjustedDate.setHours(adjustedHours, parseInt(splitTime[1]))

    return this.dateToString(adjustedDate)
  }

  /**
   * Formats a number for currency display
   * @param {any} amount
   * @param {Boolean} showDecimal
   * @param {String} format
   * negative: negative numbers have a standard negative sign
   * square: negative numbers are surrounded by square brackets
   */
  static formatCurrency(amount, showDecimal = true, format = 'negative') {
    //convert amount
    if (!Number.isInteger(amount)) amount = Number.parseInt(amount)

    //format
    let result = currencyFormatter.format((Math.round(amount) / 100).toFixed(2))
    if (result !== '$NaN'){
      //trim decimal if required
      if(!showDecimal) result =  result.substring(0, result.indexOf('.'))

      //apply additional formatting if required
      if(amount < 0 && format == 'square') result = `[${result.substring(1, result.length)}]`

      return result
    } else{
      if(showDecimal) return '$0.00'
      else return '$0'
    }
  }

  /**
   * Returns formatCurrency back to pennies
   * @param {String} dollars
   */
  static toCents (dollars) {
    return Number(dollars.replace(/[,\$\.]/g, ''))
  }  
  
  /**
   * Initiates a datepicker and datepicker change event
   * @param {HTMLElement} dp
   * @param {Function} event
   */
  static initiateDatePicker(dp, event){
    let jqueryObj = $(dp)
    jqueryObj.datepicker({
      onSelect: event
    })
    jqueryObj.datepicker('option', 'dateFormat', 'mm/dd/y')
  }

  /**
   * Creates a contact 'block' string
   * TODO this needs to be moved to another file
   * @deprecated This is set to be moved to another file
   * @param {JSON} contactData The contact(address) data
   * @returns {HTMLElement} An html element containing the formatted block
   */
  static getContactBlock(contactData, displayName = true, displayRelation = true, displayPhone = false) {
    let html = document.createElement('div')
    html.style.padding = '6px'

    //return default text if there is no contact data
    if (contactData === null) {
      html.innerText = 'None'

      return html
    }

    if (displayName) {
      let name = document.createElement('div')
      name.style.fontWeight = 'bold'
      name.innerText = `${contactData.addr_fname} ${contactData.addr_lname}`
      html.appendChild(name)
    }

    if (displayPhone) {
      if (contactData.addr_telp !== undefined && contactData.addr_telp.length > 0) {
        let telp = document.createElement('div')
        telp.innerText = this.formatPhone(contactData.addr_telp)
        html.appendChild(telp)
      }

      if (contactData.addr_telc !== undefined && contactData.addr_telc.length > 0) {
        let telc = document.createElement('div')
        telc.innerText = this.formatPhone(contactData.addr_telc)
        html.appendChild(telc)
      }

      if (contactData.addr_telw !== undefined && contactData.addr_telw.length > 0) {
        let telw = document.createElement('div')
        telw.innerText = this.formatPhone(contactData.addr_telw)
        html.appendChild(telw)
      }
    }

    if (contactData.addr_title !== undefined && contactData.addr_title.length > 0) {
      let title = document.createElement('div')
      title.style.fontStyle = 'italic'
      title.innerText = contactData.addr_title
      html.appendChild(title)
    }

    if (contactData.addr_company !== undefined && contactData.addr_company.length > 0) {
      let company = document.createElement('div')
      company.innerText = contactData.addr_company
      html.appendChild(company)
    }

    if (contactData.addr_line1 !== undefined && contactData.addr_line1.length > 0) {
      let line1 = document.createElement('div')
      line1.innerText = contactData.addr_line1
      html.appendChild(line1)
    }

    if (contactData.addr_line2 !== undefined && contactData.addr_line2.length > 0) {
      let line2 = document.createElement('div')
      line2.innerText = contactData.addr_line2
      html.appendChild(line2)
    }

    if (
      (contactData.addr_city !== undefined && contactData.addr_city.length > 0) ||
      (contactData.addr_state !== undefined && contactData.addr_state.length > 0) ||
      (contactData.addr_zip !== undefined && contactData.addr_zip.length > 0)
    ) {
      let cityStateZip = document.createElement('div')

      //set to blank data if undefined
      if (contactData.addr_city === undefined) contactData.addr_city = ''
      if (contactData.addr_state === undefined) contactData.addr_state = ''
      if (contactData.addr_zip === undefined) contactData.addr_zip = ''

      let cityText = contactData.addr_city + (contactData.addr_state.length > 0 ? ', ' : '')
      cityStateZip.innerText = `${cityText} ${contactData.addr_state} ${contactData.addr_zip}`.trim()

      html.appendChild(cityStateZip)
    }

    if (contactData.addr_email !== undefined && contactData.addr_email.length > 0) {
      let email = document.createElement('a')
      email.href = `mailto:${contactData.addr_email}`
      email.innerText = contactData.addr_email
      html.appendChild(email)
    }

    if (displayRelation && contactData.addr_relation !== undefined && contactData.addr_relation.length > 0) {
      let relation = document.createElement('div')
      relation.style.fontStyle = 'italic'
      relation.innerText = contactData.addr_relation
      html.appendChild(relation)
    }

    if (contactData.addr_info !== undefined && contactData.addr_info.length > 0) {
      let info = document.createElement('div')
      info.innerText = contactData.addr_info
      html.appendChild(info)
    }

    return html
  }
}
