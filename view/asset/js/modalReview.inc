<?php

// *************************** MODAL ***************************

  echo "<div class='modal' id='myModalReview'>";
  echo "<div class='fixed'>";
  echo "<div class='body_stripe'></div>";

  echo "<div class='head'>";
  echo "<div class='content'>";

  echo "<span id='spanReviewHead'></span>";
  echo "<span style='float:right;'><a href='javascript:fnCloseModalReview();'><img class='icon16' src='icons/no16.png' alt='Close' title='Close' /></a></span>";

  echo "</div>"; // content
  echo "</div>"; // head

  echo "<span id='spanReviewMessages'>";
  fnModalMessages();
  echo "</span>";

  echo "<div class='body'>";
  echo "\n<form method='post' name='frmModal' action='modalPRG.php'>";
  echo "<div class='content'>";

  echo "<input type='hidden' name='hidReviewID' id='hidReviewID' />";
  echo "<input type='hidden' name='hidJobID'    id='hidJobID' value='" . $valJobID . "' />";

  echo "<table class='info' id='tblReview' style='display:none;'>";

  $inpField = "inpDRec";
  $inpLabel = "Received";
  $defValue = "";
  $defClass = "inp16 req";
  $inpHelp  = "Enter the date this review was received.";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' value='" . $defValue . "' maxlength='$maxName' list='off' />";
  echo "&nbsp;<img class='icon16' title='$inpHelp' alt='Help' src='icons/question16.png' />";
  echo "</td></tr>";

  echo "<script>$(function() { $('#".  $inpField . "').datepicker(); }); </script>";

  $inpField = "inpReviewInfo";
  $inpLabel = "Review&nbsp;Text";
  $defValue = "";
  $defClass = "req";
  echo "<tr class='content'><td class='l'>$inpLabel:</td>";
  echo "<td class='r'><textarea class='$defClass' name='$inpField' id='$inpField' rows='6' cols='60'>$defValue</textarea>";
  echo "</td></tr>";

  $arrButtonRowReview[] = "<input class='inp12' type='submit' name='subAddReview' id='subAddReview' value='Add' />";
  $arrButtonRowReview[] = "<input class='inp12' type='submit' name='subChgReview' id='subChgReview' value='Save' />";
  $arrButtonRowReview[] = "<input class='inp12' type='button' name='butConReview' id='butConReview' value='Delete' onclick='fnConfirmDelReview();' />";
  $arrButtonRowReview[] = "<input class='inp12' type='button' name='butCanReview' id='butCanReview' value='Cancel' onclick='fnCloseModalReview();' />";

  echo "<tr class='content'><td></td><td>" . implode("", $arrButtonRowReview) . "</td></tr>";

  echo "</table>";

  echo "</div>"; // content
  echo "<span id='spanReviewConfirm'></span>";
  echo "</form>";
  echo "</div>"; // body

  echo "<div class='body_stripe'></div>";
  echo "</div>"; // fixed
  echo "</div>"; // modal

// *************************** LOAD ***************************

  if (isset($_SESSION['inpValue']['subAddReview']) || isset($_SESSION['inpValue']['subChgReview'])) {

    echo "\n<script language=\"JavaScript\">";
    echo "\n<!-- Hide the script from old browsers --";

    echo "\nvar objReview = {};"; // Create a new object to hold the posted values.

    if (isset($_SESSION['inpValue']['hidReviewID'])) {
      echo "\narrReviewID.push(" . $_SESSION['inpValue']['hidReviewID'] . ");";
      echo "\nobjReview.id     = \"" . $_SESSION['inpValue']['hidReviewID'] . "\";";
    }

    echo "\nobjReview.dtRec    = \"" . prepareJS($_SESSION['inpValue']['inpDRec'])       . "\";";
    echo "\nobjReview.info     = \"" . prepareJS($_SESSION['inpValue']['inpReviewInfo']) . "\";";

    foreach($_SESSION['errField'] AS $field) {

      echo "\narrReviewEr.push(\"" . $field    . "\");";
      echo "\ndocument.getElementById(\"" . $field . "\").classList.add(\"err\");";

    }

    echo "\narrReviewOb.push(objReview);"; // Tack this onto the end of the array of SKU objects.

    if (isset($_SESSION['inpValue']['subAddReview'])) echo "\nfnAddReview();";
    if (isset($_SESSION['inpValue']['subChgReview'])) echo "\nfnChgReview(" . $_SESSION['inpValue']['hidReviewID'] . ");";
    echo "\nfnFillReview(arrReviewOb.length-1);";

    echo "// --End Hiding Here -->\n";
    echo "\n</script>\n";
  }

?>