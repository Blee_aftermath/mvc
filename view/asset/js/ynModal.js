/**
 * Generic modal to prompt yes/no questions
 * @author Jake Cirino
 */
export class YNModal {
  /**
   * Creates a new YN Modal
   * @param {String} title
   * @param {String} text
   * @param {Function} callback Callback function on window action,
   * parameter is a bool of the result (yes = true, no/cancel = false)
   */
  constructor(title, text, callback) {
    this.callback = callback
    this.title = title
    this.text = text

    //create base div
    this.baseElement = document.createElement('div')
    this.baseElement.className = 'modal'
    this.baseElement.id = 'modal-yn-tmp'
    this.baseElement.style.display = 'block'
    this.populate()
  }

  /**
   * Populates the modal htnk
   */
  async populate(){
    let template = await window.templateController.getTemplate('ynModal')
    this.baseElement.innerHTML = template

    //append title and body text
    this.baseElement.querySelector('.title').innerText = this.title
    this.baseElement.querySelector("[name='body-text']").innerText = this.text

    //setup cancel buttons
    this.baseElement.querySelectorAll(`[name='action-close']`).forEach((element) => {
      element.onclick = (() => {
        this.close(false)
      }).bind(this)
    })

    //setup confirm button
    this.baseElement.querySelector("[name='action-yes']").onclick = (() => {
      this.close(true)
    }).bind(this)

    //append to body
    document.body.appendChild(this.baseElement)
  }

  /**
   * Closes the window, calling the callback on the result
   */
  close(result) {
    this.baseElement.remove()
    this.callback(result)
    delete this
  }
}
