<?php

  echo "<script type='module'>\n";
  echo "import {default as formModal} from './scripts/modules/ui/formModal.js?$scriptRandom';\n";
  echo "window.jsClkEvt['formModal'] = formModal;\n";
  echo "</script>\n";

  echo "<div class='modal' id='modal-edit-address'>\n";
  echo "<form action='addrPRG.php' method='post'>\n";
  echo "<table class='info'>\n";

  echo "\n\n<script language=\"JavaScript\">";
  echo "\n<!-- Hide the script from old browsers --";

  echo "\nvar arrAddress = [];"; // Array of address objectss for this job
  echo "\nvar arrError   = [];"; // Array of errored fields to tag in red

  echo "\nfunction fnConfirmDel() {";
  echo "\n  strConfirm  = \"<div class='content alert'>\";";
  echo "\n  strConfirm += \"Delete this address?&nbsp;&nbsp;\";";
  echo "\n  strConfirm += \"<input class='inp08' type='submit' name='subDelY' value='Yes' />\";";
  echo "\n  strConfirm += \"<input class='inp08' type='button' name='subDelN' value='No' onclick='fnDelN()' />\";";
  echo "\n  strConfirm += \"</div>\";";
  echo "\n  document.getElementById(\"spanConfirm\").innerHTML = strConfirm;";
  echo "\n}";

  echo "\nfunction fnDelN() {";
  echo "\n  document.getElementById(\"spanConfirm\").innerHTML = \"\";";
  echo "\n}";

  echo "\n// --End Hiding Here -->";
  echo "\n</script>\n";

  echo "<tr class='heading'><th colspan='2'>Printable Address</th></tr>";

  $inpField = "hidJobID";
  $inpLabel = "Job ID";
  $defClass = "inp06 reo";
  echo "<tr style='display:none' class='content'>";
  echo "<td><label for='$inpField'>$inpLabel:</label></td>";
  echo "<td class='r'><input class='$defClass' type='text' name='$inpField' id='$inpField' readonly /></td>";
  echo "</tr>\n";

  $inpField = "hidAddressID";
  $inpLabel = "Address ID";
  $defClass = "inp06 reo";
  echo "<tr style='display:none' class='content'>";
  echo "<td><label for='$inpField'>$inpLabel:</label></td>";
  echo "<td class='r'><input class='$defClass' type='text' name='$inpField' id='$inpField' readonly /></td>";
  echo "</tr>\n";

  $inpField = "hidNumInv";
  $inpLabel = "num_inv";
  echo "<tr style='display:none' class='content'>";
  echo "<td><label for='$inpField'>$inpLabel:</label></td>";
  echo "<td class='r'><input class='$defClass' type='text' name='$inpField' id='$inpField' readonly /></td>";
  echo "</tr>\n";

  $inpField = "inpFName";
  $inpLabel = "Name:";
  $defClass = "inp10 " . "req";
  echo "<tr class='content'><td>$inpLabel</td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' maxlength='$maxName' />";
  $inpField = "inpLName";
  $defClass = "inp10 " . "req";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' maxlength='$maxName' />";
  echo "<span id='spanGetCallerName'></span>";
  echo "</td></tr>";

  $inpField = "inpTitle";
  $inpLabel = "Title:";
  $inpHelp  = "Title should be a real employment title such as Director or Attorney.  It should be entered as you would expect it to show on print materials like an Aftermath invoice.";
  $defClass = "opt";
  echo "<tr class='content'><td>$inpLabel</td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' maxlength='$maxName' />";
  echo "&nbsp;<img class='icon16' title='$inpHelp' alt='Help' src='icons/question16.png' />";
  echo "</td></tr>";

  $inpField = "inpCompany";
  $inpLabel = "Company:";
  $inpHelp  = "Company should be the company this person works for.  It could be something like BNSF or the proper name of a management company.  Do not put an informal description like Management Company.  Spell out the actual company name as this may appear on print materials like an Aftermath invoice.";
  $defClass = "opt";
  echo "<tr class='content'><td class='l'>$inpLabel</td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' maxlength='$maxName' />";
  echo "&nbsp;<img class='icon16' title='$inpHelp' alt='Help' src='icons/question16.png' />";
  echo "</td></tr>";

  $inpField = "inpLine1";
  $inpLabel = "Address Line 1:";
  $defClass = "req";
  echo "<tr class='content'><td class='l'>$inpLabel</td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' maxlength='$maxAddr' />";
  echo "<span id='spanGetPropName'></span>";
  echo "</td></tr>";

  $inpField = "inpLine2";
  $inpLabel = "Address Line 2:";
  $defClass = "opt";
  echo "<tr class='content'><td class='l'>$inpLabel</td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' maxlength='$maxAddr' />";
  echo "</td></tr>";

  $inpField = "inpCity";
  $inpLabel = "City:";
  $defClass = "req";
  echo "<tr class='content'><td class='l'>$inpLabel</td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' maxlength='$maxAddr' />";
  echo "</td></tr>";

  $sqlState = "SELECT * FROM state ORDER BY st_name";
  $resState = $mysqli->query($sqlState) or die('Get state failed');
  $inpField = "selState";
  $inpLabel = "State / Zip";
  $defClass = "inp12 req";
  echo "<tr class='content'><td class='l'>$inpLabel</td><td class='r'>";
  echo "<select class='$defClass' name='$inpField' id='$inpField'>";
  echo "<option value='' selected='selected'>--- Select ---</option>";
  while ($myState = $resState->fetch_assoc()) {
    echo "<option value='" . $myState['st_id'] . "'";
    echo ">" . $myState['st_name'] . "</option>";
  } echo "</select>";

  $inpField = "inpZip";
  $defClass = "inp08 " . "req";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' maxlength='$maxZip' />";
  echo "</td></tr>";

  echo "<tr class='heading'><th colspan='2'>Contact Details</th></tr>";

  $inpField = "inpEmail";
  $inpLabel = "Email:";
  $defClass = "opt";
  echo "<tr class='content'><td>$inpLabel</td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' maxlength='$maxEmail' />";
  echo "</td></tr>";

  $inpLabel = "Phone (Home):";
  $inpField = "inpTelp";
  $defClass = "inp12 opt";
  echo "<tr class='content'><td class='l'>$inpLabel</td>";
  echo "<td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' maxLength='$maxTel'/>";
  $inpField = "inpExtp";
  $defClass = "inp08 opt";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' maxlength='$maxExt'/>";
  echo "</td></tr>";

  $inpLabel = "Phone (Cell):";
  $inpField = "inpTelc";
  $defClass = "inp12 " . "opt";
  echo "<tr class='content'><td class='l'>$inpLabel</td>";
  echo "<td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' maxlength='$maxTel' />";
  $inpField = "inpExtc";
  $defClass = "inp08 " . "opt";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' maxlength='$maxExt' />";
  echo "</td></tr>";

  $inpLabel = "Phone (Work):";
  $inpField = "inpTelw";
  $defClass = "inp12 " . "opt";
  echo "<tr class='content'><td class='l'>$inpLabel</td>";
  echo "<td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' maxlength='$maxTel' />";
  $inpField = "inpExtw";
  $defClass = "inp08 " . "opt";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' maxlength='$maxExt' />";
  echo "</td></tr>";

  $inpField = "inpRelation";
  $inpLabel = "Relationship:";
  $inpHelp  = "Relation should indicate the relationship between this person and the job.  Potential entries might be Father of decedent, Executor, Neighbor, Contractor or Property Manager.  Do not enter Owner or Property Owner here.  If this person is the property owner, check the appropriate box below.  Relation is an internal field and will not show on customer correspondence.";
  $defClass = "opt";
  echo "<tr class='content'><td class='l'>$inpLabel</td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' maxlength='$maxName' />";
  echo "&nbsp;<img class='icon16' title='$inpHelp' alt='Help' src='icons/question16.png' />";
  echo "</td></tr>";

  $inpField = "inpInfo";
  $inpLabel = "Additional<br />Information:";
  $inpHelp  = "Additional Information should indicate why this person is relevant to this job.  If there are multiple billable addresses, specify which components to bill to this person.  Additional Information is an internal field and will not show on customer correspondence.";
  $defClass = "opt";
  echo "<tr class='content'><td class='l'>$inpLabel</td><td class='r'>";
  echo "<textarea class='$defClass' name='$inpField' id='$inpField' rows='4' cols='48'></textarea>";
  echo "&nbsp;<img class='icon16' title='$inpHelp' alt='Help' src='icons/question16.png' />";
  echo "</td></tr>";

  $inpField = "chkIsCosi";
  $inpLabel = "Contract Signer:";
  echo "<tr class='content'><td>$inpLabel</td><td class='l'>";
  echo "<input type='checkbox' name='$inpField' id='$inpField'/>";
  echo "</td></tr>";

  $inpField = "chkIsProw";
  $inpLabel = "Property Owner:";
  echo "<tr class='content'><td>$inpLabel</td><td class='l'>";
  echo "<input type='checkbox' name='$inpField' id='$inpField'/>";
  echo "</td></tr>";

  $inpField = "chkIsBill";
  $inpLabel = "Billable Address:";
  echo "<tr class='content'><td>$inpLabel</td><td class='l' colspan='2'>";
  echo "<input type='checkbox' name='$inpField' id='$inpField'/>";
  echo "</td></tr>";

  $arrButtonRowAddr[] = "<input class='inp12' style='display:none' id='subChg' type='submit' name='subChg' value='Change' />";
  $arrButtonRowAddr[] = "<input class='inp12' style='display:none' id='subAdd' type='submit' name='subAdd' value='Add' />";
  $arrButtonRowAddr[] = "<input class='inp12' style='display:none' id='subDel' type='button' name='subDel' value='Delete' onclick='fnConfirmDel();'/>";
  $arrButtonRowAddr[] = "<input class='inp12 modal-close' style='float: none' type='button' value='Cancel' />";
  echo "<tr class='content'><td></td><td>" . implode("", $arrButtonRowAddr) . "</td></tr>";

  echo "</table>\n";
  echo "<span id='spanConfirm'></span>";
  echo "</form>\n";
  echo "</div>\n";