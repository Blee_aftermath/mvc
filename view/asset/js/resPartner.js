import { default as formModal } from './modules/ui/formModal.js'

/**
 * Class for Restoration Partners admin page
 * @author Scott Kiehn
 */

export class PartnerRes {
  /**
   * Creates a new PartnerRes class
   */
  constructor(opts = {}) {
    this.dom          = {}
    this.dom.partners = document.querySelector('#resPartner')
    this.dom.formMdl  = document.querySelector('#modal-respartner-save-form')
    this.dom.saveBtn  = document.querySelector('#modal-save-button')

    this.img          = {}
    this.img.edit     = new Image();
    this.img.edit.src = '/icons/edit16.png';
    this.img.done     = new Image();
    this.img.done.src = '/icons/ys16.png';

    this.cache        = {}
    this.cache.CNTY   = []

    if (this.dom.officePick  = document.querySelector('#office-pick')) {
      this.dom.officePart = document.querySelector('#office-part')
      
      this.dom.statePick = document.querySelector('#state-pick')
      this.dom.statePart = document.querySelector('#state-part')

      this.dom.officeEdit = document.querySelector('#office-edit')
      this.dom.stateEdit = document.querySelector('#state-edit')

      this.rpID       = parseInt(this.dom.officePick.dataset.rp_id)
      this.rpOffices  = JSON.parse(this.dom.officePick.dataset.rpo)
      this.rpStates = JSON.parse(this.dom.statePick.dataset.rps)

      this.dom.officePick.addEventListener('click', (ev) => {
        if (ev.target.matches('li')) this.selectRPO(ev.target)
        this.dom.officeEdit.src = this.img.done.src
      })

      this.dom.statePick.addEventListener('click', (ev) => {
        if (ev.target.matches('li')) this.selectRPS(ev.target)
        this.dom.stateEdit.src = this.img.done.src
      })

      this.dom.officeEdit.addEventListener('click', (ev) => this.openCloseList(ev.target))
      this.dom.stateEdit.addEventListener('click', (ev) => this.openCloseList(ev.target))
    }

    this.dom.partners.addEventListener('click', (ev) => {
      if (ev.target.matches('[data-opts]'))
        formModal(ev.target)      
    })

    this.dom.saveBtn.addEventListener('click', (ev) => this.saveOffice(ev.target.closest('form')))
  }

  /**
   * Build an office or county selected list
   * @param {Array} list
   * @param {Element} dom
   */
  buildSelectedList = (list, dom) => {
    let html = ''
    for (const x of list.sort()) {
      html += `<tr class="content"><td colspan="2">${x}</td></tr>`
    }
    dom.innerHTML = html
  }

  /**
   * Find the first occurrrence in an array by text
   * @param {Str} find
   */
  findState = (find) => {
    if (find.length) {
      const findRE = new RegExp(`^${find}`)
      for (const row of this.states) {
        if (row[0].toLowerCase().match(findRE)) {
          return row
        }
      }
    }
    return []
  }

  /**
   * Retrieve Counties for a State
   * @param {Element} form
   */
  getStateCounties = async (ev) => {

    const el = ev.target,
          st = this.findState(el.value.toLowerCase())

    el.value = el.value.toLowerCase().charAt(0).toUpperCase() + el.value.slice(1)

    if (ev.which === 13) {
      ev.preventDefault()

      if (st.length > 0) {

        el.value = st[0]

        if (!this.cache[st[1]]) {
          const post = await fetch('resPartnerPRG.php', {
            method: 'post',
            headers: { 'Content-Type': 'application/json;charset=utf-8' },
            body: JSON.stringify({ action: 'getCountiesByState', st_id: st[1] })
          })

          const res = await post.json()

          this.cache[st[1]] = res.ok ? res.params : []
        }

        this.dom.countyPick.innerHTML = ''
        for (const c of this.cache[st[1]]) {
          const selected = this.rpCounties.includes(c.county) ? ' class="selected"' : ''
          this.dom.countyPick.innerHTML += `<li${selected} id="rpc${c.county_id}" data-county_id="${c.county_id}" data-county="${c.county}">${c.county}</li>`
        }

        this.dom.countyPick.classList.add('expand')
      }
    } else {
      this.dom.stateShow.innerText = st.length > 0 ? st[0] : ''
    }
  }

  /**
   * Open & close the selection pick lists
   * @param {Element} icon
   */
  openCloseList = async (icon) => {
    const list = icon.nextElementSibling

    //Do only for counties
    if (icon.id == 'county-edit') {
      if (list.innerText.length === 0) {
        if (this.cache.CNTY.length == 0) {

          const post = await fetch('resPartnerPRG.php', {
            method: 'post',
            headers: { 'Content-Type': 'application/json;charset=utf-8' },
            body: JSON.stringify({ action: 'getCountiesByPartner', rp_id: this.rpID })
          })

          const res = await post.json()

          this.cache.CNTY = res.ok ? res.params : []
        }

        const cntyObjSorted = this.cache.CNTY.sort((a, b) => {
          return a.county.localeCompare(b.county)
        })

        for (const c of cntyObjSorted) {
          this.dom.countyPick.innerHTML += `<li class="selected" id="rpc${c.county_id}" data-county_id="${c.county_id}" data-county="${c.county}">${c.county}</li>`
        }
      }
    }

    list.classList.toggle('expand')
    if (!list.classList.contains('expand')) {
      icon.src = this.img.edit.src

      if (icon.id == 'county-edit' && this.dom.stateFind.value == '')
        this.dom.countyPick.innerHTML = ''
    }
  }

  /**
   * Reset the State/County search select dropdown
   */
  undoList = () => {
    this.dom.stateFind.value = ''
    this.dom.stateShow.innerText = ''
    this.dom.countyPick.classList.remove('expand')
    this.dom.countyPick.innerHTML = ''
    this.dom.countyEdit.src = this.img.edit.src
    this.dom.stateFind.focus()
  }

  /**
   * Save restoration partner information
   * @param {Element} form
   */
  saveOffice = async (form) => {

    const inpData = {}

    let errors = 0

    for (const inp of form.querySelectorAll('[name]')) {
      if (inp.type == 'checkbox') {
        inpData[inp.name] = inp.checked ? 1 : 0
      } else {
        inpData[inp.name] = inp.value
      }
      if (inp.classList.contains('req')) if (inp.value.length == 0) errors++
    }

    if (errors) return this.dom.formMdl.querySelector('.error').innerHTML = `<p>Values for the required items were not provided.</p>`;

    const post = await fetch('resPartnerPRG.php', {
      method: 'post',
      headers: { 'Content-Type': 'application/json;charset=utf-8' },
      body: JSON.stringify(inpData)
    })

    const res = await post.json()

    if ( res.ok ) {
      this.dom.formMdl.querySelector('.error').innerHTML = '';
      this.dom.formMdl.querySelector('.message').innerHTML = `<p>${res.msg}</p>`;
      setTimeout(() => {
        location.assign(res.ref)
        if (res.ref.match(/#/)) location.reload()
      }, 750)
    } else {
      const error = []

      if (typeof(res.msg) == 'object') {
        for (const m of res.msg) error.push(m.msg)
      } else {
        error[0] = res.msg
      }

      this.dom.formMdl.querySelector('.error').innerHTML = `<p>${error.join('<br />')}</p>`;
    }
  }

  /**
   * Save offices for restoration partners
   * @param {Element} el
   */
  saveRPO = async (offID, add) => {

    const action = add === true ? 'officeAdd' : 'officeDel'

    const post = await fetch('resPartnerPRG.php', {
      method: 'post',
      headers: { 'Content-Type': 'application/json;charset=utf-8' },
      body: JSON.stringify({ action: action, rpo_partner: this.rpID, rpo_office: parseInt(offID) })
    })

    const res = await post.json()

    return res.ok ? true : false
  }
  
  /**
   * Saves state for a restoration partner
   * @param stateID
   * @param {boolean} add
   * @returns {Promise<*>}
   */
  saveRPS = async (stateID, add) => {
    const post = await fetch('resPartnerPRG.php', {
      method: 'post',
      headers: {'Content-Type': 'application/json;charset=utf8'},
      body: JSON.stringify({
        action: add ? 'stateAdd' : 'stateDel',
        rps_partner: this.rpID,
        rps_state: stateID
      })
    })
    
    const res = await post.json()
    
    return res.ok;
  }

  /**
   * Select offices for restoration partners
   * @param {Element} el
   */
  selectRPO = async (el) => {
    const off   = el.dataset.office,
          offID = el.dataset.office_id

    if (this.rpOffices.includes(off)) {

      const ok = await this.saveRPO(offID, false)
      if (ok === true) {
        this.rpOffices.splice(this.rpOffices.indexOf(off), 1)
        el.classList.remove('selected')
      }
    } else {

      const ok = await this.saveRPO(offID, true)
      if (ok === true) {
        this.rpOffices.push(off)
        el.classList.add('selected')
      }
    }

    this.buildSelectedList(this.rpOffices, this.dom.officePart)
  }
  
  /**
   * Select states
   * @param el
   * @returns {Promise<void>}
   */
  selectRPS = async (el) => {
    const state = el.dataset.st_id
    
    if(this.rpStates.includes(state)) {
      const ok = await this.saveRPS(state, false)
      if(ok){
        this.rpStates.splice(this.rpStates.indexOf(state), 1)
        el.classList.remove('selected')
      }
    }else{
      const ok = await this.saveRPS(state, true)
      if(ok){
        this.rpStates.push(state)
        el.classList.add('selected')
      }
    }
    
    this.buildSelectedList(this.rpStates, this.dom.statePart)
  }

  /**
   * US States array
   */
  states = [
    ['Alabama', 'AL'],
    ['Arizona', 'AZ'],
    ['Arkansas', 'AR'],
    ['California', 'CA'],
    ['Colorado', 'CO'],
    ['Connecticut', 'CT'],
    ['Delaware', 'DE'],
    ['District of Columbia', 'DC'],
    ['Florida', 'FL'],
    ['Georgia', 'GA'],
    ['Idaho', 'ID'],
    ['Illinois', 'IL'],
    ['Indiana', 'IN'],
    ['Iowa', 'IA'],
    ['Kansas', 'KS'],
    ['Kentucky', 'KY'],
    ['Louisiana', 'LA'],
    ['Maine', 'ME'],
    ['Maryland', 'MD'],
    ['Massachusetts', 'MA'],
    ['Michigan', 'MI'],
    ['Minnesota', 'MN'],
    ['Mississippi', 'MS'],
    ['Missouri', 'MO'],
    ['Montana', 'MT'],
    ['Nebraska', 'NE'],
    ['Nevada', 'NV'],
    ['New Hampshire', 'NH'],
    ['New Jersey', 'NJ'],
    ['New Mexico', 'NM'],
    ['New York', 'NY'],
    ['North Carolina', 'NC'],
    ['North Dakota', 'ND'],
    ['Ohio', 'OH'],
    ['Oklahoma', 'OK'],
    ['Oregon', 'OR'],
    ['Pennsylvania', 'PA'],
    ['Rhode Island', 'RI'],
    ['South Carolina', 'SC'],
    ['South Dakota', 'SD'],
    ['Tennessee', 'TN'],
    ['Texas', 'TX'],
    ['Utah', 'UT'],
    ['Vermont', 'VT'],
    ['Virginia', 'VA'],
    ['Washington', 'WA'],
    ['West Virginia', 'WV'],
    ['Wisconsin', 'WI'],
    ['Wyoming', 'WY']
  ]
}
