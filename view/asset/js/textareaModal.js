/**
 * A generic textarea modal
 * @author Jake Cirino
 */
export class TextareaModal {
  /**
   * Creates a new Textarea Modal
   * @param {String} title
   * @param {String} text Default value of the textarea
   * @param {String} yesText The display value of the 'yes/save' button
   * @param {Function} callback Callback function on window action,
   * parameter is the textarea value (on saved) or  null (on canceled)
   */
  constructor(title, text, yesText = 'Save', callback) {
    this.title = title
    this.text = text
    this.yesText = yesText
    this.noText = 'Cancel'
    this.callback = callback

    //create base div
    this.baseElement = document.createElement('div')
    this.baseElement.className = 'modal'
    this.baseElement.id = 'modal-textarea-tmp'
    this.baseElement.style.display = 'block'
    this.populate()
  }

  /**
   * Populates the modal html
   */
  async populate() {
    let template = await window.templateController.getTemplate('textareaModal')
    this.baseElement.innerHTML = template

    //append title
    this.baseElement.querySelector('.title').innerText = this.title

    //change textarea value
    let textareaElement = this.baseElement.querySelector('textarea')
    textareaElement.value = this.text != undefined ? this.text : ''

    //change button values
    this.baseElement.querySelector("[name='action-yes']").value = this.yesText

    //setup cancel buttons
    this.baseElement.querySelectorAll(`[name='action-close']`).forEach((element) => {
      element.onclick = (() => {
        this.close(null)
      }).bind(this)
    })

    //setup confirm button
    this.baseElement.querySelector("[name='action-yes']").onclick = (() => {
      this.close(textareaElement.value)
    }).bind(this)

    //append to body
    document.body.appendChild(this.baseElement)
  }

  close(result){
    this.baseElement.remove()
    this.callback(result)
  }
}
