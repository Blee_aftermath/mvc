/**
 * Module that allows the selection of crew on a job
 * @author Jake Cirino
 */
import {Util} from "../util.js"
import {CrewSelectorItem} from "./crewSelectorItem.js"

export const SELECTOR_TYPE = {
  CREW: 0,
  SUP: 1
}

/**
 * Controls a crew selector object
 * @author Jake Cirino
 */
export class CrewSelector{
  /**
   * Creates a new crew selector
   * @param {Object} data
   * @param {Object} opts
   */
  constructor(data, opts) {
    this.data = data
    this.opts = opts
    console.log(data)
    
    this.baseElement = document.querySelector('[name=blockCrewSelector]')
    
    this.supBaseElement = this.baseElement.querySelector('[name=supSelector]')
    this.supDom = Util.parseDOM(this.supBaseElement)
    if(this.opts.showSup){
      let supData = {
        jobID: this.data.jobID,
        available: this.data.supAvl,
        assigned: this.data.supAsn
      }
      this.supSelector = new CrewSelectorItem(this, supData, this.supDom, SELECTOR_TYPE.SUP)
    }else{
      Util.hide(this.supBaseElement)
    }
    
    this.crewBaseElement = this.baseElement.querySelector('[name=crewSelector]')
    this.crewDom = Util.parseDOM(this.crewBaseElement)
    let crewData = {
      jobID: this.data.jobID,
      available: this.data.tecAvl,
      assigned: this.data.tecAsn
    }
    this.crewSelector = new CrewSelectorItem(this, crewData, this.crewDom, SELECTOR_TYPE.CREW)
  }
  
  initiate(){
  
  }
}