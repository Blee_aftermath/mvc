import {SELECTOR_TYPE} from "./crewSelector.js"
import {Util} from "../util.js"

const FILTER_TYPE = {
  filterTeam: 'getTeamAvl',
  filterOffice: 'getOfficeAvl',
  filterRegion: 'getRegionAvl',
  filterAll: 'getAllAvl'
}

/**
 * Controls a set of crew selectors
 * @author Jake Cirino
 */
export class CrewSelectorItem{
  constructor(parent, data, dom, type) {
    this.parent = parent
    this.data = data
    this.dom = dom
    this.type = type
    this.opts = this.parent.opts
    this.filter = this.opts.showTeam ? "filterTeam" : "filterOffice"
    this.crewChangeEvent = new CustomEvent('crewChangeEvent') //an event to dispatch on crew change
    
    this.initiate()
    this.populate()
  }
  
  /**
   * Repopulates a selector with current values
   * @param {Object} data The data to fill
   * @param {HTMLElement} dom The dom to fill
   */
  populateSelector(data, dom){
    dom.innerHTML = ''
    for (const dataKey in data) {
      const emp = data[dataKey]
      let opt = document.createElement('option')
      opt.value = emp.emp_id
      opt.innerText = emp.emp_name
      dom.appendChild(opt)
    }
  }
  
  /**
   * Sets the filter of this crew selector
   * @param {string} filterType The name of the filter type element (filterTeam, ect.)
   * @param {boolean} forceRefresh
   */
  setFilter(filterType, forceRefresh = false){
    if(this.filter == filterType && !forceRefresh) return
    
    fetch('routes/routeCrewSelector.php', {
      method: 'post',
      headers: {'Content-Type': 'application/json;charset=utf8'},
      body: JSON.stringify({
        action: FILTER_TYPE[filterType],
        jobID: this.data.jobID,
        filterSup: this.type == SELECTOR_TYPE.SUP
      })
    })
      .then(res => res.json())
      .then(body => {
        if(body.ok){
          //reset filter highlighting
          this.dom[this.filter].classList.remove('hlyel')
          this.dom[filterType].classList.add('hlyel')
          this.filter = filterType
          
          //repopulate the selector
          this.data.available = body.data
          this.populateSelector(body.data, this.dom.available)
        }else{
          //TODO throw error
        }
      })
  }
  
  /**
   * Refreshes the assigned emps from the database
   */
  refreshAssignedEmps(){
    fetch('routes/routeCrewSelector.php', {
      method: 'post',
      headers: {'Content-Type': 'application/json;charset=utf8'},
      body: JSON.stringify({
        action: 'getAssignedEmps',
        jobID: this.data.jobID,
        filterSup: this.type == SELECTOR_TYPE.SUP
      })
    })
      .then(res => res.json())
      .then(body => {
        if(body.ok){
          this.data.assigned = body.data
          this.populateSelector(body.data, this.dom.assigned)
          document.dispatchEvent(this.crewChangeEvent) //dispatch the crew change event
        }else{
          //TODO throw error
        }
      })
  }
  
  /**
   * Assigns the selected available emp
   */
  assignEmp(){
    fetch('routes/routeCrewSelector.php', {
      method: 'post',
      headers: {'Content-Type': 'application/json;charset=utf8'},
      body: JSON.stringify({
        action: 'assignEmp',
        jobID: this.data.jobID,
        empID: this.dom.available.value,
        isSup: this.type == SELECTOR_TYPE.SUP
      })
    })
      .then(res => res.json())
      .then(body => {
        if(body.ok){
          this.refreshAssignedEmps()
          window.updateCrew()
        }
      })
  }
  
  unassignEmp(){
    fetch('routes/routeCrewSelector.php', {
      method: 'post',
      headers: {'Content-Type': 'application/json;charset=utf8'},
      body: JSON.stringify({
        action: 'unassignEmp',
        jobID: this.data.jobID,
        empID: this.dom.assigned.value,
        isSup: this.type == SELECTOR_TYPE.SUP
      })
    })
      .then(res => res.json())
      .then(body => {
        if(body.ok){
          this.refreshAssignedEmps()
          window.updateCrew()
        }else{
          //TODO error message
        }
      })
  }
  
  initiate(){
    if(!this.opts.showTeam) Util.hide(this.dom.filterTeam)
    this.dom[this.filter].classList.add('hlyel')
    
    //setup filter buttons
    this.dom.filterTeam.addEventListener('click', (() => {this.setFilter('filterTeam')}).bind(this))
    this.dom.filterOffice.addEventListener('click', (() => {this.setFilter('filterOffice')}).bind(this))
    this.dom.filterRegion.addEventListener('click', (() => {this.setFilter('filterRegion')}).bind(this))
    this.dom.filterAll.addEventListener('click', (() => {this.setFilter('filterAll')}).bind(this))
    
    //setup add/remove buttons
    this.dom.assign.addEventListener('click', this.assignEmp.bind(this))
    this.dom.unassign.addEventListener('click', this.unassignEmp.bind(this))
  }
  
  populate(){
    this.populateSelector(this.data.available, this.dom.available)
    this.populateSelector(this.data.assigned, this.dom.assigned)
  }
}