
var arrJobOb   = []; // Array of JOB objects
var arrJobID   = []; // Array of JOB IDs
var arrJobEr   = []; // Array of errored fields to tag in red

var arrInsOb   = []; // Array of Insurance objects
var arrInsID   = []; // Array of Insurance IDs
var arrInsEr   = []; // Array of errored fields to tag in red

var arrRefOb   = []; // Array of Reference objects
var arrRefID   = []; // Array of Reference IDs
var arrRefEr   = []; // Array of errored fields to tag in red

// ==================================================================================================== //
// === Service address and information === //
// ==================================================================================================== //

  function fnChgCustomer(id) {
      jsIndex = arrJobID.indexOf(id);
      document.getElementById("myModalCustomer").style.display  = "block";
      fnFillCustomer(jsIndex);
  }

  function fnFillCustomer(jsIndex) {
    document.getElementById("hidJobID").value                  = arrJobID[jsIndex];
    document.getElementById("inpCusAddress").value             = arrJobOb[jsIndex].addr1;
    document.getElementById("inpCusApt").value                 = arrJobOb[jsIndex].addr2;
    document.getElementById("inpCusCity").value                = arrJobOb[jsIndex].city;
    document.getElementById("selState").value                  = arrJobOb[jsIndex].state;
    document.getElementById("inpCusZip").value                 = arrJobOb[jsIndex].zip;
    document.getElementById("inpCusYrBuilt").value             = arrJobOb[jsIndex].yrbuilt;
    document.getElementById("inpInjuredName").value            = arrJobOb[jsIndex].injured;
    document.getElementById("InpRelProOwner").value            = arrJobOb[jsIndex].relation;

    if(arrJobOb[jsIndex].policeinv!='') {
      if(arrJobOb[jsIndex].policeinv==1) { document.getElementById("InpPoliceInv1").checked =1;  }
      if(arrJobOb[jsIndex].policeinv==0) { document.getElementById("InpPoliceInv2").checked =1;  }
    }

    if(arrJobOb[jsIndex].business!='') {  
      if(arrJobOb[jsIndex].business==1) { document.getElementById("InpBusiness1").checked =1;  }
      if(arrJobOb[jsIndex].business==0) { document.getElementById("InpBusiness2").checked =1;  }
    }

    if(arrJobOb[jsIndex].nonprofit!='') {  
      if(arrJobOb[jsIndex].nonprofit==1) { document.getElementById("InpBusNProfit1").checked =1;  }
      if(arrJobOb[jsIndex].nonprofit==0) { document.getElementById("InpBusNProfit2").checked =1;  }
    }

    document.getElementById("InpPoliceContact").value          = arrJobOb[jsIndex].pcontact;
    document.getElementById("InpPoliceDept").value             = arrJobOb[jsIndex].pdept;
    document.getElementById("InpBusComp").value                = arrJobOb[jsIndex].bcompany;
  }

  function fnCloseModalCustomer() {
    document.getElementById("hidJobID").value                  = "";
    document.getElementById("inpCusAddress").value             = "";
    document.getElementById("inpCusApt").value                 = "";
    document.getElementById("inpCusCity").value                = "";
    document.getElementById("selState").value                  = "";
    document.getElementById("inpCusZip").value                 = "";
    document.getElementById("inpCusYrBuilt").value             = "";
    document.getElementById("inpInjuredName").value            = "";
    document.getElementById("InpRelProOwner").value            = "";
    document.getElementById("myModalCustomer").style.display  = "none";

    for(var i=0; i<arrJobEr.length; i++) {
      document.getElementById(arrJobEr[i]).classList.remove("err");
    }
    delete(arrJobEr);
    document.getElementById("spanCustMessages").innerHTML = "";
  }

  function fnChgCustomer(id) {
    jsIndex = arrJobID.indexOf(id);
    document.getElementById("myModalCustomer").style.display  = "block";
    fnFillCustomer(jsIndex);
   }

   // ==================================================================================================== //
  // === Insurance information === //
  // ==================================================================================================== //

   function fnChgInsurance(id) {
    jsIndex = arrInsID.indexOf(id);
    document.getElementById("myModalInsurance").style.display  = "block";
    fnFillInsurance(jsIndex);
  }

  function fnFillInsurance(jsIndex) {
    document.getElementById("hidJobID").value              = arrInsID[jsIndex];
    document.getElementById("selInsCo").value              = arrInsOb[jsIndex].company;
    document.getElementById("inpInsuredName").value        = arrInsOb[jsIndex].insured;
    document.getElementById("inpPolicyNum").value          = arrInsOb[jsIndex].policy;
    document.getElementById("inpClaimNum").value           = arrInsOb[jsIndex].claim;
    document.getElementById("inpDeductable").value         = arrInsOb[jsIndex].deductable;
    document.getElementById("inpInsuredDob").value         = arrInsOb[jsIndex].dob;
    document.getElementById("selAdjuster").value           = arrInsOb[jsIndex].adjuster;
  }

  function fnCloseModalInsurance() {
    document.getElementById("selInsCo").value              = "";
    document.getElementById("inpInsuredName").value        = "";
    document.getElementById("inpPolicyNum").value          = "";
    document.getElementById("inpClaimNum").value           = "";
    document.getElementById("inpDeductable").value         = "";
    document.getElementById("inpInsuredDob").value         = "";
    document.getElementById("selAdjuster").value           = ""; 
    document.getElementById("myModalInsurance").style.display  = "none";
    for(var i=0; i<arrInsEr.length; i++) {
      document.getElementById(arrInsEr[i]).classList.remove("err");
    }
    delete(arrInsEr);
    document.getElementById("spanInsMessages").innerHTML = "";
 }

  // ==================================================================================================== //
  // === Referral information === //
  // ==================================================================================================== //

  function fnChgReferral(id) {
    jsIndex = arrRefID.indexOf(id);
    document.getElementById("myModalReferral").style.display  = "block";
    fnFillReferral(jsIndex);
  }

  function fnFillReferral(jsIndex) {
    document.getElementById("hidJobID").value           = arrJobID[jsIndex];
    document.getElementById("selRefSource").value       = arrRefOb[jsIndex].ref;
    document.getElementById("inpRefName").value         = arrRefOb[jsIndex].contact;
    document.getElementById("inpRefCompany").value      = arrRefOb[jsIndex].company;
    document.getElementById("inpRefTel").value          = arrRefOb[jsIndex].telephone;
    if(arrRefOb[jsIndex].ref=='99') {
      document.getElementById("inpRefOther").style.display='block';
      document.getElementById("inpRefOther").value        = arrRefOb[jsIndex].other;
    }
  }

  function fnCloseModalReferral() {
    document.getElementById("myModalReferral").style.display  = "none";
    document.getElementById("selRefSource").value       = "";
    document.getElementById("inpRefName").value         = "";
    document.getElementById("inpRefCompany").value      = "";
    document.getElementById("inpRefTel").value          = "";
    for(var i=0; i<arrRefEr.length; i++) {
      document.getElementById(arrRefEr[i]).classList.remove("err");
    }
    delete(arrRefEr);
    document.getElementById("spanRefMessages").innerHTML = "";
  }

  function fnSelectOther(value) {
    if(value==99) {
      document.getElementById("inpRefOther").style.display='block';
    } else {
      document.getElementById("inpRefOther").style.display='none';
    }
  }
 

