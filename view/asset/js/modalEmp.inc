<?php

// ==================================================================================================== //
// === FUNCTIONS === //
// ==================================================================================================== //

  function fnFillEmpJS($myRes) {

    echo "\n\n<script language=\"JavaScript\">";
    echo "\n<!-- Hide the script from old browsers --";

    echo "\nvar objEmp = {};"; // Object of Employee fields.  Can also = new Object();

    echo "\nobjEmp.id            = \"" . $myRes['emp_id']                                 . "\";";
    echo "\nobjEmp.fname         = \"" . prepareJS($myRes['emp_fname'])                   . "\";";
    echo "\nobjEmp.lname         = \"" . prepareJS($myRes['emp_lname'])                   . "\";";
    echo "\nobjEmp.title         = \"" . $myRes['emp_title']                              . "\";";
    echo "\nobjEmp.office        = \"" . $myRes['emp_office']                             . "\";";
    echo "\nobjEmp.pay_id        = \"" . prepareJS($myRes['emp_pay_id'])                  . "\";";
    echo "\nobjEmp.user          = \"" . prepareJS($myRes['emp_user'])                    . "\";";
    echo "\nobjEmp.email         = \"" . prepareJS($myRes['emp_email'])                   . "\";";
    echo "\nobjEmp.email_alt     = \"" . prepareJS($myRes['emp_email_alt'])               . "\";";
    echo "\nobjEmp.telc          = \"" . formatPhone($myRes['emp_telc'])                  . "\";";
    echo "\nobjEmp.telp          = \"" . formatPhone($myRes['emp_telp'])                  . "\";";
    echo "\nobjEmp.d_hire        = \"" . formatDText(strtotime($myRes['emp_dt_hire']))    . "\";";
    echo "\nobjEmp.d_rehire      = \"" . formatDText(strtotime($myRes['emp_dt_rehire']))  . "\";";
    echo "\nobjEmp.d_hepv        = \"" . formatDText(strtotime($myRes['emp_dt_hepv']))    . "\";";
    echo "\nobjEmp.d_pptrain     = \"" . formatDText(strtotime($myRes['emp_dt_pptrain'])) . "\";";
    echo "\nobjEmp.d_cetrain     = \"" . formatDText(strtotime($myRes['emp_dt_cetrain'])) . "\";";
    echo "\nobjEmp.d_erailsafe   = \"" . formatDText(strtotime($myRes['emp_dt_erailsafe'])) . "\";";
    echo "\nobjEmp.d_ready       = \"" . formatDText(strtotime($myRes['emp_dt_ready']))   . "\";";
    echo "\nobjEmp.d_term        = \"" . formatDText(strtotime($myRes['emp_dt_term']))    . "\";";
    echo "\nobjEmp.info          = \"" . prepareJS($myRes['emp_info'])                    . "\";";
    echo "\nobjEmp.is_iicrc      = \"" . $myRes['emp_is_iicrc']                           . "\";";
    echo "\nobjEmp.is_act        = \"" . $myRes['emp_is_act']                             . "\";";
    echo "\nobjEmp.is_ena        = \"" . $myRes['emp_is_ena']                             . "\";";
    echo "\nobjEmp.is_trn        = \"" . $myRes['emp_is_trn']                             . "\";";
    echo "\nobjEmp.ccstatus      = \"" . $myRes['emp_arr_ccstatus']                       . "\";";
    echo "\nobjEmp.pay           = \"" . $myRes['emp_arr_pay']                            . "\";";
    echo "\nobjEmp.notify        = \"" . $myRes['emp_notify']                             . "\";";

    echo "\narrEmpID.push(" . $myRes['emp_id'] . ");";
    echo "\narrEmpOb.push(objEmp);"; // Push this object into the array of Objects

    echo "\n// --End Hiding Here -->";
    echo "\n</script>\n";

  }

  function fnFillEmpPost() {

    echo "\n<script language=\"JavaScript\">";
    echo "\n<!-- Hide the script from old browsers --";

    echo "\nvar objEmp = {};"; // Create a new object to hold the posted values.

    if (isset($_SESSION['inpValue']['inpEmpID'])) {
      echo "\narrEmpID.push(\"" . prepareJS($_SESSION['inpValue']['inpEmpID']) . "\");";
      echo "\nobjEmp.id     = \"" . prepareJS($_SESSION['inpValue']['inpEmpID']) . "\";";
    }

    echo "\nobjEmp.fname       = \"" . prepareJS($_SESSION['inpValue']['inpEmpFName'])       . "\";";
    echo "\nobjEmp.lname       = \"" . prepareJS($_SESSION['inpValue']['inpEmpLName'])       . "\";";
    echo "\nobjEmp.title       = \"" . $_SESSION['inpValue']['selEmpTitle']                  . "\";";
    echo "\nobjEmp.office      = \"" . $_SESSION['inpValue']['selEmpOffice']                 . "\";";
    echo "\nobjEmp.pay_id      = \"" . prepareJS($_SESSION['inpValue']['inpEmpPayID'])       . "\";";
    echo "\nobjEmp.user        = \"" . prepareJS($_SESSION['inpValue']['inpEmpUser'])        . "\";";
    echo "\nobjEmp.email       = \"" . prepareJS($_SESSION['inpValue']['inpEmpEmail'])       . "\";";
    echo "\nobjEmp.email_alt   = \"" . prepareJS($_SESSION['inpValue']['inpEmpEmailAlt'])    . "\";";
    echo "\nobjEmp.telc        = \"" . prepareJS($_SESSION['inpValue']['inpEmpTelc'])        . "\";";
    echo "\nobjEmp.telp        = \"" . prepareJS($_SESSION['inpValue']['inpEmpTelp'])        . "\";";
    echo "\nobjEmp.d_hire      = \"" . prepareJS($_SESSION['inpValue']['inpDHire'])          . "\";";
    echo "\nobjEmp.d_rehire    = \"" . prepareJS($_SESSION['inpValue']['inpDRehire'])        . "\";";
    echo "\nobjEmp.d_hepv      = \"" . prepareJS($_SESSION['inpValue']['inpDHepv'])          . "\";";
    echo "\nobjEmp.d_pptrain   = \"" . prepareJS($_SESSION['inpValue']['inpDPPTrain'])       . "\";";
    echo "\nobjEmp.d_cetrain   = \"" . prepareJS($_SESSION['inpValue']['inpDCETrain'])       . "\";";
    echo "\nobjEmp.d_erailsafe = \"" . prepareJS($_SESSION['inpValue']['inpDERailSafe'])     . "\";";
    echo "\nobjEmp.d_ready     = \"" . prepareJS($_SESSION['inpValue']['inpDReady'])         . "\";";
    echo "\nobjEmp.d_term      = \"" . prepareJS($_SESSION['inpValue']['inpDTerm'])          . "\";";
    echo "\nobjEmp.info        = \"" . prepareJS($_SESSION['inpValue']['inpEmpInfo'])        . "\";";
    echo "\nobjEmp.is_iicrc    = \"" . (isset($_SESSION['inpValue']['chkEmpIsIICRC']) ? 1 : 0) . "\";";
    echo "\nobjEmp.is_act      = \"" . (isset($_SESSION['inpValue']['chkEmpIsAct']) ? 1 : 0) . "\";";
    echo "\nobjEmp.is_ena      = \"" . (isset($_SESSION['inpValue']['chkEmpIsEna']) ? 1 : 0) . "\";";
    echo "\nobjEmp.is_trn      = \"" . (isset($_SESSION['inpValue']['chkEmpIsTrn']) ? 1 : 0) . "\";";
    echo "\nobjEmp.ccstatus    = \"" . $_SESSION['inpValue']['selEmpCCStatus']               . "\";";
    echo "\nobjEmp.pay         = \"" . $_SESSION['inpValue']['selEmpPay']                    . "\";";
    echo "\nobjEmp.notify      = \"" . (isset($_SESSION['inpValue']['chkEmpNotify']) ? 1 : 0). "\";";

    echo "\narrEmpOb.push(objEmp);"; // Tack this onto the end of the array of Emp objects.

    foreach($_SESSION['errField'] AS $field) {

      echo "\narrEmpEr.push(\"" . $field    . "\");";
      echo "\ndocument.getElementById(\"" . $field . "\").classList.add(\"err\");";

    }

    // Show the modal dialog box

    if ($_SESSION['inpValue']['subEmp'] == "Add")  echo "\nfnAddEmp();";
    if ($_SESSION['inpValue']['subEmp'] == "Save") echo "\nfnChgEmp(" . $_SESSION['inpValue']['reoEmpID'] . ");";
    echo "\nfnFillEmp(arrEmpOb.length-1);";

    echo "// --End Hiding Here -->\n";
    echo "\n</script>\n";

  }

// ==================================================================================================== //
// === MODAL === //
// ==================================================================================================== //

  echo "<div class='modal' id='myModalEmp'>";
  echo "<div class='fixed'>";
  echo "<div class='body_stripe'></div>";

  echo "<div class='head'>";
  echo "<div class='content'>";

  echo "<span id='spanEmpHead'></span>";
  echo "<span style='float:right;'><a href='javascript:fnCloseModalEmp();'><img class='icon16' src='icons/no16.png' alt='Close' title='Close' /></a></span>";

  echo "</div>"; // content
  echo "</div>"; // head

  echo "<span id='spanEmpMessages'>";
  fnModalMessages();
  echo "</span>";

  echo "<div class='body'>";
  echo "\n<form method='post' name='frmModalEmp' action='modalPRG.php'>";
  echo "<div class='content'>";

  echo "<table class='info' id='tblEmp'>";

  $inpField = "reoEmpID";
  $inpLabel = "ID";
  $defClass = "inp06 reo";
  echo "<tr class='content' id='rowEmpID'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' readonly />";
  echo "</td></tr>";

  $inpLabel = "Name";
  echo "<tr class='content'><td>$inpLabel:</td>";
  echo "<td class='r'>";
  $inpField = "inpEmpFName";
  $defClass = "inp12 req";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' maxlength='$maxName' />";
  $inpField = "inpEmpLName";
  $defClass = "inp12 req";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' maxlength='$maxName' />";
  echo "</td></tr>";

  $sqlSelTitle = "SELECT * FROM title";
  $resSelTitle = $mysqli->query($sqlSelTitle);

  $inpField = "selEmpTitle";
  $inpLabel = "Dept";
  $defClass = "req";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<select class='$defClass' name='$inpField' id='$inpField'>";
  echo "<option value='' selected='selected'>--- Select ---</option>";
  while ($mySelTitle = $resSelTitle->fetch_assoc()) {
    echo "<option value='" . $mySelTitle['title_id'] . "'>" . prepareFrm($mySelTitle['title_dsca']) . "</option>";
  }
  echo "</select>";
  echo "</td></tr>";

  $sqlOffice = "SELECT * FROM office ORDER BY office_id";
  $resOffice = $mysqli->query($sqlOffice);

  $inpField = "selEmpOffice";
  $inpLabel = "Office";
  $defClass = "req";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<select class='$defClass' name='$inpField' id='$inpField'>";
  echo "<option value='' selected='selected'>--- Select ---</option>";
  while ($myOffice = $resOffice->fetch_assoc()) {
    echo "<option value='" . $myOffice['office_id'] . "'>" . prepareFrm($myOffice['office_name']) . "</option>";
  }
  echo "</select>";
  echo "</td></tr>";

  $inpField = "inpEmpPayID";
  $inpLabel = "Paylocity";
  $defClass = "inp08 opt";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' maxlength='$lenPayID' />";
  echo "</td></tr>";

  $inpField = "inpEmpUser";
  $inpLabel = "Username";
  $defClass = "req";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' maxlength='$maxUser' />";
  echo "</td></tr>";

  $inpField = "inpEmpPass";
  $inpLabel = "Password";
  $defClass = "req";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<input class='$defClass' type='password' name='$inpField' id='$inpField' value='' maxlength='$maxPass' />";
  echo "</td></tr>";

  $inpField = "inpEmpEmail";
  $inpLabel = "Pri Email";
  $defClass = "inp24 req";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' maxlength='$maxEmail' />";
  echo "</td></tr>";

  $inpField = "inpEmpEmailAlt";
  $inpLabel = "Alt Email";
  $defClass = "inp24 req";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' maxlength='$maxEmail' />";
  echo "</td></tr>";

  $inpField = "inpEmpTelc";
  $inpLabel = "Cell Phone";
  $defClass = "opt";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' maxlength='$maxTel' />";
  echo "</td></tr>";

  $inpField = "inpEmpTelp";
  $inpLabel = "Office Phone";
  $defClass = "opt";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' maxlength='$maxTel' />";
  echo "</td></tr>";

  $inpField = "selEmpPay";
  $inpLabel = "Pay";
  $defClass = "req";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<select class='$defClass' name='$inpField' id='$inpField'>";
  echo "<option value='' selected='selected'>--- Select ---</option>";
  foreach ($arrEmpPay AS $key => $value) {
    echo "<option value='" . $key . "'>" . $value . "</option>";
  }
  echo "</select>";
  echo "</td></tr>";

  $inpField = "selEmpCCStatus";
  $inpLabel = "Credit Card";
  $defClass = "req";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<select class='$defClass' name='$inpField' id='$inpField'>";
  echo "<option value='' selected='selected'>--- Select ---</option>";
  foreach ($arrCCStatus AS $key => $value) {
    echo "<option value='" . $key . "'>" . prepareFrm($value) . "</option>";
  }
  echo "</select>";
  echo "</td></tr>";

  $inpField = "inpDHire";
  $inpLabel = "Hire Date";
  $defClass = "inp12 req";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' maxlength='$maxDate' />";
  echo "</td></tr>";
  echo "<script>$(function() { $('#".  $inpField . "').datepicker(); }); </script>";

  $inpField = "inpDRehire";
  $inpLabel = "Rehire Date";
  $defClass = "inp12 opt";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' maxlength='$maxDate' />";
  echo "</td></tr>";
  echo "<script>$(function() { $('#".  $inpField . "').datepicker(); }); </script>";

  $inpField = "inpDHepv";
  $inpLabel = "HEP-B Vaccine";
  $defClass = "inp12 opt";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' maxlength='$maxDate' />";
  echo "</td></tr>";
  echo "<script>$(function() { $('#".  $inpField . "').datepicker(); }); </script>";

  $inpField = "inpDPPTrain";
  $inpLabel = "P&P Training";
  $defClass = "inp12 opt";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' maxlength='$maxDate' />";
  echo "</td></tr>";
  echo "<script>$(function() { $('#".  $inpField . "').datepicker(); }); </script>";

  $inpField = "inpDCETrain";
  $inpLabel = "C.E. Training";
  $defClass = "inp12 opt";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' maxlength='$maxDate' />";
  echo "</td></tr>";
  echo "<script>$(function() { $('#".  $inpField . "').datepicker(); }); </script>";

  $inpField = "inpDERailSafe";
  $inpLabel = "eRailSafe Exp";
  $defClass = "inp12 opt";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' maxlength='$maxDate' />";
  echo "</td></tr>";
  echo "<script>$(function() { $('#".  $inpField . "').datepicker(); }); </script>";

  $inpField = "inpDReady";
  $inpLabel = "Job-Ready";
  $defClass = "inp12 opt";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<input  class='$defClass' type='text' name='$inpField' id='$inpField' maxlength='$maxDate' />";
  echo "</td></tr>";
  echo "<script>$(function() { $('#".  $inpField . "').datepicker(); }); </script>";

  $inpField = "inpDTerm";
  $inpLabel = "Term Date";
  $defClass = "inp12 opt";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' maxlength='$maxDate' />";
  echo "</td></tr>";
  echo "<script>$(function() { $('#".  $inpField . "').datepicker(); }); </script>";

  $inpField = "inpEmpInfo";
  $inpLabel = "Additional<br />Information";
  $defClass = "opt";
  echo "<tr class='content'><td class='l'>$inpLabel:</td><td class='r'>";
  echo "<textarea class='$defClass' name='$inpField' id='$inpField' rows='6' cols='36'></textarea>";
  echo "</td></tr>";

  $inpField = "chkEmpIsIICRC";
  $inpLabel = "IICRC Certified";
  $inpText  = "Employee is IICRC Certified.";
  $inpHelp  = "Refer to documentation on Docs Tab.";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='l'>";
  echo "<input type='checkbox' name='$inpField' id='$inpField' /> $inpText";
  echo "&nbsp;<img class='icon16' title='$inpHelp' alt='Help' src='icons/question16.png' />";
  echo "</td></tr>";

  $inpField = "chkEmpIsAct";
  $inpLabel = "Active";
  $inpText  = "Ready and able to work.";
  $inpHelp  = "Active employee, ready to service customers and not on long-term leave.";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='l'>";
  echo "<input type='checkbox' name='$inpField' id='$inpField' /> $inpText";
  echo "&nbsp;<img class='icon16' title='$inpHelp' alt='Help' src='icons/question16.png' />";
  echo "</td></tr>";

  $inpField = "chkEmpIsEna";
  $inpLabel = "Enabled";
  $inpText  = "Can log in to the Portal.";
  $inpHelp  = "Uncheck to lock out an employee.";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='l'>";
  echo "<input type='checkbox' name='$inpField' id='$inpField' /> $inpText";
  echo "&nbsp;<img class='icon16' title='$inpHelp' alt='Help' src='icons/question16.png' />";
  echo "</td></tr>";

  $inpField = "chkEmpIsTrn";
  $inpLabel = "Training";
  $inpText  = "Employee is in training.";
  $inpHelp  = "Will show employee name in (parentheses) on the Team page.";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='l'>";
  echo "<input type='checkbox' name='$inpField' id='$inpField' /> $inpText";
  echo "&nbsp;<img class='icon16' title='$inpHelp' alt='Help' src='icons/question16.png' />";
  echo "</td></tr>";

  $inpField = "chkEmpNotify";
  $inpLabel = "Notify";
  $inpText  = "Send all email notifications.";
  $inpHelp  = "Checking this box will result in a lot of Portal email being sent to this employee.";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='l'>";
  echo "<input type='checkbox' name='$inpField' id='$inpField' /> $inpText";
  echo "&nbsp;<img class='icon16' title='$inpHelp' alt='Help' src='icons/question16.png' />";
  echo "</td></tr>";

  $arrButtonRowEmp[] = "<input class='inp12' type='submit' name='subEmp'    id='subEmp'    value='Nothing'/>";
  $arrButtonRowEmp[] = "<input class='inp12' type='button' name='butConEmp' id='butConEmp' value='Delete' onclick='fnConfirmDelEmp();' />";
  $arrButtonRowEmp[] = "<input class='inp12' type='button' name='butCanEmp' id='butCanEmp' value='Cancel' onclick='fnCloseModalEmp();' />";

  echo "<tr class='content'><td></td><td>" . implode("", $arrButtonRowEmp) . "</td></tr>";

  echo "</table>";

  echo "</div>"; // content
  echo "<span id='spanEmpConfirm'></span>";
  echo "</form>";
  echo "</div>"; // body

  echo "<div class='body_stripe'></div>";
  echo "</div>"; // fixed
  echo "</div>"; // modal

?>