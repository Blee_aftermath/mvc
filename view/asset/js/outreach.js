import { default as note, close as closeNote } from './modules/lib/note.js'

/**
 * Provides ajax interface for outreach.php
 * @author Scott Kiehn
 */
export class Outreach {
  /**
   * Creates a new Outreach class
   */
  constructor() {
    this.dom = {}
    this.dom.offices = document.getElementById('offices')
    
    this.dom.offices.addEventListener('click', (ev) => this.cellUI(ev))
    this.dom.offices.addEventListener('keyup', (ev) => this.cellUI(ev))
    this.dom.offices.addEventListener('change', (ev) => this.cellUI(ev))

    document.querySelector('.button-row').addEventListener('click', (ev) => this.formUI(ev))
  }

  /**
   * Form Row/Cell interface
   * @param {Object} ev
   */
  cellUI(ev) {
    const el = ev.target,
          office = el.closest('tr')

    if (!el.name) return

    if (ev.type == 'click') {
      el.select()
      return
    }

    for (const m of office.querySelectorAll('[type=number]')) {
      m.value != m.dataset.value
        ? m.classList.add('rev')
        : m.classList.remove('rev')
    }
  }

  /**
   * Form button interface
   * @param {Object} ev
   */
  formUI(ev) {
    const el = ev.target

    if (el.matches('[name=save]')) {
      const inpData = {}
      for (const office of this.dom.offices.querySelectorAll('tr[data-office]')) {
        const officeID = office.dataset.office
        if (officeID) {
          for (const m of office.querySelectorAll('[type=number]')) {
            if (m.value != m.dataset.value) {

              if (!inpData[officeID])
                inpData[officeID] = {office: officeID}

              inpData[officeID][m.dataset.month] = m.value
            }
          }
        }
      }

      //console.log(inpData)

      this.saveRevised(el.dataset.year, inpData)
    }
    
    if (el.matches('[name=reset]')) {
      for (const office of this.dom.offices.querySelectorAll('tr')) {
        for (const m of office.querySelectorAll('[type=number]')) {
          m.value = m.dataset.value
          m.classList.remove('rev')
        }
      }
    }
  }

  /**
   * Save revised rows
   * @param {Int} yrView
   * @param {Object} inpData
   */
  async saveRevised(yrView, inpData) {

    const saveNote = note({ text: 'Saving...', class: 'alert', time: 0 })

    const post = await fetch('outreachPRG.php', {
      method: 'post',
      headers: { 'Content-Type': 'application/json;charset=utf-8' },
      body: JSON.stringify({ action: 'save', yrView: yrView, inpData: inpData })
    })

    const res = await post.json()

    closeNote(saveNote)

    if (res.ok) {
      for (const o in res.data) {

        const office = this.dom.offices.querySelector(`[data-office='${o}']`)
        for (const m in res.data[o]) {

          const month = office.querySelector(`[name=mo_${m}]`),
                value = res.data[o][m]

          month.value = value.go_outreach
          month.setAttribute('data-value', value.go_outreach)
          month.classList.remove('rev')
        }
      }

      note({ text: 'The office data has been saved.' })
    } else {
      note({ text: res.msg || 'There has been an error with your request.', class: 'error' })
    }
  }
}
