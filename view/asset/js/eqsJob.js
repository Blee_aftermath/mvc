import {Util} from './util.js'
import { default as formModal } from './modules/ui/formModal.js'
import { default as note } from './modules/lib/note.js'

/**
 * Class for Environmental Quality Jobs admin page
 * @author Scott Kiehn
 */

export class EQSJob {
  /**
   * Creates a new EQSJob class
   */
  constructor() {

    this.dom = {}

    // Parent of EQS Job DOM
    this.dom.base = document.querySelector('#eqsJob')
    this.dom.info = document.querySelector('#eqsInfo')
    this.dom.tabs = document.querySelector('#eqsTabs')

    this.pay = {
      UNK: {Start: true,  Comp: true },
      PIA: {Start: true,  Comp: true },
      POS: {Start: true,  Comp: true },
      PBR: {Start: false, Comp: true },
      N30: {Start: false, Comp: false},
      YES: {Start: false, Comp: false}
    }

    // Job data attached to a the base element
    this.data = this.dom.base.dataset.job ? JSON.parse(this.dom.base.dataset.job) : {}

    this.jobAddForm(document.querySelector('#jobAddForm')) //init form if available

    this.jobFlowForm(document.querySelector('#jobFlowForm')) //init form if available

    this.dom.base.addEventListener('click', (ev) => {
      if (ev.target.matches('[data-opts]')) {

        const fModalName = JSON.parse(ev.target.dataset.opts).modal,
          fModal = document.getElementById(fModalName)

        //This modal handled through jobAddForm()
        if (fModalName == 'modal-form-new') return

        //For the job modal, do a bit extra
        if (fModalName == 'modal-form-job') {
          for (const type of document.getElementById('job_class_job_prop')) {
            type.style.display = type.dataset.class == ev.target.dataset.class ? 'block' : 'none'
          }
        }

        formModal(ev.target)
      }
    })

    for (const save of document.querySelectorAll('.save-form'))
      save.addEventListener('click', () => this.saveForm(save.closest('form')))

    document.addEventListener('eqsServicesEvent', () => this.refreshInfoView())
  }

  /**
   * Get customer information
   * @param {Object} modalDom
   */
  addClientInfo = async (modalDom) => {
    if (modalDom.jeqs_cust.value) {

      const post = await fetch('eqsJobPRG.php', {
        method: 'post',
        headers: { 'Content-Type': 'application/json;charset=utf-8' },
        body: JSON.stringify({action: 'selectCust', cus_id: modalDom.jeqs_cust.value})
      })

      const res = await post.json()

      if ( res.ok ) {
        for (const d in res.data) {
          modalDom[d].value = res.data[d] ? res.data[d] : null
        }
      } else {
        note({class: 'error', text: 'The customer address info could not be retrieved.'})
      }
    }
  }

  /**
   * Inits job add form
   * @param {Element} form
   */
  jobAddForm = (form) => {

    if (!form) return

    const addDom = Util.parseDOM(form),
          defaults = {modal: 'modal-form-new', params: {action: 'insert'}},
          infoDom = Util.parseDOM(document.getElementById(defaults.modal))

    addDom.add_job_icon.addEventListener('click', () => {

      const classType = addDom.job_class_job_prop.options[addDom.job_class_job_prop.selectedIndex].dataset.class

      let error = 0

      if (!addDom.job_yr.value.match(/^\d\d$/)) error++
      if (!addDom.job_class_job_prop.value.match(/^\d+?-\d+?-\d+$/)) error++

      if (error) {
        note({class: 'error', text: 'Please provide complete information.'})
        addDom.add_job_icon.setAttribute('data-opts', JSON.stringify(defaults))
        return
      }

      const opts = JSON.parse(JSON.stringify(defaults))
      opts.params.job_yr             = addDom.job_yr.value
      opts.params.job_class_job_prop = addDom.job_class_job_prop.value,

      addDom.add_job_icon.setAttribute('data-opts', JSON.stringify(opts))

      infoDom.class_type_dsca.innerText = addDom.job_class_job_prop.options[addDom.job_class_job_prop.selectedIndex].dataset.dsca

      for (const option of infoDom.job_class_job_prop) {
        if (!option.value) continue
        option.style.display = (Number(option.dataset.class) == Number(classType)) ? 'block' : 'none'
      }

      formModal(addDom.add_job_icon)
    })

    infoDom.jeqs_cust.addEventListener('change', () => this.addClientInfo(infoDom))
  }

  /**
   * Inits job flow form
   * @param {Element} form
   */
  jobFlowForm = (form) => {
    if (!form) return
    const flowDom = Util.parseDOM(form)
    for (const id of ['Start', 'Comp']) {

      const dateEl = flowDom[`selD${id}`],
            timeEl = flowDom[`selT${id}`]

      jQuery(dateEl).datepicker()
      dateEl.disabled = this.pay[this.data.pmt_exp_code][id]
      timeEl.disabled = this.pay[this.data.pmt_exp_code][id]
    }
    jQuery('#selDPaid').datepicker()
  }

  /**
   * Refreshes view info based on an event
   */
  refreshInfoView = () => {
    const infoDom = Util.parseDOM(this.dom.info)

    let jobSoAmt = 0,
        jobCompAmt = 0,
        eqsServList = ''

    for (const item of this.dom.tabs.querySelectorAll('.service-item')) {
      const servDOM = Util.parseDOM(item)
      jobSoAmt += servDOM.jsvo_amt_ini.value * 100
      jobCompAmt += servDOM.jsvo_amt_fin.value * 100
      eqsServList += `<tr><td>${servDOM.svo_dsca.innerText}</td><td>$${servDOM.jsvo_amt_fin.value}</td><td>${servDOM.jsvo_note.value}</td></tr>`
    }

    infoDom.job_so_amt.innerText = Util.formatCurrency(jobSoAmt)
    infoDom.job_comp_amt.innerText = Util.formatCurrency(jobCompAmt)
    infoDom.eqsServiceList.innerHTML = eqsServList
  }

  /**
   * Save modal forms
   * @param {Element} form
   */
  saveForm = async (form) => {

    const inpData = {},
      modal = form.closest('.modal')

    let errors = 0

    for (const inp of form.querySelectorAll('[name]')) {
      inpData[inp.name] = inp.value
      if (inp.classList.contains('req')) if (inp.value.length == 0) errors++
    }

    if (errors) return modal.querySelector('.error').innerHTML = '<p>Values for the required items were not provided.</p>';

    const post = await fetch('eqsJobPRG.php', {
      method: 'post',
      headers: { 'Content-Type': 'application/json;charset=utf-8' },
      body: JSON.stringify(inpData)
    })

    const res = await post.json()

    if ( res.ok ) {
      modal.querySelector('.error').innerHTML = '';
      modal.querySelector('.message').innerHTML = `<p>${res.msg}</p>`;
      setTimeout(() => {
        location.assign(res.page)
        if (res.page.match(/#/)) location.reload()
      }, 750)
    } else {
      const error = []

      if (typeof(res.msg) == 'object') {
        for (const m of res.msg) error.push(m.msg)
      } else {
        error[0] = res.msg
      }

      modal.querySelector('.error').innerHTML = `<p>${error.join('<br />')}</p>`;
    }
  }
}
