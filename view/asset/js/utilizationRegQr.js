import { default as note, close as closeNote } from './modules/lib/note.js'

String.prototype.toNum = function () {return Number(this)}
Number.prototype.toNum = function () {return Number(this)}

/**
 * Provides ajax interface for utilizationRegQr.php
 * @author Scott Kiehn
 */
export class UtilizationRegQr {
  /**
   * Creates a new UtilizationRegQr class
   */
  constructor() {
    this.dom = {}
    this.dom.regions = document.getElementById('regions')
    this.dom.totals  = {}

    this.num = {}
    //this.num.cols = 12
    this.num.rows = this.dom.regions.dataset.regions.toNum()

    this.tags = ['amex_rev', 'sal_rev', 'util_job', 'util_mkt', 'util_ot']

    for (const t of this.dom.regions.querySelectorAll('[id]'))
      this.dom.totals[ t.id ] = t

    this.dom.regions.addEventListener('click',  (ev) => this.cellUI(ev))
    this.dom.regions.addEventListener('keyup',  (ev) => this.cellUI(ev))
    this.dom.regions.addEventListener('change', (ev) => this.cellUI(ev))

    document.querySelector('.button-row').addEventListener('click', (ev) => this.formUI(ev))
  }

  /**
   * Form Row/Cell interface
   * @param {Object} ev
   */
  cellUI(ev) {
    const el = ev.target

    if (ev.type == 'click') {
      el.select()
      return
    }

    // Return if keyup on TAB
    if (ev.which == 9) return

    Number(el.value) !== Number(el.dataset.value) ? el.classList.add('rev') : el.classList.remove('rev')
  }

  /**
   * Form button interface
   * @param {Object} ev
   */
  formUI(ev) {
    const el = ev.target

    if (el.matches('[name=save]')) {
      const inpData = {}

      for (const region of this.dom.regions.querySelectorAll('tbody[data-region]')) {
        const regionID = region.dataset.region
        if (regionID) {
          for (const m of region.querySelectorAll('[type=number]')) {

            if (!m.dataset.tag) continue

            if (m.value != m.dataset.value) {

              if (!inpData[regionID])
                inpData[regionID] = {region: regionID}

              if (!inpData[regionID][m.dataset.tag])
                inpData[regionID][m.dataset.tag]= {}

              inpData[regionID][m.dataset.tag][m.dataset.col] = m.value
            }
          }
        }
      }

      //console.log(inpData)

      this.saveRevised(el.dataset.year, inpData)
    }

    if (el.matches('[name=reset]')) {
      for (const region of this.dom.regions.querySelectorAll('tbody[data-region]')) {
        for (const m of region.querySelectorAll('[type=number]')) {
          m.value = m.dataset.value
          m.classList.remove('rev')
        }
      }
    }

  }

  /**
   * Save revised rows
   * @param {Int} yrView
   * @param {Object} inpData
   */
  async saveRevised(yrView, inpData) {

    const saveNote = note({ text: 'Saving...', class: 'alert', time: 0 })

    const post = await fetch('utilizationRegQrPRG.php', {
      method: 'post',
      headers: { 'Content-Type': 'application/json;charset=utf-8' },
      body: JSON.stringify({ action: 'save', yrView: yrView, inpData: inpData })
    })

    const res = await post.json()

    closeNote(saveNote)

    if (res.ok) {

      for (const regionID in res.data) {

        const region = this.dom.regions.querySelector(`tbody[data-region='${regionID}']`)

        for (const col in res.data[regionID]) {

          const d = res.data[regionID][col]

          for (const row of this.tags) {

            const cell = region.querySelector(`[name=qr_${row}_${col}]`)

            cell.value = d[`grq_per_${row}`]
            cell.setAttribute('data-value', d[`grq_per_${row}`])
            cell.classList.remove('rev')
          }
        }
      }

      note({ text: 'The region quarterly goal data has been saved.' })
    } else {
      note({ text: res.msg || 'There has been an error with your request.', class: 'error' })
    }
  }
}
