
var arrItemOb   = []; // Array of Item objects
var arrItemID   = []; // Array of Item IDs
var arrItemEr   = []; // Array of errored fields to tag in red

function fnAddItem() {
  document.getElementById("spanItemHead").innerHTML     = "Add Item";
  document.getElementById("rowItemID").hidden           = true;
  document.getElementById("reoItemID").disabled         = true;
  document.getElementById("subAddItem").style.display   = "inline";
  document.getElementById("subChgItem").style.display   = "none";
  document.getElementById("butConItem").style.display   = "none";
  document.getElementById("myModalItem").style.display  = "block";
}

function fnChgItem(id) {
  jsIndex = arrItemID.indexOf(id);
  document.getElementById("spanItemHead").innerHTML     = "Edit Item";
  document.getElementById("rowItemID").hidden           = false;
  document.getElementById("reoItemID").disabled         = false;
  document.getElementById("subAddItem").style.display   = "none";
  document.getElementById("subChgItem").style.display   = "inline";
  document.getElementById("butConItem").style.display   = "inline";
  document.getElementById("myModalItem").style.display  = "block";
  fnFillItem(jsIndex);
}

function fnFillItem(jsIndex) {
  document.getElementById("reoItemID").value            = arrItemID[jsIndex];
  document.getElementById("inpItemName").value          = arrItemOb[jsIndex].name;
  document.getElementById("selTyp").value               = arrItemOb[jsIndex].typ_code;
  document.getElementById("selCat").value               = arrItemOb[jsIndex].cat_code;
  document.getElementById("selVendor").value            = arrItemOb[jsIndex].vendor_code;
  document.getElementById("selItemUOM").value           = arrItemOb[jsIndex].uom_code;
  document.getElementById("inpItemInfo").value          = arrItemOb[jsIndex].info;
  document.getElementById("inpItemQtyPer").value        = arrItemOb[jsIndex].qty_per;
  document.getElementById("chkItemIsAct").checked       = (arrItemOb[jsIndex].is_act == 1 ? true : false);
}

function fnConfirmDelItem() {
  strConfirm  = "<div class='content alert'>";
  strConfirm += "Delete this Item?&nbsp;&nbsp;";
  strConfirm += "<input class='inp08' type='submit' name='subDelItemY' value='Yes' />";
  strConfirm += "<input class='inp08' type='button' name='butDelItemN' value='No' onclick='fnDelItemN()' />";
  strConfirm += "</div>";
  document.getElementById("spanItemConfirm").innerHTML = strConfirm;
}

function fnDelItemN() {
  document.getElementById("spanItemConfirm").innerHTML = "";
}

function fnCloseModalItem() {
  document.getElementById("reoItemID").value            = "";
  document.getElementById("inpItemName").value          = "";
  document.getElementById("selTyp").value               = "";
  document.getElementById("selCat").value               = "";
  document.getElementById("selVendor").value            = "";
  document.getElementById("selItemUOM").value           = "";
  document.getElementById("inpItemInfo").value          = "";
  document.getElementById("inpItemQtyPer").value        = "";
  document.getElementById("chkItemIsAct").checked       = false;
  document.getElementById("myModalItem").style.display  = "none";
  for(var i=0; i<arrItemEr.length; i++) {
    document.getElementById(arrItemEr[i]).classList.remove("err");
  }
}