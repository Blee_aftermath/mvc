<?php

// ==================================================================================================== //
// === FUNCTIONS === //
// ==================================================================================================== //

  function fnFillJobJS($myRes) {

    echo "\n\n<script language=\"JavaScript\">";
    echo "\n<!-- Hide the script from old browsers --";

    echo "\nvar objJob = {};"; // Object of Job fields.  Can also = new Object();

    echo "\nobjJob.id          = \"" . $myRes['job_id']                       . "\";";
    echo "\nobjJob.injured     = \"" . $myRes['job_decedent']                 . "\";";
    echo "\nobjJob.relation    = \"" . $myRes['job_cust_other']               . "\";";
    echo "\nobjJob.addr1       = \"" . $myRes['job_prop_addr1']               . "\";";
    echo "\nobjJob.addr2       = \"" . $myRes['job_prop_addr2']               . "\";";
    echo "\nobjJob.city        = \"" . $myRes['job_prop_city']                . "\";";
    echo "\nobjJob.state       = \"" . $myRes['job_prop_state']               . "\";";
    echo "\nobjJob.zip         = \"" . prepareJS($myRes['job_prop_zip'])      . "\";";
    echo "\nobjJob.yrbuilt     = \"" . prepareJS($myRes['job_prop_yr_built']) . "\";";
    echo "\nobjJob.policeinv   = \"" . prepareJS($myRes['job_is_police_inv']) . "\";";
    echo "\nobjJob.pcontact    = \"" . prepareJS($myRes['job_police_name'])   . "\";";
    echo "\nobjJob.business    = \"" . prepareJS($myRes['job_is_business'])   . "\";";
    echo "\nobjJob.pdept       = \"" . prepareJS($myRes['job_police_dept'])   . "\";";
    echo "\nobjJob.bcompany    = \"" . prepareJS($myRes['job_cmp_name'])      . "\";";
    echo "\nobjJob.nonprofit   = \"" . prepareJS($myRes['job_is_nonprofit'])  . "\";";


    echo "\narrJobID.push(" . $myRes['job_id'] . ");";
    echo "\narrJobOb.push(objJob);"; // Push this object into the array of Objects

    echo "\n// --End Hiding Here -->";
    echo "\n</script>\n";

  }

  function fnFillJobPost() {
    echo "\n<script language=\"JavaScript\">";
    echo "\n<!-- Hide the script from old browsers --";

    echo "\nvar objJob = {};"; // Create a new object to hold the posted values.

    if (isset($_SESSION['inpValue']['hidJobID'])) {
      echo "\narrJobID.push(\"" . prepareJS($_SESSION['inpValue']['hidJobID']) . "\");";
      echo "\nobjJob.id     = \"" . prepareJS($_SESSION['inpValue']['hidJobID']) . "\";";
    }

    echo "\nobjJob.injured       = \"" . prepareJS($_SESSION['inpValue']['inpInjuredName'])     . "\";";
    echo "\nobjJob.relation      = \"" . prepareJS($_SESSION['inpValue']['InpRelProOwner'])     . "\";";
    echo "\nobjJob.addr1         = \"" . prepareJS($_SESSION['inpValue']['inpCusAddress'])      . "\";";
    echo "\nobjJob.addr2         = \"" . prepareJS($_SESSION['inpValue']['inpCusApt'])          . "\";";
    echo "\nobjJob.city          = \"" . prepareJS($_SESSION['inpValue']['inpCusCity'])         . "\";";
    echo "\nobjJob.state         = \"" . prepareJS($_SESSION['inpValue']['selState'])           . "\";";
    echo "\nobjJob.zip           = \"" . prepareJS($_SESSION['inpValue']['inpCusZip'])          . "\";";
    echo "\nobjJob.yrbuilt       = \"" . prepareJS($_SESSION['inpValue']['inpCusYrBuilt'])      . "\";";
    if (isset($_SESSION['inpValue']['InpPoliceInv'])) {
      echo "\nobjJob.policeinv   = \"" . prepareJS($_SESSION['inpValue']['InpPoliceInv'])       . "\";";
    }
    echo "\nobjJob.pcontact      = \"" . prepareJS($_SESSION['inpValue']['InpPoliceContact'])   . "\";";
    if (isset($_SESSION['inpValue']['InpBusiness'])) {
      echo "\nobjJob.business      = \"" . prepareJS($_SESSION['inpValue']['InpBusiness'])        . "\";";
    }
    echo "\nobjJob.pdept         = \"" . prepareJS($_SESSION['inpValue']['InpPoliceDept'])      . "\";";
    echo "\nobjJob.bcompany      = \"" . prepareJS($_SESSION['inpValue']['InpBusComp'])         . "\";";
    if (isset($_SESSION['inpValue']['InpBusiness'])) {
      echo "\nobjJob.nonprofit     = \"" . prepareJS($_SESSION['inpValue']['InpBusNProfit'])      . "\";";
    }
    

    echo "\narrJobOb.push(objJob);"; // Tack this onto the end of the array of Job objects.

    foreach($_SESSION['errField'] AS $field) {

      echo "\narrJobEr.push(\"" . $field    . "\");";
      echo "\ndocument.getElementById(\"" . $field . "\").classList.add(\"err\");";

    }

    // Show the modal dialog box

    if (isset($_SESSION['inpValue']['subChgCus'])) echo "\nfnChgCustomer(" . $_SESSION['inpValue']['hidJobID'] . ");";
    echo "\nfnFillCustomer(arrJobOb.length-1);";

    echo "// --End Hiding Here -->\n";
    echo "\n</script>\n";

  }
// ==================================================================================================== //
// === MODAL === //
// ==================================================================================================== //

  echo "<div class='modal' id='myModalCustomer'>";
  echo "<div class='fixed'>";
  echo "<div class='body_stripe'></div>";

  echo "<div class='head'>";
  echo "<div class='content'>";

  echo "<span id='spanCustomerHead'>Service Address & Other Information </span>";
  echo "<span style='float:right;'><a href='javascript:fnCloseModalCustomer(1);'><img class='icon16' src='icons/no16.png' alt='Close' title='Close' /></a></span>";

  echo "</div>"; // content
  echo "</div>"; // head

  echo "<span id='spanCustMessages'>";
  fnModalMessagesBox('Customer');
  echo "</span>";
   
  echo "<div class='body'>";
  echo "\n<form method='post' name='frmModalCustomer' action='jifPRG.php'>";
  echo "<div class='content'>";

  echo "<table class='info' id='tblCustomer'>";
  if (isset($valJobID)) {
        $inpField = "hidJobID";
        $defValue = $valJobID;
        echo "<input type='hidden' name='$inpField' id='$inpField' value='" . $defValue . "' />";
  }

  echo "<tr class='heading'><th colspan='2'>Service address</th></tr>";
  
  $inpField = "inpCusAddress";
  $inpLabel = "Street Address";
  $defValue = "";
  $defClass = "opt";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' value='$defValue' list='off' />";
  echo "</td></tr>";

  $inpField = "inpCusApt";
  $inpLabel = "Apt / Unit / Suite";
  $defValue = "";
  $defClass = "inp26 opt";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' value='$defValue' list='off' />";
  echo "</td></tr>";

  $inpField = "inpCusCity";
  $inpLabel = "City";
  $defValue = "";
  $defClass = "req";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' value='$defValue' list='off' />";
  echo "</td></tr>";
 
  $sqlState = "SELECT * FROM state ORDER BY st_name";
  $resState = $mysqli->query($sqlState) or die('Get state failed');
  $inpField = "selState";
  $inpLabel = "State / Zip:";
  $defValue = "";
  $defClass = "inp10 req";

  echo "<tr class='content'><td class='l'>$inpLabel</td><td class='r'>";
  echo "<select class='$defClass' name='$inpField' id='$inpField'>";
  echo "<option value='0' selected='selected'>--- Select ---</option>";
  while ($myState = $resState->fetch_assoc()) {
    echo "<option value='" . $myState['st_id'] . "'";
    if ($myState['st_id'] == $defValue) { echo " selected='selected'"; }
    echo ">" . $myState['st_name'] . "</option>";
  } echo "</select>";

  $inpField = "inpCusZip";
  $defValue = "";
  $defClass = "inp06 req";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' value='$defValue' />";
  echo "</td></tr>";

  $inpField = "inpCusYrBuilt";
  $inpLabel = "Year Built:";
  $defValue = "";
  $defClass = "opt";
  $maxYear = 4;
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel</label></td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' value='$defValue' maxlength='$maxYear' list='off' />";
  echo "</td></tr>";
 
  $inpField = "inpInjuredName";
  $inpLabel = "Injured Name:";
  $defValue = "";
  $defClass = "inp26 opt";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' value='$defValue' list='off' />";
  echo "</td></tr>";

  $inpField = "InpRelProOwner";
  $inpLabel = "Relationship to Property Owner:";
  $defValue = "";
  $defClass = "inp26 opt";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel</label></td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' value='$defValue' list='off' />";
  echo "</td></tr>";

  echo "<tr class='heading'><th colspan='2'>Other Information</th></tr>";

  $inpField = "InpPoliceInv";
  $inpField1 = "InpPoliceInv1";
  $inpField2 = "InpPoliceInv2";
  $inpLabel = "Are the Police Involved ?";
  if($defValue=='1') {$checked = "checked";} else {$checked="";}
  $defClass = "inp26 opt";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel</label></td><td class='r'>";
  echo "<input class='$defClass' type='radio' name='$inpField' id='$inpField1' value='1' list='off' $checked/> Yes ";
  echo "<input class='$defClass' type='radio' name='$inpField' id='$inpField2'  value='0' list='off' $checked/> No ";
  echo "</td></tr>";

  $inpField = "InpPoliceContact";
  $inpLabel = "If yes, Contact name:";
  $defValue = "";
  $defClass = "inp26 opt";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel</label></td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' value='$defValue' list='off' />";
  echo "</td></tr>";

  $inpField = "InpPoliceDept";
  $inpLabel = "If yes, Dept:";
  $defValue = "";
  $defClass = "inp26 opt";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel</label></td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' value='$defValue' list='off' />";
  echo "</td></tr>";


  $inpField = "InpBusiness";
  $inpField1 = "InpBusiness1";
  $inpField2 = "InpBusiness2";
  $inpLabel = "Did this occur at a business?";
  $defValue = "";
  $defClass = "inp26 opt";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel</label></td><td class='r'>";
  echo "<input class='$defClass' type='radio' name='$inpField' id='$inpField1' value='1' list='off' />Yes ";
  echo "<input class='$defClass' type='radio' name='$inpField'  id='$inpField2'  value='0' list='off' />No";
  echo "</td></tr>";

  $inpField = "InpBusComp";
  $inpLabel = "If yes, Company name:";
  $defValue = "";
  $defClass = "inp26 opt";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel</label></td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' value='$defValue' list='off' />";
  echo "</td></tr>";


  $inpField = "InpBusNProfit";
  $inpField1 = "InpBusNProfit1";
  $inpField2 = "InpBusNProfit2";
  $inpLabel = "Nonprofit";
  $defValue = "";
  $defClass = "inp26 opt";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel</label></td><td class='r'>";
  echo "<input class='$defClass' type='radio' name='$inpField' id='$inpField1' value='1' list='off' />Yes ";
  echo "<input class='$defClass' type='radio' name='$inpField' id='$inpField2'  value='0' list='off' />No";
  echo "</td></tr>";
  

  $arrButtonRow[] = "<input class='inp12' type='submit' name='subChgCus' id='subChgCus' value='Save' />";
  $arrButtonRow[] = "<input class='inp12' type='button' name='butCanCus' id='butCanCus' value='Cancel' onclick='fnCloseModalCustomer();' />";

  echo "<tr class='content'><td></td><td>" . implode("", $arrButtonRow) . "</td></tr>";

  echo "</table>";

  echo "</div>"; // content
  echo "</form>";
  echo "</div>"; // body

  echo "<div class='body_stripe'></div>";
  echo "</div>"; // fixed
  echo "</div>"; // modal

  unset($arrButtonRow);
 ?>