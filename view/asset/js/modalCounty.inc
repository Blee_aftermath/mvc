<?php
  echo "<div class='modal modal-close' id='modal-edit-county-form' title='Edit County'>\n";
  echo "<form action='statePRG.php' method='post'>\n";
  echo "<table class='info'>\n";

  $inpField = "inpCntyID";
  $inpLabel = "County ID";
  $defClass = "inp06 reo";
  echo "<tr class='content'>";
  echo "<td><label for='$inpField'>$inpLabel:</label></td>";
  echo "<td class='r'><input class='$defClass' type='text' name='$inpField' id='$inpField' readonly /></td>";
  echo "</tr>\n";

  $inpField = "inpCntyName";
  $inpLabel = "County Name";
  $defClass = "inp16 req";
  echo "<tr class='content'>";
  echo "<td><label for='$inpField'>$inpLabel:</label></td>";
  echo "<td class='r'><input class='$defClass' type='text' name='$inpField' id='$inpField' /></td>";
  echo "</tr>\n";

  $sqlEmp = "SELECT emp_id, emp_is_act, CONCAT(SUBSTR(emp_fname, 1, 1), '. ',emp_lname) as emp_name, role_code FROM emp JOIN emp_role ON emp_id = er_emp JOIN role_list ON er_role_code = role_code WHERE role_code IN ('SR') ORDER BY emp_is_act DESC, emp_fname, emp_lname";
  $resEmp = $mysqli->query($sqlEmp);

  $arrSelEmpLesr = ['ACTIVE' => [], 'INACTIVE' => []];
  while ($res = $resEmp->fetch_assoc()) {
    $label = $res['emp_is_act'] == 1 ? 'ACTIVE' : 'INACTIVE';
    array_push($arrSelEmpLesr[$label], $res);
  }

  $inpField = "selCntyEmpSr";
  $inpLabel = "Sales Rep";
  $defClass = "inp12 req";
  echo "<tr class='content'>";
  echo "<td><label for='$inpField'>$inpLabel:</label></td>";
  echo "<td class='r'>";
  echo "<select class='$defClass' name='$inpField' id='$inpField'>";
  echo "<option value='0'>--- Select ---</option>";
  foreach($arrSelEmpLesr as $label => $arrEmp) {
    if (sizeof($arrEmp)) {
      echo "<optgroup label='$label'>";
      foreach($arrEmp AS $myEmp) {
        echo "<option value='" . $myEmp['emp_id'] . "'";
        if ($myEmp['emp_is_act'] == 0) echo " style='background-color:#ccc;'";
        echo ">" . prepareFrm($myEmp['emp_name']) . "</option>";
      }
    echo "</optgroup>";
    }
  }
  echo "</select>";
  echo "</td>";
  echo "</tr>\n";

  $sqlOffice = "SELECT * FROM office ORDER BY office_id";
  $resOffice = $mysqli->query($sqlOffice);

  $inpField  = "selCntyOffice";
  $inpLabel  = "Office";
  $defValue  = "";
  $defClass  = "req";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<select class='$defClass' name='$inpField' id='$inpField'>";
  echo "<option value='' selected='selected'>--- Select ---</option>";
  while ($myOffice = $resOffice->fetch_assoc()) {
    echo "<option value='" . $myOffice['office_id'] . "'>" . prepareFrm($myOffice['office_name']) . "</option>";
  }
  echo "</select>";
  echo "</td></tr>";

  $sqlDMA = "SELECT * FROM dma ORDER BY dma_name";
  $resDMA = $mysqli->query($sqlDMA);

  $inpField  = "selCntyDMA";
  $inpLabel  = "DMA";
  $defValue  = "";
  $defClass  = "inp24 req";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<select class='$defClass' name='$inpField' id='$inpField'>";
  echo "<option value='' selected='selected'>--- Select ---</option>";
  while ($myDMA = $resDMA->fetch_assoc()) {
    echo "<option value='" . $myDMA['dma_id'] . "'>" . prepareFrm($myDMA['dma_name']) . "</option>";
  }
  echo "</select>";
  echo "</td></tr>";

  $inpField = "inpCntyWMFee";
  $inpLabel = "WM Fee";
  $defClass = "inp08 req";
  echo "<tr class='content'>";
  echo "<td><label for='$inpField'>$inpLabel:</label></td>";
  echo "<td class='r'><input class='$defClass' type='text' name='$inpField' id='$inpField' /></td>";
  echo "</tr>\n";

  $arrButtonRowCnty[] = "<input class='inp12' type='submit' name='subEditCounty' value='Save' />";
  $arrButtonRowCnty[] = "<input class='inp12 modal-close' type='button' value='Cancel' />";
  echo "<tr class='content'><td></td><td>" . implode("", $arrButtonRowCnty) . "</td></tr>";

  echo "</table>\n";
  echo "</form>\n";
  echo "</div>\n";
?>