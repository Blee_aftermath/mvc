/**
 * Scroll to an items in a list
 * @author Scott Kiehn
 */

export class QuickFind {
  /**
   * Creates a new QuickFind class
   */
  constructor(opts = {}) {
    this.dom  = {}
    this.list = []
    this.opts = {
      find:   opts.find   || '#qFfind',
      list:   opts.list   || '#qFlist',
      scroll: opts.scroll || '#qFscroll'
    }

    this.dom.find   = document.querySelector(this.opts.find)
    this.dom.scroll = document.querySelector(this.opts.scroll)
    this.dom.rows   = document.querySelector(this.opts.list)

    this.dom.find.disabled = true

    //Set the scroll location at the top before getting the origin
    this.dom.scroll.scroll(0,0)

    const origin = parseInt(this.dom.scroll.getBoundingClientRect().top) + 24,
          tag = this.dom.rows.firstElementChild

    if (tag) {
      for (const row of this.dom.rows.querySelectorAll(tag.tagName)) {
        this.list.push([
          row.dataset.find.toLowerCase().trim(),
          parseInt(row.dataset.goto),
          parseInt(row.getBoundingClientRect().top) - origin
        ])
      }
    }

    if (location.hash) {
      const goto = location.hash.substring(1),
            position = this.findByInt(goto)

      //Removes the hash in the URL
      history.replaceState(null, document.title, location.pathname)

      if (position) this.dom.scroll.scroll(0, position)
    }

    this.dom.find.addEventListener('keyup', () => {
      const find = this.dom.find.value.toLowerCase().trim()
      if (find.length > 0) {
        const position = this.findByStr(find)
        if (position) this.dom.scroll.scroll(0, position)
      }
    })

    this.dom.scroll.style.scrollBehavior = 'smooth'
    this.dom.find.disabled = false
    this.dom.find.focus()
  }

  /**
   * Find the first occurrrence in an array by text
   * @param {Str} find
   */
  findByStr(find) {
    const findRE = new RegExp(`^${find}`)
    for (const row of this.list) {
      if (row[0].match(findRE)) {
        return row[2]
      }
    }
    return false
  }

  /**
   * Find the first occurrrence in an array by record ID
   * @param {Int} goto
   */
  findByInt(goto) {
    for (const row of this.list) {
      if (row[1] == goto)
        return row[2]
    }
    return false
  }
}
