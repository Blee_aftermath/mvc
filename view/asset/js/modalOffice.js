
var arrOfficeOb   = []; // Array of Office objects
var arrOfficeID   = []; // Array of Office IDs
var arrOfficeEr   = []; // Array of errored fields to tag in red

function fnAddOffice() {
  document.getElementById("spanOfficeHead").innerHTML     = "Add Office";
  document.getElementById("rowOfficeID").hidden           = true;
  document.getElementById("reoOfficeID").disabled         = true;
  document.getElementById("subAddOffice").style.display   = "inline";
  document.getElementById("subChgOffice").style.display   = "none";
  document.getElementById("butConOffice").style.display   = "none";
  document.getElementById("myModalOffice").style.display  = "block";
}

function fnChgOffice(id) {
  jsIndex = arrOfficeID.indexOf(id);
  document.getElementById("spanOfficeHead").innerHTML     = "Edit Office";
  document.getElementById("rowOfficeID").hidden           = false;
  document.getElementById("reoOfficeID").disabled         = false;
  document.getElementById("subAddOffice").style.display   = "none";
  document.getElementById("subChgOffice").style.display   = "inline";
  document.getElementById("butConOffice").style.display   = "inline";
  document.getElementById("myModalOffice").style.display  = "block";
  fnFillOffice(jsIndex);
}

function fnFillOffice(jsIndex) {
  document.getElementById("reoOfficeID").value            = arrOfficeID[jsIndex];
  document.getElementById("inpOfficeName").value          = arrOfficeOb[jsIndex].name;
  document.getElementById("selOfficeRegion").value        = arrOfficeOb[jsIndex].region;
  document.getElementById("inpOfficeAddr1").value         = arrOfficeOb[jsIndex].addr1;
  document.getElementById("inpOfficeAddr2").value         = arrOfficeOb[jsIndex].addr2;
  document.getElementById("inpOfficeCity").value          = arrOfficeOb[jsIndex].city;
  document.getElementById("selOfficeState").value         = arrOfficeOb[jsIndex].state;
  document.getElementById("inpOfficeZip").value           = arrOfficeOb[jsIndex].zip;
  document.getElementById("inpOfficeDLeaseExp").value     = arrOfficeOb[jsIndex].d_lease_exp;
  document.getElementById("selOfficeEmpLESR").value       = arrOfficeOb[jsIndex].emp_lesr;
  document.getElementById("selOfficeStockLevel").value    = arrOfficeOb[jsIndex].stock_level; 
  document.getElementById("inpOfficeCovAdjMi").value      = arrOfficeOb[jsIndex].cov_adj_mi || 0; 
  document.getElementById("inpOfficeLldName").value       = arrOfficeOb[jsIndex].lld_name;
  document.getElementById("inpOfficeLldTelp").value       = arrOfficeOb[jsIndex].lld_telp;
  document.getElementById("inpOfficeLldExtp").value       = arrOfficeOb[jsIndex].lld_extp;    
  document.getElementById("inpOfficeLldTelc").value       = arrOfficeOb[jsIndex].lld_telc;
  document.getElementById("inpOfficeLldExtc").value       = arrOfficeOb[jsIndex].lld_extc;
  document.getElementById("inpOfficeLldEmail").value      = arrOfficeOb[jsIndex].lld_email;  
  document.getElementById("inpOfficeTspTelp").value       = arrOfficeOb[jsIndex].tsp_telp;
  document.getElementById("inpOfficeTspPermit").value     = arrOfficeOb[jsIndex].tsp_permit;
  document.getElementById("inpOfficeBioCompany").value    = arrOfficeOb[jsIndex].bio_company;
  document.getElementById("inpOfficeBioAddr1").value      = arrOfficeOb[jsIndex].bio_addr1;
  document.getElementById("inpOfficeBioAddr2").value      = arrOfficeOb[jsIndex].bio_addr2;
  document.getElementById("inpOfficeBioCity").value       = arrOfficeOb[jsIndex].bio_city;
  document.getElementById("selOfficeBioState").value      = arrOfficeOb[jsIndex].bio_state;
  document.getElementById("inpOfficeBioZip").value        = arrOfficeOb[jsIndex].bio_zip;
  document.getElementById("inpOfficeBioTelp").value       = arrOfficeOb[jsIndex].bio_telp;
  document.getElementById("inpOfficeBioPermit").value     = arrOfficeOb[jsIndex].bio_permit;
  document.getElementById("inpOfficeInfo").value          = arrOfficeOb[jsIndex].info;
  document.getElementById("inpOfficeGoogle").value        = arrOfficeOb[jsIndex].google;
  document.getElementById("chkOfficeIsAct").checked       = (arrOfficeOb[jsIndex].is_act == 1 ? true : false);
  document.getElementById("chkOfficeIsField").checked     = (arrOfficeOb[jsIndex].is_field == 1 ? true : false);
  document.getElementById("chkOfficeIsEqs").checked       = (arrOfficeOb[jsIndex].is_eqs == 1 ? true : false);
  document.getElementById("chkOfficeHasHydroxyl").checked = (arrOfficeOb[jsIndex].has_hydroxyl == 1 ? true : false);
}

function fnConfirmDelOffice() {
  strConfirm  = "<div class='content alert'>";
  strConfirm += "Delete this Office?&nbsp;&nbsp;";
  strConfirm += "<input class='inp08' type='submit' name='subDelOfficeY' value='Yes' />";
  strConfirm += "<input class='inp08' type='button' name='butDelOfficeN' value='No' onclick='fnDelOfficeN()' />";
  strConfirm += "</div>";
  document.getElementById("spanOfficeConfirm").innerHTML = strConfirm;
}

function fnDelOfficeN() {
  document.getElementById("spanOfficeConfirm").innerHTML = "";
}

function fnCloseModalOffice() {
  document.getElementById("reoOfficeID").value            = "";
  document.getElementById("inpOfficeName").value          = "";
  document.getElementById("selOfficeRegion").value        = "";
  document.getElementById("inpOfficeAddr1").value         = "";
  document.getElementById("inpOfficeAddr2").value         = "";
  document.getElementById("inpOfficeCity").value          = "";
  document.getElementById("selOfficeState").value         = "";
  document.getElementById("inpOfficeZip").value           = "";
  document.getElementById("inpOfficeDLeaseExp").value     = "";
  document.getElementById("selOfficeStockLevel").value    = "";
  document.getElementById("inpOfficeCovAdjMi").value      = ""; 
  document.getElementById("selOfficeEmpLESR").value       = "";  
  document.getElementById("inpOfficeLldName").value       = "";
  document.getElementById("inpOfficeLldTelp").value       = "";
  document.getElementById("inpOfficeLldExtp").value       = "";
  document.getElementById("inpOfficeLldTelc").value       = "";
  document.getElementById("inpOfficeLldExtc").value       = "";
  document.getElementById("inpOfficeLldEmail").value      = "";  
  document.getElementById("inpOfficeTspTelp").value       = "";
  document.getElementById("inpOfficeTspPermit").value     = "";
  document.getElementById("inpOfficeBioCompany").value    = "";
  document.getElementById("inpOfficeBioAddr1").value      = "";
  document.getElementById("inpOfficeBioAddr2").value      = "";
  document.getElementById("inpOfficeBioCity").value       = "";
  document.getElementById("selOfficeBioState").value      = "";
  document.getElementById("inpOfficeZip").value           = "";
  document.getElementById("inpOfficeBioTelp").value       = "";
  document.getElementById("inpOfficeBioPermit").value     = "";
  document.getElementById("inpOfficeInfo").value          = "";
  document.getElementById("inpOfficeGoogle").value        = "";
  document.getElementById("chkOfficeIsAct").checked       = false;
  document.getElementById("chkOfficeIsField").checked     = false;
  document.getElementById("chkOfficeIsEqs").checked       = false;
  document.getElementById("chkOfficeHasHydroxyl").checked = false;
  document.getElementById("myModalOffice").style.display  = "none";
  for(var i=0; i<arrOfficeEr.length; i++) {
    document.getElementById(arrOfficeEr[i]).classList.remove("err");
  }
  delete(arrOfficeEr);
  document.getElementById("spanOfficeMessages").innerHTML = "";
}
