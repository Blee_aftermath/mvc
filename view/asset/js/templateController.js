const root = 'template/'

/**
 * Dynamically loads/stores HTML templates for use in blocks, modals ect.
 * @deprecated This method of obtaining a template is slow, read templates to globals from php
 * @author Jake Cirino
 */
export class TemplateController {
  /**
   * Creates a new TemplateController
   * @deprecated This method of obtaining a template is slow, read templates to globals from php
   */
  constructor() {
    this.templates = {}
  }

  /**
   * Gets an html template. This function is async for use within another async function.
   * @deprecated This method of obtaining a template is slow, read templates to globals from php
   * @param {String} template The template to get, excluding file extension and '/template/'
   */
  async getTemplate(template) {
    return new Promise((resolve) => {
      if (this.templates[template] === undefined) {
        //load template from remote
        fetch(`${root + template}.html`, {
          headers: {
            'pragma': 'no-cache',
            'cache-control': 'no-cache'
          }
        })
          .then((res) => res.text())
          .then((html) => {
            //minify html
            html = html.replace(/\n|\t/g, '')
            html = html.replace(/>[\s]*</g, '><')

            //set and return html
            this.templates[template] = html
            resolve(html)
          })
      } else resolve(this.templates[template])
    })
  }
}
