<?php
  //delete modal
  echo "<div class='modal modal-close' id='modal-cus-delete-form' title='Delete a Jail Cell Customer'>\n";
  echo "<form action='partnerCusPRG.php' method='post'>\n";
  echo "<input type='hidden' name='action' />\n";

  $inpField = "cus_id";
  echo "<input type='hidden' name='$inpField' />";

  echo "<table class='info'>\n";

  echo "<tr class='content'>";
  echo "<td>Click Yes to confirm the customer deletion.</td>";
  echo "</tr>\n";

  $arrButtonRowCnty   = [];
  $arrButtonRowCnty[] = "<input class='inp12' type='submit' value='Yes' />";
  $arrButtonRowCnty[] = "<input class='inp12 modal-close' type='button' value='No' />";
  echo "<tr class='content'><td style='padding-top: 8px;'>" . implode("", $arrButtonRowCnty) . "</td></tr>";

  echo "</table>\n";
  echo "</form>\n";
  echo "</div>\n";

  //update & insert modal
  echo "<div class='modal modal-close' id='modal-cus-save-form' title='$subTitle'>\n";
  echo "<form action='partnerCusPRG.php' method='post' novalidate='novalidate'>\n";
  echo "<input type='hidden' name='action' />\n";

  $inpField = "cus_id";
  echo "<input type='hidden' name='$inpField' />";

  $inpField = "cus_div";
  echo "<input type='hidden' name='$inpField' />";

  echo "<table class='info'>\n";

  $inpField = "cus_name";
  $inpLabel = "Customer";
  $defClass = "inp24 req";
  echo "<tr class='content'>";
  echo "<td><label for='$inpField'>$inpLabel:</label></td>";
  echo "<td class='r'><input class='$defClass' type='text' name='$inpField' id='$inpField' /></td>";
  echo "</tr>\n";

  echo "<tr class='content'><th colspan='2' style='text-decoration: underline;'>Billing</th></tr>";

  $inpField = "cus_bill_title";
  $inpLabel = "Title";
  $defClass = "inp12 opt";
  echo "<tr class='content'>";
  echo "<td><label for='$inpField'>$inpLabel:</label></td>";
  echo "<td class='r'><input class='$defClass' type='text' name='$inpField' id='$inpField' /></td>";
  echo "</tr>\n";

  $inpLabel = "Name";
  $defClass = "inp12 opt";
  echo "<tr class='content'>";
  echo "<td><label for='$inpField'>$inpLabel:</label></td>";
  echo "<td class='r'>";
  $inpField = "cus_bill_fname";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' />";
  $inpField = "cus_bill_lname";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' />";
  echo "</td>";
  echo "</tr>\n";

  $inpField = "cus_bill_dept";
  $inpLabel = "Department";
  $defClass = "inp24 opt";
  echo "<tr class='content'>";
  echo "<td><label for='$inpField'>$inpLabel:</label></td>";
  echo "<td class='r'><input class='$defClass' type='text' name='$inpField' id='$inpField' /></td>";
  echo "</tr>\n";

  $inpField = "cus_bill_addr1";
  $inpLabel = "Address 1";
  $defClass = "inp24 opt";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' maxlength='$maxAddr' list='off' />";
  echo "</td></tr>";

  $inpField = "cus_bill_addr2";
  $inpLabel = "Address 2";
  $defClass = "inp24 opt";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' maxlength='$maxAddr' list='off' />";
  echo "</td></tr>";

  $inpField = "cus_bill_city";
  $inpLabel = "City";
  $defClass = "inp24 opt";
  echo "<tr class='content'>";
  echo "<td><label for='$inpField'>$inpLabel:</label></td>";
  echo "<td class='r'><input class='$defClass' type='text' name='$inpField' id='$inpField' /></td>";
  echo "</tr>\n";

  $sqlState = "SELECT * FROM state ORDER BY st_name";
  $resState = $mysqli->query($sqlState) or die('Get state failed');

  $inpField = "cus_bill_state";
  $inpLabel = "State/Zip";
  $defClass = "inp16 opt";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<select class='$defClass' name='$inpField' id='$inpField'>";
  echo "<option value='' selected='selected'>--- Select ---</option>";
  while ($myState = $resState->fetch_assoc()) {
    echo "<option value='" . prepareFrm($myState['st_id']) . "'>" . prepareFrm($myState['st_name']) . "</option>";
  }
  echo "</select>";

  $inpField = "cus_bill_zip";
  $defClass = "inp06 opt";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' maxlength='$maxZip' />";
  echo "</td></tr>";

  $inpField = "cus_bill_telp";
  $inpLabel = "Phone";
  $defClass = "opt inp12";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' maxlength='$maxTel' />";
  $inpField = "cus_bill_extp";
  $defClass = "opt inp08";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' maxlength='$maxExt' />";
  echo "</td></tr>";

  $inpField = "cus_bill_email";
  $inpLabel = "Email";
  $defClass = "inp24 opt";
  echo "<tr class='content'>";
  echo "<td><label for='$inpField'>$inpLabel:</label></td>";
  echo "<td class='r'><input class='$defClass' type='text' name='$inpField' id='$inpField' /></td>";
  echo "</tr>\n";

  echo "<tr class='content'><th colspan='2' style='text-decoration: underline;'>Service <img id='copy-billing-info' class='icon16' src='icons/copy16.png' alt='Copy' title='Copy Billing Address' /></th></tr>";

  $inpField = "cus_serv_title";
  $inpLabel = "Title";
  $defClass = "inp12 opt";
  echo "<tr class='content'>";
  echo "<td><label for='$inpField'>$inpLabel:</label></td>";
  echo "<td class='r'><input class='$defClass' type='text' name='$inpField' id='$inpField' /></td>";
  echo "</tr>\n";

  $inpLabel = "Name";
  $defClass = "inp12 opt";
  echo "<tr class='content'>";
  echo "<td><label for='$inpField'>$inpLabel:</label></td>";
  echo "<td class='r'>";
  $inpField = "cus_serv_fname";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' />";
  $inpField = "cus_serv_lname";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' />";
  echo "</td>";
  echo "</tr>\n";

  $inpField = "cus_serv_dept";
  $inpLabel = "Department";
  $defClass = "inp24 opt";
  echo "<tr class='content'>";
  echo "<td><label for='$inpField'>$inpLabel:</label></td>";
  echo "<td class='r'><input class='$defClass' type='text' name='$inpField' id='$inpField' /></td>";
  echo "</tr>\n";

  $inpField = "cus_serv_addr1";
  $inpLabel = "Address 1";
  $defClass = "inp24 opt";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' maxlength='$maxAddr' list='off' />";
  echo "</td></tr>";

  $inpField = "cus_serv_addr2";
  $inpLabel = "Address 2";
  $defClass = "inp24 opt";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' maxlength='$maxAddr' list='off' />";
  echo "</td></tr>";

  $inpField = "cus_serv_city";
  $inpLabel = "City";
  $defClass = "inp24 req";
  echo "<tr class='content'>";
  echo "<td><label for='$inpField'>$inpLabel:</label></td>";
  echo "<td class='r'><input class='$defClass' type='text' name='$inpField' id='$inpField' /></td>";
  echo "</tr>\n";

  $sqlState = "SELECT * FROM state ORDER BY st_name";
  $resState = $mysqli->query($sqlState) or die('Get state failed');

  $inpField = "cus_serv_state";
  $inpLabel = "State/Zip";
  $defClass = "inp16 req";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<select class='$defClass' name='$inpField' id='$inpField'>";
  echo "<option value='' selected='selected'>--- Select ---</option>";
  while ($myState = $resState->fetch_assoc()) {
    echo "<option value='" . prepareFrm($myState['st_id']) . "'>" . prepareFrm($myState['st_name']) . "</option>";
  }
  echo "</select>";

  $inpField = "cus_serv_zip";
  $defClass = "inp06 req";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' maxlength='$maxZip' />";
  echo "</td></tr>";

  $inpField = "cus_serv_telp";
  $inpLabel = "Phone";
  $defClass = "opt inp12";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' maxlength='$maxTel' />";
  $inpField = "cus_serv_extp";
  $defClass = "opt inp08";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' maxlength='$maxExt' />";
  echo "</td></tr>";

  $inpField = "cus_serv_email";
  $inpLabel = "Email";
  $defClass = "inp24 opt";
  echo "<tr class='content'>";
  echo "<td><label for='$inpField'>$inpLabel:</label></td>";
  echo "<td class='r'><input class='$defClass' type='text' name='$inpField' id='$inpField' /></td>";
  echo "</tr>\n";

  $inpField = "cus_info";
  $inpLabel = "Notes";
  $defClass = "opt";
  echo "<tr class='content'><td class='l'>Additional<br />Information:</td>";
  echo "<td class='r'><textarea class='$defClass' name='$inpField' rows='4' cols='48'></textarea>";
  echo "</td></tr>";

  $arrButtonRowCnty   = [];
  $arrButtonRowCnty[] = "<button id='modal-save-button' class='inp12' type='button'>Save</button>";
  $arrButtonRowCnty[] = "<input class='inp12 modal-close' type='button' value='Cancel' />";
  echo "<tr class='content'><td style='padding-top: 8px;'></td><td style='padding-top: 8px;'>" . implode("", $arrButtonRowCnty) . "</td></tr>";

  echo "</table>\n";
  echo "</form>\n";
  echo "</div>\n";
?>