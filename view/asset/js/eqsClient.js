import { default as formModal } from './modules/ui/formModal.js'

/**
 * Class for EQS Clients admin page
 * @author Scott Kiehn
 */

export class EQSClient {
  /**
   * Creates a new EQSClient class
   */
  constructor(opts = {}) {
    this.dom  = {}

    this.dom.cus      = document.querySelector('#customer')
    this.dom.expDP    = document.querySelector('#cus_d_cont_exp')
    this.dom.saveMdl  = document.querySelector('#modal-cus-save-form')
    this.dom.saveBtn  = document.querySelector('#modal-save-button')
    this.dom.copyInfo = document.querySelector('#copy-billing-info')

    this.dom.cus.addEventListener('click', (ev) => {
      if (ev.target.matches('[data-opts]'))
        formModal(ev.target)
    })

    jQuery(this.dom.expDP).datepicker();

    this.dom.saveBtn.addEventListener('click', (ev) => this.saveCustomer(ev.target.closest('form')))

    this.dom.copyInfo.addEventListener('click', (ev) => this.copyInfo())
  }

  /**
   * Copy billing info to service info
   * @param {Element} form
   */
  copyInfo = async (form) => {
    for (const inp1 of this.dom.saveMdl.querySelectorAll('[name]')) {
      if (inp1.name.match(/_bill_/)) {
        const inp2 = inp1.name.replace(/_bill_/, '_serv_')
        this.dom.saveMdl.querySelector(`[name=${inp2}]`).value = inp1.value
      }
    }
  }

  /**
   * Save jail cell customer information
   * @param {Element} form
   */
  saveCustomer = async (form) => {

    const inpData = {}

    let errors = 0

    for (const inp of form.querySelectorAll('[name]')) {
      inpData[inp.name] = inp.value
      if (inp.classList.contains('req')) if (inp.value.length == 0) errors++
    }

    if (errors) return this.dom.saveMdl.querySelector('.error').innerHTML = `<p>Values for the required items were not provided.</p>`;

    const post = await fetch('eqsClientPRG.php', {
      method: 'post',
      headers: { 'Content-Type': 'application/json;charset=utf-8' },
      body: JSON.stringify(inpData)
    })

    const res = await post.json()

    if ( res.ok ) {
      this.dom.saveMdl.querySelector('.error').innerHTML = '';
      this.dom.saveMdl.querySelector('.message').innerHTML = `<p>${res.msg}</p>`;
      setTimeout(() => {
        location.assign(res.ref)
        if (res.ref.match(/#/)) location.reload()
      }, 750)
    } else {
      const error = []

      if (typeof(res.msg) == 'object') {
        for (const m of res.msg) error.push(m.msg)
      } else {
        error[0] = res.msg
      }

      this.dom.saveMdl.querySelector('.error').innerHTML = `<p>${error.join('<br />')}</p>`;
    }
  }
}
