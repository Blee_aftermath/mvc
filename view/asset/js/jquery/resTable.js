/**
 * A table showing a list of a restoration jobs
 * @author Jake Cirino
 */
import {RestorationTableItem} from "./resTableItem.js"

export class RestorationTable{
  /**
   * Creates a new restoration table
   * @param {Object} data
   */
  constructor(data) {
    this.data = data
    this.items = []
    
    this.baseElement = document.getElementById('resTableBody')
    
    //add items
    for (const dataKey in this.data) {
      const item = this.data[dataKey];
      this.addItem(item)
    }
  }
  
  /**
   * Adds an item to the table
   * @param {Object} data
   */
  addItem(data){
    let item = new RestorationTableItem(this, data)
    this.items.push(item)
  }
}