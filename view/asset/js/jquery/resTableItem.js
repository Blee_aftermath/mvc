import {Util} from "../util.js"

/**
 * An item on the restoration jobs table
 * @see scripts/restoration/resTable.js
 * @author Jake Cirino
 */
export class RestorationTableItem{
  constructor(parent, data) {
    this.parent = parent
    this.data = data
    console.log(data)
    
    this.baseElement = document.createElement('tr')
    this.baseElement.innerHTML = window.itemTemplate
    
    this.dom = Util.parseDOM(this.baseElement)
    this.populate()
    
    //append to parent
    this.parent.baseElement.appendChild(this.baseElement)
  }
  
  /**
   * Populate the row from the data provided
   */
  populate(){
    this.dom.job_yrnum.innerText = this.data.job_yrnum
    this.dom.job_yrnum.href = `/jif.php?job=${this.data.job_id}&phase=mgt&tab=restoration`
    
    this.dom.rp_name.innerText = this.data.rp_name == null ? 'None' : this.data.rp_name
    
    this.dom.rj_status.innerText = this.data.job_status_code
    if(this.data.rjr_code != undefined){
      this.dom.rjr_code.innerText = this.data.rjr_code
      this.dom.rjr_code.title = this.data.rjr_dsca
    }
    
    if(this.data.rj_dt_add != null)
      this.dom.rj_dt_add.innerText = this.data.rj_dt_add.split(" ")[0]
    if(this.data.rj_dt_appt != null)
      this.dom.rj_dt_appt.innerText = this.data.rj_dt_appt.split(" ")[0]
    if(this.data.rj_dt_sign != null)
      this.dom.rj_dt_sign.innerText = this.data.rj_dt_sign.split(" ")[0]
    if(this.data.rj_dt_comp != null)
      this.dom.rj_dt_comp.innerText = this.data.rj_dt_comp.split(" ")[0]
    
    this.dom.rj_amt_est.innerText = Util.formatCurrency(this.data.rj_amt_est)
    this.dom.rj_amt_act.innerText = Util.formatCurrency(this.data.rj_amt_act)
  }
}