/**
 * A progress/stage type bar that tracks the status of completion of a multi-step task
 * @author Jake Cirino
 */
export class StageBar {
  /**
   * Creates a new blank stage bar
   * @param {Array} stages An array of the names of each stage, in order
   */
  constructor(stages) {
    this.stages = []

    //create base element
    this.baseElement = document.createElement('div')
    this.baseElement.className = 'stage-bar'

    //add stages
    for (const key in stages) {
      if (Object.hasOwnProperty.call(stages, key)) {
        const stageName = stages[key]

        this.addStage(stageName)
      }
    }

    //default the stage to 0
    this.setStage(0)
  }

  /**
   * Adds a stage to the lists of stages and generates its associated HTML
   * @param {String} stageName
   */
  addStage(stageName) {
    //create stage data for our stage list
    let stageData = {
      name: stageName,
    }

    //build the base html element for the stage
    stageData.baseElement = document.createElement('div')

    //create title element
    stageData.titleElement = document.createElement('div')
    stageData.titleElement.innerText = stageName
    stageData.baseElement.appendChild(stageData.titleElement)

    //create block element
    stageData.baseElement.appendChild(document.createElement('div'))

    //append stage HTML to the base html element
    this.baseElement.appendChild(stageData.baseElement)

    //append stage data to our stage list
    this.stages.push(stageData)
  }

  /**
   * Sets the current stage status
   * @param {Number|String} stage The number index of the stage or the stage name
   */
  setStage(stage){
    if(!isNaN(stage)){
      //set the stage by number
      this.currentStage = Number(stage)
    }else{
      //set the stage by stage name
      for (const key in this.stages) {
        if (Object.hasOwnProperty.call(this.stages, key)) {
          const stageData = this.stages[key];
          
          if(stageData.name == stage){
            this.currentStage = Number(key)
            break
          }
        }
      }
    }

    //update html elements
    for (const index in this.stages) {
      if (Object.hasOwnProperty.call(this.stages, index)) {
        const element = this.stages[index];
        const baseElement = element.baseElement

        if(index < this.currentStage) baseElement.className = 'stage-completed'
        else if(index == this.currentStage) baseElement.className = 'stage-current'
        else baseElement.className = ''
      }
    }
  }

  /**
   * Sets the current stage to the next stage
   */
  nextStage(){
    this.setStage(this.currentStage + 1)
  }

  /**
   * Sets the current stage to the previous stage
   */
  previousStage(){
    this.setStage(this.currentStage - 1)
  }

  /**
   * Returns the html representing this stage bar. The returned element can be
   *  can be appended wherever you want the stagebar to show up.
   */
  getHTML(){
    return this.baseElement
  }
}
