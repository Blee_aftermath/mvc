// ==================================================================================================== //
// === GLOBAL === //
// ==================================================================================================== //

var xmlhttp
var errmsg = 'Your browser does not support XMLHTTP.'

var btn = {}

var btnSelectedSup = 'btnSupTem'
var btnSelectedTec = 'btnTecTem'

//var formerSupEmp   = 0
var formerTecEmp   = 0

//Set the default type of job the crew assignement handles as "job"
//var jifCrewWorkType = 'Job'

//Carry out the following once the DOM is loaded
document.addEventListener('DOMContentLoaded', () => {

  const blockCrew = document.getElementById('blockJifCrew')

  if (blockCrew) {

    //Change the default crew view to office if not a "job"
    if (blockCrew.dataset.worktype != 'Job') {

      btnSelectedTec = 'btnTecOff'

      document.getElementById('btnTecTem').style.display = 'none'

      if (blockCrew.dataset.worktype == 'JC')
        document.getElementById('btnTecFor').style.display = 'inline-block'

      for (const el of blockCrew.querySelectorAll('[data-crew="sup"]')) {
        el.disabled = true

      }
    }

    //Set the crew view hilite, now set in JS not PHP
    document.getElementById(btnSelectedSup).classList.add('hlyel')
    document.getElementById(btnSelectedTec).classList.add('hlyel')
  }
});

const crewChangeEvent = new CustomEvent('crewChangeEvent') //an event to dispatch on crew change

function GetXmlHttpObject() {
  if (window.XMLHttpRequest) {
    return new XMLHttpRequest()
  } // code for IE7+, Firefox, Chrome, Opera, Safari
  if (window.ActiveXObject) {
    return new ActiveXObject('Microsoft.XMLHTTP')
  } // code for IE6, IE5
  return null
}

// ==================================================================================================== //
// === ASSIGN CREW TO JOB === //
// ==================================================================================================== //

function ajaxJobEmp(strAction, strType, strJob) {
  //  alert("strAction="+strAction+" strType="+strType+" strJob="+strJob+" strEmp="+strEmp);

  if (strType == 1) spanToUpd = 'spanCrewSup'
  if (strType == 2) spanToUpd = 'spanCrewTec'

  let strEmp = ''
  if (strAction == 'addEmp') {
    if (strType == 1) {
      strEmp = document.querySelector("[name='selSupAvl']").value
    } else {
      strEmp = document.querySelector("[name='selTecAvl']").value
    }
  } else {
    if (strType == 1) {
      strEmp = document.querySelector("[name='selSupAsn']").value
    } else {
      strEmp = document.querySelector("[name='selTecAsn']").value
    }
  }

  if (!strEmp.length) return false

  var url = `includes/ajaxJif.php?action=${strAction}&job=${strJob}&type=${strType}&emp=${strEmp}&sid=${Math.random()}`

  //  alert(url);

  xmlhttpst = GetXmlHttpObject()

  if (xmlhttpst == null) {
    alert(errmsg)
    return
  }

  xmlhttpst.onreadystatechange = function () {
    if (xmlhttpst.readyState == 4) {
      //send update to jif controller
      if (window.jifController !== undefined) window.jifController.update()

      document.getElementById(spanToUpd).innerHTML = xmlhttpst.responseText

      document.dispatchEvent(crewChangeEvent) //dispatch the crew change event
    }
  }
  xmlhttpst.open('GET', url, true) // METHOD, URL, ASYNCHRONOUSLY=TRUE (CONTINUE EXECUTION AFTER SENDING)
  xmlhttpst.send(null) // SENDS THE REQUEST TO THE SERVER
}

// ==================================================================================================== //
// === TEAM OFFICE REGION ALL BUTTONS === //
// ==================================================================================================== //


function filterBtnEmp(forBtn, strAction) {

  forBtn.classList.toggle('hlyel');

  //if (strAction == 'forSub') {
  //  formerSupEmp = forBtn.classList.contains('hlyel') ? 1 : 0 
  //  document.getElementById(btnSelectedSup).click()
  //}

  if (strAction == 'forTec') {
    formerTecEmp = forBtn.classList.contains('hlyel') ? 1 : 0 
    document.getElementById(btnSelectedTec).click()
  }
}

function ajaxBtnEmp(strAction, strType, strValue, id) {
  //console.log(`strAction=${strAction} strType=${strType} value=${strValue}`);

  var formerEmp = 0

  if (strType == 1) {
    spanToUpd = 'spanSelSup'
    document.getElementById(btnSelectedSup).classList.remove('hlyel')
    btnSelectedSup = id
    //formerEmp = formerSupEmp ? 1 : 0
    formerEmp = 0
  }
  if (strType == 2) {
    spanToUpd = 'spanSelTec'
    document.getElementById(btnSelectedTec).classList.remove('hlyel')
    btnSelectedTec = id
    formerEmp = formerTecEmp ? 1 : 0
  }
  document.getElementById(id).classList.add('hlyel')

  var url = `includes/ajaxJif.php?action=${strAction}&type=${strType}&valID=${strValue}&formerEmp=${formerEmp}&sid=${Math.random()}`

  //console.log(url);

  xmlhttpst = GetXmlHttpObject()

  if (xmlhttpst == null) {
    alert(errmsg)
    return
  }

  xmlhttpst.onreadystatechange = function () {
    if (xmlhttpst.readyState == 4) {
      document.getElementById(spanToUpd).innerHTML = xmlhttpst.responseText
    }
  }
  xmlhttpst.open('GET', url, true) // METHOD, URL, ASYNCHRONOUSLY=TRUE (CONTINUE EXECUTION AFTER SENDING)
  xmlhttpst.send(null) // SENDS THE REQUEST TO THE SERVER
}
