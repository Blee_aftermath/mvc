export const ModalType = {
  INFO: 'info',
  ERROR: 'error'
}

/**
 * A 'floating' modal that can show info, errors, ect. above a specified element
 * @author Jake Cirino
 */
export class FloatingModal{
  /**
   * Creates a new floating modal
   * @param {HTMLElement} attach HTML element to attach the floating modal to
   * @param {String} text 
   * @param {String} type
   */
  constructor(text, type = ModalType.INFO){
    this.text = text
    this.type = type

    //create base div
    this.baseElement = document.createElement('div')
    this.baseElement.className = `floating-modal ${this.type}`
    this.baseElement.innerText = text

    this.baseElement.onanimationend = () => this.delete()

    //append base element to doc
    document.body.appendChild(this.baseElement)
  }

  /**
   * deletes this item
   */
  delete(){
    this.baseElement.remove()
  }
}