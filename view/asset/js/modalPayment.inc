<?php
  echo "<script type='module'>\n";
  echo "  import {default as formModal} from './scripts/modules/ui/formModal.js';\n";
  echo "  window.jsClkEvt['formModal'] = formModal;\n";

  echo "  const modal = document.getElementById('editPaymentInfo'),\n";
  echo "    form = document.getElementById('editPaymentInfoForm'),\n";
  echo "    selInsPay = form.querySelector('[name=selPayByInsNon]'),\n";
  echo "    inpIns = form.querySelector('[name=inpInsuredName]'),\n";
  echo "    inpPay = form.querySelector('[name=inpPayerName]');\n";

  echo "  modal.addEventListener('modalOpen', ev => setClass(selInsPay));\n";
  echo "  selInsPay.addEventListener('change', ev => setClass(ev.target));\n";

  echo "  const setClass = el => {\n";
  echo "    const val = el.value;\n";

  echo "    inpIns.classList.remove('req');\n";
  echo "    if ( val == 1 ) inpIns.classList.add('req');\n";

  echo "    inpPay.classList.remove('req');\n";
  echo "    if ( val == 2 ) inpPay.classList.add('req');\n";

  echo "  }\n";
  echo "</script>\n";

  echo "<div class='modal modal-close' id='editPaymentInfo' title='Edit Payment Information'>\n";

  echo "<form id='editPaymentInfoForm' action='jifPRG.php' method='post'>\n";

  $inpField = "editPaymentInfo";
  echo "<input type='hidden' name='$inpField' />\n";

  $inpField = "hidJobID";
  echo "<input type='hidden' name='$inpField' />\n";


  echo "<table class='info'>\n";

  $inpField = "selPayByInsNon";
  $inpLabel = "Job is to be paid for by";
  $defClass = "req";
  echo "<tr class='content'><td class='l'><label for='$inpField'>$inpLabel:</label></td>";
  echo "<td class='r'><select class='$defClass' id='$inpField' name='$inpField'>";
  echo "<option value='' selected='selected'></option>";
  echo "<option value='2'>Non-Insurance</option>";
  echo "<option value='1'>Insurance</option>";
  echo "</select>";
  echo "</td></tr>";

  echo "<tr><th colspan='2'>Non-Insurance</th></tr>\n";

  $inpField = "inpPayerName";
  $inpLabel = "Payer Name";
  $defClass = "opt inp20";
  echo "<tr class='content'><td class='l'><label for='$inpField'>$inpLabel:</label></td>";
  echo "<td class='r'><input id='$inpField' class='$defClass' type='text' name='$inpField' maxlength='$maxName' autocomplete='off' />";
  echo "</td></tr>";

  echo "<tr><th colspan='2'>Insurance</th></tr>\n";

  $sqlSelInsCo = "SELECT * FROM insco JOIN insco_group ON insco_group = insco_group_id ORDER BY insco_name";
  $resSelInsCo = $mysqli->query($sqlSelInsCo);
  $inpField = "selInsCo";
  $inpLabel = "Carrier";
  $defClass = "opt inp20";
  echo "<tr class='content'><td class='l'><label for='$inpField'>$inpLabel:</label></td>";
  echo "<td class='r'><select class='$defClass' id='$inpField' name='$inpField'>";
  echo "<option value='' selected='selected'>--- Select Carrier ---</option>";
  while ($mySelInsCo = $resSelInsCo->fetch_assoc()) {
    echo "<option style='background-color:#" . $mySelInsCo['insco_group_color2'] . "' value='" . $mySelInsCo['insco_id'] . "'>" . prepareWeb($mySelInsCo['insco_name']) . "</option>";
  }
  echo "</select>";
  echo "</td></tr>";

  $inpField = "inpInsuredName";
  $inpLabel = "Insured Name";
  $defClass = "opt inp20";
  echo "<tr class='content'><td class='l'><label for='$inpField'>$inpLabel:</label></td>";
  echo "<td class='r'><input id='$inpField' class='$defClass' type='text' name='$inpField' maxlength='$maxName' autocomplete='off' />";
  echo "</td></tr>";

  $inpField = "inpDInsuredDOB";
  $inpLabel = "Insured DOB";
  $defClass = "inp08 opt";
  echo "<tr class='content'><td class='l'><label for='$inpField'>$inpLabel:</label></td>";
  echo "<td class='r'><input id='$inpField' class='$defClass' type='text' name='$inpField' maxlength='$maxDate' autocomplete='off' />";
  echo "</td></tr>";
  echo "<script>$(function() { $('#".  $inpField . "').datepicker({changeMonth: true,changeYear: true, yearRange: '" . ($yrToday - 100) . ":" . $yrToday . "' }); }); </script>";

  $inpField = "inpPolicyNum";
  $inpLabel = "Policy";
  $defClass = "inp12 opt";
  echo "<tr class='content'><td class='l'><label for='$inpField'>$inpLabel:</label></td></td>";
  echo "<td class='r'><input id='$inpField' class='$defClass' type='text' name='$inpField' maxlength='$maxName' autocomplete='off' />";
  echo "</td></tr>";

  $inpField = "inpClaimNum";
  $inpLabel = "Claim";
  $defClass = "inp12 opt";
  echo "<tr class='content'><td class='l'><label for='$inpField'>$inpLabel:</label></td>";
  echo "<td class='r'><input id='$inpField' class='$defClass' type='text' name='$inpField' maxlength='$maxName' autocomplete='off' />";
  echo "</td></tr>";

  $inpField = "inpDeductible";
  $inpLabel = "Deductible";
  $defClass = "inp08 opt";
  echo "<tr class='content'><td class='l'><label for='$inpField'>$inpLabel:</label><span style='float: right;'>$</span></td>";
  echo "<td class='r'><input id='$inpField' class='$defClass' type='text' name='$inpField' maxlength='$maxAmt' />";
  echo "</td></tr>";

  $inpField = "inpAgtName";
  $inpLabel = "Agent's Name";
  $defClass = "inp20 opt";
  echo "<tr class='content'><td>$inpLabel:</td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' maxlength='$maxName' />";
  echo "</td></tr>";

  $inpLabel = "Agent's Phone:";
  $inpField = "inpAgtTelp";
  $defClass = "inp12 opt";
  echo "<tr class='content'><td class='l'>$inpLabel</td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' maxlength='$maxTel' />";
  $inpField = "inpAgtTelpx";
  $defClass = "inp08 opt";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' maxlength='$maxExt' />";
  echo "</td></tr>";

  $inpField = "inpAdjName";
  $inpLabel = "Adjustor's Name";
  $defClass = "inp20 opt";
  echo "<tr class='content'><td>$inpLabel:</td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' maxlength='$maxName' />";
  echo "</td></tr>";

  $inpLabel = "Adjustor's Phone:";
  $inpField = "inpAdjTelp";
  $defClass = "inp12 opt";
  echo "<tr class='content'><td class='l'>$inpLabel</td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' maxlength='$maxTel' />";
  $inpField = "inpAdjTelpx";
  $defClass = "inp08 opt";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' maxlength='$maxExt' />";
  echo "</td></tr>";

  $arrButtonRowState[] = "<input class='inp08' type='submit' value='Save' />";
  $arrButtonRowState[] = "<input class='inp08 modal-close' type='button' value='Cancel' />";
  echo "<tr class='content'><td>&nbsp;</td>";
  echo "<td class='r'><br />" . implode("", $arrButtonRowState) . "</td></tr>";

  echo "</table>\n";

  echo "</form>\n";

  echo "</div>\n";
?>