var arrRtlOb   = []; // Array of Retail objects
var arrRtlID   = []; // Array of Retail IDs
var arrRtlEr   = []; // Array of errored fields to tag in red

function fnAddRtl() {
  document.getElementById("spanRtlHead").innerHTML     = "Add Retail";
  document.getElementById("hidRtlID").disabled         = true;
  document.getElementById("tblRtl").style.display      = "block";
  document.getElementById("subAddRtl").style.display   = "inline";
  document.getElementById("subChgRtl").style.display   = "none";
  document.getElementById("butConRtl").style.display   = "none";
  document.getElementById("myModalRtl").style.display  = "block";
}

function fnChgRtl(id) {
  rtlIndex = arrRtlID.indexOf(id);
  document.getElementById("spanRtlHead").innerHTML     = "Edit Retail";
  document.getElementById("hidRtlID").disabled         = false;
  document.getElementById("tblRtl").style.display      = "block";
  document.getElementById("subAddRtl").style.display   = "none";
  document.getElementById("subChgRtl").style.display   = "inline";
  document.getElementById("butConRtl").style.display   = "inline";
  document.getElementById("myModalRtl").style.display  = "block";
  fnFillRtl(rtlIndex);
}

function fnFillRtl(rtlIndex) {
  document.getElementById("hidRtlID").value            = arrRtlID[rtlIndex];
  document.getElementById("inpAmtCost").value          = (arrRtlOb[rtlIndex].penCost / 100).toFixed(2);
  document.getElementById("inpDEff").value             = arrRtlOb[rtlIndex].dtEff;
}

function fnConfirmDelRtl() {
  strConfirm  = "<div class='content alert'>";
  strConfirm += "Delete this Retail amount?&nbsp;&nbsp;";
  strConfirm += "<input class='inp08' type='submit' name='subDelRtlY' value='Yes' />";
  strConfirm += "<input class='inp08' type='button' name='butDelRtlN' value='No' onclick='fnDelRtlN()' />";
  strConfirm += "</div>";
  document.getElementById("spanRtlConfirm").innerHTML = strConfirm;
}

function fnDelRtlN() {
  document.getElementById("spanRtlConfirm").innerHTML = "";
}

function fnCloseModalRtl() {
  document.getElementById("hidRtlID").value            = "";
  document.getElementById("inpAmtCost").value          = "";
  document.getElementById("inpDEff").value             = "";
  document.getElementById("spanRtlHead").innerHTML     = "";
  document.getElementById("spanRtlMessages").innerHTML = "";
  document.getElementById("spanRtlConfirm").innerHTML  = "";
  document.getElementById("myModalRtl").style.display  = "none";
  for(var i=0; i<arrRtlEr.length; i++) {
    document.getElementById(arrRtlEr[i]).classList.remove("err");
  }
}
