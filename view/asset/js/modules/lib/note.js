/**
 * Generates a small note popup
 * @author Scott Kiehn
 */

const notes = {}
let number = 0

export default (opts = {}) => {
  const id = `note-${++number}`
  notes[id] = {
    id    : id,
    class : opts.class || 'message',
    text  : opts.text,
    time  : opts.time === 0 ? 0 : (opts.time || 1500)
  }
  notes[id].el = create( notes[id] )
  notes[id].el.addEventListener('animationend', () => after( notes[id] ))
  notes[id].opened = false

  open( notes[id] )
  return notes[id]
}

const open = note => {
  note.opened = true
  note.el.style.display = 'inline-block'
  note.el.classList.remove('dom-item-out')
  note.el.classList.add('dom-item-in')
}

export const close = note => {
  note.opened = false
  note.el.classList.remove('dom-item-in')
  note.el.classList.add('dom-item-out')
}

const after = note => {
  if ( note.opened ) {
    if ( note.time ) note.timer = setTimeout(() => close( note ), note.time)
  } else {
    note.el.style.display = 'none'
    if ( note.timer ) clearTimeout( note.timer )
    note.el.remove()
    delete notes[note.el.id]
  }
}

const create = note => {
  document.body.insertAdjacentHTML('beforeend', `<div id="${note.id}" class="note-popup ${note.class}">${note.text}</div>`)
  return document.getElementById(note.id)
}

document.head.appendChild(document.createElement("style")).innerHTML = `
.note-popup {
  position: fixed;
  top: 16px;
  right: 16px;
  z-index: 10000;
  padding: 16px;
  border: 1px solid #fff;
  border-radius: 4px;
  width: auto;
  animation-fill-mode: both;
  animation-duration: 0.25s;
}
.dom-item-in {
  animation-name: dom-item-in;
}
.dom-item-out {
  animation-name: dom-item-out;
}
@keyframes dom-item-in {
  from {opacity: 0; transform: translate3d(0, -10px, 0);}
  to   {opacity: 1; transform: translate3d(0, 0, 0);}
}
@keyframes dom-item-out {
  from {opacity: 1;}
  to   {opacity: 0; transform: translate3d(0, -10px, 0);}
}
`;
