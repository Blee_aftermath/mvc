const modals = {};

let currentCallback;

export const modal = (opts = {}, cb = {}) => {
  // To keep it simpler, a submitted id will determine if the modal
  // will persist in the DOM or if it should be removed. However,
  // if opts.persist in explicitly set, then use it instead.

  if (opts.persist === true) {
    opts.persist = true;
  } else if (opts.persist === false) {
    opts.persist = false;
  } else {
    opts.persist = opts.id ? true : false;
  }

  opts.id = opts.id || "modal-" + Date.now();

  // Executes only the first time the modal is used.
  if (!modals[opts.id]) {
    const modalEl = create(opts);

    modalEl.addEventListener("click", (ev) => {
      if (ev.target.matches(".modal-close")) {
        ev.preventDefault();
        close(modalEl, opts.persist, currentCallback);
      }
    });

    // Add the modal to the top level "modals" object
    modals[opts.id] = modalEl;
  }

  currentCallback = cb;

  open(modals[opts.id], cb);
};

const create = (opts) => {
  if (opts.body) {
    document.body.insertAdjacentHTML("beforeend", content(opts));
    return document.getElementById(opts.id);
  } else {
    const modalEl = document.getElementById(opts.id),
      childEl = modalEl.children[0];

    if (!opts.title) opts.title = modalEl.title;

    // Use the class "fixed" to determine if the modal
    // already exists as a complete modal. Change title
    // for this static modal if a new title was submitted.
    // If not a static modal, then embed core modal content
    // into an existing modal div.

    if (childEl.classList.contains("fixed")) {
      modalEl.querySelector(".title").innerHTML = opts.title;
    } else {
      modalEl.removeAttribute("title");
      modalEl.insertAdjacentHTML("afterbegin", core(opts));
      modalEl.querySelector(".body").appendChild(childEl);
    }

    return modalEl;
  }
};

const content = (opts) =>
  `<div class="modal modal-close" id="${opts.id}">${core(opts)}</div>`;

const core = (opts) => {
  return `
  <div class="fixed">
    <div class="body_stripe"></div>
    <div class="head">
      <div class="content"><span class="title">${opts.title || "&nbsp;"}</span>
        <span class="modal-close"><img class="icon16" src="icons/no16.png" alt="Close" title="Close" /></span>
      </div>
    </div>
    <div class="message"></div>
    <div class="error">${opts.errors || ""}</div>
    <div class="body">
      ${opts.body || ""}
    </div>
    <div class="alert"></div>
    <div class="body_stripe"></div>
  </div>
`;
};

const open = (el, cb) => {
  if (cb.beforeOpen) cb.beforeOpen(el);
  el.style.display = "block";
  if (cb.afterOpen) cb.afterOpen(el);

  // Create a custom event "modalOpen"
  // that can be used by an addEventListener.
  // Listed after above code so it is fired after
  // the modal is opened.
  const event = new CustomEvent('modalOpen');
  el.dispatchEvent(event);
};

const close = (el, persist, cb) => {
  if (cb.beforeClose) cb.beforeClose(el);
  el.style.display = "none";
  if (cb.afterClose) cb.afterClose(el);

  // Only remove dynamic modals from the DOM.
  if (persist) {
    el.querySelector(".message").innerHTML = "";
    el.querySelector(".error").innerHTML = "";
    el.querySelector(".alert").innerHTML = "";
  } else {
    el.parentNode.removeChild(el);
    delete modals[el.id];
  }
};

document.head.appendChild(document.createElement("style")).innerHTML = `
.modal .head {
  text-align: center;
}
.modal .head .content .modal-close {
  float: right;
}
.modal .modal-close {
  cursor: pointer;
}
.modal .modal-close .icon16 {
  pointer-events: none;
}
.modal .body {
  padding: 1rem 1.5rem;
}
`;
