const params = form => {
  const p = {};
  for (const f of form.querySelectorAll('[name]')) {
    p[f.name] = f.value;
  }
  return p;
}

export const submit = (el, cb) => {

  const form = el.closest('form');

  let opts = {post: false},
    oops = 0;

  if ( el.dataset.opts ) {
    opts = JSON.parse(el.dataset.opts);
    opts.post = parseInt( opts.post ) === 1 ? true : false;
  }

  for ( const formEl of form.querySelectorAll('.req') ) {
    formEl.value = formEl.value.trim();
    if ( formEl.value.length < 1 ) oops++;
  }

  if ( oops ) {
    return cb({ ok: false, say: 'Please provide values for the required items.' });
  } else {

    if ( opts.post ) {
      form.submit();
    } else {

      fetch(form.action, {
        method: 'post',
        headers: {'Content-Type':'application/json;charset=utf-8'},
        body: JSON.stringify( params( form ) )
      }).then((res) => {
        return res.ok ? res.json() : {ok: false, say: 'There has been an error with the server request.'};
      }).then((body) => {
        return cb(body);
      });

    }
  }
}
