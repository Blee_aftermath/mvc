import {modal} from './modal.js';

export const message = opts => {
  const modalId = `message-modal-${Date.now()}`,
    reload = opts.reload ? true : false,
    text = opts.text,
    time = opts.time || 4000,
    title = opts.title || 'Message';

  modal({id: modalId, title: title, persist: false, body: `<p style="text-align: center;">${text}</p>`}, {
    beforeOpen: () => {
      if (  opts.confirm && typeof opts.confirm == 'function' ) {
        document.getElementById(modalId).querySelector('[data-yes]').addEventListener('click', ev => {
          opts.confirm();
        })
      }
    },
    afterOpen: () => {
      setTimeout(() => {
        const messageModal = document.getElementById(modalId);
        if ( messageModal) messageModal.querySelector('.modal-close').click();
      }, time);
    },
    afterClose: () => {
      if ( reload ) location.reload();
    }
  })
}
