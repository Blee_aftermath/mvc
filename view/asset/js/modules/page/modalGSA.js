import {default as formModal, fetches as fetchData} from '../ui/formModal.js';
import {default as formModalAjax} from '../ui/formModalAjax.js'

const runOnce = {};

export default (el) => formModal(el, ( formEl, params, option = 'set' ) => {

  formData(formEl, params, option);

  const gsaStates = formEl.querySelector('.gsaStates');
  if ( gsaStates ) {
    gsaStates.innerHTML = option == 'set' ? gsaStates.innerHTML = `States using this GSA ${formEl.querySelector('[name=inpGSAType]').value} text: ${params.inpGSAStates}` : '';
  }

  if ( !runOnce[`${formEl.id}`] ) {
    runOnce[`${formEl.id}`] = true;

    const toggleEl = formEl.querySelector('.form-toggle'),
      submitEl = formEl.querySelector('.form-submit');

    if ( toggleEl ) {
      toggleEl.addEventListener('click', (ev) => {
        for ( const childEl of formEl.querySelectorAll('p[id], label, [type=submit], [type=button]') ) {
          childEl.classList.toggle('hide');
        }
      });
    }

    if ( submitEl ) {
      submitEl.addEventListener('click', (ev) => {

        //formModalAjax(submitEl);

        // Two options, use above to just use the default formModalAjax along with an
        // an edit in the gsaPRG.php to set a session message and send along a reload option.
        // Or, use the below with a custom callback to rewrite the modal form and text
        // as well as save it to formModal's fetches object (as fetchData).

        formModalAjax(submitEl, (el, body) => {
          const modalEl = el.closest('.modal');

          if ( body.ok ) {

            const fetchId = `${modalEl.id}-${params['inpGSAID']}`;

            // Save the new data into formModal's fetches object (as fetchData)
            for ( const p in body.params ) {
             //console.log(fetchData[fetchId][p]);
             //console.log(body.params[p]);
             fetchData[fetchId][p] = body.params[p];
            }

            formData(formEl, body.params, 'set');

            modalEl.querySelector('.error').innerHTML = '';
            modalEl.querySelector('.message').innerHTML = `<p>${body.say}</p>`;
          } else {
            modalEl.querySelector('.message').innerHTML = '';
            modalEl.querySelector('.error').innerHTML = `<p>${body.say}</p>`;
          }
        });

      });
    }
  }
});

const formData = ( formEl, params, option ) => {
  for ( const p in params ) {

    const inputEl = formEl.querySelector(`[name=${p}]`),
      textEl = formEl.querySelector(`#${p}`);

    if ( inputEl ) {
      option == 'set'
        ? inputEl.value = params[p] || ''
        : inputEl.removeAttribute('value');
    }

    if ( textEl ) {
      textEl.innerHTML = option == 'set' ? (params[p] || '') : '';
    }
  }
}

document.head.appendChild( document.createElement('style') ).innerHTML = `
#form-gsa-buyer label,
#form-gsa-cancel label {
  display: block;
}
#form-gsa-buyer label.hide,
#form-gsa-cancel label.hide {
  display: none;
}
`;
