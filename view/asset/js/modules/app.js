/*
 * 
 * Global click event handler for
 * functions imported into the
 * window.jsClkEvt object.
 * 
 */

window.jsClkEvt = {}

const version = `?v${document.body.dataset.version || 1}`

document.addEventListener('click', (ev) => {
  if ( ev.target.matches('[data-js]') ) {
    ev.preventDefault()
    if ( window.jsClkEvt[ ev.target.dataset.js ] !== undefined ) window.jsClkEvt[ ev.target.dataset.js ]( ev.target )
  }
})
