import {submit} from '../lib/submit.js';

// Pass in a callback function or use the default serverResponse

export default (el, cb = undefined) => {

  if ( typeof cb != 'function' ) cb = serverResponse;

  submit( el, (body) => {
    cb( el, body );
  });
}

const serverResponse = (el, body) => {
  const modalEl = el.closest('.modal');
  if ( body.ok ) {
    if ( body.reload ) return location.reload();
    modalEl.querySelector('.error').innerHTML = '';
    modalEl.querySelector('.message').innerHTML = `<p>${body.say}</p>`;
  } else {
    modalEl.querySelector('.message').innerHTML = '';
    modalEl.querySelector('.error').innerHTML = `<p>${body.say}</p>`;
  }
}
