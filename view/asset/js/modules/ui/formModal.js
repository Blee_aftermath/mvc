import {modal} from '../lib/modal.js'

export const fetches = {}

const clkRegExp = /#clk-/

document.addEventListener('DOMContentLoaded', ev => {
  doClickAttr()
  doClickHash()
})

document.addEventListener('DynModsLoaded', ev => doClickHash())

// Pass in a callback function or use the default setValues

export default (el, cb = undefined) => {

  if ( typeof cb != 'function' ) cb = setValues

  const opts    = JSON.parse(el.dataset.opts),
    modalId     = opts.modal,
    modalErrors = opts.errors ? generateErrorHTML(opts.errors) : '',
    title       = opts.title || undefined,
    formEl      = document.getElementById(modalId).querySelector('form')

  let params = opts.params

  modal(
    { errors: modalErrors, id: modalId, title: title },
    {
      beforeOpen: async () => {

        if (opts.fetch) {
          const fetchId = opts.fetch_id
          if (fetches[fetchId]) {
            params = Object.assign(params, fetches[fetchId])
          } else {

            const init = {method: 'get'}

            if (opts.post) {
              init.method  = 'post'
              init.headers = { 'Content-Type': 'application/jsoncharset=utf-8' }
              init.body    = JSON.stringify(params)
            }

            const req = await fetch(opts.fetch, init),
                  res = await req.json()

            if ( res.ok ) {
              fetches[fetchId] = res.params
              params = Object.assign(params, fetches[fetchId])
            } else {
              //add 'msg', deprecate 'say'
              return {ok: false, msg: 'There has been an error with the server request.', say: 'There has been an error with the server request.'}
            }

          }
        }

        cb(formEl, params)

      },
      afterClose: () => {
        setValues(formEl, params, 'reset')
      },
    }
  )
}

const doClickAttr = () => {
  const clk = document.querySelector('[data-clk]')
  if (clk) {
    clk.removeAttribute('data-clk')
    clk.click()
  }
}

const doClickHash = () => {
  if (location.hash) {
    const hash = location.hash
    if ( hash.match(clkRegExp, '') ) {
      const clk = document.getElementById( hash.replace(clkRegExp, '') )
      if ( clk ) clk.click()
    }
  }
}

const isNull = v => !v && typeof v === 'object'

/**
 * Generates HTML to stick in error span
 * @param errors Array of error messages to append
 * @returns String containing spans
 */
const generateErrorHTML = (errors) => {
  let errHTML = ''
  for (let i in errors)
    errHTML += `<p>${errors[i]}</p>`

  return errHTML
}

const setValues = (formEl, params, option = 'set') => {
  for (const p in params) {
    if (isNull(params[p])) params[p] = ''

    const el = formEl.querySelector(`[name=${p}]`)
    if (el) {
      if (option == 'set') {
        //check if params is an array

        if (params[p].constructor == Object) {
          //loop through each param
          for (const val in params[p]) {
            switch (val) {
              case 'addClass':
                el.classList.add(params[p][val])
                break
              case 'style.display':
                el.style.display = params[p][val]
                break
              default:
                setElValue(el, params[p][val])
                break
            }
          }
        } else {
          //default to value if only 1 param
          setElValue(el, params[p])
        }
      } else if (option == 'reset') {
        //if were not in set mode, we want to remove instead of set these attributes
        if (params[p].constructor == Object) {
          //loop through each param
          for (const val in params[p]) {
            switch (val) {
              case 'addClass':
                el.classList.remove(params[p][val])
                break
              case 'style.display':
                el.style.display = 'none'
                break
              default:
                setElValue(el)
                break
            }
          }
        } else {
          //default to value if only 1 param
          setElValue(el)
        }
      }
    }
  }
}

const setElValue = (el, val = '') => {
  //console.log(el.type)
  switch (el.type) {
    case 'checkbox':
      el.checked = val == ('' || 0) ? false : true
      break

    //case 'radio':
    //  break

    //case 'select-one':
    //  break

    default:
      el.value = val
      break
  }
}

