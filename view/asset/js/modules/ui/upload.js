const uploads = {};

export default (el) => {
  const opts = el.dataset.opts ? JSON.parse(el.dataset.opts) : {},
    id = opts.id || 'upload-' + Date.now();

  if ( !uploads[id] ) uploads[id] = form(opts);

  uploads[id].click();
}

const form = opts => {
  document.body.insertAdjacentHTML('beforeend', html(opts));

  const form = document.getElementById(opts.id),
    field = form.querySelector('[type=file]');

  field.addEventListener('change', ev => form.submit());

  return field;
}

const html = opts => {
  const params = opts.params;
  let inputs = '';
  
  for ( const p in params ) inputs += `<input type="hidden" name="${p}" value="${params[p]}" />`;

  return `<form id="${opts.id}" method="post" action="${opts.action}" enctype="multipart/form-data" style="display: none;">${inputs}<input type="file" name="${opts.name}" ${opts.mult ? 'multiple ' : ''}/></form>`;
}
