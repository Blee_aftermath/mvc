/**
 * Class for jQueryUI Datepicker variations
 * @author Scott Kiehn
 */

let increment = 0

export class DP {

  /**
   * Sets an html input element as a single item datepicker
   * @param {HTMLElement} element
   */
  static single(element){

    element.insertAdjacentHTML('afterend', `<span id="nix-${++increment}"></span>`);

    const $inp = jQuery(element), // $ precedes the jQuery object variable
          nix  = document.getElementById(`nix-${increment}`)

    if ($inp.val().length) nix.classList.add('inp-nix')

    $inp.datepicker({
      dateFormat: 'mm/dd/y',
      onSelect: (selectedDate) => {
        $inp.val().length ? nix.classList.add('inp-nix') : nix.classList.remove('inp-nix')
      }
    })

    nix.addEventListener('click', (ev) => {
      $inp.val('')
      nix.classList.remove('inp-nix')
    })
  }

  /**
   * Sets an html input element as a double item datepicker
   * @param {HTMLElement} element
   */
  static double(element){

    element.insertAdjacentHTML('afterend', `<span id="nix-${++increment}"></span>`);

    const $inp = jQuery(element), // $ precedes the jQuery object variable
          nix  = document.getElementById(`nix-${increment}`)

    if ($inp.val().length) nix.classList.add('inp-nix')

    $inp.datepicker({
      dateFormat: 'mm/dd/y',
      onSelect: (selectedDate) => {

        const data = $inp.data().datepicker

        if(!data.first){
          data.inline = true
          data.first = selectedDate
        } else {
          if (selectedDate > data.first){
            $inp.val( `${data.first}-${selectedDate}` )
          } else {
            $inp.val( `${selectedDate}-${data.first}` )
          }
          data.inline = false
        }

      },
      onClose: () => {
        const data = $inp.data().datepicker
        if (data.first) nix.classList.add('inp-nix')
        delete data.first
        data.inline = false
      }
    })

    nix.addEventListener('click', (ev) => {
      $inp.val('')
      nix.classList.remove('inp-nix')
    })
  }
}

// Add CSS for the X delete
document.head.appendChild(document.createElement('style')).innerHTML = `
.inp-nix {
  display: inline-block;
  position: relative;
  top: 5px;
  left: -14px;
  width: 15px;
  height: 15px;
  cursor: pointer;
  font-size: 15px;
  color: #777;
}
.inp-nix:after {
  position: absolute;
  top: -4px;
  content: "\\000D7";
}
`
