import { default as note, close as closeNote } from './modules/lib/note.js'

String.prototype.toNum = function () {return Number(this)}
Number.prototype.toNum = function () {return Number(this)}

/**
 * Provides ajax interface for utilizationOffQr.php
 * @author Scott Kiehn
 */
export class UtilizationOffQr {
  /**
   * Creates a new UtilizationOffQr class
   */
  constructor() {
    this.dom = {}
    this.dom.offices = document.getElementById('offices')
    this.dom.totals  = {}

    this.num = {}
    //this.num.cols = 12
    this.num.rows = this.dom.offices.dataset.offices.toNum()

    this.tags = ['amex_rev', 'sal_rev', 'util_job', 'util_mkt', 'util_ot']

    for (const t of this.dom.offices.querySelectorAll('[id]'))
      this.dom.totals[ t.id ] = t

    this.dom.offices.addEventListener('click',  (ev) => this.cellUI(ev))
    this.dom.offices.addEventListener('keyup',  (ev) => this.cellUI(ev))
    this.dom.offices.addEventListener('change', (ev) => this.cellUI(ev))

    document.querySelector('.button-row').addEventListener('click', (ev) => this.formUI(ev))
  }

  /**
   * Form Row/Cell interface
   * @param {Object} ev
   */
  cellUI(ev) {
    const el = ev.target

    if (ev.type == 'click') {
      el.select()
      return
    }

    // Return if keyup on TAB
    if (ev.which == 9) return

    Number(el.value) !== Number(el.dataset.value) ? el.classList.add('rev') : el.classList.remove('rev')
  }

  /**
   * Form button interface
   * @param {Object} ev
   */
  formUI(ev) {
    const el = ev.target

    if (el.matches('[name=save]')) {
      const inpData = {}

      for (const office of this.dom.offices.querySelectorAll('tbody[data-office]')) {
        const officeID = office.dataset.office
        if (officeID) {
          for (const m of office.querySelectorAll('[type=number]')) {

            if (!m.dataset.tag) continue

            if (m.value != m.dataset.value) {

              if (!inpData[officeID])
                inpData[officeID] = {office: officeID}

              if (!inpData[officeID][m.dataset.tag])
                inpData[officeID][m.dataset.tag]= {}

              inpData[officeID][m.dataset.tag][m.dataset.col] = m.value
            }
          }
        }
      }

      //console.log(inpData)

      this.saveRevised(el.dataset.year, inpData)
    }

    if (el.matches('[name=reset]')) {
      for (const office of this.dom.offices.querySelectorAll('tbody[data-office]')) {
        for (const m of office.querySelectorAll('[type=number]')) {
          m.value = m.dataset.value
          m.classList.remove('rev')
        }
      }
    }

  }

  /**
   * Save revised rows
   * @param {Int} yrView
   * @param {Object} inpData
   */
  async saveRevised(yrView, inpData) {

    const saveNote = note({ text: 'Saving...', class: 'alert', time: 0 })

    const post = await fetch('utilizationOffQrPRG.php', {
      method: 'post',
      headers: { 'Content-Type': 'application/json;charset=utf-8' },
      body: JSON.stringify({ action: 'save', yrView: yrView, inpData: inpData })
    })

    const res = await post.json()

    closeNote(saveNote)

    if (res.ok) {

      for (const officeID in res.data) {

        const office = this.dom.offices.querySelector(`tbody[data-office='${officeID}']`)

        for (const col in res.data[officeID]) {

          const d = res.data[officeID][col]

          for (const row of this.tags) {

            const cell = office.querySelector(`[name=qr_${row}_${col}]`)

            cell.value = d[`goq_per_${row}`]
            cell.setAttribute('data-value', d[`goq_per_${row}`])
            cell.classList.remove('rev')
          }
        }
      }

      note({ text: 'The office quarterly goal data has been saved.' })
    } else {
      note({ text: res.msg || 'There has been an error with your request.', class: 'error' })
    }
  }
}
