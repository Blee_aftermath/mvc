<?php

// ==================================================================================================== //
// === FUNCTIONS === //
// ==================================================================================================== //

  function fnFillTruckJS($myRes) {

    echo "\n\n<script language=\"JavaScript\">";
    echo "\n<!-- Hide the script from old browsers --";

    echo "\nvar objTruck = {};"; // Object of Office fields.  Can also = new Object();

    echo "\nobjTruck.id          = \"" . $myRes['truck_id']                       . "\";";
    echo "\nobjTruck.yr          = \"" . $myRes['truck_yr']                       . "\";";
    echo "\nobjTruck.type        = \"" . $myRes['truck_type']                     . "\";";
    echo "\nobjTruck.fuel        = \"" . $myRes['truck_arr_fuel']                 . "\";";
    echo "\nobjTruck.office      = \"" . $myRes['truck_office']                   . "\";";
    echo "\nobjTruck.status      = \"" . $myRes['truck_status']                   . "\";";
    echo "\nobjTruck.trans       = \"" . $myRes['truck_trans_type_id']            . "\";";
    echo "\nobjTruck.transnumber = \"" . $myRes['truck_trans_number']             . "\";";
//  echo "\nobjTruck.is_act      = \"" . $myRes['truck_is_act']                   . "\";";
    echo "\nobjTruck.vin         = \"" . prepareJS($myRes['truck_vin'])           . "\";";
    echo "\nobjTruck.lic         = \"" . prepareJS($myRes['truck_lic'])           . "\";";
    echo "\nobjTruck.state       = \"" . prepareJS($myRes['truck_lic_state'])     . "\";";
    echo "\nobjTruck.ent_unit    = \"" . prepareJS($myRes['truck_ent_unit'])      . "\";";
    echo "\nobjTruck.geo         = \"" . prepareJS($myRes['truck_geo'])           . "\";";
    echo "\nobjTruck.info        = \"" . prepareJS($myRes['truck_info'])          . "\";";
    echo "\nobjTruck.d_ins_exp   = \"" . formatDText(strtotime($myRes['truck_dt_ins_exp'])) . "\";";
    echo "\nobjTruck.d_reg_exp   = \"" . formatDText(strtotime($myRes['truck_dt_reg_exp'])) . "\";";

    echo "\narrTruckID.push(" . $myRes['truck_id'] . ");";
    echo "\narrTruckOb.push(objTruck);"; // Push this object into the array of Objects

    echo "\n// --End Hiding Here -->";
    echo "\n</script>\n";

  }

  function fnFillTruckPost() {
    echo "\n<script language=\"JavaScript\">";
    echo "\n<!-- Hide the script from old browsers --";

    echo "\nvar objTruck = {};"; // Create a new object to hold the posted values.

    if (isset($_SESSION['inpValue']['inpTruckID'])) {
      echo "\narrTruckID.push(\"" . prepareJS($_SESSION['inpValue']['inpTruckID']) . "\");";
      echo "\nobjTruck.id     = \"" . prepareJS($_SESSION['inpValue']['inpTruckID']) . "\";";
    }
    echo "\nobjTruck.yr          = \"" . prepareJS($_SESSION['inpValue']['inpTruckYr'])          . "\";";
    echo "\nobjTruck.type        = \"" . prepareJS($_SESSION['inpValue']['selTruckType'])        . "\";";
    echo "\nobjTruck.fuel        = \"" . prepareJS($_SESSION['inpValue']['selTruckFuel'])        . "\";";
    echo "\nobjTruck.office      = \"" . prepareJS($_SESSION['inpValue']['selTruckOffice'])      . "\";";
    echo "\nobjTruck.status      = \"" . prepareJS($_SESSION['inpValue']['selTruckStatus'])      . "\";";
    echo "\nobjTruck.trans      = \"" .  prepareJS($_SESSION['inpValue']['selTruckTrans'])       . "\";";
    echo "\nobjTruck.transnumber = \"" . prepareJS($_SESSION['inpValue']['inpTruckNumber'])      . "\";";
    echo "\nobjTruck.vin         = \"" . prepareJS($_SESSION['inpValue']['inpTruckVin'])         . "\";";
    echo "\nobjTruck.lic         = \"" . prepareJS($_SESSION['inpValue']['inpTruckLic'])         . "\";";
    echo "\nobjTruck.state       = \"" . prepareJS($_SESSION['inpValue']['selTruckLicState'])    . "\";";
    echo "\nobjTruck.ent_unit    = \"" . prepareJS($_SESSION['inpValue']['inpTruckEntUnit'])     . "\";";
    echo "\nobjTruck.geo         = \"" . prepareJS($_SESSION['inpValue']['inpTruckGeo'])         . "\";";
    echo "\nobjTruck.info        = \"" . prepareJS($_SESSION['inpValue']['inpTruckInfo'])        . "\";";
  //  echo "\nobjTruck.is_act      = \"" . (isset($_SESSION['inpValue']['chkTruckIsAct']) ? 1 : 0) . "\";";
    echo "\nobjTruck.d_ins_exp   = \"" . formatDText(strtotime($_SESSION['inpValue']['inpTruckDInsExp'])) . "\";";
    echo "\nobjTruck.d_reg_exp   = \"" . formatDText(strtotime($_SESSION['inpValue']['inpTruckDRegExp'])) . "\";";

    echo "\narrTruckOb.push(objTruck);"; // Tack this onto the end of the array of Truck objects.

    foreach($_SESSION['errField'] AS $field) {

      echo "\narrTruckEr.push(\"" . $field    . "\");";
      echo "\ndocument.getElementById(\"" . $field . "\").classList.add(\"err\");";

    }

    // Show the modal dialog box

    if (isset($_SESSION['inpValue']['subAddTruck'])) echo "\nfnAddTruck();";
    if (isset($_SESSION['inpValue']['subChgTruck'])) echo "\nfnChgTruck(" . $_SESSION['inpValue']['inpTruckID'] . ");";
    echo "\nfnFillTruck(arrTruckOb.length-1);";

    echo "// --End Hiding Here -->\n";
    echo "\n</script>\n";

  }

// ==================================================================================================== //
// === MODAL === //
// ==================================================================================================== //

  echo "<div class='modal' id='myModalTruck'>";
  echo "<div class='fixed'>";
  echo "<div class='body_stripe'></div>";

  echo "<div class='head'>";
  echo "<div class='content'>";

  echo "<span id='spanTruckHead'></span>";
  echo "<span style='float:right;'><a href='javascript:fnCloseModalTruck();'><img class='icon16' src='icons/no16.png' alt='Close' title='Close' /></a></span>";

  echo "</div>"; // content
  echo "</div>"; // head

  echo "<span id='spanTruckMessages'>";
  fnModalMessages();
  echo "</span>";

  echo "<div class='body'>";
  echo "\n<form method='post' name='frmModalTruck' action='modalPRG.php'>";
  echo "<div class='content'>";

  echo "<table class='info' id='tblTruck'>";

  $inpField = "inpTruckID";
  $inpLabel = "Truck ID";
  $defValue = "";
  $defClass = "inp08 req";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' value='$defValue' />";
  echo "</td></tr>";

  $inpField = "inpTruckYr";
  $inpLabel = "Year";
  $defValue = "";
  $defClass = "inp08 req";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' value='$defValue' maxlength='4' list='off' />";
  echo "</td></tr>";

  $sqlType = "SELECT * FROM truck_type ORDER BY truck_type_id";
  $resType = $mysqli->query($sqlType);

  $inpField = "selTruckType";
  $inpLabel = "Type";
  $defValue = "";
  $defClass = "req";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<select class='$defClass' name='$inpField' id='$inpField'>";
  echo "<option value='' selected='selected' >--- Select ---</option>";
  while ($myType = $resType->fetch_assoc()) {
    echo "<option value='" . $myType['truck_type_id'] . "'>" . prepareFrm($myType['truck_type_dsca']) . "</option>";
  }
  echo "</select>";
  echo "</td></tr>";

  $inpField = "selTruckFuel";
  $inpLabel = "Fuel";
  $defValue = "";
  $defClass = "req";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<select class='$defClass' name='$inpField' id='$inpField'>";
  echo "<option value='' selected='selected' >--- Select ---</option>";
  foreach ($arrTruckFuel AS $key => $value) {
    echo "<option value='" . $key . "'>" . $value . "</option>";
  }
  echo "</select>";
  echo "</td></tr>";

  $sqlOffice = "SELECT * FROM office ORDER BY office_id";
  $resOffice = $mysqli->query($sqlOffice);
  $inpField  = "selTruckOffice";
  $inpLabel  = "Office";
  $defValue  = "";
  $defClass  = "req";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<select class='$defClass' name='$inpField' id='$inpField'>";
  echo "<option value='' selected='selected'>--- Select ---</option>";
  while ($myOffice = $resOffice->fetch_assoc()) {
    echo "<option value='" . $myOffice['office_id'] . "'>" . prepareFrm($myOffice['office_name']) . "</option>";
  }
  echo "</select>";
  echo "</td></tr>";
  
  // Truck Status
  $sqlTruckStatus = "SELECT * FROM truck_status ORDER BY truck_status_id";
  $resStatus = $mysqli->query($sqlTruckStatus);
  $inpField  = "selTruckStatus";
  $inpLabel  = "Truck Status";
  $defValue  = "";
  $defClass  = "req";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<select class='$defClass' name='$inpField' id='$inpField'>";
  echo "<option value='' selected='selected'>--- Select ---</option>";
  while ($myStatus = $resStatus->fetch_assoc()) {
    echo "<option value='" . $myStatus['truck_status_id'] . "'>" . prepareFrm($myStatus['truck_status_dsca']) . "</option>";
  }
  echo "</select>";
  echo "</td></tr>";
   
  // Truck Transponder type
  $sqlTruckTrans = "SELECT * FROM truck_transponder ORDER BY truck_trans_type";
  $resTruckTrans = $mysqli->query($sqlTruckTrans);
  $inpField  = "selTruckTrans";
  $inpLabel  = "Transponder Type";
  $defValue  = "";
  $defClass  = "opt";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<select class='$defClass' name='$inpField' id='$inpField'>";
  echo "<option value='' selected='selected'>--- Select ---</option>";
  while ($myStatus = $resTruckTrans->fetch_assoc()) {
    echo "<option value='" . $myStatus['truck_trans_id'] . "'>" . prepareFrm($myStatus['truck_trans_type']) . "</option>";
  }
  echo "</select>";
  echo "</td></tr>";
  
 // Truck Transponder number
  $inpField = "inpTruckNumber";
  $inpLabel = "Transponder Number";
  $defValue = "";
  $defClass = "opt";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' value='$defValue' maxlength='$maxTrans' list='off' />";
  echo "</td></tr>";


  $inpField = "inpTruckVin";
  $inpLabel = "VIN";
  $defValue = "";
  $defClass = "opt";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' value='$defValue' maxlength='$maxVIN' list='off' />";
  echo "</td></tr>";

  $inpField = "inpTruckLic";
  $inpLabel = "License Plate";
  $defValue = "";
  $defClass = "inp10 opt";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' value='$defValue' maxlength='$maxLic' list='off' />";

  $sqlState = "SELECT * FROM state ORDER BY st_name";
  $resState = $mysqli->query($sqlState) or die('Get state failed');

  $inpField = "selTruckLicState";
  $defClass = "inp06 opt";
  echo "<select class='$defClass' name='$inpField' id='$inpField'>";
  echo "<option value='' selected='selected'>---</option>";
  while ($myState = $resState->fetch_assoc()) {
    echo "<option value='" . prepareFrm($myState['st_id']) . "'>" . prepareFrm($myState['st_id']) . "</option>";
  }
  echo "</select>";
  echo "</td></tr>";

  $inpField = "inpTruckEntUnit";
  $inpLabel = "Enterprise Unit";
  $defValue = "";
  $defClass = "inp08 opt";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' value='$defValue' maxlength='$lenEntUnit' list='off' />";
  echo "</td></tr>";

  $inpField = "inpTruckGeo";
  $inpLabel = "GeoTab";
  $defValue = "";
  $defClass = "inp08 opt";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' value='$defValue' maxlength='$maxVIN' list='off' />";
  echo "</td></tr>";

  $inpField = "inpTruckDInsExp";
  $inpLabel = "Insurance Exp";
  $defClass = "inp12 opt";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<input id='$inpField' class='$defClass' type='text' name='$inpField' id='$inpField' maxlength='$maxDate' list='off' />";
  echo "</td></tr>";
  echo "<script>$(function() { $('#".  $inpField . "').datepicker(); }); </script>";

  $inpField = "inpTruckDRegExp";
  $inpLabel = "Registration Exp";
  $defClass = "inp12 opt";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<input id='$inpField' class='$defClass' type='text' name='$inpField' id='$inpField' maxlength='$maxDate' list='off' />";
  echo "</td></tr>";
  echo "<script>$(function() { $('#".  $inpField . "').datepicker(); }); </script>";

  $inpField = "inpTruckInfo";
  $inpLabel = "Additional<br />Information";
  $defValue = "";
  $defClass = "opt";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<textarea class='$defClass' name='$inpField' id='$inpField' rows='6' cols='48'>$defValue</textarea>";
  echo "</td></tr>";


  $arrButtonRowTruck[] = "<input class='inp12' type='submit' name='subAddTruck' id='subAddTruck' value='Add' />";
  $arrButtonRowTruck[] = "<input class='inp12' type='submit' name='subChgTruck' id='subChgTruck' value='Save' />";
  $arrButtonRowTruck[] = "<input class='inp12' type='button' name='butConTruck' id='butConTruck' value='Delete' onclick='fnConfirmDelTruck();' />";
  $arrButtonRowTruck[] = "<input class='inp12' type='button' name='butCanTruck' id='butCanTruck' value='Cancel' onclick='fnCloseModalTruck();' />";

  echo "<tr class='content'><td></td><td>" . implode("", $arrButtonRowTruck) . "</td></tr>";

  echo "</table>";

  echo "</div>"; // content
  echo "<span id='spanTruckConfirm'></span>";
  echo "</form>";
  echo "</div>"; // body

  echo "<div class='body_stripe'></div>";
  echo "</div>"; // fixed
  echo "</div>"; // modal

?>