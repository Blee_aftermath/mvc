import { default as note, close as closeNote } from './modules/lib/note.js'

String.prototype.toNum = function () {return Number(this)}
Number.prototype.toNum = function () {return Number(this)}

/**
 * Provides ajax interface for utilization.php
 * @author Scott Kiehn
 */
export class Utilization {
  /**
   * Creates a new Utilization class
   */
  constructor() {
    this.dom = {}
    this.dom.offices = document.getElementById('offices')
    this.dom.totals  = {}

    this.num = {}
    this.num.cols = 12
    this.num.rows = this.dom.offices.dataset.offices.toNum()

    this.tags = ['job','mkt','drv','rt_oth','ot_oth','oth','tot']

    for (const t of this.dom.offices.querySelectorAll('[id]'))
      this.dom.totals[ t.id ] = t

    this.dom.offices.addEventListener('click', (ev) => this.cellUI(ev))
    this.dom.offices.addEventListener('keyup', (ev) => this.cellUI(ev))
    this.dom.offices.addEventListener('change', (ev) => this.cellUI(ev))

    this.calcAll()

    document.querySelector('.button-row').addEventListener('click', (ev) => this.formUI(ev))
  }

  /**
   * Form Row/Cell interface
   * @param {Object} ev
   */
  cellUI(ev) {
    const el = ev.target

    if (!el.name || el.name.match(/mo_oth_/) || el.name.match(/mo_tot_/)) return

    if (ev.type == 'click') {
      el.select()
      return
    }

    // Return if keyup on TAB
    if (ev.which == 9) return

    Number(el.value) !== Number(el.dataset.value) ? el.classList.add('rev') : el.classList.remove('rev')

    if (el.dataset.tie) {
      const tie = el.dataset.tie,
            all = this.dom.offices.querySelectorAll(`[data-tie='${tie}']`),
            [office, month] = tie.split(/-/)

      let oth = 0,
          tot = 0

      //These will occur in the HTML order
      for (const a of all) {
        if (a.name == `mo_job_${month}`)    tot += a.value.toNum()
        if (a.name == `mo_mkt_${month}`)    tot += a.value.toNum()
        if (a.name == `mo_drv_${month}`)    tot += a.value.toNum()
        if (a.name == `mo_rt_oth_${month}`) oth += a.value.toNum()
        if (a.name == `mo_ot_oth_${month}`) oth += a.value.toNum()
        if (a.name == `mo_oth_${month}`)    a.value = oth.toFixed(2)
        if (a.name == `mo_tot_${month}`)    a.value = (oth + tot).toFixed(2)
      }
    }

    this.calcRow(el.dataset.row, el.dataset.tag)
    this.calcCol(el.dataset.col, el.dataset.tag)

    // If the item is tied to a readonly cell, then calc it too
    this.calcCol(el.dataset.col, 'tot')
    this.calcRow(el.dataset.row, 'tot')

    if ( el.dataset.tag == 'rt_oth' || el.dataset.tag == 'ot_oth') {
      this.calcCol(el.dataset.col, 'oth')
      this.calcRow(el.dataset.row, 'oth')
    }

    this.calcTot()
  }

  /**
   * Calculate all totals 
   */
  calcAll() {
    for (let n = 1; n <= this.num.cols; n++) {
      for (const tag of this.tags)
        this.calcCol(n, tag)
    }
    for (let n = 1; n <= this.num.rows; n++) {
      for (const tag of this.tags)
        this.calcRow(n, tag)
    }
    this.calcTot()
  }

  /**
   * Calculate a column
   */
  calcCol(col, tag) {
    let colSum = 0

    for (const c of this.dom.offices.querySelectorAll(`[data-col='${col}']`))
      if (c.dataset.tag == tag) colSum += c.value.toNum()

    this.dom.totals[`col-tot-${tag}-${col}`].innerText = colSum.toFixed(2)
  }

  /**
   * Calculate a row
   */
  calcRow(row, tag) {
    let rowSum = 0

    for (const c of this.dom.offices.querySelectorAll(`[data-row='${row}']`))
      if (c.dataset.tag == tag) rowSum += c.value.toNum()

    this.dom.totals[`row-tot-${tag}-${row}`].innerText = rowSum.toFixed(2)
  }

  /**
   * Calculate bottom totals
   */
  calcTot() {
    for (const tag of this.tags) {
      let totSum = 0

      for (let n = 1; n <= this.num.cols; n++)
        totSum += this.dom.totals[`col-tot-${tag}-${n}`].innerText.toNum()

      this.dom.totals[`tot-${tag}`].innerText = totSum.toFixed(2)
    }
  }

  /**
   * Form button interface
   * @param {Object} ev
   */
  formUI(ev) {
    const el = ev.target

    if (el.matches('[name=save]')) {
      const inpData = {}

      for (const office of this.dom.offices.querySelectorAll('tbody[data-office]')) {
        const officeID = office.dataset.office
        if (officeID) {
          for (const m of office.querySelectorAll('[type=number]')) {

            if (!m.dataset.tag || m.dataset.tag == 'oth' || m.dataset.tag == 'tot') continue

            if (m.value != m.dataset.value) {

              if (!inpData[officeID])
                inpData[officeID] = {office: officeID}

              if (!inpData[officeID][m.dataset.tag])
                inpData[officeID][m.dataset.tag]= {}

              inpData[officeID][m.dataset.tag][m.dataset.col] = m.value
            }
          }
        }
      }

      //console.log(inpData)

      this.saveRevised(el.dataset.year, inpData)
    }

    if (el.matches('[name=reset]')) {
      for (const office of this.dom.offices.querySelectorAll('tbody[data-office]')) {
        for (const m of office.querySelectorAll('[type=number]')) {
          m.value = m.dataset.value
          m.classList.remove('rev')
        }
      }
      this.calcAll()
    }

  }

  /**
   * Save revised rows
   * @param {Int} yrView
   * @param {Object} inpData
   */
  async saveRevised(yrView, inpData) {

    const saveNote = note({ text: 'Saving...', class: 'alert', time: 0 })

    const post = await fetch('utilizationPRG.php', {
      method: 'post',
      headers: { 'Content-Type': 'application/json;charset=utf-8' },
      body: JSON.stringify({ action: 'save', yrView: yrView, inpData: inpData })
    })

    const res = await post.json()

    closeNote(saveNote)

    if (res.ok) {

      for (const officeID in res.data) {

        const office = this.dom.offices.querySelector(`tbody[data-office='${officeID}']`)

        for (const col in res.data[officeID]) {

          const d = res.data[officeID][col]

          for (const row of this.tags) {

            const cell = office.querySelector(`[name=mo_${row}_${col}]`)

            cell.value = d[`go_hr_util_${row}`]
            cell.setAttribute('data-value', d[`go_hr_util_${row}`])
            cell.classList.remove('rev')
          }
        }
      }

      this.calcAll()

      note({ text: 'The office data has been saved.' })
    } else {
      note({ text: res.msg || 'There has been an error with your request.', class: 'error' })
    }
  }
}
