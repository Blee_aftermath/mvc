<?php

// ==================================================================================================== //
// === FUNCTIONS === //
// ==================================================================================================== //

  function fnFillOfficeJS($myRes) {

    echo "\n\n<script language=\"JavaScript\">";
    echo "\n<!-- Hide the script from old browsers --";

    echo "\nvar objOffice = {};"; // Object of Office fields.  Can also = new Object();


    echo "\nobjOffice.id           = \"" . $myRes['office_id']                      . "\";";
    echo "\nobjOffice.region       = \"" . $myRes['office_region']                  . "\";";
    echo "\nobjOffice.is_act       = \"" . $myRes['office_is_act']                  . "\";";
    echo "\nobjOffice.is_field     = \"" . $myRes['office_is_field']                . "\";";
    echo "\nobjOffice.is_eqs       = \"" . $myRes['office_is_eqs']                  . "\";";
    echo "\nobjOffice.has_hydroxyl = \"" . $myRes['office_has_hydroxyl']            . "\";";
    echo "\nobjOffice.name         = \"" . prepareJS($myRes['office_name'])         . "\";";
    echo "\nobjOffice.addr1        = \"" . prepareJS($myRes['office_addr1'])        . "\";";
    echo "\nobjOffice.addr2        = \"" . prepareJS($myRes['office_addr2'])        . "\";";
    echo "\nobjOffice.city         = \"" . prepareJS($myRes['office_city'])         . "\";";
    echo "\nobjOffice.state        = \"" . prepareJS($myRes['office_state'])        . "\";";
    echo "\nobjOffice.zip          = \"" . prepareJS($myRes['office_zip'])          . "\";";
    echo "\nobjOffice.stock_level  = \"" . prepareJS($myRes['office_stock_level'])  . "\";";
    echo "\nobjOffice.cov_adj_mi   = \"" . prepareJS($myRes['office_cov_adj_mi'])   . "\";";
    echo "\nobjOffice.google       = \"" . prepareJS($myRes['office_google'])       . "\";";
    echo "\nobjOffice.info         = \"" . prepareJS($myRes['office_info'])         . "\";";
    echo "\nobjOffice.lld_name     = \"" . prepareJS($myRes['office_lld_name'])     . "\";";
    echo "\nobjOffice.lld_telp     = \"" . formatPhone($myRes['office_lld_telp'])   . "\";";
    echo "\nobjOffice.lld_extp     = \"" . prepareJS($myRes['office_lld_extp'])     . "\";";
    echo "\nobjOffice.lld_telc     = \"" . formatPhone($myRes['office_lld_telc'])   . "\";";
    echo "\nobjOffice.lld_extc     = \"" . prepareJS($myRes['office_lld_extc'])     . "\";";
    echo "\nobjOffice.lld_email    = \"" . prepareJS($myRes['office_lld_email'])    . "\";";
    echo "\nobjOffice.d_lease_exp  = \"" . (empty($myRes['office_d_lease_exp']) ? "" : formatDText(strtotime($myRes['office_d_lease_exp']))) . "\";";
    echo "\nobjOffice.emp_lesr     = \"" . $myRes['office_emp_lesr']                . "\";";
    echo "\nobjOffice.tsp_telp     = \"" . formatPhone($myRes['office_tsp_telp'])   . "\";";
    echo "\nobjOffice.tsp_permit   = \"" . prepareJS($myRes['office_tsp_permit'])       . "\";";
    echo "\nobjOffice.bio_company  = \"" . prepareJS($myRes['office_bio_company'])      . "\";";
    echo "\nobjOffice.bio_addr1    = \"" . prepareJS($myRes['office_bio_addr1'])        . "\";";
    echo "\nobjOffice.bio_addr2    = \"" . prepareJS($myRes['office_bio_addr2'])        . "\";";
    echo "\nobjOffice.bio_city     = \"" . prepareJS($myRes['office_bio_city'])         . "\";";
    echo "\nobjOffice.bio_state    = \"" . prepareJS($myRes['office_bio_state'])        . "\";";
    echo "\nobjOffice.bio_zip      = \"" . prepareJS($myRes['office_bio_zip'])          . "\";";
    echo "\nobjOffice.bio_telp     = \"" . formatPhone($myRes['office_bio_telp'])       . "\";";
    echo "\nobjOffice.bio_permit   = \"" . prepareJS($myRes['office_bio_permit'])       . "\";";

    echo "\narrOfficeID.push(" . $myRes['office_id'] . ");";
    echo "\narrOfficeOb.push(objOffice);"; // Push this object into the array of Objects

    echo "\n// --End Hiding Here -->";
    echo "\n</script>\n";

  }

  function fnFillOfficePost() {

    echo "\n<script language=\"JavaScript\">";
    echo "\n<!-- Hide the script from old browsers --";

    echo "\nvar objOffice = {};"; // Create a new object to hold the posted values.

    if (isset($_SESSION['inpValue']['reoOfficeID'])) {
      echo "\narrOfficeID.push(" . $_SESSION['inpValue']['reoOfficeID'] . ");";
      echo "\nobjOffice.id     = \"" . $_SESSION['inpValue']['reoOfficeID'] . "\";";
    }

    echo "\nobjOffice.region      = \"" . $_SESSION['inpValue']['selOfficeRegion']                 . "\";";
    echo "\nobjOffice.name        = \"" . prepareJS($_SESSION['inpValue']['inpOfficeName'])        . "\";";
    echo "\nobjOffice.addr1       = \"" . prepareJS($_SESSION['inpValue']['inpOfficeAddr1'])       . "\";";
    echo "\nobjOffice.addr2       = \"" . prepareJS($_SESSION['inpValue']['inpOfficeAddr2'])       . "\";";
    echo "\nobjOffice.city        = \"" . prepareJS($_SESSION['inpValue']['inpOfficeCity'])        . "\";";
    echo "\nobjOffice.state       = \"" . prepareJS($_SESSION['inpValue']['selOfficeState'])       . "\";";
    echo "\nobjOffice.zip         = \"" . prepareJS($_SESSION['inpValue']['inpOfficeZip'])         . "\";";
    echo "\nobjOffice.stock_level = \"" . prepareJS($_SESSION['inpValue']['selOfficeStockLevel'])  . "\";";
    echo "\nobjOffice.cov_adj_mi  = \"" . prepareJS($_SESSION['inpValue']['inpOfficeCovAdjMi'])    . "\";";
    echo "\nobjOffice.google      = \"" . prepareJS($_SESSION['inpValue']['inpOfficeGoogle'])      . "\";";
    echo "\nobjOffice.lld_name    = \"" . prepareJS($_SESSION['inpValue']['inpOfficeLldName'])     . "\";";
    echo "\nobjOffice.lld_telp    = \"" . prepareJS($_SESSION['inpValue']['inpOfficeLldTelp'])     . "\";";
    echo "\nobjOffice.lld_extp    = \"" . prepareJS($_SESSION['inpValue']['inpOfficeLldExtp'])     . "\";";
    echo "\nobjOffice.lld_telc    = \"" . prepareJS($_SESSION['inpValue']['inpOfficeLldTelc'])     . "\";";
    echo "\nobjOffice.lld_extc    = \"" . prepareJS($_SESSION['inpValue']['inpOfficeLldExtc'])     . "\";";
    echo "\nobjOffice.lld_email   = \"" . prepareJS($_SESSION['inpValue']['inpOfficeLldEmail'])    . "\";";
    echo "\nobjOffice.info        = \"" . prepareJS($_SESSION['inpValue']['inpOfficeInfo'])        . "\";";
    echo "\nobjOffice.is_act      = \"" . (isset($_SESSION['inpValue']['chkOfficeIsAct']) ? 1 : 0) . "\";";
    echo "\nobjOffice.is_field    = \"" . (isset($_SESSION['inpValue']['chkOfficeIsField']) ? 1 : 0) . "\";";
    echo "\nobjOffice.is_eqs      = \"" . (isset($_SESSION['inpValue']['chkOfficeIsEqs']) ? 1 : 0) . "\";";
    echo "\nobjOffice.has_hydroxyl= \"" . (isset($_SESSION['inpValue']['chkOfficeHasHydroxyl']) ? 1 : 0) . "\";";
    echo "\nobjOffice.d_lease_exp = \"" . formatDText(strtotime($_SESSION['inpValue']['inpOfficeDLeaseExp'])) . "\";";
    echo "\nobjOffice.emp_lesr    = \"" . prepareJS($_SESSION['inpValue']['selOfficeEmpLESR'])     . "\";";
    echo "\nobjOffice.tsp_telp        = \"" . prepareJS($_SESSION['inpValue']['inpOfficeTspTelp'])        . "\";";
    echo "\nobjOffice.tsp_permit      = \"" . prepareJS($_SESSION['inpValue']['inpOfficeTspPermit'])      . "\";";
    echo "\nobjOffice.bio_company     = \"" . prepareJS($_SESSION['inpValue']['inpOfficeBioCompany'])     . "\";";
    echo "\nobjOffice.bio_addr1       = \"" . prepareJS($_SESSION['inpValue']['inpOfficeBioAddr1'])       . "\";";
    echo "\nobjOffice.bio_addr2       = \"" . prepareJS($_SESSION['inpValue']['inpOfficeBioAddr2'])       . "\";";
    echo "\nobjOffice.bio_city        = \"" . prepareJS($_SESSION['inpValue']['inpOfficeBioCity'])        . "\";";
    echo "\nobjOffice.bio_state       = \"" . prepareJS($_SESSION['inpValue']['selOfficeBioState'])       . "\";";
    echo "\nobjOffice.bio_zip         = \"" . prepareJS($_SESSION['inpValue']['inpOfficeBioZip'])         . "\";";
    echo "\nobjOffice.bio_permit      = \"" . prepareJS($_SESSION['inpValue']['inpOfficeBioPermit'])      . "\";";

    echo "\narrOfficeOb.push(objOffice);"; // Tack this onto the end of the array of Office objects.

    foreach($_SESSION['errField'] AS $field) {

      echo "\narrOfficeEr.push(\"" . $field    . "\");";
      echo "\ndocument.getElementById(\"" . $field . "\").classList.add(\"err\");";

    }

    // Show the modal dialog box

    if (isset($_SESSION['inpValue']['subAddOffice'])) echo "\nfnAddOffice();";
    if (isset($_SESSION['inpValue']['subChgOffice'])) echo "\nfnChgOffice(" . $_SESSION['inpValue']['reoOfficeID'] . ");";
    echo "\nfnFillOffice(arrOfficeOb.length-1);";

    echo "// --End Hiding Here -->\n";
    echo "\n</script>\n";

  }

// ==================================================================================================== //
// === MODAL === //
// ==================================================================================================== //

  echo "<div class='modal' id='myModalOffice'>";
  echo "<div class='fixed'>";
  echo "<div class='body_stripe'></div>";

  echo "<div class='head'>";
  echo "<div class='content'>";

  echo "<span id='spanOfficeHead'></span>";
  echo "<span style='float:right;'><a href='javascript:fnCloseModalOffice();'><img class='icon16' src='icons/no16.png' alt='Close' title='Close' /></a></span>";

  echo "</div>"; // content
  echo "</div>"; // head

  echo "<span id='spanOfficeMessages'>";
  fnModalMessages();
  echo "</span>";

  echo "<div class='body'>";
  echo "\n<form method='post' name='frmModalOffice' action='modalPRG.php'>";
  echo "<div class='content'>";

  echo "<table class='info' id='tblOffice'>";

  $inpField = "reoOfficeID";
  $inpLabel = "Office ID";
  $defValue = "";
  $defClass = "inp06 reo";
  echo "<tr class='content' id='rowOfficeID'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' value='$defValue' readonly />";
  echo "</td></tr>";

  $inpField = "inpOfficeName";
  $inpLabel = "Name";
  $defValue = "";
  $defClass = "inp16 req";
  $inpHelp  = "Enter the common name / metro area for this office.";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' value='$defValue' maxlength='$maxName' list='off' />";
  echo "&nbsp;<img class='icon16' title='$inpHelp' alt='Help' src='icons/question16.png' />";
  echo "</td></tr>";

  $sqlRegion = "SELECT * FROM region ORDER BY region_id";
  $resRegion = $mysqli->query($sqlRegion);

  $inpField = "selOfficeRegion";
  $inpLabel = "Region";
  $defValue = "";
  $defClass = "inp12 req";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<select class='$defClass' name='$inpField' id='$inpField'>";
  echo "<option value='' selected='selected'>--- Select ---</option>";
  while ($myRegion = $resRegion->fetch_assoc()) {
    echo "<option value='" . $myRegion['region_id'] . "'>" . prepareFrm($myRegion['region_name']) . "</option>";
  }
  echo "</select>";
  echo "</td></tr>";

  $inpField = "inpOfficeAddr1";
  $inpLabel = "Address 1";
  $defValue = "";
  $defClass = "inp24 req";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' value='$defValue' maxlength='$maxAddr' list='off' />";
  echo "</td></tr>";

  $inpField = "inpOfficeAddr2";
  $inpLabel = "Address 2";
  $defValue = "";
  $defClass = "inp24 req";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' value='$defValue' maxlength='$maxAddr' list='off' />";
  echo "</td></tr>";

  $inpField = "inpOfficeCity";
  $inpLabel = "City";
  $defValue = "";
  $defClass = "inp16 req";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' value='$defValue' maxlength='$maxAddr' list='off' />";
  echo "</td></tr>";

  $sqlState = "SELECT * FROM state ORDER BY st_name";
  $resState = $mysqli->query($sqlState) or die('Get state failed');

  $inpField = "selOfficeState";
  $inpLabel = "State/Zip";
  $defValue = "";
  $defClass = "inp10 req";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<select class='$defClass' name='$inpField' id='$inpField'>";
  echo "<option value='' selected='selected'>--- Select ---</option>";
  while ($myState = $resState->fetch_assoc()) {
    echo "<option value='" . prepareFrm($myState['st_id']) . "'>" . prepareFrm($myState['st_name']) . "</option>";
  }
  echo "</select>";

  $inpField = "inpOfficeZip";
  $defValue = "";
  $defClass = "inp06 req";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' value='$defValue' maxlength='$maxZip' />";
  echo "</td></tr>";

  $inpField = "inpOfficeDLeaseExp";
  $inpLabel = "Lease Exp";
  $defClass = "inp12 req";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<input id='$inpField' class='$defClass' type='text' name='$inpField' id='$inpField' maxlength='$maxDate' list='off' />";
  echo "</td></tr>";
  echo "<script>$(function() { $('#".  $inpField . "').datepicker(); }); </script>";

  $sqlSelEmpLESR     = "SELECT emp_id, emp_is_act, emp_fname, emp_lname FROM emp JOIN emp_role ON emp_id = er_emp JOIN role_list ON er_role_code = role_code WHERE role_code IN ('SR') ORDER BY emp_is_act DESC, emp_fname, emp_lname";
  if ($resSelEmpLESR = $mysqli->query($sqlSelEmpLESR)) {

    $inpField = "selOfficeEmpLESR";
    $inpLabel = "LE Sales Rep";
    $defValue = "";
    $defClass = "inp12 req";
    while ($mySelEmpLESR = $resSelEmpLESR->fetch_assoc()) {
      $arrViewEmpActID[$mySelEmpLESR['emp_is_act']][] = $mySelEmpLESR['emp_id'];
      $arrViewEmpIDName[$mySelEmpLESR['emp_id']]      = prepareWeb(substr($mySelEmpLESR['emp_fname'],0,1) . "." . $mySelEmpLESR['emp_lname']);
    }
    echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
    echo "<select class='$defClass' name='$inpField' id='$inpField'>";
    echo "<option value='0' selected='selected'>--- Select ---</option>";
    foreach($arrViewEmpActID AS $act => $arrViewEmpID) {
      echo "<optgroup label='" . $arrIsAct[$act] . "' " . ($act == 0 ? " class='gray'" : "") . ">"; 
      foreach($arrViewEmpID AS $empID) {
        echo "<option value='" . $empID . "'" . ($empID == $defValue ? " selected='selected'" : "") . ">" . $arrViewEmpIDName[$empID] . "</option>";
      } echo "</optgroup>";
    }
    echo "</select>";
    echo "</td></tr>";
  }

  $sqlStockLevel = "SELECT sl_code FROM stock_level ORDER BY sl_code";
  $resStockLevel = $mysqli->query($sqlStockLevel);
  $inpField = "selOfficeStockLevel";
  $inpLabel = "Stock Level";
  $defValue = "";
  $defClass = "inp06 opt";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<select class='$defClass' name='$inpField' id='$inpField'>";
  echo "<option value='' selected='selected'>---</option>";
  while ($myStockLevel = $resStockLevel->fetch_assoc()) {
    echo "<option value='" . prepareFrm($myStockLevel['sl_code']) . "'>" . prepareFrm($myStockLevel['sl_code']) . "</option>";
  }
  echo "</select>";
  echo "</td></tr>";

  $inpField = "inpOfficeCovAdjMi";
  $inpLabel = "Coverage Adjustment";
  $defValue = "";
  $defClass = "inp06 opt";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' value='$defValue' maxlength='$maxName' />";
  echo "</td></tr>";

  $inpField = "inpOfficeGoogle";
  $inpLabel = "Google Review";
  $defValue = "";
  $defClass = "inp24 opt";
  $inpHelp  = "Enter URL for Google Reviews.  Should start with http://";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' value='$defValue' maxlength='$maxURL' list='off' />";
  echo "&nbsp;<img class='icon16' title='$inpHelp' alt='Help' src='icons/question16.png' />";
  echo "</td></tr>";

  $inpField = "inpOfficeLldName";
  $inpLabel = "Landlord Name";
  $defValue = "";
  $defClass = "inp24 opt";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' value='$defValue' maxlength='$maxName' />";
  echo "</td></tr>";

  $inpField = "inpOfficeLldTelp";
  $inpLabel = "Landlord Phone";
  $defValue = "";
  $defClass = "opt inp12";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' value='$defValue' maxlength='$maxTel' />";
  $inpField = "inpOfficeLldExtp";
  $defValue = "";
  $defClass = "opt inp08";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' value='$defValue' maxlength='$maxExt' />";
  echo "</td></tr>";

  $inpField = "inpOfficeLldTelc";
  $inpLabel = "Landlord Cell Phone";
  $defValue = "";
  $defClass = "opt inp12";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' value='$defValue' maxlength='$maxTel' />";
  $inpField = "inpOfficeLldExtc";
  $defValue = "";
  $defClass = "opt inp08";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' value='$defValue' maxlength='$maxExt' />";
  echo "</td></tr>";

  $inpField = "inpOfficeLldEmail";
  $inpLabel = "Landlord Email";
  $defValue = "";
  $defClass = "inp24 opt";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' value='$defValue' maxlength='$maxEmail' />";
  echo "</td></tr>";

  $inpField = "inpOfficeTspTelp";
  $inpLabel = "Transport Phone";
  $defValue = "";
  $defClass = "opt inp12";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' value='$defValue' maxlength='$maxTel' />";
  echo "</td></tr>";

  $inpField = "inpOfficeTspPermit";
  $inpLabel = "Transport Permit";
  $defValue = "";
  $defClass = "inp32 opt";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' value='$defValue' maxlength='$maxPermit' list='off' />";
  echo "</td></tr>";

  $inpField = "inpOfficeBioCompany";
  $inpLabel = "Bio Treatment Co";
  $defValue = "";
  $defClass = "inp24 opt";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' value='$defValue' maxlength='$maxAddr' list='off' />";
  echo "</td></tr>";

  $inpField = "inpOfficeBioAddr1";
  $inpLabel = "Bio Address 1";
  $defValue = "";
  $defClass = "inp24 opt";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' value='$defValue' maxlength='$maxAddr' list='off' />";
  echo "</td></tr>";

  $inpField = "inpOfficeBioAddr2";
  $inpLabel = "Bio Address 2";
  $defValue = "";
  $defClass = "inp24 opt";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' value='$defValue' maxlength='$maxAddr' list='off' />";
  echo "</td></tr>";

  $inpField = "inpOfficeBioCity";
  $inpLabel = "Bio City";
  $defValue = "";
  $defClass = "inp16 opt";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' value='$defValue' maxlength='$maxAddr' list='off' />";
  echo "</td></tr>";

  $sqlState = "SELECT * FROM state ORDER BY st_name";
  $resState = $mysqli->query($sqlState) or die('Get state failed');

  $inpField = "selOfficeBioState";
  $inpLabel = "Bio State/Zip";
  $defValue = "";
  $defClass = "inp10 opt";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<select class='$defClass' name='$inpField' id='$inpField'>";
  echo "<option value='' selected='selected'>--- Select ---</option>";
  while ($myState = $resState->fetch_assoc()) {
    echo "<option value='" . prepareFrm($myState['st_id']) . "'>" . prepareFrm($myState['st_name']) . "</option>";
  }
  echo "</select>";

  $inpField = "inpOfficeBioZip";
  $defValue = "";
  $defClass = "inp06 opt";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' value='$defValue' maxlength='$maxZip' />";
  echo "</td></tr>";

  $inpField = "inpOfficeBioTelp";
  $inpLabel = "Bio Treatment Phone";
  $defValue = "";
  $defClass = "opt inp12";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' value='$defValue' maxlength='$maxTel' />";
  echo "</td></tr>";

  $inpField = "inpOfficeBioPermit";
  $inpLabel = "Bio Treatment Permit";
  $defValue = "";
  $defClass = "inp32 opt";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' value='$defValue' maxlength='$maxPermit' list='off' />";
  echo "</td></tr>";

  $inpField = "inpOfficeInfo";
  $inpLabel = "Additional<br />Information";
  $defValue = "";
  $defClass = "opt";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<textarea class='$defClass' name='$inpField' id='$inpField' rows='6' cols='48'>$defValue</textarea>";
  echo "</td></tr>";

  $inpField = "chkOfficeIsAct";
  $inpLabel = "Active";
  $inpHelp  = "An active office (checked) that is linked to teams may be assigned to jobs.  Uncheck to temporarily hide this office and all associated teams on the Team and Job pages.";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='l' colspan='2'>";
  echo "<input type='checkbox' name='$inpField' id='$inpField' />";
  echo "&nbsp;<img class='icon16' title='$inpHelp' alt='Help' src='icons/question16.png' />";
  echo "</td></tr>";

  $inpField = "chkOfficeIsField";
  $inpLabel = "Field";
  $inpHelp  = "A Field location is managed by a Regional Manager and has employees, teams and trucks.";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='l' colspan='2'>";
  echo "<input type='checkbox' name='$inpField' id='$inpField' />";
  echo "&nbsp;<img class='icon16' title='$inpHelp' alt='Help' src='icons/question16.png' />";
  echo "</td></tr>";

  $inpField = "chkOfficeIsEqs";
  $inpLabel = "EQS";
  $inpHelp  = "Office employees may be dispatched to Environmental Quality jobs.";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='l' colspan='2'>";
  echo "<input type='checkbox' name='$inpField' id='$inpField' />";
  echo "&nbsp;<img class='icon16' title='$inpHelp' alt='Help' src='icons/question16.png' />";
  echo "</td></tr>";

  $inpField = "chkOfficeHasHydroxyl";
  $inpLabel = "Hydroxyl Generator";
  $inpHelp  = "This office has a Hydroxyl Generator (yes/no).";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='l' colspan='2'>";
  echo "<input type='checkbox' name='$inpField' id='$inpField' />";
  echo "&nbsp;<img class='icon16' title='$inpHelp' alt='Help' src='icons/question16.png' />";
  echo "</td></tr>";

  $arrButtonRowOffice[] = "<input class='inp12' type='submit' name='subAddOffice' id='subAddOffice' value='Add' />";
  $arrButtonRowOffice[] = "<input class='inp12' type='submit' name='subChgOffice' id='subChgOffice' value='Save' />";
  $arrButtonRowOffice[] = "<input class='inp12' type='button' name='butConOffice' id='butConOffice' value='Delete' onclick='fnConfirmDelOffice();' />";
  $arrButtonRowOffice[] = "<input class='inp12' type='button' name='butCanOffice' id='butCanOffice' value='Cancel' onclick='fnCloseModalOffice();' />";

  echo "<tr class='content'><td></td><td>" . implode("", $arrButtonRowOffice) . "</td></tr>";

  echo "</table>";

  echo "</div>"; // content
  echo "<span id='spanOfficeConfirm'></span>";
  echo "</form>";
  echo "</div>"; // body

  echo "<div class='body_stripe'></div>";
  echo "</div>"; // fixed
  echo "</div>"; // modal

?>
