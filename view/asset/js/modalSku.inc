<?php

// *************************** QUERY ***************************

  $sqlSelUOM = "SELECT uom_code, uom_dsca FROM uom ORDER BY uom_code";
  $resSelUOM = $mysqli->query($sqlSelUOM);
  $resSelUOMAll = $resSelUOM->fetch_all(MYSQLI_ASSOC);

// *************************** MODAL ***************************

  echo "<div class='modal' id='myModalSku'>";
  echo "<div class='fixed'>";
  echo "<div class='body_stripe'></div>";

  echo "<div class='head'>";
  echo "<div class='content'>";

  echo "<span id='spanSkuHead'></span>";
  echo "<span style='float:right;'><a href='javascript:fnCloseModalSku();'><img class='icon16' src='icons/no16.png' alt='Close' title='Close' /></a></span>";

  echo "</div>"; // content
  echo "</div>"; // head

  echo "<span id='spanSkuMessages'>";
  fnModalMessages();
  echo "</span>";

  echo "<div class='body'>";
  echo "\n<form method='post' name='frmModalSku' action='modalPRG.php'>";
  echo "<div class='content'>";

  echo "<input type='hidden' name='hidSkuID' id='hidSkuID' />";

  echo "<table class='info' id='tblSku' style='display:none;'>";

  $inpField = "reoItemID";
  $inpLabel = "Item";
  $defValue = $valItemID;
  $defClass = "inp16 reo";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' value='$defValue' readonly />";
  echo "</td></tr>";

  $inpField = "reoVendor";
  $inpLabel = "Vendor";
  $defValue = $frmVendorCode;
  $defClass = "inp16 reo";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' value='$defValue' readonly />";
  echo "</td></tr>";

  $inpField = "inpSkuNum";
  $inpLabel = "SKU";
  $defClass = "inp16 req";
  $inpHelp  = "A SKU should be 9 digits.  The first 5 should match the item and the last 4 can be chosen indicate the variation for this SKU.  If there is only one SKU for one Item, then use the Item Number plus 0000.";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' />";
  echo "&nbsp;<img class='icon16' title='$inpHelp' alt='Help' src='icons/question16.png' />";
  echo "</td></tr>";

  $inpField = "inpSkuNama";
  $inpLabel = "Name";
  $defClass = "inp24 req";
  $inpHelp  = "Enter the full text name for this SKU.";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' maxlength='$maxName' list='off' />";
  echo "&nbsp;<img class='icon16' title='$inpHelp' alt='Help' src='icons/question16.png' />";
  echo "</td></tr>";

  $inpField = "inpSkuPart";
  $inpLabel = "Vendor Part";
  $defClass = "inp16 req";
  $inpHelp  = "Enter the vendor part number for this SKU.";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' maxlength='$maxName' list='off' />";
  echo "&nbsp;<img class='icon16' title='$inpHelp' alt='Help' src='icons/question16.png' />";
  echo "</td></tr>";

  $inpField = "selUOM";
  $inpLabel = "Unit of Measure";
  $defClass = "inp12 req";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td>";
  echo "<select class='$defClass' name='$inpField' id='$inpField'>";
  echo "<option value='' selected>--- Select ---</option>";
  foreach ($resSelUOMAll as $mySelUOM) {
    echo "<option value='" . $mySelUOM['uom_code'] . "'>" . prepareWeb($mySelUOM['uom_dsca']) . "</option>"; }
  echo "</select>";
  echo "</td></tr>";

  $inpField = "inpQtyPer";
  $inpLabel = "Qty Per Unit";
  $defClass = "inp12 req";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' maxlength='" . strlen($maxSmallintUn) . "' />";
  echo "</td></tr>";

  $inpField = "inpSkuUPC";
  $inpLabel = "UPC Code";
  $defClass = "inp16 opt";
  $inpHelp  = "Enter the UPC Code for this SKU.";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' maxlength='$maxName' list='off' />";
  echo "&nbsp;<img class='icon16' title='$inpHelp' alt='Help' src='icons/question16.png' />";
  echo "</td></tr>";

  $inpField = "chkIsAggr";
  $inpLabel = "Is Aggregate";
  $inpHelp  = "Create an aggregate SKU to subtotal multiple SKUs for one item.  This item may not be ordered or stocked, however you may define aggregate Stock Levels.  It is recommended to end an Item SKU in 9999.";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='l' colspan='2'>";
  echo "<input type='checkbox' name='$inpField' id='$inpField' onclick='javascript:fnToggleAggrReq();' />";
  echo "&nbsp;<img class='icon16' title='$inpHelp' alt='Help' src='icons/question16.png' />";
  echo "</td></tr>";

  $inpField = "chkIsCons";
  $inpLabel = "Is Job Consumable";
  $inpHelp  = "Allows for this item to have job pricing units of measurement and quantities.";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='l' colspan='2'>";
  echo "<input type='checkbox' name='$inpField' id='$inpField' onclick='javascript:fnToggleConsReq();' />";
  echo "&nbsp;<img class='icon16' title='$inpHelp' alt='Help' src='icons/question16.png' />";
  echo "</td></tr>";

  $inpField = "selUOMJif";
  $inpLabel = "Unit of Measure (Job)";
  $defClass = "inp12 opt";
  $inpHelp  = "Is required if this item is job consumable.";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td>";
  echo "<select class='$defClass' name='$inpField' id='$inpField'>";
  echo "<option value='' selected>--- Select ---</option>";
  foreach ($resSelUOMAll as $mySelUOM) {
    echo "<option value='" . $mySelUOM['uom_code'] . "'>" . prepareWeb($mySelUOM['uom_dsca']) . "</option>"; }
  echo "</select>";
  echo "&nbsp;<img class='icon16' title='$inpHelp' alt='Help' src='icons/question16.png' />";
  echo "</td></tr>";

  $inpField = "inpQtyPerJif";
  $inpLabel = "Qty Per Unit (Job)";
  $defClass = "inp12 opt";
  $inpHelp  = "Is required if this item is job consumable.";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' maxlength='" . strlen($maxSmallintUn) . "' />";
  echo "&nbsp;<img class='icon16' title='$inpHelp' alt='Help' src='icons/question16.png' />";
  echo "</td></tr>";

  $arrButtonRowSku[] = "<input class='inp12' type='submit' name='subAddSku' id='subAddSku' value='Add' />";
  $arrButtonRowSku[] = "<input class='inp12' type='submit' name='subChgSku' id='subChgSku' value='Save' />";
  $arrButtonRowSku[] = "<input class='inp12' type='button' name='butConSku' id='butConSku' value='Delete' onclick='fnConfirmDelSku();' />";
  $arrButtonRowSku[] = "<input class='inp12' type='button' name='butCanSku' id='butCanSku' value='Cancel' onclick='fnCloseModalSku();' />";

  echo "<tr class='content'><td></td><td>" . implode("", $arrButtonRowSku) . "</td></tr>";

  echo "</table>";

  echo "</div>"; // content
  echo "<span id='spanSkuConfirm'></span>";
  echo "</form>";
  echo "</div>"; // body

  echo "<div class='body_stripe'></div>";
  echo "</div>"; // fixed
  echo "</div>"; // modal

// *************************** LOAD ***************************

  if (isset($_SESSION['inpValue']['subAddSku']) || isset($_SESSION['inpValue']['subChgSku'])) {

    echo "\n<script language=\"JavaScript\">";
    echo "\n<!-- Hide the script from old browsers --";

    echo "\nvar objSku = {};"; // Create a new object to hold the posted values.

    if (isset($_SESSION['inpValue']['hidSkuID'])) {
      echo "\narrSkuID.push(" . $_SESSION['inpValue']['hidSkuID'] . ");";
      echo "\nobjSku.id     = \"" . $_SESSION['inpValue']['hidSkuID'] . "\";";
    }
    echo "\nobjSku.item     = \"" . $_SESSION['inpValue']['reoItemID']             . "\";";
    echo "\nobjSku.num      = \"" . prepareJS($_SESSION['inpValue']['inpSkuNum'])  . "\";";
    echo "\nobjSku.uom_code = \"" . prepareJS($_SESSION['inpValue']['selUOM'])     . "\";";
    echo "\nobjSku.qty_per  = \"" . $_SESSION['inpValue']['inpQtyPer']             . "\";";
    echo "\nobjSku.nama     = \"" . prepareJS($_SESSION['inpValue']['inpSkuNama']) . "\";";
    echo "\nobjSku.part     = \"" . prepareJS($_SESSION['inpValue']['inpSkuPart']) . "\";";
    echo "\nobjSku.upc      = \"" . prepareJS($_SESSION['inpValue']['inpSkuUPC'])  . "\";";

    echo "\nobjSku.is_aggr         = \"" . (isset($_SESSION['inpValue']['chkIsAggr']) ? 1 : 0) . "\";";
    echo "\nobjSku.is_consumable   = \"" . (isset($_SESSION['inpValue']['chkIsCons']) ? 1 : 0) . "\";";
    echo "\nobjSku.uom_code_jif    = \"" . prepareJS($_SESSION['inpValue']['selUOMJif'])       . "\";";
    echo "\nobjSku.uom_qty_per_jif = \"" . $_SESSION['inpValue']['inpQtyPerJif']               . "\";";

    foreach($_SESSION['errField'] AS $field) {

      echo "\narrSkuEr.push(\"" . $field    . "\");";
      echo "\ndocument.getElementById(\"" . $field . "\").classList.add(\"err\");";

    }

    echo "\narrSkuOb.push(objSku);"; // Tack this onto the end of the array of SKU objects.

    if (isset($_SESSION['inpValue']['subAddSku'])) echo "\nfnAddSku();";
    if (isset($_SESSION['inpValue']['subChgSku'])) echo "\nfnChgSku(" . $_SESSION['inpValue']['hidSkuID'] . ");";
    echo "\nfnFillSku(arrSkuOb.length-1);";

    echo "// --End Hiding Here -->\n";
    echo "\n</script>\n";
  }

?>