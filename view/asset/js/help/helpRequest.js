import {HelpPage} from "./helpPage.js"
import {Util} from "../util.js"

const PAGE_STATE = {
  SUBMIT: 0,
  COMPLETE: 1,
  FAIL: 2
}

export class HelpRequest extends HelpPage{
  constructor(controller) {
    super(controller, 'request')
    
    this.dom = Util.parseDOM(this.baseElement)
    
    //populate ticket types
    for (const ticketTypesKey in window.ticketTypes) {
      const element = window.ticketTypes[ticketTypesKey]
      
      let opt = document.createElement('option')
      opt.value = element.ticket_type_id
      opt.innerText = element.ticket_type_title
      this.dom.type.appendChild(opt)
    }
    
    //setup select button switch
    this.dom.type.addEventListener('change', (event => {
      //get the ticket type
      for (const ticketTypesKey in window.ticketTypes) {
        const item = window.ticketTypes[ticketTypesKey]
        if(item.ticket_type_id == event.target.value){
          this.dom.anonArea.innerHTML = item.ticket_type_dsca
//          this.dom.anon.style.display = item.ticket_type_anonymous == 1 ? '' : 'none'
        }
      }
    }).bind(this))
    
    //setup submit button
    this.dom.submitButton.addEventListener('click', ((event) => {
      event.target.disabled = true
      this.submit()
    }))
    
    //setup back button
    let backButtons = this.baseElement.querySelectorAll('[name=backButton]')
    console.log(backButtons)
    for (const backButtonsKey in backButtons) {
      if(backButtons.hasOwnProperty(backButtonsKey)){
        backButtons[backButtonsKey].addEventListener('click', (() => {
          this.setState(PAGE_STATE.SUBMIT)
        }).bind(this))
      }
    }
    
    //set initial page state
    this.setState(PAGE_STATE.SUBMIT)
  }
  
  /**
   * Sets the state of the page
   * @param {number} state
   */
  setState(state){
    this.pageState = state
    
    this.dom.submitSection.style.display = state == PAGE_STATE.SUBMIT ? '' : 'none'
    this.dom.completeSection.style.display = state == PAGE_STATE.COMPLETE ? '' : 'none'
    this.dom.failSection.style.display = state == PAGE_STATE.FAIL ? '' : 'none'
  }
  
  /**
   * Gets the data, or null if there are errors in the data
   * @returns {{ticket_type: *, ticket_msg: *}|null}
   */
  getData(){
    let data = {
      ticket_type: this.dom.type.value,
      ticket_msg: this.dom.msg.value
    }
    
    let numErrors = 0
    if(data.ticket_type == ''){
      numErrors++
      this.dom.type.classList.add('error')
    }
    
    if(data.ticket_msg.length < 3){
      numErrors++
      this.dom.msg.classList.add('error')
    }
    
    if(numErrors == 0) return data
    else return null
  }
  
  /**
   * Resets the form to its default state
   */
  clearForm(){
    this.dom.type.classList.remove('error')
    this.dom.type.value = ''
    
    this.dom.msg.classList.remove('error')
    this.dom.msg.value =''
    
//    this.dom.anon.style.display = 'none'
  }
  
  submit(){
    let data = this.getData()
    if(data == null){
      this.dom.submitButton.disabled = false
      return
    }
    
    fetch('helpPRG.php', {
      method: 'post',
      headers: {'Content-Type': 'application/json;charset=utf8'},
      body: JSON.stringify({
        action: 'submitTicket',
        ticket_type: data.ticket_type,
        ticket_msg: data.ticket_msg
      })
    })
      .then(res => res.json())
      .then(body => {
        this.dom.submitButton.disabled = false //re-enable save button
        
        if(body.ok){
          this.clearForm()
          this.setState(PAGE_STATE.COMPLETE)
        }else{
          this.clearForm()
          this.setState(PAGE_STATE.FAIL)
        }
      })
  }
}