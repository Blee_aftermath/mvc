import {HelpRequest} from "./helpRequest.js"

/**
 * Handles functionality of the help page
 */
export class HelpController {
  constructor() {
    //get the page base element
    this.baseElement = document.querySelector('.body_fixed')
    
    //create tab objects
    this.tabs = {
      request: new HelpRequest(this)
    }
    
    //get the currently selected tab from the url
    let selected = window.location.pathname.split('/')
    if(selected.length <= 2) this.setSelectedTab('request') //TODO update set call
    else this.setSelectedTab(selected[2])
  }
  
  /**
   * Gets the currently selected tab
   * @returns {HelpPage}
   */
  getSelectedTab(){
    return this.tabs[this.selectedTab]
  }
  
  /**
   * Sets the currently selected tab
   * @param tab
   */
  setSelectedTab(tab){
    if(this.tabs[tab] !== undefined) tab = 'request' //set tab to default if
    
    this.selectedTab = tab
    
    //show/hide tabs
    for (const tabsKey in this.tabs) {
      const element = this.tabs[tabsKey]
      
      //show/hide body content
      if(tabsKey == tab) element.show()
      else element.hide()
    }
    
    //update the url
    window.history.pushState('', '', `/help.php?${tab}`)
  }
}