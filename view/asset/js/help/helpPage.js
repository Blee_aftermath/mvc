/**
 * Base class for help pages. Handles showing/hiding the page
 * @author Jake Cirino
 */
export class HelpPage {
  /**
   *
   * @param {HTMLElement} baseElement
   * @param {boolean} [visible]
   */
  constructor(controller, tag, visible = false) {
    this.controller = controller
    this.baseElement = controller.baseElement.querySelector(`[tabcontent=${tag}]`)
    this.tabElement = controller.baseElement.querySelector(`[tab=${tag}]`)
    this.setVisible(visible)
  }
  
  /**
   * Shows this page
   */
  show(){
    this.setVisible(true)
  }
  
  /**
   * Hides this page
   */
  hide(){
    this.setVisible(false)
  }
  
  /**
   * Sets whether or not the page is visible
   * @param {boolean} visible
   */
  setVisible(visible){
    this._visible = visible
    this.baseElement.style.display = visible ? '' : 'none'

    if(visible) this.tabElement.classList.add('alert')
    else this.tabElement.classList.remove('alert')
  }
}