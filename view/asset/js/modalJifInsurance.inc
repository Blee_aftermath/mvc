<?php
// ==================================================================================================== //
// === FUNCTIONS === //
// ==================================================================================================== //

function fnFillInsJS($myRes) {

    echo "\n\n<script language=\"JavaScript\">";
    echo "\n<!-- Hide the script from old browsers --";

    echo "\nvar objIns = {};"; // Object of Job fields.  Can also = new Object();

    echo "\nobjIns.id           = \"" . $myRes['job_id']                         . "\";";
    echo "\nobjIns.company      = \"" . $myRes['insco_id']                       . "\";";
    echo "\nobjIns.claim        = \"" . $myRes['job_pmt_claim']                  . "\";";
    echo "\nobjIns.policy       = \"" . $myRes['job_pmt_policy']                 . "\";";
    echo "\nobjIns.insured      = \"" . $myRes['job_pmt_ins_name']               . "\";";
    echo "\nobjIns.deductable   = \"" . $myRes['job_pmt_deductible']             . "\";";
    echo "\nobjIns.dob          = \"" . prepareJS($myRes['job_pmt_insured_dob']) . "\";";
    echo "\nobjIns.adjuster     = \"" . prepareJS($myRes['con_id']) . "\";";

    echo "\narrInsID.push(" . $myRes['job_id'] . ");";
    echo "\narrInsOb.push(objIns);"; // Push this object into the array of Objects
    echo "\n// --End Hiding Here -->";
    echo "\n</script>\n";

  }

  function fnFillInsPost() {
    echo "\n<script language=\"JavaScript\">";
    echo "\n<!-- Hide the script from old browsers --";

    echo "\nvar objIns = {};"; // Create a new object to hold the posted values.

    if (isset($_SESSION['inpValue']['hidJobID'])) {
      echo "\narrInsID.push(\"" . prepareJS($_SESSION['inpValue']['hidJobID']) . "\");";
      echo "\nobjIns.id     = \"" . prepareJS($_SESSION['inpValue']['hidJobID']) . "\";";
    }

    echo "\nobjIns.company       = \"" . prepareJS($_SESSION['inpValue']['selInsCo'])        . "\";";
    echo "\nobjIns.claim         = \"" . prepareJS($_SESSION['inpValue']['inpClaimNum'])     . "\";";
    echo "\nobjIns.policy        = \"" . prepareJS($_SESSION['inpValue']['inpPolicyNum'])    . "\";";
    echo "\nobjIns.insured       = \"" . prepareJS($_SESSION['inpValue']['inpInsuredName'])  . "\";";
    echo "\nobjIns.deductable    = \"" . prepareJS($_SESSION['inpValue']['inpDeductable'])   . "\";";
    echo "\nobjIns.dob           = \"" . prepareJS($_SESSION['inpValue']['inpInsuredDob'])   . "\";";
    echo "\nobjIns.adjuster      = \"" . prepareJS($_SESSION['inpValue']['selAdjuster'])     . "\";";
    

    echo "\narrInsOb.push(objIns);"; // Track this onto the end of the array of Insurance objects.
    echo "console.log(arrInsOb)";

    foreach($_SESSION['errField'] AS $field) {

      echo "\narrInsEr.push(\"" . $field    . "\");";
      echo "\ndocument.getElementById(\"" . $field . "\").classList.add(\"err\");";

    }

    // Show the modal dialog box

    if (isset($_SESSION['inpValue']['subChgIns'])) echo "\nfnChgInsurance(" . $_SESSION['inpValue']['hidJobID'] . ");";
    echo "\nfnFillInsurance(arrInsOb.length-1);";

    echo "// --End Hiding Here -->\n";
    echo "\n</script>\n";

  }
// ==================================================================================================== //
// === MODAL === //
// ==================================================================================================== //
  echo "<div class='modal' id='myModalInsurance'>";
  echo "<div class='fixed'>";
  echo "<div class='body_stripe'></div>";

  echo "<div class='head'>";
  echo "<div class='content'>";

  echo "Insurance Information <span id='spanAddressID'></span>";
  echo "<span style='float:right;'><a href='javascript:fnCloseModalInsurance();'><img class='icon16' src='icons/no16.png' alt='Close' title='Close' /></a></span>";

  echo "</div>"; // content
  echo "</div>"; // head

  echo "<span id='spanInsMessages'>";
  fnModalMessagesBox('Insurance');
  echo "</span>";

  echo "<div class='body'>";
  echo "\n<form method='post' name='frmInsurance' action='jifPRG.php'>";
  echo "<div class='content'>";
 
  if (isset($valJobID)) {
    $inpField = "hidJobID";
    $defValue = $valJobID;
    echo "<input type='hidden' name='$inpField' id='$inpField' value='" . $defValue . "' />";
  }
  echo "<table class='info'>";

  $defValue = ""; // set by JS for all fields in the modal table
  $checked  = ""; // set by JS for all fields in the modal table


  $sqlSelInsCo = "SELECT * FROM insco JOIN insco_group ON insco_group = insco_group_id ORDER BY insco_name";
  $resSelInsCo = $mysqli->query($sqlSelInsCo);

  $inpField = "selInsCo";
  $defValue = "";
  $defClass = "req";
  echo "<tr class='content'><td class='l'><label for='$inpField'>Company / Carrier: </label></td>";
  echo "<td class='r'><select class='$defClass' name='$inpField' id='$inpField'>";
  echo "<option value='' selected='selected'>--- Select Carrier ---</option>";
  while ($mySelInsCo = $resSelInsCo->fetch_assoc()) {
    echo "<option style='background-color:#" . $mySelInsCo['insco_group_color2'] . "' value='" . $mySelInsCo['insco_id'] . "'";
    if ($mySelInsCo['insco_id'] == $defValue) { echo " selected='selected'"; }
    echo ">" . prepareWeb($mySelInsCo['insco_name']) . "</option>";
  }
  echo "</select>";
  echo "<a href='insurance.php'><img class='icon16' alt='View' title='View' src='icons/view16.png' /></a>";
  echo "</td></tr>";

  $inpField = "inpInsuredName";
  $defValue = "";
  $defClass = "opt";
  echo "<tr class='content'><td class='l'>Insured Name :</td>";
  echo "<td class='r'><input class='" . $defClass . "' type='text' name='" . $inpField . "' id='" . $inpField . "' value='" . $defValue . "' maxlength='$maxName' />";
  echo "</td></tr>";   

  
  echo "<tr class='content'><td class='l'><label for='$inpField'>Select Adjuster: </label></td>";

  $sqlAdjuster =  "SELECT * FROM contact LEFT JOIN insco ON con_insco = insco_id";
  $sqlAdjuster .= " WHERE con_insco = 1";
    if (isset($valPmtInsCo))  $sqlAdjuster .= " OR con_insco = $valPmtInsCo";
    if (isset($valMastInsCo)) $sqlAdjuster .= " OR con_insco = $valMastInsCo";
    $sqlAdjuster .= " ORDER BY insco_name, con_fname, con_lname";
    $resAdjuster = $mysqli->query($sqlAdjuster);

  $sqlJcPri ="select jc_con from job_con where jc_job = '$valJobID' and jc_con_pri=1";
  $resultJcPri = $mysqli->query($sqlJcPri);
  $numJcPri = $resultJcPri->num_rows;
  if($numJcPri > 0) {
    while ($myJcPri = $resultJcPri->fetch_assoc()) {
        $jc_con = $myJcPri['jc_con'];
        $defValue = $jc_con;
    }
  }
  $inpField = "selAdjuster";
  $defClass = "opt";
  $defValue = "";

  echo "<td class='r'><select class='$defClass' name='$inpField' id='$inpField'>";
  echo "<option value='' selected='selected'>--- Select Adjuster ---</option>";
  while ($myAdjuster = $resAdjuster->fetch_assoc()) {
        echo "<option value='" . $myAdjuster['con_id'] . "'";
        if ($myAdjuster['con_id'] == $defValue) { echo " selected='selected'"; }
        echo ">" . prepareWeb($myAdjuster['insco_name']) . " (" . prepareWeb($myAdjuster['con_fname'] . " " . $myAdjuster['con_lname']) . ")</option>";
     }
  echo "</select>";
  echo "</td></tr>";
  
  $inpField = "inpInsuredDob";
  $inpLabel = "Insured DOB:";
  $defValue = "";
  $defClass = "opt inp14";

  echo "<tr class='content'><td class='l'>$inpLabel</td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' value='$defValue' autocomplete='off'/>";
  echo "<script>$(function() { $('#inpInsuredDob').datepicker(); }); </script>";

  $inpField = "inpPolicyNum";
  $defValue = "";
  $defClass = "opt";
  echo "<tr class='content'><td class='l'>Policy #: </td>";
  echo "<td class='r'><input class='" . $defClass . "' type='text' id='" . $inpField . "' name='" . $inpField . "' value='" . $defValue . "' maxlength='$maxName' />";
  echo "</td></tr>";

  echo "<tr class='content'><td class='l'>Claim #: </td>";
  $inpField = "inpClaimNum";
  $defValue = "";
  $defClass = "opt";
  echo "<td class='r'><input class='" . $defClass . "' type='text' id='" . $inpField . "' name='" . $inpField . "' value='" . $defValue . "' maxlength='$maxName' />";
  echo "</td></tr>";

  echo "<tr class='content'><td class='l'>Deductable ($): </td>";
  $inpField = "inpDeductable";
  $defValue = "";
  $defClass = "opt";
  echo "<td class='r'><input class='" . $defClass . "' type='text' id='" . $inpField . "' name='" . $inpField . "' value='" . $defValue . "' maxlength='$maxName' />";
  echo "</td></tr>";

  $arrButtonRow[] = "<input class='inp12' type='submit' name='subChgIns' id='subChgIns' value='Save' />";
  $arrButtonRow[] = "<input class='inp08' type='button' name='subCan' id='subCan' value='Cancel' onclick='fnCloseModalInsurance();' />";

  echo "<tr><td></td><td>" . implode("", $arrButtonRow) . "</td></tr>";

  echo "</table>";

  echo "</div>"; // content
  echo "<span id='spanConfirm'></span>";
  echo "</form>";
  echo "</div>"; // body

  echo "<div class='body_stripe'></div>";
  echo "</div>"; // fixed
  echo "</div>"; // modal

 unset($arrButtonRow);
  

?>