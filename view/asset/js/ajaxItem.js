var xmlhttp
var errmsg = "Your browser does not support XMLHTTP.";

function GetXmlHttpObject()
{
if (window.XMLHttpRequest)
  {
  // code for IE7+, Firefox, Chrome, Opera, Safari
  return new XMLHttpRequest();
  }
if (window.ActiveXObject)
  {
  // code for IE6, IE5
  return new ActiveXObject("Microsoft.XMLHTTP");
  }
return null;
}

// **************************************** LinkSkuAval ****************************************

function ajaxSkuAval(strAction, strSku, strAval)
{
//alert("strAction="+strAction+"  strSku="+strSku+"  strAval="+strAval);

xmlhttp=GetXmlHttpObject();

if (xmlhttp==null) { alert(errmsg); return; }

var url="scripts/ajaxItem.php";
url=url+"?action="+strAction;
url=url+"&sku="+strSku;
url=url+"&aval="+strAval;
url=url+"&sid="+Math.random();

//alert(url);

var strError1 = "<p class='error'>Error description here.</p><br />";
var strError2 = "<p class='error'>Error description here.</p><br />";

xmlhttp.onreadystatechange=function()
{
  if (xmlhttp.readyState==4) {
    if (xmlhttp.responseText=="error1") {
      document.getElementById("spanErr").innerHTML=strError1;
    } else if (xmlhttp.responseText=="error2") {
      document.getElementById("spanErr").innerHTML=strError2;
    } else {
      document.getElementById("spanLink").innerHTML=xmlhttp.responseText;
    }
  }
}
xmlhttp.open("GET",url,true);    // METHOD, URL, ASYNCHRONOUSLY=TRUE (CONTINUE EXECUTION AFTER SENDING)
xmlhttp.send(null);              // SENDS THE REQUEST TO THE SERVER
}

// **************************************** AjaxSaveOrder ****************************************

function ajaxSaveOrder(attrID)
{
var strReturn = "";
var serialized = "";
attrIndex = arrAttrID.indexOf(attrID);
for(var i = 0; i < arrAvalOb[attrIndex].length; i++) {
  strReturn += arrAvalOb[attrIndex][i].avalID + "=" + i + ", ";
}

alert(strReturn);

xmlhttp=GetXmlHttpObject();

if (xmlhttp==null) { alert(errmsg); return; }

var url="scripts/ajaxItem.php";
url=url+"?action="+strAction;
url=url+"&emp="+strEmp;
url=url+"&role="+strRole;
url=url+"&sid="+Math.random();

//alert(url);

var strErrorItem = "<p class='error'>Items exist, cannot remove Type.</p><br />";
var strErrorCat  = "<p class='error'>Categories exist, cannot remove Type.</p><br />";

xmlhttp.onreadystatechange=function()
{
  if (xmlhttp.readyState==4) {
    if (xmlhttp.responseText=="errorItem") {
      document.getElementById("spanErrTyp").innerHTML=strErrorItem;
    } else if (xmlhttp.responseText=="errorCat") {
      document.getElementById("spanErrTyp").innerHTML=strErrorCat;
    } else {
      document.getElementById("spanLinked").innerHTML=xmlhttp.responseText;
    }
  }
}
xmlhttp.open("GET",url,true);    // METHOD, URL, ASYNCHRONOUSLY=TRUE (CONTINUE EXECUTION AFTER SENDING)
xmlhttp.send(null);              // SENDS THE REQUEST TO THE SERVER
}
