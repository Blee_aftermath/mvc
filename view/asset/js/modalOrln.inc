<?php

// ==================================================================================================== //
// === MODAL === //
// ==================================================================================================== //

  echo "<div class='modal' id='myModalOrln'>";
  echo "<div class='fixed'>";
  echo "<div class='body_stripe'></div>";

  echo "<div class='head'>";
  echo "<div class='content'>";

  echo "<span id='spanOrlnHead'></span>";
  echo "<span style='float:right;'><a href='javascript:fnCloseModalOrln();'><img class='icon16' src='icons/no16.png' alt='Close' title='Close' /></a></span>";

  echo "</div>"; // content
  echo "</div>"; // head

  echo "<span id='spanOrlnMessages'>";
  fnModalMessages();
  echo "</span>";

  echo "<div class='body'>";
  echo "\n<form method='post' name='frmModalOrln' action='modalPRG.php'>";
  echo "\n<input type='hidden' name='hidOrderID' value='$valOrderID' />";
  echo "\n<input type='hidden' name='hidOrlnID' id='hidOrlnID' />";

  echo "<div class='content'>";
  echo "<table class='info' id='tblOrln'>";

  $sqlSku   = "SELECT * FROM sku LEFT JOIN item ON sku_item = item_id LEFT JOIN item_type ON item_typ_code = ityp_code WHERE ityp_is_orderable = 1 ORDER BY sku_nama";
  $resSku   = $mysqli->query($sqlSku);

  $inpLabel = "SKU Number";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  while ($mySku = $resSku->fetch_assoc()) {
    $inpFieldNm = "radOrlnSku[]";
    $inpFieldID = "radOrlnSku_" . $mySku['sku_num'];
    $defClass = "req";
    echo "<input type='radio' name='$inpFieldNm' id='$inpFieldID' /> " . prepareFrm($mySku['sku_nama']);
  }
  echo "</td></tr>";

  $inpField = "inpOrlnQtyReq";
  $inpLabel = "Qty Requested";
  $defClass = "req";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' maxlength='$maxTinyintUn' list='off' />";
  echo "</td></tr>";

  $arrButtonRowOrln[] = "<input class='inp12' type='submit' name='subAddOrln' id='subAddOrln' value='Add' />";
  $arrButtonRowOrln[] = "<input class='inp12' type='submit' name='subChgOrln' id='subChgOrln' value='Save' />";
  $arrButtonRowOrln[] = "<input class='inp12' type='button' name='butConOrln' id='butConOrln' value='Delete' onclick='fnConfirmDelOrln();' />";
  $arrButtonRowOrln[] = "<input class='inp12' type='button' name='butCanOrln' id='butCanOrln' value='Cancel' onclick='fnCloseModalOrln();' />";

  echo "<tr class='content'><td></td><td>" . implode("", $arrButtonRowOrln) . "</td></tr>";

  echo "</table>";

  echo "</div>"; // content
  echo "<span id='spanOrlnConfirm'></span>";
  echo "</form>";
  echo "</div>"; // body

  echo "<div class='body_stripe'></div>";
  echo "</div>"; // fixed
  echo "</div>"; // modal

?>