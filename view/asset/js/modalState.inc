<?php
  echo "<div class='modal modal-close' id='modalStateForm' title='Edit State'>\n";
  echo "<form action='statePRG.php' method='post'>\n";

  $inpField = "subEditState";
  echo "<input type='hidden' name='$inpField' />\n";

  echo "<table class='info'>\n";

  $inpField = "inpStID";
  $inpLabel = "State ID";
  $defClass = "inp06 reo";
  echo "<tr class='content'>";
  echo "<td><label for='$inpField'>$inpLabel:</label></td>";
  echo "<td class='r'><input class='$defClass' type='text' name='$inpField' id='$inpField' readonly /></td>";
  echo "</tr>\n";

  $sqlRegion = "SELECT * FROM region ORDER BY region_id";
  $resRegion = $mysqli->query($sqlRegion);

  $inpField = "selStReg";
  $inpLabel = "Region";
  $defClass = "inp12 req";
  echo "<tr class='content'>";
  echo "<td><label for='$inpField'>$inpLabel:</label></td>";
  echo "<td class='r'>";
  echo "<select class='$defClass' name='$inpField' id='$inpField'>";
  echo "<option value=''>--- Select ---</option>";
  while ($myRegion = $resRegion->fetch_assoc()) {
    echo "<option value='" . $myRegion['region_id'] . "'>" . prepareFrm($myRegion['region_name']) . "</option>";
  }
  echo "</select>";
  echo "</td>";
  echo "</tr>\n";

  $sqlEmp  = "SELECT emp_id, emp_is_act, CONCAT(SUBSTR(emp_fname,1,1), '. ',emp_lname) as emp_name, role_code FROM emp";
  $sqlEmp .= " JOIN emp_role ON emp_id = er_emp";
  $sqlEmp .= " JOIN role_list ON er_role_code = role_code WHERE 1";
  $sqlEmp .= " AND role_code IN ('SR','FUN')";
  $sqlEmp .= " ORDER BY emp_is_act DESC, emp_fname, emp_lname";
  $resEmp = $mysqli->query($sqlEmp);

  $arrSelMkgEmp = ['ACTIVE' => [], 'INACTIVE' => []];
  $arrSelFunEmp = ['ACTIVE' => [], 'INACTIVE' => []];
  while ($res = $resEmp->fetch_assoc()) {
    $label = $res['emp_is_act'] == 1 ? 'ACTIVE' : 'INACTIVE';
    $res['role_code'] == 'SR'
      ? array_push($arrSelMkgEmp[$label], $res)
      : array_push($arrSelFunEmp[$label], $res);
  }

  $inpField = "selStMkt";
  $inpLabel = "Marketing Rep";
  $defClass = "inp12 req";
  echo "<tr class='content'>";
  echo "<td><label for='$inpField'>$inpLabel:</label></td>";
  echo "<td class='r'>";
  echo "<select class='$defClass' name='$inpField' id='$inpField'>";
  echo "<option value=''>--- Select ---</option>";
  foreach($arrSelMkgEmp as $label => $arrEmp) {
    if (sizeof($arrEmp)) {
      echo "<optgroup label='$label'>";
      foreach($arrEmp as $myEmp) {
        echo "<option value='" . $myEmp['emp_id'] . "'";
        if ($myEmp['emp_is_act'] == 0) echo " style='background-color:#ccc;'";
        echo ">" . prepareFrm($myEmp['emp_name']) . "</option>";
      }
    echo "</optgroup>";
    }
  }
  echo "</select>";

  echo "<label style='display: inline-block; padding-left: 8px;'>Also save to: Counties <input type='checkbox' name='inpSaveCntySR' /></label> | <label>Offices <input type='checkbox' name='inpSaveOfficeSR' /></label>";

  echo "</td>";
  echo "</tr>\n";

  $inpField = "selStFun";
  $inpLabel = "Funeral Home Rep";
  $defClass = "inp12 req";
  echo "<tr class='content'>";
  echo "<td><label for='$inpField'>$inpLabel:</label></td>";
  echo "<td class='r'>";
  echo "<select class='$defClass' name='$inpField' id='$inpField'>";
  echo "<option value=''>--- Select ---</option>";
  foreach($arrSelFunEmp as $label => $arrEmp) {
    if (sizeof($arrEmp)) {
      echo "<optgroup label='$label'>";
      foreach($arrEmp as $myEmp) {
        echo "<option value='" . $myEmp['emp_id'] . "'";
        if ($myEmp['emp_is_act'] == 0) echo " style='background-color:#ccc;'";
        echo ">" . prepareFrm($myEmp['emp_name']) . "</option>";
      }
      echo "</optgroup>";
    }
  }
  echo "</select>";
  echo "</td>";
  echo "</tr>\n";

  $sqlGSA = "SELECT gsa_id, gsa_buyer_title1, gsa_cancel_dsca FROM gsa ORDER BY gsa_id";
  $resGSA = $mysqli->query($sqlGSA);
  $arrGSA = $resGSA->fetch_all(MYSQLI_ASSOC);

  $inpField = "selStGSAB";
  $inpLabel = "GSA Buyer";
  $defClass = "inp12 req";
  echo "<tr class='content'>";
  echo "<td><label for='$inpField'>$inpLabel:</label></td>";
  echo "<td class='r'>";
  echo "<select class='$defClass' name='$inpField' id='$inpField'>";
  echo "<option value=''>--- Select ---</option>";
  foreach ($arrGSA as $myGSA) {
    if ($myGSA['gsa_buyer_title1']) {
      echo "<option value='" . $myGSA['gsa_id'] . "'>" . $myGSA['gsa_id'] . "</option>";
    }
  }
  echo "</select>";
  echo "</td>";
  echo "</tr>\n";

  $inpField = "selStGSAC";
  $inpLabel = "GSA Cancel";
  $defClass = "inp12 req";
  echo "<tr class='content'>";
  echo "<td><label for='$inpField'>$inpLabel:</label></td>";
  echo "<td class='r'>";
  echo "<select class='$defClass' name='$inpField' id='$inpField'>";
  echo "<option value=''>--- Select ---</option>";
  foreach ($arrGSA as $myGSA) {
    if ($myGSA['gsa_cancel_dsca']) {
      echo "<option value='" . $myGSA['gsa_id'] . "'>" . $myGSA['gsa_id'] . "</option>";
    }
  }
  echo "</select>";
  echo "</td>";
  echo "</tr>\n";

  $inpField = "inpStTxSupRes";
  $inpLabel = "Tax - Supplies Res.";
  $defClass = "inp06 opt";
  echo "<tr class='content'>";
  echo "<td><label for='$inpField'>$inpLabel:</label></td>";
  echo "<td class='r'><input class='$defClass' type='text' name='$inpField' id='$inpField' /></td>";
  echo "</tr>\n";

  $inpField = "inpStTxSupBus";
  $inpLabel = "Tax - Supplies Bus.";
  $defClass = "inp06 opt";
  echo "<tr class='content'>";
  echo "<td><label for='$inpField'>$inpLabel:</label></td>";
  echo "<td class='r'><input class='$defClass' type='text' name='$inpField' id='$inpField' /></td>";
  echo "</tr>\n";

  $inpField = "inpStTxLabRes";
  $inpLabel = "Tax - Labor Res.";
  $defClass = "inp06 opt";
  echo "<tr class='content'>";
  echo "<td><label for='$inpField'>$inpLabel:</label></td>";
  echo "<td class='r'><input class='$defClass' type='text' name='$inpField' id='$inpField' /></td>";
  echo "</tr>\n";

  $inpField = "inpStTxLabBus";
  $inpLabel = "Tax - Labor Bus.";
  $defClass = "inp06 opt";
  echo "<tr class='content'>";
  echo "<td><label for='$inpField'>$inpLabel:</label></td>";
  echo "<td class='r'><input class='$defClass' type='text' name='$inpField' id='$inpField' /></td>";
  echo "</tr>\n";

  $arrButtonRowState[] = "<input class='inp12' type='submit' value='Save' />";
  $arrButtonRowState[] = "<input class='inp12 modal-close' type='button' value='Cancel' />";
  echo "<tr class='content'><td></td><td>" . implode("", $arrButtonRowState) . "</td></tr>";

  echo "</table>\n";
  echo "</form>\n";
  echo "</div>\n";
?>