
var arrTruckOb   = []; // Array of Truck objects
var arrTruckID   = []; // Array of Truck IDs
var arrTruckEr   = []; // Array of errored fields to tag in red

function fnAddTruck() {
  document.getElementById("spanTruckHead").innerHTML     = "Add Truck";
  document.getElementById("inpTruckID").disabled         = false;
  document.getElementById("inpTruckID").readOnly         = false;
  document.getElementById("inpTruckID").classList.remove("reo");
  document.getElementById("subAddTruck").style.display   = "inline";
  document.getElementById("subChgTruck").style.display   = "none";
  document.getElementById("butConTruck").style.display   = "none";
  document.getElementById("myModalTruck").style.display  = "block";
}

function fnChgTruck(id) {
  jsIndex = arrTruckID.indexOf(id);
  document.getElementById("spanTruckHead").innerHTML     = "Edit Truck";
  document.getElementById("inpTruckID").disabled         = false;
  document.getElementById("inpTruckID").readOnly         = true;
  document.getElementById("inpTruckID").classList.add("reo");
  document.getElementById("subAddTruck").style.display   = "none";
  document.getElementById("subChgTruck").style.display   = "inline";
  document.getElementById("butConTruck").style.display   = "inline";
  document.getElementById("myModalTruck").style.display  = "block";
  fnFillTruck(jsIndex);
}

function fnFillTruck(jsIndex) {

  document.getElementById("inpTruckID").value            = arrTruckID[jsIndex];
  document.getElementById("inpTruckYr").value            = arrTruckOb[jsIndex].yr;
  document.getElementById("selTruckType").value          = arrTruckOb[jsIndex].type;
  document.getElementById("selTruckFuel").value          = arrTruckOb[jsIndex].fuel;
  document.getElementById("selTruckOffice").value        = arrTruckOb[jsIndex].office;
  document.getElementById("selTruckStatus").value        = arrTruckOb[jsIndex].status;
  document.getElementById("selTruckTrans").value         = arrTruckOb[jsIndex].trans;
  document.getElementById("inpTruckNumber").value        = arrTruckOb[jsIndex].transnumber;
  document.getElementById("inpTruckVin").value           = arrTruckOb[jsIndex].vin;
  document.getElementById("inpTruckLic").value           = arrTruckOb[jsIndex].lic;
  document.getElementById("selTruckLicState").value      = arrTruckOb[jsIndex].state;
  document.getElementById("inpTruckEntUnit").value       = arrTruckOb[jsIndex].ent_unit;
  document.getElementById("inpTruckGeo").value           = arrTruckOb[jsIndex].geo;
  document.getElementById("inpTruckInfo").value          = arrTruckOb[jsIndex].info;
  document.getElementById("inpTruckDInsExp").value       = arrTruckOb[jsIndex].d_ins_exp;
  document.getElementById("inpTruckDRegExp").value       = arrTruckOb[jsIndex].d_reg_exp;
  //document.getElementById("chkTruckIsAct").checked       = (arrTruckOb[jsIndex].is_act == 1 ? true : false);
}

function fnConfirmDelTruck() {
  strConfirm  = "<div class='content alert'>";
  strConfirm += "Delete this Truck?&nbsp;&nbsp;";
  strConfirm += "<input class='inp08' type='submit' name='subDelTruckY' value='Yes' />";
  strConfirm += "<input class='inp08' type='button' name='butDelTruckN' value='No' onclick='fnDelTruckN()' />";
  strConfirm += "</div>";
  document.getElementById("spanTruckConfirm").innerHTML = strConfirm;
}

function fnDelTruckN() {
  document.getElementById("spanTruckConfirm").innerHTML = "";
}

function fnCloseModalTruck() {
  document.getElementById("inpTruckID").value            = "";
  document.getElementById("inpTruckYr").value            = "";
  document.getElementById("selTruckType").value          = "";
  document.getElementById("selTruckFuel").value          = "";
  document.getElementById("selTruckOffice").value        = "";
  document.getElementById("selTruckStatus").value        = "";
  document.getElementById("inpTruckNumber").value        = "";
  document.getElementById("inpTruckVin").value           = "";
  document.getElementById("inpTruckLic").value           = "";
  document.getElementById("selTruckLicState").value      = "";
  document.getElementById("inpTruckGeo").value           = "";
  document.getElementById("inpTruckEntUnit").value       = "";
  document.getElementById("inpTruckInfo").value          = "";
  document.getElementById("inpTruckDInsExp").value       = "";
  document.getElementById("inpTruckDRegExp").value       = "";
  //document.getElementById("chkTruckIsAct").checked       = false;
  document.getElementById("myModalTruck").style.display  = "none";
  for(var i=0; i<arrTruckEr.length; i++) {
    document.getElementById(arrTruckEr[i]).classList.remove("err");
  }
  delete(arrTruckEr);
  document.getElementById("spanTruckMessages").innerHTML = "";
}
