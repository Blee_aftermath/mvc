var arrReviewOb   = []; // Array of Review objects
var arrReviewID   = []; // Array of Review IDs
var arrReviewEr   = []; // Array of errored fields to tag in red

function fnAddReview() {
  document.getElementById("spanReviewHead").innerHTML     = "Add Review";
  document.getElementById("hidReviewID").disabled         = true;
  document.getElementById("tblReview").style.display      = "block";
  document.getElementById("subAddReview").style.display   = "inline";
  document.getElementById("subChgReview").style.display   = "none";
  document.getElementById("butConReview").style.display   = "none";
  document.getElementById("myModalReview").style.display  = "block";
}

function fnChgReview(id) {
  jsIndex = arrReviewID.indexOf(id);
  document.getElementById("spanReviewHead").innerHTML     = "Edit Review";
  document.getElementById("hidReviewID").disabled         = false;
  document.getElementById("tblReview").style.display      = "block";
  document.getElementById("subAddReview").style.display   = "none";
  document.getElementById("subChgReview").style.display   = "inline";
  document.getElementById("butConReview").style.display   = "inline";
  document.getElementById("myModalReview").style.display  = "block";
  fnFillReview(jsIndex);
}

function fnFillReview(jsIndex) {
  document.getElementById("hidReviewID").value         = arrReviewID[jsIndex];
  document.getElementById("inpDRec").value             = arrReviewOb[jsIndex].dtRec;
  document.getElementById("inpReviewInfo").value       = arrReviewOb[jsIndex].info;
}

function fnConfirmDelReview() {
  strConfirm  = "<div class='content alert'>";
  strConfirm += "Delete this Review?&nbsp;&nbsp;";
  strConfirm += "<input class='inp08' type='submit' name='subDelReviewY' value='Yes' />";
  strConfirm += "<input class='inp08' type='button' name='butDelReviewN' value='No' onclick='fnDelReviewN()' />";
  strConfirm += "</div>";
  document.getElementById("spanReviewConfirm").innerHTML = strConfirm;
}

function fnDelReviewN() {
  document.getElementById("spanReviewConfirm").innerHTML = "";
}

function fnCloseModalReview() {
  document.getElementById("hidReviewID").value            = "";
  document.getElementById("inpDRec").value                = "";
  document.getElementById("inpReviewInfo").value          = "";
  document.getElementById("spanReviewHead").innerHTML     = "";
  document.getElementById("spanReviewMessages").innerHTML = "";
  document.getElementById("spanReviewConfirm").innerHTML  = "";
  document.getElementById("myModalReview").style.display  = "none";
  for(var i=0; i<arrReviewEr.length; i++) {
    document.getElementById(arrReviewEr[i]).classList.remove("err");
  }
}
