<?php
 echo "\n\n<script language=\"JavaScript\">";
  echo "\n<!-- Hide the script from old browsers --";

  echo "\nvar arrAddress = [];"; // Array of address objectss for this job
  echo "\nvar arrError   = [];"; // Array of errored fields to tag in red

// Store some info about the caller and property for fnGetCaller and fnGetProperty.
// Make two attempts to do this -- from a possible $myJob or $myInv on the including page.

  if (isset($myJob)) {
    echo "\nvar objCallerAddress = {};";
    echo "\nvar objPropAddress   = {};";
    echo "\nobjCallerAddress.fname    = \"" . prepareJS($myJob['job_cust_fname'])    . "\";";
    echo "\nobjCallerAddress.lname    = \"" . prepareJS($myJob['job_cust_lname'])    . "\";";
    echo "\nobjCallerAddress.company  = \"" . prepareJS($myJob['job_cust_other'])  . "\";";
    echo "\nobjCallerAddress.email    = \"" . prepareJS($myJob['job_cust_email'])    . "\";";
    echo "\nobjCallerAddress.telp     = \"" . formatPhone($myJob['job_cust_telp'])   . "\";";
    echo "\nobjCallerAddress.extp     = \"" . prepareJS($myJob['job_cust_ext'])      . "\";";
    echo "\nobjPropAddress.addr1      = \"" . prepareJS($myJob['job_prop_addr1'])    . "\";";
    echo "\nobjPropAddress.addr2      = \"" . prepareJS($myJob['job_prop_addr2'])    . "\";";
    echo "\nobjPropAddress.city       = \"" . prepareJS($myJob['job_prop_city'])     . "\";";
    echo "\nobjPropAddress.state      = \"" . prepareJS($myJob['job_prop_state'])    . "\";";
    echo "\nobjPropAddress.zip        = \"" . prepareJS($myJob['job_prop_zip'])      . "\";";

  } elseif (isset($myInv)) {
    echo "\nvar objCallerAddress = {};";
    echo "\nvar objPropAddress   = {};";
    echo "\nobjCallerAddress.fname    = \"" . prepareJS($myInv['job_cust_fname'])    . "\";";
    echo "\nobjCallerAddress.lname    = \"" . prepareJS($myInv['job_cust_lname'])    . "\";";
    echo "\nobjCallerAddress.company  = \"" . prepareJS($myInv['job_cust_other'])  . "\";";
    echo "\nobjCallerAddress.email    = \"" . prepareJS($myInv['job_cust_email'])    . "\";";
    echo "\nobjCallerAddress.telp     = \"" . formatPhone($myInv['job_cust_telp'])   . "\";";
    echo "\nobjCallerAddress.extp     = \"" . prepareJS($myInv['job_cust_ext'])      . "\";";
    echo "\nobjPropAddress.addr1      = \"" . prepareJS($myInv['job_prop_addr1'])    . "\";";
    echo "\nobjPropAddress.addr2      = \"" . prepareJS($myInv['job_prop_addr2'])    . "\";";
    echo "\nobjPropAddress.city       = \"" . prepareJS($myInv['job_prop_city'])     . "\";";
    echo "\nobjPropAddress.state      = \"" . prepareJS($myInv['job_prop_state'])    . "\";";
    echo "\nobjPropAddress.zip        = \"" . prepareJS($myInv['job_prop_zip'])      . "\";";
  }

  echo "\nfunction fnGetCaller() {";
  echo "\n  document.getElementById(\"inpFName\").value    = objCallerAddress.fname;";
  echo "\n  document.getElementById(\"inpLName\").value    = objCallerAddress.lname;";
  echo "\n  document.getElementById(\"inpRelation\").value = objCallerAddress.company;";
  echo "\n  document.getElementById(\"inpEmail\").value    = objCallerAddress.email;";
  echo "\n  document.getElementById(\"inpTelp\").value     = objCallerAddress.telp;";
  echo "\n  document.getElementById(\"inpExtp\").value     = objCallerAddress.extp;";
  echo "\n}";

  echo "\nfunction fnGetProperty() {";
  echo "\n  document.getElementById(\"inpLine1\").value    = objPropAddress.addr1;";
  echo "\n  document.getElementById(\"inpLine2\").value    = objPropAddress.addr2;";
  echo "\n  document.getElementById(\"inpCity\").value     = objPropAddress.city;";
  echo "\n  document.getElementById(\"selState\").value    = objPropAddress.state;";
  echo "\n  document.getElementById(\"inpZip\").value      = objPropAddress.zip;";
  echo "\n}";

  echo "\nfunction fnFillForm(id) {";
  echo "\n  document.getElementById(\"inpFName\").value    = arrAddress[id].fname;";
  echo "\n  document.getElementById(\"inpLName\").value    = arrAddress[id].lname;";
  echo "\n  document.getElementById(\"inpTitle\").value    = arrAddress[id].title;";
  echo "\n  document.getElementById(\"inpCompany\").value  = arrAddress[id].company;";
  echo "\n  document.getElementById(\"inpLine1\").value    = arrAddress[id].line1;";
  echo "\n  document.getElementById(\"inpLine2\").value    = arrAddress[id].line2;";
  echo "\n  document.getElementById(\"inpCity\").value     = arrAddress[id].city;";
  echo "\n  document.getElementById(\"selState\").value    = arrAddress[id].state;";
  echo "\n  document.getElementById(\"inpZip\").value      = arrAddress[id].zip;";
  echo "\n  document.getElementById(\"inpEmail\").value    = arrAddress[id].email;";
  echo "\n  document.getElementById(\"inpTelp\").value     = arrAddress[id].telp;";
  echo "\n  document.getElementById(\"inpTelc\").value     = arrAddress[id].telc;";
  echo "\n  document.getElementById(\"inpTelw\").value     = arrAddress[id].telw;";
  echo "\n  document.getElementById(\"inpExtp\").value     = arrAddress[id].extp;";
  echo "\n  document.getElementById(\"inpExtc\").value     = arrAddress[id].extc;";
  echo "\n  document.getElementById(\"inpExtw\").value     = arrAddress[id].extw;";
  echo "\n  document.getElementById(\"inpRelation\").value = arrAddress[id].relation;";
  echo "\n  document.getElementById(\"inpInfo\").value     = arrAddress[id].info;";
  echo "\n  if (arrAddress[id].is_cosi == 1) { document.getElementById(\"chkIsCosi\").checked = true; }";
  echo "\n  if (arrAddress[id].is_prow == 1) { document.getElementById(\"chkIsProw\").checked = true; }";
  echo "\n  if (arrAddress[id].is_bill == 1) { document.getElementById(\"chkIsBill\").checked = true; }";
  echo "\n}";

  echo "\nfunction fnAddAddress() {";
  echo "\n  document.getElementById(\"spanAddressID\").innerHTML = \" " . $delim . " Add\";";
  echo "\n  document.getElementById(\"hidAddressID\").disabled   = true;";
  echo "\n  document.getElementById(\"hidAddressID\").value      = \"\";";
  echo "\n  document.getElementById(\"subAddAddr\").style.display    = \"inline\";";
  echo "\n  document.getElementById(\"subChgAddr\").style.display    = \"none\";";
  echo "\n  document.getElementById(\"subCan\").style.display    = \"inline\";";
  echo "\n  document.getElementById(\"myModalAddress\").style.display   = \"block\";";
  echo "\n  if(window.objCallerAddress) {"; // Only show the Get Caller option if the objCallerAddress was created earlier.
  echo "\n    document.getElementById(\"spanGetCallerName\").innerHTML    = \"&nbsp;[<a href='javascript:fnGetCaller();'>Get Caller</a>]\";";
  echo "\n  }";
  echo "\n  if(window.objPropAddress) {"; // Only show the Get Property option if the objCallerAddress was created earlier.
  echo "\n    document.getElementById(\"spanGetPropName\").innerHTML    = \"&nbsp;[<a href='javascript:fnGetProperty();'>Get Property</a>]\";";
  echo "\n  }";
  echo "\n  document.getElementById(\"spanConfirm\").innerHTML = \"\";";
  echo "\n}";

  echo "\nfunction fnEditAddress(id) {";
  echo "\n  fnFillForm(id);";
  echo "\n  document.getElementById(\"spanAddressID\").innerHTML = arrAddress[id].id + \" " . $delim . " Edit\";";
  echo "\n  document.getElementById(\"hidAddressID\").disabled   = false;";
  echo "\n  document.getElementById(\"hidAddressID\").value      = arrAddress[id].id;";
  echo "\n  document.getElementById(\"subAddAddr\").style.display  = \"none\";";
  echo "\n  document.getElementById(\"subChgAddr\").style.display  = \"inline\";";
  echo "\n  document.getElementById(\"subCan\").style.display  = \"inline\";";
  echo "\n  document.getElementById(\"myModalAddress\").style.display = \"block\";";
  echo "\n  document.getElementById(\"spanGetCallerName\").innerHTML  = \"\";";
  echo "\n  document.getElementById(\"spanGetPropName\").innerHTML    = \"\";";
  echo "\n}";

  echo "\nfunction fnCloseAddress() {";
  echo "\n  document.getElementById(\"hidAddressID\").value      = \"\";";
  echo "\n  document.getElementById(\"spanAddressID\").innerHTML = \"\";";
  echo "\n  document.getElementById(\"inpFName\").value          = \"\";";
  echo "\n  document.getElementById(\"inpLName\").value          = \"\";";
  echo "\n  document.getElementById(\"inpTitle\").value          = \"\";";
  echo "\n  document.getElementById(\"inpCompany\").value        = \"\";";
  echo "\n  document.getElementById(\"inpLine1\").value          = \"\";";
  echo "\n  document.getElementById(\"inpLine2\").value          = \"\";";
  echo "\n  document.getElementById(\"inpCity\").value           = \"\";";
  echo "\n  document.getElementById(\"selState\").value          = \"\";";
  echo "\n  document.getElementById(\"inpZip\").value            = \"\";";
  echo "\n  document.getElementById(\"inpEmail\").value          = \"\";";
  echo "\n  document.getElementById(\"inpTelp\").value           = \"\";";
  echo "\n  document.getElementById(\"inpExtp\").value           = \"\";";
  echo "\n  document.getElementById(\"inpTelc\").value           = \"\";";
  echo "\n  document.getElementById(\"inpExtc\").value           = \"\";";
  echo "\n  document.getElementById(\"inpTelw\").value           = \"\";";
  echo "\n  document.getElementById(\"inpExtw\").value           = \"\";";
  echo "\n  document.getElementById(\"inpRelation\").value       = \"\";";
  echo "\n  document.getElementById(\"inpInfo\").value           = \"\";";
  echo "\n  document.getElementById(\"chkIsCosi\").checked       = false;";
  echo "\n  document.getElementById(\"chkIsProw\").checked       = false;";
  echo "\n  document.getElementById(\"chkIsBill\").checked       = false;";
  echo "\n  for(var i=0; i<arrError.length; i++) {";
  echo "\n    document.getElementById(arrError[i]).classList.remove(\"err\");";
  echo "\n  }";
  echo "\n  document.getElementById(\"spanMessages\").innerHTML  = \"\";";
  echo "\n  document.getElementById(\"myModalAddress\").style.display   = \"none\";";
  echo "\n}";

    echo "\n// --End Hiding Here -->";
  echo "\n</script>\n";

  if (isset($valJobID)) {
    $sqlAddr = "SELECT address.* FROM address WHERE addr_job = $valJobID";

    if ($resAddr = $mysqli->query($sqlAddr)) {
      $numAddr = $resAddr->num_rows;
      if ($numAddr > 0) {

        while ($myAddr = $resAddr->fetch_assoc()) {

          echo "\n<script type=\"text/javascript\">";
          echo "\n<!-- Hide the script from old browsers --";

          echo "\nvar objAddress = {};"; // Object of address fields for this address.  Can also = new Object();
          $state = $myAddr['addr_state'];

          echo "\nobjAddress.id       = \"" . $myAddr['addr_id']                  . "\";";
          echo "\nobjAddress.fname    = \"" . prepareJS($myAddr['addr_fname'])    . "\";";
          echo "\nobjAddress.lname    = \"" . prepareJS($myAddr['addr_lname'])    . "\";";
          echo "\nobjAddress.title    = \"" . prepareJS($myAddr['addr_title'])    . "\";";
          echo "\nobjAddress.company  = \"" . prepareJS($myAddr['addr_company'])  . "\";";
          echo "\nobjAddress.line1    = \"" . prepareJS($myAddr['addr_line1'])    . "\";";
          echo "\nobjAddress.line2    = \"" . prepareJS($myAddr['addr_line2'])    . "\";";
          echo "\nobjAddress.city     = \"" . prepareJS($myAddr['addr_city'])     . "\";";
          echo "\nobjAddress.state    = \"" . $myAddr['addr_state']               . "\";";
          echo "\nobjAddress.zip      = \"" . $myAddr['addr_zip']                 . "\";";
          echo "\nobjAddress.email    = \"" . prepareJS($myAddr['addr_email'])    . "\";";
          echo "\nobjAddress.telp     = \"" . formatPhone($myAddr['addr_telp'])   . "\";";
          echo "\nobjAddress.telc     = \"" . formatPhone($myAddr['addr_telc'])   . "\";";
          echo "\nobjAddress.telw     = \"" . formatPhone($myAddr['addr_telw'])   . "\";";
          echo "\nobjAddress.extp     = \"" . prepareJS($myAddr['addr_extp'])     . "\";";
          echo "\nobjAddress.extc     = \"" . prepareJS($myAddr['addr_extc'])     . "\";";
          echo "\nobjAddress.extw     = \"" . prepareJS($myAddr['addr_extw'])     . "\";";
          echo "\nobjAddress.relation = \"" . prepareJS($myAddr['addr_relation']) . "\";";
          echo "\nobjAddress.info     = \"" . prepareJS($myAddr['addr_info'])     . "\";";
          echo "\nobjAddress.is_cosi  = \"" . prepareJS($myAddr['addr_is_cosi'])  . "\";";
          echo "\nobjAddress.is_prow  = \"" . prepareJS($myAddr['addr_is_prow'])  . "\";";
          echo "\nobjAddress.is_bill  = \"" . prepareJS($myAddr['addr_is_bill'])  . "\";";

          echo "\narrAddress[" . $myAddr['addr_id'] . "] = objAddress;"; // Push this object into the array of addresses

          echo "\n// --End Hiding Here -->";
          echo "\n</script>\n";

        } // while
      } // if ($numAddr > 0)
    } // if ($resAddr)
  } // if ($valJobID)

  $strIcons  = "<span style='float:right;'>";
  $strIcons .= "<a href='javascript:fnAddAddress();'><img class='icon16' src='icons/add16.png' alt='Add' title='Add' /></a>";
  $strIcons .= "</span>";
  echo "<div class='modal' id='myModalAddress'>";
  echo "<div class='fixed'>";
  echo "<div class='body_stripe'></div>";

  echo "<div class='head'>";
  echo "<div class='content'>";

  echo "Address <span id='spanAddressID'></span>";
  echo "<span style='float:right;'><a href='javascript:fnCloseAddress();'><img class='icon16' src='icons/no16.png' alt='Close' title='Close' /></a></span>";

  echo "</div>"; // content
  echo "</div>"; // head

  echo "<span id='spanMessages'>";
  fnModalMessagesBox('AuthContract');
  echo "</span>";

  echo "<div class='body'>";
  echo "\n<form method='post' name='frmAddress' action='jifPRG.php'>";
  echo "<div class='content'>";

  $inpField = "hidAddressID";
  $defValue = ""; // set by JS
  echo "<input type='hidden' name='$inpField' id='$inpField' value='" . $defValue . "' />";

  if (isset($valJobID)) {
    $inpField = "hidJobID";
    $defValue = $valJobID;
    echo "<input type='hidden' name='$inpField' id='$inpField' value='" . $defValue . "' />";
  }
  echo "<table class='info'>";

  $defValue = ""; // set by JS for all fields in the modal table
  $checked  = ""; // set by JS for all fields in the modal table

  echo "<tr class='heading'><th colspan='2'>Printable Address</th></tr>";

  $inpField = "inpFName";
  $inpLabel = "Name:";
  $defClass = "inp10 " . "req";
  echo "<tr class='content'><td>$inpLabel</td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' value='$defValue' maxlength='$maxName' />";
  $inpField = "inpLName";
  $defClass = "inp10 " . "req";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' value='$defValue' maxlength='$maxName' />";
  echo "<span id='spanGetCallerName'></span>";
  echo "</td></tr>";

  $inpField = "inpTitle";
  $inpLabel = "Title:";
  $inpHelp  = "Title should be a real employment title such as Director or Attorney.  It should be entered as you would expect it to show on print materials like an Aftermath invoice.";
  $defClass = "opt";
  echo "<tr class='content'><td>$inpLabel</td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' value='" . $defValue . "' maxlength='$maxName' />";
  echo "&nbsp;<img class='icon16' title='$inpHelp' alt='Help' src='icons/question16.png' />";
  echo "</td></tr>";

  $inpField = "inpCompany";
  $inpLabel = "Company:";
  $inpHelp  = "Company should be the company this person works for.  It could be something like BNSF or the proper name of a management company.  Do not put an informal description like Management Company.  Spell out the actual company name as this may appear on print materials like an Aftermath invoice.";
  $defClass = "opt";
  echo "<tr class='content'><td class='l'>$inpLabel</td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' value='$defValue' maxlength='$maxName' />";
  echo "&nbsp;<img class='icon16' title='$inpHelp' alt='Help' src='icons/question16.png' />";
  echo "</td></tr>";

  $inpField = "inpLine1";
  $inpLabel = "Address Line 1:";
  $defClass = "req";
  echo "<tr class='content'><td class='l'>$inpLabel</td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' value='$defValue' maxlength='$maxAddr' />";
  echo "<span id='spanGetPropName'></span>";
  echo "</td></tr>";

  $inpField = "inpLine2";
  $inpLabel = "Address Line 2:";
  $defClass = "opt";
  echo "<tr class='content'><td class='l'>$inpLabel</td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' value='$defValue' maxlength='$maxAddr' />";
  echo "</td></tr>";

  $inpField = "inpCity";
  $inpLabel = "City:";
  $defClass = "req";
  echo "<tr class='content'><td class='l'>$inpLabel</td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' value='$defValue' maxlength='$maxAddr' />";
  echo "</td></tr>";

  $sqlState = "SELECT * FROM state ORDER BY st_name";
  $resState = $mysqli->query($sqlState) or die('Get state failed');
  $inpField = "selState";
  $inpLabel = "State / Zip:";
  $defClass = "inp12 " . "req";
  echo "<tr class='content'><td class='l'>$inpLabel</td><td class='r'>";
  echo "<select class='$defClass' name='$inpField' id='$inpField'>";
  echo "<option value='' selected='selected'>--- Select ---</option>";
  while ($myState = $resState->fetch_assoc()) {
    echo "<option value='" . $myState['st_id'] . "'";
    if(isset($state)) { 
      if ($myState['st_id'] == $state) { echo " selected='selected'"; }
    }
    echo ">" . $myState['st_name'] . "</option>";
  } echo "</select>";
  $inpField = "inpZip";
  $defClass = "inp08 " . "req";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' value='$defValue' maxlength='$maxZip' />";
  echo "</td></tr>";

  echo "<tr class='heading'><th colspan='2'>Contact Details</th></tr>";

  $inpField = "inpEmail";
  $inpLabel = "Email:";
  $defClass = "opt";
  echo "<tr class='content'><td>$inpLabel</td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' value='$defValue' maxlength='$maxEmail' />";
  echo "</td></tr>";

  $inpLabel = "Phone (Home):";
  echo "<tr class='content'><td class='l'>$inpLabel</td>";
  echo "<td class='r'>";
  $inpField = "inpTelp";
  $defClass = "inp12 " . "opt";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' value='$defValue' maxlength='$maxTel' />";
  $inpField = "inpExtp";
  $defClass = "inp08 " . "opt";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' value='$defValue' maxlength='$maxExt' />";
  echo "</td></tr>";

  $inpLabel = "Phone (Cell):";
  echo "<tr class='content'><td class='l'>$inpLabel</td>";
  echo "<td class='r'>";
  $inpField = "inpTelc";
  $defClass = "inp12 " . "opt";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' value='$defValue' maxlength='$maxTel' />";
  $inpField = "inpExtc";
  $defClass = "inp08 " . "opt";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' value='$defValue' maxlength='$maxExt' />";
  echo "</td></tr>";

  $inpLabel = "Phone (Work):";
  echo "<tr class='content'><td class='l'>$inpLabel</td>";
  echo "<td class='r'>";
  $inpField = "inpTelw";
  $defClass = "inp12 " . "opt";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' value='$defValue' maxlength='$maxTel' />";
  $inpField = "inpExtw";
  $defClass = "inp08 " . "opt";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' value='$defValue' maxlength='$maxExt' />";
  echo "</td></tr>";

  $inpField = "inpRelation";
  $inpLabel = "Relationship:";
  $inpHelp  = "Relation should indicate the relationship between this person and the job.  Potential entries might be Father of decedent, Executor, Neighbor, Contractor or Property Manager.  Do not enter Owner or Property Owner here.  If this person is the property owner, check the appropriate box below.  Relation is an internal field and will not show on customer correspondence.";
  $defClass = "opt";
  echo "<tr class='content'><td class='l'>$inpLabel</td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' value='$defValue' maxlength='$maxName' />";
  echo "&nbsp;<img class='icon16' title='$inpHelp' alt='Help' src='icons/question16.png' />";
  echo "</td></tr>";

  $inpField = "inpInfo";
  $inpLabel = "Additional<br />Information:";
  $inpHelp  = "Additional Information should indicate why this person is relevant to this job.  If there are multiple billable addresses, specify which components to bill to this person.  Additional Information is an internal field and will not show on customer correspondence.";
  $defClass = "opt";
  echo "<tr class='content'><td class='l'>$inpLabel</td><td class='r'>";
  echo "<textarea class='$defClass' name='$inpField' id='$inpField' rows='4' cols='48'>$defValue</textarea>";
  echo "&nbsp;<img class='icon16' title='$inpHelp' alt='Help' src='icons/question16.png' />";
  echo "</td></tr>";

  $inpField = "chkIsCosi";
  $inpLabel = "Contract Signer:";
  echo "<tr class='content'><td>$inpLabel</td><td class='l'>";
  echo "<input type='checkbox' name='$inpField' id='$inpField' $checked/>";
  echo "</td></tr>";

  $inpField = "chkIsProw";
  $inpLabel = "Property Owner:";
  echo "<tr class='content'><td>$inpLabel</td><td class='l'>";
  echo "<input type='checkbox' name='$inpField' id='$inpField' $checked/>";
  echo "</td></tr>";

  $inpField = "chkIsBill";
  $inpLabel = "Billable Address:";
  echo "<tr class='content'><td>$inpLabel</td><td class='l' colspan='2'>";
  echo "<input type='checkbox' name='$inpField' id='$inpField' $checked/>";
  echo "</td></tr>";


  $arrButtonRow[] = "<input class='inp12' type='submit' name='subAddAddr' id='subAddAddr' value='Add' />";
  $arrButtonRow[] = "<input class='inp12' type='submit' name='subChgAddr' id='subChgAddr' value='Change' />";
  $arrButtonRow[] = "<input class='inp08' type='button' name='subCan' id='subCan' value='Cancel' onclick='fnCloseAddress();' />";

  echo "<tr><td></td><td>" . implode("", $arrButtonRow) . "</td></tr>";

  echo "</table>";

  echo "</div>"; // content
  echo "<span id='spanConfirm'></span>";
  echo "</form>";
  echo "</div>"; // body

  echo "<div class='body_stripe'></div>";
  echo "</div>"; // fixed
  echo "</div>"; // modal

  if (isset($_SESSION['inpValue']['subAddAddr']) || isset($_SESSION['inpValue']['subChgAddr'])) {
    echo "\n<script language=\"JavaScript\">";
    echo "\n<!-- Hide the script from old browsers --";

    echo "\nvar objAddress = {};"; // Create a new object to hold the posted values, otherwise it updates the last one displayed earlier

    if (isset($_SESSION['inpValue']['hidAddressID'])) {
      echo "\nobjAddress.id     = \"" . prepareJS($_SESSION['inpValue']['hidAddressID']) . "\";";
    }
    echo "\nobjAddress.fname    = \"" . prepareJS($_SESSION['inpValue']['inpFName'])     . "\";";
    echo "\nobjAddress.lname    = \"" . prepareJS($_SESSION['inpValue']['inpLName'])     . "\";";
    echo "\nobjAddress.title    = \"" . prepareJS($_SESSION['inpValue']['inpTitle'])     . "\";";
    echo "\nobjAddress.company  = \"" . prepareJS($_SESSION['inpValue']['inpCompany'])   . "\";";
    echo "\nobjAddress.line1    = \"" . prepareJS($_SESSION['inpValue']['inpLine1'])     . "\";";
    echo "\nobjAddress.line2    = \"" . prepareJS($_SESSION['inpValue']['inpLine2'])     . "\";";
    echo "\nobjAddress.city     = \"" . prepareJS($_SESSION['inpValue']['inpCity'])      . "\";";
    echo "\nobjAddress.state    = \"" . $_SESSION['inpValue']['selState']   . "\";";
    echo "\nobjAddress.zip      = \"" . $_SESSION['inpValue']['inpZip']     . "\";";
    echo "\nobjAddress.email    = \"" . prepareJS($_SESSION['inpValue']['inpEmail'])     . "\";";
    echo "\nobjAddress.telp     = \"" . formatPhone($_SESSION['inpValue']['inpTelp'])    . "\";";
    echo "\nobjAddress.telc     = \"" . formatPhone($_SESSION['inpValue']['inpTelc'])    . "\";";
    echo "\nobjAddress.telw     = \"" . formatPhone($_SESSION['inpValue']['inpTelw'])    . "\";";
    echo "\nobjAddress.extp     = \"" . prepareJS($_SESSION['inpValue']['inpExtp'])      . "\";";
    echo "\nobjAddress.extc     = \"" . prepareJS($_SESSION['inpValue']['inpExtc'])      . "\";";
    echo "\nobjAddress.extw     = \"" . prepareJS($_SESSION['inpValue']['inpExtw'])      . "\";";
    echo "\nobjAddress.relation = \"" . prepareJS($_SESSION['inpValue']['inpRelation'])  . "\";";
    echo "\nobjAddress.info     = \"" . prepareJS($_SESSION['inpValue']['inpInfo'])      . "\";";
    echo "\nobjAddress.is_cosi  = \"" . (isset($_SESSION['inpValue']['chkIsCosi']) ? 1 : 0)   . "\";";
    echo "\nobjAddress.is_prow  = \"" . (isset($_SESSION['inpValue']['chkIsProw']) ? 1 : 0)   . "\";";
    echo "\nobjAddress.is_bill  = \"" . (isset($_SESSION['inpValue']['chkIsBill']) ? 1 : 0)   . "\";";

    foreach($_SESSION['errField'] AS $field) {

      echo "\narrError.push(\"" . $field    . "\");";
      echo "\ndocument.getElementById(\"" . $field . "\").classList.add(\"err\");";

    }

    echo "\narrAddress[0] = objAddress;"; // Put this object into the array of addresses

    if (isset($_SESSION['inpValue']['subAddAddr'])) { echo "\nfnFillForm(0);";
                                                  echo "\nfnAddAddress();";   }
    if (isset($_SESSION['inpValue']['subChgAddr'])) { echo "\nfnEditAddress(0);"; }

    echo "// --End Hiding Here -->\n";
    echo "\n</script>\n";
  }
 unset($arrButtonRow);

?>