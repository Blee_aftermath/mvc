<?php
  session_start();
  if (!in_array('MASTER', $_SESSION['perm'])) {  exit("Session Error"); }

  include("../includes/dbconn.inc");

// *************************** TESTING ***************************

// Sample URL: http://portal.aftermath.com/includes/ajaxAdmin.php?action=addEmpRole&emp=1&role=5
// include("testing.inc");

// *************************** DECLARATIONS ***************************

  $errCount = 0;

// *************************** FUNCTIONS ***************************

// *************************** GET ***************************

  if (isset($_GET['action']) && strlen($_GET['action']) > 0) {
    $action = $_GET['action'];
  } else {
    unset($action); $errCount++;
  }

  if (isset($_GET['sku']) && strlen($_GET['sku']) <= 9) {
    $safSkuNum = "'" . $mysqli->real_escape_string($_GET['sku']) . "'";  // Escape and Quote
  }

  if (isset($_GET['aval']) && is_numeric($_GET['aval'])) {
    $valAval = $_GET['aval'];
  }

// *************************** RESPONSE ***************************

  switch($action) {
  case "addSkuAval":

    if ($errCount==0) {
      $sqlInsSkuVar = "INSERT INTO sku_var SELECT $safSkuNum, aval_attr, sku_item, $valAval FROM attr_val JOIN sku ON sku_num = $safSkuNum WHERE aval_id = $valAval";
      $resInsSkuVar = $mysqli->query($sqlInsSkuVar);
    }
    $return = "updSelLink";

  break;
  case "delSkuAval":

    if ($errCount==0) {
      $sqlDelEmpRole = "DELETE FROM sku_var WHERE svar_sku = $safSkuNum AND svar_aval = $valAval";
      $resDelEmpRole = $mysqli->query($sqlDelEmpRole);
    }
    $return = "updSelLink";

  break;
  }
  switch ($return) {
  case "updSelLink":

    $sqlLink = "SELECT svar_aval, attr_nama, aval_nama FROM sku_var JOIN attribute ON attr_id = svar_attr JOIN attr_val ON aval_id = svar_aval WHERE svar_sku = $safSkuNum";
    $resLink = $mysqli->query($sqlLink);
    $numLink  = $resLink->num_rows;

    echo "<select class='inp18 req' name='selLink' size='10'>\n";

    if ($numLink == 0) echo "<option value='' disabled>--- Select an Attribute ---</option>";
    while ($myLink = $resLink->fetch_assoc()) {
      echo "<option value='" . $myLink['svar_aval'] . "'>" . $myLink['attr_nama'] . ": " . $myLink['aval_nama'] . "</option>";
    }
    echo "</select>";

  break;
  }
?>