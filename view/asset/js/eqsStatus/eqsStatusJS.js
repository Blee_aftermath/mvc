import {Util} from "../util.js"
import {EQSStatus} from "./eqsStatus.js"
import note from "../modules/lib/note.js"

/**
 * Class that handles the JS section on the EQS job status tab
 * @author Scott Kiehn
 */
export class EQSStatusJS{
  /**
   * Creates a new EQS status js section
   * @param {eqsStatus} parent
   * @param {Object} data
   */
  constructor(parent, data) {
    this.parent = parent
    this.data = data
    
    this.baseElement = parent.dom.jsBody
    this.dom = Util.parseDOM(this.baseElement)
    
    //init change events
    let changeFunc = () => {
      this.dom.saveButton.disabled = false
    }
    EQSStatus.initiateChangeEvents(this.dom, changeFunc)
    EQSStatus.initiateDatePicker(this.dom.dateAppt, changeFunc)
    
    //initialize save button events
    this.dom.saveButton.addEventListener('click', this.saveJob.bind(this))
    this.populate()
  }
  
  /**
   * Populates data from this.data
   */
  populate(){
    try{
      const eqsData = this.data.eqsData,
            dateTime = eqsData.job_dt_appt ? Util.dateToString( new Date(eqsData.job_dt_appt) ).split(' ') : ['','']

      this.dom.dateAppt.value = dateTime[0]
      this.dom.timeAppt.value = Util.parseTime24(dateTime[1])
  
      let onSite = eqsData.job_yn_onsite != null && eqsData.job_yn_onsite != 0
      this.dom.job_yn_onsite.checked = onSite
      this.dom.job_so_amt.value = (Number(eqsData.job_so_amt)/100).toFixed(2)  
      this.dom.jeqs_info_js.value = eqsData.jeqs_info_js

      //fill hidden form items, used for upper info area
      this.dom.job_status_dsca.value = eqsData.job_status_dsca
      this.dom.job_comp_amt.value = (Number(eqsData.job_comp_amt)/100).toFixed(2)
    }catch(e){console.log('Catch error')}


    //set res section disabled
    this.updateSectionDisabled()
    this.dom.saveButton.disabled = true
  }
  
  /**
   * Sets the EQS time disabled/enabled based on the job status
   */
  updateSectionDisabled(){
    const status = this.data.eqsData.job_status_code
    const jobDisabled  = status == 'JL'
    
    this.dom.dateAppt.disabled = jobDisabled
    this.dom.timeAppt.disabled = jobDisabled
    this.dom.job_yn_onsite.disabled = jobDisabled
    this.dom.job_so_amt.disabled = jobDisabled
    this.dom.jeqs_info_js.disabled = jobDisabled
  }

  /**
   * Saves the job data for this section
   * @param {boolean} populate
   * @returns {Promise<boolean>}
   */
  async saveJob(populate = true){
    const data = {
      action:        'status_js',
      job_id:        this.data.eqsData.job_id,
      job_dt_appt:   this.dom.dateAppt.value ? Util.dateToString(Util.parseDate(this.dom.dateAppt.value, this.dom.timeAppt.value)) : null,
      job_yn_onsite: this.dom.job_yn_onsite.checked,
      job_so_amt:    Number(this.dom.job_so_amt.value).toFixed(2) * 100,
      jeqs_job:      this.data.eqsData.jeqs_job,
      jeqs_info_js:  this.dom.jeqs_info_js.value
    }

    const post = await fetch('eqsJobPRG.php', {
      method: 'post',
      headers: { 'Content-Type': 'application/json;charset=utf-8' },
      body: JSON.stringify(data)
    })

    const res = await post.json()

    if ( res.ok ) {
      if(populate) this.parent.populate(res.msg)
    } else {
      note({class: 'error', text: 'Error inserting data'})
    }
  }

}