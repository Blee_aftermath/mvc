import {Util} from "../util.js"
import {EQSStatus} from "./eqsStatus.js"
import note from "../modules/lib/note.js"

/**
 * Class that handles the CO section on the EQS job status tab
 * @author Scott Kiehn
 */
export class EQSStatusCO{
  /**
   * Creates a new CO section
   * @param {eqsStatus} parent
   * @param {Object} data
   */
  constructor(parent, data) {
    this.parent = parent
    this.data = data
    
    this.baseElement = parent.dom.coBody
    this.dom = Util.parseDOM(this.baseElement)
    
    let changeFunc = () => {
      this.dom.saveButton.disabled = false
    }
    EQSStatus.initiateChangeEvents(this.dom, changeFunc)
    EQSStatus.initiateDatePicker(this.dom.dateComp, changeFunc)
    
    this.dom.saveButton.addEventListener('click', this.saveJob.bind(this))
    this.populate()
  }
  
  /**
   * Populates this form with this.data
   */
  populate(){
    try{
      const eqsData = this.data.eqsData,
            dateTime = eqsData.job_dt_comp ? Util.dateToString( new Date(eqsData.job_dt_comp) ).split(' ') : ['','']

      this.dom.dateComp.value = dateTime[0]
      this.dom.timeComp.value = Util.parseTime24(dateTime[1])
      this.dom.jeqs_info_co.value = eqsData.jeqs_info_co

    }catch(e){console.log('Catch error')}
    
    this.dom.saveButton.disabled = true
    this.updateSectionDisabled()
  }
  
  /**
   * Updates the disabled sections
   */
  updateSectionDisabled(){
    const status = this.data.eqsData.job_status_code
    const jobDisabled  = status != 'SO' && status != 'CO'
    
    this.dom.dateComp.disabled = jobDisabled
    this.dom.timeComp.disabled = jobDisabled
    this.dom.jeqs_info_co.disabled = jobDisabled
  }
  
  /**
   * Saves the job data for this section
   * @param {boolean} populate
   * @returns {Promise<boolean>}
   */
  async saveJob(populate = true){
    const data = {
      action:       'status_co',
      job_id:       this.data.eqsData.job_id,
      job_dt_comp:  this.dom.dateComp.value ? Util.dateToString(Util.parseDate(this.dom.dateComp.value, this.dom.timeComp.value)) : null,
      jeqs_job:     this.data.eqsData.jeqs_job,
      jeqs_info_co: this.dom.jeqs_info_co.value
    }

    const post = await fetch('eqsJobPRG.php', {
      method: 'post',
      headers: { 'Content-Type': 'application/json;charset=utf-8' },
      body: JSON.stringify(data)
    })

    const res = await post.json()

    if ( res.ok ) {
      if(populate) this.parent.populate(res.msg)
    } else {
      note({class: 'error', text: 'Error inserting data'})
    }
  }

}