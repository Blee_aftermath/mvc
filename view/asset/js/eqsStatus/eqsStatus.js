/**
 * Status tab on the EQS job page
 * @author Scott Kiehn
 */
import {Util} from "../util.js"
import {EQSStatusJS} from "./eqsStatusJS.js"
import {EQSStatusSO} from "./eqsStatusSO.js"
import {EQSStatusCO} from "./eqsStatusCO.js"
import note from "../modules/lib/note.js"

export class EQSStatus {
  /**
   * Creates a new EQS status tab controller
   * @param {Object} data
   */
  constructor(data) {
    this.data = data
    console.log(this.data)
    
    this.baseElement = document.querySelector('[name=tabStatus]')
    this.dom = Util.parseDOM(this.baseElement)

    //create section objects
    this.js = new EQSStatusJS(this, this.data)
    this.so = new EQSStatusSO(this, this.data)
    this.co = new EQSStatusCO(this, this.data)

    this.eqsStatusChangeEvent = new CustomEvent('eqsStatusChangeEvent') //an event to dispatch on status change
  }
  
  /**
   * Repopulates the data of all fields with a new data object
   * @param {object} data
   */
  populate(data){
    //set data objects
    this.data.eqsData = data
    this.js.populate()
    this.so.populate()
    this.co.populate()
    document.dispatchEvent(this.eqsStatusChangeEvent) //dispatch the status change event
  }
  
  /**
   * Updates the data by matching dom elements
   * @param dom
   * @param data
   */
  updateData(dom){
    for (const domKey in dom) {
      if(!dom.hasOwnProperty(domKey) || this.data[domKey] === undefined) continue
      const elem = dom[domKey]
      this.data[domKey] = elem.value
    }
  }
  
  /**
   * Initiates change events for all of the input items in the dom
   * @param {Object} dom
   * @param {Function} callback
   */
  static initiateChangeEvents(dom, callback) {
    for (const domKey in dom) {
      if (!dom.hasOwnProperty(domKey)) continue
      let elem = dom[domKey]
      
      const tagName = elem.tagName.toLowerCase()
      if (tagName != 'input' && tagName != 'textarea') continue
      switch (elem.type) {
        case 'button':
          continue
        default:
          elem.addEventListener('change', callback)
      }
    }
  }
  
  /**
   * Initiates a datepicker and datepicker change event
   * @param {HTMLElement} dp
   * @param {Function} event
   */
  static initiateDatePicker(dp, event){
    let jqueryObj = $(dp)
    jqueryObj.datepicker({
      onSelect: event
    })
    jqueryObj.datepicker('option', 'dateFormat', 'mm/dd/y')
  }
}
