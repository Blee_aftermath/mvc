import {Util} from "../util.js"
import {EQSStatus} from "./eqsStatus.js"
import note from "../modules/lib/note.js"

/**
 * Class that handles the SO section on the EQS job status tab
 * @author Scott Kiehn
 */
export class EQSStatusSO{
  /**
   * Creates a new SO section
   * @param {eqsStatus} parent
   * @param {Object} data
   */
  constructor(parent, data) {
    this.parent = parent
    this.data = data
    
    this.baseElement = parent.dom.soBody
    this.dom = Util.parseDOM(this.baseElement)
    
    let changeFunc = () => {
      this.dom.saveButton.disabled = false
    }
    EQSStatus.initiateChangeEvents(this.dom, changeFunc)
    EQSStatus.initiateDatePicker(this.dom.dateStart, changeFunc)
    
    this.dom.saveButton.addEventListener('click', this.saveJob.bind(this))
    this.populate()
  }
  
  /**
   * Populates the form with data from this.data
   */
  populate(){
    try{
      const eqsData = this.data.eqsData,
            dateTime = eqsData.job_dt_start ? Util.dateToString( new Date(eqsData.job_dt_start) ).split(' ') : ['','']

      this.dom.dateStart.value = dateTime[0]
      this.dom.timeStart.value = Util.parseTime24(dateTime[1])
      this.dom.jeqs_info_so.value = eqsData.jeqs_info_so

    }catch(e){console.log('Catch error')}
    
    this.updateSectionDisabled()
    this.dom.saveButton.disabled = true
  }
  
  /**
   * Sets different sections to be enabled based on the job status
   */
  updateSectionDisabled(){
    const status = this.data.eqsData.job_status_code
    const jobDisabled  = status == 'JN' || status == 'JP' || status == 'JL'
    
    this.dom.dateStart.disabled = jobDisabled
    this.dom.timeStart.disabled = jobDisabled
    this.dom.jeqs_info_so.disabled = jobDisabled
  }

  /**
   * Saves the job data for this section
   * @param {boolean} populate
   * @returns {Promise<boolean>}
   */
  async saveJob(populate = true){
    const data = {
      action:       'status_so',
      job_id:       this.data.eqsData.job_id,
      job_dt_start: this.dom.dateStart.value ? Util.dateToString(Util.parseDate(this.dom.dateStart.value, this.dom.timeStart.value)) : null,
      jeqs_job:     this.data.eqsData.jeqs_job,
      jeqs_info_so: this.dom.jeqs_info_so.value
    }

    const post = await fetch('eqsJobPRG.php', {
      method: 'post',
      headers: { 'Content-Type': 'application/json;charset=utf-8' },
      body: JSON.stringify(data)
    })

    const res = await post.json()

    if ( res.ok ) {
      if(populate) this.parent.populate(res.msg)
    } else {
      note({class: 'error', text: 'Error inserting data'})
    }
  }
}