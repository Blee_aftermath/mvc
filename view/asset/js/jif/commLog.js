import { CommLogItem } from './commLogItem.js'
import { PhaseDocuments } from './checklistDocuments.js'

/**
 * Enums for filtering types
 */
const Filter = {
  COMM: 'jif',
  JOB: 'job',
  INVOICE: 'csr',
  ALL: 'all'
}

const removeRE = /^(res|eqs)/

/**
 * Object that creates comm log content
 * @author Jake Cirino
 */
export class CommLog {
  /**
   * Creates a new CommLog
   * @param {Number} jobID The id of the job this comm log is displaying
   * @param {JSON} commData The comm data for all the items in the comm log
   * @param {JSON} timezoneData The timezone data
   * @param {String} curPage The page this commlog is on, used to determine input fields
   * @param {Number} invID Optional invoice id
   */
  constructor(jobID, classID, commData, timezoneData, curPage, invID = null) {

    this.jobID = jobID
    this.classID = classID
    this.commData = commData
    this.timezoneData = timezoneData
    this.page = curPage = curPage.replace(removeRE, '').toLowerCase() // remove 'eqs' & 'res' so job template used
    this.invID = invID
    this.inputFile = `jif/commLog${curPage.charAt(0).toUpperCase() + curPage.slice(1)}Input`
    this.filter = Filter.ALL

    //console.log(commData)

    //add to Filter if proper class
    if (Number(this.classID) == 1) Filter.RES = 'res'
    if (Number(this.classID) == 6) Filter.PAR = 'par'

    //create elements
    this.baseElement = document.createElement('div')
    this.baseElement.id = 'jifCommLog'

    //populate and append
    this.populate()
  }

  async populate() {
    //load HTML template
    let template = await window.templateController.getTemplate('jif/commLog')
    this.baseElement.innerHTML = template

    //load and append input section
    let inputForm = document.createElement('form')
    let inputTemplate = await window.templateController.getTemplate(this.inputFile)
    inputForm.innerHTML = inputTemplate
    this.baseElement.insertBefore(inputForm, this.baseElement.querySelector('.com-header'))

    //set/select form elements
    this.textArea = this.baseElement.querySelector('textarea')
    this.dateField = this.baseElement.querySelector("[name='dateField']")
    this.timeField = this.baseElement.querySelector("[name='selTNote']")
    this.timezoneElement = this.baseElement.querySelector("[name='timezone']")
    this.submitButton = this.baseElement.querySelector("[name='action-yes']")
    this.activityElement = this.baseElement.querySelector("[name='note_code_activity']")
    this.statusElement = this.baseElement.querySelector("[name='note_code_status']")
    this.filterButtons = this.baseElement.querySelector('.com-filterbar').children
    this.itemBody = this.baseElement.querySelector('.body_content_scroll')

    //setup filter buttons
    for (const key in this.filterButtons) {
      if (this.filterButtons.hasOwnProperty(key)) {
        const element = this.filterButtons[key]

        Object.keys(Filter).forEach((key) => {
          if (Filter[key] == element.name) {
            element.style.display = 'inline-block'
          }
        })

        element.addEventListener('click', () => {
          //this.setFilter(element.name)
          this.preFilter(element.name)
        })
      }
    }

    //setup submit button
    this.submitButton.addEventListener(
      'click',
      ((event) => {
        event.preventDefault()
        this.submitPost()
      }).bind(this)
    )

    //generate log messages
    this.items = []
    this.maxNoteId = 1
    for (const key in this.commData) {
      if (this.commData.hasOwnProperty(key)) {
        const element = this.commData[key];
        let logItem = new CommLogItem(this, element, this.timezoneData)
        await logItem.populate()
        this.items.push(logItem)

        //set new max note id
        if (element.noteID > this.maxNoteId) this.maxNoteId = element.noteID
      }
    }

    //set initial filter
    this.setFilter(this.filter)

    //append to document
    document.querySelector("[name='tab_body']").appendChild(this.baseElement)

    //setup datepicker or populate note code options
    if (this.page == 'jif') {
      $(document).ready(function () {
        $(function () {
          $('#selDNote').datepicker()
        })
      })
    } else if (this.page == 'csr') {
      //populate options
      fetch('commLogPRG.php', {
        method: 'post',
        headers: { 'Content-Type': 'application/json;charset=utf8' },
        body: JSON.stringify({
          action: 'getInvoiceOptions',
        }),
      })
        .then((res) => res.json())
        .then((body) => {
          let items = body.ok

          //populate activities
          for (const key in items.activities) {
            if (items.activities.hasOwnProperty(key)) {
              const element = items.activities[key]

              //create element
              let option = document.createElement('option')
              option.value = element.inv_activity_code
              option.innerText = element.inv_activity_dsca
              this.activityElement.appendChild(option)
            }
          }

          //populate statuses
          for (const key in items.statuses) {
            if (items.statuses.hasOwnProperty(key)) {
              const element = items.statuses[key]

              //create element
              let option = document.createElement('option')
              option.value = element.inv_status_code
              option.innerText = element.inv_status_dsca
              this.statusElement.appendChild(option)
            }
          }
        })
    }
  }

  /**
   * Checks for change log size and re-populates if there is
   * @param {String} filter The filter option to pass on
   */
  async preFilter(filter) {

    //check for change in job note ammount and update all notes if changed
    const post = await fetch('commLogPRG.php', {
      method: 'post',
      headers: { 'Content-Type': 'application/json;charset=utf-8' },
      body: JSON.stringify({ action: 'noteCount', jobID: this.jobID, currentCount: this.items.length })
    })
    const res = await post.json()
    if (res.ok) {
      if (res.commData.length) {
        this.commData = res.commData
        this.baseElement.innerHTML = ''
        this.filter = filter
        this.populate()
      } else {
        this.setFilter(filter)
      }
    } else {
      this.setFilter(filter)
    }

  }

  /**
   * Sets the filter for the logs
   * @param {String} filter The filter option to set
   */
  setFilter(filter) {
    this.filter = filter

    //change tab colors
    for (const key in this.filterButtons) {
      if (this.filterButtons.hasOwnProperty(key)) {
        const element = this.filterButtons[key]
        let isSelected = element.getAttribute('name') == filter

        element.style.backgroundColor = isSelected ? '#ff9' : ''
      }
    }

    //set logs to display
    this.items.forEach((element) => {

      let isDisplay = element.data.noteType == filter || filter == Filter.ALL
        || (element.data.noteType == 'crem' && filter == 'csr')

      element.baseElement.style.display = isDisplay ? 'block' : 'none'

      //change background colors on all page
      if(filter == Filter.ALL){
        switch(element.data.noteType){
          case Filter.INVOICE:
          element.noteBody.style.backgroundColor = '#fdd'
          break
          case Filter.COMM:
          element.noteBody.style.backgroundColor = '#ffd'
          break
          case Filter.PAR:
          element.noteBody.style.backgroundColor = '#dfd'
          element.noteBody.innerHTML = element.noteBody.innerHTML + "<p class='note-flag'>Parent Note</p>"
          break
          case Filter.RES:
          element.noteBody.style.backgroundColor = '#dfd'
          element.noteBody.innerHTML = element.noteBody.innerHTML + "<p class='note-flag'>Restoration Note</p>"
          break
          default: // job, eqsJob, resJOB
          element.noteBody.style.backgroundColor = '#dff'
          break
        }
      }else{
        element.noteBody.style.backgroundColor = 'white'
      }
    })
  }

  /**
   * Submits a post to the database
   */
  submitPost() {
    //make sure there is data in the textarea to send
    if (this.textArea.value.length > 0) {

      //disable submit button to prevent resubmits
      this.submitButton.disabled = true

      //reset colors and tooltips
      this.textArea.style.backgroundColor = ''
      this.textArea.title = ''

      switch (this.page) {
        case 'jif':
          this.dateField.style.backgroundColor = ''
          this.dateField.title = ''

          this.timeField.style.backgroundColor = ''
          this.timeField.title = ''

          //call api to post note
          this.fetchAPI('post', (body) => {
            if (body.ok) {
              this.textArea.value = ''
              this.update(body.msg)
            } else {
              //invalid date is the only error that could have occured
              this.dateField.style.backgroundColor = '#f66'
              this.dateField.title = 'Date/time must not exceed current time'
              this.timeField.style.backgroundColor = '#f66'
              this.timeField.title = 'Date/time must not exceed current time'
            }
          })
          break
        case 'csr':
          this.activityElement.style.backgroundColor = ''
          this.activityElement.title = ''

          this.statusElement.style.backgroundColor = ''
          this.statusElement.title = ''

          fetch('commLogPRG.php', {
            method: 'post',
            headers: { 'Content-Type': 'application/json;charset=utf8' },
            body: JSON.stringify({
              action: 'postInvoice',
              jobID: this.jobID,
              invID: this.invID,
              body: this.textArea.value,
              activityCode: this.activityElement.value,
              statusCode: this.statusElement.value,
            }),
          })
            .then((res) => res.json())
            .then((body) => {
              //TODO reset errors
              if (body.ok) {
                this.textArea.value = ''
                this.update()
              } else {
                //TODO failure to post note
                this.activityElement.style.backgroundColor = '#f66'
                this.activityElement.title = 'Activity/Status must both be set'

                this.statusElement.style.backgroundColor = '#f66'
                this.statusElement.title = 'Activity/Status must both be set'
              }
            })
          break
        default: // job, eqsJob, resJOB
          fetch('commLogPRG.php', {
            method: 'post',
            headers: { 'Content-Type': 'application/json;charset=utf8' },
            body: JSON.stringify({
              action: 'postJob',
              jobID: this.jobID,
              body: this.textArea.value,
            }),
          })
            .then((res) => res.json())
            .then((body) => {
              //TODO reset errors
              if (body.ok) {
                this.textArea.value = ''
                this.update()
              } else {
                //TODO failure to post note
              }
            })
          break
      }

      //change filter to current pages filter
      if(this.filter != this.page || this.filter != Filter.ALL)
        this.setFilter(this.page)

      //re-enable submit
      this.submitButton.disabled = false

    } else {
      this.textArea.style.backgroundColor = '#f66'
      this.textArea.title = 'Note must contain text'
    }
  }

  /**
   * Updates the DOM for messages based on new data coming in
   * @param {JSON} data The data to update the commlog with, if any
   */
  async update(data = undefined) {
    //remove all current commlogs
    this.itemBody.querySelectorAll('.com-item').forEach((element) => {
      element.remove()
    })
    this.items = []

    //if data is provided, use that data to populate
    if (data !== undefined) {
      for (const key in data) {
        if (data.hasOwnProperty(key)) {
          const element = data[key];
          let newNote = new CommLogItem(this, element, this.timezoneData)
          await newNote.populate()
          this.items.push(newNote)
    
          this.setFilter(this.filter)
        }
      }
    } else {
      //fetch all the commlogs
      fetch('commLogPRG.php', {
        method: 'post',
        headers: { 'Content-Type': 'application/json;charset=utf8' },
        body: JSON.stringify({
          action: 'get',
          jobID: this.jobID,
        }),
      })
        .then((res) => res.json())
        .then(async (body) => {
          if (body.ok) {
            //add all new commlogs
            for (const key in body.msg) {
              if (body.msg.hasOwnProperty(key)) {
                const element = body.msg[key];
                
                let newNote = new CommLogItem(this, element, this.timezoneData)
                await newNote.populate()
                this.items.push(newNote)
              }
            }

            //update filter displays
            this.setFilter(this.filter)
          }
        })
    }
  }

  /**
   * Calls fetch to the proper endpoint, filling in the proper data
   * @param {String} action The action to perform (override/update/remove)
   * @param {Function} callback The callback function for the JSON results
   */
  fetchAPI(action, callback) {
    fetch('commLogPRG.php', {
      method: 'post',
      headers: { 'Content-Type': 'application/json;charset=utf8' },
      body: JSON.stringify({
        action: action,
        jobID: this.jobID,
        body: this.textArea.value,
        date: this.dateField.value,
        time: this.timeField.value,
        timeOffset: -6 - Number(this.timezoneData.tz_offset),
      }),
    })
      .then((res) => {
        return res.json()
      })
      .then(callback)
  }
}
