import { default as messageNote } from '../modules/lib/note.js'
import { Util } from "../util.js"

export class PostJob{
  constructor(data) {
    this.data = data
    this.baseElement = document.querySelector('[name=postJob]')
    this.bodyElement = document.querySelector('[name=postJobBody')
    this.dom = Util.parseDOM(this.baseElement)

    // display post job list
    for (const postJobKey in this.data.postJob) {
      const row = this.data.postJob[postJobKey];
      let eTR = document.createElement('tr')

      eTR.innerHTML = this.data.itemTemplate
      eTR.querySelector('input[name="post-job-id"]').value = row.post_job_id;
      eTR.querySelector('[name=recommend]').innerHTML = row.post_job_name;

      eTR.querySelector('[name="post-job-note"]').disabled = true;
      eTR.querySelector('button[name="post-job-save"]').hidden = true;

      if ( row.jpj_post == row.post_job_id) {   // data exist
        eTR.querySelector('[name="post-job-note"]').disabled = false;
        eTR.querySelector('button[name="post-job-save"]').hidden  = false;
      
        eTR.querySelector('[name="post-job-note"]').innerHTML = row.jpj_note;
        eTR.querySelector('[name="jpj-id"]').value = row.jpj_id;
        eTR.querySelector('[name="post-job-check"]').checked = true;
      }
      this.bodyElement.appendChild(eTR)
    }

    // checkbox event
    document.querySelectorAll('[name="post-job-check"]').forEach(item => {
      item.addEventListener('click', ((event) => {
        let p      = event.target.closest('tr');
        let pjNote = p.querySelector('textarea[name="post-job-note"]').value;
        let eCheck = p.querySelector('input[name="post-job-check"]');
        let pid    = p.querySelector('input[name="post-job-id"]').value;
        let id     = p.querySelector('input[name="jpj-id"]').value;

        if ( eCheck.checked == true ) {
          p.querySelector('[name="post-job-note"]').disabled = false;
          p.querySelector('button[name="post-job-save"]').hidden  = false;
        } else {
          p.querySelector('[name="post-job-note"]').disabled = true;
          p.querySelector('button[name="post-job-save"]').hidden  = true;
          if ( pjNote != '' ) {
            p.querySelector('[name="post-job-note"]').value = '';
            this.deletePostJobObject(pid)

          }
        }
      }).bind(this));
    })

    document.querySelectorAll('button[name="post-job-save"]').forEach(item => {
      item.addEventListener('click', ((event) => {
        
        let parent        = event.target.parentElement.parentNode; // closet('tr')
        let note          = parent.querySelector('textarea[name="post-job-note"]').value;
        let ePostJobCheck = parent.querySelector('input[name="post-job-check"]');
        let postJobID     = parent.querySelector('input[name="post-job-id"]').value;
        let jpjID         = parent.querySelector('input[name="jpj-id"]').value;

        ePostJobCheck.checked = true;
        this.addPostJobObject(jpjID,postJobID,note)
        
      }).bind(this));
    })
  }
 
  addPostJobObject(jpjID,postJobID,note){
    //prepare data
    let data = {
      jpjID: jpjID,
      jobID: this.data.jobID,
      postJobID: postJobID,
      postJobNote: note
    }
    //send fetch request
    fetch('/jif/postjob/addPostJob', {
      method: 'post',
      headers: {'Content-Type':'application/json;charset=utf8'},
      body: JSON.stringify(data)
    })
      .then(res => res.json())
      .then(body => {
        if(body.ok){
          messageNote({ text: 'Post Job info has been saved.' })
        } else {
          messageNote({ text: 'There has been an error with your request.', class: 'error' })
        }
      })
  }

  deletePostJobObject(postJobID){
    //prepare data
    let data = {
      jobID: this.data.jobID,
      postJobID: postJobID
    }
    //send fetch request
    fetch('/index.php?route=jif/postjob/deletePostJob', {
      method: 'post',
      headers: {'Content-Type':'application/json;charset=utf8'},
      body: JSON.stringify(data)
    })
      .then(res => res.json())
      .then(body => {
        if(body.ok){
          messageNote({ text: 'Post Job info has been deleted.' })
        } else {
          messageNote({ text: 'There has been an error with your request.', class: 'error' })
        }
      })
  }
}