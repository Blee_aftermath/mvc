/**
 * Contains styling structures for the checkmark
 */
const isPass = [
  {
    class: 'jif-check-red',
    src: 'icons/no16.png',
    title: 'Incomplete',
  },
  {
    class: 'jif-check-green',
    src: 'icons/ys16.png',
    title: 'Complete',
  },
  {
    class: 'jif-check-yellow',
    src: 'icons/ys16.png',
    title: 'Complete - Overridden',
  },
]

/**
 * An item for the jif checklist
 * @author Jake Cirino
 */
export class ChecklistItem {
  /**
   * Creates a new checklist item
   * @param {Object} parent The parent class, the jifSidebar this is contained in
   * @param {JSON} data Data for this checklist item
   */
  constructor(parent, data) {
    this.parent = parent
    this.data = data

    //redefine clickExpander so that we can add/remove the event listener
    this.expanderClick = this.expanderClick.bind(this)

    //create main element
    this.headElement = document.createElement('div')

    //create info element
    this.createInfoElement(data.name)
    this.headElement.appendChild(this.infoElement)

    //create dropdown element
    this.createDropdownElement()
    this.headElement.appendChild(this.dropdownElement)
    this.setExpanded(false)

    //append elements
    this.parent.baseElement.appendChild(this.headElement)

    this.setIsPass(this.data.isPass)
  }

  /**
   * Creates the info element
   * @param {String} title Title of the checklist item
   * @param {Number} status Checkbox status
   */
  createInfoElement(title, status = 0) {
    //create elements
    this.infoElement = document.createElement('div')
    this.infoElement.classList.add('jif-checklist-item')

    this.expanderElement = document.createElement('img')
    this.expanderElement.src = 'icons/dn16.png'
    this.expanderElement.title = ''
    this.expanderElement.className = 'jif-expander hidden'
    this.infoElement.appendChild(this.expanderElement)

    this.titleElement = document.createElement('span')
    this.titleElement.innerText = title

    this.checkElement = document.createElement('img')

    //append elements
    this.infoElement.appendChild(this.titleElement)
    this.infoElement.appendChild(this.checkElement)
  }

  /**
   * Creates dropdown element
   */
  createDropdownElement() {
    //create elements
    this.dropdownElement = document.createElement('form')
    this.dropdownElement.classList.add('jif-dropdown')

    this.textArea = document.createElement('textarea')
    this.textArea.rows = 5
    this.textArea.setAttribute('check-id', this.data.checkID)
    this.textArea.value = this.data.reason
    this.textArea.maxLength = 90

    //remove animation on textarea after it ends
    this.textArea.addEventListener(
      'webkitAnimationEnd',
      () => {
        this.style.animationName = ''
        this.style.backgroundColor = 'white'
      },
      false
    )

    this.submitButton = document.createElement('input')
    this.submitButton.type = 'submit'
    this.submitButton.style.display = 'inline-block'
    this.submitButton.style.margin = 'auto'
    this.submitButton.addEventListener(
      'click',
      this.submitOverride.bind(this),
      false
    )

    this.removeButton = document.createElement('input')
    this.removeButton.type = 'button'
    this.removeButton.value = 'Remove'
    this.removeButton.style.margin = 'auto'
    this.removeButton.addEventListener(
      'click',
      this.submitRemove.bind(this),
      false
    )

    //append elements
    this.dropdownElement.appendChild(this.textArea)
    this.dropdownElement.appendChild(this.submitButton)
    this.dropdownElement.appendChild(this.removeButton)
  }

  //setters & getters

  /**
   * Sets whether the dropdown is expanded or not
   * @param {Boolean} expanded
   */
  setExpanded(expanded) {
    if (this.data.skippable) {
      if (expanded) {
        //expand
        this.expanderElement.style.transform = ''
        this.expanderElement.title = 'Collapse'
        this.dropdownElement.classList.add('open')
        this.textArea.focus()
      } else {
        //collapse
        this.expanderElement.style.transform = 'rotate(-90deg)'
        this.expanderElement.title = 'Expand'
        this.dropdownElement.classList.remove('open')
      }

      this.expanded = expanded
    }
  }

  /**
   * Sets the job_check_is_pass status of the checklist item
   * @param {Number} status
   */
  setIsPass(status) {
    //set icon
    //console.log(status)
    let checkStyle = isPass[status]
    this.checkElement.src = checkStyle.src
    this.checkElement.title = checkStyle.title
    this.checkElement.className = checkStyle.class

    //if the check is pass and the element is skippable, we dont want this element to be expandable
    if (status == 1 && this.data.skippable) {
      //hide expander options
      this.infoElement.style.cursor = 'auto'
      this.expanderElement.className = 'jif-expander hidden'
      this.infoElement.removeEventListener('click', this.expanderClick, false)

      //collapse expander
      this.setExpanded(false)
    } else if (this.data.skippable) {
      //show expander options
      this.infoElement.style.cursor = 'pointer'
      this.expanderElement.className = 'jif-expander'
      this.infoElement.addEventListener(
        'click',
        this.expanderClick,
        false
      )
    }

    //set buttons
    if (this.data.skippable) {
      this.submitButton.value = status != 2 ? 'Override' : 'Update'
      this.removeButton.style.display = status == 2 ? 'inline-block' : 'none'

      //remove textarea value if pass ends up being false
      if (status != 2) this.textArea.value = ''
    }

    this.status = status
  }

  /**
   * Action on the expander click event.
   * @param {Event} event
   */
  expanderClick(event) {
    this.setExpanded(!this.expanded)
  }

  /**
   * Submits the removal of an override
   * @param {Event} event
   */
  submitRemove(event) {
    event.preventDefault()

    this.fetchAPI('remove', (body) => {
      if (body.ok) {
        //set is pass and collapse form
        this.setIsPass(body.isPass)
        this.setExpanded(false)

        //call update
        window.jifController.update(this.parent)
      }
    })
  }

  /**
   * Handles click event for submitting/updating override on a checklist item
   * @param {Event} event The click event
   */
  submitOverride(event) {
    event.preventDefault()

    if (this.textArea.value.length > 20 && this.textArea.value.length <= 90) {
      this.fetchAPI(this.submitButton.value.toLowerCase(), (body) => {
        //handle response
        console.log(body)
        if (body.ok) {
          //update isPass and collapse form
          this.textArea.style.backgroundColor = 'white'
          this.setIsPass(body.isPass)
          this.setExpanded(false)

          //call update
          window.jifController.update(this.parent)
        } else {
          //update failed
          this.textArea.style.backgroundColor = '#f66'
          this.textArea.title = body.msg
        }
      })
    } else {
      this.textArea.style.backgroundColor = '#f66'
      this.textArea.title = 'Override reason must be at least 20 characters'
    }
  }

  /**
   * Calls fetch to the proper endpoint, filling in the proper data
   * @param {String} action The action to perform (override/update/remove)
   * @param {Function} callback The callback function for the JSON results
   */
  fetchAPI(action, callback) {
    fetch('checklistPRG.php', {
      method: 'post',
      headers: { 'Content-Type': 'application/json;charset=utf8' },
      body: JSON.stringify({
        action: action,
        jobCheckID: this.textArea.getAttribute('check-id'),
        jobID: this.parent.jobID,
        phaseID: this.parent.phaseID,
        reason: this.textArea.value,
      }),
    })
      .then((res) => {
        return res.ok
          ? res.json()
          : { ok: false, msg: 'Error with server request.' }
      })
      .then(callback)
  }
}
