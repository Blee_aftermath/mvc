import { Util } from '../util.js'
import { WBGTTestingItem } from './wbgtTestingItem.js'

/**
 * WBGT Tab functionality
 * @author Jake Cirino
 */
export class WBGTTesting {
  constructor(data) {
    this.data = data
    this.items = []
    console.log(this.data)

    //create base element
    this.baseElement = document.createElement('div')
    this.baseElement.innerHTML = this.data.template

    //select/set elements
    this.dateField = this.baseElement.querySelector("[name='date']")
    this.timeField = this.baseElement.querySelector("[name='time']")
    this.readingField = this.baseElement.querySelector("[name='reading']")
    this.addButton = this.baseElement.querySelector("[name='addButton']")
    this.contentBody = this.baseElement.querySelector("[name='content-body']")
    this.inputRow = this.contentBody.children[0]

    //populate items
    this.populate()

    //add button functionality
    this.addButton.addEventListener('click', () => {
      fetch('wbgtPRG.php', {
        method: 'post',
        headers: { 'Content-Type': 'application/json;charset=utf8' },
        body: JSON.stringify({
          action: 'addItem',
          jobID: this.data.jobID,
          wbgt_dt: Util.dateToString(Util.parseDate(this.dateField.value, this.timeField.value)),
          wbgt_reading: this.readingField.value,
        }),
      })
        .then((res) => res.json())
        .then((body) => {
          if (body.ok) {
            //update DOM if result successful
            this.update()
          }
        })
    })

    //append to document
    document.querySelector('.body_jif_l').appendChild(this.baseElement)

    //setup datepicker
    $(document).ready(() => {
      $(() => {
        $("[name|='date']").datepicker()
        $("[name|='date']").datepicker('option', 'dateFormat', 'mm/dd/y')
      })
    })
  }

  /**
   * Populate items based off current data
   */
  populate() {
    //remove items
    for (const key in this.items) {
      if (this.items.hasOwnProperty(key)) {
        const element = this.items[key]

        //remove item
        element.delete()
      }
    }
    this.items = []

    //add items
    for (const key in this.data.items) {
      if (this.data.items.hasOwnProperty(key)) {
        const element = this.data.items[key]

        //setup data
        let data = element
        data.template = this.data.templateItem

        //create and append wbgt item
        this.items.push(new WBGTTestingItem(this, data))
      }
    }
  }

  /**
   * Gets a new list of items and updates the dom
   */
  update() {
    fetch('wbgtPRG.php', {
      method: 'post',
      headers: { 'Content-Type': 'application/json;charset=utf8' },
      body: JSON.stringify({
        action: 'getItems',
        jobID: this.data.jobID,
      }),
    })
      .then((res) => res.json())
      .then((body) => {
        if (body.ok) {
          //repopulate DOM
          this.data.items = body.msg
          this.populate()
        }
      })
  }
}
