import { TimesheetItem } from './timesheetItem.js'
import { Util } from '../util.js'
import { TimesheetInfoItem } from './timesheetInfoItem.js'
import { TimesheetBreakItem } from './timesheetBreakItem.js'

/**
 * Section for entering timesheet data
 * @author Jake Cirino
 */
export class Timesheet {
  /**
   * Creates a new timesheet item
   * @param {JSON} data
   * @param {boolean} includeSummary
   */
  constructor(data) {
    this.data = data
    this.includeSummary = this.data.primaryInv != null
    this.items = []
    this.priInfoItems = []
    this.secInfoItems = []
    this.allItems = []
    this.breakItems = []
    this.eachDay = {}

    //convert estimate data fields
    let filteredPriEst = {}
    for (const key in this.data.primaryEstimate) {
      if (this.data.primaryEstimate.hasOwnProperty(key)) {
        const element = this.data.primaryEstimate[key]
        let jiniKey = key.replace('jcur_', 'jini_')

        filteredPriEst[jiniKey] = element
      }
    }
    this.data.primaryEstimate = filteredPriEst

    //filter secondary estimate if it exists
    if (this.data.secondaryEstimate != null) {
      let filteredSecEst = {}
      for (const key in this.data.secondaryEstimate) {
        if (this.data.secondaryEstimate.hasOwnProperty(key)) {
          const element = this.data.secondaryEstimate[key]
          let jiniKey = key.replace('jcur_', 'jini_')

          filteredSecEst[jiniKey] = element
        }
      }
      this.data.secondaryEstimate = filteredSecEst
    }

    //create base element
    this.baseElement = document.createElement('div')
    this.baseElement.style.backgroundColor = 'white'

    this.populate()
  }

  populate() {
    //load html template
    this.baseElement.innerHTML = window.mainTemplate
    this.dom = Util.parseDOM(this.baseElement)

    //select/set info sections
    if(this.includeSummary){
      this.priElement = this.baseElement.querySelector("[name='info-pri-totals']")
      this.priTitle = this.priElement.querySelector("[name='info-pri-title']")
      this.secElement = this.baseElement.querySelector("[name='info-sec-totals']")
      this.secTitle = this.secElement.querySelector("[name='info-sec-title']")
      this.totalElement = this.baseElement.querySelector("[name='info-totals']")
    }else{
      this.baseElement.querySelector("[name=infoSection]").style.display = 'none'
      this.baseElement.querySelector("[name=infoSeparator]").style.display = 'none'
    }

    //select/set main fields
    this.activityField = this.baseElement.querySelector("[name='ts_activity']")
    this.employeeMultiField = this.baseElement.querySelector("[name='ts_emp_multi']")
    this.scopeField = this.baseElement.querySelector("[name='ts_seq']")
    this.billableField = this.baseElement.querySelector("[name='ts_is_billable']")
    this.respiratorField = this.baseElement.querySelector("[name='ts_respirator_worn']")
    this.startDateField = this.baseElement.querySelector("[name='startDate']")
    this.startTimeField = this.baseElement.querySelector("[name='startTime']")
    this.endDateField = this.baseElement.querySelector('[name=endDate]')
    this.endTimeField = this.baseElement.querySelector("[name='endTime']")
    this.durationField = this.baseElement.querySelector("[name='duration']")

    //setup pri/sec sections and titles
    if(this.includeSummary){
      this.priTitle.innerText = `${this.data.primaryInv} Subtotal`
      if (this.data.secondaryInv != null) {
        this.secElement.style.display = '' //set secondary subtotal visible
        this.secTitle.innerText = `${this.data.secondaryInv} Subtotal` //set secondary title
      }
    }

    //select/set break buttons

    //populate activity items
    for (const key in this.data.activities) {
      if (this.data.activities.hasOwnProperty(key)) {
        const activity = this.data.activities[key]

        //append selectbox items
        let option = document.createElement('option')
        option.value = activity.act_id
        option.innerText = activity.act_dsca
        this.activityField.appendChild(option)

        //append total objects
        if (activity.act_def_billable == 1) {
          //create normal element
          let initialHoursPri = this.data.primaryEstimate['jini_hr_' + activity.act_code.toLowerCase()]
          //create pri item
          if(this.includeSummary){
            let priItem = new TimesheetInfoItem(
              this,
              activity,
              activity.act_def_billable == 1 ? 'pri' : 'all',
              initialHoursPri
            )
            this.priInfoItems.push(priItem)
            priItem.populate()
          }

          //create sec item if we have a sec scope
          //TODO initial hours sec
          if (this.data.secondaryInv != null && this.includeSummary) {
            let initialHoursSec = this.data.secondaryEstimate['jini_hr_' + activity.act_code.toLowerCase()]
            let secItem = new TimesheetInfoItem(
              this,
              activity,
              activity.act_def_billable == 1 ? 'sec' : 'all',
              initialHoursSec
            )
            this.secInfoItems.push(secItem)
            secItem.populate()
          }
        } else {
          if(this.includeSummary){
            //create billable item
            let initialHours = this.data.initialHours['jini_hr_' + activity.act_code.toLowerCase()]
            let allItem = new TimesheetInfoItem(this, activity, 'all', initialHours)
            this.allItems.push(allItem)
            allItem.populate()
          }
        }
      }
    }
    
    this.employeeMultiField.setItems(this.data.employees)

    //populate scope items
    //set primary inv if unset
    this.data.primaryInv = this.data.primaryInv != null ? this.data.primaryInv : 'Pre-Est'
    let priOption = document.createElement('option')
    priOption.value = 1
    priOption.innerText = this.data.primaryInv
    this.scopeField.appendChild(priOption)

    if (this.data.secondaryInv != null) {
      let secOption = document.createElement('option')
      secOption.value = 2
      secOption.innerText = this.data.secondaryInv
      this.scopeField.appendChild(secOption)
    }

    //populate break items
    for (const key in this.data.breakItems) {
      if (this.data.breakItems.hasOwnProperty(key)) {
        const element = this.data.breakItems[key]
        let breakItem = new TimesheetBreakItem(this, element)
        this.breakItems.push(breakItem)
        breakItem.populate()
      }
    }

    //calculate duration on time changed
    this.startDateField.addEventListener('change', this.recalculateDuration.bind(this))
    this.startTimeField.addEventListener('change', this.recalculateDuration.bind(this))
    this.endDateField.addEventListener('change', this.recalculateDuration.bind(this))
    this.endTimeField.addEventListener('change', this.recalculateDuration.bind(this))

    //setup activity field
    this.activityField.addEventListener(
      'change',
      (() => {
        let act_id = this.activityField.value
        
        //find the item with the act id
        for (const key in this.data.activities) {
          if (this.data.activities.hasOwnProperty(key)) {
            const element = this.data.activities[key];
            const elementActID = element.act_id

            if(elementActID == act_id){
              this.billableField.checked = element.act_def_billable == 1
              this.respiratorField.checked = element.act_def_respirator == 1
              break
            }
          }
        }
      }).bind(this)
    )

    this.startTimeField.addEventListener('change', (event) => {
      try {
        let startDatetime = Util.parseDate(this.startDateField.value, this.startTimeField.value)

        //calculate next time and set date/time values
        const activityID = this.activityField.value
        let activityTimeframe = 15
        try {
          if (Number.parseInt(activityID) == 92) { //lunch is 30m
            activityTimeframe = 30
          }
        } catch (ex) {}

        const timePeriod = new Date(1000 * 60 * activityTimeframe) //15 mins
        let nextTime = new Date(startDatetime.getTime() + timePeriod.getTime()),
          nextStartTime = Util.dateToString(nextTime.getTime()).split(' ')

        //set date/time values and recalculate the duration
        this.endDateField.value = nextStartTime[0]
        this.endTimeField.value = nextStartTime[1]
        this.recalculateDuration()
      } catch (ex) {}
    })

    //setup add button
    this.baseElement.querySelector("[name='addButton']").addEventListener('click', this.insertItems.bind(this))

    //setup break save buttons
    this.baseElement.querySelector("[name='btnBreaksAll']").addEventListener('click', () => {
      //loop through and save items
      for (const key in this.breakItems) {
        if (this.breakItems.hasOwnProperty(key)) {
          const element = this.breakItems[key]
          element.setAllBreaksTaken()
          element.save()
        }
      }

      window.jifController.update()
    })
    this.baseElement.querySelector("[name='btnBreaksSave']").addEventListener('click', () => {
      //loop through and save items
      for (const key in this.breakItems) {
        if (this.breakItems.hasOwnProperty(key)) {
          const element = this.breakItems[key]
          element.save()
        }
      }

      window.jifController.update()
    })

    document.addEventListener('crewChangeEvent', ev => {

      const crewSupAsn = document.getElementById('spanCrewSup').querySelector('[name=selSupAsn]'),
            crewTecAsn = document.getElementById('spanCrewTec').querySelector('[name=selTecAsn]')

      /*
      this.data.employees = []

      if (crewSupAsn.childElementCount) {
        for (const option of crewSupAsn.querySelectorAll('option'))
          this.data.employees.push(JSON.parse(option.dataset.emp))
      }

      if (crewTecAsn.childElementCount) {
        for (const option of crewTecAsn.querySelectorAll('option'))
          this.data.employees.push(JSON.parse(option.dataset.emp))
      }*/

      fetch('timesheetPRG.php', {
        method: 'post',
        headers: {'Content-Type': 'application/json;charset=utf8'},
        body: JSON.stringify({
          action: 'getTimesheetEmployees',
          jobID: this.data.jobID
        })
      })
      .then(res => res.json())
      .then(body => {
        if(body.ok){
          this.data.employees = body.data
          this.employeeMultiField.setItems(this.data.employees)
        }
      })

      //this.employeeMultiField.setItems(this.data.employees)
    })

    //data population
    this.populateItems()

    //2021-01-08: Revised from append -> insertBefore as a php include block was added to appear below timesheet
    //document.querySelector('.body_jif_l').appendChild(this.baseElement)
    const jifBody = document.querySelector('.body_jif_l')
    jifBody.insertBefore(this.baseElement, jifBody.querySelector('#blockJifCrew'))
    
    window.jifController.registerComponent(this)

    //setup datepickers
    $(document).ready(function () {
      $(function () {
        $("[name|='startDate']").datepicker({
          onSelect: this.recalculateDuration.bind(this)
        })
        $("[name|='startDate']").datepicker('option', 'dateFormat', 'mm/dd/y')
        $("[name|='endDate']").datepicker({
          onSelect: this.recalculateDuration.bind(this)
        })
        $("[name|='endDate']").datepicker('option', 'dateFormat', 'mm/dd/y')
      }.bind(this))
    }.bind(this))

    //default date to current day
    let currentDate = Util.dateToString(new Date()).split(' ')[0]
    this.startDateField.value = currentDate
    this.endDateField.value = currentDate
  }

  /**
   * Populates the timesheet items with the current set data
   */
  async populateItems() {
    //remove previous items
    for (const index in this.items) {
      if (this.items.hasOwnProperty(index)) {
        const item = this.items[index]
        item.baseElement.remove()
      }
    }
    this.items = []

    for (const index in this.data.timesheetItems) {
      if (this.data.timesheetItems.hasOwnProperty(index)) {
        const item = this.data.timesheetItems[index]
        const timesheetItem = new TimesheetItem(
          this,
          this.data.activities,
          this.data.employees,
          this.data.selectedScopes,
          item
        )
        this.items.push(timesheetItem)
        timesheetItem.populate()
      }
    }

    this.highlightOverlaps()
    this.calculateInfoTotals()
  }

  /**
   * Sorts the order of the timesheet entries
   */
  sortItems() {
    //TODO sort items
  }

  /**
   * Recalculates duration
   */
  recalculateDuration() {
    try {
      let startDate = this.startDateField.value,
        startTime = this.startTimeField.value,
        endDate = this.endDateField.value,
        endTime = this.endTimeField.value,
        startDatetime = Util.parseDate(startDate, startTime),
        endDatetime = Util.parseDate(endDate, endTime)

      //adjust endDatetime to next day if needed
      //if (endDatetime.getTime() - startDatetime.getTime() < 0)
      //  endDatetime.setTime(endDatetime.getTime() + 1000 * 60 * 60 * 24)

      let duration = (endDatetime.getTime() - startDatetime.getTime()) / 1000 / 60 / 60
      duration = duration.toFixed(2)
      this.durationField.value = `${duration} Hr`
    } catch (err) {
      this.durationField.value = '0.00 Hr'
      return false
    }
  }

  /**
   * Highlights time overlaps across timesheet items
   */
  highlightOverlaps() {
    //unhighlight all items
    for (const key in this.items) {
      if (this.items.hasOwnProperty(key)) {
        const element = this.items[key]
        element.baseElement.style.backgroundColor = ''

        element.recalculateDuration()
      }
    }

    //highlight conflicting items
    for (let i1 = 0; i1 < this.items.length - 1; i1++) {
      const item1 = this.items[i1]
      for (let i2 = i1 + 1; i2 < this.items.length; i2++) {
        const item2 = this.items[i2]
        if (item1.compareTimes(item2)) {
          item1.baseElement.style.backgroundColor = '#f99'
          item2.baseElement.style.backgroundColor = '#f99'
        }
      }
    }
  }

  /**
   * Calculate totals in the info section
   */
  calculateInfoTotals() {
    let priTotals = {
      bill_sup: 0,
      bill_tec: 0,
      bill_tot: 0,
      prog_ini: 0,
      prog_diff: 0,
      nonbill_sup: 0,
      nonbill_tec: 0,
      nonbill_tot: 0,
    }
    let secTotals = {
      bill_sup: 0,
      bill_tec: 0,
      bill_tot: 0,
      prog_ini: 0,
      prog_diff: 0,
      nonbill_sup: 0,
      nonbill_tec: 0,
      nonbill_tot: 0,
    }
    let totalTotals = {
      bill_sup: 0,
      bill_tec: 0,
      bill_tot: 0,
      prog_ini: 0,
      prog_diff: 0,
      nonbill_sup: 0,
      nonbill_tec: 0,
      nonbill_tot: 0,
    }

    //calc pri totals
    for (const key in this.priInfoItems) {
      if (this.priInfoItems.hasOwnProperty(key)) {
        const element = this.priInfoItems[key]
        //calculate element
        element.calculate()

        //update totals
        priTotals.bill_sup += element.bilSup
        priTotals.bill_tec += element.bilTec
        priTotals.bill_tot += element.bilTot
        priTotals.prog_ini += !Number.isNaN(element.progIni) ? element.progIni : 0
        priTotals.prog_diff += !Number.isNaN(element.progDiff) ? element.progDiff : 0
        priTotals.nonbill_sup += element.nonbilSup
        priTotals.nonbill_tec += element.nonbilTec
        priTotals.nonbill_tot += element.nonbilTot
      }
    }

    //if secondary exists, calc sec totals
    if (this.data.secondaryInv != null) {
      for (const key in this.secInfoItems) {
        if (this.secInfoItems.hasOwnProperty(key)) {
          const element = this.secInfoItems[key]
          //calculate element
          element.calculate()

          //update totals
          secTotals.bill_sup += element.bilSup
          secTotals.bill_tec += element.bilTec
          secTotals.bill_tot += element.bilTot
          secTotals.prog_ini += !Number.isNaN(element.progIni) ? element.progIni : 0
          secTotals.prog_diff += !Number.isNaN(element.progDiff) ? element.progDiff : 0
          secTotals.nonbill_sup += element.nonbilSup
          secTotals.nonbill_tec += element.nonbilTec
          secTotals.nonbill_tot += element.nonbilTot
        }
      }
    }

    //call all totals
    for (const key in this.allItems) {
      if (this.allItems.hasOwnProperty(key)) {
        const element = this.allItems[key]
        //calculate element
        element.calculate()

        //update totals
        totalTotals.bill_sup += element.bilSup
        totalTotals.bill_tec += element.bilTec
        totalTotals.bill_tot += element.bilTot
        totalTotals.prog_ini += !Number.isNaN(element.progIni) ? element.progIni : 0
        totalTotals.prog_diff += !Number.isNaN(element.progDiff) ? element.progDiff : 0
        totalTotals.nonbill_sup += element.nonbilSup
        totalTotals.nonbill_tec += element.nonbilTec
        totalTotals.nonbill_tot += element.nonbilTot
      }
    }

    //finish total calcs
    for (const key in priTotals) {
      if (priTotals.hasOwnProperty(key)) {
        const priVal = priTotals[key],
          secVal = secTotals[key]

        totalTotals[key] += priVal + secVal
      }
    }

    //display total rows
    if(this.includeSummary){
      for (const key in priTotals) {
        if (priTotals.hasOwnProperty(key)) {
          const priVal = priTotals[key],
            secVal = secTotals[key],
            totalVal = totalTotals[key]
      
          this.priElement.querySelector(`[name='${key}']`).innerText = priVal
          this.secElement.querySelector(`[name='${key}']`).innerText = secVal
          this.totalElement.querySelector(`[name='${key}']`).innerText = totalVal
        }
      }
    }
  }

  /**
   * Removes all of the break items
   */
  removeBreakItems() {
    for (const key in this.breakItems) {
      if (this.breakItems.hasOwnProperty(key)) {
        const element = this.breakItems[key]
        element.baseElement.remove()
      }
    }
    this.breakItems = []
  }
  
  insertItems(){
    let activity = this.activityField.value,
      selectedEmps = this.employeeMultiField.getSelected(),
      seq = this.scopeField.value,
      billable = this.billableField.checked,
      respiratorWorn = this.respiratorField.checked,
      startDate = this.startDateField.value,
      endDate = this.endDateField.value,
      startTime = this.startTimeField.value,
      endTime = this.endTimeField.value,
      startDatetime = Util.parseDate(startDate, startTime),
      endDatetime = Util.parseDate(endDate, endTime),
      duration = this.durationField.value.split(' ')[0]
  
    //parse employee json values
    let employees = [];
    for (const key in selectedEmps) {
      if (selectedEmps.hasOwnProperty(key)) {
        const element = selectedEmps[key]
        employees[key] = JSON.parse(element)
      }
    }
  
    //TODO verify fields
    if (endDatetime.getTime() > startDatetime.getTime()) {
      fetch('timesheetPRG.php', {
        method: 'post',
        headers: {'Content-Type': 'application/json;charset=utf8'},
        body: JSON.stringify({
          action: 'addMultiTimesheetItems',
          ts_job: this.data.jobID,
          ts_activity: activity,
          employees: employees,
          ts_seq: seq,
          ts_is_billable: billable,
          ts_respirator_worn: respiratorWorn,
          ts_dt_fr: Util.dateToString(startDatetime),
          ts_dt_to: Util.dateToString(endDatetime),
          ts_duration: duration,
        }),
      })
        .then((res) => res.json())
        .then((body) => {
          if (body.ok) {
            //clear/set fields
            //change focus
            this.activityField.focus()
        
            //calculate next time and set date/time values
            const timePeriod = new Date(1000 * 60 * 15) //15 mins
            let nextTime = new Date(endDatetime.getTime() + timePeriod.getTime()),
              nextStartTime = Util.dateToString(endDatetime.getTime()).split(' ')
            nextTime = Util.dateToString(nextTime).split(' ')
        
            //set date/time values and recalculate the duration
            this.startDateField.value = nextStartTime[0]
            this.startTimeField.value = nextStartTime[1]
            this.endDateField.value = nextTime[0]
            this.endTimeField.value = nextTime[1]
            this.recalculateDuration()
        
            //add items
            //this.data.timesheetItems = body.msg
            //this.populateItems()
            for (const msgKey in body.msg) {
              const item = body.msg[msgKey];
              const timesheetItem = new TimesheetItem(
                this,
                this.data.activities,
                this.data.employees,
                this.data.selectedScopes,
                item
              )
              this.items.push(timesheetItem)
              timesheetItem.populate()
            }
        
            //jifcontroller update
            window.jifController.update()
          }
        })
    }
  }
  
  /**
   * Updates the timesheet listing in the block
   * Called by the jifcontroller on page update.
   * @param {Function} callback
   */
  update(callback) {
    //get timesheet items
    fetch('timesheetPRG.php', {
      method: 'post',
      headers: { 'Content-Type': 'application/json;charset=utf8' },
      body: JSON.stringify({
        action: 'getTimesheetItems',
        jobID: this.data.jobID,
      }),
    })
      .then((res) => res.json())
      .then((body) => {
        if (body.ok) {
          //repopulate fields
          this.data.timesheetItems = body.msg
          this.populateItems()
        }

        //get and populate break items
        fetch('timesheetPRG.php', {
          method: 'post',
          headers: { 'Content-Type': 'application/json;charset=utf8' },
          body: JSON.stringify({
            action: 'getBreakItems',
            jobID: this.data.jobID,
          }),
        })
          .then((res) => res.json())
          .then((body) => {
            if (body.ok) {
              //remove existing break items
              this.removeBreakItems()

              //populate break items
              for (const key in body.msg) {
                if (body.msg.hasOwnProperty(key)) {
                  const element = body.msg[key]
                  let breakItem = new TimesheetBreakItem(this, element)
                  this.breakItems.push(breakItem)
                  breakItem.populate()
                }
              }
            }

            callback()
          })
      })
  }
}
