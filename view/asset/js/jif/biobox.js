import { default as note } from '../modules/lib/note.js'
import { Util } from '../util.js'

String.prototype.toNum = function () {return Number(this)}
Number.prototype.toNum = function () {return Number(this)}

/**
 * Contains information on the biobox manifests used in the jobs
 * @author Scott Kiehn, Jake Cirino
 */
export class Biobox {
  /**
   * Creates a biobox waste manifest form
   * @param {JSON} data
   */
  constructor(data) {
    this.data = data
    this.jobID = this.data.jobID
    this.base = {}
    this.info = this.data.info
    this.dom = {}
    console.log(this.data)

    this.baseElement = document.createElement('div')
    this.baseElement.addEventListener('click', (ev) => this.clickEvent(ev.target))
    this.baseElement.innerHTML = data.template

    //select/set elements
    this.selInvoice = this.baseElement.querySelector("[name='bbx_inv']")
    this.selPurpose = this.baseElement.querySelector('[name=bbx_purpose]')

    // Collect DOM ids and place into an object
    for (const el of this.baseElement.querySelectorAll('[id]')) this.dom[el.id] = el

    // Add jQuery date picker
    $(this.dom.bbx_d_gen).datepicker()

    this.bbxForm()

    // Append content to document
    document.querySelector('.body_jif_l').appendChild(this.baseElement)
  }

  /**
   * Add a row to the biobox form
   * @param {Object} data
   * @returns {Object} HTML element  to append
   */
  bbxRow(item) {
    console.log(item)
    //TODO needs to accomodate bbx_seq select box
    const row = document.createElement('tr')
    row.setAttribute('id', `${item.bbx_id}`)
    row.setAttribute('class', 'table-input')
    row.innerHTML = `
      <td>
        <span>${item.bbx_seq == 1 ? this.data.primaryInv : this.data.secondaryInv}</span><span class='hide'>
        <select name='bbx_invoice'></select>
      </td>
      <td>
        <span>${item.bbx_purpose == 1 ? 'Suits' : 'Debris'}</span><span class='hide'>
        <select name='bbx_purpose' value='${item.bbx_purpose}'>
          <option value='1'>Suits</option>
          <option value='2'>Debris</option>
        </select>
      </td>
      <td>
        <span>${item.bbx_tag}</span><span class="hide"><input name="bbx_tag" type="text" value="${item.bbx_tag}" style="padding: 2px;" /></span>
      </td>
      <td>
        <span>${item.bbx_lbs}</span><span class="hide"><input name="bbx_lbs" type="number" value="${item.bbx_lbs}" min="0" step="0.1" /></span>
      </td>
      <td>
        <span>${this.dateMDY(item.bbx_d_gen)}</span><span class="hide"><input name="bbx_d_gen" class="datePicker" type="text" value="${this.dateMDY(item.bbx_d_gen)}" style="padding: 2px;" /></span>
      </td>
      <td>
        <span style="inline-block; float: right;"><img name="bbxEdtBtn" class="icon16" src="icons/edit16.png" style="padding: 0; cursor: pointer" />&nbsp;<img name="bbxDelBtn" class="icon16" src="icons/trash16.png" style="padding: 0; cursor: pointer; filter: grayscale(1)" /></span>
        <span class="hide"><button name="bbxSavBtn" style="width: 100%;">Save</button></span>
      </td>`

    //setup invoice options
    let invSelector = row.querySelector("[name='bbx_invoice']")
    let priInv = document.createElement('option')
    priInv.value = 1
    priInv.innerText = this.data.primaryInv
    invSelector.appendChild(priInv)

    //add secondary invoice if the invoice is defined
    if (this.data.secondaryInv != null) {
      let secInv = document.createElement('option')
      secInv.value = 2
      secInv.innerText = this.data.secondaryInv
      invSelector.appendChild(secInv)
    }
    invSelector.value = item.bbx_seq

    //update purpose checkbox
    let purposeSelector = row.querySelector('[name=bbx_purpose]')
    purposeSelector.value = item.bbx_purpose

    // Add jQuery date picker
    const datePicker = row.querySelector('.datePicker')
    $(datePicker).datepicker()

    return row
  }

  /**
   * Builds the biobox form
   * @param {Element} el
   */
  bbxForm() {
    //setup invoice options
    let priInv = document.createElement('option')
    priInv.value = 1
    priInv.innerText = this.data.primaryInv
    this.selInvoice.appendChild(priInv)

    //add secondary invoice if the invoice is defined
    if (this.data.secondaryInv != null) {
      let secInv = document.createElement('option')
      secInv.value = 2
      secInv.innerText = this.data.secondaryInv
      this.selInvoice.appendChild(secInv)
    }
    for (const item of this.info) this.dom.bbxList.insertBefore(this.bbxRow(item), this.dom.bbxAdd)
  }

  /**
   * Deletes a bio box record
   */
  async bbxDelete(row, bbxID = 0) {
    const post = await fetch('bioboxPRG.php', {
      method: 'post',
      headers: { 'Content-Type': 'application/json;charset=utf-8' },
      body: JSON.stringify({ action: 'delete', job: this.jobID, bbx_id: bbxID }),
    })

    const res = await post.json()

    if (res.ok) {
      row.parentNode.removeChild(row)

      //trigger jifcontroller update
      window.jifController.update()
    } else {
      note({ text: 'There has been an error with your request.', class: 'error' })
    }
  }

  /**
   * Creates a new biobox
   * @param {JSON} bbx
   */
  async addBiobox(bbx) {

    console.log(bbx)

    let error = 0
    if (bbx.bbx_lbs.toNum() <= 0) error++
    if (!(bbx.bbx_tag.length > 1 && bbx.bbx_tag.length < 46)) error++
    if (!bbx.bbx_d_gen.match(/\d\d\/\d\d\/\d\d\d\d/)) error++
    if (bbx.bbx_purpose.length == 0) error++
    if (bbx.bbx_seq.length == 0) error++

    if (error) {
      note({ text: 'Your data was incomplete. Please provide data for each field.', class: 'error', time: 4000 })
      return
    }

    await fetch('bioboxPRG.php', {
      method: 'post',
      headers: { 'Content-Type': 'application/json;charset=utf8' },
      body: JSON.stringify(bbx),
    })
      .then((res) => res.json())
      .then((body) => {
        if (body.ok) {
          note({ text: 'Your bio box info has been added.' })

          //reset input fields
          this.dom.bbx_tag.value = ''
          this.dom.bbx_lbs.value = '0.0'
          this.dom.bbx_d_gen.value = ''
          this.selInvoice.value = ''
          this.selInvoice.focus()

          //add new biobox item
          this.dom.bbxList.insertBefore(this.bbxRow(body.ok), this.dom.bbxAdd)

          //trigger jifcontroller update
          window.jifController.update()
        } else {
          note({ text: 'There has been an error with your request.', class: 'error' })
        }
      })
  }

  /**
   * Saves a bio box record
   * @param {JSON} bbx
   */
  async bbxSave(bbx) {
    let error = 0
    if (bbx.bbx_lbs.toNum() <= 0) error++
    if (!(bbx.bbx_tag.length > 1 && bbx.bbx_tag.length < 46)) error++
    if (!bbx.bbx_d_gen.match(/\d\d\/\d\d\/\d\d\d\d/)) error++
    //if (this.selInvoice.value == '') error++

    if (error) {
      note({ text: 'Your data was incomplete. Please provide data for each field.', class: 'error', time: 4000 })
      return
    }

    const post = await fetch('bioboxPRG.php', {
      method: 'post',
      headers: { 'Content-Type': 'application/json;charset=utf-8' },
      body: JSON.stringify({
        action: 'update',
        jobID: this.jobID,
        bbx_id: bbx.bbx_id,
        bbx_seq: bbx.bbx_seq,
        bbx_purpose: bbx.bbx_purpose,
        bbx_tag: bbx.bbx_tag,
        bbx_lbs: bbx.bbx_lbs,
        bbx_d_gen: bbx.bbx_d_gen,
      }),
    })

    const res = await post.json()

    if (res.ok) {
      note({ text: 'Your bio box info has been saved.' })
      if (bbx.bbx_id) {
        const row = document.getElementById(bbx.bbx_id)

        const bbxInv = row.querySelector('[name=bbx_invoice]')
        bbxInv.value = res.bbx.bbx_seq
        bbxInv.parentNode.previousSibling.innerHTML =
          res.bbx.bbx_seq == 1 ? this.data.primaryInv : this.data.secondaryInv

        const bbxPur = row.querySelector('[name=bbx_purpose]')
        bbxPur.value = res.bbx.bbx_purpose
        bbxPur.parentNode.previousSibling.innerHTML = res.bbx.bbx_purpose == 1 ? 'Suits' : 'Debris'

        const bbxTag = row.querySelector('[name=bbx_tag]')
        bbxTag.value = res.bbx.bbx_tag
        bbxTag.parentNode.previousSibling.innerHTML = res.bbx.bbx_tag

        const bbxLbs = row.querySelector('[name=bbx_lbs]')
        bbxLbs.value = res.bbx.bbx_lbs
        bbxLbs.parentNode.previousSibling.innerHTML = res.bbx.bbx_lbs

        const bbxDt = row.querySelector('[name=bbx_d_gen]')
        bbxDt.value = this.dateMDY(res.bbx.bbx_d_gen)
        bbxDt.parentNode.previousSibling.innerHTML = this.dateMDY(res.bbx.bbx_d_gen)

        for (const span of row.querySelectorAll('span')) {
          span.classList.toggle('hide')
        }
        //trigger jifcontroller update
        window.jifController.update()
      } else {
        this.dom.bbx_tag.value = ''
        this.dom.bbx_lbs.value = '0.0'
        this.dom.bbx_d_gen.value = ''
        this.dom.bbx_tag.focus()
        this.dom.bbxList.insertBefore(this.bbxRow(res.bbx), this.dom.bbxAdd)
      }
    } else {
      note({ text: 'There has been an error with your request.', class: 'error' })
    }
  }

  /**
   * Handles button actions
   * @param {Element} el
   */
  clickEvent(el) {
    switch (true) {
      case el.matches(`#${this.dom.bbxAddBtn.id}`):
        this.addBiobox({
          action: 'add',
          jobID: this.jobID,
          bbx_seq: this.selInvoice.value,
          bbx_purpose: this.selPurpose.value,
          bbx_tag: this.dom.bbx_tag.value,
          bbx_lbs: this.dom.bbx_lbs.value,
          bbx_d_gen: this.dom.bbx_d_gen.value || '',
        })
        break

      case el.matches('[name=bbxSavBtn]'):
        const rowSav = el.closest('tr')
        this.bbxSave({
          bbx_id: rowSav.id,
          bbx_seq: rowSav.querySelector('[name=bbx_invoice]').value,
          bbx_purpose: rowSav.querySelector('[name=bbx_purpose]').value,
          bbx_tag: rowSav.querySelector('[name=bbx_tag]').value,
          bbx_lbs: rowSav.querySelector('[name=bbx_lbs]').value,
          bbx_d_gen: rowSav.querySelector('[name=bbx_d_gen]').value || '',
        })
        break

      case el.matches('[name=bbxEdtBtn]'):
        const rowEdt = el.closest('tr')
        for (const span of rowEdt.querySelectorAll('span')) {
          span.classList.toggle('hide')
        }
        break

      case el.matches('[name=bbxDelBtn]'):
        const rowDel = el.closest('tr')
        this.bbxDelete(rowDel, rowDel.id.toNum())
        break
    }
  }

  /**
   * YMD to MDY
   * @param {String} ymd
   */
  dateMDY = (ymd) => {
    const d = ymd.split('-')
    return `${d[1]}/${d[2]}/${d[0]}`
  }
}
