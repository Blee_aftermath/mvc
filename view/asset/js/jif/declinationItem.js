/**
 * Represents a declination item
 * @author Jake Cirino
 */
import {Util} from "../util.js"

export class DeclinationItem{
  constructor(parent, data) {
    this.parent = parent
    this.data = data
    console.log(this.data)
    
    //create base element
    this.baseElement = document.createElement('tr')
    this.baseElement.innerHTML = window.declinationItem
    this.dom = Util.parseDOM(this.baseElement)
    
    //setup delete button
    this.dom.deleteButton.addEventListener('click', this.delete.bind(this))
    
    //populate fields and set the edit mode
    this.populate()
    
    //append the html element to the table
    let declineBody = this.parent.dom.declineBody
    declineBody.insertBefore(this.baseElement, declineBody.lastElementChild)
  }
  
  /**
   * Fills the dom with this data
   */
  populate(){
    const declItem = window.declination[this.data.jd_decl - 1]
    
    this.dom.recommend.innerText = declItem.decl_title
    this.dom.desc.innerText = declItem.decl_rec
    this.dom.areas.innerText = this.data.jd_aff_area
  }
  
  /**
   * Deletes this declination item
   */
  delete(){
    fetch('declinationPRG.php', {
      method: 'post',
      headers: {'Content-Type': 'application/json;charset=utf8'},
      body: JSON.stringify({
        action: 'deleteDeclination',
        jobID: this.data.jd_job,
        declination: this.data.jd_decl
      })
    })
      .then(res => res.json())
      .then(body => {
        if(body.ok){
          this.parent.items--
          this.parent.updateForm()
          this.baseElement.remove()
        }
      })
  }
}