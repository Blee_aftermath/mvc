/**
 * Controls the declination tab
 * @author Jake Cirino
 */
import {Util} from "../util.js"
import {DeclinationItem} from "./declinationItem.js"

export class Declination{
  constructor(data) {
    this.data = data
    this.baseElement = document.querySelector('[name=declination]')
    this.dom = Util.parseDOM(this.baseElement)
    
    //setup process selector
    for (const declinationKey in window.declination) {
      const row = window.declination[declinationKey];
      
      let opt = document.createElement('option')
      opt.value = row.decl_id
      opt.innerText = row.decl_title
      this.dom.declineRec.appendChild(opt)
    }
    
    //setup add button
    this.dom.addButton.addEventListener('click', (() => {
      //validate input
      let err = false
      if(this.dom.declineLocation.value.length == 0){
        err = true
        this.dom.declineLocation.style.backgroundColor = '#ff6666'
      }
      if(this.dom.declineRec.value == ''){
        err = true
        this.dom.declineRec.style.backgroundColor = '#f66'
      }
      
      if(!err) this.addDeclObject()
    }).bind(this))
    
    //add declination items
    this.items = 0
    for (const itemsKey in this.data.items) {
      let item = new DeclinationItem(this, this.data.items[itemsKey])
      this.items++
    }
    this.updateForm()
  }
  
  updateForm(){
    this.dom.addButton.disabled = this.items >= window.maxDeclination
  }
  
  /**
   * Clears the add form of its input
   */
  clearForm(){
    this.dom.declineLocation.style.backgroundColor = ''
    this.dom.declineLocation.value = ''
    
    this.dom.declineRec.style.backgroundColor = ''
    this.dom.declineRec.value = ''
    this.dom.declineRec.focus()
  }
  
  addDeclObject(){
    //prepare data
    let data = {
      action: 'addDeclination',
      jobID: this.data.jobID,
      process: this.dom.declineRec.value,
      area: this.dom.declineLocation.value
    }
    
    //send fetch request
    fetch('declinationPRG.php', {
      method: 'post',
      headers: {'Content-Type':'application/json;charset=utf8'},
      body: JSON.stringify(data)
    })
      .then(res => res.json())
      .then(body => {
        if(body.ok){
          //add object and clear form
          new DeclinationItem(this, {
            jd_job: this.data.jobID,
            jd_decl: this.dom.declineRec.value,
            jd_aff_area: this.dom.declineLocation.value
          })
          this.items++
          this.updateForm()
          this.clearForm()
        }
      })
  }
}