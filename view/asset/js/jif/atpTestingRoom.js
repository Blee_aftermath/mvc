import { ATPTestingItem } from "./atpTestingItem.js"

/**
 * An ATP testing room item
 * @author Jake Cirino
 */
export class ATPTestingRoom{
  /**
   * Creates a new ATP testing room item
   * @param {ATPTesting} parent 
   * @param {JSON} data 
   */
  constructor(parent, data){
    this.parent = parent
    this.data = data
    this.swabs = []
    console.log(this.data)

    //create base element
    this.baseElement = document.createElement('div')
    this.baseElement.innerHTML = this.data.roomTemplate

    //select/set elements
    this.room = this.baseElement.querySelector("[name='atpt_room']")
    this.roomSaveButton = this.baseElement.querySelector("[name='bedroomSave']")
    this.deleteButton = this.baseElement.querySelector("[name='deleteButton']")
    this.itemBody = this.baseElement.querySelector('tbody')
    this.inputSection = this.itemBody.children[0]
    this.surfaceField = this.baseElement.querySelector("[name='atps_surface']")
    this.preField = this.baseElement.querySelector("[name='atps_read_pre']")
    this.pfPreField = this.baseElement.querySelector("[name='pf-pre']")
    this.postField = this.baseElement.querySelector("[name='atps_read_pos']")
    this.pfPostField = this.baseElement.querySelector("[name='pf-post']")
    this.addButton = this.baseElement.querySelector("[name='add']")

    //populate fields
    this.room.value = this.data.atpt_room

    //setup room save button
    this.roomSaveButton.addEventListener('click', event => {
      fetch('atpPRG.php', {
        method: 'post',
        headers: {'Content-Type': 'application/json;charset=utf8'},
        body: JSON.stringify({
          action: 'updateRoomName',
          roomID: this.data.atpt_id,
          roomName: this.room.value
        })
      })
      .then(res => res.json())
      .then(body => {
        this.parent.update()
      })
    })

    //setup delete button
    this.deleteButton.addEventListener('click', event => {
      fetch('atpPRG.php', {
        method: 'post',
        headers: {'Content-Type': 'application/json;charset=utf8'},
        body: JSON.stringify({
          action: 'deleteRoom',
          roomID: this.data.atpt_id
        })
      })
      .then(res => res.json())
      .then(body => {
        this.parent.update()
      })
    })

    //setup add button
    this.addButton.addEventListener('click', () => {
      fetch('atpPRG.php', {
        method: 'post',
        headers: {'Content-Type': 'application/json;charset=utf8'},
        body: JSON.stringify({
          action: 'addSwab',
          roomID: this.data.atpt_id,
          surface: this.surfaceField.value,
          pre: this.preField.value * 10,
          post: this.postField.value * 10,
        })
      })
      .then(res => res.json())
      .then(body => {
        this.parent.update()
      })
    })

    //insert swab elements
    for (const key in this.data.swabs) {
      if (this.data.swabs.hasOwnProperty(key)) {
        const element = this.data.swabs[key];
        
        //setup data
        let data = element
        data.itemTemplate = this.data.itemTemplate

        //add swab item
        let swabItem = new ATPTestingItem(this, data)
        this.swabs.push(swabItem)
      }
    }

    //append to document
    this.parent.blockBody.appendChild(this.baseElement)
  }

  /**
   * Deletes this item from the DOM
   */
  delete(){
    this.baseElement.remove()
  }
}