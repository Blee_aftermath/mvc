/**
 * Cost margin calculation encapsulation
 * @author Scott Kiehn
 */

import { Util } from '../util.js'

String.prototype.toNum = function () {return Number(this)}
Number.prototype.toNum = function () {return Number(this)}

const strRegEx = /costlist_/

export class CostMarginCalc {
  /**
   * Creates a new cost margin
   * @param {JSON} data
   */
  constructor(data) {
    this.job          = data.job
    //console.log(this.job)
    this.costlist     = this.makeCostlist(data.costlist)

    // 2021-03-19 Realizations based on real data now in use
    this.realzns      = this.makeRealzns(data.realzns)
    // ********************************************************

    this.expense      = {}
    this.signal       = {text: 'Stop', color: 'hlneg', state: ''}
    this.threshold    = {stop: 15.0, consult: 40.0}

    // 2021-03-19 Hardcoded Realizations no longer in use, but still used in console comparison
    this.realized       = {INS:  0.75, NON: 0.90, 1: 'INS', 2: 'NON'}
    this.realized.label = this.job.jobPmtInsNon ? this.realized[this.job.jobPmtInsNon] : ' '
    this.realized.per   = this.job.jobPmtInsNon ? this.realized[this.realized.label]   : 1.00
    // ********************************************************
  }

  /**
   * Database to costlist object
   * @param {object} list
   */
  makeCostlist(list) {
    const costs = {}
    for (const key in list) {
      if (key.match(strRegEx)) {
        const key2 = key.replace(strRegEx, '')
        costs[key2] = isNaN(list[key].toNum()) ? list[key] : list[key].toNum()
      }
    }

    //costlist contains a join to the office record to determine PRM or STD for the following items
    costs.labor  = list.office_is_labor_stdprm == 2 ? costs.labor_prm  : costs.labor_std
    costs.drive  = list.office_is_drive_stdprm == 2 ? costs.drive_prm  : costs.drive_std
    costs.per_wc = list.office_is_wc_stdprm    == 2 ? costs.per_wc_prm : costs.per_wc_std
    return costs
  }

  /**
   * Get a relization item from realzns object
   * @param {int} amt
   */
  getRealzn(amt) {
    let tier = ''
    for (const key in this.realzns)
      if (amt > this.realzns[key].base) tier = key
    return this.realzns[tier] || this.realzns['T1']
  }

  /**
   * Database to realization object
   * @param {object} list
   */
  makeRealzns(list) {
    const realzns = {}
    for (const key in list) {
      const o = list[key]
      realzns[o.tier_code] = {
        base:      o.tier_fr.toNum(),
        percent:   o.per_real.toNum(),
        fraction:  (o.per_real/100).toFixed(3).toNum(),
        tag:       o.tag_real,
        tier:      o.tier_code,
        cap:       o.cap_real
      }
    }
    //console.log(realzns)
    return realzns
  }

  /**
   * Calculates cost margin
   */
  calcMargins() {

    const realizn                = this.getRealzn(this.job.quotePrice)

    if (this.job.jifConfirmed === 1 && this.job.jifPerRealEst) {
      realizn.percent  = this.job.jifPerRealEst
      realizn.fraction = (this.job.jifPerRealEst/100).toFixed(3).toNum()
    }

    this.job.roundTrips          = this.calcRoundTrips()
    this.job.driveTime           = this.job.transit * this.job.roundTrips * 2 // 2 for there and back again
    this.expense.bioboxExp       = this.costlist.sup_biobox * this.job.bioboxes
    this.expense.laborExp        = this.costlist.labor * this.job.manHours
    this.expense.driveExp        = this.costlist.drive * this.job.crew * this.job.driveTime
    this.expense.nonbillHoursExp = this.job.manHours * this.costlist.nonbill_rate
    this.expense.payrollExp      = this.calcPayrollExp()
    this.expense.fuelExp         = this.calcFuelExp()
    this.expense.hotelExp        = Math.round(this.job.crew * this.job.hotels * this.costlist.exp_hotel)
    this.expense.vehicleExp      = Math.round(this.costlist.exp_vehicle * this.job.trucks * this.job.driveTime)
    this.expense.rentalExp       = this.job.rentals * this.costlist.rental
    this.expense.branchOvhExp    = this.job.manHours * this.costlist.branch_ovh
    this.expense.dumpsterExp     = this.job.dumpsters * this.costlist.dumpster
    this.expense.bagsterExp      = this.job.bagsters * this.costlist.bagster
    this.expense.supplyEquipExp  = Math.round((this.job.supplies + this.job.equipment) * (this.costlist.per_supplies/100))
    this.expense.internalExp     = this.job.internalExp
    this.job.estCOGS             = this.calcEstCOGS()
    this.job.anticipColl         = Math.round(this.job.quotePrice * realizn.fraction)
    this.job.grossProfit         = this.job.anticipColl - this.job.estCOGS
    this.job.costMargin          = this.calcCostMargin()

    this.job.realiznInfo         = `${realizn.tier}|${realizn.tag}|${realizn.percent}${realizn.cap ? '|' + realizn.cap : ''}${this.job.jifConfirmed ? '|CNF' : ''}`
    this.job.realiznInfo2        = `${realizn.tier.replace(/^T/, 'Tier ')} | ${realizn.tag} | ${realizn.percent}%${realizn.cap ? ' | ' + realizn.cap : ''}${this.job.jifConfirmed ? ' | Confirmed' : ''}`

    // Console log comparison: Old hardcoded realizations (this.realized.per) VS DB realizatons (realizn.fraction)
    const OLDanticipColl = Math.round(this.job.quotePrice * this.realized.per),
          OLDgrossProfit = OLDanticipColl - this.job.estCOGS,
          OLDcostMargin  = this.job.quotePrice == 0 ? 0 : ((OLDgrossProfit / this.job.quotePrice) * 100).toFixed(1).toNum()

    console.log('#######################################################')

    console.log(`Total:                ${this.pad(Util.formatCurrency(this.job.quotePrice))} | ${Util.formatCurrency(this.job.quotePrice)}`)
    console.log(`Estimated COGS:       ${this.pad(Util.formatCurrency(this.job.estCOGS))} | ${Util.formatCurrency(this.job.estCOGS)}`)
    console.log(`Realization:          ${this.pad(this.realized.label + ', ' + (this.realized.per * 100).toFixed(0) + '%')} | ${this.job.realiznInfo}`)
    console.log(`Anticip. Collections: ${this.pad(Util.formatCurrency(OLDanticipColl))} | ${Util.formatCurrency(this.job.anticipColl)}`)
    console.log(`Job Gross Profit:     ${this.pad(Util.formatCurrency(OLDgrossProfit))} | ${Util.formatCurrency(this.job.grossProfit)}`)
    console.log(`Job Profit %:         ${this.pad(OLDcostMargin + '%')} | ${this.job.costMargin}%`)

    console.log('#######################################################')

    if (this.job.costMargin < this.threshold.stop) {
      this.signal.text  = 'Stop'
      this.signal.color = 'hlneg'
      this.job.ok       = false
      this.job.approval = 'JA2'
    } else if (this.job.costMargin < this.threshold.consult) {
      this.signal.text  = 'Consult'
      this.signal.color = 'hlyel'
      this.job.ok       = false
      this.job.approval = 'JA1'
    } else {
      this.signal.text  = 'Proceed'
      this.signal.color = 'hlpos'
      this.job.ok       = true
      this.job.approval = ''
    }

    if (this.signal.text != this.signal.state) {
      //console.log(`########## BOOM: ${this.signal.text} ##########`)
      this.job.boom = true
      this.signal.state = this.signal.text
    } else {
      this.job.boom = false
    }
  }

  /**
   * Calculates final cost margin
   */
  calcCostMargin() {
    if (this.job.quotePrice == 0)
      return 0

    return ((this.job.grossProfit / this.job.quotePrice) * 100).toFixed(1).toNum()
  }

  /**
   * Calculates estimated COGS
   */
  calcEstCOGS() {
    return this.expense.laborExp   +
      this.expense.driveExp        +
      this.expense.nonbillHoursExp +
      this.expense.payrollExp      +
      this.expense.fuelExp         +
      this.expense.hotelExp        +
      this.expense.vehicleExp      +
      this.expense.rentalExp       +
      this.expense.branchOvhExp    +
      this.expense.bioboxExp       +
      this.expense.dumpsterExp     +
      this.expense.bagsterExp      +
      this.expense.supplyEquipExp  +
      this.expense.internalExp
  }

  /**
   * Calculates fuel expense
   */
  calcFuelExp() {
    return Math.round(
      (this.job.driveTime * (this.costlist.miles_hr / this.costlist.miles_gal))
      *
      (this.costlist.amt_gal * this.job.trucks)
    )
  }

  /**
   * Calculates payroll cost (taxes & worker comp)
   */
  calcPayrollExp() {
    return Math.round(
      (this.costlist.per_wc/100)
      *
      (this.expense.laborExp + this.expense.driveExp + this.expense.nonbillHoursExp)
    )
  }

  /**
   * Calculates number of round trip from job days and hotel nights
   */
  calcRoundTrips() {
    const roundTrips = this.job.days - this.job.hotels
    return roundTrips <= 0 ? 1 : roundTrips
  }

  /**
   * Adds padding to end of string for console.log columns
   * @param {int} x
   * @param {int} y
   */
  pad(x, y = 11) {
    try {
      return x + ' '.repeat( y - x.toString().length )
    } catch {
      return 0
    }
  }
}
