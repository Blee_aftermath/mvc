import { Util } from '../util.js'

/**
 * Creates a pre arrival call info/checklist item
 * @author Jake Cirino
 */
export class PreArrivalCall {
  constructor(parent, data) {
    this.parent = parent
    this.data = Util.filterNulls(data)
    console.log(this.data)

    this.baseElement = document.createElement('div')
    this.baseElement.className = 'pre-ariv-call'
  }

  async populate() {
    //append html template
    let template = await window.templateController.getTemplate('jif/contactBox')
    this.baseElement.innerHTML = template

    //set checkbox state
    let checkboxElement = this.baseElement.querySelector("[type='checkbox']")
    checkboxElement.checked = this.data.addr_pre_call_made == 1 ? true : false

    //setup checkbox changed
    checkboxElement.addEventListener(
      'change',
      ((event) => {
        this.setPreCallMade(event.target.checked)
      }).bind(this)
    )

    //set name field
    let nameField = this.baseElement.querySelector('.contact-box-header')
    nameField.innerText = `${this.data.addr_fname} ${this.data.addr_lname}`
    if(this.data.addr_relation != '')
      nameField.innerText += ` (${this.data.addr_relation})`

    //append contact info

    let contactInfo = Util.getContactBlock(this.data, false, false, true)
    this.baseElement.appendChild(contactInfo)

    //append to document
    this.parent.bodyElement.appendChild(this.baseElement)
  }

  /**
   * Calls the API and sets whether the pre-call has been made for this contact.
   * @param {Boolean} isMade
   */
  setPreCallMade(isMade) {
    fetch('preArivCallPRG.php', {
      method: 'post',
      headers: { 'Content-Type': 'application/json;charset=utf8' },
      body: JSON.stringify({
        addr_id: this.data.addr_id,
        isMade: isMade,
      }),
    })
      .then((res) => res.json())
      .then((body) => {
        if (body.ok) window.jifController.update()
      })
  }
}
