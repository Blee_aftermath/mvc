import { Util } from '../util.js'

/**
 * Creates and controls the covid initial estimate block
 * @author Jake Cirino
 */
export class CovidInitialEstimate {
  /**
   * Creates a new covid initial estimate object
   * @param {JSON} data
   */
  constructor(data) {
    this.data = data
    console.log(data)

    //parse base element and dom elements
    this.baseElement = document.querySelector('[name=covid-ini]')
    this.dom = Util.parseDOM(this.baseElement)
    this.dom.save2Btn      = this.baseElement.querySelector('[name=save-2]')
    this.dom.confirmBtn    = this.baseElement.querySelector('[name=confirm]')
    this.dom.unconfirmBtn  = this.baseElement.querySelector('[name=unconfirm]')
    this.dom.confCopy      = this.baseElement.querySelector('[name=confirm-copy]')
    this.dom.confInitial   = this.baseElement.querySelector('[name=confirm-initial]')
    this.dom.unconfInitial = this.baseElement.querySelector('[name=unconfirm-initial]')
    this.dom.confHistory   = this.baseElement.querySelector('[name=confirm-history]')
  
    //create a permision object
    this.perm       = {}
    this.perm.auth = 'jif_a' // auth: Authorize JIFs
    this.perm.disc = 'jif_d' // disc: Discount over 50%
    this.perm.mnge = 'jif_m' // mnge: Manage JIFs
    this.perm.edit = 'jif_e' // edit: Edit JIFs
    this.perm.read = 'jif_r' // read: Edit JIFs, Edit own

    //setup input field updating
    for (const key in this.dom) {
      if (Object.hasOwnProperty.call(this.dom, key)) {
        const element = this.dom[key];

        element.addEventListener('change', (event) => {
          let key = event.target.name
          let value = event.target.value

          if(key != 'createCheck'){
            if(key.includes('_num_') || key.includes('_hr_')) this.data.covid[key] = value
            else if(key.includes('_amt_')) this.data.covid[key] = value * 100
            this.enableSaveBtn(true)
  
            this.calculate()
          }
        })
      }
    }

    //setup pull est button
    this.dom.pullEstButton.addEventListener('click', () => {
      this.dom.jini_amt_est.value = this.dom.estField.value
      this.data.covid.jini_amt_est = this.dom.estField.value * 100

      this.enableSaveBtn(true)
      this.calculate()
    })

    //setup save button
    this.dom.save2Btn.addEventListener('click', () => {
      let covidData = JSON.parse(JSON.stringify(this.data.covid))
      covidData.action = 'update'

      fetch('covidInitialEstimatePRG.php', {
        method: 'post',
        headers: {'Content-Type': 'application/json;charset=utf8'},
        body: JSON.stringify(covidData)
      })
      .then(res => res.json())
      .then(body => {
        this.enableSaveBtn(false)
      })
    })

    //set confirm button click event
    this.dom.confirmBtn.addEventListener('click', ((ev) => {
      this.confirm(ev.target, true)
    }).bind(this))

    //set unconfirm button click event
    this.dom.unconfirmBtn.addEventListener('click', ((ev) => {
      this.confirm(ev.target, false)
    }).bind(this))

    //setup confirm/unconfirm
    if (this.data.info.jif_ini_is_confirmed == 1 && window.aftermath.hasPerm(this.perm.mnge)) {
      console.log(this.data.info)
      this.dom.confirmBtn.style.display = 'none'
      this.dom.unconfirmBtn.style.display = 'inline-block'
    }

    //setup create/delete checkbox
    this.dom.createCheck.addEventListener('change', event => {
      if(event.target.checked) this.createEstimate()
      else this.deleteEstimate()
    })

    //populate data items
    this.populate()
  }

  /**
   * Creates a new estimate
   */
  createEstimate(){
    fetch('covidInitialEstimatePRG.php', {
      method: 'post',
      headers: {'Content-Type': 'application/json;charset=utf8'},
      body: JSON.stringify({
        action: 'create',
        jobID: this.data.jobID
      })
    })
    .then(res => res.json())
    .then(body => {
      if(body.ok){
        this.data.covid = body.ok
        console.log(this.data.covid)
        this.populate()
      }
    })
  }

  /**
   * Deletes a new estimate
   */
  deleteEstimate(){
    fetch('covidInitialEstimatePRG.php', {
      method: 'post',
      headers: {'Content-Type': 'application/json;charset=utf8'},
      body: JSON.stringify({
        action: 'delete',
        jobID: this.data.jobID
      })
    })
    .then(res => res.json())
    .then(body => {
      if(body.ok){
        this.data.covid = null
        this.populate()
      }
    })
  }

  /**
   * Populates input fields from this.data
   */
  populate() {
    //disable save button
    this.enableSaveBtn(false)

    //determine whether or not the estimate is defined
    if(this.data.covid == null){
      //set creation checkbox state
      this.dom.createCheck.checked = false

      //disable confirm
      this.dom.confirm.disabled = true

      //disable all input fields
      for (const key in this.dom) {
        if (Object.hasOwnProperty.call(this.dom, key)) {
          const element = this.dom[key];
          if(key.includes('jini_') || key == 'estField'){
            element.disabled = true
            element.value = ''
          }
        }
      }
    }else{
      //set creation checkbox state
      this.dom.createCheck.checked = true

      //populate data fields
      for (const key in this.dom) {
        if (Object.hasOwnProperty.call(this.dom, key)) {
          const element = this.dom[key]

          if(key.includes('jini_')) element.disabled = false

          if (this.data.covid[element.name] != null) {
            if(key.includes('_amt_')) element.value = this.data.covid[element.name] / 100
            else element.value = this.data.covid[element.name]
          }
        }
      }

      this.calculate()
      this.history()
    }
  }

  /**
   * Calculates the taxes
   * @param {Number} taxType jini_tax_type  
   * @param {Number} ynTax jini_yn_state_tax
   * @param {Number} taxRate jini_tax_rate
   * @param {Number} supplyCost supplyUsage
   * @param {Number} subtotal subtotal
   */
  calculateTaxes(taxType, ynTax, taxRate, supplyCost, subtotal) {
    if(taxType == 1){ //taxType 1: Only supplies are taxed
      return Math.round(ynTax * supplyCost * taxRate / 100)
    }else if(taxType == 2){ //taxType 2: Entire subtotal is taxed
      return Math.round(ynTax * subtotal * taxRate / 100)
    }

    return 0 //invalid/no tax type, taxes are 0
  }

  /**
   * Recalculates the estimate data
   */
  calculate(){
    //init data references
    let data = this.data.covid
    console.log(this.data.covid)
    let crewSize = data.jini_num_crew

    //calc covid hours
    let covidHoursSup = data.jini_hr_cov * data.jini_price_cov_sup / crewSize
    let covidHoursTec = data.jini_hr_cov * data.jini_price_cov_tec / ((crewSize - 1) / crewSize)
    if(!Number.isFinite(covidHoursSup) || Number.isNaN(covidHoursSup)) covidHoursSup = 0
    if(!Number.isFinite(covidHoursTec) || Number.isNaN(covidHoursTec)) covidHoursTec = 0
    let covidHours = covidHoursSup + covidHoursTec
    let covidHoursCalc = this.dom.hoursCheck.checked ? covidHours : 0
    console.log(covidHoursSup)
    console.log(covidHoursTec)

    //calc entities
    let ent = data.jini_num_ent * data.jini_price_ent
    let entCalc = this.dom.entCheck.checked ? ent : 0

    //calc sqft
    let sqft = data.jini_num_sqft * data.jini_price_sqft
    let sqftCalc = this.dom.sqftCheck.checked ? sqft : 0

    //calc surface swabs
    let swab = data.jini_num_srf_swab * data.jini_price_srf_swab
    let swabCalc = this.dom.swabCheck.checked ? swab : 0

    //calc suggested est
    let suggestedEst = covidHoursCalc + entCalc + sqftCalc + swabCalc

    //calc markup
    let markup = data.jini_amt_est - suggestedEst

    //calc subtotal
    let subtotal = data.jini_amt_est - data.jini_disc_amt_other

    //calc taxes
    let taxes = this.calculateTaxes(data.jini_tax_type, data.jini_yn_state_tax, data.jini_tax_rate, 0, subtotal)

    //calc total
    let total = subtotal - taxes

    //populate calc fields
    this.dom.hoursCalc.innerText = Util.formatCurrency(covidHoursCalc)
    this.dom.entCalc.innerText = Util.formatCurrency(entCalc)
    this.dom.sqftCalc.innerText = Util.formatCurrency(sqftCalc)
    this.dom.swabCalc.innerText = Util.formatCurrency(swabCalc)

    //populate est fields
    this.dom.estField.value = (suggestedEst/100).toFixed(2)
    this.dom.estCalc.innerText = Util.formatCurrency(suggestedEst)
    this.dom.suggestedEst.innerText = Util.formatCurrency(suggestedEst)

    //populate markup
    this.dom.markupEst.innerText = Util.formatCurrency(markup)

    //populate rack
    this.dom.rackEst.innerText = Util.formatCurrency(data.jini_amt_est)

    //populate discount
    this.dom.discEst.innerText = Util.formatCurrency(data.jini_disc_amt_other)

    //populate subtotal
    this.dom.subEst.innerText = Util.formatCurrency(subtotal)

    //populate tax
    this.dom.taxEst.innerText = Util.formatCurrency(taxes)

    //populate total
    this.dom.totEst.innerText = Util.formatCurrency(total)
  }

  /**
   * Enables/disables the save/confirm buttons
   * @param {Boolean} enabled
   */
  enableSaveBtn(enabled = true) {
    // 1. Toggle the save buttons, but always disable confirm
    this.dom.save2Btn.disabled = !enabled
    this.dom.confirmBtn.disabled = true

    // 2. Enable confirm if ready
      this.dom.confirmBtn.disabled = enabled

    // 4. At end, check if confirmed in jif and disable confirm button
    if (this.confirmed)
      this.dom.confirmBtn.disabled = true
  }

    /**
   * Confirms the entire form
   */
  confirm(button, doConfirmed = true){
    let html = doConfirmed ? this.dom.confInitial.innerHTML : this.dom.unconfInitial.innerHTML

    button.disabled = true

    let scopeCopy = 'Covid'

    this.showCopy(html.replace(/{{scopes}}/, scopeCopy))

    this.dom.confCopy.querySelector('[data-no]').addEventListener('click', ev => {
      button.disabled = false
      this.eraseCopy(this.dom.confCopy)
    })

    this.dom.confCopy.querySelector('[data-yes]').addEventListener('click', ev => {
      fetch('jifItemPRG.php', {
        method: 'post',
        headers: {'Content-Type': 'application/json;charset=utf8'},
        body: JSON.stringify({
          action: 'updateIniConfirmInfo',
          data: {jif_id: this.data.info.jif_id, jif_ini_is_confirmed: (doConfirmed ? 1 : 0)}
        })
      })
      .then(res => res.json())
      .then(body => {
        if (body.ok) {
          this.data.info = body.info

          this.confirmed = this.data.info.jif_ini_is_confirmed == 1 ? true : false

          this.eraseCopy(this.dom.confCopy, this.history.bind(this))

          if (this.confirmed) {
            this.dom.confirmBtn.disabled = true

            if (window.aftermath.hasPerm(this.perm.mnge)) {
              this.dom.confirmBtn.style.display = 'none'
              this.dom.unconfirmBtn.style.display = 'inline-block'
              this.dom.unconfirmBtn.disabled = false
            }
          } else {
            this.dom.unconfirmBtn.style.display = 'none'
            this.dom.confirmBtn.style.display = 'inline-block'
            this.dom.confirmBtn.disabled = false
          }

          window.jifController.update() //update sidebar checklist
        } else {
          note({text: 'There has been an error with your request.', class: 'error'})
        }
      }) 
    })
  }

  /**
   * Provides history content
   */
  history(){
    let html = ''
    if (this.confirmed) {
      let jifIniDTConf = new Date(this.data.info.jif_ini_dt_confirmed)
      html += `<p>The estimate was confirmed on ${Util.dateToString(jifIniDTConf)} by ${this.data.info.jif_ini_name_confirmed}</p>`
    } else if (this.data.info.jif_ini_emp_confirmed) {
      let jifIniDTConf = new Date(this.data.info.jif_ini_dt_confirmed)
      html += `<p>The estimate was unconfirmed on ${Util.dateToString(jifIniDTConf)} by ${this.data.info.jif_ini_name_confirmed}</p>`
    }
    this.dom.confHistory.innerHTML = html
    if (html.length) this.dom.confHistory.style.opacity = '1'
  }

  /**
   * Shows dynamic text
   * @param {string} copy
   */
  showCopy(copy){
    this.dom.confCopy.innerHTML = copy
    this.dom.confCopy.style.opacity = '1'
  }

  /**
   * Erases a DOM item per it's transition duration if set
   * @param {element} domItem 
   */
  eraseCopy(domItem, func = {}){
      let duration = domItem.style.transitionDuration ? parseFloat(domItem.style.transitionDuration) * 1000 : 0
      domItem.style.opacity = '0'
      setTimeout(() => {
        domItem.innerHTML = ''
        if (typeof func == 'function') func()
      }, duration)
  }
}
