/**
 * Contains document names for each phase
 */
export const PhaseDocuments = [
  ['CIS', 'SAJA', 'GSA'],                                     //pre-arrival
  ['CIS', 'SAJA', 'GSA', 'INI', 'PPR', 'DECL', 'WHT', 'IED'], //initial estimate 
  ['FIN', 'FPP', 'TIME', 'BIO', 'PPR', 'DECL', 'INSP', 'PJR', 'EQP', 'SUP'],                      //job management
  ['FIN', 'FPP', 'TIME', 'BIO', 'PPR', 'DECL', 'INSP', 'PJR', 'EQP', 'SUP'],                      //final estimate
  ['FIN', 'FPP', 'EQP', 'SUP', 'TIME', 'BIO', 'INSP', 'PJR']         //post job
]

/**
 * Handles the creation and state management of the documents section in the checklist
 * @author Jake Cirino
 */
export class ChecklistDocuments {
  /**
   * Creates a new checklist documents section given provided data
   * @param {HTMLElement} head The element to append this section to
   * @param {JSON} data The JSON data to be used to fill this element
   */
  constructor(head, data) {
    this.data = data
    this.items = []

    //create main element
    this.head = document.createElement('form')

    for (let val of PhaseDocuments[data.phaseID - 1]) {
      let itemElement = this.createDocumentElement(val)
      this.items.push(itemElement)
      this.head.appendChild(itemElement.head)
    }

    //create select/download buttons
    let buttonDiv = document.createElement('div')
    buttonDiv.style.textAlign = 'center'
    buttonDiv.className = 'jif-checklist-section'

    this.selectAllButton = document.createElement('input')
    this.selectAllButton.type = 'button'
    this.selectAllButton.value = 'Select All'
    this.selectAllButton.addEventListener('click', this.selectAll.bind(this), false)

    this.clearButton = document.createElement('input')
    this.clearButton.type = 'button'
    this.clearButton.value = 'Clear'
    this.clearButton.addEventListener('click', this.clearAll.bind(this), false)

    this.downloadButton = document.createElement('input')
    this.downloadButton.type = 'button'
    this.downloadButton.value = 'Download'

    //append buttons
    buttonDiv.appendChild(this.selectAllButton)
    buttonDiv.appendChild(this.clearButton)
    buttonDiv.appendChild(this.downloadButton)
    this.head.appendChild(buttonDiv)

    //append elements
    head.appendChild(this.head)
  }

  /**
   * Create a document element
   * @param {String} name The name of the document
   */
  createDocumentElement(name) {

    const docName = {
      CIS:  "Customer Info Sheet",
      SAJA: "Site Assessment Job Aid",
      GSA:  "General Services Agreement",
      INI:  "Initial Estimate",
      IED:  "CSR Estimate Detail",
      DECL: "Declined Processes",
      PPR:  "Personal Property",
      WHT:  "Whole Home Throw Out",
      FIN:  "Final Estimate",
      FINW: "Final Estimate Test",
      FPP:  "Final Estimate PP",
      TIME: "Customer Time Sheet",
      BIO:  "Biowaste Manifest",
      INSP: "Final Inspection",
      EQP:  "Equipment Detail",
      SUP:  "Supply Detail",
      PJR:  "Post-Job Recommendations"
    };

    //create base element
    let documentElement = document.createElement('div')
    documentElement.classList.add('jif-checklist-section')

    //create internal elements
    let checkboxElement = document.createElement('input')
    checkboxElement.type = 'checkbox'
    checkboxElement.addEventListener('change', this.updateDownloads.bind(this))

    //MSims change document text to verbose description.
    let titleElement = document.createElement('span')
//    titleElement.innerText = name.toUpperCase()
    titleElement.innerText = docName[name].toUpperCase()

    let buttonLink = document.createElement('a')
    buttonLink.href = `createDoc.php?doc=${name}&job=${this.data.jobID}`

    let buttonIcon = document.createElement('img')
    buttonIcon.className = 'icon16'
    buttonIcon.src = '/view/asset/icons/ext/pdf16.png'
    buttonIcon.alt = 'Create PDF'
    buttonIcon.title = 'Create PDF'

    buttonLink.appendChild(buttonIcon)

    //append elements
    documentElement.appendChild(checkboxElement)
    documentElement.appendChild(titleElement)
    documentElement.appendChild(buttonLink)

    return {
      name: name,
      head: documentElement,
      checkbox: checkboxElement
    }
  }

  updateDownloads(){
    let downloadString = `/createDoc.php?job=${this.data.jobID}`
    
    //get names of items to download
    let downloadItems = []
    this.items.forEach(item => {
      if(item.checkbox.checked){
        downloadItems.push(item.name)
      }
    })

    if(downloadItems.length > 1){
      downloadString += `&docs=${JSON.stringify(downloadItems)}`
    }else if(downloadItems.length == 1){
      downloadString += `&doc=${downloadItems[0]}`
    }

    console.log(downloadString)
    this.downloadButton.onclick = () => {
      location.href = downloadString
    }
  }

  /**
   * Selects all checkboxes in the documents sections
   */
  selectAll(event){
    this.items.forEach(element => {
      element.checkbox.checked = true
      this.updateDownloads()
    })
  }

  /**
   * Clears all checkboxes in the documents sections
   */
  clearAll(event){
    this.items.forEach(element => {
      element.checkbox.checked = false
      this.updateDownloads()
    })
  }

}
