/**
 * Represents a discount item in the job scope
 * @author Jake Cirino & Scott Kiehn
 */

const re = new RegExp(/[\$\.,]/g),
      toCents = m => Number(m.replace(re, ''))

export class JobScopeDiscountItem {
  /**
   * Creates a new job scope discount item
   * @param {JobScope} parent
   * @param {JSON} scopeData
   */
  constructor(parent, scopeData) {
    this.parent = parent
    this.scopeData = scopeData

    //set null to null string if not set
    this.scopeData.jini_disc = this.scopeData.jini_disc == null ? 'NULL' : this.scopeData.jini_disc
    //TODO initial population
    //console.log(scopeData)

    this.baseElement = document.createElement('div')
    this.baseElement.id = `scope-discount-item-${this.scopeData.scope_id}`
    this.baseElement.className = 'scope-item'
    this.approvalType = ''
    this.approved = true
    this.approval = {}

    //Employee may no longer be active, add into the approval object
    const formerEmp = this.scopeData.jini_disc_emp_appr
      ? {[this.scopeData.jini_disc_emp_appr]: this.scopeData.jini_disc_emp_name}
      : {}

    //Clone the parent object to get a copy, not reference.
    //Also, only add former employee to proper discount
    if (Number(this.scopeData.jini_disc_per) > 50) {
      this.approval.JA2 = Object.assign(formerEmp, this.parent.approval.JA2)
      this.approval.JA1 = Object.assign({}, this.parent.approval.JA1)
      this.approval.JA0 = Object.assign({}, this.parent.approval.JA0)
    } else if (Number(this.scopeData.jini_disc_per) > 20) {
      this.approval.JA2 = Object.assign({}, this.parent.approval.JA2)
      this.approval.JA1 = Object.assign(formerEmp, this.parent.approval.JA1)
      this.approval.JA0 = Object.assign({}, this.parent.approval.JA0)
    } else {
      this.approval.JA2 = Object.assign({}, this.parent.approval.JA2)
      this.approval.JA1 = Object.assign({}, this.parent.approval.JA1)
      this.approval.JA0 = Object.assign(formerEmp, this.parent.approval.JA0)
    }
  }

  async populate() {
    let template = await window.templateController.getTemplate('jif/jobScopeDiscountItem')
    this.baseElement.innerHTML = template

    this.discType      = this.baseElement.querySelector("[name='dis-type']")
    this.discAmount    = this.baseElement.querySelector("[name='dis-amount']")
    this.discOther     = this.baseElement.querySelector("[name='dis-other']")
    this.discPercent   = this.baseElement.querySelector("[name='dis-percent']")
    this.discApproval  = this.baseElement.querySelector("[name='dis-approval']")

    //add discount type options
    let discounts = this.scopeData.discounts ? Object.assign(this.scopeData.discounts, {other: {jdisc_dsca: 'Other', jdisc_id: 0}}) : {}
    for (let key in discounts) {
      let discount = discounts[key]
      let option = document.createElement('option')
      option.value = discount.jdisc_id
      option.innerText = discount.jdisc_dsca
      this.discType.appendChild(option)
    }

    //populate existing data on initial load & enable discount feature per permissions and scopes,
    //possible values passed are discount type, NULL or 0 (0 for other)
    if(this.setDiscountType(Number(this.scopeData.jini_disc_amt_other) > 0 ? 0 : this.scopeData.jini_disc)) {
      this.discAmount.value = this.scopeData.jini_disc_per
    } else if (this.scopeData.jini_disc_amt_other > 0) {
      this.discOther.value = this.scopeData.jini_disc_amt_other / 100
      this.discPercent.innerHTML = this.discOtherPercentView()
    }

    //build approver list
    this.buildApprovers()
    this.discApproval.value = this.scopeData.jini_disc_emp_appr

    if (this.isDiscountGreater(this.parent.approval.pct2)) {
      this.discOther.style.backgroundColor    = this.parent.approval.col2
      this.discApproval.style.backgroundColor = this.parent.approval.col2
    } else if (this.isDiscountGreater(this.parent.approval.pct1)) {
      this.discAmount.style.backgroundColor   = this.discAmount.value ? this.parent.approval.col1 : ''
      this.discOther.style.backgroundColor    = this.discOther.value.length ? this.parent.approval.col1 : ''
      this.discApproval.style.backgroundColor = this.parent.approval.col1
    }

    //allow event handlers only if discounts potentially needed
    //if ( ! ['Limited Scope', 'Selected Scopes'].includes(this.scopeData.scope_name) ) {
    if ( ! ['Selected Scopes'].includes(this.scopeData.scope_name) ) {

      //discount type change listener
      this.discType.addEventListener('change', (event) => {
        this.setDiscountType(event.target.value)
        this.discountChange()
      })

      //discount amount change listener
      this.discAmount.addEventListener('change', (event) => this.discountChange())

      //discount other keyup listener
      this.discOther.addEventListener('keyup', (event) => {
        if (window.aftermath.hasPerm(this.parent.perm.mnge)) {
          if (this.isDiscountGreater(100)) {
            this.discOther.value = toCents(this.parent.costItems[this.scopeData.scope_id].subtotalElement.innerText) / 100
          }
        } else {
          if (this.isDiscountGreater(this.parent.approval.pct2)) {
            this.discOther.value = Math.floor( toCents(this.parent.costItems[this.scopeData.scope_id].subtotalElement.innerText) * (this.parent.approval.pct2/100) ) / 100
          }
        }

        this.discountChange()
      })

      //discount approver change listener
      this.discApproval.addEventListener('change', (event) => this.discountChange())
    }

    //append to document
    this.parent.baseElement.querySelector("[name='ini-scope-discount']").appendChild(this.baseElement)
  }

  /**
   * Common changes for the above events
   */
  discountChange() {
    //recalc totals
    this.parent.calculateCosts(this.scopeData.scope_id)

    //build approver list
    this.buildApprovers()

    if (this.approvalType == 'JA0' && this.discType.value != 'NULL' && !this.discApproval.value)
      this.discApproval.value = this.parent.approval.emp

    if (this.isDiscountGreater(this.parent.approval.pct2)) {
      this.discAmount.style.backgroundColor   = ''
      this.discOther.style.backgroundColor    = this.parent.approval.col2
      this.discApproval.style.backgroundColor = this.parent.approval.col2
      this.approved = false
    } else if (this.isDiscountGreater(this.parent.approval.pct1)) {
      this.discAmount.style.backgroundColor   = this.discAmount.value ? this.parent.approval.col1 : ''
      this.discOther.style.backgroundColor    = this.discOther.value.length ? this.parent.approval.col1 : ''
      this.discApproval.style.backgroundColor = this.parent.approval.col1
      this.approved = false
    } else {
      this.discAmount.style.backgroundColor   = ''
      this.discOther.style.backgroundColor    = ''
      this.discApproval.style.backgroundColor = ''
      this.approved = true
    }

    if (!this.approved) {
      if (this.discApproval.value)
        this.approved = true
    }

    //if needed, set percent view for other
    this.discPercent.innerHTML = this.discOtherPercentView()

    //enable save buttons
    this.parent.enableSaveBtn()
  }

  /**
   * Sets the currently selected discount type
   * @param {} jiniDisc
   */
  setDiscountType(jiniDisc) {
    //remove amount options and set discType value
    this.discAmount.innerHTML = ''
    this.discType.value = jiniDisc
    this.discId = jiniDisc

    if (jiniDisc == 'NULL') {
      this.discOther.value = ''
      this.discPercent.innerHTML = ''
      this.discApproval.value = ''
      this.discAmount.style.backgroundColor   = ''
      this.discOther.style.backgroundColor    = ''
      this.discApproval.style.backgroundColor = ''
      this.setDiscountForm((this.scopeData.jini_scope ? true : false), 0)
      return false
    } else {
      if (parseInt(jiniDisc) > 0) {
        let discount = this.scopeData.discounts[jiniDisc]
        this.discOther.value = ''
        this.discPercent.innerHTML = ''

        //add discount amount options
        for (let i = 0; i < discount.jdiscper_per.length; i++) {
          let percentage = parseInt(discount.jdiscper_per[i])

          //permission is needed
          let reqApproval = discount.jdiscper_req_apr[i] == 1 ? true : false

          let option = document.createElement('option')
          option.value = percentage
          option.innerText = `${percentage}%${reqApproval ? ' - Requires Exec Approval' : ''}`
          option.style.backgroundColor = reqApproval ? this.parent.approval.col1 : ''
          this.discAmount.appendChild(option)
        }

        this.setDiscountForm(true, 1)
        return true;

      } else {
        this.discType.value = 0
        this.discOther.value = 0
        this.discApproval.value = ''

        this.setDiscountForm(true, 2)
        return false;
      }
    }
  }

  /**
   * Adds an option to the discount amount select
   * @param {Number} discAmount
   */
  addDiscountAmount(discAmount) {
    let option = document.createElement('option')
    option.value = discAmount
    option.innerText = discAmount + '%'
    this.discAmount.appendChild(option)
  }

  /**
   * Adds an option to the discount type select
   * @param {String} discType
   */
  addDiscountType(discType) {
    let option = document.createElement('option')
    option.value = discType.toLowerCase
    option.innerText = discType
    this.discType.appendChild(option)
  }

  /**
   * Adds an option to the discount amount select
   * @param {String} type
   */
  buildApprovers() {
    //if ( ['Limited Scope', 'Selected Scopes'].includes(this.scopeData.scope_name) ) return
    if ( ['Selected Scopes'].includes(this.scopeData.scope_name) ) return

    let type = ''

    this.isDiscountGreater(this.parent.approval.pct2)
      ? type = 'JA2' 
      : this.isDiscountGreater(this.parent.approval.pct1)
        ? type = 'JA1' : type = 'JA0'

    //if no change, do not run it
    if (type == this.approvalType) return

    this.approvalType = type
    this.discApproval.innerHTML = '<option value=""></option>'
    let approval = this.approval[type]
    for ( let a in approval ) {
      let option = document.createElement('option')
      option.value = a
      option.innerText = approval[a]
      this.discApproval.appendChild(option)
    }
  }

  /**
   * Calculates discount percentage view
   */
  discOtherPercentView() {
    if (this.getSubtotal() === 0)
      return `0&#65130;`

    if (this.discType.value == 0)
      return `${ Math.round(this.getDiscountOther()/this.getSubtotal() * 100) }&#65130;`
    else
      return ''
  }

  /**
   * Compare discount to input, return boolean
   * @param {Number} num
   */
  isDiscountGreater(num) {
    if (Number(this.discAmount.value) > num)
      return true

    if (Number(this.discOther.value) > 0) {
      if ((this.getDiscountOther() / this.getSubtotal()) * 100 > num)
        return true
    }

    return false
  }

  /**
   * Enable discount feature per permissions and scopes
   * @param {Boolean} enabled
   * @param {Number} toggle
   */
  setDiscountForm(enabled, toggle = 1) {

    //no, if do not have base permissions
    if (!window.aftermath.hasPerm(this.parent.perm.edit)) return

    //no, for the first and last columns
    //if ( ['Limited Scope', 'Selected Scopes'].includes(this.scopeData.scope_name) ) return
    if ( ['Selected Scopes'].includes(this.scopeData.scope_name) ) return

    //enable/disable the "Discount Type" select
    this.discType.disabled = !enabled

    if (toggle > 0) {
      if (toggle == 1) {
        this.discAmount.disabled = false
        this.discOther.disabled = true
      } else {
        this.discAmount.disabled = true
        this.discOther.disabled = false
        this.discOther.select()
      }
      this.discApproval.disabled = false
    } else {
      this.discOther.disabled = true
      this.discAmount.disabled = true
      this.discApproval.disabled = true
    }
  }

  /**
   * Gets the raw discount value (0-100)
   */
  getRawDiscount() {
    return this.discAmount.value == '' ? 0 : this.discAmount.value
  }

  /**
   * Gets the discount rate as a decimal for calculations
   */
  getDiscount() {
    return this.discAmount.value / 100
  }

  /**
   * Gets the discount approver
   */
  getDiscountApprover() {
    return this.discApproval.value
  }

  /**
   * Gets the discount type
   */
  getDiscountType() {
    return Number(this.discId) > 0 ? this.discId : 'NULL'
  }

  /**
   * Gets the "discount other" amount as pennies for calculations
   */
  getDiscountOther() {
    return this.discOther.value ? Math.round(this.discOther.value * 100) : 0
  }

  /**
   * Gets the subtotal from costItems or initial the jif_initial
   */
  getSubtotal() {
    if (this.parent.costItems[this.scopeData.scope_id])
      return toCents(this.parent.costItems[this.scopeData.scope_id].subtotalElement.innerText)
    else
      return Number(this.scopeData.jini_calc_sub_full)
  }
}
