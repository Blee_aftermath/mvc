import { Util } from '../util.js'

/**
 * An item in the WBGT testing list
 * @author Jake Cirino
 */
export class WBGTTestingItem {
  /**
   * Creates a new WBGT Testing Item
   * @param {WBGTTesting} parent
   * @param {JSON} data
   */
  constructor(parent, data) {
    this.parent = parent
    this.data = data
    console.log(this.data)

    //create base element
    this.baseElement = document.createElement('tr')
    this.baseElement.innerHTML = this.data.template

    //set/select elements
    this.date = this.baseElement.querySelector("[name='date']")
    this.dateEdit = this.baseElement.querySelector("[name='dateEdit']")
    this.dateEditField = this.dateEdit.children[0]
    this.time = this.baseElement.querySelector("[name='time']")
    this.timeEdit = this.baseElement.querySelector("[name='timeEdit']")
    this.timeEditField = this.timeEdit.children[0]
    this.reading = this.baseElement.querySelector("[name='wbgt_reading']")
    this.readingEdit = this.baseElement.querySelector("[name='readingEdit']")
    this.readingEditField = this.readingEdit.children[0]
    this.level = this.baseElement.querySelector("[name='wbgt_calc_level']")
    this.levelInfo = this.baseElement.querySelector("[name='levelInfo']")
    this.levelEdit = this.baseElement.querySelector("[name='levelEdit']")
    this.editButton = this.baseElement.querySelector("[name='editButton']")
    this.deleteButton = this.baseElement.querySelector("[name='deleteButton']")
    this.saveButton = this.baseElement.querySelector("[name='saveButton']")

    //implement edit button
    this.editButton.addEventListener('click', () => {
      this.setEditMode(true)
    })

    //implement save button
    this.saveButton.addEventListener('click', () => {
      //call update endpoint
      fetch('wbgtPRG.php', {
        method: 'post',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({
          action: 'editItem',
          wbgtID: this.data.wbgt_id,
          wbgt_dt: Util.dateToString(Util.parseDate(this.dateEditField.value, this.timeEditField.value)),
          wbgt_reading: this.readingEditField.value,
        }),
      })
        .then((res) => res.json())
        .then((body) => {
          if (body.ok) {
            //reset edit mode and update display data
            this.setEditMode(false)
            this.data.wbgt_dt = Util.dateToString(Util.parseDate(this.dateEditField.value, this.timeEditField.value))
            this.data.wbgt_reading = this.readingEditField.value
            this.populate()
          }
        })
    })

    //implement delete button
    this.deleteButton.addEventListener('click', () => {
      fetch('wbgtPRG.php', {
        method: 'post',
        headers: { 'Content-Type': 'application/json;charset=utf8' },
        body: JSON.stringify({
          action: 'deleteItem',
          wbgtID: this.data.wbgt_id,
        }),
      })
        .then((res) => res.json())
        .then((body) => {
          if (body.ok) {
            this.parent.update()
          }
        })
    })

    //append to document
    this.parent.contentBody.insertBefore(this.baseElement, this.parent.inputRow)

    //setup datepicker and populate data
    $(this.baseElement).ready(() => {
      $(() => {
        $("[name|='dateEditField']").datepicker()
        $("[name|='dateEditField']").datepicker('option', 'dateFormat', 'mm/dd/y')

        //populate data
        this.populate()
        this.setEditMode(false)
      })
    })
  }

  /**
   * Populates fields based off data
   */
  populate() {
    let dateTime = this.data.wbgt_dt.split(' ')
    this.date.innerText = dateTime[0]
    this.dateEditField.value = dateTime[0]
    this.time.innerText = dateTime[1]
    this.timeEditField.value = dateTime[1]
    this.reading.innerText = parseFloat(this.data.wbgt_reading).toFixed(1)
    this.readingEditField.value = parseFloat(this.data.wbgt_reading).toFixed(1)

    this.calcLevel()
  }

  /**
   * Sets the form to edit mode or not
   * @param {Boolean} editMode
   */
  setEditMode(editMode) {
    this.editMode = editMode

    let infoDisplay = editMode ? 'none' : '',
      editDisplay = editMode ? '' : 'none'

    this.baseElement.className = editMode ? 'table-input' : ''

    //set info elements to display
    this.date.style.display = infoDisplay
    this.time.style.display = infoDisplay
    this.reading.style.display = infoDisplay
    this.levelInfo.style.display = infoDisplay

    //set edit elements to display
    this.dateEdit.style.display = editDisplay
    this.timeEdit.style.display = editDisplay
    this.readingEdit.style.display = editDisplay
    this.levelEdit.style.display = editDisplay
  }

  /**
   * Recalculates
   */
  calcLevel() {
    let reading = parseFloat(this.data.wbgt_reading),
      level = 0
    if (reading <= 74) {
      level = 1
    } else if (reading > 74 && reading <= 76) {
      level = 2
    } else if (reading > 76 && reading < 82) {
      level = 3
    } else {
      level = 4
    }

    this.level.innerText = level
  }

  /**
   * Deletes this item from the dom
   */
  delete() {
    this.baseElement.remove()
  }
}
