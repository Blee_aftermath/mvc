import { Util } from "../util.js"

/**
 * A widget for the jif sidebar that contains both an hours check and a value check
 */
export class SidebarHoursCheck{
  /**
   * Creates a bew sidebar hours check item
   * @param {JifSidebar} parent 
   * @param {String} template
   * @param {JSON} data 
   */
  constructor(parent, template, data){
    this.parent = parent
    this.data = data

    //create title element
    this.baseElement = document.createElement('div')
    this.baseElement.innerHTML = template

    //setup elements
    this.dom = Util.parseDOM(this.baseElement)

    //setup expander button
    this.dom.expanderHeader.addEventListener('click', () => {
      this.setExpanded(!this.expanded)
    })

    //setup primary scope selector option
    let priOpt = document.createElement('option')
    priOpt.value = '1'
    priOpt.innerText = this.data.primaryInvType
    this.dom.scopeSelector.appendChild(priOpt)

    //secondary scope selector option
    if(this.data.secondaryEstimate != null){
      let secOpt = document.createElement('option')
      secOpt.value = '2'
      secOpt.innerText = this.data.secondaryInvType
      this.dom.scopeSelector.appendChild(secOpt)
    }

    //all scope selector option
    let allOpt = document.createElement('option')
    allOpt.value = '3'
    allOpt.innerText = 'All'
    this.dom.scopeSelector.appendChild(allOpt)

    //setup scope selector
    this.dom.scopeSelector.addEventListener('change', event => {
      this.setSelectedScope(event.target.value)
    })

    //append to document
    this.parent.baseElement.appendChild(this.baseElement)

    //set to initials
    window.addEventListener('load', () => {
      this.setExpanded(true)
      this.setSelectedScope(1)
    })
  }

  /**
   * Updates the DOM with data to reflect the current data of this object 
   */
  update(){
    //setup estimate and final and difference data sets
    let estData, finData, difData
    switch (this.selectedScope) {
      case 1:
      case '1':
        estData = this.data.primaryJini
        finData = this.data.primaryFinal
        difData = Util.addPrefixes(Util.subtractArrays(finData, estData, true), 'diff')
        break
      case 2:
      case '2':
        estData = this.data.secondaryJini
        finData = this.data.secondaryFinal
        difData = Util.addPrefixes(Util.subtractArrays(finData, estData, true), 'diff')
        break
      case 3:
      case '3':
        estData = Util.sumArrays(this.data.primaryJini, this.data.primaryJini)
        finData = Util.sumArrays(this.data.primaryFinal, this.data.secondaryFinal)
        difData = Util.addPrefixes(Util.subtractArrays(finData, estData, true), 'diff')
      break
    }

    let totData = {
      est: 0,
      act: 0,
      diff: 0
    }

    //populate estimate data
    for (const key in estData) {
      if (estData.hasOwnProperty(key)) {
        const value = estData[key];
        const element = this.dom[key]
        if(element != undefined){
          element.innerText = Number(value).toFixed(2)
          totData.est += Number(value)
        }
      }
    }

    //populate actual
    for (const key in finData) {
      if (finData.hasOwnProperty(key)) {
        const value = finData[key];
        const element = this.dom[key]
        if(element != undefined){
          element.innerText = Number(value).toFixed(2)
          totData.act += Number(value)
        }
      }
    }

    //populate diff
    for (const key in difData) {
      if (difData.hasOwnProperty(key)) {
        const value = difData[key];
        const element = this.dom[key]
        if(element != undefined) {
          element.innerText = Number(value).toFixed(2)
          totData.diff += Number(value)
        }
      }
    }

    //populate total fields
    this.dom.estimateTotal.innerText = totData.est.toFixed(2)
    this.dom.actualTotal.innerText = totData.act.toFixed(2)
    this.dom.diffTotal.innerText = totData.diff.toFixed(2)
  }

  /**
   * Sets whether this elements info is expanded or not
   * @param {Boolean} expanded 
   */
  setExpanded(expanded){
    this.expanded = expanded

    //TODO calc height based on number of elements? or just keep the number updated
    //set content height
    let height = this.dom['content-collapse'].children[0].getBoundingClientRect().height
    this.dom['content-collapse'].style.height = expanded ? `${height}px` : '0px'

    //rotate icon
    this.dom.expander.style.transform = expanded ? '' : 'rotate(-90deg)'
  }

  /**
   * Sets the selected scope(s) to display
   * @param {Number} scope Either 1 (pri), 2 (sec) or 3 (all)
   */
  setSelectedScope(scope){
    this.selectedScope = scope
    this.update()
  }

}