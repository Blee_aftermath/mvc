import { default as note } from '../modules/lib/note.js'

String.prototype.toNum = function () {return Number(this)}
Number.prototype.toNum = function () {return Number(this)}

const pricelistForm = document.getElementById('pricelistForm'),
      pricelist     = document.getElementById('pricelist'),
      lists         = document.getElementById('lists'),
      shiftRight    = document.getElementById('shift-right'),
      shiftLeft     = document.getElementById('shift-left'),
      cssStyle      = getComputedStyle(lists),
      cssPosition   = cssStyle.left.replace(/px/, '').toNum(),
      cssWidth      = cssStyle.width.replace(/px/, '').toNum(),
      ulLists       = []

let cssLeft         = cssPosition,
    current         = 0,
    previous        = 0

// Create HTMLCollections and store 
for (const ul of pricelist.querySelectorAll('ul')) {
  const li = ul.getElementsByTagName('li')
  for (const key of Object.keys(li))
    li[key].setAttribute('value',key)
  ulLists.push(li)
}

document.addEventListener('keyup', ev => keyboardNav(ev))
pricelistForm.addEventListener('change', ev => formEvents(ev.target))
pricelistForm.addEventListener('click', ev => formEvents(ev.target))

// Use collections to provide a hover effect on the same li index 
// across separate ul columns (hilite row effect)
pricelist.addEventListener('mouseover', ev => {
  const el = ev.target
  if (el.matches('li')) {
    current = el.value
    let change = current != previous ? true : false
    for (const ul of ulLists) {
      if (change) {
        ul.item(current).classList.add('hover')
        ul.item(previous).classList.remove('hover')
      }
    }
    if (change) previous = current
  }
})

pricelist.addEventListener('mouseleave', ev => {
  for (const ul of ulLists)
    ul.item(previous).classList.remove('hover')

  previous = 0
  current  = 0
})

const keyboardNav = ev => {
  if (ev.which == 37) shiftLeft.click()
  if (ev.which == 39) shiftRight.click()
}

const formEvents = el => {

  if (el.matches('[type=number]')) {
    if (el.step == '0.01')
      el.value = (Math.round(el.value * 100) / 100).toFixed(2)
    else if (el.step == '0.1')
      el.value = (Math.round(el.value * 100) / 100).toFixed(1)
    else
      el.value = (Math.round(el.value * 100) / 100).toFixed(0)
  }

  if (el.matches('[data-add]')) {
    const px = el.dataset.add.toNum()
    cssLeft = cssLeft !== px ? px : cssPosition
    lists.style.left = `${cssLeft}px`
  }

  if (el.matches('[data-edit]')) {
    const list = el.closest('ul')
    list.classList.toggle('toggle')
    for (const li of list.querySelectorAll('li')) {
      const inp = li.querySelector('input')
      if (inp)
        inp.value = inp.dataset.value
    }
  }

  if (el.matches('[data-shift]')) {
    const px = el.dataset.shift.toNum()
    cssLeft = cssLeft + px
    if (cssLeft > 0) cssLeft = 0
    if (Math.abs(cssLeft) >= cssWidth) cssLeft = -(cssWidth + px)
    lists.style.left = `${cssLeft}px`
  }

  if (el.matches('[data-save]'))
    pricelistSave(el.closest('ul'))
}

const pricelistSave = async (list) => {
  const params = {}

  let error = 0

  for (const inp of list.querySelectorAll('[name]')) {

    //if (inp.value.length == 0 || inp.value * 1 === 0)
    if (inp.value.length == 0)
      error++

    if (error) {
      const name = inp.placeholder || document.getElementById(inp.name).innerText
      note({text: `Please provide a value for: ${name}`, class: 'error'})
      return
    }

    let value = ''

    //adjust monetary to pennies
    if (inp.step == '0.01')
      value = inp.value * 100
    else
      value = inp.value

    params[inp.name] = value;
  }

  const post = await fetch('/pricelistPRG.php', {
    method: 'post',
    headers: {'Content-Type':'application/json;charset=utf-8'},
    body: JSON.stringify(params)
  })

  const res = await post.json()

  if (res.ok) {
    location.reload()
  } else {
    note({text: res.msg, class: 'error'})
  }
}
