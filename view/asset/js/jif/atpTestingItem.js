/**
 * An ATP swab item
 * @author Jake Cirino
 */
export class ATPTestingItem {
  /**
   * Creates a new ATP swab item
   * @param {ATPTestingRoom} parent
   * @param {JSON} data
   */
  constructor(parent, data) {
    this.parent = parent
    this.data = data

    //create base element
    this.baseElement = document.createElement('tr')
    this.baseElement.innerHTML = this.data.itemTemplate

    //select/set fields
    this.surfaceField = this.baseElement.querySelector("[name='atps_surface']")
    this.preField = this.baseElement.querySelector("[name='atps_read_pre']")
    this.prePassField = this.baseElement.querySelector("[name='pre_pass']")
    this.postField = this.baseElement.querySelector("[name='atps_read_pos']")
    this.postPassField = this.baseElement.querySelector("[name='post_pass']")
    this.surfaceEdit = this.baseElement.querySelector("[name='surfaceEdit']")
    this.surfaceEditField = this.baseElement.querySelector("[name='surfaceField']")
    this.preEdit = this.baseElement.querySelector("[name='preEdit']")
    this.preEditField = this.baseElement.querySelector("[name='preField']")
    this.postEdit = this.baseElement.querySelector("[name='postEdit']")
    this.postEditField = this.baseElement.querySelector("[name='postField']")
    this.icons = this.baseElement.querySelector("[name='icons']")
    this.editButton = this.baseElement.querySelector("[name='editButton']")
    this.deleteButton = this.baseElement.querySelector("[name='deleteButton']")
    this.editSave = this.baseElement.querySelector("[name='editSave']")
    this.saveButton = this.baseElement.querySelector("[name='saveButton']")

    //setup edit button
    this.editButton.addEventListener('click', () => {
      this.setEditing(true)
    })

    //setup delete button
    this.deleteButton.addEventListener('click', () => {
      fetch('atpPRG.php', {
        method: 'post',
        headers: { 'Content-Type': 'application/json;charset=utf8' },
        body: JSON.stringify({
          action: 'deleteSwab',
          swabID: this.data.atps_id,
        }),
      })
        .then((res) => res.json())
        .then((body) => {
          if (body.ok) {
            this.parent.parent.update()
            this.parent.parent.repopulate()
          }
        })
    })

    //setup editing save button
    this.saveButton.addEventListener('click', () => {
      fetch('atpPRG.php', {
        method: 'post',
        headers: { 'Content-Type': 'application/json;charset=utf8' },
        body: JSON.stringify({
          action: 'editSwab',
          swabID: this.data.atps_id,
          surface: this.surfaceEditField.value,
          pre: this.preEditField.value * 10,
          post: this.postEditField.value * 10,
        }),
      })
        .then((res) => res.json())
        .then((body) => {
          if (body.ok) {
            //update data
            this.data.atps_surface = this.surfaceEditField.value
            this.data.atps_read_pre = this.preEditField.value * 10
            this.data.atps_read_pos = this.postEditField.value * 10

            //reupdate fields
            this.populate()

            //switch back from edit mode
            this.setEditing(false)

            //trigger jifcontroller update
            window.jifController.update()
          }
        })
    })

    //populate fields
    this.populate()
    this.setEditing(false)

    //append to body
    this.parent.itemBody.insertBefore(this.baseElement, this.parent.inputSection)
  }

  /**
   * Populates the fields based off this items data
   */
  populate() {
    this.surfaceField.innerText = this.data.atps_surface
    this.surfaceEditField.value = this.data.atps_surface
    let pre = (this.data.atps_read_pre / 10).toFixed(1)
    this.preField.innerText = pre
    this.preEditField.value = pre
    let post = (this.data.atps_read_pos / 10).toFixed(1)
    this.postField.innerText = post
    this.postEditField.value = post
    let prePass = pre == 0 ? true : false
    this.prePassField.innerText = prePass ? 'Pass' : 'Fail'
    this.prePassField.style.backgroundColor = prePass ? '#c6efce' : '#ffc7ce'
    let postPass = post == 0 ? true : false
    this.postPassField.innerText = postPass ? 'Pass' : 'Fail'
    this.postPassField.style.backgroundColor = postPass ? '#c6efce' : '#ffc7ce'
  }

  /**
   * Sets whether or not the form is in edit mode
   * @param {Boolean} editing
   */
  setEditing(editing) {
    this.editing = editing

    let infoDisplay = editing ? 'none' : '',
      editDisplay = editing ? '' : 'none'

    //change table row class
    this.baseElement.className = editing ? 'table-input' : ''

    //set info elements to display
    this.surfaceField.style.display = infoDisplay
    this.preField.style.display = infoDisplay
    this.postField.style.display = infoDisplay
    this.icons.style.display = infoDisplay

    //set edit elements to display
    this.surfaceEdit.style.display = editDisplay
    this.preEdit.style.display = editDisplay
    this.postEdit.style.display = editDisplay
    this.editSave.style.display = editDisplay
  }
}
