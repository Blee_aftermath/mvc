/**
 * An item in the remediation section of personal property
 * @author Jake Cirino
 */
export class RemediationItem {
  /**
   * Creates a new remediation section item
   * @param {PersonalProperty} parent
   * @param {JSON} data
   */
  constructor(parent, data) {
    this.parent = parent
    this.data = data
    this.editMode = false

    this.baseElement = document.createElement('tr')

    this.populate()
  }

  async populate() {
    //load template
    let template = await window.templateController.getTemplate('jif/remediationItem')
    this.baseElement.innerHTML = template

    //select/set elements
    this.description = this.baseElement.querySelector("[name='description']")
    this.descriptionEdit = this.baseElement.querySelector("[name='descriptionEdit']")
    this.descriptionField = this.baseElement.querySelector("[name='descriptionField']")
    this.location = this.baseElement.querySelector("[name='location']")
    this.locationEdit = this.baseElement.querySelector("[name='locationEdit']")
    this.locationField = this.baseElement.querySelector("[name='locationField']")
    this.saveEdit = this.baseElement.querySelector("[name='saveEdit']")
    this.buttons = this.baseElement.querySelector("[name='buttons']")

    //setup buttons
    this.editButton = this.baseElement.querySelector("[name='editButton']")
    this.editButton.addEventListener(
      'click',
      (() => {
        this.setEditMode(true)
      }).bind(this)
    )

    this.deleteButton = this.baseElement.querySelector("[name='deleteButton']")
    this.deleteButton.addEventListener('click', () => {
      this.deleteRemediationItem()
    })

    this.saveButton = this.baseElement.querySelector("[name='saveButton']")
    this.saveButton.addEventListener(
      'click',
      (() => {
        this.updateRemediationItem()
      }).bind(this)
    )

    //initial data population
    this.setData(this.data.ppreme_dsca, this.data.ppreme_loc)

    //append to document
    this.parent.remediationElement.insertBefore(this.baseElement, this.parent.remediationItemsElement)
  }

  /**
   * Sets the data
   * @param {String} description
   * @param {String} location
   */
  setData(description, location) {
    //set data variables
    this.data.ppreme_dsca = description
    this.data.ppreme_loc = location

    //set display
    this.description.innerText = description
    this.descriptionField.value = description
    this.location.innerText = location
    this.locationField.value = location
  }

  /**
   * Sets this item to edit mode
   * @param {Boolean} editMode True if setting to edit mode, false if disabling
   */
  setEditMode(editMode) {
    this.editMode = editMode

    let infoDisplay = editMode ? 'none' : ''
    let editDisplay = editMode ? '' : 'none'

    this.baseElement.className = editMode ? 'table-input' : ''

    //set info elements to display
    this.description.style.display = infoDisplay
    this.location.style.display = infoDisplay
    this.buttons.style.display = infoDisplay

    //set edit elements to display
    this.descriptionEdit.style.display = editDisplay
    this.locationEdit.style.display = editDisplay
    this.saveEdit.style.display = editDisplay
  }

  /**
   * Updates this remediation item in the database
   */
  updateRemediationItem(){
    let data = {
      ppreme_id: this.data.ppreme_id,
      ppreme_dsca: this.descriptionField.value,
      ppreme_loc: this.locationField.value
    }

    fetch('./personalPropertyPRG.php', {
      method: 'post',
      headers: { 'Content-Type': 'application/json;charset=utf8'},
      body: JSON.stringify({
        action: 'editRemediation',
        data: data
      })
    })
    .then(res => res.json())
    .then(body => {
      if(body.ok){
        this.setData(data.ppreme_dsca, data.ppreme_loc)

        this.setEditMode(false)
      }
    })
  }

  /**
   * Delete this remediation item in the database
   */
  deleteRemediationItem(){
    fetch('./personalPropertyPRG.php', {
      method: 'post',
      headers: {'Content-Type': 'application/json;charset=utf8'},
      body: JSON.stringify({
        action: 'deleteRemediation',
        itemID: this.data.ppreme_id
      })
    })
    .then(res => res.json())
    .then(body => {
      if(body.ok){
        this.baseElement.remove()
        window.jifController.update()

        //remove from parent array
        for (const key in this.parent.remediationItems) {
          if (this.parent.remediationItems.hasOwnProperty(key)) {
            let element = this.parent.remediationItems[key]
            if(element === this)
              this.parent.remediationItems.splice(key, 1)
          }
        }
      }
    })
  }

}
