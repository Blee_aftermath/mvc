import { Util } from "../util.js"

/**
 * The info section on the job scope
 * @author Jake Cirino & Scott Kiehn
 */
export class JobScopeInfo{
  /**
   * Creates a new
   * @param {JobScope} parent 
   * @param {JSON} data 
   */
  constructor(parent, data){
    this.parent = parent
    this.data = data
    this.fields = {}

    //set base element
    this.baseElement = this.parent.baseElement.querySelector('.ini-info')

    //set the scope selection on the parent DOM object to be used in other scope items
    this.parent.dom.rec = this.baseElement.querySelector('[name=jif_scope_rec]')
    this.parent.dom.pri = this.baseElement.querySelector('[name=jif_scope_pri]')
    this.parent.dom.sec = this.baseElement.querySelector('[name=jif_scope_sec]')

    //disable secondary if there is no primary scope selected
    this.parent.dom.sec.disabled = this.data.jif_scope_pri ? false : true

    //disable primary if there is no secondary scope selected
    this.parent.dom.pri.disabled = this.data.jif_scope_sec ? true : false 

    //disable secondary if primary is PP
    if (this.data.jif_scope_pri == 5) this.parent.dom.sec.disabled = true

    //get fields and add event listeners
    this.baseElement.querySelectorAll('input, textarea, select').forEach(element => {

      this.fields[element.name] = element

      element.addEventListener('change', ((event) => {

        //update data field
        this.data[event.target.name] = event.target.value == '' ? null : event.target.value

        if (event.target.matches('select')) {

          //scope info select events are autosaved
          this.scopeEvent(event.target)

        } else {

          //enable save button
          this.parent.enableSaveBtn()

        }

      }).bind(this))
    })

    //populate data
    for (const key in this.data) {
      if (this.data.hasOwnProperty(key)) {
        const element = this.data[key];
        if(this.fields[key] !== undefined)
          this.fields[key].value = element
      }
    }
  }

  /**
   * Scope selection event and autosave
   */
  async scopeEvent(select){

    await this.save({
      jif_id:        this.data.jif_id,
      jif_scope_rec: this.parent.dom.rec.value || null,
      jif_scope_pri: this.parent.dom.pri.value || null,
      jif_scope_sec: this.parent.dom.sec.value || null
    })

    if (select.matches('[name=jif_scope_pri]') || select.matches('[name=jif_scope_sec]')) {
      // Dispatch an event upon scope change
      document.dispatchEvent( this.parent.customEvent.ScopeSelectionFinished );
    }

    if (select.matches('[name=jif_scope_pri]')) {
      //enable secondary scope select if there is a primary scope selected
      this.parent.dom.sec.disabled = select.value ? false : true

      //if primary is PP, then disable secondary
      if (select.value == 5)
        this.parent.dom.sec.disabled = true
    }

    if (select.matches('[name=jif_scope_sec]')) {
      //disable primary scope select if there is a secondary scope selected
      this.parent.dom.pri.disabled = select.value ? true : false
    }

    //the below will not exist on page load, but will when chnage event occurs
    let iniScope = this.parent.baseElement.querySelector('[name=ini-scope]')

    //first, set all to enable
    for (let input of iniScope.querySelectorAll('[name=enable-scope]'))
      input.disabled = false

    //second, set scope selected to disable
    for (let key of ['rec', 'pri', 'sec']) {
      let value = this.parent.dom[key].value
      if (value) {
        iniScope.querySelector(`#scope-item-${value}`).querySelector('[name=enable-scope]').disabled = true
      }
    }

    //Running enableSaveButton with false also causes the
    //confirm buttom to become available upon primary selection
    this.parent.enableSaveBtn(false)
  }

  /**
   * Updates the data in the database to match the fields
   */
  save(data = {}){

    if (!Object.keys(data).length)
      data = this.data

    fetch('jifItemPRG.php', {
      method: 'post',
      headers: {'Content-Type': 'application/json;charset=utf8'},
      body: JSON.stringify({
        action: 'updateScopeInfo',
        data: data
      })
    })
    .then(res => res.json())
    .then(body => {
      //console.log(body)
    }) 
  }
}