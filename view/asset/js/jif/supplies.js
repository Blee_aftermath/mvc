import { default as note } from '../modules/lib/note.js'
import { message } from '../modules/lib/util.js'
import { Util } from '../util.js'

String.prototype.toNum = function () {return Number(this)}
Number.prototype.toNum = function () {return Number(this)}

/**
 * Contains information on the supplies used in the jobs
 * @author Scott Kiehn
 */
export class Supplies {
  /**
   * Creates a new supplies section
   * @param {JSON} data
   */
  constructor(data) {
    this.jobID = data.jobID
    this.info  = data.info
    this.supp  = data.supp
    this.misc  = data.misc
    this.base  = {}
    this.dom   = {}
    this.ttl   = {}
    this.alert = false

    this.newRecord = this.supp.new
    this.supp = this.supp.data
    console.log(`New record: ${this.newRecord}`)

    // Collect some base data into a central object
    this.base.scpPri  = this.info.jif_scope_pri ? this.info.jif_scope_pri.toNum() : 0
    this.base.scpSec  = this.info.jif_scope_sec ? this.info.jif_scope_sec.toNum() : 0
    this.base.numDays = this.info.jif_num_days ? this.info.jif_num_days.toNum() : 0
    this.base.numCrew = this.info.jif_num_crew ? this.info.jif_num_crew.toNum() : 0
    this.base.estPri  = this.info.jini_calc_sup_pri ? this.info.jini_calc_sup_pri.toNum() : 0
    this.base.estSec  = this.info.jini_calc_sup_sec ? this.info.jini_calc_sup_sec.toNum() : 0
    this.base.lowTh   = 10.0
    this.base.highTh  = 20.0
    this.base.supFee  = this.info.jfin_calc_supply_fee ? this.info.jfin_calc_supply_fee.toNum() : 0

    this.baseElement = document.createElement('div')
    this.baseElement.addEventListener('change', (ev) => this.changeEvent(ev.target))
    this.baseElement.addEventListener('click', (ev) => this.clickEvent(ev.target))
    this.baseElement.addEventListener('keyup', (ev) => this.keyupEvent(ev.target))
    this.baseElement.innerHTML = data.template

    // Collect DOM ids and place into an object
    for (const el of this.baseElement.querySelectorAll('[id]')) this.dom[el.id] = el

    // Set some header area DOM items
    this.dom.scpPri.innerHTML = this.base.scpPri ? this.info.scope_code_pri : '&empty;'
    this.dom.scpSec.innerHTML = this.base.scpSec ? this.info.scope_code_sec : '&empty;'
    this.dom.numDays.value    = this.base.numDays
    this.dom.numCrew.value    = this.base.numCrew

    //if data, build the supply form
    if (Object.keys(this.supp).length) {
      this.supplyForm()
      this.dom.addn.disabled = false
      //this.dom.init.style.display = 'none'
      //this.dom.rein.style.display = 'inline-block'
    }

    //document.getElementById('jifLock').classList.add('lock')

    // Append content to document
    document.querySelector('.body_jif_l').appendChild(this.baseElement)

    //jsup_new_record appended on a new record,
    //if true then init called on the initial page load
    if (this.newRecord) {
      // Set up setDefaults
      this.setDefaults()

      // Update any row values
      for (const el of this.baseElement.querySelectorAll('.itemnum')) this.rowTotal(el)

      // Save to DB
      this.saveSupplies(false)
    }

    this.supplyTotal()
  }

  async initSupplies() {
    const post = await fetch('suppliesPRG.php', {
      method: 'post',
      headers: { 'Content-Type': 'application/json;charset=utf-8' },
      body: JSON.stringify({ action: 'init', job: this.jobID }),
    })

    const res = await post.json()

    if (res.ok) {
      // this.supp will only have values on a reinit, so message
      if (Object.keys(this.supp).length) note({ text: 'Supplies have been reinitialized.' })

      if (Object.keys(res.supp).length) {
        this.supp = res.supp
        this.supplyForm()
      }

      // Set up setDefaults
      this.setDefaults()

      // Update any row values
      for (const el of this.baseElement.querySelectorAll('.itemnum')) this.rowTotal(el)

      // Save to DB
      this.saveSupplies(false)

      //trigger jifcontroller update
      window.jifController.update()
    } else {
      note({ text: res.msg || 'There has been an error with your request.', class: 'error' })
    }
  }

  async saveSupplies(msg = true) {
    // Collect info for supply items, and added misc items.
    // suppEl & miscEl are different types of data

    const supp = {},
      misc = []

    for (const suppEl of this.baseElement.querySelectorAll('.supply-item')) {
      supp[suppEl.dataset.jsup_id] = []
      supp[suppEl.dataset.jsup_id].push(suppEl.querySelector('[name=jsup_used_pri]').value)
      supp[suppEl.dataset.jsup_id].push(suppEl.querySelector('[name=jsup_used_sec]').value)
    }

    for (const miscEl of this.dom.MIS.querySelectorAll('tr:not(.defaultRow)')) {

      let retailAmtPri = Math.round( miscEl.querySelector('[name=jsup_misc_retail_pri').value * 100 + 0.001 )
      if (isNaN(retailAmtPri)) retailAmtPri = 0

      let retailAmtSec = Math.round( miscEl.querySelector('[name=jsup_misc_retail_sec').value * 100 + 0.001 )
      if (isNaN(retailAmtSec)) retailAmtSec = 0

      misc.push({
        jsup_misc_id: miscEl.querySelector('[name=jsup_misc_id').value,
        jsup_misc_name: miscEl.querySelector('[name=jsup_misc_name').value,
        jsup_misc_dsca: miscEl.querySelector('[name=jsup_misc_dsca').value,
        jsup_misc_retail_pri: retailAmtPri,
        jsup_misc_retail_sec: retailAmtSec,
      })
    }

    // Collect changes made to the Jif data
    const jif = {jif_num_days: this.base.numDays, jif_num_crew: this.base.numCrew}

    const post = await fetch('suppliesPRG.php', {
      method: 'post',
      headers: { 'Content-Type': 'application/json;charset=utf-8' },
      body: JSON.stringify({ action: 'save', job: this.jobID, supp: supp, misc: misc }),
      body: JSON.stringify({ action: 'save', job: this.jobID, supp: supp, misc: misc, jif: jif })
    })

    const res = await post.json()

    if (res.ok) {

      //remove any old notes
      for (const netEl of document.querySelectorAll('.note-popup'))
        netEl.remove()

      if (msg) note({ text: 'Your supplies have been saved.' })

      //this.dom.init.disabled = false
      this.dom.rein.disabled = false
      this.dom.save.disabled = true
      this.alert = false

      // Use return value to rebuild misc portion of the form
      this.misc = res.misc
      this.supplyFormMisc()

      //trigger jifcontroller update
      window.jifController.update()
    } else {
      note({ text: 'There has been an error with your request.', class: 'error' })
    }
    this.supplyTotal()
  }

  async trashMisc(miscSupID) {
    const post = await fetch('suppliesPRG.php', {
      method: 'post',
      headers: { 'Content-Type': 'application/json;charset=utf-8' },
      body: JSON.stringify({ action: 'trash', job: this.jobID, miscSupID: miscSupID })
    })

    const res = await post.json()

    if (res.ok) {
      // Use return value to rebuild misc portion of the form
      this.misc = res.misc
      this.supplyFormMisc()

      //trigger jifcontroller update
      window.jifController.update()
    } else {
      note({ text: 'There has been an error. The item has not been deleted.', class: 'error', time: 2500 })
    }
  }

  /**
   * Handles change events on the [type=number] inputs
   * @param {Element} el
   */
  changeEvent(el) {
    if (el.matches('[type=number]') || el.matches('[type=text]')) {

      //Suits & gloves: gloves = suits
      if (el.id == 'Pri-100109999' || el.id == 'Pri-100039999') {
        const gloves = document.getElementById('Pri-100039999'),
              amount = document.getElementById('Pri-100109999').value

        if (gloves.value.toNum() < amount) {
          gloves.value = amount
          this.rowTotal(gloves)
        }
      }

      //Bioboxes & liners: liners 2Xs bioboxes
      if (el.id == 'Pri-100750000' || el.id == 'Pri-100500000') {
        const liners = document.getElementById('Pri-100500000'),
              amount = document.getElementById('Pri-100750000').value * 2

        if (liners.value.toNum() < amount) {
          liners.value = amount
          this.rowTotal(liners)
        }
      }

      if (el.matches('.itemnum')) this.rowTotal(el)
      this.supplyTotal()
      //this.dom.init.disabled = true
      this.dom.rein.disabled = true
      this.dom.save.disabled = false

      if (!this.alert) {
        note({ text: 'There are unsaved changes on the page.', class: 'alert', time: 0 })
        this.alert = true
      }
    }
  }

  /**
   * Handles button actions
   * @param {Element} el
   */
  clickEvent(el) {
    switch (true) {
      case el.matches('[type=number]'):
        el.select()
        break
      //case el.matches(`#${this.dom.init.id}`):
      //  if (this.base.scpPri == 0) {
      //    note({
      //      text: 'Please select a primary scope prior to intializing this supply form.',
      //      time: 5000,
      //      class: 'error',
      //    })
      //  } else if (this.base.estPri == 0) {
      //    note({
      //      text: 'An initial supply estimate needs to be set prior to intializing this supply form.',
      //      time: 5000,
      //      class: 'error',
      //    })
      //  } else {
      //    this.initSupplies()
      //  }
      //  break
      case el.matches(`#${this.dom.rein.id}`):
        message({
          title: 'Reinitialize Supplies',
          text:
            'Click Yes to reset supply items to their recommended values.<br /><br /><button data-yes="1" class="modal-close inp12">Yes</button><button class="modal-close inp12">No</button>',
          time: 10000,
          confirm: this.initSupplies.bind(this)
        })
        break
      case el.matches(`#${this.dom.save.id}`):
        this.saveSupplies()
        break
      case el.matches(`#${this.dom.addn.id}`):
        // Clear out content if no tr rows
        if (this.dom.MIS.querySelectorAll('tr.defaultRow')[0]) this.dom.MIS.innerHTML = ''
        this.dom.MIS.append(this.supplyMiscRow())
        break
      case el.matches('[data-trash]'):
        const miscSupID = el.getAttribute('data-trash')
        if (miscSupID > 0) {
          message({
            title: 'Remove miscellaneous item?',
            text:
              'Click Yes to continue.<br /><br /><button data-yes="1" class="modal-close inp12">Yes</button><button class="modal-close inp12">No</button>',
            time: 10000,
            confirm: this.trashMisc.bind(this, miscSupID)
          })
        } else {
          el.closest('tr').remove()
          if (!this.dom.MIS.innerHTML.length) {
            this.dom.MIS.innerHTML = `<tr class="defaultRow"><td colspan="6" style="padding: 16px;">Miscellaneous Supplies (e.g., Plywood, Tools)</td></tr>`
          }
        }
        break
    }
  }

  /**
   * Handles keyup validation on the [type=number] input
   * @param {Element} el
   */
  keyupEvent(el) {
    if (el.matches('[type=number]') || el.matches('[type=text]')) {
      // step = 1 is integer, else money
      if (el.step == 1) {
        if (el.value.match(/\.\d/)) el.value = el.value.toNum().toFixed(0)
      } else {
        if (el.value.match(/\.\d\d\d/)) el.value = el.value.toNum().toFixed(2)
      }
    }
  }

  /**
   * Calculate a row total
   * @param {Element} el
   */
  rowTotal(el) {
    const itemTotalEl = el.closest('td').nextElementSibling
    if (el.value > 0) {
      itemTotalEl.setAttribute('data-itemtotal', Math.round(parseFloat(el.value) * parseInt(el.dataset.pennies) + 0.001))
      itemTotalEl.innerHTML = `${Util.formatCurrency(itemTotalEl.dataset.itemtotal)}`
    } else {
      itemTotalEl.setAttribute('data-itemtotal', 0)
      itemTotalEl.innerHTML = ''
    }
  }

  /**
   * Set default use amounts for some primary supply items
   */
  setDefaults() {
    // Painter's tape, Bio-Hazard Tape, 3M 77 Super Spray Adhesive
    // At least 1
    for (const sku of ['100620000', '100530000', '100240000']) {
      const el = document.getElementById(`Pri-${sku}`)
      if (!el) break
      el.value = 1
    }

    // Biotic Solvent, Biotic Disinfectant,  Cloth Towels
    // 1 per day
    for (const sku of ['100140000', '100150000', '100520000', '100510000', '100490000']) {
      const el = document.getElementById(`Pri-${sku}`)
      if (!el) break
      el.value = this.base.numDays
    }

    // Y/B Gloves
    // 1 per crew
    for (const sku of ['100029999']) {
      const el = document.getElementById(`Pri-${sku}`)
      if (!el) break
      el.value = this.base.numCrew
    }

    // Safety glasses, Surgical mask, Knee pads, Leather gloves, Boot Covers
    // 1 per day per crew
    for (const sku of ['100060000', '100090000', '100110000', '100050000', '100010000']) {
      const el = document.getElementById(`Pri-${sku}`)
      if (!el) break
      el.value = this.base.numDays * this.base.numCrew
    }

    // Plastic sheeting 100440000 if scope is BR/BS/BH, 2/3/4
    if ([2, 3, 4].includes(this.base.scpPri)) {
      const el = document.getElementById('Pri-100440000')
      if (el) el.value = 6
    }

    // Biotic Odor Counteractant 100130000 if scope is BS/BH, 3/4
    if ([3, 4].includes(this.base.scpPri)) {
      const el = document.getElementById('Pri-100130000')
      if (el) el.value = this.base.numDays
    }
  }

  /**
   * Add a supply item row
   * @param {Object} data
   * @returns {Object} HTML element  to append
   */
  supplyRow(data) {
    const row = document.createElement('tr')
    row.setAttribute('class', 'supply-item')
    row.setAttribute('data-jsup_id', data.jsup_id)
    row.innerHTML = `
      <td title="${data.jsup_sku}">${data.sku_nama}</td>
      <td>${data.sku_part || ''}</td>
      <td>${data.uom_dsca || ''}</td>
      <td class="r br" style="padding-right: 8px;">${Util.formatCurrency(data.jsup_retail)}&nbsp;</td>
      <td><input id="Pri-${
        data.jsup_sku
      }" class="itemnum" style="padding-right: 0; width: 100%; text-align: right;" name="jsup_used_pri" type="number" min="0" step="1" value="${
      data.jsup_used_pri
    }" data-pennies="${data.jsup_retail}" data-scope="pri" /></td>
      <td class="Pri-itemtotal r br" data-itemtotal="${data.jsup_retail * data.jsup_used_pri}">${
      data.jsup_used_pri > 0 ? Util.formatCurrency(data.jsup_retail * data.jsup_used_pri) : '&nbsp;'
    }</td>
      <td><input id="Sec-${
        data.jsup_sku
      }" class="itemnum" style="padding-right: 0; width: 100%; text-align: right;" name="jsup_used_sec" type="number" min="0" step="1" value="${
      data.jsup_used_sec
    }" data-pennies="${data.jsup_retail}" data-scope="sec"${this.base.scpSec ? '' : ' disabled="disabled"'} /></td>
      <td class="Sec-itemtotal r" data-itemtotal="${data.jsup_retail * data.jsup_used_sec}">${
      data.jsup_used_sec > 0 ? Util.formatCurrency(data.jsup_retail * data.jsup_used_sec) : '&nbsp;'
    }</td>`
    return row
  }

  /**
   * Add a new miscellaneous supply item row
   * @param {Object} data
   * @returns {Object} HTML element  to append
   */
  supplyMiscRow(data = {}) {
    const row = document.createElement('tr')
    row.innerHTML = `
      <td><input name="jsup_misc_id" type="hidden" value="${data.jsup_misc_id || 0}" /><input style="width: 80%;" name="jsup_misc_name" type="text" value="${data.jsup_misc_name || ''}" />
        <img class="icon16" data-trash="${data.jsup_misc_id || 0}" src="icons/trash16.png" alt="Delete" style="margin: 0 0 2px; padding: 0; cursor: pointer; filter: grayscale(1);" />
      </td>
      <td class="br"><input style="width: 80%;" name="jsup_misc_dsca" type="text" value="${
        data.jsup_misc_dsca || ''
      }" /></td>
      <td class="r"><span class="input-dollar-in"><input class="itemnum" style="padding-right: 0; width: 100%; text-align: right;" name="jsup_misc_retail_pri" type="number" min="0.00" step="0.01" value="${
        data.jsup_misc_retail_pri ? data.jsup_misc_retail_pri / 100 : '0.00'
      }" data-pennies="100" data-scope="pri" /></span></td>
      <td class="Pri-itemtotal br r" data-itemtotal="${data.jsup_misc_retail_pri || 0}">${
      data.jsup_misc_retail_pri > 0 ? Util.formatCurrency(data.jsup_misc_retail_pri) : '&nbsp;'
    }</td>
      <td class="r"><input class="itemnum" style="padding-right: 0; width: 100%; text-align: right;" name="jsup_misc_retail_sec" type="number" min="0.00" step="0.01" value="${
        data.jsup_misc_retail_sec ? data.jsup_misc_retail_sec / 100 : '0.00'
      }" data-pennies="100" data-scope="sec"${this.base.scpSec ? '' : ' disabled="disabled"'} /></td>
      <td class="Sec-itemtotal r" data-itemtotal="${data.jsup_misc_retail_sec || 0}">${
      data.jsup_misc_retail_sec > 0 ? Util.formatCurrency(data.jsup_misc_retail_sec) : '&nbsp;'
    }</td>`
    return row
  }

  /**
   * Reiterates the data and builds the form
   */
  supplyForm() {
    //this.dom.init.style.display = 'none'
    //this.dom.rein.style.display = 'inline-block'
    this.dom.addn.disabled = false
    this.dom.PPE.innerHTML = ''
    this.dom.CHE.innerHTML = ''
    this.dom.CON.innerHTML = ''

    for (const key in this.supp) {
      if (this.supp.hasOwnProperty(key))
        this.dom[this.supp[key]['item_cat_code']].append(this.supplyRow(this.supp[key]))
    }

    // Clear out defaut content if actual data coming from the server.
    if (Object.keys(this.misc).length) this.dom.MIS.innerHTML = ''

    this.supplyFormMisc()
  }

  /**
   * Reiterates the data and builds the misc supply form portion
   */
  supplyFormMisc() {
    if (Object.keys(this.misc).length) {
      this.dom.MIS.innerHTML = ''
    } else {
      this.dom.MIS.innerHTML = `<tr class="defaultRow"><td colspan="6" style="padding: 16px;">Miscellaneous Supplies (e.g., Plywood, Tools)</td></tr>`
    }
    for (const item of this.misc) this.dom.MIS.append(this.supplyMiscRow(item))
  }

  /**
   * Calculate supply total
   */
  supplyTotal() {
    // Reverse the assignment done in constructor
    // to be ready for a save event
    this.base.numDays = this.dom.numDays.value.toNum()
    this.base.numCrew = this.dom.numCrew.value.toNum()

    this.dom.supFee.innerHTML = Util.formatCurrency(this.base.supFee)

    for (const x of ['Pri', 'Sec']) {
      this.ttl[`sup${x}`] = 0
      this.ttl[`dif${x}`] = 0
      this.ttl[`ppe${x}`] = 0
      this.ttl[`che${x}`] = 0
      this.ttl[`con${x}`] = 0
      this.ttl[`mis${x}`] = 0

      for (const item of this.dom.PPE.querySelectorAll(`.${x}-itemtotal`))
        this.ttl[`ppe${x}`] += item.dataset.itemtotal.toNum()

      for (const item of this.dom.CHE.querySelectorAll(`.${x}-itemtotal`))
        this.ttl[`che${x}`] += item.dataset.itemtotal.toNum()

      for (const item of this.dom.CON.querySelectorAll(`.${x}-itemtotal`))
        this.ttl[`con${x}`] += item.dataset.itemtotal.toNum()

      for (const item of this.dom.MIS.querySelectorAll(`.${x}-itemtotal`))
        this.ttl[`mis${x}`] += item.dataset.itemtotal.toNum()

      this.ttl[`sup${x}`] = this.ttl[`ppe${x}`] + this.ttl[`che${x}`] + this.ttl[`con${x}`] + this.ttl[`mis${x}`]

      this.ttl[`dif${x}`] = this.ttl[`sup${x}`] - this.base[`est${x}`]
      this.ttl[`per${x}`] =
        this.base[`est${x}`] === 0 ? 0 : ((this.ttl[`sup${x}`] / this.base[`est${x}`]) * 100 - 100).toFixed(1)

      this.dom[`estTtl${x}`].innerHTML = Util.formatCurrency(this.base[`est${x}`])
      this.dom[`ppeTtl${x}`].innerHTML = Util.formatCurrency(this.ttl[`ppe${x}`])
      this.dom[`cheTtl${x}`].innerHTML = Util.formatCurrency(this.ttl[`che${x}`])
      this.dom[`conTtl${x}`].innerHTML = Util.formatCurrency(this.ttl[`con${x}`])
      this.dom[`misTtl${x}`].innerHTML = Util.formatCurrency(this.ttl[`mis${x}`])
      this.dom[`supTtl${x}`].innerHTML = Util.formatCurrency(this.ttl[`sup${x}`])

      let bgClass = ''

      if (Math.abs(this.ttl[`per${x}`]) >= this.base.highTh) {
        bgClass = 'hlneg'
      } else if (Math.abs(this.ttl[`per${x}`]) >= this.base.lowTh) {
        bgClass = 'hlyel'
      } else {
        bgClass = 'hlpos'
      }

      if (x == 'Sec' && !this.base.scpSec) bgClass = ''

      this.dom[`difTtl${x}`].className = bgClass
      this.dom[`difPer${x}`].className = bgClass

      this.dom[`difTtl${x}`].innerHTML = Util.formatCurrency(this.ttl[`dif${x}`])
      this.dom[`difPer${x}`].innerHTML = `(${this.ttl[`per${x}`]}%)`
    }

    this.ttl.estTtlTot = this.base.estPri + this.base.estSec
    this.dom.estTtlTot.innerHTML = Util.formatCurrency(this.ttl.estTtlTot)

    this.ttl.supTtlTot = this.ttl.supPri + this.ttl.supSec + this.base.supFee
    this.dom.supTtlTot.innerHTML = Util.formatCurrency(this.ttl.supTtlTot)

    this.ttl.difTtlTot = this.ttl.supTtlTot - this.ttl.estTtlTot
    this.ttl.perTtlTot =
      this.ttl.estTtlTot === 0 ? 0 : ((this.ttl.supTtlTot / this.ttl.estTtlTot) * 100 - 100).toFixed(1)

    let bgClass = ''

    if (Math.abs(this.ttl.perTtlTot) >= this.base.highTh) {
      bgClass = 'hlneg'
    } else if (Math.abs(this.ttl.perTtlTot) >= this.base.lowTh) {
      bgClass = 'hlyel'
    } else {
      bgClass = 'hlpos'
    }

    this.dom.difTtlTot.className = bgClass
    this.dom.difPerTot.className = bgClass

    this.dom.difTtlTot.innerHTML = Util.formatCurrency(this.ttl.difTtlTot)
    this.dom.difPerTot.innerHTML = `(${this.ttl.perTtlTot}%)`

    //console.log('supplyTotal updated')
  }
}
