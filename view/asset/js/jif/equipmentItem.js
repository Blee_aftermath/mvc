import { Util } from "../util.js"

/**
 * An item in the equipment section
 * @author Jake Cirino
 */
export class EquipmentItem{
  /**
   * Creates a new equipment item
   * @param {Equipment} parent
   * @param {JSON} data The data for this object, includes name, unit, price, num used and total
   * @param {String} template The HTML template for this item
   */
  constructor(parent, data, template){
    this.parent = parent
    this.data = data
    this.isSpecial = this.data.equip_prefix == 'eqp' && this.data.equip_suffix == 'special'

    //create base element and append HTML
    this.baseElement = document.createElement('tr')
    this.baseElement.setAttribute('name', this.data.equip_suffix)
    this.baseElement.innerHTML = template

    //select/set elements
    this.equipName = this.baseElement.querySelector("[name='equip_nama']")
    this.unit = this.baseElement.querySelector("[name='uom_dsca']")
    this.price = this.baseElement.querySelector("[name='price']")
    this.priceInput = this.baseElement.querySelector("[name='price-input']")
    this.amount = this.baseElement.querySelector("[name='num']")
    this.amountSec = this.baseElement.querySelector("[name='num-sec']")
    this.total = this.baseElement.querySelector("[name='total']")
    this.totalSec = this.baseElement.querySelector("[name='total-sec']")

    //populate fields
    this.equipName.innerText = this.data.equip_nama
    this.unit.innerText = this.data.uom_dsca
    this.amount.value = this.data.amount
    if(this.data.amountSec){
      this.amountSec.value = this.data.amountSec
    }

    //if this is special equipment, we need to be able to enter the price
    if(this.isSpecial){
      this.priceInput.value = (this.data.price/100).toFixed(2)
    }else{
      this.price.innerText = Util.formatCurrency(this.data.price)
    }

    //setup amount changed
    this.amount.addEventListener('change', ((event) => {
      this.data.amount = parseInt(event.target.value)
      this.update()
      this.parent.saveButton.disabled = false
    }).bind(this))
    if(this.data.amountSec){
      this.amountSec.addEventListener('change', event => {
        this.data.amountSec = parseInt(event.target.value)
        this.update()
        this.parent.saveButton.disabled = false
      })
    }else{
      this.amountSec.disabled = true
    }

    //setup price changed if this item is special equipment
    if(this.isSpecial){
      this.priceInput.addEventListener('change', ((event) => {
        this.data.price = parseFloat(event.target.value) * 100
        this.update()
        this.parent.saveButton.disabled = false
      }).bind(this))
    }
    
    this.update()
  }

  /**
   * Gets the amount in the amount field
   * @returns {Number}
   */
  getAmount(){
    return this.amount.value
  }

  /**
   * Gets the secondary scope amount in the amount field
   * @returns {Number}
   */
  getAmountSec(){
    return this.amountSec.value
  }

  /**
   * Sets the amount in this items amount field, and updates the data
   * @param {Number} amount 
   */
  setAmount(amount){
    this.amount.value = amount
    this.data.amount = amount
    this.update()
  }

  /**
   * Sets the amount in this items secondary amount field, and updates the data
   * @param {Number} amount 
   */
  setAmountSec(amount){
    this.amountSec.value = amount
    this.data.amountSec = amount
    this.update()
  }

  /**
   * Updates totals and amounts
   */
  update(){
    //update price if this item is special equipment
    this.data.total = this.data.price * this.data.amount + .001
    if(this.data.amountSec){
      this.data.totalSec = this.data.price * this.data.amountSec + .001
      this.totalSec.innerText = Util.formatCurrency(this.data.totalSec)
    } else{
      this.totalSec.innerText = Util.formatCurrency(0)
    }
    
    this.total.innerText = Util.formatCurrency(this.data.total)

    this.parent.updateTotals()
  }
}