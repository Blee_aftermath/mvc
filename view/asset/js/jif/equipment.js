import { Util } from '../util.js'
import { YNModal } from '../ynModal.js'
import { EquipmentItem } from './equipmentItem.js'

/**
 * Contains information on the equipment used in the jobs
 */
export class Equipment {
  /**
   * Creates a new equipment section
   * @param {JSON} data
   */
  constructor(data) {
    this.data = data
    this.equipItems = {}
    this.filterItems = {}
    this.disItems = {}

    this.newRecord = data.primaryEquipment.new
    data.primaryEquipment = data.primaryEquipment.data

    //create base element
    this.baseElement = document.createElement('div')
    this.baseElement.innerHTML = this.data.template

    //create/select
    this.primaryTitle = this.baseElement.querySelector("[name='primaryTitle']")
    this.secondaryTitle = this.baseElement.querySelector("[name='secondaryTitle']")
    this.iniCost = this.baseElement.querySelector('#estTtl')
    this.equipCost = this.baseElement.querySelector('#eqpTtl')
    this.difCost = this.baseElement.querySelector('#difTtl')
    this.difPriPer = this.baseElement.querySelector('#difPriPer')
    this.iniSecCost = this.baseElement.querySelector('#estTtlSec')
    this.equipSecCost = this.baseElement.querySelector('#eqpTtlSec')
    this.difSecCost = this.baseElement.querySelector('#difTtlSec')
    this.iniTotCost = this.baseElement.querySelector('#estTtlTot')
    this.equipTotCost = this.baseElement.querySelector('#eqpTtlTot')
    this.difTotCost = this.baseElement.querySelector('#difTtlTot')
    this.difTotPer = this.baseElement.querySelector('#difTtlPer')
    this.inputCrew = this.baseElement.querySelector('#numCrew')
    this.inputDays = this.baseElement.querySelector('#numDays')
    this.reinitButton = this.baseElement.querySelector('#init')
    this.saveButton = this.baseElement.querySelector('#save')

    this.equipTable = this.baseElement.querySelector("[name='equip-items']")
    this.equipTotalRow = this.equipTable.children[0]
    this.equipTotal = this.equipTable.querySelector("[name='total-row']")
    this.equipSecTotal = this.equipTable.querySelector("[name='total-row-sec']")
    this.filterTable = this.baseElement.querySelector("[name='filter-items']")
    this.filterTotalRow = this.filterTable.children[0]
    this.filterTotal = this.filterTable.querySelector("[name='total-row']")
    this.filterSecTotal = this.filterTable.querySelector("[name='total-row-sec']")
    this.disTable = this.baseElement.querySelector("[name='dis-items']")
    this.disTotalRow = this.disTable.children[0]
    this.disTotal = this.disTable.querySelector("[name='total-row']")
    this.disSecTotal = this.disTable.querySelector("[name='total-row-sec']")

    //setup header 
    this.primaryTitle.innerText = `Primary Scope (${this.data.scopeCode})`
    this.iniCost.innerText = Util.formatCurrency(this.data.estimateData.jini_calc_eqp)

    //Errors are thrown if in iniest 2nd scope removed, but 2nd equip record exists.
    //Proer correction is to delete equip rec if 2nd scope removed, but for now
    //make secondaryEquipment false if estimateDataSec is null
    if (this.data.estimateDataSec == null)
      this.data.secondaryEquipment = false

    if (this.data.secondaryEquipment) {
      this.secondaryTitle.innerText = `Secondary Scope (${this.data.scopeCodeSec})`
      this.iniSecCost.innerText = Util.formatCurrency(this.data.estimateDataSec.jini_calc_eqp)
      this.iniTotCost.innerText = Util.formatCurrency(
        parseFloat(this.data.estimateData.jini_calc_eqp) + parseFloat(this.data.estimateDataSec.jini_calc_eqp)
      )
    } else {
      this.iniTotCost.innerText = Util.formatCurrency(this.data.estimateData.jini_calc_eqp)
    }

    //setup reinit button functionality
    this.reinitButton.addEventListener(
      'click',
      (() => {
        new YNModal(
          'Initialize Equipemnt',
          'Click Yes to reset equipment items to their recommended values.',
          (result) => {
            if (result) {
              this.initialize()
            }
          }
        )
      }).bind(this)
    )

    //setup crew/days
    this.inputDays.innerText = this.data.jobCrew.jif_num_days
    this.inputDays.value = this.data.jobCrew.jif_num_days
    this.inputDays.addEventListener('change', (event) => {
      this.data.jobCrew.jif_num_days = event.target.value
      this.saveButton.disabled = false
    })

    this.inputCrew.innerText = this.data.jobCrew.jif_num_crew
    this.inputCrew.value = this.data.jobCrew.jif_num_crew
    this.inputCrew.addEventListener('change', (event) => {
      this.data.jobCrew.jif_num_crew = event.target.value
      this.saveButton.disabled = false
    })

    //initialize equipment items
    for (const key in this.data.equipmentItems) {
      if (this.data.equipmentItems.hasOwnProperty(key)) {
        let data = this.data.equipmentItems[key]
        let priceField = `jeqp_price_${data.equip_prefix}_${data.equip_suffix}`,
          numField = `jeqp_used_${data.equip_prefix}_${data.equip_suffix}`
        data.price = this.data.primaryEquipment[priceField]
        data.amount = this.data.primaryEquipment[numField]

        if (this.data.secondaryEquipment) {
          data.amountSec = this.data.secondaryEquipment[numField]
        }

        //TODO primary and secondary data

        //create item and append to item lists
        let equipItem = new EquipmentItem(this, data, this.data.itemTemplate)
        switch (data.equip_prefix) {
          case 'eqp':
            this.equipTable.insertBefore(equipItem.baseElement, this.equipTotalRow)
            this.equipItems[data.equip_suffix] = equipItem
            break
          case 'fil':
            this.filterTable.insertBefore(equipItem.baseElement, this.filterTotalRow)
            this.filterItems[data.equip_suffix] = equipItem
            break
          case 'dis':
            this.disTable.insertBefore(equipItem.baseElement, this.disTotalRow)
            this.disItems[data.equip_suffix] = equipItem
            break
        }
      }
    }
    this.initItemConstraints()

    //setup save button functionality
    this.saveButton.addEventListener(
      'click',
      ((event) => {
        this.save()
      }).bind(this)
    )

    //if true then init called on the initial page load only
    if (this.newRecord) this.initialize()

    //append to document
    document.querySelector('.body_jif_l').appendChild(this.baseElement)
    this.updateTotals()
  }

  /**
   * Initializes the ties and constraints between different equipment item fields
   */
  initItemConstraints() {
    /*
    We use try/catch statements to attempt to constrain every field regardless of job type.
    This allows us to get away from finnicky class-based conditionals for equip items.
    - Jake
    */
    //hepa500
    try{
      this.equipItems.hepa500.amount.addEventListener('change', (event) => {
        this.filterItems.air1stage.setAmount(event.target.value)
        this.disItems.hepa500.setAmount(event.target.value)
  
        let filter23 = event.target.value >= 1 ? 1 : 0
        this.filterItems.air2stage.setAmount(filter23)
        this.filterItems.air3stage.setAmount(filter23)
      })
      this.equipItems.hepa500.amountSec.addEventListener('change', (event) => {
        this.filterItems.air1stage.setAmountSec(event.target.value)
        this.disItems.hepa500.setAmountSec(event.target.value)
  
        let filter23 = event.target.value >= 1 ? 1 : 0
        this.filterItems.air2stage.setAmountSec(filter23)
        this.filterItems.air3stage.setAmountSec(filter23)
      })
    }catch(err){}

    //hepavac
    //***** 2021-01-29: No default for Filter section for HEPA Filter
    //***** 2021-07-12: Default of at least 1 for Filter section for HEPA Filter
    
    //***** 2021-01-29: At least 1 for Disinfection section for HEPA Filter
    try{
      this.equipItems.hepavac.amount.addEventListener('change', (event) => {
        this.filterItems.hepavac.setAmount(event.target.value >= 1 ? 1 : 0)
        //this.disItems.hepavac.setAmount(event.target.value)
        if (event.target.value > 0) {
          let hepaAmt = this.disItems.hepavac.getAmount()
          this.disItems.hepavac.setAmount( hepaAmt > 0 ? hepaAmt : 1 )
        } else {
          this.disItems.hepavac.setAmount(0)
        }
      })
      this.equipItems.hepavac.amountSec.addEventListener('change', (event) => {
        this.filterItems.hepavac.setAmountSec(event.target.value >= 1 ? 1 : 0)
        if (event.target.value > 0) {
          let hepaAmt = this.disItems.hepavac.getAmount()
          this.disItems.hepavac.setAmount( hepaAmt > 0 ? hepaAmt : 1 )
        } else {
          this.disItems.hepavac.setAmount(0)
        }
      })
    }catch(err){}

    //air mover
    try{
      this.equipItems.airmover.amount.addEventListener('change', (event) => {
        this.disItems.airmover.setAmount(event.target.value)
      })
      this.equipItems.airmover.amountSec.addEventListener('change', (event) => {
        this.disItems.airmover.setAmountSec(event.target.value)
      })
    }catch(err){}

    //hydroxyl gen
    try{
      this.equipItems.hydroxyl.amount.addEventListener('change', (event) => {
        this.filterItems.hydroxyl.setAmount(event.target.value > 0 ? 1 : 0)
        this.disItems.hydroxyl.setAmount(event.target.value)
      })
      this.equipItems.hydroxyl.amountSec.addEventListener('change', (event) => {
        this.filterItems.hydroxyl.setAmountSec(event.target.value > 0 ? 1 : 0)
        this.disItems.hydroxyl.setAmountSec(event.target.value)
      })
    }catch(err){}

    //electro 5gal
    try{
      this.equipItems.electro5gal.amount.addEventListener('change', (event) => {
        let total = parseInt(this.equipItems.electrohand.getAmount()) + parseInt(event.target.value)
        this.disItems.electro.setAmount(total)
      })
      this.equipItems.electro5gal.amountSec.addEventListener('change', (event) => {
        let total = parseInt(this.equipItems.electrohand.getAmountSec()) + parseInt(event.target.value)
        this.disItems.electro.setAmountSec(total)
      })
    }catch(err){}

    //electro hand
    try{
      this.equipItems.electrohand.amount.addEventListener('change', (event) => {
        let total = parseInt(this.equipItems.electro5gal.getAmount()) + parseInt(event.target.value)
        this.disItems.electro.setAmount(total)
      })
      this.equipItems.electrohand.amountSec.addEventListener('change', (event) => {
        let total = parseInt(this.equipItems.electro5gal.getAmountSec()) + parseInt(event.target.value)
        this.disItems.electro.setAmountSec(total)
      })
    }catch(err){}

    //power tools
    try{
      this.equipItems.powertools.amount.addEventListener('change', (event) => {
        this.disItems.powertools.setAmount(event.target.value)
      })
      this.equipItems.powertools.amountSec.addEventListener('change', (event) => {
        this.disItems.powertools.setAmountSec(event.target.value)
      })
    }catch(err){}

    //respirators
    try{
      this.equipItems.respirator.amount.addEventListener('change', (event) => {
        this.filterItems.cartridge.setAmount(event.target.value)
        this.disItems.respirator.setAmount(event.target.value)
      })
      this.equipItems.respirator.amountSec.addEventListener('change', (event) => {
        this.filterItems.cartridge.setAmountSec(event.target.value)
        this.disItems.respirator.setAmountSec(event.target.value)
      })
    }catch(err){}

    //weights
    try{
      for (let item of [this.equipItems, this.disItems]) {
        item.weights.amount.addEventListener('change', (event) => {
          let amount = event.target.value >= 1 ? 1 : 0 //cap at 1
          this.equipItems.weights.setAmount(amount)
          this.disItems.weights.setAmount(amount)
        })
        if (item.weights.amountSec) {
          item.weights.amountSec.addEventListener('change', (event) => {
            let amount = event.target.value >= 1 ? 1 : 0 //cap at 1
            this.equipItems.weights.setAmountSec(amount)
            this.disItems.weights.setAmountSec(amount)
          })
        }
      }
    }catch(err){}

    //hand tools disinfection
    try{
      this.disItems.handtools.amount.addEventListener('change', (event) => {
        let days = this.data.jobCrew.jif_num_days
        this.disItems.handtools.setAmount(event.target.value < days ? days : event.target.value)
      })
    }catch(err){}

    //special
    try{
      this.equipItems.special.amount.addEventListener('change', (event) => {
        this.disItems.special.setAmount(event.target.value)
      })
      this.equipItems.special.amountSec.addEventListener('change', (event) => {
        this.disItems.special.setAmountSec(event.target.value)
      })
    }catch(err){}

    //reset special price
    try{
      this.equipItems.special.price.value = 0.0
    }catch(err){}
  }

  /**
   * Runs initialization calculations based on crew members, job days, ect.
   */
  initialize() {
    //initialize calc variables
    let days = this.data.jobCrew.jif_num_days,
      crewSize = this.data.jobCrew.jif_num_crew,
      scope = this.data.scopeCode,
      bioBoxes = Number(this.data.estimateData.jini_num_box_debris)

    // == EQUIPMENT == //
    //hepa 500
    let hepa500 = days
    try{
      this.equipItems['hepa500'].setAmount(hepa500)
    }catch(err){}

    //hepavac
    let hepavac = 1
    try{
      this.equipItems['hepavac'].setAmount(hepavac)
    }catch(err){}

    //air mover
    let airmover = 0
    try{
      this.equipItems['airmover'].setAmount(airmover)
    }catch(err){}

    //hydroxyl generator
    let hydroxyl = 0
    try{
      this.equipItems['hydroxyl'].setAmount(hydroxyl)
    }catch(err){}

    //electro 5gal
    try{
      this.equipItems['electro5gal'].setAmount(0)
    }catch(err){}

    //electro handheld
    try{
      this.equipItems['electrohand'].setAmount(0)
    }catch(err){}
    
    //powertools
    try{
      this.equipItems['powertools'].setAmount(0)
    }catch(err){}
    
    //respirators TODO complex calculation
    let resp = crewSize
    try{
      this.equipItems['respirator'].setAmount(resp)
    }catch(err){}
    
    //weights and measurements
    try{
      this.equipItems['weights'].setAmount(bioBoxes >= 1 ? 1 : 0)
    }catch(err){}
    
    //special
    try{
      this.equipItems['special'].setAmount(0)
    }catch(err){}
    
    // == FILTERS == //
    //1st stage filters
    let filter1 = days
    try{
      this.filterItems['air1stage'].setAmount(filter1)
    }catch(err){}
    
    //2nd/3rd stage filters
    let filter23 = hepa500 >= 1 ? 1 : 0
    try{
      this.filterItems['air2stage'].setAmount(filter23)
      this.filterItems['air3stage'].setAmount(filter23)
    }catch(err){}
    
    //cartridges
    try{
      this.filterItems['cartridge'].setAmount(resp) //same as respirator
    }catch(err){}
    
    //hyroxyl
    try{
      this.filterItems['hydroxyl'].setAmount(hydroxyl >= 1 ? 1 : 0)
    }catch(err){}
    
    //hepavac
    //***** 2021-01-29: No default for Filter section "HEPA Filter for Shop Vacuum" *****
    //***** 2021-07-12: Default set to 1 if "Equipment HEPA Vacuum" is indicated *****
    try{
      this.filterItems['hepavac'].setAmount(hepavac >= 1 ? 1 : 0)
      //this.filterItems['hepavac'].setAmount(0)
    }catch(err){}
    
    // == DISINFECTION == //
    //hepa500
    try{
      this.disItems['hepa500'].setAmount(hepa500)
    }catch(err){}
    
    //hepavac
    try{
      this.disItems['hepavac'].setAmount(hepavac)
    }catch(err){}
    
    //air mover
    try{
      this.disItems['airmover'].setAmount(airmover)
    }catch(err){}
    
    //hydroxyl
    try{
      this.disItems['hydroxyl'].setAmount(hydroxyl)
    }catch(err){}
    
    //electrostatic TODO calculate
    try{
      this.disItems['electro'].setAmount(0)
    }catch(err){}
    
    //powertools
    try{
      this.disItems['powertools'].setAmount(0)
    }catch(err){}
    
    //handtools
    try{
      this.disItems['handtools'].setAmount(days)
    }catch(err){}
    
    //respiratory
    try{
      this.disItems['respirator'].setAmount(crewSize * days)
    }catch(err){}
    
    //weights
    try{
      this.disItems['weights'].setAmount(bioBoxes >= 1 ? 1 : 0)
    }catch(err){}

    //special TODO calc
    try{
      this.disItems['special'].setAmount(0)
    }catch(err){}
    
    //init secondary to 0
    if(this.data.secondaryEquipment){
      //set equip items to 0
      for (const key in this.equipItems) {
        if (Object.hasOwnProperty.call(this.equipItems, key)) {
          const element = this.equipItems[key];
          element.setAmountSec(0)
        }
      }

      //set filter items to 0
      for (const key in this.filterItems) {
        if (Object.hasOwnProperty.call(this.filterItems, key)) {
          const element = this.filterItems[key];
          element.setAmountSec(0)
        }
      }

      //set disinfection items
      for (const key in this.disItems) {
        if (Object.hasOwnProperty.call(this.disItems, key)) {
          const element = this.disItems[key];
          element.setAmountSec(0)
        }
      }
    }

    //save the equipment fields
    //this.save()
    this.updateTotals()
  }

  /**
   * Updates the totals for each section
   */
  updateTotals() {
    //update equip totals
    let equipTotal = 0,
      equipSecTotal = 0
    for (const key in this.equipItems) {
      if (this.equipItems.hasOwnProperty(key)) {
        const element = this.equipItems[key]

        equipTotal += element.data.total

        if (element.data.totalSec) equipSecTotal += element.data.totalSec
      }
    }
    this.equipTotal.innerText = Util.formatCurrency(equipTotal)
    this.equipSecTotal.innerText = Util.formatCurrency(equipSecTotal)

    //update filter totals
    let filterTotal = 0,
      filterSecTotal = 0
    for (const key in this.filterItems) {
      if (this.filterItems.hasOwnProperty(key)) {
        const element = this.filterItems[key]

        filterTotal += element.data.total

        if (element.data.totalSec) filterSecTotal += element.data.totalSec
      }
    }
    this.filterTotal.innerText = Util.formatCurrency(filterTotal)
    this.filterSecTotal.innerText = Util.formatCurrency(filterSecTotal)

    //update disinfection totals
    let disTotal = 0,
      disSecTotal = 0
    for (const key in this.disItems) {
      if (this.disItems.hasOwnProperty(key)) {
        const element = this.disItems[key]

        disTotal += element.data.total

        if (element.data.totalSec) disSecTotal += element.data.totalSec
      }
    }
    this.disTotal.innerText = Util.formatCurrency(disTotal)
    this.disSecTotal.innerText = Util.formatCurrency(disSecTotal)

    //update overall totals in header
    let allTotal = equipTotal + filterTotal + disTotal,
      allSecTotal = equipSecTotal + filterSecTotal + disSecTotal,
      allTotTotal = allTotal + allSecTotal

    this.equipCost.innerText = Util.formatCurrency(allTotal)
    this.equipSecCost.innerText = Util.formatCurrency(allSecTotal)
    this.equipTotCost.innerText = Util.formatCurrency(allTotTotal)

    //calc pri diff
    let difCalc = allTotal - this.data.estimateData.jini_calc_eqp
    let percentDiff = (allTotal / this.data.estimateData.jini_calc_eqp) * 100 - 100
    if (Math.abs(percentDiff) >= 20) {
      this.difCost.className = 'hlneg'
      this.difPriPer.className = 'hlneg'
    } else if (Math.abs(percentDiff) >= 10) {
      this.difCost.className = 'hlyel'
      this.difPriPer.className = 'hlyel'
    } else {
      this.difCost.className = 'hlpos'
      this.difPriPer.className = 'hlpos'
    }
    this.difCost.innerText = `${Util.formatCurrency(difCalc)}`
    this.difPriPer.innerText = `(${percentDiff.toFixed(1)}%)`

    //calc sec diff
    let difCalcSec = 0,
      percentDiffSec = 0
    if (this.data.secondaryEquipment) {
      difCalcSec = allSecTotal - this.data.estimateDataSec.jini_calc_eqp
      percentDiffSec = (allSecTotal / this.data.estimateDataSec.jini_calc_eqp) * 100 - 100
      this.difSecCost.innerText = `${Util.formatCurrency(difCalcSec)}`
    }

    //calc total diff
    let difCalcTot, percentDiffTot
    if (this.data.secondaryEquipment) {
      difCalcTot = difCalc + difCalcSec
      percentDiffTot = (allTotTotal / (parseInt(this.data.estimateData.jini_calc_eqp) + parseInt(this.data.estimateDataSec.jini_calc_eqp))) * 100 - 100
    } else {
      difCalcTot = difCalc
      percentDiffTot = percentDiff
    }
    if (Math.abs(percentDiffTot) >= 20) {
      this.difTotCost.className = 'hlneg'
      this.difTotPer.className = 'hlneg'
    } else if (Math.abs(percentDiffTot) >= 10) {
      this.difTotCost.className = 'hlyel'
      this.difTotPer.className = 'hlyel'
    } else {
      this.difTotCost.className = 'hlpos'
      this.difTotPer.className = 'hlpos'
    }
    this.difTotCost.innerText = `${Util.formatCurrency(difCalcTot)}`
    this.difTotPer.innerText = `(${percentDiffTot.toFixed(1)}%)`
  }

  /**
   * Gets usedData for form submission from all equipment items
   * @returns {JSON}
   */
  getUsedFields() {
    let usedData = {}

    //equip
    for (const key in this.equipItems) {
      if (this.equipItems.hasOwnProperty(key)) {
        const element = this.equipItems[key]
        let data = element.data

        usedData[`${data.equip_prefix}_${data.equip_suffix}`] = data.amount
      }
    }

    //filter
    for (const key in this.filterItems) {
      if (this.filterItems.hasOwnProperty(key)) {
        const element = this.filterItems[key]
        let data = element.data

        usedData[`${data.equip_prefix}_${data.equip_suffix}`] = data.amount
      }
    }

    //disinfection
    for (const key in this.disItems) {
      if (this.disItems.hasOwnProperty(key)) {
        const element = this.disItems[key]
        let data = element.data

        usedData[`${data.equip_prefix}_${data.equip_suffix}`] = data.amount
      }
    }

    return usedData
  }

  getSecUsedFields() {
    let usedData = {}

    //equip
    for (const key in this.equipItems) {
      if (this.equipItems.hasOwnProperty(key)) {
        const element = this.equipItems[key]
        let data = element.data

        usedData[`${data.equip_prefix}_${data.equip_suffix}`] = data.amountSec
      }
    }

    //filter
    for (const key in this.filterItems) {
      if (this.filterItems.hasOwnProperty(key)) {
        const element = this.filterItems[key]
        let data = element.data

        usedData[`${data.equip_prefix}_${data.equip_suffix}`] = data.amountSec
      }
    }

    //disinfection
    for (const key in this.disItems) {
      if (this.disItems.hasOwnProperty(key)) {
        const element = this.disItems[key]
        let data = element.data

        usedData[`${data.equip_prefix}_${data.equip_suffix}`] = data.amountSec
      }
    }

    return usedData
  }

  /**
   * Gets the special cost field data from equipment items
   * @returns {Number}
   */
  getSpecialCost() {
    for (const key in this.equipItems) {
      if (this.equipItems.hasOwnProperty(key)) {
        const element = this.equipItems[key]

        if (element.isSpecial) {
          return element.data.price
        }
      }
    }

    return null
  }

  /**
   * Saves the state of entries on the form
   */
  save() {
    //save crew/job
    this.saveJobCrewItems(
      ((result) => {
        //TODO save equipment items
        if (result) {
          this.saveEquipmentItems(
            ((ok) => {
              if (ok) {
                //disable save and trigger jifcontroller update
                this.saveButton.disabled = true
                if (typeof window.jifController !== 'undefined') {
                  window.jifController.update()
                }
              } else {
                //TODO
              }
            }).bind(this)
          )
        } else {
          //TODO
        }
      }).bind(this)
    )
  }

  /**
   * Saves the equipment items
   * @param {Function} callback
   */
  saveEquipmentItems(callback) {
    //setup request body
    let body = {
      action: 'updateEquipment',
      equipID: this.data.primaryEquipment.jeqp_id,
      usedData: this.getUsedFields(),
      specialCost: this.getSpecialCost(),
    }
    if(this.data.secondaryEquipment){
      body.equipIDSec = this.data.secondaryEquipment.jeqp_id,
      body.usedDataSec = this.getSecUsedFields()
    }

    //execute fetch
    fetch('equipmentPRG.php', {
      method: 'post',
      headers: { 'Content-Type': 'application/json;charset=utf8' },
      body: JSON.stringify(body),
    })
      .then((res) => res.json())
      .then((body) => {
        callback(body.ok)
      })
  }

  /**
   * Saves the job crew items
   * @param {Function} callback
   */
  saveJobCrewItems(callback) {
    fetch('jifItemPRG.php', {
      method: 'post',
      headers: { 'Content-Type': 'application/json;charset=utf8' },
      body: JSON.stringify({
        action: 'updateJobCrewInfo',
        jobID: this.data.primaryEquipment.jeqp_job,
        crewSize: this.inputCrew.value,
        jobDays: this.inputDays.value,
      }),
    })
      .then((res) => res.json())
      .then((body) => {
        callback(body.ok)
      })
  }
}
