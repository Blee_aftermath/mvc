/**
 * Percentage hours difference to display the row in red
 */
const MARGIN_RED = 0.3
/**
 * Percentage hours difference to display the row in yellow
 */
const MARGIN_YELLOW = 0.1

/**
 * An item in the timesheet info section
 * @author Jake Cirino
 */
export class TimesheetInfoItem {
  /**
   * Creates a new timesheet info item
   * @param {Timesheet} parent
   * @param {JSON} data
   * @param {String} scope Either pri/sec/alll
   * @param {JSON} initialHours
   */
  constructor(parent, data, scope, initialHours) {
    this.parent = parent
    this.data = data
    this.scope = scope
    this.initialHours = initialHours
    this.fields = {}

    //total vars
    this.bilSup = 0
    this.bilTec = 0
    this.bilTot = 0
    this.nonbilSup = 0
    this.nonbilTec = 0
    this.nonbilTot = 0

    this.baseElement = document.createElement('tr')
  }

  populate() {
    //load and apply template
    this.baseElement.innerHTML = window.infoTemplate

    this.progIniField = this.baseElement.querySelector("[name='prog-ini']")
    this.progDiffField = this.baseElement.querySelector("[name='prog-diff']")

    //append fields to json object
    for (const index in this.baseElement.children) {
      if (this.baseElement.children.hasOwnProperty(index)) {
        const element = this.baseElement.children[index]
        this.fields[element.getAttribute('name')] = element
      }
    }

    //populate fields
    this.fields.activity.innerText = this.data.act_dsca

    //append to table
    let section = ''
    switch (this.scope) {
      case 'pri':
        section = 'info-pri-totals'
        break
      case 'sec':
        section = 'info-sec-totals'
        break
      case 'all':
        section = 'info-totals'
        break
    }
    /*
    switch (Number.parseInt(this.data.act_ts_section)) {
      case 1:
        section = 'info-pri-totals'
        break
      case 2:
        section = 'info-job-totals'
        break
      case 3:
        section = 'info-totals'
        break
    }*/
    this.parent.baseElement
      .querySelector("[name='info-body']")
      .insertBefore(this.baseElement, this.parent.baseElement.querySelector(`[name='${section}']`))
  }

  /**
   * Calculates the totals for the table
   */
  calculate() {
    //calculate billable info
    this.bilSup = 0
    this.bilTec = 0
    this.progIni = Number.parseInt(this.data.act_id) != 1 ? Number.parseFloat(this.initialHours) : 0
    this.nonbilSup = 0
    this.nonbilTec = 0

    for (const key in this.parent.items) {
      if (this.parent.items.hasOwnProperty(key)) {
        const data = this.parent.items[key].data

        //check if field is valid to calculate
        if (data.ts_activity == this.data.act_id 
          && this.scope == (data.act_ts_section == 3 ? 'all' : data.ts_seq == 1 ? 'pri' : 'sec')) {
          let duration = Number.parseFloat(data.ts_duration)
          //check if billable
          if (data.ts_is_billable == 1) {
            //check if sup or tec
            if (data.ts_crew_type == 1) {
              this.bilSup += duration
            } else {
              this.bilTec += duration
            }
          } else {
            //check if sup or tec
            if (data.ts_crew_type == 1) {
              this.nonbilSup += duration
            } else {
              this.nonbilTec += duration
            }
          }
        }
      }
    }

    //calculate totals/diff
    (this.bilTot = this.bilSup + this.bilTec), (this.nonbilTot = this.nonbilSup + this.nonbilTec)
    this.progDiff = this.bilTot - this.progIni

    //conditionally show prog
    if (this.data.act_ts_section != 3) {
      this.fields['bill-sup'].innerText = this.bilSup
      this.fields['bill-tec'].innerText = this.bilTec
      this.fields['bill-tot'].innerText = this.bilTot
      this.fields['prog-ini'].innerText = this.progIni
      this.fields['prog-diff'].innerText = this.progDiff
    }

    //calculate difference
    try {
      this.progIniField.style.backgroundColor = ''
      this.progDiffField.style.backgroundColor = ''

      let percentDiff = Math.abs(this.progDiff) / this.progIni

      if (percentDiff > MARGIN_RED) {
        this.progDiffField.style.backgroundColor = '#f99'
      } else if (percentDiff > MARGIN_YELLOW) {
        this.progDiffField.style.backgroundColor = '#ff9'
      }
    } catch (ex) {
      console.log(ex)
    }

    this.fields['nonbill-sup'].innerText = this.nonbilSup
    this.fields['nonbill-tec'].innerText = this.nonbilTec
    this.fields['nonbill-tot'].innerText = this.nonbilTot
  }
}
