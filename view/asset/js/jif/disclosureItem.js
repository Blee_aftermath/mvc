import { Util } from '../util.js'

/**
 * An entry for the disclosure
 * @author Jake Cirino
 */
export class DisclosureItem {
  /**
   * Creates a new personal property disclosure item
   * @param {PersonalProperty} parent
   * @param {JSON} data
   */
  constructor(parent, data) {
    this.parent = parent
    this.data = data
    this.editMode = false

    this.baseElement = document.createElement('tr')
    this.populate()
  }

  async populate() {
    let template = await window.templateController.getTemplate('jif/disclosureItem')
    this.baseElement.innerHTML = template

    //select/set elements
    this.description = this.baseElement.querySelector("[name='description']")
    this.descriptionEdit = this.baseElement.querySelector("[name='descriptionEdit']")
    this.descriptionField = this.baseElement.querySelector("[name='descriptionField']")
    this.location = this.baseElement.querySelector("[name='location']")
    this.locationEdit = this.baseElement.querySelector("[name='locationEdit']")
    this.locationField = this.baseElement.querySelector("[name='locationField']")
    this.itemFound = this.baseElement.querySelector("[name='itemFound']")
    this.itemFoundEdit = this.baseElement.querySelector("[name='itemFoundEdit']")
    this.itemFoundField = this.baseElement.querySelector("[name='itemFoundField']")
    this.itemGivenTo = this.baseElement.querySelector("[name='itemGivenTo']")
    this.itemGivenToEdit = this.baseElement.querySelector("[name='itemGivenToEdit']")
    this.itemGivenToField = this.baseElement.querySelector("[name='itemGivenToField']")
    this.saveEdit = this.baseElement.querySelector("[name='saveEdit']")
    this.buttons = this.baseElement.querySelector("[name='buttons']")

    //setup buttons
    this.editButton = this.baseElement.querySelector("[name='editButton']")
    this.editButton.addEventListener(
      'click',
      (() => {
        this.setEditMode(true)
      }).bind(this)
    )

    this.deleteButton = this.baseElement.querySelector("[name='deleteButton']")
    this.deleteButton.addEventListener('click', () => {
      this.deleteDisclosureItem()
    })

    this.saveButton = this.baseElement.querySelector("[name='saveButton']")
    this.saveButton.addEventListener(
      'click',
      (() => {
        this.updateDisclosureItem()
      }).bind(this)
    )

    //initial data population
    this.setData(
      this.data.ppdisc_dsca,
      this.data.ppdisc_loc,
      Util.fromMysqlBool(this.data.ppdisc_is_found),
      this.data.ppdisc_givento
    )

    //append to document
    this.parent.disclosure.insertBefore(this.baseElement, this.parent.disclosureItemsElement)
  }

  /**
   * Sets the data
   * @param {String} description
   * @param {String} location
   * @param {Boolean} itemFound
   * @param {String} itemGivenTo
   */
  setData(description, location, itemFound, itemGivenTo) {
    //set data
    this.data.ppdisc_dsca = description
    this.data.ppdisc_loc = location
    this.data.ppdisc_is_found = itemFound
    this.data.ppdisc_givento = itemGivenTo

    //set display
    this.description.innerText = description
    this.descriptionField.value = description
    this.location.innerText = location
    this.locationField.value = location
    this.itemFound.innerText = itemFound ? 'Yes' : 'No'
    this.itemFoundField.checked = itemFound
    this.itemGivenTo.innerText = itemGivenTo
    this.itemGivenToField.value = itemGivenTo
  }

  /**
   * Sets this item to edit mode
   * @param {Boolean} editMode True if setting to edit mode, false if disabling
   */
  setEditMode(editMode) {
    this.editMode = editMode

    let infoDisplay = editMode ? 'none' : ''
    let editDisplay = editMode ? '' : 'none'

    this.baseElement.className = editMode ? 'table-input' : ''

    //set info elements to display
    this.description.style.display = infoDisplay
    this.location.style.display = infoDisplay
    this.itemFound.style.display = infoDisplay
    this.itemGivenTo.style.display = infoDisplay
    this.buttons.style.display = infoDisplay

    //set edit elements to display
    this.descriptionEdit.style.display = editDisplay
    this.locationEdit.style.display = editDisplay
    this.itemFoundEdit.style.display = editDisplay
    this.itemGivenToEdit.style.display = editDisplay
    this.saveEdit.style.display = editDisplay
  }

  /**
   * Update this disclosure item in the database
   */
  updateDisclosureItem() {
    let data = {
      ppdisc_id: this.data.ppdisc_id,
      ppdisc_dsca: this.descriptionField.value,
      ppdisc_loc: this.locationField.value,
      ppdisc_is_found: this.itemFoundField.checked,
      ppdisc_givento: this.itemGivenToField.value,
    }

    fetch('./personalPropertyPRG.php', {
      method: 'post',
      headers: { 'Content-Type': 'application/json;charset=utf8' },
      body: JSON.stringify({
        action: 'editDisclosure',
        data: data,
      }),
    })
      .then((res) => res.json())
      .then((body) => {
        if (body.ok) {
          this.setData(data.ppdisc_dsca, data.ppdisc_loc, data.ppdisc_is_found, data.ppdisc_givento)

          this.setEditMode(false)
        }
      })
  }

  /**
   * Delete this disclosure item in the database
   */
  deleteDisclosureItem() {
    fetch('./personalPropertyPRG.php', {
      method: 'post',
      headers: { 'Content-Type': 'application/json;charset=utf8' },
      body: JSON.stringify({
        action: 'deleteDisclosure',
        itemID: this.data.ppdisc_id,
      }),
    })
      .then((res) => res.json())
      .then((body) => {
        if (body.ok) {
          this.baseElement.remove()
          window.jifController.update()

          //remove from parent array
          for (const key in this.parent.disclosureItems) {
            if (this.parent.disclosureItems.hasOwnProperty(key)) {
              const element = this.parent.disclosureItems[key]
              if(element === this)
                this.parent.disclosureItems.splice(key, 1)
            }
          }
        }
      })
  }
}
