import { Util } from '../util.js'

/**
 * Sidebar item that tracks cost
 * @author Jake Cirino
 */
export class SidebarCostCheck {
  /**
   * Creates a new cost check sidebar item
   * @param {JifSidebar} parent
   * @param {String} template
   * @param {JSON} data
   */
  constructor(parent, template, data) {
    this.parent = parent
    this.data = data

    //setup pri estimate data
    this.data.primaryJini = this.calculateCosts(this.data.primaryJini)
    this.data.primaryFinal.jfin_calc_labor = this.data.primaryFinal.jfin_calc_sub_labor

    //setup secondary estimate data
    if (this.data.secondaryEstimate != null) {
      this.data.secondaryJini = this.calculateCosts(this.data.secondaryJini)
      this.data.secondaryFinal.jfin_calc_labor = this.data.secondaryFinal.jfin_calc_sub_labor
    }

    //create base element
    this.baseElement = document.createElement('div')
    this.baseElement.innerHTML = template

    //setup elements
    this.dom = Util.parseDOM(this.baseElement)

    //setup expander button
    this.dom.expanderHeader.addEventListener('click', () => {
      this.setExpanded(!this.expanded)
    })

    //setup primary scope selector option
    let priOpt = document.createElement('option')
    priOpt.value = '1'
    priOpt.innerText = this.data.primaryInvType
    this.dom.scopeSelector.appendChild(priOpt)

    //secondary scope selector option
    if (this.data.secondaryEstimate != null) {
      let secOpt = document.createElement('option')
      secOpt.value = '2'
      secOpt.innerText = this.data.secondaryInvType
      this.dom.scopeSelector.appendChild(secOpt)
    }

    //all scope selector option
    let allOpt = document.createElement('option')
    allOpt.value = '3'
    allOpt.innerText = 'All'
    this.dom.scopeSelector.appendChild(allOpt)

    //setup scope selector
    this.dom.scopeSelector.addEventListener('change', (event) => {
      this.setSelectedScope(event.target.value)
    })

    //append to document
    this.parent.baseElement.appendChild(this.baseElement)

    //set to initially be expanded
    window.addEventListener('load', () => {
      this.setExpanded(true)
      this.setSelectedScope(1)
    })
  }

  /**
   * Updates the DOM with data to reflect the current data of this object
   */
  update() {
    //setup estimate, final and diff data sets
    let estData, finData, difData
    switch (this.selectedScope) {
      case 1:
      case '1':
        estData = this.data.primaryJini
        finData = this.data.primaryFinal
        difData = Util.addPrefixes(Util.subtractArrays(finData, estData, true), 'diff')
        break
      case 2:
      case '2':
        estData = this.data.secondaryJini
        finData = this.data.secondaryFinal
        difData = Util.addPrefixes(Util.subtractArrays(finData, estData, true), 'diff')
        break
      case 3:
      case '3':
        estData = Util.sumArrays(this.data.primaryJini, this.data.secondaryJini)
        finData = Util.sumArrays(this.data.primaryFinal, this.data.secondaryFinal)
        difData = Util.addPrefixes(Util.subtractArrays(finData, estData, true), 'diff')
        break
    }

    let totData = {
      est: 0,
      act: 0,
      diff: 0,
    }

    //populate estimate
    for (const key in estData) {
      if (estData.hasOwnProperty(key)) {
        const value = estData[key]
        const element = this.dom[key]
        if (element != undefined) {
          //remove decimal when displaying to save room
          element.innerText = Util.formatCurrency(value, false)
          totData.est += Number(value)
        }
      }
    }

    //populate actual
    for (const key in finData) {
      if (finData.hasOwnProperty(key)) {
        const value = finData[key]
        const element = this.dom[key]
        if (element != undefined) {
          //remove decimal when displaying to save room
          element.innerText = Util.formatCurrency(value, false)
          totData.act += Number(value)
        }
      }
    }

    //populate diff
    for (const key in difData) {
      if (difData.hasOwnProperty(key)) {
        const value = difData[key]
        const element = this.dom[key]
        if (element != undefined) {
          //remove decimal when displaying to save room
          element.innerText = Util.formatCurrency(value, false, 'square')
          totData.diff += Number(value)
        }
      }
    }

    //calc/update difference section
    let diffs = {
      laborDiff: estData.jcur_calc_labor == 0 ? 1 : finData.jfin_calc_sub_labor / estData.jcur_calc_labor,
      wasteDiff: estData.jcur_calc_sub_waste == 0 ? 1 : finData.jfin_calc_sub_waste / estData.jcur_calc_sub_waste,
      equipDiff: estData.jcur_calc_eqp == 0 ? 1 : finData.jfin_calc_eqp / estData.jcur_calc_eqp,
      supplyDiff: estData.jcur_calc_sup == 0 ? 1 : finData.jfin_calc_sup / estData.jcur_calc_sup,
      otherDiff: estData.jcur_calc_other == 0 ? 1 : finData.jfin_calc_other / estData.jcur_calc_other,
      subtotalDiff: estData.jcur_calc_sub == 0 ? 1 : finData.jfin_calc_sub / estData.jcur_calc_sub,
    }
    for (const key in diffs) {
      if (diffs.hasOwnProperty(key)) {
        const value = (diffs[key] - 1) * 100
        const element = this.dom[key]

        //append value
        if (value < 0) element.innerText = `[${value.toFixed(1).substring(1)}%]`
        else element.innerText = `${value.toFixed(1)}%`

        //change background color. below margin(yellow), within margin (green), above margin (red)
        let threshold = key == 'subtotalDiff' ? 10 : 20
        if (value < -threshold) element.parentElement.style.backgroundColor = '#ff9'
        else if (value > threshold) element.parentElement.style.backgroundColor = '#f99'
        else element.parentElement.style.backgroundColor = '#9d6'
      }
    }
  }

  /**
   * Sets whether this elements info is expanded or not
   * @param {Boolean} expanded
   */
  setExpanded(expanded) {
    this.expanded = expanded

    //set content height
    let children = this.dom['content-collapse'].children
    let height = children[0].getBoundingClientRect().height + children[1].getBoundingClientRect().height
    this.dom['content-collapse'].style.height = expanded ? `${height}px` : '0px'

    //rotate icon
    this.dom.expander.style.transform = expanded ? '' : 'rotate(-90deg)'
  }

  /**
   * Sets the selected scope(s) to display
   * @param {Number} scope Either 1 (pri), 2 (sec) or 3 (all)
   */
  setSelectedScope(scope) {
    this.selectedScope = scope
    this.update()
  }
  
  /**
   * Takes an estimate array and calculates its cost totals, this is to be used on jif_initial only
   * @param {JSON} data The data to calculate costs for
   * @returns {JSON} The added calc fields to the
   */
  calculateCosts(data) {
    //console.log(data)
    let crewSize = this.data.crewSize,
      discountRate = data.jini_disc_per / 100,
      discountOther = data.jini_disc_amt_other,
      taxType = data.jini_tax_type,
      ynTax = data.jini_yn_state_tax,
      taxRate = data.jini_tax_rate

    //calc man/working total number of hours
    let manHours = 0
    for (const key in data) {
      if (data.hasOwnProperty(key)) {
        const value = data[key]
        if (key.includes('_hr_')) manHours += Number(value)
      }
    }
    let hours = manHours / crewSize
  
    let sumHours = Number(data.jcur_hr_haz) + Number(data.jcur_hr_set) + Number(data.jcur_hr_bio) + Number(data.jcur_hr_cnt)
      + Number(data.jcur_hr_srf) + Number(data.jcur_hr_cln) + Number(data.jcur_hr_poe) + Number(data.jcur_hr_fin)
      + Number(data.jcur_hr_ppr)
    //calc sup cost
    let supCost = sumHours
    data.jini_is_iirc == "1" ? supCost *= data.jini_rate_iicrc : supCost *= data.jini_rate_sup
    supCost /= crewSize

    //calc tec cost
    let tecCost = sumHours
    tecCost *= data.jini_rate_tec
    tecCost /= ((crewSize - 1) / crewSize)

    let laborCost = Math.round(supCost + tecCost)

    //calc waste mgmt
    let numBoxes = Number(data.jcur_num_box_suits) + Number(data.jcur_num_box_debris)
    let costPerBox = Number(data.jini_price_transport) + Number(data.jini_price_disposal)
    let wasteCost = Math.round(numBoxes * costPerBox) + Number(data.jini_price_wm_fee)  

    //calc other fields
    let equipDisCost = Math.round((laborCost * data.jini_per_dis) / 100),
      projMgmtCost = Math.round(((laborCost + wasteCost + equipDisCost) * data.jini_per_ovh) / 100),
      emergencyDispCost = Math.round(Number(data.jini_price_dispatch)),
      atpCost = Math.round(Number(data.jini_price_atp_test) * Number(data.jini_yn_atp_testing)),
      asbTestCost = data.jcur_num_asb_vial > 0 ? Math.round(Number(data.jini_price_asb_test)) : 0,
      asbVialCost = Math.round(data.jini_price_asb_vial * data.jcur_num_asb_vial),
      demobilCost = Math.round(Number(data.jini_price_demob)),
      truckDisCost = Number(data.jini_price_disinfect),
      phodocCost = Number(data.jini_price_phodoc),
      digrepCost = Number(data.jini_price_digrep),
      dumpsterCost = Math.round(
        data.jcur_num_dumpster * data.jini_price_dumpster + data.jcur_num_bagster * data.jini_price_bagster
      ),
      usageSubtotal =
        laborCost +
        wasteCost +
        equipDisCost +
        projMgmtCost +
        emergencyDispCost +
        atpCost +
        asbTestCost +
        asbVialCost +
        demobilCost +
        truckDisCost +
        phodocCost +
        digrepCost +
        dumpsterCost,
      equipUsageCost = Math.round((usageSubtotal + 0.001) * (data.jini_per_eqp / 100)),
      supplyUsageCost = Math.round((usageSubtotal + 0.001) * (data.jini_per_sup / 100)),
      otherCost = usageSubtotal - laborCost - wasteCost - equipDisCost,
      extraCost = Number(data.jcur_amt_other) + Number(data.jcur_amt_3rd_party),
      subtotal = Math.round(
        laborCost + wasteCost + equipUsageCost + equipDisCost + supplyUsageCost + otherCost
      ),
      discount = data.jcur_calc_disc

    //calc taxes
    let taxes = 0
    if (taxType == 1) {
      taxes = Math.round((ynTax * supplyUsageCost * taxRate) / 100)
    } else if (taxType == 2) {
      taxes = Math.round((ynTax * subtotal * taxRate) / 100)
    }
    //calc total
    let total = Math.round(subtotal - discount + taxes + extraCost)

    //add calc fields to data and return the modified JSON object
    //console.log(data)
    let newData = JSON.parse(JSON.stringify(data))
    newData.jcur_calc_sup_labor = supCost
    newData.jcur_calc_tec_labor = tecCost
    newData.jcur_calc_sub_waste = Number(data.jcur_calc_waste) + Number(data.jcur_calc_dumpster)
    newData.jcur_calc_waste = Number(data.jcur_calc_waste) + Number(data.jcur_calc_dumpster)
    newData.jcur_calc_eqp = Number(data.jcur_calc_eqp) + Number(data.jcur_calc_dis)
    //newData.jcur_calc_sup = supplyUsageCost
    //newData.jcur_calc_other = otherCost
    //newData.jcur_calc_sub_full =
    //  supCost + tecCost + wasteCost + newData.jcur_calc_eqp + supplyUsageCost + otherCost
    newData.jcur_calc_disc = discount
    newData.jcur_calc_sub = newData.jcur_calc_sub_full - discount
    newData.jcur_calc_3rd_other = extraCost
    //newData.jcur_calc_tax = taxes
    //newData.jcur_calc_tot = total

    return newData
  }
}
