import { Util } from '../util.js'
import { YNModal } from '../ynModal.js'

/**
 * Represents an item on the timesheet list
 * @author Jake Cirino
 */
export class TimesheetItem {
  /**
   * Creates a new timesheet item
   * @param {Timesheet} parent
   * @param {JSON} activities
   * @param {JSON} employees
   * @param {JSON} scopes
   * @param {JSON} data
   */
  constructor(parent, activities, employees, scopes, data) {
    this.parent = parent
    this.activities = activities
    this.employees = employees
    this.scopes = scopes
    this.data = data

    //create base element
    this.baseElement = document.createElement('tr')
  }

  populate() {
    this.baseElement.innerHTML = window.itemTemplate

    //set field values
    this.dom = Util.parseDOM(this.baseElement)

    //populate activity items
    this.activities.forEach((activity) => {
      let option = document.createElement('option')
      option.value = activity.act_id
      option.innerText = activity.act_dsca

      this.dom.activityField.appendChild(option)
    })

    //add this item's emp to list even if no longer in crew
    let option = document.createElement('option')
    option.value = this.data.ts_emp
    option.innerText = `${this.data.emp_fname} ${this.data.emp_lname}`
    this.dom.empField.appendChild(option)

    //now populate remainig crew
    this.employees.forEach((employee) => {
      if (employee.emp_id != this.data.ts_emp ) {
        let option = document.createElement('option')
        option.value = employee.emp_id
        option.innerText = `${employee.emp_fname} ${employee.emp_lname}`
        this.dom.empField.appendChild(option)
      }
    })

    //populate scope items
    let priOption = document.createElement('option')
    priOption.value = 1
    priOption.innerText = this.parent.data.primaryInv
    this.dom.scopeField.appendChild(priOption)

    if(this.parent.data.secondaryInv != null){
      let secOption = document.createElement('option')
      secOption.value = 2
      secOption.innerText = this.parent.data.secondaryInv
      this.dom.scopeField.appendChild(secOption)
    }

    //edit functionality
    this.dom.editButton.addEventListener('click', () => {
      this.setEditMode(true)
    })

    //delete button functionality
    this.dom.deleteButton.addEventListener('click', () => {
      fetch('timesheetPRG.php', {
        method: 'post',
        headers: { 'Content-Type': 'application/json;charset=utf8' },
        body: JSON.stringify({
          action: 'deleteTimesheetItem',
          ts_id: this.data.ts_id,
        }),
      })
        .then((res) => res.json())
        .then((body) => {
          if (body.ok) {
            this.baseElement.remove()

            //remove from parent array
            const index = this.parent.items.indexOf(this)
            if (index > -1) this.parent.items.splice(index, 1)

            window.jifController.update()
            this.parent.highlightOverlaps()
          }
        })
    })

    //update S/T selector on employee change
    this.dom.empField.addEventListener('change', this.updateSelectableRoles.bind(this))

    //recalc duration on time changed
    this.dom.dateStartField.addEventListener('change', this.recalculateDuration.bind(this))
    this.dom.timeStartField.addEventListener('change', this.recalculateDuration.bind(this))
    this.dom.dateEndField.addEventListener('change', this.recalculateDuration.bind(this))
    this.dom.timeEndField.addEventListener('change', this.recalculateDuration.bind(this))

    this.dom.saveButton.addEventListener('click', () => {
      //update data values
      this.data.ts_activity = this.dom.activityField.value
      this.data.ts_crew_type = this.dom.roleField.value
      this.data.ts_emp = this.dom.empField.value
      this.data.ts_seq = this.dom.scopeField.value
      this.data.ts_is_billable = this.dom.billableField.checked
      this.data.ts_respirator_worn = this.dom.respiratorField.checked
      let startDatetime = Util.parseDate(this.dom.dateStartField.value, this.dom.timeStartField.value)
      let endDatetime = Util.parseDate(this.dom.dateEndField.value, this.dom.timeEndField.value)
      this.data.ts_dt_fr = Util.dateToString(startDatetime)
      this.data.ts_dt_to = Util.dateToString(endDatetime)
      this.data.ts_duration = this.dom.durationField.value.split(' ')[0]

      //adjust endDatetime to next day if needed
      if (endDatetime.getTime() - startDatetime.getTime() < 0)
        endDatetime.setTime(endDatetime.getTime() + 1000 * 60 * 60 * 24)

      //TODO verify fields

      //post edit to server
      fetch('timesheetPRG.php', {
        method: 'post',
        headers: { 'Content-Type': 'application/json;charset=utf8' },
        body: JSON.stringify({
          action: 'editTimesheetItem',
          ts_id: this.data.ts_id,
          ts_activity: this.data.ts_activity,
          ts_crew_type: this.data.ts_crew_type,
          ts_emp: this.data.ts_emp,
          ts_seq: this.data.ts_seq,
          ts_is_billable: this.data.ts_is_billable,
          ts_respirator_worn: this.data.ts_respirator_worn,
          ts_dt_fr: this.data.ts_dt_fr,
          ts_dt_to: this.data.ts_dt_to,
          ts_duration: this.data.ts_duration
        }),
      })
        .then((res) => res.json())
        .then((body) => {
          if (body.ok) {
            //reset UI if edit successful
            this.updateData()
            this.setEditMode(false)
            this.parent.highlightOverlaps()
            window.jifController.update()
          }
        })
    })

    //initial setup
    this.updateData(true)
    this.setEditMode(false)

    //append to document
    this.parent.dom['data-body']
      .insertBefore(this.baseElement, this.parent.baseElement.querySelector('.table-input'))

    //setup datepickers
    let dateStartField = $(this.dom.dateStartField)
    dateStartField.datepicker({
      onSelect: this.recalculateDuration.bind(this)
    })
    
    let dateEndField = $(this.dom.dateEndField)
    dateEndField.datepicker({
      onSelect: this.recalculateDuration.bind(this)
    })
  }

  /**
   * Recalculates and populates duration
   */
  recalculateDuration() {
    try {
      let startDate = this.dom.dateStartField.value,
        startTime = this.dom.timeStartField.value,
        endDate = this.dom.dateEndField.value,
        endTime = this.dom.timeEndField.value,
        startDatetime = Util.parseDate(startDate, startTime),
        endDatetime = Util.parseDate(endDate, endTime)

      //adjust endDatetime to next day if needed
      //if (endDatetime.getTime() - startDatetime.getTime() < 0)
      //  endDatetime.setTime(endDatetime.getTime() + 1000 * 60 * 60 * 24)

      let duration = (endDatetime.getTime() - startDatetime.getTime()) / 1000 / 60 / 60
      this.data.ts_duration = duration

      //hightlight based off duration
      if(duration >= 12){
        this.baseElement.style.backgroundColor = '#f99'
      }else if(duration >= 8){
        this.baseElement.style.backgroundColor = '#ff9'
      }

      duration = duration.toFixed(2)
      duration = `${duration} Hr`
      this.dom.durationField.value = duration
      this.dom.duration.innerText = duration
    } catch (err) {
      this.dom.durationField.value = '0.00 Hr'
      this.dom.duration.innerText = '0.00 Hr'
      return false
    }
  }

  /**
   * Updates the data being displayed in fields
   * @param {Boolean} updateEdit Set to true if you also want the edit fields to be repopulated
   */
  updateData(updateEdit = false) {
    //split datetime values
    let dateTimeStart = this.data.ts_dt_fr.split(' '),
      dateTimeEnd = this.data.ts_dt_to.split(' ')

    //Add a line for a new day 
    if (!this.parent.eachDay[`${dateTimeStart[0]}`]) {
      this.parent.eachDay[`${dateTimeStart[0]}`] = true
      this.baseElement.classList.add('rowline')
    }

    //Non-billable background subdued 
    if (this.data.ts_is_billable != 1)
      this.baseElement.classList.add('hlgry')

    //set display values
    let role = this.data.ts_crew_type == 1 ? 'S' : 'T'
    this.dom.ts_type.innerText = role
    //this.dom.roleEdit.innerText = role 

    this.dom.ts_activity.innerText = this.data.act_dsca
    this.dom.ts_emp.innerText = `${this.data.emp_fname} ${this.data.emp_lname}`
    this.dom.ts_scope.innerText = this.data.ts_seq == 1 ? this.parent.data.primaryInv : this.parent.data.secondaryInv
    this.dom.ts_is_billable.innerText = this.data.ts_is_billable == 1 ? 'Y' : 'N'
    this.dom.ts_respirator_worn.innerText = this.data.ts_respirator_worn == 1 ? 'Y' : 'N'
    this.dom.dateStart.innerText = dateTimeStart[0]
    this.dom.timeStart.innerText = dateTimeStart[1]
    this.dom.dateEnd.innerText = dateTimeEnd[0]
    this.dom.timeEnd.innerText = dateTimeEnd[1]

    //set input field values
    if (updateEdit) {
      this.dom.activityField.value = this.data.ts_activity
      this.dom.roleField.value = this.data.ts_crew_type
      this.dom.empField.value = this.data.ts_emp
      this.dom.scopeField.value = this.data.ts_seq
      this.dom.billableField.checked = this.data.ts_is_billable == 1
      this.dom.respiratorField.checked = this.data.ts_respirator_worn == 1
      this.dom.dateStartField.value = dateTimeStart[0]
      this.dom.timeStartField.value = dateTimeStart[1]
      this.dom.dateEndField.value = dateTimeEnd[0]
      this.dom.timeEndField.value = dateTimeEnd[1]
      this.updateSelectableRoles()
    }

    this.recalculateDuration()
    this.parent.calculateInfoTotals()
  }

  /**
   * Sets this item to edit mode
   * @param {Boolean} editMode True if setting to edit mode, false if disabling
   */
  setEditMode(editMode) {
    this.editMode = editMode

    let infoDisplay = editMode ? 'none' : '',
      editDisplay = editMode ? '' : 'none'

    editMode ? this.baseElement.classList.add('table-input') : this.baseElement.classList.remove('table-input')

    //set info elements to display
    this.dom.ts_type.style.display = infoDisplay
    this.dom.ts_activity.style.display = infoDisplay
    this.dom.ts_emp.style.display = infoDisplay
    this.dom.ts_scope.style.display = infoDisplay
    this.dom.ts_is_billable.style.display = infoDisplay
    this.dom.ts_respirator_worn.style.display = infoDisplay
    this.dom.dateStart.style.display = infoDisplay
    this.dom.dateEnd.style.display = infoDisplay
    this.dom.timeStart.style.display = infoDisplay
    this.dom.timeEnd.style.display = infoDisplay
    this.dom.duration.style.display = infoDisplay
    this.dom.iconButtons.style.display = infoDisplay

    //set edit elements to display
    this.dom.roleEdit.style.display = editDisplay
    this.dom.activityEdit.style.display = editDisplay
    this.dom.empEdit.style.display = editDisplay
    this.dom.scopeEdit.style.display = editDisplay
    this.dom.billableEdit.style.display = editDisplay
    this.dom.respiratorEdit.style.display = editDisplay
    this.dom.dateStartEdit.style.display = editDisplay
    this.dom.dateEndEdit.style.display = editDisplay
    this.dom.timeStartEdit.style.display = editDisplay
    this.dom.timeEndEdit.style.display = editDisplay
    this.dom.durationEdit.style.display = editDisplay
    this.dom.saveEdit.style.display = editDisplay
  }

  /**
   * @returns {JSON} The start/end timestamps in this format:
   * {
   *  start: number
   *  end: number
   * }
   */
  getTimestamps() {
    let startDatetime = Util.parseDate(this.dom.dateStartField.value, this.dom.timeStartField.value),
      endDatetime = Util.parseDate(this.dom.dateStartField.value, this.dom.timeEndField.value)

    //adjust endDatetime to next day if needed
    if (endDatetime.getTime() - startDatetime.getTime() < 0)
      endDatetime.setTime(endDatetime.getTime() + 1000 * 60 * 60 * 24)

    return {
      start: startDatetime.getTime(),
      end: endDatetime.getTime()
    }
  }

  /**
   * Compares this item and another, determining if there is a time overlap
   * @param {TimesheetItem} other
   * @returns {Boolean} True if the emp/times overlap, false if not
   */
  compareTimes(other) {
    //check if employees are the same
    if(this.data.ts_emp != other.data.ts_emp)
      return false
    
    //check if any timestamps collide
    let thisTS = this.getTimestamps(),
      otherTS = other.getTimestamps()
    return (thisTS.start >= otherTS.start && thisTS.start < otherTS.end)
      || (thisTS.end > otherTS.start && thisTS.end <= otherTS.end)
      || (thisTS.start <= otherTS.start && thisTS.end >= otherTS.end)
  }

  /**
   * Updates the selectable roles for this job
   */
  updateSelectableRoles(){
    let empID = this.dom.empField.value

    let empIsSup = this.isEmpSup(empID)
    this.dom.roleField.children[0].disabled = !empIsSup
    if(!empIsSup) this.dom.roleField.value = 2
  }

  /**
   * Checks if an employee is a sup
   * @param {Number} empID 
   */
  isEmpSup(empID){
    for (const key in this.employees) {
      if (Object.hasOwnProperty.call(this.employees, key)) {
        const empData = this.employees[key];
        
        if(empData.crew_emp == empID) return empData.is_sup == 1
      }
    }
  }
}
