import { Util } from '../util.js'
import { CostMarginCalc } from './costMarginCalc.js'
import { default as note } from '../modules/lib/note.js'

/**
 * A class that controls the final estimate tab
 * @author Jake Cirino
 */
export class FinalEstimate {
  /**
   * Creates a new final estimate item
   * @param {JSON} data
   */
  constructor(data) {
    this.data = data
    this.hasSecondary = this.data.secondaryInvType != null
    //console.log(data)

    //create a permision object
    this.perm      = {}
    this.perm.mnge = 'jif_m'  // mnge: Manage JIFs
    this.perm.verf = 'job_cf' // verf: Override job verification

    //set initial global state data
    window.aftermath.jifSaveState = true

    //let startLoad = Date.now()

    //create base element and parse the dom
    this.baseElement = document.querySelector('[name=finalEstimate]')

    //get input/info sections
    this.inputSection = this.baseElement.querySelector('[name=inpSection]')
    this.saveSection = this.baseElement.querySelector('[name=saveSection]')
    this.infoSection = this.baseElement.querySelector('[name=infoSection]')
    this.mgnSection = this.baseElement.querySelector('[name=marginSection]')
    this.inpDom = Util.parseDOM(this.inputSection)
    this.saveDom = Util.parseDOM(this.saveSection)
    this.infoDom = Util.parseDOM(this.infoSection)
    this.mgnDom = Util.parseDOM(this.mgnSection)

    //setup pri/sec filters and run initial calcs
    this.inpDom.filterPri = this.baseElement.querySelector('[name=filterPri]')
    this.inpDom.filterSec = this.baseElement.querySelector('[name=filterSec]')

    this.inpDom.filterPri.value = this.data.primaryInvType
    this.data.primaryIni = this.iniCalculate(this.data.primaryIni)

    if (this.hasSecondary) {
      this.inpDom.filterSec.value = this.data.secondaryInvType
      this.data.secondaryIni = this.iniCalculate(this.data.secondaryIni)
    } else {
      this.inpDom.filterSec.style.display = 'none'
    }

    //setup filter buttons
    this.inpDom.filterPri.addEventListener('click', () => {
      if (this.selectedScope != 1) this.setSelectedScope(1)
      this.viewMarginCalc()
    })

    this.inpDom.filterSec.addEventListener('click', () => {
      if (this.selectedScope != 2) this.setSelectedScope(2)
      this.viewMarginCalc()
    })

    //populate approver options
    for (const key in this.data.approvers) {
      if (this.data.approvers.hasOwnProperty(key)) {
        const element = this.data.approvers[key]
        let option = document.createElement('option')
        option.value = element.emp_id
        option.innerText = element.emp_name

        this.inpDom.jfin_disc_addl_emp_appr.appendChild(option)
      }
    }

    //enable save button on input change
    for (const key in this.inpDom) {
      if (this.inpDom.hasOwnProperty(key)) {
        const element = this.inpDom[key]

        if (key.includes('jfin_')) {
          element.addEventListener('change', (event) => {

            //enable save button
            this.saveDom.saveButton.disabled = false

            //disable confirm button
            this.saveDom.confirm.disabled = true

            //set global state data
            window.aftermath.jifSaveState = false

            //update value in data
            let currentScope = this.selectedScope == 1 ? this.data.primaryActual : this.data.secondaryActual
            if (event.target.value == '' && key.includes('tax')) currentScope[key] = null
            else if (key.includes('_amt_')) currentScope[key] = event.target.value * 100
            else currentScope[key] = event.target.value

            //calc/update
            this.calculate()
          })
        }
      }
    }

    //setup save button
    this.saveDom.saveButton.addEventListener('click', () => {
      let currentScope = this.selectedScope == 1 ? this.data.primaryActual : this.data.secondaryActual
      let updateData = JSON.parse(JSON.stringify(currentScope))
      updateData.action = 'updateFinal'
      fetch('finalEstimatePRG.php', {
        method: 'post',
        headers: { 'Content-Type': 'application/json;charset=utf8' },
        body: JSON.stringify(updateData),
      })
        .then((res) => res.json())
        .then((body) => {
          if (body.ok) {

            //disable save button
            this.saveDom.saveButton.disabled = true

            //enable confirm button
            this.saveDom.confirm.disabled = false

            //set global state data
            window.aftermath.jifSaveState = true

            window.jifController.update()
          }
        })
    })

    //setup confirm inputs
    this.saveDom.confirm.addEventListener('click', () => {
      this.saveDom.confirmInitial.style.display = 'block'

      if (this.saveDom.overrideReason && this.verify.override) {
        this.saveDom.overrideReason.closest('label').style.display = 'block'
        if (window.aftermath.hasPerm(this.perm.verf)) {
          this.saveDom.overrideReason.disabled = false
        }
      }

    })
    this.saveDom.confirmYes.addEventListener('click', () => {

      const overrideData = {}
      if (this.saveDom.overrideReason) {
        if (this.verify.override && this.saveDom.overrideReason.value.length === 0) {
          note({text: 'The verification override reason is required.', class: 'alert'})
          return
        } else {
          overrideData['jvo_job']    = this.data.jobID
          overrideData['jvo_phase']  = this.saveDom.overrideReason.dataset.phase
          overrideData['jvo_reason'] = this.saveDom.overrideReason.value
        }
      }

      fetch('finalEstimatePRG.php', {
        method: 'post',
        headers: { 'Content-Type': 'application/json;charset=utf8' },
        body: JSON.stringify({
          action: 'confirmFinal',
          jobID: this.data.jobID,
          overrideData: overrideData
        }),
      })
        .then((res) => res.json())
        .then((body) => {
          if (body.ok) {

            if (body.override && body.override === false) {
              note({text: 'There was an error in saving the verification override data.'})
            }

            //set lock data
            this.data.lockInfo = body.msg

            //show/hide buttons
            this.saveDom.confirmInitial.style.display = 'none'
            this.saveDom.overrideReason.closest('label').style.display = 'none'
            this.saveDom.overrideReason.disabled = true
            this.saveDom.overrideReason.value = ''

            //display confirm history
            this.setLocked(true)

            //update jifcontroller
            window.jifController.update()
          }
        })
    })
    this.saveDom.confirmNo.addEventListener('click', () => {
      this.saveDom.confirmInitial.style.display = 'none'
      this.saveDom.overrideReason.closest('label').style.display = 'none'
      this.saveDom.overrideReason.disabled = true
      this.saveDom.overrideReason.value = ''
    })

    //setup unlock inputs
    this.saveDom.unlock.addEventListener('click', () => {
      this.saveDom.confirmUnlock.style.display = ''
      this.saveDom.confirmHistory.style.display = 'none'
    })
    this.saveDom.unlockYes.addEventListener('click', () => {
      fetch('finalEstimatePRG.php', {
        method: 'post',
        headers: { 'Content-Type': 'application/json;charset=utf8' },
        body: JSON.stringify({
          action: 'unconfirmFinal',
          jobID: this.data.jobID,
        }),
      })
        .then((res) => res.json())
        .then((body) => {
          if (body.ok) {
            //set confirm text to not visible
            this.saveDom.confirmUnlock.style.display = 'none'

            //change locked status
            this.setLocked(false)

            //update jifcontroller
            window.jifController.update()

            //run to adjust jobVerify DOM message/button
            this.jobVerifyConnect()
          }
        })
    })
    this.saveDom.unlockNo.addEventListener('click', () => {
      this.saveDom.confirmUnlock.style.display = 'none'
      this.saveDom.confirmHistory.style.display = 'block'
    })

    this.saveDom.unlockNo.addEventListener('click', () => {
      this.saveDom.confirmUnlock.style.display = 'none'
      this.saveDom.confirmHistory.style.display = 'block'
    })

    /*setup additional charges input
    this.inpDom.addlInput.addEventListener('change', (event) => {
      //enable save button
      this.saveDom.saveButton.disabled = false

      //update data
      let actual = this.selectedScope == 1 ? this.data.primaryActual : this.data.secondaryActual
      actual.jfin_amt_addl_final = event.target.value.length == 0 ? null : event.target.value * 100
      //recalculate
      this.calculate()
    })

    //setup lock button functionality
    this.inpDom.addlLock.addEventListener('mouseover', () => {
      this.inpDom.addlLock.src = this.isAdditionalLocked() ? 'icons/unlocked16.png' : 'icons/locked16.png'
    })
    this.inpDom.addlLock.addEventListener('click', () => {
      this.setAdditionalLocked(!this.isAdditionalLocked())

      //enable save button
      this.saveDom.saveButton.disabled = false
    })
    this.inpDom.addlLock.addEventListener('mouseout', () => {
      this.inpDom.addlLock.src = this.isAdditionalLocked() ? 'icons/locked16.png' : 'icons/unlocked16.png'
    })*/

    //setup cost breakdown expanders
    let expanderElements = this.infoSection.querySelectorAll('[name=expander]')
    for (const key in expanderElements) {
      if (Object.hasOwnProperty.call(expanderElements, key)) {
        const element = expanderElements[key];
        
        element.addEventListener('click', event => {
          this.toggleEstimateExpanded(event.target)
        })
      }
    }
    /*
    this.infoDom.wasteExpander.addEventListener('click', this.toggleEstimateExpanded('waste'))
    this.infoDom.equipExpander.addEventListener('click', this.toggleEstimateExpanded('equip'))
    this.infoDom.supplyExpander.addEventListener('click', this.toggleEstimateExpanded('supply'))
    this.infoDom.otherExpander.addEventListener('click', this.toggleEstimateExpanded('other'))
    */

    //populate items initially
    this.setSelectedScope(1)
    this.populate()
    this.calculate()
    this.setLocked(this.data.lockInfo.jif_fin_is_confirmed == 1)

    //console.log(`loaded content in ${Date.now() - startLoad}`)

    // Due to async loading of JS modules, only run
    // jobVerifyConnect() after window property exists
    this.verify = {} //create an object for any verify needs
    if (window.aftermath.hasOwnProperty('jifVerifyCount')) {
      this.jobVerifyConnect()
    } else {
      document.addEventListener('JobVerifyChangeEvent', () => this.jobVerifyConnect())
    }
  }

  /**
   * jobVerify concerns
   */
  jobVerifyConnect() {
    if (Number(window.aftermath.jifVerifyCount.remaining_4) > 0)
      this.verify.override = true
    else
      this.verify.override = false
  }

  /**
   * Sets the selected scope
   * @param {Number} scope 1 for pri, 2 for sec
   */
  setSelectedScope(scope) {
    this.selectedScope = scope

    //set button backgrounds
    this.inpDom.filterPri.style.backgroundColor = scope == 1 ? '#ffff99' : ''
    this.inpDom.filterSec.style.backgroundColor = scope == 2 ? '#ffff99' : ''

    //repopulate data
    this.populate()
  }

  /**
   * Toggles extra info on final estimate pricing
   * @param {HTMLElement} target The expander button click event target
   */
  toggleEstimateExpanded(target){
    let category = target.getAttribute('category'),
      labels = this.infoDom[`${category}Labels`],
      initials = this.infoDom[`${category}Initials`],
      finals = this.infoDom[`${category}Finals`]
    let expand = target.src.includes('_rt_') //true if we are going to expand this section

    target.src = expand ? 'icons/arrow_dn_6x9.gif' : 'icons/arrow_rt_6x9.gif'
    target.title = expand ? 'Collapse' : 'Expand'
    target.alt = expand ? 'Collapse' : 'Expand'

    labels.style.display = expand ? '' : 'none'
    initials.style.display = expand ? '' : 'none'
    finals.style.display = expand ? '' : 'none'
  }

  /**
   * Builds and views a margin calc with permision
   */
  viewMarginCalc() {

    //Dont't allow margin without proper permission
    if (!window.aftermath.hasPerm(this.perm.mnge)) return

    const e = this.selectedScope == 1 ? this.data.primaryIni : this.data.secondaryIni,
      d = this.selectedScope == 1 ? this.data.primaryActual : this.data.secondaryActual,
      job = {
        crew: Number(this.data.crewInfo.jif_num_crew),
        days: Number(this.data.crewInfo.jif_num_days),
        transit: Number(this.data.crewInfo.jif_calc_hr_transit),
        trucks: Number(d.jfin_num_truck),
        rentals: Number(d.jfin_num_rental),
        hotels: Number(d.jfin_num_hotel),
        dumpsters: Number(d.jfin_num_dumpster),
        bagsters: Number(d.jfin_num_bagster),
        bioboxes: Number(d.jfin_num_box_debris) + Number(d.jfin_num_box_suits),
        equipment: Number(d.jfin_calc_eqp),
        supplies: Number(d.jfin_calc_sup),
        manHours: Number(d.jfin_sum_hr_sup) + Number(d.jfin_sum_hr_tec),
        quotePrice: Number(d.jfin_calc_tot),
        jobPmtInsNon: Number(this.data.jobPmtInsNon),
        jifConfirmed: Number(e.jif_ini_is_confirmed),
        jifPerRealEst: e.jif_per_real_est ? Number(e.jif_per_real_est) : null,
        internalExp: Number(d.jfin_amt_int_exp)
      },
      cm = new CostMarginCalc({ costlist: this.data.costlist, realzns: this.data.realzns, job: job })

    cm.calcMargins()

    //this.mgnDom.quotePrice.innerText      = Util.formatCurrency(cm.job.quotePrice)
    this.mgnDom.laborExp.innerText = Util.formatCurrency(cm.expense.laborExp)
    this.mgnDom.nonbillHoursExp.innerText = Util.formatCurrency(cm.expense.nonbillHoursExp)
    this.mgnDom.payrollExp.innerText = Util.formatCurrency(cm.expense.payrollExp)
    this.mgnDom.driveExp.innerText = Util.formatCurrency(cm.expense.driveExp)
    this.mgnDom.fuelExp.innerText = Util.formatCurrency(cm.expense.fuelExp)
    this.mgnDom.vehicleExp.innerText = Util.formatCurrency(cm.expense.vehicleExp)
    this.mgnDom.rentalExp.innerText = Util.formatCurrency(cm.expense.rentalExp)
    this.mgnDom.hotelExp.innerText = Util.formatCurrency(cm.expense.hotelExp)
    this.mgnDom.branchOvhExp.innerText = Util.formatCurrency(cm.expense.branchOvhExp)
    this.mgnDom.bioboxExp.innerText = Util.formatCurrency(cm.expense.bioboxExp)
    this.mgnDom.dumpsterExp.innerText = Util.formatCurrency(cm.expense.dumpsterExp)
    this.mgnDom.bagsterExp.innerText = Util.formatCurrency(cm.expense.bagsterExp)
    this.mgnDom.supplyEquipExp.innerText = Util.formatCurrency(cm.expense.supplyEquipExp)
    this.mgnDom.internalExp.innerText = Util.formatCurrency(cm.expense.internalExp)
    this.mgnDom.estCOGS.innerText = Util.formatCurrency(cm.job.estCOGS)
    this.mgnDom.anticipColl.innerText = Util.formatCurrency(cm.job.anticipColl)
    this.mgnDom.grossProfit.innerText = Util.formatCurrency(cm.job.grossProfit)
    this.mgnDom.costMargin.innerText = `${cm.job.costMargin}%`
    this.mgnDom.costMargin.classList = cm.signal.color
    this.mgnDom.costMarginLabel.classList = cm.signal.color
    this.mgnSection.style.display = 'block'
  }

  /**
   * Sets whether the form fields are locked or not
   * @param {Boolean} locked
   */
  setLocked(locked) {
    this.locked = locked

    //set lock overlay display vars
    let lockedDisplay = locked ? 'block' : 'none',
      unlockedDisplay = locked ? 'none' : 'block',
      lockedInlineDisplay = locked ? 'inline-block' : 'none'

    //set history text if the data is available
    if (locked) {
      let lockDate = this.data.lockInfo.jif_fin_dt_confirmed,
        empName = `${this.data.lockInfo.emp_fname} ${this.data.lockInfo.emp_lname}`

      this.saveDom.confirmHistory.innerText = `This estimate was confirmed on ${lockDate} by ${empName}`
    }

    //set display for locked items
    //this.inpDom.lock.style.display = lockedDisplay
    //this.infoDom.lock.style.display = lockedDisplay
    this.saveDom.confirmHistory.style.display = locked ? '' : 'none'
    this.saveDom.confirmHistory.style.opacity = locked ? 1 : 0
    this.saveDom.unlock.style.display = locked ? 'inline-block' : 'none'
    this.saveDom.unlock.disabled = !locked

    //set display for unlocked items
    this.saveDom.confirm.style.display = locked ? 'none' : 'inline-block'
  }

  /**
   * Populates data fields based off this items data
   */
  populate() {
    //select data based off selected scope
    let estimate = this.selectedScope == 1 ? this.data.primaryIni : this.data.secondaryIni
    let actual = this.selectedScope == 1 ? this.data.primaryActual : this.data.secondaryActual

    //populate primary input fields
    for (const key in actual) {
      if (actual.hasOwnProperty(key)) {
        const value = actual[key]
        const element = this.inpDom[key]

        //TODO current formatting
        if (element != undefined) {
          if (key.includes('_amt_') || key.includes('calc_disc')) element.value = (value / 100).toFixed(2)
          else element.value = value
        }
      }
    }

    /*populate additional charges input
    this.inpDom.addlInput.value = (actual.jfin_calc_addl_other / 100).toFixed(2)
    this.setAdditionalLocked(actual.jfin_amt_addl_final == null, true)
    */
    
    //populate initial output fields
    for (const key in estimate) {
      if (estimate.hasOwnProperty(key)) {
        const value = estimate[key]
        const element = this.infoDom[key]

        if (element != undefined) element.innerText = Util.formatCurrency(value)
      }
    }

    //populate primary output fields
    for (const key in actual) {
      if (actual.hasOwnProperty(key)) {
        const value = actual[key]
        const element = this.infoDom[key]

        if (element != undefined) element.innerText = Util.formatCurrency(value)
      }
    }
  }

  /**
   * Takes an estimate array and calculates its cost totals, this is to be used on jif_initial only
   * @param {JSON} data The data to calculate costs for
   * @returns {JSON} The added calc fields to the
   */
  iniCalculate(data) {
    let crewSize = this.data.crewSize,
      discountRate = data.jini_disc_per / 100,
      discountOther = data.jini_disc_amt_other,
      taxType = data.jini_tax_type,
      ynTax = data.jini_yn_state_tax,
      taxRate = data.jini_tax_rate

    //calc man/working total number of hours
    let manHours = 0
    for (const key in data) {
      if (data.hasOwnProperty(key)) {
        const value = data[key]
        if (key.includes('_hr_')) manHours += Number(value)
      }
    }
    let hours = manHours / crewSize

    let sumHours = data.jcur_hr_haz + data.jcur_hr_set + data.jcur_hr_bio + data.jcur_hr_cnt + data.jcur_hr_srf
      + data.jcur_hr_cln + data.jcur_hr_poe + data.jcur_hr_fin + data.jcur_hr_ppr
    
    //calc sup cost
    let supCost = sumHours
    data.jini_is_iicrc == "1" ? supCost *= data.jini_rate_iicrc : supCost *= data.jini_rate_sup
    supCost /= crewSize
    
    //calc tec cost
    let tecCost = sumHours * data.jini_rate_tec * ((crewSize - 1) / crewSize)

    let laborCost = supCost + tecCost

    //calc waste mgmt
    let numBoxes = Number(data.jcur_num_box_suits) + Number(data.jcur_num_box_debris)
    let costPerBox = Number(data.jini_price_transport) + Number(data.jini_price_disposal)
    let wasteCost = Math.round(numBoxes * costPerBox) + Number(data.jini_price_wm_fee)

    //calc other fields
    let equipDisCost = Math.round((laborCost * data.jini_per_dis) / 100),
      projMgmtCost = Math.round(((laborCost + wasteCost + equipDisCost) * data.jini_per_ovh) / 100),
      emergencyDispCost = Math.round(Number(data.jini_price_dispatch)),
      atpCost = Math.round(Number(data.jini_price_atp_test) * Number(data.jini_yn_atp_testing)),
      asbTestCost = data.jcur_num_asb_vial > 0 ? Math.round(Number(data.jini_price_asb_test)) : 0,
      asbVialCost = Math.round(data.jini_price_asb_vial * data.jcur_num_asb_vial),
      demobilCost = Math.round(Number(data.jini_price_demob)),
      truckDisCost = Number(data.jini_price_disinfect),
      phodocCost = Number(data.jini_price_phodoc),
      digrepCost = Number(data.jini_price_digrep),
      dumpsterCost = Math.round(
        data.jcur_num_dumpster * data.jini_price_dumpster + data.jcur_num_bagster * data.jini_price_bagster
      ),
      usageSubtotal =
        supCost +
        tecCost +
        wasteCost +
        equipDisCost +
        projMgmtCost +
        emergencyDispCost +
        atpCost +
        asbTestCost +
        asbVialCost +
        demobilCost +
        truckDisCost +
        phodocCost +
        digrepCost +
        dumpsterCost,
      equipUsageCost = Math.round((usageSubtotal + 0.001) * (data.jini_per_eqp / 100)),
      supplyUsageCost = Math.round((usageSubtotal + 0.001) * (data.jini_per_sup / 100)),
      otherCost = usageSubtotal - supCost - tecCost - wasteCost - equipDisCost,
      extraCost = Number(data.jcur_amt_other) + Number(data.jcur_amt_3rd_party),
      subtotal = Math.round(
        supCost + tecCost + wasteCost + equipUsageCost + equipDisCost + supplyUsageCost + otherCost
      ),
      discount = data.jcur_calc_disc

    //calc taxes
    let taxes = 0
    if (taxType == 1) {
      taxes = Math.round((ynTax * supplyUsageCost * taxRate) / 100)
    } else if (taxType == 2) {
      taxes = Math.round((ynTax * subtotal * taxRate) / 100)
    }
    //calc total
    let total = Math.round(subtotal - discount + taxes + extraCost)

    //add calc fields to data and return the modified JSON object
    let newData = JSON.parse(JSON.stringify(data))
    newData.jcur_calc_sup_labor = supCost
    newData.jcur_calc_tec_labor = tecCost
    newData.jcur_calc_sub_waste = wasteCost
    newData.jcur_calc_waste = Number(data.jcur_calc_waste) + Number(data.jcur_calc_dumpster)
    newData.jcur_calc_eqp = Number(data.jcur_calc_eqp) + Number(data.jcur_calc_dis)
    //newData.jcur_calc_sup = supplyUsageCost
    //newData.jcur_calc_other = otherCost
    //newData.jcur_calc_sub_full =
    //  supCost + tecCost + wasteCost + newData.jcur_calc_sub_full + supplyUsageCost + otherCost
    newData.jcur_calc_disc = discount
    newData.jcur_calc_sub = newData.jcur_calc_sub_full - discount
    newData.jcur_calc_3rd_other = extraCost
    //newData.jcur_calc_tax = taxes
    //newData.jcur_calc_tot = total

    return newData
  }

  /**
   * Recalculates the final cost based on input data
   */
  calculate() {
    //select data based off selected scope
    let estimate = this.selectedScope == 1 ? this.data.primaryIni : this.data.secondaryIni
    let data = this.selectedScope == 1 ? this.data.primaryActual : this.data.secondaryActual

    //calc dumpster
    let costBagster = data.jfin_num_bagster * estimate.jini_price_bagster
    let costDumpster = data.jfin_num_dumpster * estimate.jini_price_dumpster + costBagster

    //calc waste
    let numBoxes = Number(data.jfin_num_box_suits) + Number(data.jfin_num_box_debris)
    let costPerBox = Number(estimate.jini_price_transport) + Number(estimate.jini_price_disposal)
    data.jfin_calc_sub_waste = Math.round(numBoxes * costPerBox) + Number(estimate.jini_price_wm_fee) + costDumpster

    //calc asb test
    data.jfin_calc_asb_test = data.jfin_num_asb_vial > 0 ? Number(estimate.jini_price_asb_test) : 0
    data.jfin_calc_asb_vial = estimate.jini_price_asb_vial * data.jfin_num_asb_vial

    //calc overhead
    data.jfin_calc_ovh = Math.round(
      ((Number(data.jfin_calc_sub_labor) + Number(data.jfin_calc_sub_waste) + Number(data.jfin_calc_eqp_dis)) *
        estimate.jini_per_ovh) /
        100
    )

    //calc covid swabs
    if (data.jfin_num_srf_swab == 0) {
      data.jfin_calc_srf_swab = 0
    } else {
      data.jfin_calc_srf_swab =
        Number(data.jfin_num_srf_swab) * Number(estimate.jini_price_srf_swab) + Number(estimate.jini_price_srf_test)
    }

    //we need to calc a temp other/sub_full to use in additional charges calc
    let tmpOther =
      Number(data.jfin_calc_dispatch) +
      Number(data.jfin_calc_atp_test) +
      Number(data.jfin_calc_asb_test) +
      Number(data.jfin_calc_asb_vial) +
      Number(data.jfin_calc_demob) +
      Number(data.jfin_calc_ovh) +
      Number(data.jfin_calc_disinfect) +
      Number(data.jfin_calc_srf_swab)

    let tmpFull =
      Number(data.jfin_calc_sub_labor) +
      Number(data.jfin_calc_sub_waste) +
      Number(data.jfin_calc_eqp) +
      Number(data.jfin_calc_sup) +
      tmpOther

    /*addl/other calc
    if (this.isAdditionalLocked()) {
      //recalculate additional field
      let diff = estimate.jini_calc_sub_full - tmpFull
      data.jfin_calc_addl_rec = diff > 0 ? diff : 0

      //repopulate calc field
      data.jfin_calc_addl_other = data.jfin_calc_addl_rec
      this.inpDom.addlInput.value = (data.jfin_calc_addl_other/100).toFixed(2)
    } else {
      data.jfin_calc_addl_other = data.jfin_amt_addl_final
    }*/

    //calc other
    data.jfin_calc_other =
      Number(data.jfin_calc_dispatch) +
      Number(data.jfin_calc_atp_test) +
      Number(data.jfin_calc_asb_test) +
      Number(data.jfin_calc_asb_vial) +
      Number(data.jfin_calc_demob) +
      Number(data.jfin_calc_ovh) +
      Number(data.jfin_calc_disinfect) +
      Number(data.jfin_calc_phodoc) +
      Number(data.jfin_calc_digrep) +
      Number(data.jfin_amt_addl_final) +
      Number(data.jfin_calc_srf_swab)

    //calc rack (sub_full)
    data.jfin_calc_sub_full =
      Number(data.jfin_calc_sub_labor) +
      Number(data.jfin_calc_sub_waste) +
      Number(data.jfin_calc_eqp) +
      Number(data.jfin_calc_sup) +
      Number(data.jfin_calc_other)

    //calc initial discount
    if (data.jfin_disc_amt_other != 0) {
      data.jfin_calc_disc_ini = data.jfin_disc_amt_other
    } else {
      data.jfin_calc_disc_ini = Math.round((data.jfin_calc_sub_full * data.jfin_disc_per) / 100)
    }

    //calc max discount
    data.jfin_calc_disc_max = Number(data.jfin_calc_sub_full) - Number(estimate.jcur_calc_sub)
    if (data.jfin_calc_disc_max < 0) data.jfin_calc_disc_max = 0

    //calc discount
    if (data.jfin_calc_disc_ini < data.jfin_calc_disc_max) {
      data.jfin_calc_disc = Number(data.jfin_calc_disc_ini) + Number(data.jfin_disc_amt_addl)
    } else {
      data.jfin_calc_disc = Number(data.jfin_calc_disc_max) + Number(data.jfin_disc_amt_addl)
    }

    //calc discounted subtotal
    data.jfin_calc_sub = data.jfin_calc_sub_full - data.jfin_calc_disc

    //calc 3p/other
    data.jfin_calc_3rd_other = Number(data.jfin_amt_other) + Number(data.jfin_amt_3rd_party)

    //calc tax
    if (data.jfin_amt_tax_final != null && data.jfin_amt_tax_final != undefined) {
      data.jfin_calc_tax = data.jfin_amt_tax_final
    } else if (estimate.jini_tax_type == 1) {
      data.jfin_calc_tax = Math.round((estimate.jini_yn_state_tax * data.jfin_calc_sup * estimate.jini_tax_rate) / 100)
    } else if (estimate.jini_tax_type == 2) {
      data.jfin_calc_tax = Math.round((estimate.jini_yn_state_tax * data.jfin_calc_sub * estimate.jini_tax_rate) / 100)
    } else {
      data.jfin_calc_tax = 0
    }

    //calc tot
    data.jfin_calc_tot = Number(data.jfin_calc_sub) + Number(data.jfin_calc_tax) + Number(data.jfin_calc_3rd_other)

    //populate calculated fields
    this.infoDom.jfin_calc_sub_waste.innerText = Util.formatCurrency(data.jfin_calc_sub_waste)
    this.infoDom.jfin_calc_other.innerText = Util.formatCurrency(data.jfin_calc_other)
    this.infoDom.jfin_calc_sub_full.innerText = Util.formatCurrency(data.jfin_calc_sub_full)
    this.infoDom.jfin_calc_disc.innerText = Util.formatCurrency(data.jfin_calc_disc)
    this.infoDom.jfin_calc_sub.innerText = Util.formatCurrency(data.jfin_calc_sub)
    this.infoDom.jfin_amt_3rd_party.innerText = Util.formatCurrency(data.jfin_amt_3rd_party)
    this.infoDom.jfin_amt_other.innerText = Util.formatCurrency(data.jfin_amt_other)
    this.infoDom.jfin_calc_tax.innerText = Util.formatCurrency(data.jfin_calc_tax)
    this.infoDom.jfin_calc_tot.innerText = Util.formatCurrency(data.jfin_calc_tot)

    //populate discount input fields
    this.inpDom.jfin_calc_disc_ini.value = data.jfin_calc_disc_ini / 100
    this.inpDom.jfin_calc_disc_max.value = data.jfin_calc_disc_max / 100

    //populate waste breakdown fields
    this.infoDom.finalBoxes.innerText = Util.formatCurrency(Math.round(numBoxes * costPerBox))
    this.infoDom.finalDumpsters.innerText = Util.formatCurrency(costDumpster)
    this.infoDom.finalWastemgmt.innerText = Util.formatCurrency(estimate.jini_price_wm_fee)
    
    //populate equip breakdown fields
    this.infoDom.finalEquipUsage.innerText = Util.formatCurrency(data.jfin_calc_eqp_use)
    this.infoDom.finalEquipDis.innerText = Util.formatCurrency(data.jfin_calc_eqp_dis)

    //populate supply breakdown fields
    let supplyUsage = Number(data.jfin_calc_sup) - Number(data.jfin_calc_supply_fee)
    this.infoDom.finalSupplyUsage.innerText = Util.formatCurrency(supplyUsage)
    this.infoDom.finalSupplyExtra.innerText = Util.formatCurrency(data.jfin_calc_supply_fee)

    //populate other breakdown fields
    this.infoDom.finalDispatch.innerText = Util.formatCurrency(data.jfin_calc_dispatch)
    this.infoDom.finalATP.innerText = Util.formatCurrency(data.jfin_calc_atp_test)
    this.infoDom.finalASBTest.innerText = Util.formatCurrency(data.jfin_calc_asb_test)
    this.infoDom.finalASBSwab.innerText = Util.formatCurrency(data.jfin_calc_asb_vial)
    this.infoDom.finalSurfaceSwab.innerText = Util.formatCurrency(data.jfin_calc_srf_swab)
    this.infoDom.finalDemob.innerText = Util.formatCurrency(data.jfin_calc_demob)
    this.infoDom.finalDisinfect.innerText = Util.formatCurrency(data.jfin_calc_disinfect)
    this.infoDom.finalPhodoc.innerText = Util.formatCurrency(data.jfin_calc_phodoc)
    this.infoDom.finalDigrep.innerText = Util.formatCurrency(data.jfin_calc_digrep)
    this.infoDom.finalOverhead.innerText = Util.formatCurrency(data.jfin_calc_ovh)
    this.infoDom.finalAdditional.innerText = Util.formatCurrency(data.jfin_amt_addl_final)

    //run & view the margin calculation
    this.viewMarginCalc()
  }

  /**
   * Gets the currently selected final estimate data
   */
  getSelectedFinal() {
    return this.selectedScope == 1 ? this.data.primaryActual : this.data.secondaryActual
  }

  /**
   * Gets whether or not the currently selected scope is locked
   * @deprecated
   */
  isAdditionalLocked() {
    return this.getSelectedFinal().additionalLocked
  }

  /**
   * Sets the additional charges override field to be locked/unlocked
   * @deprecated
   * @param {Boolean} locked The estimated value will be used if true, manual override if false
   * @param {Boolean} initial Whether this is the initialization call or not
   */
  setAdditionalLocked(locked, initial = false) {
    let data = this.getSelectedFinal()
    data.additionalLocked = locked

    //change styling
    this.inpDom.addlLock.src = locked ? 'icons/locked16.png' : 'icons/unlocked16.png'
    this.inpDom.addlLock.title = locked ? 'Unlock/Override' : 'Lock'
    //this.inpDom.addlInput.value = data.jfin_calc_addl_rec / 100
    this.inpDom.addlInput.disabled = locked

    //update data
    if (!initial) {
      if (locked) {
        data.jfin_amt_addl_final = null
      } else {
        data.jfin_amt_addl_final = data.jfin_calc_addl_rec
      }

      this.inpDom.addlInput.value = (data.jfin_calc_addl_rec / 100).toFixed(2)
    }

    this.calculate()
  }
}
