import { JobScopeItem } from "./jobScopeItem.js"
import { JobScopeCostItem } from "./jobScopeCostItem.js"
import { JobScopeDiscountItem } from "./jobScopeDiscountItem.js"
import { JobScopeInfo } from "./jobScopeInfo.js"
import { JobScopeMarginItem } from "./jobScopeMarginItem.js?v5"
import { JobScopeTotals } from "./jobScopeTotals.js"
import { default as note } from '../modules/lib/note.js'
import { Util } from '../util.js'

/**
 * Block that holds input fields for setting a job scope
 * @author Jake Cirino & Scott Kiehn
 */
export class JobScope{
  /**
   * Creates a new job scope item
   * @param {JSON} data 
   */
  constructor(data){
    this.data = data
    //console.log(this.data)

    //throw error temporarily
    window.onerror = (msg, url, line, col, error) => {
      let subject = `jobScope.js error`
      let message = `
      ${msg}
      <br>
      ${url} line ${line}, col ${col}
      <br>
      ${error}
      `
      console.log(message)
    
      fetch('errorReportPRG.php', {
        method: 'post',
        headers: {'Content-Type': 'application/json;charset=utf8'},
        body: JSON.stringify({
          action: 'jsError',
          subject: subject,
          message: message
        })
      })
        .then(res => res.json())
        .then(body => {
          console.log(`Error has${body.ok ? '' : ' not'} been reported`)
        })
    }

    this.confirmed = this.data.info.jif_ini_is_confirmed == 1 ? true : false
    //console.log(`CONFIRMED: ${this.confirmed}`)

    //TODO Lock initial estimate if not user's own JIF

    //create a permision object
    this.perm      = {}
    this.perm.auth = 'jif_a'  // auth: Authorize JIFs
    this.perm.disc = 'jif_d'  // disc: Discount over 50%
    this.perm.mnge = 'jif_m'  // mnge: Manage JIFs
    this.perm.edit = 'jif_e'  // edit: Edit JIFs
    this.perm.read = 'jif_r'  // read: Edit JIFs, Edit own
    this.perm.verf = 'job_cf' // verf: Override job verification

    this.perm.component        = {}
    this.perm.component.margin = this.perm.mnge

    //create an object for discount approval
    this.approval      = {}
    this.approval.all  = true
    this.approval.emp  = 0
    this.approval.JA0  = {}
    this.approval.JA1  = {}
    this.approval.JA2  = {}
    this.approval.pct1 = 20
    this.approval.pct2 = 50
    this.approval.col1 = '#fcc'
    this.approval.col2 = '#f66'

    //create JA0, JA1 & JA2
    for (let a of this.data.approvers)
      this.approval[a.er_role_code][a.emp_id] = a.emp_name

    //get employee id
    this.approval.emp = Object.keys(this.approval.JA0)[0]

    //Combine JA1 into JA0
    this.approval.JA0 = Object.assign(this.approval.JA0, this.approval.JA1);

    this.scopeItems = {}
    this.costItems = {}
    this.discountItems = {}
    this.marginItems = {}
    this.customEvent = {
      ScopeSelectionFinished: new CustomEvent('ScopeSelectionFinished'),
      CalculateCostsFinished: new CustomEvent('CalculateCostsFinished')
    }
    this.baseElement = document.querySelector('.scope')

    //create a parent level DOM object, this can be added to in each block
    this.dom               = {}
    this.dom.save1Btn      = this.baseElement.querySelector('[name=save-1]')
    this.dom.save2Btn      = this.baseElement.querySelector('[name=save-2]')
    this.dom.confirmBtn    = this.baseElement.querySelector('[name=confirm]')
    this.dom.unconfirmBtn  = this.baseElement.querySelector('[name=unconfirm]')
    this.dom.confCopy      = this.baseElement.querySelector('[name=confirm-copy]')
    this.dom.confInitial   = this.baseElement.querySelector('[name=confirm-initial]')
    this.dom.unconfInitial = this.baseElement.querySelector('[name=unconfirm-initial]')
    this.dom.confHistory   = this.baseElement.querySelector('[name=confirm-history]')
    this.dom.confVerify    = this.baseElement.querySelector('[name=confirm-verify]')

    if (this.confirmed && window.aftermath.hasPerm(this.perm.mnge)) {
      this.dom.confirmBtn.style.display = 'none'
      this.dom.unconfirmBtn.style.display = 'inline-block'
    }

    //show confirm history if any
    this.history()

    this.populate()

    // Due to async loading of JS modules, only run
    // jobVerifyConnect() after window property exists
    this.verify = {} //create an object for any verify needs
    if (window.aftermath.hasOwnProperty('jifVerifyCount')) {
      this.jobVerifyConnect()
    } else {
      document.addEventListener('JobVerifyChangeEvent', () => this.jobVerifyConnect())
    }
  }

  /**
   * jobVerify concerns
   * TODO This area can be added to 
   */
  jobVerifyConnect() {
    if (Number(window.aftermath.jifVerifyCount.remaining_2) > 0)
      this.verify.override = true
    else
      this.verify.override = false
  }

  async populate(){

    //create scope info block
    this.scopeInfo = new JobScopeInfo(this, this.data.info)

    //push to data.scopes to force an additioanl column for scope totals
    this.data.scopes.push({ scope_id: 0, scope_name: 'Selected Scopes', scope_short: 'Selected Scopes', scope_code: 'Selected Scopes' })

    //generate scope items, we dont use foreach here because its non-blocking
    for(let i in this.data.scopes){

      let scopeData = this.data.scopes[i]

      //create discount item
      let discountItem = new JobScopeDiscountItem(this, scopeData)
      await discountItem.populate()
      this.discountItems[scopeData.scope_id] = discountItem

      //create cost item
      let costItem = new JobScopeCostItem(this, scopeData)
      await costItem.populate()
      this.costItems[scopeData.scope_id] = costItem

      //create scope item
      let scopeItem = new JobScopeItem(this, discountItem, costItem, scopeData)
      await scopeItem.populate() //blocks loop from reaching next iteration until completion
      this.scopeItems[scopeData.scope_id] = scopeItem

      //create cost margin
      let marginItem = new JobScopeMarginItem(this, scopeData)
      await marginItem.populate()
      this.marginItems[scopeData.scope_id] = marginItem

      costItem.calculateCosts(scopeItem.data)
    }

    //set initial state of save & confirm buttons
    this.enableSaveBtn(false)

    //create scope total
    this.scopeTotal = new JobScopeTotals(this)

    //set save buttons click event
    this.dom.save1Btn.addEventListener('click', (() => {
      this.save()
    }).bind(this))
    this.dom.save2Btn.addEventListener('click', (() => {
      this.save()
    }).bind(this))

    //set confirm button click event
    this.dom.confirmBtn.addEventListener('click', ((ev) => {
      this.confirm(ev.target, true)
    }).bind(this))

    //set unconfirm button click event
    this.dom.unconfirmBtn.addEventListener('click', ((ev) => {
      this.confirm(ev.target, false)
    }).bind(this))
  }

  /**
   * Calculates and updates the costs for a specified scope
   * @param {Number} scope_id 
   */
  calculateCosts(scope_id) {
    this.costItems[scope_id].calculateCosts()
  }

  /**
   * Enables/disables the save/confirm buttons
   * @param {Boolean} enabled
   */
  enableSaveBtn(enabled = true) {
    // 1. Toggle the save buttons, but always disable confirm
    this.dom.save1Btn.disabled = !enabled
    this.dom.save2Btn.disabled = !enabled
    this.dom.confirmBtn.disabled = true

    const showCopy = []

    // 2. Enable confirm if ready
    if (this.dom.pri.value) 
      this.dom.confirmBtn.disabled = enabled

    // 3. Check for Discount Approval and disable all if no approval
    this.approval.all = true
    for (let x in this.discountItems) {
      if (!this.discountItems[x].approved)
        this.approval.all = false
    }

    if (!this.approval.all) {
      this.dom.save1Btn.disabled = true
      this.dom.save2Btn.disabled = true
      this.dom.confirmBtn.disabled = true
      showCopy.push('The "Discount Approval" selection is required to Save and Confirm this estimate.')
    }

    // 4. Check for Margin Approval and disable confirm if no approval
    if (this.marginItems[this.dom.pri.value]) {
      const marginItem = this.marginItems[this.dom.pri.value]
      if (marginItem.cm.job.ok !== true) {
        if (marginItem.dom.mgnApproval.value < 1) {
          this.dom.confirmBtn.disabled = true
          showCopy.push('The "Margin Approval" for the primary scope is required to Confirm this estimate.')
        }
      }
    }
    if (this.marginItems[this.dom.sec.value]) {
      const marginItem = this.marginItems[this.dom.sec.value]
      if (marginItem.cm.job.ok !== true) {
        if (marginItem.dom.mgnApproval.value < 1) {
          this.dom.confirmBtn.disabled = true
          showCopy.push('The "Margin Approval" for the secondary scope is required to Confirm this estimate.')
        }
      }
    }

    // 6. At end, check if confirmed in jif and disable confirm button
    if (this.confirmed)
      this.dom.confirmBtn.disabled = true

    // 7. Add button message if provided.
    if (showCopy.length) {
      this.showCopy(showCopy.join('<br>'))
    } else {
      this.eraseCopy(this.dom.confCopy)
    }
  }

  /**
   * Saves the entire form
   */
  save(){
    //collect scope data
    let scopes = {}

    //loop through each item
    for(let scopeID in this.scopeItems){
      let scopeItem  = this.scopeItems[scopeID],
        discountItem = this.discountItems[scopeID],
        costItem     = this.costItems[scopeID],
        marginItem   = this.marginItems[scopeID]

      if (!scopeItem.data.scope_id) continue

      if(scopeItem.data.jini_id !== null){
        //add scope data
        scopes[scopeID] = scopeItem.data
        //console.log(scopeItem.data)
        
        //add discount data
        scopes[scopeID].jini_disc = discountItem.getDiscountType()
        scopes[scopeID].jini_disc_per = discountItem.getRawDiscount()
        scopes[scopeID].jini_disc_amt_other = discountItem.getDiscountOther()
        scopes[scopeID].jini_disc_emp_appr = discountItem.getDiscountApprover() || 'NULL'

        //add margin data
        scopes[scopeID].jini_mgn_emp_appr = marginItem.getMarginApprover() || 'NULL'
      }
    }

    //save scope info
    this.scopeInfo.save()

    //post to database
    fetch('jobScopePRG.php', {
      method: 'post',
      headers: {'Content-Type': 'application/json;charset=utf8'},
      body: JSON.stringify({
        action: 'save',
        jobID: this.data.jobID,
        scopes: scopes
      })
    })
    .then(res => res.json())
    .then(body => {
      this.enableSaveBtn(!body.ok)
      window.jifController.update()
    })
  }

  /**
   * Confirms the entire form
   */
  confirm(button, doConfirmed = true){
    let selectPri = this.dom.pri,
        selectSec = this.dom.sec,
        html = doConfirmed ? this.dom.confInitial.innerHTML : this.dom.unconfInitial.innerHTML

    button.disabled = true

    let scopeCopy = selectPri.options[selectPri.selectedIndex].innerText
    scopeCopy    += selectSec.value ? ' and ' + selectSec.options[selectSec.selectedIndex].innerText : ''

    this.showCopy(html.replace(/{{scopes}}/, scopeCopy))

    const domOverride = this.dom.confCopy.querySelector('[name=override-reason]')
    if (domOverride && this.verify.override) {
      domOverride.closest('label').style.display = 'block'
      if (window.aftermath.hasPerm(this.perm.verf)) {
        domOverride.disabled = false
      }
    }

    this.dom.confCopy.querySelector('[data-no]').addEventListener('click', ev => {
      button.disabled = false
      this.eraseCopy(this.dom.confCopy)
    })

    this.dom.confCopy.querySelector('[data-yes]').addEventListener('click', ev => {

      const overrideData = {}
      if (domOverride) {
        if (this.verify.override && domOverride.value.length === 0) {
          note({text: 'The verification override reason is required.', class: 'alert'})
          return
        } else {
          overrideData['jvo_job']    = this.data.jobID
          overrideData['jvo_phase']  = domOverride.dataset.phase
          overrideData['jvo_reason'] = domOverride.value
        }
      }

      fetch('jifItemPRG.php', {
        method: 'post',
        headers: {'Content-Type': 'application/json;charset=utf8'},
        body: JSON.stringify({
          action: 'updateIniConfirmInfo',
          data: {jif_id: this.data.info.jif_id, jif_ini_is_confirmed: (doConfirmed ? 1 : 0)},
          overrideData: overrideData
        })
      })
      .then(res => res.json())
      .then(body => {
        if (body.ok) {

          if (body.override && body.override === false) {
            note({text: 'There was an error in saving the verification override data.'})
          }

          this.data.info = body.info

          this.confirmed = this.data.info.jif_ini_is_confirmed == 1 ? true : false

          this.eraseCopy(this.dom.confCopy, this.history.bind(this))

          if (this.confirmed) {
            this.dom.confirmBtn.disabled = true

            if (window.aftermath.hasPerm(this.perm.mnge)) {
              this.dom.confirmBtn.style.display = 'none'
              this.dom.unconfirmBtn.style.display = 'inline-block'
              this.dom.unconfirmBtn.disabled = false
            }
          } else {
            this.dom.unconfirmBtn.style.display = 'none'
            this.dom.confirmBtn.style.display = 'inline-block'
            this.dom.confirmBtn.disabled = false
          }

          this.enableSaveBtn(false)

          window.jifController.update() //update sidebar checklist

          this.jobVerifyConnect() //reset the verify status

        } else {
          note({text: 'There has been an error with your request.', class: 'error'})
        }
      }) 
    })
  }

  /**
   * Provides history content
   */
  history(){
    let html = ''
  
    let jifIniDTConf = new Date(this.data.info.jif_ini_dt_confirmed)
    if(isNaN(jifIniDTConf)){ //replace - with / if the device doesnt support it
      jifIniDTConf = new Date(this.data.info.jif_ini_is_confirmed.replaceAll('-', '/'))
    }
  
    //create date object
    if (this.confirmed) {
      html += `<p>The estimate was confirmed on ${Util.dateToString(jifIniDTConf)} by ${this.data.info.jif_ini_name_confirmed}</p>`
    } else if (this.data.info.jif_ini_emp_confirmed) {
      html += `<p>The estimate was unconfirmed on ${Util.dateToString(jifIniDTConf)} by ${this.data.info.jif_ini_name_confirmed}</p>`
    }
    this.dom.confHistory.innerHTML = html
    if (html.length) this.dom.confHistory.style.opacity = '1'
  }

  /**
   * Shows dynamic text
   * @param {string} copy
   */
  showCopy(copy){
    this.dom.confCopy.innerHTML = copy
    this.dom.confCopy.style.opacity = '1'
  }

  /**
   * Erases a DOM item per it's transition duration if set
   * @param {element} domItem 
   */
  eraseCopy(domItem, func = {}){
      let duration = domItem.style.transitionDuration ? parseFloat(domItem.style.transitionDuration) * 1000 : 0
      domItem.style.opacity = '0'
      setTimeout(() => {
        domItem.innerHTML = ''
        if (typeof func == 'function') func()
      }, duration)
  }
}
