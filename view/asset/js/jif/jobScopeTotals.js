/**
 * Provides scope totals for the Initial Estimate
 * @author Scott Kiehn
 */

import { Util } from "../util.js"

String.prototype.toNum = function () {return Number(this)}
Number.prototype.toNum = function () {return Number(this)}

const re  = new RegExp(/[\$\.,]/g, 'g'),
      re2 = new RegExp(/@/g, 'g'),
      $re = new RegExp(/^\$/),
      toMoney = (p, s) => Util.formatCurrency( p.replace(re, '').toNum() + s.replace(re, '').toNum() )

export class JobScopeTotals{
  /**
   * Creates a new
   * @param {JobScope} parent 
   */
  constructor(parent){
    this.parent = parent
    this.ttl = {}
    this.pri = {}
    this.sec = {}
    this.priScope = undefined
    this.secScope = undefined

    // Disable all form inputs so they just used for a view
    for (const input of document.getElementById('scope-item-0').querySelectorAll('input'))
      input.disabled = true

    // Set calc to run upon an event dispatched (JobScopeInfo.constructor())
    document.addEventListener('ScopeSelectionFinished', ev => this.setScopeData())

    // Set calc to run upon an event dispatched (JobScopeCostItem.calculateCosts())
    document.addEventListener('CalculateCostsFinished', ev => this.calculateScopeTotals())

    this.buildScopeElements('ttl', 0)
    this.setScopeData()
  }

  /**
   * 
   */
  buildScopeElements(key, id) {
    for (const el of document.getElementById(`scope-item-${id}`).querySelectorAll('[name]'))          this[key][el.getAttribute('name')] = el
    for (const el of document.getElementById(`scope-discount-item-${id}`).querySelectorAll('[name]')) this[key][el.getAttribute('name')] = el
    for (const el of document.getElementById(`scope-cost-item-${id}`).querySelectorAll('[name]'))     this[key][el.getAttribute('name')] = el
    for (const el of document.getElementById(`scope-margin-item-${id}`).querySelectorAll('[name]'))   this[key][el.getAttribute('name')] = el
  }

  /**
   * 
   */
  setScopeData() {
    if (this.priScope != this.parent.dom.pri.value) {
      this.priScope = this.parent.dom.pri.value || undefined
      if (this.priScope)
        this.buildScopeElements('pri', this.priScope)
      else
        this.pri = {}
    }

    if (this.secScope != this.parent.dom.sec.value) {
      this.secScope = this.parent.dom.sec.value || undefined
      if (this.secScope)
        this.buildScopeElements('sec', this.secScope)
      else
        this.sec = {}
    }

    this.calculateScopeTotals()
  }

  /**
   * 
   */
  calculateScopeTotals() {
    if (this.priScope) {

      this.ttl.jini_num_days.value   = this.pri.jini_num_days.value
      this.ttl.jini_num_crew.value   = this.pri.jini_num_crew.value
      this.ttl.jini_num_zone.value   = this.pri.jini_num_zone.value
      this.ttl.jini_num_truck.value  = this.pri.jini_num_truck.value
      this.ttl.jini_num_rental.value = this.pri.jini_num_rental.value
      this.ttl.jini_num_hotel.value  = this.pri.jini_num_hotel.value

      this.ttl.jini_hr_haz.value     = this.pri.jini_hr_haz.value.toNum() + ( this.secScope ? this.sec.jini_hr_haz.value.toNum() : 0 )
      this.ttl.jini_hr_set.value     = this.pri.jini_hr_set.value.toNum() + ( this.secScope ? this.sec.jini_hr_set.value.toNum() : 0 )
      this.ttl.jini_hr_bio.value     = this.pri.jini_hr_bio.value.toNum() + ( this.secScope ? this.sec.jini_hr_bio.value.toNum() : 0 )
      this.ttl.jini_hr_cnt.value     = this.pri.jini_hr_cnt.value.toNum() + ( this.secScope ? this.sec.jini_hr_cnt.value.toNum() : 0 )
      this.ttl.jini_hr_srf.value     = this.pri.jini_hr_srf.value.toNum() + ( this.secScope ? this.sec.jini_hr_srf.value.toNum() : 0 )
      this.ttl.jini_hr_cln.value     = this.pri.jini_hr_cln.value.toNum() + ( this.secScope ? this.sec.jini_hr_cln.value.toNum() : 0 )
      this.ttl.jini_hr_poe.value     = this.pri.jini_hr_poe.value.toNum() + ( this.secScope ? this.sec.jini_hr_poe.value.toNum() : 0 )
      this.ttl.jini_hr_fin.value     = this.pri.jini_hr_fin.value.toNum() + ( this.secScope ? this.sec.jini_hr_fin.value.toNum() : 0 )
      this.ttl.jini_hr_ppr.value     = this.pri.jini_hr_ppr.value.toNum() + ( this.secScope ? this.sec.jini_hr_ppr.value.toNum() : 0 )

      this.ttl['total-manhours'].innerText  = this.pri['total-manhours'].innerText.toNum() + ( this.secScope ? this.sec['total-manhours'].innerText.toNum() : 0 )
      this.ttl['total-hours'].innerText     = (this.pri['total-hours'].innerText.toNum() + ( this.secScope ? this.sec['total-hours'].innerText.toNum() : 0 )).toFixed(2)
      this.ttl.jini_num_box_suits.value     = this.pri.jini_num_box_suits.value.toNum() + ( this.secScope ? this.sec.jini_num_box_suits.value.toNum() : 0 )
      this.ttl.jini_num_box_debris.value    = this.pri.jini_num_box_debris.value.toNum() + ( this.secScope ? this.sec.jini_num_box_debris.value.toNum() : 0 )
      this.ttl['total-boxes'].innerText     = this.pri['total-boxes'].innerText.toNum() + ( this.secScope ? this.sec['total-boxes'].innerText.toNum() : 0 )
      this.ttl.jini_num_dumpster.value      = this.pri.jini_num_dumpster.value.toNum() + ( this.secScope ? this.sec.jini_num_dumpster.value.toNum() : 0 )
      this.ttl.jini_num_bagster.value       = this.pri.jini_num_bagster.value.toNum() + ( this.secScope ? this.sec.jini_num_bagster.value.toNum() : 0 )
      this.ttl.jini_num_asb_vial.value      = this.pri.jini_num_asb_vial.value.toNum() + ( this.secScope ? this.sec.jini_num_asb_vial.value.toNum() : 0 )
      this.ttl.jini_amt_other.value         = (this.pri.jini_amt_other.value.toNum() + ( this.secScope ? this.sec.jini_amt_other.value.toNum() : 0 )).toFixed(2)
      this.ttl.jini_amt_3rd_party.value     = (this.pri.jini_amt_3rd_party.value.toNum() + ( this.secScope ? this.sec.jini_amt_3rd_party.value.toNum() : 0 )).toFixed(2)

      this.ttl['man-hours'].innerText       = toMoney(this.pri['man-hours'].innerText, ( this.secScope ? this.sec['man-hours'].innerText : '$0.00' ))
      this.ttl['waste-mgmt'].innerText      = toMoney(this.pri['waste-mgmt'].innerText, ( this.secScope ? this.sec['waste-mgmt'].innerText : '$0.00' ))
      this.ttl['equip-dis'].innerText       = toMoney(this.pri['equip-dis'].innerText, ( this.secScope ? this.sec['equip-dis'].innerText : '$0.00' ))
      this.ttl['proj-mgmt'].innerText       = toMoney(this.pri['proj-mgmt'].innerText, ( this.secScope ? this.sec['proj-mgmt'].innerText : '$0.00' ))
      this.ttl['emergency-disp'].innerText  = toMoney(this.pri['emergency-disp'].innerText, ( this.secScope ? this.sec['emergency-disp'].innerText : '$0.00' ))
      this.ttl['atp-test'].innerText        = toMoney(this.pri['atp-test'].innerText, ( this.secScope ? this.sec['atp-test'].innerText : '$0.00' ))
      this.ttl['asbestos-test'].innerText   = toMoney(this.pri['asbestos-test'].innerText, ( this.secScope ? this.sec['asbestos-test'].innerText : '$0.00' ))
      this.ttl['asbestos-vials'].innerText  = toMoney(this.pri['asbestos-vials'].innerText, ( this.secScope ? this.sec['asbestos-vials'].innerText : '$0.00' ))
      this.ttl['offsite-demobil'].innerText = toMoney(this.pri['offsite-demobil'].innerText, ( this.secScope ? this.sec['offsite-demobil'].innerText : '$0.00' ))
      this.ttl['truck-dis'].innerText       = toMoney(this.pri['truck-dis'].innerText, ( this.secScope ? this.sec['truck-dis'].innerText : '$0.00' ))
      this.ttl.dumpster.innerText           = toMoney(this.pri.dumpster.innerText, ( this.secScope ? this.sec.dumpster.innerText : '$0.00' ))
      this.ttl['equip-usage'].innerText     = toMoney(this.pri['equip-usage'].innerText, ( this.secScope ? this.sec['equip-usage'].innerText : '$0.00' ))
      this.ttl['supply-usage'].innerText    = toMoney(this.pri['supply-usage'].innerText, ( this.secScope ? this.sec['supply-usage'].innerText : '$0.00' ))
      this.ttl.subtotal.innerText           = toMoney(this.pri.subtotal.innerText, ( this.secScope ? this.sec.subtotal.innerText : '$0.00' ))
      this.ttl.discount.innerText           = toMoney(this.pri.discount.innerText, ( this.secScope ? this.sec.discount.innerText : '$0.00' ))
      this.ttl.taxes.innerText              = toMoney(this.pri.taxes.innerText, ( this.secScope ? this.sec.taxes.innerText : '$0.00' ))
      this.ttl['3rd-party'].innerText       = toMoney(this.pri['3rd-party'].innerText, ( this.secScope ? this.sec['3rd-party'].innerText : '$0.00' ))
      this.ttl.total.innerText              = toMoney(this.pri.total.innerText, ( this.secScope ? this.sec.total.innerText : '$0.00' ))

      this.ttl.suprRate.innerText           = toMoney(this.pri.suprRate.innerText, ( this.secScope ? this.sec.suprRate.innerText : '$0.00' ))
      this.ttl.suprRateWknd.innerText       = toMoney(this.pri.suprRateWknd.innerText, ( this.secScope ? this.sec.suprRateWknd.innerText : '$0.00' ))
      this.ttl.techRate.innerText           = toMoney(this.pri.techRate.innerText, ( this.secScope ? this.sec.techRate.innerText : '$0.00' ))
      this.ttl.techRateWknd.innerText       = toMoney(this.pri.techRateWknd.innerText, ( this.secScope ? this.sec.techRateWknd.innerText : '$0.00' ))
      this.ttl.supr.innerText               = this.pri.supr.innerText
      this.ttl.tech.innerText               = this.pri.tech.innerText
      this.ttl.crew.innerText               = this.pri.crew.innerText
      this.ttl.manHours.innerText           = this.pri.manHours.innerText.toNum() + ( this.secScope ? this.sec.manHours.innerText.toNum() : 0 )
      this.ttl.roundTrips.innerText         = this.pri.roundTrips.innerText

      this.ttl.laborExp.innerText        = toMoney(this.pri.laborExp.innerText, ( this.secScope ? this.sec.laborExp.innerText : '$0.00' ))
      this.ttl.nonbillHoursExp.innerText = toMoney(this.pri.nonbillHoursExp.innerText, ( this.secScope ? this.sec.nonbillHoursExp.innerText : '$0.00' ))
      this.ttl.payrollExp.innerText      = toMoney(this.pri.payrollExp.innerText, ( this.secScope ? this.sec.payrollExp.innerText : '$0.00' ))
      this.ttl.driveExp.innerText        = toMoney(this.pri.driveExp.innerText, ( this.secScope ? this.sec.driveExp.innerText : '$0.00' ))
      this.ttl.fuelExp.innerText         = toMoney(this.pri.fuelExp.innerText, ( this.secScope ? this.sec.fuelExp.innerText : '$0.00' ))
      this.ttl.vehicleExp.innerText      = toMoney(this.pri.vehicleExp.innerText, ( this.secScope ? this.sec.vehicleExp.innerText : '$0.00' ))
      this.ttl.rentalExp.innerText       = toMoney(this.pri.rentalExp.innerText, ( this.secScope ? this.sec.rentalExp.innerText : '$0.00' ))
      this.ttl.hotelExp.innerText        = toMoney(this.pri.hotelExp.innerText, ( this.secScope ? this.sec.hotelExp.innerText : '$0.00' ))
      this.ttl.branchOvhExp.innerText    = toMoney(this.pri.branchOvhExp.innerText, ( this.secScope ? this.sec.branchOvhExp.innerText : '$0.00' ))
      this.ttl.bioboxExp.innerText       = toMoney(this.pri.bioboxExp.innerText, ( this.secScope ? this.sec.bioboxExp.innerText : '$0.00' ))
      this.ttl.dumpsterExp.innerText     = toMoney(this.pri.dumpsterExp.innerText, ( this.secScope ? this.sec.dumpsterExp.innerText : '$0.00' ))
      this.ttl.supplyEquipExp.innerText  = toMoney(this.pri.supplyEquipExp.innerText, ( this.secScope ? this.sec.supplyEquipExp.innerText : '$0.00' ))
      this.ttl.estCOGS.innerText         = toMoney(this.pri.estCOGS.innerText, ( this.secScope ? this.sec.estCOGS.innerText : '$0.00' ))
      this.ttl.anticipColl.innerText     = toMoney(this.pri.anticipColl.innerText, ( this.secScope ? this.sec.anticipColl.innerText : '$0.00' ))
      this.ttl.grossProfit.innerText     = toMoney(this.pri.grossProfit.innerText, ( this.secScope ? this.sec.grossProfit.innerText : '$0.00' ))
      this.ttl.laborLabel.innerText      = this.pri.laborLabel.innerText
      this.ttl.wcLabel.innerText         = this.pri.wcLabel.innerText
      this.ttl.driveLabel.innerText      = this.pri.driveLabel.innerText
      this.ttl.driveTime.innerText       = this.pri.driveTime.innerText
      this.ttl.driveTime.innerText       = this.pri.driveTime.innerText
      this.ttl.trucks.innerText          = this.pri.trucks.innerText
      this.ttl.rentals.innerText         = this.pri.rentals.innerText
      this.ttl.hotels.innerText          = this.pri.hotels.innerText
      this.ttl.bioboxes.innerText        = '@' + (this.pri.bioboxes.innerText.replace(re2, '').toNum() + ( this.secScope ? this.sec.bioboxes.innerText.replace(re2, '').toNum() : 0 ))
      this.ttl.dumpsters.innerText       = '@' + (this.pri.dumpsters.innerText.replace(re2, '').toNum() + ( this.secScope ? this.sec.dumpsters.innerText.replace(re2, '').toNum() : 0 ))
      this.ttl.per_supplies.innerText    = this.pri.per_supplies.innerText
      //this.ttl.insNonLabel.innerText     = this.pri.insNonLabel.innerText

      const anticipColl = this.ttl.anticipColl.innerText.replace(re, '').toNum(),
            grossProfit = this.ttl.grossProfit.innerText.replace(re, '').toNum(),
            totalEstimate = this.ttl.total.innerText.replace(re, '').toNum(),
            costMargin = totalEstimate ? (grossProfit / totalEstimate).toFixed(3) : 0,
            marginSignal = document.getElementById('scope-margin-signal-0')

      marginSignal.innerHTML = this.parent.marginItems[this.priScope].cm.signal.text
      marginSignal.className = `scope-margin-signal ${this.parent.marginItems[this.priScope].cm.signal.color}`

      this.ttl.costMargin.innerText = `${(100 * costMargin).toFixed(1)}%`

    } else {

      for (const el of document.getElementById('scope-item-0').querySelectorAll('[name]')) {
        if (el.matches('[name=title]')) continue
        el.matches('input') ? el.value = '' : el.innerText = 0
      }

      for (const el of document.getElementById(`scope-cost-item-0`).querySelectorAll('[name]')) {
        if (el.matches('[name=title]')) continue
        el.innerText = '$0.00'
      }

      for (const el of document.getElementById(`scope-margin-item-0`).querySelectorAll('[name]')) {
        if (el.dataset.text || el.dataset.at || el.dataset.percent || el.dataset.hour) {
          el.innerText = ''
          continue
        }
        if (el.innerText.match($re)) el.innerText = '$0.00'
      }

      this.ttl.crew.innerText = '(0)'
      this.ttl.supr.innerText = 'S0 '
      this.ttl.tech.innerText = 'T0 '
      this.ttl.manHours.innerText = '0'
      this.ttl.roundTrips.innerText = '0'
      this.ttl.costMargin.innerText = '0.0%'
      const marginSignal = document.getElementById('scope-margin-signal-0')
      marginSignal.innerHTML = ''
      marginSignal.className = 'scope-margin-signal white1'
    }
  }
}
