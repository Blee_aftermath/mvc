import { PhaseListItem } from './phaseListItem.js'

/**
 * Toolbar for phases
 * @author Jake Cirino
 */
export class PhaseList {
  /**
   * Creates a new phaselist menu
   * @param {Array} data The data for this phaselist
   */
  constructor(data) {
    this.data = data
    this.jobID = data.jobID

    //get head element to append to
    this.head = document.querySelector('[phaselist-insert]')

    //create base element
    this.baseElement = document.createElement('u')
    this.baseElement.id = 'jifNav'

    //create list items
    this.items = []
    let itemCount = 0 //need to track where to put delimeters
    let tail = undefined
    this.data.phases.forEach((element) => {
      if (itemCount != 0) {
        //append delimeter
        let delim = document.createElement('li')
        delim.className = 'delim'
        this.baseElement.appendChild(delim)
      }

      var phaseItem = new PhaseListItem(this, element, tail)
      this.items.push(phaseItem)
      tail = phaseItem

      itemCount++
    })

    //append to head
    document.querySelector('[phaselist-insert]').appendChild(this.baseElement)

    //register with the jif controller
    window.jifController.registerComponent(this)
  }

  /**
   * Calls the phaseListPRG and updates the phaselist DOM.
   * This is called by the JifController on page update.
   * @param {Function} callback The callback for when the update is completed
   */
  update(callback) {
    fetch('phaseListPRG.php', {
      method: 'post',
      headers: { 'Content-Type': 'application/json;charset=utf8' },
      body: JSON.stringify({
        jobID: this.jobID
      }),
    })
      .then((res) => {
        return res.ok ? res.json() : res
      })
      .then((body) => {
        if (body.data.phases !== undefined) {

          //loop through and update states
          let index = 0
          body.data.phases.forEach(element => {
            this.items[index].update(element)
            index++
          })
        }else{
          console.error(body)
        }

        callback()
      })
  }
}
