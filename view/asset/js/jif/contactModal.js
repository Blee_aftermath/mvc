/**
 * Modal for editing addresses on the contact tab of the jif
 * @todo Needs refactoring
 * @author Jake Cirino
 */
export class ContactModal {
  /**
   * Creates a new contact modal. It can be destroyed using the javascript delete operator
   * @param {JSON} data The contact data to populate the form with:
   * {
   *  jobId: (int) The id of the job for this modal
   *  title: (string) The title of the modal
   *  type: (string) The type of modal, add or edit
   *  impType: (string) The important contact type of this modal
   *  displaySelectors: (bool) Determines if the modal provides the options to choose a new or existing contact
   *  displayForm: (bool) Determines if the model should initially display the form
   *  contacts: (array) Array of all the contacts for this form
   *  selectedContact: (json) JSON data for the currently selected contact
   *  states: (array) List of states
   * }
   */
  constructor(parent, data) {
    this.inputs = []
    this.data = data
    this.parent = parent

    //create base elements
    this.baseElement = document.createElement('div')
    this.baseElement.className = 'modal'
    this.baseElement.id = 'modal-contact-tmp'
    this.baseElement.style.display = 'block'

    this.populate()
  }

  /**
   * Populates the modal
   */
  async populate(){
    let template = await window.templateController.getTemplate('jif/contactModal')
    this.baseElement.innerHTML = template

    this.selectorsElement = this.baseElement.querySelector("[name='selectors']")
    this.formElement = this.baseElement.querySelector("[name='form']")

    //set title
    this.baseElement.querySelector('.title').innerText = this.data.title

    //setup radio buttons
    this.baseElement.querySelectorAll("[name='contactType']").forEach((element) => {
      element.addEventListener(
        'click',
        ((event) => {
          if (this.selectorType != element.value) {
            this.setSelectorType(element.value)
            this.setSelectedContact(undefined)
          }
        }).bind(this),
        false
      )
    })

    //push input elements into inputs array
    let inputFields = this.formElement.querySelectorAll('td.r input, td.r select, td.r textarea')
    inputFields.forEach((element) => {
      //handle special cases for element names
      switch (element.name) {
        case 'addr_state':
          //create state options
          this.data.states.forEach((state) => {
            let stateOption = document.createElement('option')
            stateOption.value = state.st_id
            stateOption.innerText = state.st_name
            element.appendChild(stateOption)
          })
          break
        case 'sel-contact':
          //create contact options
          if (this.data.contacts !== null) {
            this.data.contacts.forEach((contact) => {
              let contactOption = document.createElement('option')
              contactOption.value = contact.addr_id
              contactOption.innerText = `${contact.addr_fname} ${contact.addr_lname}`
              element.appendChild(contactOption)
            })
          }
          break
      }

      this.inputs.push(element)
    })

    //populate contact selector
    this.contactSelector = this.baseElement.querySelector("[name='sel-contact']")
    if (this.data.contacts !== undefined) {
      this.data.contacts.forEach((contact) => {
        let contactOption = document.createElement('option')
        contactOption.value = contact.addr_id
        contactOption.innerText = `${contact.addr_fname} ${contact.addr_lname}`
        this.contactSelector.appendChild(contactOption)
      })
    }

    //update form fields on selector
    this.contactSelector.addEventListener(
      'change',
      ((event) => {
        if (event.target.value != '') {
          this.data.contacts.forEach((element) => {
            if (element.addr_id == event.target.value) this.setSelectedContact(element)
          })
        } else {
          this.clearForm()
        }
      }).bind(this),
      false
    )

    //populate form data if a contact is selected
    if (this.data.selectedContact !== undefined) {
      this.setSelectedContact(this.data.selectedContact)

      //select contact in contractSelector
      this.contactSelector.value = this.data.selectedContact.addr_id
    } else this.setSelectedContact(undefined)

    //setup close buttons
    this.baseElement.querySelectorAll("[name='action-close']").forEach((element) => {
      element.addEventListener('click', this.close.bind(this), false)
    })

    //setup submit button
    let saveButton = this.baseElement.querySelector("[name='action-submit']")
    saveButton.addEventListener(
      'click',
      ((event) => {
        event.preventDefault()
        this.submit()
      }).bind(this),
      false
    )

    //append modal to document
    document.body.appendChild(this.baseElement)

    //set default values
    this.setSelectorType(this.data.type)
    this.setDisplaySelectors(this.data.displaySelectors)
    this.setDisplayForm(this.data.displayForm)
  }

  /**
   * Returns if this modal is for an important contact
   */
  isImportantContact() {
    return this.data.impType !== undefined
  }

  /**
   * Returns if the current state is to add a contact
   */
  isAddContact() {
    return this.data.type == 'add'
  }

  /**
   * Sets the selector type, either existing or add new
   * @param {String} type The selector type. Either 'add' or 'edit'
   */
  setSelectorType(type) {
    this.selectorType = type

    //change display conditionally
    this.setDisplayForm(type == 'add')
    if (type == 'important' || type == 'edit') this.contactSelector.style.display = ''
    else this.contactSelector.style.display = 'none'
  }

  /**
   * Sets whether the form will display the option selectors at the top of the form
   * @param {Boolean} displaySelectors
   */
  setDisplaySelectors(displaySelectors) {
    this.displaySelectors = displaySelectors

    this.selectorsElement.style.display = displaySelectors ? '' : 'none'
  }

  /**
   * Sets whether the input form should be displayed
   * @param {Boolean} displayForm
   */
  setDisplayForm(displayForm) {
    this.displayForm = displayForm

    this.formElement.style.display = displayForm ? '' : 'none'
  }

  /**
   * Sets the currently selected contact
   * @param {JSON} contactData
   */
  setSelectedContact(contactData) {
    this.selectedContact = contactData
    if (contactData != undefined) {
      //fill form fields
      this.inputs.forEach((element) => {
        if (element.type == 'checkbox') element.checked = contactData[element.name] == 1 ? true : false
        else element.value = contactData[element.name]
      })
    } else {
      this.clearForm()
      this.contactSelector.value = ''
    }
  }

  /**
   * Highlights error fields
   * @param {Array} fields
   */
  highlightErrors(fields) {
    fields.forEach((element) => {
      this.baseElement.querySelector(`[name='${element}']`).style.backgroundColor = 'rgb(255, 102, 102)'
    })
  }

  /**
   * Clears the form of all input
   */
  clearForm() {
    this.inputs.forEach((element) => {
      if (element.type == 'checkbox') element.checked = false
      else element.value = ''
    })
  }

  /**
   * Closes the modal, deleting it from the dom
   */
  close() {
    this.baseElement.remove()
  }

  /**
   * Submits the form
   */
  submit() {
    //get the type of submit we have to do
    let type
    if (this.isImportantContact()) {
      if (this.contactSelector.style.display == 'none') type = 'addImportant'
      else if (this.contactSelector.value == '') type = 'rmvImportant'
      else type = 'updImportant'
    } else {
      type = this.isAddContact() ? 'add' : 'edit'
    }

    //prepare submit body
    let submitData = {
      action: type,
      jobId: this.data.jobId,
      impType: this.data.impType,
    }
    switch (type) {
      case 'add':
        submitData.data = this.getForm()
        break
      case 'edit':
        submitData.addrId = this.selectedContact.addr_id
        submitData.data = this.getForm()
        break
      case 'addImportant':
        submitData.data = this.getForm()
        submitData.data['addr_is_' + this.data.impType] = true
        break
      case 'updImportant':
        submitData.addrId = this.selectedContact.addr_id
        break
    }

    console.log(submitData)

    //call api and get results
    fetch('jifContactsPRG.php', {
      method: 'post',
      headers: { 'Content-Type': 'application/json;charset=utf8' },
      body: JSON.stringify(submitData),
    })
      .then((res) => {
        return res.json()
      })
      .then((body) => {
        //if the response was ok, update. otherwise highlight errors
        if (body.ok) {
          //update the jif controller
          window.jifController.update(this.parent)

          //update parent dom
          this.parent.data.contacts = body.msg
          this.parent.populateContacts(this.parent.data)

          //close the modal
          this.close()
        } else {
          this.highlightErrors(body.msg)
        }
        console.log(body) //TODO update, close modal, error returns
      })
  }

  /**
   * Gets data from the form to be submitted
   */
  getForm() {
    //retrieve form data from fields
    let formData = {}
    this.inputs.forEach((element) => {
      if (element.type == 'checkbox') {
        formData[element.name] = element.checked
      } else {
        formData[element.name] = element.value.trim()
      }
    })

    return formData
  }
}
