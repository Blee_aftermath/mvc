import {TextareaModal} from '../textareaModal.js'
import {Util} from '../util.js'
import {ChangeOrder} from './changeOrder.js'
import {CostMarginCalc} from './costMarginCalc.js'

String.prototype.toNum = function () {return Number(this)}
Number.prototype.toNum = function () {return Number(this)}

/**
 * A change order input section
 * @author Jake Cirino
 */
export class ChangeOrderItem {
  /**
   * Creates a new change order item
   * @param {ChangeOrder} parent
   * @param {JSON} data
   * @param {String} prefix Either jini, jco or tot
   * @param {String} type Either pri, sec or tot
   * @param {Boolean} disabled
   */
  constructor(parent, data, prefix, type, disabled) {
    this.parent = parent
    this.data = data
    
    this.prefix = prefix
    this.type = type
    this.disabled = disabled
    this.visible = true
    
    //TODO trucks and rental trucks
    
    //create base element
    this.baseElement = document.createElement('div')
    this.baseElement.className = 'scope-item'
    this.baseElement.innerHTML = this.data.template
    
    //create discount base element
    this.discountBaseElement = document.createElement('div')
    this.discountBaseElement.className = 'scope-item'
    this.discountBaseElement.innerHTML = this.data.discountTemplate
    
    //create cost base element
    this.costBaseElement = document.createElement('div')
    this.costBaseElement.className = 'scope-item'
    this.costBaseElement.setAttribute('name', 'alt-color')
    this.costBaseElement.innerHTML = this.data.costTemplate
    
    //create margin base element
    this.marginBaseElement = document.createElement('div')
    this.marginBaseElement.className = 'scope-item'
    this.marginBaseElement.setAttribute('name', 'alt-color')
    this.marginBaseElement.innerHTML = this.data.marginTemplate
    
    //resize elements for jco
    if (this.prefix == 'jco') {
      this.baseElement.style.width = '143.25px'
      this.discountBaseElement.style.width = '143.25px'
      this.costBaseElement.style.width = '143.25px'
      this.marginBaseElement.style.width = '143.25px'
    }
    
    //strip prefixes off item data, total fields dont need prefix stripping
    //TODO handle final data
    this.stripItemPrefixes()
    
    //declare elements
    this.titleElement = this.baseElement.querySelector("[name='title']")
    this.invElement = this.baseElement.querySelector("[name='inv']")
    this.deleteButton = this.baseElement.querySelector("[name='deleteButton']")
    this.pdfButton = this.baseElement.querySelector("[name='pdfButton']")
    this.pdfButtonLink = this.baseElement.querySelector("[name='pdfButtonLink']")
    this.addButton = this.baseElement.querySelector("[name='addButton']")
    this.explanationButton = this.baseElement.querySelector('[name=editExplanation]')
    this.costTitle = this.costBaseElement.querySelector("[name='title']")
    this.costInvTitle = this.costBaseElement.querySelector("[name='inv']")
    this.totalManhours = this.baseElement.querySelector("[name='total-manhours']")
    this.totalHours = this.baseElement.querySelector("[name='total-hours']")
    this.totalBoxes = this.baseElement.querySelector("[name='total-boxes']")
    this.discountType = this.discountBaseElement.querySelector("[name='dis-type']")
    this.discountAmount = this.discountBaseElement.querySelector("[name='dis-amount']")
    this.discountOther = this.discountBaseElement.querySelector("[name='dis-other']")
    this.discountApproval = this.discountBaseElement.querySelector("[name='dis-approval']")
    
    //declare cost elements
    this.manHoursCost = this.costBaseElement.querySelector("[name='man-hours']")
    this.wasteManagementCost = this.costBaseElement.querySelector("[name='waste-mgmt']")
    this.equipDisCost = this.costBaseElement.querySelector("[name='equip-dis']")
    this.projectMgmtCost = this.costBaseElement.querySelector("[name='proj-mgmt']")
    this.emergencyDispCost = this.costBaseElement.querySelector("[name='emergency-disp']")
    this.atpTestCost = this.costBaseElement.querySelector("[name='atp-test']")
    this.asbestosTestCost = this.costBaseElement.querySelector("[name='asbestos-test']")
    this.asbestosVialsCost = this.costBaseElement.querySelector("[name='asbestos-vials']")
    this.demobilizationCost = this.costBaseElement.querySelector("[name='offsite-demobil']")
    this.truckDisCost = this.costBaseElement.querySelector("[name='truck-dis']")
    this.phodocCost = this.costBaseElement.querySelector("[name='phodoc']")
    this.digrepCost = this.costBaseElement.querySelector("[name='digrep']")
    this.dumpsterCost = this.costBaseElement.querySelector("[name='dumpster']")
    this.equipUsageCost = this.costBaseElement.querySelector("[name='equip-usage']")
    this.supplyUsageCost = this.costBaseElement.querySelector("[name='supply-usage']")
    this.subtotalCost = this.costBaseElement.querySelector("[name='subtotal']")
    this.discountCost = this.costBaseElement.querySelector("[name='discount']")
    this.taxesCost = this.costBaseElement.querySelector("[name='taxes']")
    this.thirdPartyCost = this.costBaseElement.querySelector("[name='3rd-party']")
    this.totalCost = this.costBaseElement.querySelector("[name='total']")
    
    //declare margin elements
    this.mgnSignal = this.marginBaseElement.querySelector('.scope-margin-signal')
    this.mgnValues = this.marginBaseElement.querySelector('.scope-margin-values')
    this.mgnSuprRate = this.marginBaseElement.querySelector("[name='mgn_suprRate']")
    this.mgnSuprRateWknd = this.marginBaseElement.querySelector("[name='mgn_suprRateWknd']")
    this.mgnTechRate = this.marginBaseElement.querySelector("[name='mgn_techRate']")
    this.mgnTechRateWknd = this.marginBaseElement.querySelector("[name='mgn_techRateWknd']")
    this.mgnCrew = this.marginBaseElement.querySelector("[name='mgn_crew']")
    this.mgnManHours = this.marginBaseElement.querySelector("[name='mgn_manHours']")
    this.mgnRoundTrips = this.marginBaseElement.querySelector("[name='mgn_roundTrips']")
    this.mgnSupr = this.marginBaseElement.querySelector("[name='mgn_supr']")
    this.mgnTech = this.marginBaseElement.querySelector("[name='mgn_tech']")
    this.mgnLaborExp = this.marginBaseElement.querySelector("[name='mgn_laborExp']")
    this.mgnLaborLabel = this.marginBaseElement.querySelector("[name='mgn_laborLabel']")
    this.mgnNonbillHoursExp = this.marginBaseElement.querySelector("[name='mgn_nonbillHoursExp']")
    this.mgnPayrollExp = this.marginBaseElement.querySelector("[name='mgn_payrollExp']")
    this.mgnWcLabel = this.marginBaseElement.querySelector("[name='mgn_wcLabel']")
    this.mgnDriveExp = this.marginBaseElement.querySelector("[name='mgn_driveExp']")
    this.mgnDriveLabel = this.marginBaseElement.querySelector("[name='mgn_driveLabel']")
    this.mgnFuelExp = this.marginBaseElement.querySelector("[name='mgn_fuelExp']")
    this.mgnDriveTime = this.marginBaseElement.querySelector("[name='mgn_driveTime']")
    this.mgnVehicleExp = this.marginBaseElement.querySelector("[name='mgn_vehicleExp']")
    this.mgnTruck = this.marginBaseElement.querySelector("[name='mgn_truck']")
    this.mgnRentalExp = this.marginBaseElement.querySelector("[name='mgn_rentalExp']")
    this.mgnRental = this.marginBaseElement.querySelector("[name='mgn_rental']")
    this.mgnHotelExp = this.marginBaseElement.querySelector("[name='mgn_hotelExp']")
    this.mgnHotel = this.marginBaseElement.querySelector("[name='mgn_hotel']")
    this.mgnBranchOvhExp = this.marginBaseElement.querySelector("[name='mgn_branchOvhExp']")
    this.mgnBioboxExp = this.marginBaseElement.querySelector("[name='mgn_bioboxExp']")
    this.mgnBioboxes = this.marginBaseElement.querySelector("[name='mgn_bioboxes']")
    this.mgnDumpsterExp = this.marginBaseElement.querySelector("[name='mgn_dumpsterExp']")
    this.mgnDumpsters = this.marginBaseElement.querySelector("[name='mgn_dumpsters']")
    this.mgnBagsterExp = this.marginBaseElement.querySelector("[name='mgn_bagsterExp']")
    this.mgnBagsters = this.marginBaseElement.querySelector("[name='mgn_bagsters']")
    this.mgnSupplyEquipExp = this.marginBaseElement.querySelector("[name='mgn_supplyEquipExp']")
    this.mgnSupplyPer = this.marginBaseElement.querySelector("[name='mgn_supplyPer']")
    this.mgnInternalExp = this.marginBaseElement.querySelector("[name=mgn_internalExp]")
    this.mgnEstCOGS = this.marginBaseElement.querySelector("[name='mgn_estCOGS']")
    this.mgnAnticipColl = this.marginBaseElement.querySelector("[name='mgn_anticipColl']")
    this.mgnRealiznInfo = this.marginBaseElement.querySelector("[name='mgn_realiznInfo']")
    this.mgnGrossProfit = this.marginBaseElement.querySelector("[name='mgn_grossProfit']")
    this.mgnCostMargin = this.marginBaseElement.querySelector("[name='mgn_costMargin']")
    
    //Allow margin to be viewed if proper permission
    if (window.aftermath.hasPerm(this.parent.marginPerm))
      this.mgnValues.style.display = 'block'
    
    //setup input field updating
    let inputFields = this.baseElement.querySelectorAll('input')
    for (const key in inputFields) {
      if (inputFields.hasOwnProperty(key)) {
        const element = inputFields[key]
        element.addEventListener('change', (event) => {
          //enable save button
          this.parent.saveButton.disabled = false
          
          //update data
          if (event.target.type != 'button')
            if (event.target.name.includes('amt')) {
              this.data.item[event.target.name] = event.target.value * 100
            } else if (event.target.name.startsWith('hr')) {
              let targetNum = Number(event.target.value)
              let rounded = Math.round(targetNum / 0.25) * 0.25
              this.data.item[event.target.name] = event.target.value
              event.target.value = rounded
            } else if (event.target.name.startsWith('is')) this.data.item[event.target.name] = event.target.checked ? 1 : 0
            else this.data.item[event.target.name] = event.target.value
          
          //calculate
          this.calculate()
          
          //update totals
          this.parent.updateTotals()
        })
      }
    }
    
    //For now (2020-12-11), no discounts to be added
    //setup discount field updating
    //this.discountType.addEventListener('change', (event) => {
    //  this.data.item.disc_per = 0
    //  this.setDiscount(event.target.value)
    
    //  this.parent.updateTotals()
    //  this.parent.saveButton.disabled = false
    //})
    
    //setup discount amount updating
    //this.discountAmount.addEventListener('change', (event) => {
    //  this.data.item.disc_per = event.target.value
    //  this.calculate()
    
    //  this.parent.updateTotals()
    //  this.parent.saveButton.disabled = false
    //})
    
    //setup explanation edit button
    this.explanationButton.addEventListener('click', () => {
      new TextareaModal('Change Order Explanation', this.data.item.explanation, 'Save', (explanation) => {
        if (explanation != null) {
          //enable save button
          this.parent.saveButton.disabled = false
          
          //update data
          this.data.item.explanation = explanation
        }
      })
    })
    
    //initialize when this item is a not a total
    if (this.prefix != 'tot') this.initialize()
    
    //conditionally append elements to document
    switch (this.prefix) {
      case 'jini':
        this.parent.scopeBody.insertBefore(this.baseElement, this.parent.scopeScroll)
        this.parent.discountBody.insertBefore(this.discountBaseElement, this.parent.discountScroll)
        this.parent.costBody.insertBefore(this.costBaseElement, this.parent.costScroll)
        this.parent.marginBody.insertBefore(this.marginBaseElement, this.parent.marginScroll)
        break
      case 'jco':
        this.parent.scopeScroll.appendChild(this.baseElement)
        this.parent.discountScroll.appendChild(this.discountBaseElement)
        this.parent.costScroll.appendChild(this.costBaseElement)
        this.parent.marginScroll.appendChild(this.marginBaseElement)
        break
      default:
        this.parent.scopeBody.insertBefore(this.baseElement, this.parent.scopeScroll.nextSibling)
        this.parent.discountBody.insertBefore(this.discountBaseElement, this.parent.discountScroll.nextSibling)
        this.parent.costBody.insertBefore(this.costBaseElement, this.parent.costScroll.nextSibling)
        this.parent.marginBody.insertBefore(this.marginBaseElement, this.parent.marginScroll.nextSibling)
        break
    }
  }
  
  /**
   * Initializes the item based off its data
   */
  initialize() {
    //setup delete button based on condition
    if (this.prefix == 'jco') {
      this.deleteButton.addEventListener('click', (event) => {
        fetch('changeOrderPRG.php', {
          method: 'post',
          headers: {'Content-Type': 'application/json;charset=utf8'},
          body: JSON.stringify({
            action: 'deleteChangeOrder',
            jcoID: this.data.item.id,
          }),
        })
          .then((res) => res.json())
          .then((body) => {
            this.parent.deleteItem(this)
          })
      })
      
      this.pdfButtonLink.setAttribute('href', `${this.pdfButtonLink.getAttribute('href')}&job=${this.data.item.job}&jco=${this.data.item.jco}`)
      
    } else {
      this.deleteButton.style.display = 'none'
      this.pdfButtonLink.style.display = 'none'
    }
    
    //setup add button for jini items
    if (this.prefix == 'jini') {
      this.addButton.addEventListener('click', (event) => {
        fetch('changeOrderPRG.php', {
          method: 'post',
          headers: {'Content-Type': 'application/json;charset=utf8'},
          body: JSON.stringify({
            action: 'createChangeOrder',
            jiniID: this.data.item.id,
          }),
        })
          .then((res) => res.json())
          .then((body) => {
            if (body.ok) {
              //add new change order
              let jcoData
              if (this.type == 'pri') jcoData = JSON.parse(JSON.stringify(this.parent.priData))
              else if (this.type == 'sec') jcoData = JSON.parse(JSON.stringify(this.parent.secData))
              jcoData.item = body.msg
              let item = new ChangeOrderItem(this.parent, jcoData, 'jco', this.type, false)
              if (this.type == 'pri') this.parent.items.push(item)
              else if (this.type == 'sec') this.parent.secItems.push(item)
              this.parent.updateTotals()
              window.jifController.update()
            }
          })
      })
    } else {
      this.addButton.style.display = 'none'
    }
    
    //set titles
    if (this.prefix == 'jini') {
      this.titleElement.innerText = 'Initial Estimate'
      this.costTitle.innerText = 'Initial Estimate'
      this.invElement.innerText = this.data.invType
      this.costInvTitle.innerText = this.data.invType
    } else if (this.prefix == 'jco') {
      this.titleElement.innerText = `Change Order ${this.data.item.num}`
      this.costTitle.innerText = `Change Order ${this.data.item.num}`
      this.invElement.innerText = this.data.invType
      this.costInvTitle.innerText = this.data.invType
    } else if (this.prefix == 'fin') {
      this.titleElement.innerText = 'Actual Final'
      this.costTitle.innerText = 'Actual Final'
      this.invElement.innerText = this.data.invType
      this.costInvTitle.innerText = this.data.invType
    } else {
      this.titleElement.innerText = 'Current Estimate'
      this.costTitle.innerText = 'Current Estimate'
      this.invElement.innerText = 'Total'
      this.costInvTitle.innerText = 'Total'
    }
    
    //populate discount options
    if (this.prefix != 'tot') {
      
      for (const key in this.data.discounts) {
        if (this.data.discounts.hasOwnProperty(key)) {
          const element = this.data.discounts[key]
          
          //create new option
          let option = document.createElement('option')
          option.value = key
          option.innerText = element.jdisc_dsca
          
          //append to discount type selector
          this.discountType.appendChild(option)
        }
        
        if (this.data.item.disc_emp_appr) {
          //only build a select option of one as it is just used for a view
          this.discountApproval.innerHTML = `<option value="${this.data.item.disc_emp_appr}" selected="selected">${this.data.item.disc_emp_name}</option>`
        }
      }
    }
    
    //setup disabled conditions
    //TODO this needs to be different for jif_final
    if (this.disabled) {
      
      //disable all input elements
      let inputElements = this.baseElement.querySelectorAll('input')
      for (const key in inputElements) {
        if (inputElements.hasOwnProperty(key)) {
          const element = inputElements[key]
          element.disabled = true
        }
      }
      
      //disable discount input
      let discountInputs = this.discountBaseElement.querySelectorAll('select')
      for (const key in discountInputs) {
        if (discountInputs.hasOwnProperty(key)) {
          const element = discountInputs[key]
          element.disabled = true
        }
      }
    } else if (this.prefix == 'jco') {
      this.discountType.disabled = true
      this.discountAmount.disabled = true
      this.discountOther.disabled = true
      this.discountApproval.disabled = true
    }
    
    //populate fields
    this.populate()
  }
  
  /**
   * Populates fields based off this items data
   */
  populate() {
    //populate data fields
    let inputFields = this.baseElement.querySelectorAll('input')
    for (const key in inputFields) {
      if (inputFields.hasOwnProperty(key)) {
        const field = inputFields[key]
        const value = this.data.item[field.name]
        if (field.type != 'button' && field.type != 'checkbox') {
          if (field.name.startsWith('num')) field.value = value
          else if (field.name.startsWith('hr')) field.value = Number(value).toFixed(2)
          else if (field.name.startsWith('amt')) field.value = (value / 100).toFixed(2)
        } else if (field.type == 'checkbox') field.checked = value == 1
      }
    }
    
    //set discount if this is not a total field
    //TODO do running estimate and final estimate need discounts to carry over?
    if (this.prefix != 'tot' && this.prefix != 'fin') this.setDiscount(this.data.item.disc)
    
    //recalculate totals
    this.calculate()
  }
  
  /**
   * Sets whether this change order item displays or not
   * @param {Boolean} visible
   */
  setVisible(visible) {
    this.visible = visible
    let dis = visible ? '' : 'none'
    
    this.baseElement.style.display = dis
    this.discountBaseElement.style.display = dis
    this.costBaseElement.style.display = dis
    this.marginBaseElement.style.display = dis
  }
  
  /**
   * Checks whether this change order item is visible or not
   * @returns {Boolean} True if the change order item is visible, false if it is not
   */
  isVisible() {
    return this.visible
  }
  
  /**
   * Sets the currently selected discount
   * @param {Number} discID
   */
  setDiscount(discID) {
    let data = this.data.discounts[discID]
    if (discID != null && discID != 'null') this.data.item.disc = discID
    else this.data.item.disc = null
    
    this.discountType.value = discID
    
    //remove previous discount amount options
    this.discountAmount.innerHTML = ''
    
    if (discID != null && discID != 'null') {
      //add discount amount options
      for (const key in data.jdiscper_per) {
        if (data.jdiscper_per.hasOwnProperty(key)) {
          const amount = data.jdiscper_per[key]
          const reqApr = data.jdiscper_req_apr
          
          if (this.data.item.disc_per == 0) this.data.item.disc_per = amount
          
          //create option
          let option = document.createElement('option')
          option.value = amount
          option.innerText = `${amount}%`
          //option.innerText = `${amount}% ${reqApr == 1 ? '- Requires Exec Approval' : ''}`
          //option.style.backgroundColor = reqApr == 1 ? 'rgb(255, 204, 204)' : ''
          
          //append option to discount amount selector
          this.discountAmount.appendChild(option)
        }
      }
      
      if (this.data.item.disc_emp_appr == 0) this.data.item.disc_per = amount
      
    } else {
      this.data.item.disc_per = 0
    }
    
    this.discountAmount.value = this.data.item.disc_per
    if (this.data.item.disc_amt_other) this.discountOther.value = this.data.item.disc_amt_other / 100
    this.calculate()
  }
  
  calculateManHours(data) {
    let crewSize = data.num_crew
    
    let sumHours = Number(data.hr_haz) + Number(data.hr_set) + Number(data.hr_bio) + Number(data.hr_cnt)
      + Number(data.hr_srf) + Number(data.hr_cln) + Number(data.hr_poe) + Number(data.hr_fin) + Number(data.hr_ppr)
    
    //man hours
    let manHoursSup = sumHours
    data.is_iirc == "1" ? manHoursSup *= this.data.pricing.rate_iicrc : manHoursSup *= this.data.pricing.rate_sup
    manHoursSup /= crewSize
    
    let manHoursTec = sumHours
    manHoursTec *= this.data.pricing.rate_tec
    manHoursTec *= ((crewSize - 1) / crewSize)
    
    return Math.round(manHoursSup + manHoursTec)
  }
  
  /**
   * Calculates the waste management
   * @param {JSON} data
   */
  calculateWasteManagement(data) {
    let numBoxes = Number(data.num_box_suits) + Number(data.num_box_debris)
    let costPerBox = Number(this.data.pricing.price_transport) + Number(this.data.pricing.price_disposal)
    return Math.round(numBoxes * costPerBox)
  }
  
  /**
   * Calculates the discount
   * @param {Number} subtotal
   * @param {Number} discountRate
   * @param {Number} discountOther
   * @returns {Number}
   */
  calculateDiscount(subtotal, discountRate, discountOther) {
    if (discountOther > 0) {
      return discountOther
    } else {
      return Math.round(subtotal * discountRate)
    }
  }
  
  /**
   * Calculates the taxes
   * @param {Number} taxType jini_tax_type
   * @param {Number} ynTax jini_yn_state_tax
   * @param {Number} taxRate jini_tax_rate
   * @param {Number} supplyCost supplyUsage
   * @param {Number} subtotal subtotal
   */
  calculateTaxes(taxType, ynTax, taxRate, supplyCost, subtotal) {
    if (taxType == 1) {
      //taxType 1: Only supplies are taxed
      return Math.round((ynTax * supplyCost * taxRate) / 100)
    } else if (taxType == 2) {
      //taxType 2: Entire subtotal is taxed
      return Math.round((ynTax * subtotal * taxRate) / 100)
    }
    
    return 0 //invalid/no tax type, taxes are 0
  }
  
  /**
   * Calculates values for this change order item
   */
  calculate() {
    console.log(`##### ${this.prefix} #####`)
    let data = this.data.item
    let crewSize = data.num_crew
    //calculate totals from data
    let manHours = 0,
      boxes = 0,
      discountRate = data.disc_per / 100,
      discountOther = data.disc_amt_other
    for (const field in data) {

      if (data.hasOwnProperty(field)) {
        const value = data[field]
        
        if (
          field.startsWith('hr') &&
          !field.startsWith('hr_bb') &&
          !field.startsWith('hr_nb_') &&
          !field.startsWith('hr_tec_') &&
          !field.startsWith('hr_sup_') &&
          !field.startsWith('hr_sup_') &&
          !field.startsWith('hr_wknd') &&
          field != 'hr_brk' &&
          field != 'hr_drv' &&
          field != 'hr_off'
        ) {
          manHours += new Number(value)
          console.log(`${field}: +${value} = ${manHours}`)
        } else if (field.startsWith('num_box')) {
          boxes += new Number(value)
        }
      }
    }

    //display hour totals
    this.totalManhours.innerText = manHours.toFixed(2)
    this.totalHours.innerText = (manHours / parseInt(crewSize)).toFixed(2)
    
    //display boxes total
    this.totalBoxes.innerText = boxes
    
    if (this.prefix == 'fin') {
      this.manHoursCost.innerText = Util.formatCurrency(data.calc_sub_labor)
      this.wasteManagementCost.innerText = Util.formatCurrency(Number(data.calc_sub_waste) - Number(data.calc_dumpster))
      this.equipDisCost.innerText = Util.formatCurrency(data.calc_eqp_dis)
      this.projectMgmtCost.innerText = Util.formatCurrency(data.calc_ovh)
      this.emergencyDispCost.innerText = Util.formatCurrency(data.calc_dispatch)
      this.atpTestCost.innerText = Util.formatCurrency(data.calc_atp_test)
      this.asbestosTestCost.innerText = Util.formatCurrency(data.calc_asb_test)
      this.asbestosVialsCost.innerText = Util.formatCurrency(data.calc_asb_vial)
      this.demobilizationCost.innerText = Util.formatCurrency(data.calc_demob)
      this.truckDisCost.innerText = Util.formatCurrency(data.calc_disinfect)
      this.phodocCost.innerText = Util.formatCurrency(data.calc_phodoc)
      this.digrepCost.innerText = Util.formatCurrency(data.calc_digrep)
      this.dumpsterCost.innerText = Util.formatCurrency(data.calc_dumpster)
      this.equipUsageCost.innerText = Util.formatCurrency(data.calc_eqp_use)
      this.supplyUsageCost.innerText = Util.formatCurrency(data.calc_sup)
      this.subtotalCost.innerText = Util.formatCurrency(data.calc_sub_full)
      this.discountCost.innerText = Util.formatCurrency(data.calc_disc)
      this.taxesCost.innerText = Util.formatCurrency(data.calc_tax)
      this.thirdPartyCost.innerText = Util.formatCurrency(Number(data.calc_3rd_other) + .001)
      this.totalCost.innerText = Util.formatCurrency(data.calc_tot)
    } else if (this.prefix != 'tot') {
      
      //cost section calculations
      let manHoursCost = this.calculateManHours(data)
      let wasteMgmt = this.calculateWasteManagement(data)
      let equipDis = Math.round((manHoursCost * this.data.markup.per_dis) / 100)
      let projectMgmt = Math.round(((manHoursCost + wasteMgmt + equipDis) * this.data.markup.per_ovh) / 100)
      let emergencyDisp = this.prefix == 'jco' ? 0 : Math.round(Number(this.data.pricing.price_dispatch))
      let atpTest =
        this.prefix == 'jco' ? 0 : Math.round(Number(this.data.pricing.price_atp_test) * Number(data.yn_atp_testing))
      let asbestosTest =
        this.prefix == 'jco' ? 0 : data.num_asb_vial > 0 ? Math.round(Number(this.data.pricing.price_asb_test)) : 0
      let asbestosVials = Math.round(this.data.pricing.price_asb_vial * data.num_asb_vial)
      let offsiteDemobil =
        this.prefix == 'jco' ? 0 : Math.round(Number(this.data.pricing.price_demob))
      let truckDis = this.prefix == 'jco' ? 0 : Number(this.data.pricing.price_disinfect)
      let phodocDis = this.prefix == 'jco' ? 0 : Number(this.data.pricing.price_phodoc)
      let digrepDis = this.prefix == 'jco' ? 0 : Number(this.data.pricing.price_digrep)
      let dumpster = Math.round(
        data.num_dumpster * this.data.pricing.price_dumpster + data.num_bagster * this.data.pricing.price_bagster
      )
      let usageSubtotal =
        manHoursCost +
        wasteMgmt +
        equipDis +
        projectMgmt +
        emergencyDisp +
        atpTest +
        asbestosTest +
        asbestosVials +
        offsiteDemobil +
        truckDis +
        dumpster
      let equipUsage = Math.round((usageSubtotal + 0.001) * (this.data.markup.per_eqp / 100))
      let supplyUsage = Math.round((usageSubtotal + 0.001) * (this.data.markup.per_sup / 100))
      let subtotal = Math.round(usageSubtotal + equipUsage + supplyUsage + phodocDis + digrepDis)
      let discountLabor =
        this.prefix != 'tot' ? Math.round(manHoursCost * discountRate) : this.data.item.calc_disc_labor
      let discountSupply =
        this.prefix != 'tot' ? Math.round((subtotal - manHoursCost) * discountRate) : this.data.item.calc_disc_other
      let discount =
        this.prefix != 'tot' ? -this.calculateDiscount(subtotal, discountRate, discountOther) : this.data.item.calc_disc
      let priTaxes = this.parent.data.primaryTaxes,
        secTaxes = this.parent.data.secondaryTaxes,
        taxType = this.type == 'pri' ? priTaxes.tax_type : secTaxes.tax_type,
        ynTax = this.type == 'pri' ? priTaxes.yn_state_tax : secTaxes.yn_state_tax,
        taxRate = this.type == 'pri' ? priTaxes.tax_rate : secTaxes.tax_rate
      let taxes = this.calculateTaxes(taxType, ynTax, taxRate, supplyUsage, subtotal)
      let thirdParty = Number(data.amt_3rd_party) + Number(data.amt_other) + .001
      let total = Math.round(subtotal + discount + taxes + thirdParty)
      
      //display cost values
      this.manHoursCost.innerText = Util.formatCurrency(manHoursCost)
      this.wasteManagementCost.innerText = Util.formatCurrency(wasteMgmt)
      this.equipDisCost.innerText = Util.formatCurrency(equipDis)
      this.projectMgmtCost.innerText = Util.formatCurrency(projectMgmt)
      this.emergencyDispCost.innerText = Util.formatCurrency(emergencyDisp)
      this.atpTestCost.innerText = Util.formatCurrency(atpTest)
      this.asbestosTestCost.innerText = Util.formatCurrency(asbestosTest)
      this.asbestosVialsCost.innerText = Util.formatCurrency(asbestosVials)
      this.demobilizationCost.innerText = Util.formatCurrency(offsiteDemobil)
      this.truckDisCost.innerText = Util.formatCurrency(truckDis)
      this.phodocCost.innerText = Util.formatCurrency(phodocDis)
      this.digrepCost.innerText = Util.formatCurrency(digrepDis)
      this.dumpsterCost.innerText = Util.formatCurrency(dumpster)
      this.equipUsageCost.innerText = Util.formatCurrency(equipUsage)
      this.supplyUsageCost.innerText = Util.formatCurrency(supplyUsage)
      this.subtotalCost.innerText = Util.formatCurrency(subtotal)
      this.discountCost.innerText = Util.formatCurrency(discount)
      this.taxesCost.innerText = Util.formatCurrency(taxes)
      this.thirdPartyCost.innerText = Util.formatCurrency(thirdParty)
      this.totalCost.innerText = Util.formatCurrency(total)
      
      //update calc data to be used by the total column, does not update the database
      this.data.item.calc_man_hours = manHours
      this.data.item.calc_labor = manHoursCost
      this.data.item.calc_waste = wasteMgmt
      this.data.item.calc_dis = equipDis
      this.data.item.calc_ovh = projectMgmt
      this.data.item.calc_dispatch = emergencyDisp
      this.data.item.calc_atp_test = atpTest
      this.data.item.calc_asb_test = asbestosTest
      this.data.item.calc_asb_vial = asbestosVials
      this.data.item.calc_demob = offsiteDemobil
      this.data.item.calc_disinfect = truckDis
      this.data.item.calc_dumpster = dumpster
      this.data.item.calc_eqp = equipUsage
      this.data.item.calc_sup = supplyUsage
      this.data.item.calc_sub = subtotal
      this.data.item.calc_disc_labor = discountLabor
      this.data.item.calc_disc_other = discountSupply
      this.data.item.calc_disc = discount
      this.data.item.calc_tax = taxes
      this.data.item.calc_tot = total
      
      //blank out the non-total cost margin signal blocks
      this.mgnSignal.style.backgroundColor = '#fff'
      this.mgnSignal.parentNode.style.backgroundColor = '#fff'
      this.mgnSignal.innerHTML = ''
    } else {
      //set fields based off item calc data
      this.manHoursCost.innerText = Util.formatCurrency(this.data.item.calc_labor)
      this.wasteManagementCost.innerText = Util.formatCurrency(this.data.item.calc_waste)
      this.equipDisCost.innerText = Util.formatCurrency(this.data.item.calc_dis)
      this.projectMgmtCost.innerText = Util.formatCurrency(this.data.item.calc_ovh)
      this.emergencyDispCost.innerText = Util.formatCurrency(this.data.item.calc_dispatch)
      this.atpTestCost.innerText = Util.formatCurrency(this.data.item.calc_atp_test)
      this.asbestosTestCost.innerText = Util.formatCurrency(this.data.item.calc_asb_test)
      this.asbestosVialsCost.innerText = Util.formatCurrency(this.data.item.calc_asb_vial)
      this.demobilizationCost.innerText = Util.formatCurrency(this.data.item.calc_demob)
      this.truckDisCost.innerText = Util.formatCurrency(this.data.item.calc_disinfect)
      this.phodocCost.innerText = Util.formatCurrency(this.data.item.calc_phodoc)
      this.digrepCost.innerText = Util.formatCurrency(this.data.item.calc_digrep)
      this.dumpsterCost.innerText = Util.formatCurrency(this.data.item.calc_dumpster)
      this.equipUsageCost.innerText = Util.formatCurrency(this.data.item.calc_eqp)
      this.supplyUsageCost.innerText = Util.formatCurrency(this.data.item.calc_sup)
      this.subtotalCost.innerText = Util.formatCurrency(this.data.item.calc_sub)
      this.discountCost.innerText = Util.formatCurrency(this.data.item.calc_disc)
      this.taxesCost.innerText = Util.formatCurrency(this.data.item.calc_tax)
      this.thirdPartyCost.innerText = Util.formatCurrency(
        Number(this.data.item.amt_3rd_party) + Number(this.data.item.amt_other)
      )
      this.totalCost.innerText = Util.formatCurrency(this.data.item.calc_tot)
      
      //create margin calc object
      this.cm = new CostMarginCalc({
        costlist: this.data.costlist, realzns: this.data.realzns, job: {
          quotePrice: data.calc_tot.toNum(),
          jobPmtInsNon: this.parent.data.jobPmtInsNon ? this.parent.data.jobPmtInsNon.toNum() : 0,
          jifConfirmed: this.data.iniEst.ini_is_confirmed.toNum(),
          jifPerRealEst: this.data.markup.per_real_est ? this.data.markup.per_real_est.toNum() : null,
          days: this.parent.data.crewInfo.jif_num_days.toNum(),
          crew: this.parent.data.crewInfo.jif_num_crew.toNum(),
          manHours: data.calc_man_hours.toNum(),
          trucks: data.num_truck.toNum(),
          transit: this.parent.data.crewInfo.jif_calc_hr_transit.toNum(),
          rentals: data.num_rental.toNum(),
          hotels: data.num_hotel.toNum(),
          dumpsters: data.num_dumpster.toNum(),
          bagsters: data.num_bagster.toNum(),
          bioboxes: data.num_box_debris.toNum() + data.num_box_suits.toNum(),
          equipment: data.calc_eqp.toNum(),
          supplies: data.calc_sup.toNum(),
          internalExp: data.amt_int_exp.toNum()
        }
      })
      
      //run margin calc
      this.cm.calcMargins()
      
      //set margin calc view
      let suprRate = data.is_iicrc == "1" ? data.rate_iicrc : data.rate_sup
      let suprRateWknd = data.is_iicrc == "1" ? data.wknd_iicrc : data.wknd_sup
      this.mgnSuprRate.innerText = Util.formatCurrency(suprRate)
      this.mgnSuprRateWknd.innerText = Util.formatCurrency(suprRateWknd)
      this.mgnTechRate.innerText = Util.formatCurrency(data.rate_tec)
      this.mgnTechRateWknd.innerText = Util.formatCurrency(data.wknd_tec)
      this.mgnSupr.innerText = '@1'
      this.mgnTech.innerText = `@${this.cm.job.crew - 1}`
      this.mgnCrew.innerText = this.cm.job.crew
      this.mgnManHours.innerText = this.cm.job.manHours
      this.mgnRoundTrips.innerText = this.cm.job.roundTrips
      this.mgnLaborExp.innerText = Util.formatCurrency(this.cm.expense.laborExp)
      this.mgnNonbillHoursExp.innerText = Util.formatCurrency(this.cm.expense.nonbillHoursExp)
      this.mgnPayrollExp.innerText = Util.formatCurrency(this.cm.expense.payrollExp)
      this.mgnDriveExp.innerText = Util.formatCurrency(this.cm.expense.driveExp)
      this.mgnFuelExp.innerText = Util.formatCurrency(this.cm.expense.fuelExp)
      this.mgnDriveTime.innerText = `${this.cm.job.driveTime}h`
      this.mgnVehicleExp.innerText = Util.formatCurrency(this.cm.expense.vehicleExp)
      this.mgnTruck.innerText = `@${this.cm.job.trucks}`
      this.mgnRentalExp.innerText = Util.formatCurrency(this.cm.expense.rentalExp)
      this.mgnRental.innerText = `@${this.cm.job.rentals}`
      this.mgnHotelExp.innerText = Util.formatCurrency(this.cm.expense.hotelExp)
      this.mgnHotel.innerText = `@${this.cm.job.hotels}`
      this.mgnBranchOvhExp.innerText = Util.formatCurrency(this.cm.expense.branchOvhExp)
      this.mgnBioboxExp.innerText = Util.formatCurrency(this.cm.expense.bioboxExp)
      this.mgnBioboxes.innerText = `@${this.cm.job.bioboxes}`
      this.mgnDumpsterExp.innerText = Util.formatCurrency(this.cm.expense.dumpsterExp)
      this.mgnDumpsters.innerText = `@${this.cm.job.dumpsters}`
      this.mgnBagsterExp.innerText = Util.formatCurrency(this.cm.expense.bagsterExp)
      this.mgnBagsters.innerText = `@${this.cm.job.bagsters}`
      this.mgnSupplyEquipExp.innerText = Util.formatCurrency(this.cm.expense.supplyEquipExp)
      this.mgnSupplyPer.innerText = `${this.cm.costlist.per_supplies}%`
      this.mgnInternalExp.innerText = Util.formatCurrency(this.cm.expense.internalExp)
      this.mgnEstCOGS.innerText = Util.formatCurrency(this.cm.job.estCOGS)
      this.mgnAnticipColl.innerText = Util.formatCurrency(this.cm.job.anticipColl)
      this.mgnRealiznInfo.innerText = this.cm.job.realiznInfo
      this.mgnGrossProfit.innerText = Util.formatCurrency(this.cm.job.grossProfit)
      this.mgnCostMargin.innerText = `${this.cm.job.costMargin}%`
      this.mgnLaborLabel.innerText = this.cm.costlist.office_is_labor_stdprm == 2 ? 'PRE' : 'STD'
      this.mgnDriveLabel.innerText = this.cm.costlist.office_is_drive_stdprm == 2 ? 'PRE' : 'STD'
      this.mgnWcLabel.innerText = this.cm.costlist.office_is_wc_stdprm == 2 ? 'PRE' : 'STD'
      this.mgnSignal.innerHTML = this.cm.signal.text
      this.mgnSignal.className = `scope-margin-signal ${this.cm.signal.color}`
    }
  }
  
  /**
   * Saves this change order item
   * @param {Function} callback The callback on completion
   */
  save(callback) {
    //prepare fetch body
    let body = {
      action: 'updateChangeOrder',
    }
    for (const key in this.data.item) {
      if (this.data.item.hasOwnProperty(key)) {
        const value = this.data.item[key]
        if (
          key.startsWith('hr_') ||
          key.startsWith('num_') ||
          key.startsWith('amt_') ||
          key.startsWith('disc') ||
          key.startsWith('id') ||
          key == 'explanation' ||
          key == 'is_complicating' ||
          key == 'is_revised_scope'
        ) {
          body['jco_' + key] = value
        }
      }
    }
    
    fetch('changeOrderPRG.php', {
      method: 'post',
      headers: {'Content-Type': 'application/json;charset=utf8'},
      body: JSON.stringify(body),
    })
      .then((res) => res.json())
      .then((body) => {
        if (body.ok) {
          this.parent.saveButton.disabled = true
          callback()
        } else {
          this.parent.saveButton.disabled = false
        }
      })
  }
  
  /**
   * Deletes this item from the dom
   */
  delete() {
    this.baseElement.remove()
    this.discountBaseElement.remove()
    this.costBaseElement.remove()
  }
  
  /**
   * Sets and updates the item data for this item
   * @param {JSON} item
   */
  setItemData(item) {
    this.data.item = item
    this.stripItemPrefixes()
  }
  
  /**
   * Strips prefixes off this.data.item
   */
  stripItemPrefixes() {
    if (this.prefix != 'tot') {
      let stripped = {}
      for (const key in this.data.item) {
        if (this.data.item.hasOwnProperty(key)) {
          const element = this.data.item[key]
          if (!key.startsWith('jcur_') && !key.startsWith('jif_') && !key.startsWith('scope_')) {
            stripped[key.substring(key.indexOf('_') + 1)] = element
          }
        }
      }
      this.data.item = stripped
    }
  }
  
  /**
   * Checks if this item is equal to another
   * @param {ChangeOrderItem} other
   * @returns {Boolean} True if the items are equal, false if they are not
   */
  equals(other) {
    return this.prefix == other.prefix && this.data.item.id == other.data.item.id && this.type == other.type
  }
}
