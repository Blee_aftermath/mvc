import {Util} from '../util.js'

/**
 * A specified item that represents a scope. ex Limited Scope, BioRemoval, ect.
 * @author Jake Cirino
 */
export class JobScopeItem {
  /**
   * Creates a new job scope item
   * @param {JobScope} parent
   * @param {JobScopeDiscountItem} discountItem
   * @param {JobScopeCostItem} costItem
   * @param {JSON} data The field/pricing, ect. data for this scope
   */
  constructor(parent, discountItem, costItem, data) {
    this.parent = parent
    this.data = data
    this.discountItem = discountItem
    this.costItem = costItem
    this.baseElement = document.createElement('div')
    this.baseElement.id = `scope-item-${this.data.scope_id}`
    this.baseElement.className = 'scope-item'
    
    if (this.data.scope_id) {
      //set the scope selector options
      this.recOpt = this.parent.dom.rec.querySelector(`option[value='${this.data.scope_id}']`)
      this.priOpt = this.parent.dom.pri.querySelector(`option[value='${this.data.scope_id}']`)
      this.secOpt = this.parent.dom.sec.querySelector(`option[value='5']`)
    }
  }
  
  /**
   * Populates the form
   */
  async populate() {
    //load html
    //TODO get rid of templating system
    let template = await window.templateController.getTemplate('jif/jobScopeItem')
    this.baseElement.innerHTML = template
    
    //populate fields
    let titleElement = this.baseElement.querySelector("[name='title']")
    titleElement.innerText = this.data.scope_code == 'LS' ? `${this.data.scope_code}/COV ` : this.data.scope_code
    
    //added so as to be accessed elsewhere
    this.numDays = this.baseElement.querySelector("[name='jini_num_days']")
    this.numCrew = this.baseElement.querySelector("[name='jini_num_crew']")
    this.numTruck = this.baseElement.querySelector("[name='jini_num_truck']")
    this.numRental = this.baseElement.querySelector("[name='jini_num_rental']")
    this.numHotel = this.baseElement.querySelector("[name='jini_num_hotel']")
    this.numDumpster = this.baseElement.querySelector("[name='jini_num_dumpster']")
    this.numBagster = this.baseElement.querySelector("[name='jini_num_bagster']")
    
    //calculate totals
    this.totalManhours = this.baseElement.querySelector("[name='total-manhours']")
    this.totalManhours.innerText = '0'
    
    this.totalHours = this.baseElement.querySelector("[name='total-hours']")
    this.totalHours.innerText = '0'
    
    console.log(this.data)
    this.totalWeekend = this.baseElement.querySelector("[name='jini_hr_wknd']")
    this.totalWeekend.innerText = '0'
    this.totalWeekend.disabled = this.data.jini_yn_wknd_ovrd == "0"
    
    this.totalWeekendCheckbox = this.baseElement.querySelector("[name=jini_yn_wknd_ovrd]")
    this.totalWeekendCheckbox.addEventListener('change', ((event) => {
      this.data.jini_yn_wknd_ovrd = event.target.checked ? 1 : 0
      this.totalWeekend.disabled = !event.target.checked
    }).bind(this))
    
    this.totalBoxes = this.baseElement.querySelector("[name='total-boxes']")
    this.totalBoxes.innerText = '0'
    
    this.enableCheckbox = this.baseElement.querySelector("[name='enable-scope']")
    let enabled = this.data.jini_id !== null
    this.setEnabled(enabled)
    this.enableCheckbox.checked = enabled
    
    if (
      this.data.scope_id == this.parent.dom.rec.value ||
      this.data.scope_id == this.parent.dom.pri.value ||
      this.data.scope_id == this.parent.dom.sec.value
    )
      this.enableCheckbox.disabled = true
    
    this.enableCheckbox.addEventListener(
      'change',
      ((event) => {
        //send add/delete request
        let action = event.target.checked ? 'createScope' : 'deleteScope'
        fetch('./jobScopePRG.php', {
          method: 'post',
          headers: {'Content-Type': 'application/json;charset=utf8'},
          body: JSON.stringify({
            action: action,
            jobID: this.parent.data.jobID,
            scope: this.data.scope_id,
            iniID: this.data.jini_id,
          }),
        })
          .then((res) => res.json())
          .then((body) => {
            if (body.ok) {
              this.data = body.msg
              this.setEnabled(event.target.checked, true)
              
              event.target.checked ? this.scopeSelectorEnable(true) : this.scopeSelectorEnable(false)
              
              window.jifController.update()
              
              if (this.data.jini_id !== null) this.populateFields()
              this.costItem.calculateCosts(this.data.scope_id)
            }
          })
      }).bind(this)
    )
    
    //populate initial data if it exists
    if (enabled) {
      this.populateFields()
      this.scopeSelectorEnable(true)
    } else {
      this.scopeSelectorEnable(false)
    }
    
    //setup event listener for fields
    this.baseElement.querySelectorAll('input').forEach(
      ((element) => {
        if (element != this.enableCheckbox) {
          //select listener
          element.addEventListener('focus', (event) => {
            event.target.select()
          })
          
          //change listener
          element.addEventListener('change', (event) => {
            //enable save button
            this.parent.enableSaveBtn()
            
            //update data
            if (event.target.type == 'checkbox') this.data[event.target.name] = event.target.checked ? 1 : 0
            else if (event.target.name.includes('amt')) {
              this.data[event.target.name] = parseInt(event.target.value * 100 + .001)
            } else if (event.target.name.startsWith('jini_hr')) {
              let targetNum = Number(event.target.value)
              let rounded = Math.round(targetNum / 0.25) * 0.25
              this.data[event.target.name] = rounded
              event.target.value = rounded
            } else this.data[event.target.name] = event.target.value
            
            //recalculate totals
            this.calculateTotals()
            this.costItem.calculateCosts(this.data.scope_id)
          })
        }
      }).bind(this)
    )
    
    //remove elements not needed for the total column
    if (!this.data.scope_id) {
      this.enableCheckbox.style.display = 'none'
      const cbEl = this.baseElement.querySelector("[name='jini_yn_include']")
      cbEl.parentNode.removeChild(cbEl)
    }
    
    //append to parents base element
    this.parent.baseElement.querySelector("[name='ini-scope']").appendChild(this.baseElement)
  }
  
  /**
   * Populates the input fields based off this.data
   */
  populateFields() {
    this.baseElement.querySelectorAll('input').forEach((element) => {
      if (element != this.enableCheckbox) {
        let val = this.data[element.name]
        if (element.type == 'checkbox') element.checked = val == 1
        else if (element.name.includes('amt')) element.value = (val / 100).toFixed(2)
        else element.value = val
      }
    })
    
    this.calculateTotals()
  }
  
  /**
   * Enables or disables scope selector options in the Scope Info
   * @param {Boolean} checked
   */
  scopeSelectorEnable(checked) {
    if (this.data.scope_id) {
      if (checked) {
        this.recOpt.disabled = false
        this.priOpt.disabled = false
        if (this.data.scope_id == 5) this.secOpt.disabled = false
      } else {
        this.recOpt.disabled = true
        this.priOpt.disabled = true
        if (this.data.scope_id == 5) this.secOpt.disabled = true
      }
    }
  }
  
  /**
   * Calculates the number of weekend/regular actual hours there is for an estimate
   * @param {number} manHours
   * @param {number} actualHours
   * @returns {{regularHours: number, weekendHours: number} | null}
   */
  calculateWeekendHours(manHours, actualHours) {
    try {
      let day = new Date(this.data.jini_dt_add).getDay()
      
      let regularHours = 0, weekendHours = 0
      while (actualHours > 0) {
        let hrSet = actualHours <= 10 ? actualHours : 10
        if (day == 0 || day == 5 || day == 6) {
          weekendHours += hrSet
        } else {
          regularHours += hrSet
        }
        
        actualHours -= hrSet
        day == 6 ? day = 0 : day++
      }
      
      return {regularHours: regularHours, weekendHours: weekendHours}
    }catch(ex) {
      return null
    }
  }
  
  /**
   * Recalculates totals in the total rows
   */
  calculateTotals() {
    if (!this.data.scope_id) return
    
    let calculatedHours =
      Number(this.data.jini_hr_haz) +
      Number(this.data.jini_hr_set) +
      Number(this.data.jini_hr_bio) +
      Number(this.data.jini_hr_cnt) +
      Number(this.data.jini_hr_srf) +
      Number(this.data.jini_hr_cln) +
      Number(this.data.jini_hr_poe) +
      Number(this.data.jini_hr_fin) +
      Number(this.data.jini_hr_ppr)
    
    this.totalManhours.innerText = calculatedHours.toFixed(2)
    
    let crewSize = this.data.jini_num_crew
    this.totalHours.innerText = (calculatedHours / crewSize).toFixed(2)
    if(this.data.jini_yn_wknd_ovrd != 1){
      let weekendHours = this.calculateWeekendHours(calculatedHours, calculatedHours/crewSize).weekendHours * crewSize
      this.totalWeekend.value = weekendHours.toFixed(2)
    }
    
    this.totalBoxes.innerText = Number(this.data.jini_num_box_suits) + Number(this.data.jini_num_box_debris)
  }
  
  /**
   * Sets whether the form is selected/available
   * @param {Boolean} selected
   * @param {Boolean} isNew
   */
  setEnabled(selected, isNew = false) {
    this.enabled = selected
    
    //used to only use for a new item
    //JobScopeDiscountItem handles page load discount items
    if (isNew) this.discountItem.setDiscountForm(true, 0)
    
    if (!this.enabled) {
      this.discountItem.setDiscountType('NULL')
      this.discountItem.setDiscountForm(false, 0)
    }
    
    //clear/disable fields
    this.baseElement.querySelectorAll('input').forEach((element) => {
      if (element.getAttribute('name') != 'enable-scope') {
        //disable/enable the form
        switch (element.name) {
          case 'jini_yn_guarantee':
            //conditionally set satisfaction to disabled
            if (this.data.jini_scope === null || this.data.jini_scope == 3 || this.data.jini_scope == 4) {
              this.baseElement.querySelector("[name='jini_yn_guarantee']").disabled = !selected
            }
            break
          case 'jini_is_iicrc':
            this.baseElement.querySelector('[name=jini_is_iicrc]').disabled = true
            break
          case 'jini_hr_wknd':
            element.disabled = !selected || this.data.jini_yn_wknd_ovrd == 0
            break
          case 'jini_yn_wknd_ovrd':
            element.disabled = !selected
            break
          default:
            element.disabled = !selected
            break
        }
        
        //clear the form fields if needed
        if (!selected) this.clearElement(element)
      }
    })
    
    //reset total fields if necessary
    if (!selected) {
      this.totalHours.innerText = '0'
      this.totalManhours.innerText = '0'
      this.totalBoxes.innerText = '0'
    }
  }
  
  /**
   * Clears input values from an element
   * @param {HTMLElement} element
   */
  clearElement(element) {
    if (element.type == 'checkbox') element.checked = false
    else element.value = ''
  }
  
  /**
   * Clears the form of any input data entered
   */
  clearForm() {
    this.baseElement.querySelectorAll('input').forEach((element) => {
      if (element.getAttribute('name') != 'enable-scope') {
        this.clearElement(element)
      }
    })
  }
}
