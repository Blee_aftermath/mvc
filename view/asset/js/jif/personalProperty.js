import { DisclosureItem } from './disclosureItem.js'
import { RemediationItem } from './remediationItem.js'
import { DisposalItem } from './disposalItem.js'

/**
 * Personal property section
 * @author Jake Cirino
 */
export class PersonalProperty {
  /**
   * Creates a new personal property object
   * @param {JSON} data
   */
  constructor(data) {
    this.data = data
    this.disclosureItems = []
    this.remediationItems = []
    this.biohazardItems = []
    this.generalItems = []

    this.baseElement = document.createElement('div')

    this.populate()
  }

  async populate() {
    //load html template
    let template = await window.templateController.getTemplate('jif/personalProperty')
    this.baseElement.innerHTML = template

    //setup disclosure
    this.disclosure = this.baseElement.querySelector("[name='disclosure']")
    this.disclosureItemsElement = this.disclosure.querySelector('.table-input')
    this.disclosureDescriptionField = this.disclosureItemsElement.querySelector("[name='description']")
    this.disclosureLocationField = this.disclosureItemsElement.querySelector("[name='location']")
    this.disclosureFoundField = this.disclosureItemsElement.querySelector("[name='itemFound']")
    this.disclosureGivenField = this.disclosureItemsElement.querySelector("[name='itemGivenTo']")

    this.disclosureAddButton = this.disclosureItemsElement.querySelector("[type='button']")
    this.disclosureAddButton.addEventListener(
      'click',
      (() => {
        this.addDisclosureItem()
        this.disclosureDescriptionField.focus()
      }).bind(this)
    )

    //populate disclosure items
    this.data.disclosureItems.forEach(itemData => {
      let disclosureItem = new DisclosureItem(this, itemData)
      this.disclosureItems.push(disclosureItem)
    })

    //setup remediation
    this.remediationElement = this.baseElement.querySelector("[name='remediation']")
    this.remediationItemsElement = this.remediationElement.querySelector('.table-input')
    this.remediationDescriptionField = this.remediationItemsElement.querySelector("[name='description']")
    this.remediationLocationField = this.remediationItemsElement.querySelector("[name='location']")

    this.remediationAddButton = this.remediationItemsElement.querySelector("[type='button']")
    this.remediationAddButton.addEventListener('click', (() => {
      this.addRemediationItem()
      this.remediationDescriptionField.focus()
    }).bind(this))

    //populate remediation items
    this.data.remediationItems.forEach(itemData => {
      let remediationItem = new RemediationItem(this, itemData)
      this.remediationItems.push(remediationItem)
    })

    //setup biohazard disposal
    this.biohazardElement = this.baseElement.querySelector("[name='biohazard']")
    this.biohazardItemsElement = this.biohazardElement.querySelector('.table-input')
    this.biohazardDescriptionField = this.biohazardItemsElement.querySelector("[name='description']")
    
    this.biohazardAddButton = this.biohazardItemsElement.querySelector("[type='button']")
    this.biohazardAddButton.addEventListener('click', (() => {
      this.addBiohazardItem()
      this.biohazardDescriptionField.focus()
    }).bind(this))

    //setup general disposal
    this.generalElement = this.baseElement.querySelector("[name='general']")
    this.generalItemsElement = this.generalElement.querySelector('.table-input')
    this.generalDescriptionField = this.generalItemsElement.querySelector("[name='description']")

    this.generalAddButton = this.generalItemsElement.querySelector("[type='button']")
    this.generalAddButton.addEventListener('click', (() => {
      this.addGeneralItem()
      this.generalDescriptionField.focus()
    }).bind(this))

    //populate disposal items
    this.data.disposalItems.forEach(itemData => {
      let item = new DisposalItem(this, itemData)
      if(itemData.ppdisp_bio_gen == 1){
        this.biohazardItems.push(item)
      }else{
        this.generalItems.push(item)
      }
    })

    //append to document
    document.querySelector('.body_jif_l').appendChild(this.baseElement)
  }

  /**
   * Removes all the disclosure items
   */
  clearDisclosureItems(){
    for (const key in this.disclosureItems) {
      if (this.disclosureItems.hasOwnProperty(key)) {
        let element = this.disclosureItems[key];
        element.baseElement.remove()
      }
    }
    this.disclosureItems = []
  }

  /**
   * Removes all the remediation items
   */
  clearRemediationItems(){
    for (const key in this.remediationItems) {
      if (this.remediationItems.hasOwnProperty(key)) {
        let element = this.remediationItems[key];
        element.baseElement.remove()
      }
    }
    this.remediationItems = []
  }

  /**
   * Removes all the disposal items
   */
  clearDisposalItems(){
    this.clearBiohazardItems()
    this.clearGeneralItems()
  }

  /**
   * Removes all the biohazard disposal items
   */
  clearBiohazardItems(){
    for (const key in this.biohazardItems) {
      if (this.biohazardItems.hasOwnProperty(key)) {
        const element = this.biohazardItems[key];
        element.baseElement.remove()
      }
    }
    this.biohazardItems = []
  }

  /**
   * Removes all the general disposal items
   */
  clearGeneralItems(){
    for (const key in this.generalItems) {
      if (this.generalItems.hasOwnProperty(key)) {
        const element = this.generalItems[key];
        element.baseElement.remove()
      }
    }
    this.generalItems = []
  }

  /**
   * Adds a disclosure item
   * @returns {any} JSON data for item if added, false if failed
   */
  addDisclosureItem() {
    let data = {
      ppdisc_dsca: this.disclosureDescriptionField.value,
      ppdisc_loc: this.disclosureLocationField.value,
      ppdisc_is_found: this.disclosureFoundField.checked,
      ppdisc_givento: this.disclosureGivenField.value,
    }

    if (data.ppdisc_dsca.length >= 3) {
      fetch('./personalPropertyPRG.php', {
        method: 'post',
        headers: { 'Content-Type': 'application/json;charset=utf8' },
        body: JSON.stringify({
          action: 'addDisclosure',
          jobID: this.data.jobID,
          data: data,
        })
      })
      .then(res => res.json())
      .then(body => {
        if(body.ok){
          //clear old items
          this.clearDisclosureItems()
          window.jifController.update()

          //add new items
          body.msg.forEach(itemData => {
            let disclosureItem = new DisclosureItem(this, itemData)
            this.disclosureItems.push(disclosureItem)
          })

          //clear input fields
          this.disclosureDescriptionField.value = ''
          this.disclosureLocationField.value = ''
          this.disclosureFoundField.checked = false
          this.disclosureGivenField.value = ''
        }
      })
    } else return false
  }

  /**
   * Adds a remediation item
   * @returns {any} JSON data for item if added, flase if failed
   */
  addRemediationItem(){
    let data = {
      ppreme_dsca: this.remediationDescriptionField.value,
      ppreme_loc: this.remediationLocationField.value
    }

    if(data.ppreme_dsca.length >= 3){
      fetch('./personalPropertyPRG.php', {
        method: 'post',
        headers: {'Content-Type': 'application/json;charset=utf8'},
        body: JSON.stringify({
          action: 'addRemediation',
          jobID: this.data.jobID,
          data: data
        })
      })
      .then(res => res.json())
      .then(body => {
        if(body.ok){
          //clear old items
          this.clearRemediationItems()
          window.jifController.update()

          //add new items
          body.msg.forEach(itemData => {
            let remediationItem = new RemediationItem(this, itemData)
            this.remediationItems.push(remediationItem)
          })

          //clear input fields
          this.remediationDescriptionField.value = ''
          this.remediationLocationField.value = ''
        }
      })
    } else return false
  }

  /**
   * Adds a biohazard disposal item
   * @returns {any} JSON data for item if added, false if failed
   */
  addBiohazardItem(){
    let data = {
      ppdisp_dsca: this.biohazardDescriptionField.value,
      ppdisp_bio_gen: 1
    }

    if(data.ppdisp_dsca.length >= 3){
      fetch('./personalPropertyPRG.php', {
        method: 'post',
        headers: {'Content-Type': 'application/json;charset=utf8'},
        body: JSON.stringify({
          action: 'addDisposal',
          jobID: this.data.jobID,
          data: data
        })
      })
      .then(res => res.json())
      .then(body => {
        if(body.ok){
          //clear old items
          this.clearDisposalItems()
          window.jifController.update()

          //add new items
          body.msg.forEach(itemData => {
            let item = new DisposalItem(this, itemData)
            if(itemData.ppdisp_bio_gen == 1){
              this.biohazardItems.push(item)
            }else{
              this.generalItems.push(item)
            }
          })

          //clear input fields
          this.biohazardDescriptionField.value = ''
        }
      })
    } else return false
  }

  /**
   * Adds a general disposal item
   * @returns {any} JSON data for item if added, false if failed
   */
  addGeneralItem(){
    let data = {
      ppdisp_dsca: this.generalDescriptionField.value,
      ppdisp_bio_gen: 0
    }

    if(data.ppdisp_dsca.length >= 3){
      fetch('./personalPropertyPRG.php', {
        method: 'post',
        headers: {'Content-Type': 'application/json;charset=utf8'},
        body: JSON.stringify({
          action: 'addDisposal',
          jobID: this.data.jobID,
          data: data
        })
      })
      .then(res => res.json())
      .then(body => {
        if(body.ok){
          //clear old items
          this.clearDisposalItems()
          window.jifController.update()

          //add new items
          body.msg.forEach(itemData => {
            let item = new DisposalItem(this, itemData)
            if(itemData.ppdisp_bio_gen == 1){
              this.biohazardItems.push(item)
            }else{
              this.generalItems.push(item)
            }
          })

          //clear input fields
          this.generalDescriptionField.value = ''
        }
      })
    } else return false
  }
}
