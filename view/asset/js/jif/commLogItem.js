import { YNModal } from '../ynModal.js'
import { CommLogModal } from '../jif/commLogModal.js'
import { Util } from '../util.js'

/**
 * Creates/represents an item/note in the CommLog
 * @author Jake Cirino
 */
export class CommLogItem {
  /**
   * Creates a new CommLogItem
   * @param {CommLog} parent The parent container
   * @param {JSON} data The data for this commlogitem
   * @param {JSON} timezoneData
   */
  constructor(parent, data, timezoneData) {
    //set values
    this.parent = parent
    this.data = data
    this.timezoneData = timezoneData

    //create dom elements
    this.baseElement = document.createElement('div')
    this.baseElement.className = 'com-item'
  }

  /**
   * Populates the commlog item
   */
  async populate() {
    //conditionally load template
    let template
    switch (this.data.noteType) {
      case 'jif':
        template = 'jif/commLogJifItem'
        break
      case 'csr':
        template = 'jif/commLogCsrItem'
        break
      case 'crem':
        template = 'jif/commLogCremItem'
        break
      default: // job, eqsJob, resJOB
        template = 'jif/commLogJobItem'
        break
    }
    this.baseElement.innerHTML = await window.templateController.getTemplate(template)

    //TODO select/set elements
    this.noteElement = this.baseElement.querySelector("[name='noteID']")
    this.nameDate = this.baseElement.querySelector("[name='nameDate']")
    this.noteBody = this.baseElement.querySelector('.com-body')

    //set name date
    this.nameDate.innerHTML = `${this.data.employeeFirst} ${this.data.employeeLast} - ${this.data.dateAdded}`

    //set note id
    this.noteElement.innerText = this.data.noteID

    //set note body
    if (this.data.noteType != 'crem') this.noteBody.innerHTML = Util.prepareWeb(this.data.noteBody)

    //note type specific code
    switch (this.data.noteType) {
      case 'jif':
        //set new namedate
        this.noteElement.innerHTML += ` - Com Date: ${this.data.dateAddedCom}`

        //setup icons
        this.icons = this.baseElement.querySelector("[name='icons']")
        this.deleteIcon = this.icons.querySelector("[name='delete']")
        this.editIcon = this.icons.querySelector("[name='edit']")

        this.editIcon.addEventListener('click', this.editLog.bind(this))
        this.deleteIcon.addEventListener('click', this.deleteLog.bind(this))

        //hide icons if the commlog is uneditable
        if (this.data.job_note_editable == 0) {
          this.icons.style.display = 'none'
        }
        break
      case 'csr':
        //setup elements
        this.status = this.baseElement.querySelector("[name='status']")
        this.activity = this.baseElement.querySelector("[name='activity']")

        //set element displays
        this.status.innerText = this.data.statusDesc
        this.activity.innerText = this.data.activityDesc
        break
      case 'crem':
        //setup elemenets
        this.cremLink = this.baseElement.querySelector("[name='cremLink']")
        this.cremLink.href = `crem.php?id=${this.data.noteID}`
        break
    }

    //append to parent
    this.parent.itemBody.appendChild(this.baseElement)
  }

  /**
   * Function that is called when the edit icon is pressed
   */
  editLog() {
    new CommLogModal(this.data, this.timezoneData, (result) => {
      if (result !== null) {
        fetch('commLogPRG.php', {
          method: 'post',
          headers: { 'Content-Type': 'application/json;charset=utf8' },
          body: JSON.stringify({
            action: 'update',
            jobID: this.parent.jobID,
            noteID: this.data.noteID,
            body: result.body,
            date: result.date,
            time: result.time,
            timeOffset: -6 - Number(result.timezoneOffset),
          }),
        })
          .then((res) => res.json())
          .then((body) => {
            if (body.ok) this.parent.update(body.msg)
          })
      }
    })
  }

  /**
   * Function that is called when the delete icon is pressed
   */
  deleteLog() {
    new YNModal('Delete CommLog', 'Are you sure you wish to delete this CommLog?', (result) => {
      if (result) {
        fetch('commLogPRG.php', {
          method: 'post',
          headers: { 'Content-Type': 'application/json;charset=utf8' },
          body: JSON.stringify({
            action: 'delete',
            jobID: this.parent.jobID,
            noteID: this.data.noteID,
          }),
        })
          .then((res) => res.json())
          .then((body) => {
            if (body.ok) this.parent.update(body.msg)
          })
      }
    })
  }
}
