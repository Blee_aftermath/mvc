import { Util } from "../util.js"

/**
 * A modal for editing commlog items
 * @author Jake Cirino
 */
export class CommLogModal {
  /**
   * Creates a new commlog modal
   * @param {JSON} data
   * @param {JSON} timezoneData
   * @param {Function} callback
   */
  constructor(data, timezoneData, callback) {
    this.data = data
    this.callback = callback
    this.timezoneData = timezoneData

    //create base div
    this.baseElement = document.createElement('div')
    this.baseElement.className = 'modal'
    this.baseElement.id = 'modal-commlog-tmp'
    this.baseElement.style.display = 'block'
    this.populate()
  }

  /**
   * Populates the modal html
   */
  async populate() {
    let template = await window.templateController.getTemplate('jif/commLogModal')
    this.baseElement.innerHTML = template

    //populate fields with commlog data
    let textarea = this.baseElement.querySelector('textarea')
    textarea.value = this.data.noteBody

    let splitDatetime = Util.shiftTimezone(this.data.dateAddedCom, this.timezoneData.tz_offset).split(' ')
    let dateElement = this.baseElement.querySelector("[name='comm-datepicker']")
    dateElement.value = splitDatetime[0]

    let timeElement = this.baseElement.querySelector('select')
    timeElement.value = splitDatetime[1]

    let timezoneElement = this.baseElement.querySelector("[name='timezone']")
    timezoneElement.innerText = this.timezoneData.tz

    //setup cancel buttons
    this.baseElement.querySelectorAll("[name='action-close']").forEach((element) => {
      element.onclick = (() => {
        this.close(null)
      }).bind(this)
    })

    //setup save button
    this.baseElement.querySelector("input[type='submit']").onclick = ((event) => {
      //TODO
      event.preventDefault()
      this.close({
        body: textarea.value,
        date: dateElement.value,
        time: timeElement.value,
        timezoneOffset: this.timezoneData.tz_offset
      })
    }).bind(this)

    //append modal to document
    document.body.appendChild(this.baseElement)

    //setup datepicker
    $(document).ready(function () {
      $(function () {
        $("[name|='comm-datepicker']").datepicker()
      })
    })
  }

  close(result) {
    this.baseElement.remove()
    this.callback(result)
  }
}
