const State = {
  ACTIVE: 'active',
  AVAILABLE: 'avail',
  UNAVAILABLE: 'future',
}

/**
 * An item for the phase list.
 *
 * This class functions similar to a reverse linked list,
 * where each element contains a reference to the previous.
 * This is because the state of each PhaseItem depends on the
 * amount of checks passed in the previous phase.
 * @author Jake Cirino
 */
export class PhaseListItem {
  /**
   * Creates a new phaselist menu item
   * @param {Object} parent The parent object, the phaseList containing this item
   * @param {JSON} data The item data
   * @param {Object} tail Points to the previous object in the phase list
   */
  constructor(parent, data, tail) {
    this.parent = parent
    this.tail = tail

    this.baseElement = document.createElement('li')

    this.linkElement = document.createElement('a')
    this.linkElement.href = 'jif.php?job=' + parent.jobID + '&phase=' + data.phaseCode
    this.linkElement.text = data.title

    this.textElement = document.createElement('div')
    this.textElement.innerText = data.title

    //set and update data
    this.update(data)

    this.baseElement.appendChild(this.linkElement)
    this.baseElement.appendChild(this.textElement)
    parent.baseElement.appendChild(this.baseElement)
  }

  /**
   * Sets the state of this item
   * @param {State} state The state to set the item to
   */
  setState(state) {
    //change base styling
    this.baseElement.className = state

    //change whether element is clickable
    this.textElement.style.display = state == State.UNAVAILABLE ? 'block' : 'none'
    this.linkElement.style.display = state == State.UNAVAILABLE ? 'none' : 'block'

    this.state = state
  }

  /**
   * Called on update, resets data params
   */
  update(data) {
    this.data = data

    //set state
    if (this.parent.data.phase == data.phaseCode) this.setState(State.ACTIVE)
    else if (this.tail === undefined || (this.tail.data.checksFailed == 0 && this.tail.state != State.UNAVAILABLE))
      this.setState(State.AVAILABLE)
    else this.setState(State.UNAVAILABLE)
  }
}
