import { ChecklistItem } from './checklistItem.js'
import { ChecklistDocuments, PhaseDocuments } from './checklistDocuments.js'
import { SidebarHoursCheck } from './sidebarHoursCheck.js'
import { SidebarCostCheck } from './sidebarCostCheck.js'
import { SidebarAdmin } from './sidebarAdmin.js'

/**
 * Sidebar for the jif page
 * @author Jake Cirino
 */
export class JifSidebar {
  /**
   * Creates a new checklist menu
   * @param {Array} data The data to populate the checklist with
   */
  constructor(data) {
    this.data = data
    this.phaseID = data.phaseID
    this.jobID = data.jobID
    this.items = {}
    //console.log(data)

    //create base element
    this.baseElement = document.createElement('div')
    this.baseElement.className = 'jif-checklist'
    this.baseElement.innerHTML = this.data.template

    //select/set elements
    this.checklistTitle = this.baseElement.querySelector("[name='checklist']")
    this.documentsTitle = this.baseElement.querySelector("[name='documents']")

    //set checklist title
    this.checklistTitle.innerHTML = `${this.data.phaseData[this.phaseID - 1].phase_dsca} Checklist`.toUpperCase()

    //insert checklist items from data
    if (data.items !== undefined && data.items.length > 0) {
      data.items.forEach((element) => {
        let item = new ChecklistItem(this, element)
        this.items[element.checkID] = item
      })
    } else {
      //hide checklist section if there are no items
      this.checklistTitle.style.display = 'none'
    }

    //documents section, only display if there are phase documents for this phase
    if (PhaseDocuments[this.phaseID - 1].length > 0) {
      let documentsHeader = document.createElement('h1')
      documentsHeader.innerText = 'DOCUMENTS'
      this.baseElement.appendChild(documentsHeader)

      this.documents = new ChecklistDocuments(this.baseElement, data)
    }
    
    //display lock/unlock section
    this.admin = new SidebarAdmin(this, this.data)

    //hours/cost check. only show on phase >= 3
    if (this.phaseID >= 3) {
      //create sidebar margin check
      //TODO non-null data
      //setup estimate/final data array for each sidebar item
      let scopeData = {
        primaryJini: this.data.primaryJini,
        primaryInvType: this.data.primaryInvType,
        primaryEstimate: this.data.primaryEstimate,
        primaryFinal: this.data.primaryFinal,
        secondaryJini: this.data.secondaryJini,
        secondaryInvType: this.data.secondaryInvType,
        secondaryEstimate: this.data.secondaryEstimate,
        secondaryFinal: this.data.secondaryFinal,
        crewSize: this.data.jobInfo.jif_num_crew,
        jobDays: this.data.jobInfo.jif_num_days,
      }

      //add sidebar items
      this.hoursCheck = new SidebarHoursCheck(this, this.data.hoursCheckTemplate, scopeData)
      this.costCheck = new SidebarCostCheck(this, this.data.costCheckTemplate, scopeData)
    }

    //append base element to the dom
    document.querySelector('[jif-bottom]').appendChild(this.baseElement)

    //register with the jif controller
    window.jifController.registerComponent(this)
  }

  /**
   * Calls the jobCheckPRG and updates the checklist DOM.
   * This is called by the JifController on page update.
   * @param {Function} callback The callback for when the update is completed
   */
  update(callback) {
    //update checklist
    fetch('jobCheckPRG.php', {
      method: 'post',
      headers: { 'Content-Type': 'application/json;charset=utf8' },
      body: JSON.stringify({
        action: 'check',
        jobID: this.data.jobID,
        phaseID: this.data.phaseID,
        jobClass: this.data.jobClass
      }),
    })
      .then((res) => {
        return res.ok ? res.json() : res
      })
      .then((body) => {
        if (body.data !== undefined) {
          //update each checklist item with our new data
          console.log(body.data)
          console.log(this.items)
          body.data.forEach((element) => {
            this.items[element.checkID].setIsPass(element.isPass)
          })

          callback()
        }
      })

    //update admin panel
    this.admin.update()

    //update sidebar items for phase > 3
    if (this.phaseID >= 3) {
      fetch('sidebarCheckPRG.php', {
        method: 'post',
        headers: { 'Content-Type': 'application/json;charset=utf8' },
        body: JSON.stringify({
          action: 'getData',
          jobID: this.data.jobID,
        }),
      })
        .then((res) => res.json())
        .then((body) => {
          if (body.ok) {
            this.hoursCheck.data = body.data
            this.hoursCheck.update()

            //TODO do we need to add secondary estimate to this data?
            this.costCheck.data = body.data
            this.costCheck.data.primaryFinal.jfin_calc_labor = body.data.primaryFinal.jfin_calc_sub_labor
            this.costCheck.data.primaryJini = this.costCheck.calculateCosts(this.costCheck.data.primaryJini)

            //setup secondary estimate data
            if (body.data.secondaryEstimate != null) {
              this.costCheck.data.secondaryFinal.jfin_calc_labor = body.data.secondaryFinal.jfin_calc_sub_labor
            }

            this.costCheck.update()
          }
        })
    }
  }
}
