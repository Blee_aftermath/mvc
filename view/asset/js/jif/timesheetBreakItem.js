/**
 * An item in the timesheet break section
 * @author Jake Cirino
 */
export class TimesheetBreakItem{
  /**
   * Creates a new timesheet break item
   * @param {Timesheet} parent 
   * @param {JSON} data 
   */
  constructor(parent, data){
    this.parent = parent
    this.data = data

    this.baseElement = document.createElement('tr')
  }

  populate(){
    //load template
    this.baseElement.innerHTML = window.breakTemplate

    //select/set field values
    this.dayField = this.baseElement.querySelector("[name='tsdy_dy']")
    this.empField = this.baseElement.querySelector("[name='tsdy_emp']")
    this.hourField = this.baseElement.querySelector("[name='tsdy_calc_hr']")
    this.reqBreakField = this.baseElement.querySelector("[name='tsdy_calc_break']")
    this.breakField = this.baseElement.querySelector("[name='tsdy_num_break']")
    this.reqLunchField = this.baseElement.querySelector("[name='tsdy_calc_lunch']")
    this.lunchField = this.baseElement.querySelector("[name='tsdy_num_lunch']")
    
    //update data on change
    this.breakField.addEventListener('change', ((event) => {
      this.data.tsdy_num_break = Number.parseInt(event.target.value)
    }))

    //populate data
    this.populateData()

    //append to table
    this.parent.baseElement.querySelector("[name='break-body']").appendChild(this.baseElement)
  }

  /**
   * Populates fields with the current data
   */
  populateData(){
    this.dayField.innerText = this.data.tsdy_dy
    this.empField.innerText = `${this.data.emp_fname} ${this.data.emp_lname}`
    this.hourField.innerText = this.data.tsdy_calc_hr
    this.reqBreakField.innerText = this.data.tsdy_calc_break
    this.breakField.value = this.data.tsdy_num_break
    this.reqLunchField.innerText = this.data.tsdy_calc_lunch
    this.lunchField.innerText = this.data.tsdy_num_lunch
  }

  /**
   * Sets breaks/lunches to the required amount
   */
  setAllBreaksTaken(){
    this.breakField.value = this.data.tsdy_calc_break
    this.data.tsdy_num_break = Number.parseInt(this.breakField.value)
  }

  /**
   * Saves this item in the database
   */
  save(){
    fetch('timesheetPRG.php', {
      method: 'post',
      headers: {'Content-Type': 'application/json;charset=utf8'},
      body: JSON.stringify({
        action: 'editBreakItem',
        tsdy_id: this.data.tsdy_id,
        tsdy_num_break: this.data.tsdy_num_break
      })
    })
    .then(res => res.json())
    .then(body => {
      //window.jifController.update()
      //this.parent.removeBreakItems()
    })
  }
}