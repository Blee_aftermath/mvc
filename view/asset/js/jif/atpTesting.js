import { ATPTestingRoom } from './atpTestingRoom.js'

/**
 * Block that handles entering ATP testing data
 * @author Jake Cirino
 */
export class ATPTesting {
  /**
   * Creates a new ATP Testing section
   * @param {JSON} data
   */
  constructor(data) {
    this.data = data
    this.rooms = []
    console.log(data)

    //create base element
    this.baseElement = document.createElement('div')
    this.baseElement.innerHTML = this.data.template

    //select/set elements
    this.addButton = this.baseElement.querySelector("[name='addRoom']")
    this.blockBody = this.baseElement.querySelector('.block-body')

    //setup add button
    this.addButton.addEventListener('click', () => {
      fetch('atpPRG.php', {
        method: 'post',
        headers: { 'Content-Type': 'application/json;charset=utf8' },
        body: JSON.stringify({
          action: 'addRoom',
          jobID: this.data.jobID,
        }),
      })
        .then((res) => res.json())
        .then((body) => {
          if (body.ok) {
            this.update()

            //trigger jifcontroller update
            window.jifController.update()
          }
        })
    })

    //populate items
    this.repopulate()

    //append to document
    document.querySelector('.body_jif_l').appendChild(this.baseElement)
  }

  /**
   * Repopulates all items based on current data
   */
  repopulate() {
    //reset all items
    for (const key in this.rooms) {
      if (this.rooms.hasOwnProperty(key)) {
        const element = this.rooms[key]

        element.delete()
      }
    }
    this.rooms = []

    //populate room items
    for (const key in this.data.rooms) {
      if (this.data.rooms.hasOwnProperty(key)) {
        const roomData = this.data.rooms[key]

        //set data
        let data = roomData
        data.roomTemplate = this.data.roomTemplate
        data.itemTemplate = this.data.itemTemplate
        data.swabs = []

        //get all swabs for this room
        for (const key in this.data.swabs) {
          if (this.data.swabs.hasOwnProperty(key)) {
            const swabData = this.data.swabs[key]

            if (swabData.atps_test == roomData.atpt_id) data.swabs.push(swabData)
          }
        }

        //create room item
        this.rooms.push(new ATPTestingRoom(this, data))
      }
    }
  }

  /**
   * Updates all the items in the ATP block
   */
  update() {
    fetch('atpPRG.php', {
      method: 'post',
      headers: { 'Content-Type': 'application/json;charset=utf8' },
      body: JSON.stringify({
        action: 'getItems',
        jobID: this.data.jobID,
      }),
    })
      .then((res) => res.json())
      .then((body) => {
        //set data and repopulate
        this.data.rooms = body.rooms
        this.data.swabs = body.swabs
        this.repopulate()
        //trigger jifcontroller update
        window.jifController.update()
      })
  }
}
