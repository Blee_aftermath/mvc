import { Util } from "../util.js"

/**
 * Lockable tabs that are determined by whether the ini est is confirmed
 */
const INI_LOCKABLES = [
  'scope',
  'covidInitialEstimate',
  'personalProperty',
  'info'
]

/**
 * Lockable tabs that are determined by whether the fin est is confirmed
 */
const FIN_LOCKABLES = [
  'scope',
  'covidInitialEstimate',
  //'timesheet', //Temp commented out on 2021-03-17 SK per MS
  'equipment',
  'supplies',
  'atp',
  'bio',
  'wbgt',
  'changeOrder',
  'finalEstimate'
]

/**
 * Controls the admin sidebar
 * @author Jake Cirino
 */
export class SidebarAdmin{
  /**
   * Creates sidebar lock object
   * @param {JifSidebar} parent
   * @param {JSON} data 
   */
  constructor(parent, data){
    this.parent = parent
    this.data = data
    this.lockElement = document.getElementById('jifLock')
    //console.log(this.data)

    //create base element
    this.baseElement = document.createElement('div')
    this.baseElement.innerHTML = this.data.sidebarAdminTemplate

    //parse dom
    this.dom = Util.parseDOM(this.baseElement)

    //lock/unlock button initialization and functionality
    this.dom.lockButton.addEventListener('click', () => {
      fetch('sidebarAdminPRG.php', {
        method: 'post',
        headers: {'Content-Type': 'application/json;charset=utf8'},
        body: JSON.stringify({
          action: 'setLocked',
          jobID: this.data.jobID,
          locked: this.locked == 1 ? 0 : 1  
        })
      })
      .then(res => res.json())
      .then(body => {
        if(body.ok){
          console.log(this.locked)
          this.setLocked(this.locked == 1 ? 0 : 1)
        }
      })
    })

    //set initial lock state
    this.setLocked(this.data.jifLocked)

    //append to document if permissions met
    if(this.data.perms.includes('JIF_M'))
      this.parent.baseElement.appendChild(this.baseElement)
  }

  /**
   * Sets whether the jif is locked or not
   * @param {Number} locked 1 for locked, 0 for unlocked
   */
  setLocked(locked){
    this.locked = locked

    //set button text
    this.dom.lockButton.value = locked == 1 ? 'Unlock' : 'Lock'

    //set background color
    if(this.locked == 0){
      this.dom.adminPanel.style.backgroundColor = '#ff9999'
      this.dom.unlockedText.style.display = ''
    }else{
      this.dom.adminPanel.style.backgroundColor = ''
      this.dom.unlockedText.style.display = 'none'
    }

    //calculate and set lock displays depending on what page is loaded
    if(this.data.tab == 'scope' || this.data.tab == 'finalEstimate' || this.data.tab == 'covidInitialEstimate'){
      //get the lock elements from the page
      let inpLockElement = document.querySelector('[name=inpLock]')
      let saveLockElement = document.querySelector('[name=saveLock]')
      let inpLock = Number(this.data.tab == 'finalEstimate' ? this.data.finConfirmed : this.data.iniConfirmed) 
      let saveLock = this.locked * inpLock
      //console.log(inpLock)

      //set inp lock visible
      if(inpLock == 1){
        inpLockElement.classList.add('lock')
      }else{
        inpLockElement.classList.remove('lock')
      }

      //set save lock visible
      if(saveLock == 1){
        saveLockElement.classList.add('lock')
      }else{
        saveLockElement.classList.remove('lock')
      }
    }else{
      //calculate whether to lock the current tab
      let tabLock = 0
      if(INI_LOCKABLES.includes(this.data.tab)){
        tabLock = this.locked * this.data.iniConfirmed
      }else if(FIN_LOCKABLES.includes(this.data.tab)){
        tabLock = this.locked * this.data.finConfirmed
      }
      //console.log(tabLock)
  
      //set lock visible or not
      if(tabLock == 1){
        //this.lockElement.classList.add('block-lock lock')
        this.lockElement.className = 'block-lock lock'
      }else{
        //this.lockElement.classList.remove('block-lock lock')
        this.lockElement.className = ''
      }
    }
  }

  //TODO jifcontroller update
  update(){
    //get latest lock info
    fetch('sidebarAdminPRG.php', {
      method: 'post',
      headers: {'Content-Type': 'application/json;charset=utf8'},
      body: JSON.stringify({
        action: 'getLockInfo',
        jobID: this.data.jobID
      })
    })
    .then(res => res.json())
    .then(body => {
      if(body.ok){
        //set new data
        this.data.jifLocked = body.ok.jif_is_locked
        this.data.iniConfirmed = body.ok.jif_ini_is_confirmed
        this.data.finConfirmed = body.ok.jif_fin_is_confirmed

        //reset lock status
        this.setLocked(this.data.jifLocked)
      }
    })
  }
}