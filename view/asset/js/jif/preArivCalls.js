import { PreArrivalCall } from "./preArivCall.js"

/**
 * Block that contains a checklist for all pre-arrival calls to be made
 * @author Jake Cirino
 */
export class PreArrivalCalls{

  constructor(data){
    this.data = data
    this.baseElement = document.createElement('div')
    this.populate()
  }

  /**
   * Populates the pre arrival calls list
   */
  async populate(){
    //append html to base element
    let template = await window.templateController.getTemplate('jif/preArivCalls')
    this.baseElement.innerHTML = template

    //populate each caller element
    this.bodyElement = this.baseElement.querySelector('.block-body')
    if(this.data.length > 0){
      this.data.forEach(async callData => {
        await new PreArrivalCall(this, callData).populate()
      })
    }else{
      this.bodyElement.innerText = 'None'
    }
    
    //append to document
    let refNode = document.querySelector("[name='frmJifDispatch']")
    refNode.parentNode.insertBefore(this.baseElement, refNode.nextSibling)
  }

}