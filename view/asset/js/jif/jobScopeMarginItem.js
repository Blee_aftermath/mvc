/**
 * The margin section on the job scope
 * @author Scott Kiehn
 */

import {Util} from '../util.js'
import {CostMarginCalc} from './costMarginCalc.js?v5'

String.prototype.toNum = function () {return Number(this)}
Number.prototype.toNum = function () {return Number(this)}

const mnyRegEx = new RegExp(/[\$\.,]/g)

export class JobScopeMarginItem {
  /**
   * Creates a new job scope cost item
   * @param {JobScope} parent
   * @param {JSON} scopeData
   */
  constructor(parent, scopeData) {
    this.parent = parent
    this.scopeData = scopeData
    this.approval = {}
    this.initialCM = undefined
    
    const job = {scope: 0}
    console.log(scopeData)
    if (scopeData.jini_scope) {
      job.quotePrice = this.scopeData.jini_calc_sub.toNum()
      job.jobPmtInsNon = this.parent.data.jobPmtInsNon ? this.parent.data.jobPmtInsNon.toNum() : 0
      job.jifConfirmed = this.parent.data.info.jif_ini_is_confirmed.toNum()
      job.jifPerRealEst = this.parent.data.info.jif_per_real_est ? this.parent.data.info.jif_per_real_est.toNum() : null
      job.days = this.scopeData.jini_num_days.toNum()
      job.crew = this.scopeData.jini_num_crew.toNum()
      job.manHours = this.scopeData.jini_sum_hr.toNum()
      job.trucks = this.scopeData.jini_num_truck.toNum()
      job.transit = this.parent.data.info.jif_calc_hr_transit.toNum()
      job.rentals = this.scopeData.jini_num_rental.toNum()
      job.hotels = this.scopeData.jini_num_hotel.toNum()
      job.dumpsters = this.scopeData.jini_num_dumpster.toNum()
      job.bagsters = this.scopeData.jini_num_bagster.toNum()
      job.bioboxes = this.scopeData.jini_num_box_suits.toNum() + this.scopeData.jini_num_box_debris.toNum()
      job.equipment = this.scopeData.jini_calc_eqp.toNum()
      job.supplies = this.scopeData.jini_calc_sup.toNum()
      job.internalExp = Number(this.scopeData.jini_amt_int_exp)
    }
    this.cm = new CostMarginCalc({job: job, costlist: this.parent.data.costlist, realzns: this.parent.data.realzns})
    
    this.viewInfo = {}
    if (scopeData.jini_scope) {
      this.viewInfo.pricelistDsca = this.scopeData.pricelist_dsca || '&nbsp;'
      this.viewInfo.supr = 1
      this.viewInfo.tech = this.scopeData.jini_num_crew.toNum() - 1
      this.viewInfo.suprRate = this.scopeData.jini_is_iicrc == "1" ? Number(this.scopeData.jini_rate_iicrc) : Number(this.scopeData.jini_rate_sup)
      this.viewInfo.suprRateWknd = this.scopeData.jini_is_iicrc == "1" ? Number(this.scopeData.jini_wknd_iicrc) : Number(this.scopeData.jini_wknd_sup)
      this.viewInfo.techRate = Number(this.scopeData.jini_rate_tec)
      this.viewInfo.techRateWknd = Number(this.scopeData.jini_wknd_tec)
      this.viewInfo.laborLabel = this.parent.data.costlist.office_is_labor_stdprm == 2 ? 'PRE' : 'STD'
      this.viewInfo.driveLabel = this.parent.data.costlist.office_is_drive_stdprm == 2 ? 'PRE' : 'STD'
      this.viewInfo.wcLabel = this.parent.data.costlist.office_is_wc_stdprm == 2 ? 'PRE' : 'STD'
      this.viewInfo.realiznInfo = ''
      this.viewInfo.realiznInfo2 = ''
    }
    
    this.dom = {base: document.createElement('div')}
    this.dom.base.id = `scope-margin-item-${this.scopeData.scope_id}`
    this.dom.base.className = 'scope-item'
    this.dom.base.setAttribute('name', 'alt-color')
    
    // This element is in the main template & only needed to performed once
    if (window.aftermath.hasPerm(this.parent.perm.component.margin))
      this.parent.baseElement.querySelector(".scope-margin-labels").style.display = 'block'
  }
  
  async populate() {
    let template = await window.templateController.getTemplate('jif/jobScopeMarginItem')
    this.dom.base.innerHTML = template
    
    this.dom.showResult = this.dom.base.querySelector('.scope-margin-signal')
    this.dom.showResult.id = `scope-margin-signal-${this.scopeData.scope_id}`
    
    this.dom.mgnApproval = this.dom.base.querySelector('#mgn-approval')
    
    this.parent.baseElement.querySelector("[name='ini-scope-margin']").appendChild(this.dom.base)
    
    this.dom.mgnApproval.addEventListener('change', () => this.parent.enableSaveBtn())
    
    if (window.aftermath.hasPerm(this.parent.perm.component.margin))
      this.dom.base.querySelector(".scope-margin-values").style.display = 'block'
  }
  
  /**
   * Recalculates and updates the scope cost margins
   */
  calculateMargins(scopeData) {
    const scopeItem = this.parent.scopeItems[scopeData.scope_id]
    console.log(scopeItem)
    const costItem = this.parent.costItems[scopeData.scope_id]
    //console.log(scopeItem)
    
    if (!scopeData.jini_calc_tot) {
      this.dom.showResult.innerHTML = ''
      this.dom.showResult.className = 'scope-margin-signal white1'
      
      this.dom.mgnApproval.style.display = 'none'
      
      for (const el of this.dom.base.querySelectorAll('[name]'))
        el.innerText = el.innerText.match(/^\$/) ? '$0.00' : ''
      
      this.dom.base.querySelector('[name=crew]').innerText = '(0)'
      this.dom.base.querySelector('[name=supr]').innerText = 'S0 '
      this.dom.base.querySelector('[name=tech]').innerText = 'T0 '
      this.dom.base.querySelector('[name=manHours]').innerText = '0'
      this.dom.base.querySelector('[name=roundTrips]').innerText = '0'
      this.dom.base.querySelector('[name=costMargin]').innerText = '0%'
      return
    } else {
      this.dom.mgnApproval.style.display = 'block'
    }
    
    this.viewInfo.supr = 1
    this.viewInfo.tech = scopeItem.data.jini_num_crew.toNum() - 1

    this.viewInfo.pricelistDsca = scopeItem.data.pricelist_dsca || '&nbsp;'

    let supRate = scopeItem.data.jini_is_iicrc == "1" ? scopeItem.data.jini_rate_iicrc : scopeItem.data.jini_rate_sup
    let supRateWknd = scopeItem.data.jini_is_iicrc == "1" ? scopeItem.data.jini_wknd_iicrc : scopeItem.data.jini_wknd_sup
    this.viewInfo.suprRate = Number(supRate)
    this.viewInfo.suprRateWknd = Number(supRateWknd)
    this.viewInfo.techRate = Number(scopeItem.data.jini_rate_tec)
    this.viewInfo.techRateWknd = Number(scopeItem.data.jini_wknd_tec)
    
    // Update costMargin job data
    this.cm.job.quotePrice = costItem.totalElement.innerText.replace(mnyRegEx, '').toNum()
    this.cm.job.days = scopeItem.numDays.value.toNum()
    this.cm.job.crew = scopeItem.numCrew.value.toNum()
    this.cm.job.manHours = scopeItem.totalManhours.innerText.toNum()
    this.cm.job.trucks = scopeItem.numTruck.value.toNum()
    this.cm.job.transit = this.parent.data.info.jif_calc_hr_transit.toNum()
    this.cm.job.rentals = scopeItem.numRental.value.toNum()
    this.cm.job.hotels = scopeItem.numHotel.value.toNum()
    this.cm.job.dumpsters = scopeItem.numDumpster.value.toNum()
    this.cm.job.bagsters = scopeItem.numBagster.value.toNum()
    this.cm.job.bioboxes = scopeItem.totalBoxes.innerText.toNum()
    this.cm.job.equipment = costItem.equipUsageElement.innerText.replace(mnyRegEx, '').toNum()
    this.cm.job.supplies = costItem.supplyUsageElement.innerText.replace(mnyRegEx, '').toNum()
    this.cm.job.internalExp = Number(scopeItem.data.jini_amt_int_exp)
    
    // Run calculations
    this.cm.calcMargins()
    
    //The approver selects are built here, not in the constructor.
    //Unlike in discounts, this cost margin is not in the DB, but calculated.
    //However, the approver is in the jif_initial, but not their role (JA2 or 1).
    //But, the DB data for the initial calc on load will pretty much be the
    //the "saved calc." Adding a former employee here and only for the initial
    //calc will keep them in the proper list (JA2 or 1) for the dynamic use.
    if (!this.initialCM) {
      this.initialCM = this.cm.job.costMargin
      
      //Employee may no longer be active, add into the approval object
      const formerEmp = this.scopeData.jini_mgn_emp_appr
        ? {[this.scopeData.jini_mgn_emp_appr]: this.scopeData.jini_mgn_emp_name}
        : {}
      
      //Clone the parent object to get a copy, not reference.
      //Also, only add former employee to proper margin
      if (this.cm.signal.text == 'Stop') {
        this.approval.JA2 = Object.assign(formerEmp, this.parent.approval.JA2)
        this.approval.JA1 = Object.assign({}, this.parent.approval.JA1)
      } else if (this.cm.signal.text == 'Consult') {
        this.approval.JA2 = Object.assign({}, this.parent.approval.JA2)
        this.approval.JA1 = Object.assign(formerEmp, this.parent.approval.JA1)
      } else {
        this.approval.JA2 = Object.assign({}, this.parent.approval.JA2)
        this.approval.JA1 = Object.assign({}, this.parent.approval.JA1)
      }
    }
    
    // Display cost margin signals
    this.dom.showResult.innerHTML = this.cm.signal.text
    this.dom.showResult.className = `scope-margin-signal ${this.cm.signal.color}`
    
    //provide approval selects if needed
    //boom represents a change in state as the below should only run
    //if a change is signal state, not every time
    if (this.cm.job.boom) {
      if (this.cm.job.ok) {
        this.dom.mgnApproval.innerHTML = ''
        this.dom.mgnApproval.disabled = true
      } else {
        this.buildApprovers(scopeData)
      }
    }
    
    // Make the realization tooltip visible
    this.dom.base.querySelector('.tooltip').style.display = 'inline'
    
    // The html area is display none, but values added to the DOM as they are needed by final total column
    for (const el of this.dom.base.querySelectorAll('[name]')) {
      const name = el.getAttribute('name')
      let text = 0
      
      if (this.cm.costlist[name]) {
        text = this.cm.costlist[name]
      } else if (this.cm.job[name]) {
        text = this.cm.job[name]
      } else if (this.cm.expense[name]) {
        text = this.cm.expense[name]
      } else if (this.viewInfo[name]) {
        text = this.viewInfo[name]
      }

      switch (true) {
        case el.dataset.at == 1:
          el.innerHTML = `@${text}`
          break
        case el.dataset.text == 1:
          el.innerHTML = text
          break
        case el.dataset.hour == 1:
          el.innerHTML = `${text}h`
          break
        case el.dataset.percent == 1:
          el.innerHTML = `${text}%`
          break
        case el.dataset.paran == 1:
          el.innerHTML = `(${text})`
          break
        case el.dataset.name == 1:
          el.innerHTML = `${name.charAt(0).toUpperCase()}${text} `
          break
        case el.dataset.regx == 1:
          el.innerHTML = text.replace(/[ -]/g, '')
          break
        default:
          el.innerHTML = Util.formatCurrency(text)
      }

    }
  }
  
  /**
   * Builds the approval selects
   */
  buildApprovers(scopeData) {
    const approval = Object.assign({}, this.approval[this.cm.job.approval])
    
    this.dom.mgnApproval.innerHTML = '<option value="0"></option>'
    
    for (const a in approval) {
      const option = document.createElement('option')
      option.value = a
      
      if (scopeData.jini_mgn_emp_appr && a == scopeData.jini_mgn_emp_appr)
        option.selected = true
      
      option.innerText = approval[a]
      this.dom.mgnApproval.appendChild(option)
    }
    this.dom.mgnApproval.disabled = false
  }
  
  /**
   * Gets the margin approver
   */
  getMarginApprover() {
    return this.dom.mgnApproval.value > 0 ? this.dom.mgnApproval.value : null
  }
}
