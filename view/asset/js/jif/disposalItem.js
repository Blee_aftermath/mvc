/**
 * An entry in the disposal section of the personal property tab
 * @author Jake Cirino
 */
export class DisposalItem {
  /**
   * Creates a new personal property disposal item
   * @param {PersonalProperty} parent
   * @param {JSON} data
   */
  constructor(parent, data) {
    this.parent = parent
    this.data = data
    this.editMode = false
    this.disposalType = data.ppdisp_bio_gen

    //create base element
    this.baseElement = document.createElement('tr')

    this.populate()
  }

  async populate() {
    //load template
    let template = await window.templateController.getTemplate('jif/disposalItem')
    this.baseElement.innerHTML = template

    //select/set elements
    this.description = this.baseElement.querySelector("[name='description']")
    this.buttons = this.baseElement.querySelector("[name='buttons']")
    this.descriptionEdit = this.baseElement.querySelector("[name='descriptionEdit']")
    this.descriptionField = this.descriptionEdit.querySelector("[name='descriptionField']")
    this.saveEdit = this.baseElement.querySelector("[name='saveEdit']")

    //setup buttons
    this.editButton = this.buttons.querySelector("[name='editButton']")
    this.editButton.addEventListener(
      'click',
      (() => {
        this.setEditMode(true)
      }).bind(this)
    )

    this.deleteButton = this.buttons.querySelector("[name='deleteButton']")
    this.deleteButton.addEventListener('click', () => {
      this.deleteDisposalItem()
    })

    this.saveButton = this.saveEdit.querySelector("[name='saveButton']")
    this.saveButton.addEventListener(
      'click',
      (() => {
        this.updateDisposalItem()
      }).bind(this)
    )

    //get parent table to append to
    if (this.data.ppdisp_bio_gen == 1) {
      this.parentTable = this.parent.biohazardElement
      this.parentItems = this.parent.biohazardItemsElement
    } else {
      this.parentTable = this.parent.generalElement
      this.parentItems = this.parent.generalItemsElement
    }

    //initial data population
    this.setData(this.data.ppdisp_dsca)

    //append to parent
    this.parentTable.insertBefore(this.baseElement, this.parentItems)
  }

  /**
   * Sets the data
   * @param {String} description
   */
  setData(description) {
    //set data
    this.data.ppdisp_dsca = description

    //set display
    this.description.innerText = description
    this.descriptionField.value = description
  }

  /**
   * Sets this item to edit mode
   * @param {Boolean} editMode
   */
  setEditMode(editMode) {
    this.editMode = editMode

    let infoDisplay = editMode ? 'none' : ''
    let editDisplay = editMode ? '' : 'none'

    this.baseElement.className = editMode ? 'table-input' : ''

    //set info elements to display
    this.description.style.display = infoDisplay
    this.buttons.style.display = infoDisplay

    //set edit items to display
    this.descriptionEdit.style.display = editDisplay
    this.saveEdit.style.display = editDisplay
  }

  /**
   * Update this disposal item in the database
   */
  updateDisposalItem(){
    let data = {
      ppdisp_id: this.data.ppdisp_id,
      ppdisp_bio_gen: this.data.ppdisp_bio_gen,
      ppdisp_dsca: this.descriptionField.value
    }

    fetch('./personalPropertyPRG.php', {
      method: 'post',
      headers: {'Content-Type': 'application/json;charset=utf8'},
      body: JSON.stringify({
        action: 'editDisposal',
        data: data
      })
    })
    .then(res => res.json())
    .then(body => {
      if(body.ok){
        this.setData(data.ppdisp_dsca)

        this.setEditMode(false)
      }
    })
  }

  /**
   * Delete this disposal item in the database
   */
  deleteDisposalItem(){
    fetch('./personalPropertyPRG.php', {
      method: 'post',
      headers: {'Content-Type': 'application/json;charset=utf8'},
      body: JSON.stringify({
        action: 'deleteDisposal',
        itemID: this.data.ppdisp_id
      })
    })
    .then(res => res.json())
    .then(body => {
      if(body.ok){
        window.jifController.update()
        this.baseElement.remove()

        //remove from parent array
        for (const key in this.parent.disposalItems) {
          if (this.parent.disposalItems.hasOwnProperty(key)) {
            const element = this.parent.disposalItems[key]
            if(element === this)
              this.parent.disposalItems.splice(key, 1)
          }
        }
      }
    })
  }
}
