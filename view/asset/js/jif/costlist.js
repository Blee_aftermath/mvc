import { default as note } from '../modules/lib/note.js'
import { message } from '../modules/lib/util.js'

const costlistForm = document.getElementById('costlistForm'),
      datePicker = document.getElementById('costlistDatePicker'),
      minDate = new Date((datePicker.dataset.epoch * 1000) + (60 * 60 * 24 * 1000))

$(datePicker).datepicker({
  dateFormat: 'mm/dd/y',
  minDate: minDate
})

costlistForm.addEventListener('change', ev => formEvents(ev.target))
costlistForm.addEventListener('click',  ev => formEvents(ev.target))

const formEvents = el => {

  if (el.matches('[type=number]')) {
    if (el.step == '0.01')
      el.value = (Math.round(el.value * 100) / 100).toFixed(2)
    else if (el.step == '0.1')
      el.value = (Math.round(el.value * 100) / 100).toFixed(1)
    else
      el.value = (Math.round(el.value * 100) / 100).toFixed(0)
  }

  if (el.matches('[data-add]'))
    document.getElementById('costlistInputs').style.display = 'table-row'

  if (el.matches('[data-cancel]')) {
    document.getElementById('costlistInputs').style.display = 'none'
    for (const inp of costlistForm.querySelectorAll('input')) {
      inp.value = inp.dataset.value
    }
  }

  if (el.matches('[data-save]'))
    costlistConfirmSave()
}

const costlistConfirmSave = () => {
  message({
    title: 'Save Cost List',
    text: 'Click Yes to create a new cost list. Cost lists are read only data and go into effect upon the date selected.<br /><br /><button data-yes="1" class="modal-close inp12">Yes</button><button class="modal-close inp12">No</button>',
    time: 10000,
    confirm: costlistSave
  })
}

const costlistSave = async () => {

  const params = {}
  let error = 0

  for (const el of costlistForm.querySelectorAll('[name]')) {

    if (el.type == 'number' && el.value == 0)
      error++
    else if (el.value.length == 0)
      error++

    if (error) {
      note({text: 'Please provide values for all fields.', class: 'error'})
      return
    }

    let value = ''

    if (el.step == '0.01')
      value = el.value * 100
    else if (el.id == 'datePicker')
      value = dateYMD(el.value)
    else
      value = el.value

    params[el.name] = value;
  }

  if (error) {
    note({text: 'Please provide values for all fields.', class: 'error'})
    return
  }

  const post = await fetch('/costlistPRG.php', {
    method: 'post',
    headers: {'Content-Type':'application/json;charset=utf-8'},
    body: JSON.stringify(params)
  })

  const res = await post.json()

  if (res.ok) {
    location.reload()
  } else {
    note({text: res.msg[0].msg, class: 'error'})
  }
}

/**
 * MDY to YMD
 * @param {String} mdy
 */
const dateYMD = (mdy) => {
  const d = mdy.split('/')
  return `${d[2]}-${d[0]}-${d[1]}`
}
