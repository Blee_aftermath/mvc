/**
 * Manages and controls the state of JS objects on the jif page
 * @author Jake Cirino
 */
export class JifController{

  /**
   * Creates a new jif controller
   */
  constructor(){

    /**
     * Stores the components 
     */
    this.components = []
  }

  /**
   * Registers a component with the jif controller 
   * @param {Object} component The component to be registered
   */
  registerComponent(component){
    this.components.push(component)
  }

  /**
   * Gets called when a form/item within the jif is updated.
   * This will recursively call the update functions for each registered
   * component.
   * @param {Object} caller The component that called/requested the update.
   * This will ensure the callers update function doesnt get called
   * @param {Number} index The index in the component array to call
   */
  update(caller = undefined, index = 0){
    if(index < this.components.length){
      let component = this.components[index]

      if(component !== caller){
        component.update(() => {
          this.update(caller, index+1)
        })
      }else
        this.update(caller, index+1)
    }
  }

}