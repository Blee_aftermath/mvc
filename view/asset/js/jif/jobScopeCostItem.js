import { Util } from '../util.js'

const currencyFormatter = new Intl.NumberFormat('en-US', {
  style: 'currency',
  currency: 'USD',
})

/**
 * Creates an item for the estiamted job cost
 * @author Jake Cirino
 */
export class JobScopeCostItem {
  /**
   * Creates a new job scope cost item
   * @param {JobScope} parent
   * @param {JSON} scopeData
   */
  constructor(parent, scopeData) {
    this.parent = parent
    this.scopeData = scopeData
    //console.log(scopeData)

    this.baseElement = document.createElement('div')
    this.baseElement.id = `scope-cost-item-${this.scopeData.scope_id}`
    this.baseElement.className = 'scope-item'
    this.baseElement.setAttribute('name', 'alt-color')
  }

  async populate() {
    let template = await window.templateController.getTemplate('jif/jobScopeCostItem')
    this.baseElement.innerHTML = template

    //populate title
    let titleElement = this.baseElement.querySelector("[name='title']")
    titleElement.innerText = this.scopeData.scope_code == 'LS' ? `${this.scopeData.scope_code}/COV ` : this.scopeData.scope_code

    //set href of pdf icon, but will only be used if scope is active
    this.pdfButton = this.baseElement.querySelector('[name=pdfButton]')
    this.pdfButton.setAttribute('href', `createDoc.php?doc=IED&job=${this.parent.data.jobID}&scope=${this.scopeData.scope_id}`)

    //declare fields
    this.manHoursElement = this.baseElement.querySelector("[name='man-hours']")
    this.wasteMgmtElement = this.baseElement.querySelector("[name='waste-mgmt']")
    this.equipDisElement = this.baseElement.querySelector("[name='equip-dis']")
    this.projMgmtElement = this.baseElement.querySelector("[name='proj-mgmt']")
    this.emergencyDispElement = this.baseElement.querySelector("[name='emergency-disp']")
    this.atpTestElement = this.baseElement.querySelector("[name='atp-test']")
    this.asbestosTestElement = this.baseElement.querySelector("[name='asbestos-test']")
    this.asbestosVialsElement = this.baseElement.querySelector("[name='asbestos-vials']")
    this.offsiteDemobilElement = this.baseElement.querySelector("[name='offsite-demobil']")
    this.truckDisElement = this.baseElement.querySelector("[name='truck-dis']")
    this.phodocElement = this.baseElement.querySelector("[name=phodoc]")
    this.digrepElement = this.baseElement.querySelector("[name=digrep]")
    this.dumpsterElement = this.baseElement.querySelector("[name='dumpster']")
    this.equipUsageElement = this.baseElement.querySelector("[name='equip-usage']")
    this.supplyUsageElement = this.baseElement.querySelector("[name='supply-usage']")
    this.subtotalElement = this.baseElement.querySelector("[name='subtotal']")
    this.discountElement = this.baseElement.querySelector("[name='discount']")
    this.taxesElement = this.baseElement.querySelector("[name='taxes']")
    this.thirdPartyElement = this.baseElement.querySelector("[name='3rd-party']")
    this.totalElement = this.baseElement.querySelector("[name='total']")

    this.parent.baseElement.querySelector("[name='ini-scope-cost']").appendChild(this.baseElement)
  }

  /**
   * Calculates the cost of man-hours
   * @param {JSON} data
   */
  calculateManHours(data) {
    let crewSize = Number(data.jini_num_crew)

    let sumHours = Number(data.jini_hr_haz) + Number(data.jini_hr_set) + Number(data.jini_hr_bio) + Number(data.jini_hr_cnt)
      + Number(data.jini_hr_srf) + Number(data.jini_hr_cln) + Number(data.jini_hr_poe) + Number(data.jini_hr_fin)
      + Number(data.jini_hr_ppr)
    let weekendHours = Number(data.jini_hr_wknd)
    let normalHours = sumHours - weekendHours
    
    //sup cost
    let supNormalRate = Number(data.jini_is_iicrc == "1" ? data.jini_rate_iicrc : data.jini_rate_sup)
    let supWkndRate = Number(data.jini_is_iicrc == "1" ? data.jini_wknd_iicrc : data.jini_wknd_sup)
    let manHoursSup = (normalHours * supNormalRate + weekendHours * supWkndRate) / crewSize

    let manHoursTec = (normalHours * data.jini_rate_tec + weekendHours * data.jini_wknd_tec) * ((crewSize - 1) / crewSize)
    
    return Math.round(manHoursSup + manHoursTec)
  }

  /**
   * Calculates the cost of waste management
   * @param {JSON} data
   */
  calculateWasteManagement(data) {
    let numBoxes = Number(data.jini_num_box_suits) + Number(data.jini_num_box_debris)
    let costPerBox = Number(data.jini_price_transport) + Number(data.jini_price_disposal)
    return Math.round(numBoxes * costPerBox) + Number(data.jini_price_wm_fee)
  }

  /**
   * Calculates the cost of dumpsters/bagster
   * @param {JSON} data
   */
  calculateDumpster(data) {
    return Math.round(
      data.jini_num_dumpster * data.jini_price_dumpster + data.jini_num_bagster * data.jini_price_bagster
    )
  }

  /**
   * Calculates the discount
   * @param {Number} subtotal 
   * @param {Number} discountRate 
   * @param {Number} discountOther
   * @returns {Number} 
   */
  calculateDiscount(subtotal, discountRate, discountOther) {
    if (discountOther > 0) {
      return discountOther
    } else {
      return Math.round(subtotal * discountRate)
    }
  }

  /**
   * Calculates the taxes
   * @param {Number} taxType jini_tax_type  
   * @param {Number} ynTax jini_yn_state_tax
   * @param {Number} taxRate jini_tax_rate
   * @param {Number} supplyCost supplyUsage
   * @param {Number} subtotal subtotal
   */
  calculateTaxes(taxType, ynTax, taxRate, supplyCost, subtotal, discount) {
    if(taxType == 1){ //taxType 1: Only supplies are taxed
      return Math.round(ynTax * supplyCost * taxRate / 100)
    }else if(taxType == 2){ //taxType 2: Entire subtotal is taxed
      return Math.round(ynTax * (subtotal + discount) * taxRate / 100)
    }

    return 0 //invalid/no tax type, taxes are 0
  }

  /**
   * Recalculates and updates the estimated cost
   */
  calculateCosts() {

    //activate pdf icon link if scope is selected
    if (this.scopeData.scope_id > 0)
      this.pdfButton.style.display = this.parent.scopeItems[this.scopeData.scope_id].enabled ? 'inline-block' : 'none'

    //init data references
    let data = this.parent.scopeItems[this.scopeData.scope_id].data,
      discountRate = this.parent.discountItems[this.scopeData.scope_id].getDiscount(),
      discountOther = this.parent.discountItems[this.scopeData.scope_id].getDiscountOther()

    //calculate values
    let manHours = this.calculateManHours(data),
      wasteMgmt = this.calculateWasteManagement(data),
      equipDis = Math.round((manHours * data.jini_per_dis) / 100),
      projMgmt = Math.round(((manHours + wasteMgmt + equipDis) * data.jini_per_ovh) / 100),
      emergencyDisp = Math.round(Number(data.jini_price_dispatch)),
      atpTest = Math.round(Number(data.jini_price_atp_test) * Number(data.jini_yn_atp_testing)),
      asbestosTest = data.jini_num_asb_vial > 0 ? Math.round(Number(data.jini_price_asb_test)) : 0,
      asbestosVials = Math.round(data.jini_price_asb_vial * data.jini_num_asb_vial),
      offsiteDemobil = Math.round(Number(data.jini_price_demob)),
      truckDis = Number(data.jini_price_disinfect),
      phodoc = Number(data.jini_price_phodoc),
      digrep = Number(data.jini_price_digrep),
      dumpster = this.calculateDumpster(data),
      usageSubtotal =
        manHours +
        wasteMgmt +
        equipDis +
        projMgmt +
        emergencyDisp +
        atpTest +
        asbestosTest +
        asbestosVials +
        offsiteDemobil +
        truckDis +
        phodoc +
        digrep +
        dumpster,
      equipUsage = Math.round((usageSubtotal + 0.001) * (data.jini_per_eqp / 100)),
      supplyUsage = Math.round((usageSubtotal + 0.001) * (data.jini_per_sup / 100)),
      subtotal = Math.round(usageSubtotal + equipUsage + supplyUsage),
      discount = -this.calculateDiscount(subtotal, discountRate, discountOther),
      taxes = this.calculateTaxes(data.jini_tax_type, data.jini_yn_state_tax, data.jini_tax_rate, supplyUsage, subtotal, discount),
      thirdParty = parseInt(data.jini_amt_3rd_party) + parseInt(data.jini_amt_other),
      total = Math.round(subtotal + discount + taxes + thirdParty)

    //display values
    this.manHoursElement.innerText = Util.formatCurrency(manHours)
    this.wasteMgmtElement.innerText = Util.formatCurrency(wasteMgmt)
    this.equipDisElement.innerText = Util.formatCurrency(equipDis)
    this.projMgmtElement.innerText = Util.formatCurrency(projMgmt)
    this.emergencyDispElement.innerText = Util.formatCurrency(emergencyDisp)
    this.atpTestElement.innerText = Util.formatCurrency(atpTest)
    this.asbestosTestElement.innerText = Util.formatCurrency(asbestosTest)
    this.asbestosVialsElement.innerText = Util.formatCurrency(asbestosVials)
    this.offsiteDemobilElement.innerText = Util.formatCurrency(offsiteDemobil)
    this.truckDisElement.innerText = Util.formatCurrency(truckDis)
    this.phodocElement.innerText = Util.formatCurrency(phodoc)
    this.digrepElement.innerText = Util.formatCurrency(digrep)
    this.dumpsterElement.innerText = Util.formatCurrency(dumpster)
    this.equipUsageElement.innerText = Util.formatCurrency(equipUsage)
    this.supplyUsageElement.innerText = Util.formatCurrency(supplyUsage)
    this.subtotalElement.innerText = Util.formatCurrency(subtotal)
    this.discountElement.innerText = Util.formatCurrency(discount)
    this.taxesElement.innerText = Util.formatCurrency(taxes)
    this.thirdPartyElement.innerText = Util.formatCurrency(thirdParty + .001)
    this.totalElement.innerText = Util.formatCurrency(total)

    // Calculate the cost margin
    this.parent.marginItems[this.scopeData.scope_id].calculateMargins(data)

    // Dispatch an event upon cost calculation
    document.dispatchEvent(this.parent.customEvent.CalculateCostsFinished)
  }

  /**
   * Formats a number for currency display
   * @param {Number} val
   */
  formatCurrency(val) {
    return currencyFormatter.format((Math.round(val) / 100).toFixed(2))
  }
}
