import { FloatingModal, ModalType } from '../floatingModal.js'
import note from '../modules/lib/note.js'
import { Util } from '../util.js'
import { YNModal } from '../ynModal.js'
import {SelectModal} from "../selectModal.js"

const RES_JOB_STATE = {
  CREATE: 1,
  EDIT: 2,
  LOST: 3
}

/**
 * Tab in the jif that handles restoration jobs
 * @author Jake Cirino
 */
export class Restoration {
  /**
   * Creates a new restoration tab object
   * @param {JSON} data
   */
  constructor(data) {
    this.data = data
    this.radioDebounce = true
    console.log(data)

    //parse the dom
    this.baseElement = document.querySelector('[name=tabRestoration]')
    this.dom = Util.parseDOM(this.baseElement)
    this.dom.deconYes = this.baseElement.querySelector('#deconYes')
    this.dom.deconNo = this.baseElement.querySelector('#deconNo')

    //setup y/n has decon buttons
    if(this.data.hasDecon == true) this.dom.deconYes.checked = true
    // MSims 210811 This used to be true, but this.data.hasDecon is coming thru as false even if table value is NULL.
    else if(this.data.hasDecon == false) this.dom.deconNo.checked = false
    this.dom.deconYes.addEventListener('change', ((event) => {
      if(this.radioDebounce)
        this.setJobHasDecon(event)
    }).bind(this))
    this.dom.deconNo.addEventListener('change', ((event) => {
      if(this.radioDebounce)
        this.setJobHasDecon(event)
    }).bind(this))
    
    //setup create button
    this.dom.createRestButton.addEventListener('click', this.createResJob.bind(this))

    //setup amount input change
    let inputElements = this.baseElement.querySelectorAll('input')
    for (const key in inputElements) {
      if (Object.hasOwnProperty.call(inputElements, key)) {
        const element = inputElements[key];
        const inpType = element.getAttribute('type')

        if(inpType == 'text'){
          element.addEventListener('change', (() => {
            this.dom.saveButton.disabled = false
          }).bind(this))
        }else if(inpType == 'number'){
          element.addEventListener('change', (() => {
            this.dom.saveButton.disabled = false

            this.updateRevenue()
          }).bind(this))
        }
      }
    }

    //setup datepickers
    $("[name|='rj_dt_appt']").datepicker({
      onSelect: (() => {this.dom.saveButton.disabled = false}).bind(this)
    })
    $("[name|='rj_dt_appt']").datepicker('option', 'dateFormat', 'mm/dd/y')
    $("[name|='rj_dt_sign']").datepicker({
      onSelect: (() => {this.dom.saveButton.disabled = false}).bind(this)
    })
    $("[name|='rj_dt_sign']").datepicker('option', 'dateFormat', 'mm/dd/y')
    $("[name|='rj_dt_comp']").datepicker({
      onSelect: (() => {this.dom.saveButton.disabled = false}).bind(this)
    })
    $("[name|='rj_dt_comp']").datepicker('option', 'dateFormat', 'mm/dd/y')

    //set initial job state
    if(this.data.resData == null){
      this.setJobState(RES_JOB_STATE.CREATE)
    }else{
      if(this.data.resData.rj_status == 8){
        this.setJobState(RES_JOB_STATE.LOST)
      }else{
        this.setJobState(RES_JOB_STATE.EDIT)
      }
      
      this.populate() //we only want to populate the job if the job data exists
    }
  }
  
  /**
   * Populates the edit section with the current res data
   */
  populate() {
    //populate fields
    for (const key in this.data.resData) {
      if (Object.hasOwnProperty.call(this.data.resData, key)) {
        const element = this.data.resData[key]

        if (key in this.dom) {
          if (key.includes('_amt_')) {
            this.dom[key].value = (parseInt(element) / 100).toFixed(2)
          } else {
            this.dom[key].value = element != null ? element.split(' ')[0] : ''
          }
        }
      }
    }

    this.dom.lostReasonText.innerText = 'Reason: ' + this.data.resData.rjr_dsca
  }

  setJobState(jobState){
    this.jobState = jobState
    
    //create job state
    this.dom.createRest.style.display = jobState == RES_JOB_STATE.CREATE ? 'block' : 'none'
    this.dom.createRestButton.style.display = this.data.hasDecon ? '' : 'none'
    
    //edit job state
    this.dom.editRest.style.display = jobState == RES_JOB_STATE.EDIT ? 'block' : 'none'

    //job lost state
    this.dom.closeRest.style.display = jobState == RES_JOB_STATE.LOST ? 'block' : 'none'
  }

  /**
   * Creates a new res job
   */
  createResJob() {
    //TODO update this to new job type
    fetch('restorationPRG.php', {
      method: 'post',
      headers: { 'Content-Type': 'application/json;charset=utf8' },
      body: JSON.stringify({
        action: 'createResJob',
        jobID: this.data.jobID,
      }),
    })
      .then((res) => res.json())
      .then((body) => {
        if (body.ok) {
          this.data.resData = body.data
          this.setJobState(RES_JOB_STATE.EDIT)
          this.populate()
          window.jifController.update()
        }
      })
    
  }

  /**
   * Collects data from fields and posts updates to the server
   */
  updateResJob() {
    //check if there is no partner selected
    if(this.partnerSelector.getSelectedPartner() == null){
      //new FloatingModal('Must have a restoration partner selected', ModalType.ERROR)
      note({
        text: 'Must have a restoration partner selected',
        class: 'error'
      })
      return //return early
    }

    //prepare data
    for (const key in this.dom) {
      if (Object.hasOwnProperty.call(this.dom, key)) {
        const element = this.dom[key]

        if (key in this.data.resData) {
          if (key.includes('_amt_')) {
            this.data.resData[key] = Number(element.value) * 100
          } else {
            this.data.resData[key] = element.value.length == 0 ? null : element.value
          }
        }
      }
    }

    //get selected partner id
    this.data.resData.rj_partner = this.partnerSelector.getSelectedPartnerID()

    //finalize post data
    let postData = JSON.parse(JSON.stringify(this.data.resData))
    postData.action = 'updateResJob'

    fetch('restorationPRG.php', {
      method: 'post',
      headers: { 'Content-Type': 'application/json;charset=utf8' },
      body: JSON.stringify(postData),
    })
      .then((res) => res.json())
      .then((body) => {
        if (body.ok) {
          this.dom.saveButton.disabled = true
          
          //update data and header status
          this.data.resData = body.data
        }
      })
  }
  
  /**
   * Set the restoration job to lost
   * MSims 210811 Not used any more.  This was from when we would set lost on the JIF page.  RJR fields are gone.
   */
  setResJobLost(){
    //prepare restoration data
    let selectData = []
    for (const reasonsKey in window.reasons) {
      const item = window.reasons[reasonsKey]

      selectData.push({
        value: item.rjr_id,
        text: item.rjr_dsca
      })
    }
    
    let options = {
      title: 'Set Job Lost',
      label: 'Reason:',
      okText: 'Set Lost'
    }
    
    new SelectModal(selectData, options, (result, resultID) => {
      if(result, resultID){
        fetch('restorationPRG.php', {
          method: 'post',
          headers: {'Content-Type': 'application/json;charset=utf8'},
          body: JSON.stringify({
            action: 'setResLost',
            jobID: this.data.jobID,
            reason: resultID
          })
        })
          .then(res => res.json())
          .then(body => {
            if(body.ok){
              this.dom.lostReasonText.innerText = "Reason: " + this.getLostReason(resultID)
              this.setJobState(RES_JOB_STATE.LOST)
            }
          })
      }
    })
  }
  
  /**
   * Resets the restoration job
   */
  resetResJob(){
    new YNModal(
      'Reset Restoration Job',
      'Are you sure you want to reset this restoration job? It will unset it as lost with default restoration job settings.',
      result => {
        if(result){
          fetch('restorationPRG.php', {
            method: 'post',
            headers: {'Content-Type': 'application/json;charset=utf8'},
            body: JSON.stringify({
              action: 'resetResJob',
              jobID: this.data.jobID
            })
          })
            .then(res => res.json())
            .then(body => {
              if(body.ok){
                //set the job data
                this.data.resData = body.data
                this.populate()

                //set the job state
                this.setJobState(RES_JOB_STATE.EDIT)
                
                //disable the save button
                this.dom.saveButton.disabled = true
              }
            })
        }
      }
    )
  }
  
  setJobHasDecon(event){
    this.radioDebounce = false
    let target = event.target
    let hasDecon = event.target.id == 'deconYes' ? true : false
    target.checked = false
    fetch('restorationPRG.php',
      {
        method: 'post',
        headers: {'Content-Type': 'application/json;charset=utf8'},
        body: JSON.stringify({
          action: 'setJobHasDecon',
          jobID: this.data.jobID,
          hasDecon: hasDecon
        })
      })
      .then(res => res.json())
      .then(body => {
        if(body.ok){
          target.checked = true
          this.data.hasDecon = hasDecon
          this.setJobState(RES_JOB_STATE.CREATE)
          this.radioDebounce = true
          if(hasDecon)
            this.createResJob()
          window.jifController.update()
        }
      })
  }
}
