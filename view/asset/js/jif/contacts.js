import { ContactModal } from './contactModal.js'
import { YNModal } from '../ynModal.js'
import { Util } from '../util.js'

/**
 * Contains information about contacts for a job in the jif
 * @author Jake Cirino
 */
export class Contacts {
  /**
   * Creates a new contacts list
   * @param {String} template The html template for the contact tab
   * @param {JSON} data The contact data to list
   */
  constructor(data) {
    this.data = data
    this.contacts = []
    this.impContacts = {}

    this.baseElement = document.createElement('div')

    //load template
    this.populate()
  }

  /**
   * Populates the block
   */
  async populate(){
    let template = await window.templateController.getTemplate('jif/contactTab')
    this.baseElement.innerHTML = template

    //set element vars
    this.prowElement = this.baseElement.querySelector("[name='prow']")
    this.cosiElement = this.baseElement.querySelector("[name='cosi']")
    this.oscoElement = this.baseElement.querySelector("[name='osco']")

    //populate contacts
    this.populateContacts(this.data)

    //append html to document
    document.querySelector('.body_jif_l').appendChild(this.baseElement)

    //register with the jif controller
    window.jifController.registerComponent(this)
  }

  populateContacts(data) {
    //initially clear all contacts
    this.clearAllContacts()

    //add contacts
    this.contactAppend = this.baseElement.querySelector("[name='contact-insert']")
    data.contacts.forEach((element) => {
      let tmpContact = this.createTableContact(element)

      this.contacts.push(tmpContact)
      this.contactAppend.parentNode.insertBefore(tmpContact.html, this.contactAppend.nextSibling)

      if (element.addr_is_prow == 1) this.setImportantContact('prow', element)

      if (element.addr_is_cosi == 1) this.setImportantContact('cosi', element)

      if (element.addr_is_osco == 1) this.setImportantContact('osco', element)
    })

    //Add contact button functionality
    let modalData = {
      jobId: this.data.jobId,
      type: 'add',
      title: 'Add Contact',
      displaySelectors: false,
      displayForm: true,
      states: this.data.states,
    }
    this.baseElement.querySelector("[name='add-contact']").onclick = () => {
      new ContactModal(this, modalData)
    }
  }

  /**
   * Removes all contacts
   */
  clearAllContacts() {
    //reset important contacts
    this.setImportantContact('prow')
    this.setImportantContact('cosi')
    this.setImportantContact('osco')

    //remove all contacts from array and DOM
    this.contacts = []
    this.baseElement.querySelectorAll("[name='contact']").forEach((element) => element.remove())
  }

  /**
   * Sets the important contact
   * @param {String} type The type of contact. Either prow, cosi or osco.
   * @param {JSON} contactData
   */
  setImportantContact(type, contactData = null) {
    //set in impcontact array
    let contact = this.createImportantContact(contactData)
    this.impContacts[type] = contact

    //change edit/add button in header
    let buttonElem = this.baseElement.querySelector(`[name='${type}-btn']`)
    buttonElem.alt = contactData === null ? 'Add' : 'Edit'

    let modalData = {
      jobId: this.data.jobId,
      type: 'important',
      impType: type,
      addrId: contactData !== null ? contactData.addr_id : undefined,
      title: 'Edit Contact',
      displaySelectors: true,
      displayForm: false,
      contacts: this.data.contacts,
      selectedContact: contactData === null ? undefined : contactData,
      states: this.data.states,
    }
    buttonElem.onclick = (() => {
      new ContactModal(this, modalData)
    }).bind(this)

    //set inner content
    let contentElem = this.baseElement.querySelector(`[name='${type}']`)
    if(contentElem.children.length > 1)
      contentElem.removeChild(contentElem.lastChild)
    contentElem.appendChild(contact.html)
  }

  /**
   * Creates an important contact to be appended to the important contact section
   * @param {JSON} contactData
   * @returns {JSON} JSON containing the contact data and HTML element for this contact
   */
  createImportantContact(contactData) {
    let html = Util.getContactBlock(contactData)

    return {
      data: contactData,
      html: html,
    }
  }

  /**
   * Creates a contact to be appended to the all contacts table
   * @param {JSON} contactData
   * @returns {JSON} JSON containing the contact data and HTML element for this contact
   */
  createTableContact(contactData) {
    //create htmlelement
    let row = document.createElement('tr')
    row.setAttribute('name', 'contact')

    //build field strings from data
    contactData = Util.filterNulls(contactData)

    //create/append field elements
    let nameField = document.createElement('td')
    nameField.innerText = `${contactData.addr_fname} ${contactData.addr_lname}`.trim()
    row.appendChild(nameField)

    let relationField = document.createElement('td')
    relationField.innerText = contactData.addr_relation.trim()
    row.appendChild(relationField)

    let companyField = document.createElement('td')
    companyField.innerText = contactData.addr_company.trim()
    row.appendChild(companyField)
    
    let infoField = document.createElement('td')
    infoField.innerText = contactData.addr_info.trim()
    row.appendChild(infoField)

    let addrField = document.createElement('td')
    addrField.innerText = `${contactData.addr_line1} ${contactData.addr_city}, ${contactData.addr_state}`.trim()
    row.appendChild(addrField)

    let buttonField = document.createElement('td')

    //add delete button
    let deleteButton = document.createElement('img')
    deleteButton.className = 'icon16'
    deleteButton.src = 'icons/trash16.png'
    deleteButton.alt = 'Edit'
    deleteButton.style.float = 'right'
    deleteButton.style.cursor = 'pointer'
    deleteButton.style.filter = 'grayscale(1)'

    //create y/n modal when the delete button is clicked
    deleteButton.onclick = (() => {
      new YNModal(
        'Delete Contact',
        'Are you sure you want to delete this contact?',
        ((result) => {
          if (result) {
            //call delete on this contact
            fetch('jifContactsPRG.php', {
              method: 'post',
              headers: { 'Content-Type': 'application/json;charset=utf8' },
              body: JSON.stringify({
                action: 'delete',
                jobId: this.data.jobId,
                addrId: contactData.addr_id,
              }),
            })
              .then((res) => {
                return res.json()
              })
              .then((body) => {
                if (body.ok) {
                  //update the jif controller
                  window.jifController.update(this)

                  //update dom
                  this.data.contacts = body.msg
                  this.populateContacts(this.data)
                }
              })
          }
        }).bind(this)
      )
    }).bind(this)
    buttonField.appendChild(deleteButton)

    //add edit button
    let editData = {
      title: 'Edit Contact',
      type: 'edit',
      jobId: this.data.jobId,
      displaySelectors: false,
      displayForm: true,
      selectedContact: contactData,
      states: this.data.states,
    }
    let editButton = document.createElement('img')
    editButton.className = 'icon16'
    editButton.src = 'icons/edit16.png'
    editButton.alt = 'Edit'
    editButton.style.float = 'right'
    editButton.style.cursor = 'pointer'
    editButton.onclick = (() => {
      new ContactModal(this, editData)
    }).bind(this)
    buttonField.appendChild(editButton)
    row.appendChild(buttonField)

    return {
      data: contactData,
      html: row,
    }
  }

  /**
   * Calls the jifContactsPRG and updates the contacts DOM
   * This is called by the JifController on page update.
   * @param {Function} callback
   */
  update(callback) {
    //if we already have data, we dont need to call
    if (data !== undefined) {
      //update data contacts
      this.data.contacts = data
      this.populateContacts(this.data)

      callback()
    } else {
      fetch('jifContactsPRG.php', {
        method: 'post',
        headers: { 'Content-Type': 'application/json;charset=utf8' },
        body: JSON.stringify({
          jobId: this.data.jobId,
          action: 'get',
        }),
      })
        .then((res) => res.json())
        .then((body) => {
          //update data contacts
          this.data.contacts = body
          this.populateContacts(this.data)

          callback()
        })
    }
  }
}
