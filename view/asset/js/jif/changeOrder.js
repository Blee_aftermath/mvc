import { ChangeOrderItem } from './changeOrderItem.js'
import { AsyncScheduler } from '../asyncScheduler.js'

String.prototype.ucFirst = function () {return this.charAt(0).toUpperCase() + this.slice(1)}

/**
 * Class for handling change order
 * @author Jake Cirino
 */
export class ChangeOrder {
  /**
   * Creates a new change order block
   * @param {JSON} data
   */
  constructor(data) {
    this.data = data
    this.selectedScope = 'pri'
    this.items = []
    this.secItems = []

    //console.log(this.data)

    this.marginPerm = 'jif_m'

    //setup primary scope pricing data
    this.data.primaryPricing = {}
    this.data.primaryMarkup = {}  //2021-03-24 Now, also includes the ini-est confirmed realization percent as DB field name is jif_per_real_est.
    this.data.primaryTaxes = {}
    this.data.primaryIniEst = {}
    for (const key in this.data.primaryIni) {
      if (this.data.primaryIni.hasOwnProperty(key)) {
        const element = this.data.primaryIni[key]
        if (key.includes('price_') || key.includes('rate_')) this.data.primaryPricing[key.substring(key.indexOf('_') + 1)] = element
        else if (key.includes('per_')) this.data.primaryMarkup[key.substring(key.indexOf('_') + 1)] = element
        else if (key.includes('_tax')) this.data.primaryTaxes[key.substring(key.indexOf('_') + 1)] = element
        else if (key.includes('_ini')) this.data.primaryIniEst[key.substring(key.indexOf('_') + 1)] = element
      }
    }

    //setup secondary scope pricing data
    if (this.data.secondaryIni != null) {
      this.data.secondaryPricing = {}
      this.data.secondaryMarkup = {}
      this.data.secondaryTaxes = {}
      this.data.secondaryIniEst = {}
      for (const key in this.data.secondaryIni) {
        if (this.data.secondaryIni.hasOwnProperty(key)) {
          const element = this.data.secondaryIni[key]
          if (key.includes('price_')) this.data.secondaryPricing[key.substring(key.indexOf('_') + 1)] = element
          else if (key.includes('per_')) this.data.secondaryMarkup[key.substring(key.indexOf('_') + 1)] = element
          else if (key.includes('_tax')) this.data.secondaryTaxes[key.substring(key.indexOf('_') + 1)] = element
          else if (key.includes('_ini')) this.data.secondaryIniEst[key.substring(key.indexOf('_') + 1)] = element
        }
      }
    } else {
      this.data.secondaryPricing = null
      this.data.secondaryMarkup = null
      this.data.secondaryTaxes = null
      this.data.secondaryIniEst = null
    }

    //create base element
    this.baseElement = document.createElement('div')
    this.baseElement.className = 'scope'
    this.baseElement.innerHTML = this.data.template

    //define/set elements
    this.saveButton = this.baseElement.querySelector("[name='save-button']")
    this.scopeBody = this.baseElement.querySelector("[name='ini-scope']")
    this.scopeScroll = this.baseElement.querySelector("[name='scope-jco']")
    this.discountBody = this.baseElement.querySelector("[name='ini-scope-discount']")
    this.discountScroll = this.baseElement.querySelector("[name='discount-jco']")
    this.costBody = this.baseElement.querySelector("[name='ini-scope-cost']")
    this.costScroll = this.baseElement.querySelector("[name='cost-jco']")
    this.marginBody = this.baseElement.querySelector("[name='ini-scope-margin']")
    this.marginScroll = this.baseElement.querySelector("[name='margin-jco']")

    //Allow margin to be viewed if proper permission
    if (window.aftermath.hasPerm(this.marginPerm))
      this.marginBody.querySelector('.scope-margin-labels').style.display = 'block'

    //setup pri/sec/all filters and run initial calcs
    //pri used for limited or structure
    this.filterPri = this.baseElement.querySelector('[name=filterPri]')
    this.filterPri.value = this.data.primaryInvType
    this.filterSec = this.baseElement.querySelector('[name=filterSec]')
    this.filterAll = this.baseElement.querySelector('[name=filterAll]')
    if (this.data.secondaryIni == null) {
      this.filterSec.style.display = 'none'
      this.filterAll.style.display = 'none'
    }

    //setup scroll events
    this.scopeScroll.addEventListener('scroll', (event) => {
      this.discountScroll.scrollLeft = event.target.scrollLeft
      this.marginScroll.scrollLeft = event.target.scrollLeft
      this.costScroll.scrollLeft = event.target.scrollLeft
    })
    this.discountScroll.addEventListener('scroll', (event) => {
      this.scopeScroll.scrollLeft = event.target.scrollLeft
      this.costScroll.scrollLeft = event.target.scrollLeft
      this.marginScroll.scrollLeft = event.target.scrollLeft
    })
    this.costScroll.addEventListener('scroll', (event) => {
      this.discountScroll.scrollLeft = event.target.scrollLeft
      this.scopeScroll.scrollLeft = event.target.scrollLeft
      this.marginScroll.scrollLeft = event.target.scrollLeft
    })
    this.marginScroll.addEventListener('scroll', (event) => {
      this.scopeScroll.scrollLeft = event.target.scrollLeft
      this.discountScroll.scrollLeft = event.target.scrollLeft
      this.costScroll.scrollLeft = event.target.scrollLeft
    })

    //setup save button
    this.saveButton.addEventListener('click', (event) => {
      this.save()
    })

    //setup item template json
    this.priData = {
      template: this.data.itemTemplate,
      discountTemplate: this.data.discountTemplate,
      costTemplate: this.data.costTemplate,
      marginTemplate: this.data.marginTemplate,
      crewSize: this.data.crewInfo.jif_num_crew,
      jobDays: this.data.crewInfo.jif_num_days,
      discounts: this.data.primaryDiscounts,
      pricing: this.data.primaryPricing,
      markup: this.data.primaryMarkup,
      taxes: this.data.primaryTaxes,
      iniEst: this.data.primaryIniEst,
      invType: this.data.primaryInvType,
      costlist: this.data.costlist,
      realzns: this.data.realzns,
      jifInfo: this.data.jifInfo,
    }

    this.secData = {
      template: this.data.itemTemplate,
      discountTemplate: this.data.discountTemplate,
      costTemplate: this.data.costTemplate,
      marginTemplate: this.data.marginTemplate,
      crewSize: this.data.crewInfo.jif_num_crew,
      jobDays: this.data.crewInfo.jif_num_days,
      discounts: this.data.secondaryDiscounts,
      pricing: this.data.secondaryPricing,
      markup: this.data.secondaryMarkup,
      taxes: this.data.secondaryTaxes,
      iniEst: this.data.secondaryIniEst,
      invType: this.data.secondaryInvType,
      costlist: this.data.costlist,
      realzns: this.data.realzns,
      jifInfo: this.data.jifInfo,
    }

    //create primary ini
    let priIniData = JSON.parse(JSON.stringify(this.priData))
    priIniData.item = this.data.primaryIni
    this.priIniItem = new ChangeOrderItem(this, priIniData, 'jini', 'pri', true)

    //create secondary ini if it exists
    if (this.data.secondaryIni != null) {
      let secIniData = JSON.parse(JSON.stringify(this.secData))
      secIniData.item = this.data.secondaryIni
      this.secIniItem = new ChangeOrderItem(this, secIniData, 'jini', 'sec', true)
    }

    //add primary change orders
    for (const key in this.data.primaryChangeOrders) {
      if (this.data.primaryChangeOrders.hasOwnProperty(key)) {
        const element = this.data.primaryChangeOrders[key]
        let jcoData = JSON.parse(JSON.stringify(this.priData))
        jcoData.item = element
        let item = new ChangeOrderItem(this, jcoData, 'jco', `pri`, false)
        this.items.push(item)
      }
    }

    //add secondary change orders, if they exist
    if (this.data.secondaryIni != null) {
      for (const key in this.data.secondaryChangeOrders) {
        if (this.data.secondaryChangeOrders.hasOwnProperty(key)) {
          const element = this.data.secondaryChangeOrders[key]
          let jcoData = JSON.parse(JSON.stringify(this.secData))
          jcoData.item = element
          let item = new ChangeOrderItem(this, jcoData, 'jco', 'sec', false)
          this.secItems.push(item)
        }
      }
    }

    //create final estimate item
    //TODO needs to accomodate pri/sec/all filtering
    let finalData = JSON.parse(JSON.stringify(this.priData))
    finalData.item = this.data.primaryActual
    //console.log(finalData.item)

    this.finalEstItem = new ChangeOrderItem(this, finalData, 'fin', 'fin', true)

    //create running estimate item
    let totalData = JSON.parse(JSON.stringify(this.priData))
    this.totalItem = new ChangeOrderItem(this, totalData, 'tot', 'tot', true)

    //setup filter buttons and the default
    this.filterPri.addEventListener('click', () => this.setSelectedScope('pri'))
    this.filterSec.addEventListener('click', () => this.setSelectedScope('sec'))
    this.filterAll.addEventListener('click', () => this.setSelectedScope('all'))
    this.setSelectedScope(this.selectedScope)

    //update totals
    this.updateTotals()

    //append to body
    document.querySelector('.body_jif_l').appendChild(this.baseElement)
  }

  /**
   * Sets the selected scope in this change order view
   * @param {String} scope Either 'pri', 'sec' or 'all'
   */
  setSelectedScope(scope) {
    this.selectedScope = scope

    //set active button
    this.filterPri.style.backgroundColor = ''
    this.filterSec.style.backgroundColor = ''
    this.filterAll.style.backgroundColor = ''
    this[`filter${scope.ucFirst()}`].style.backgroundColor = '#ffff99'

    let priVisible = this.selectedScope == 'all' || this.selectedScope == 'pri',
      secVisible = this.selectedScope == 'all' || this.selectedScope == 'sec'

    //set items visible based on selected scope
    this.priIniItem.setVisible(priVisible)
    if(this.secIniItem != null) this.secIniItem.setVisible(secVisible)

    //set display for pri jco items
    for (const key in this.items) {
      if (this.items.hasOwnProperty(key)) {
        const element = this.items[key];
        element.setVisible(priVisible)
      }
    }

    //set display for sec jco items
    if(this.secIniItem != null){
      for (const key in this.secItems) {
        if (this.secItems.hasOwnProperty(key)) {
          const element = this.secItems[key];
          element.setVisible(secVisible)
        }
      }
    }

    //calculate scrollbox sizes
    let columnWidth = 143.25
    let numColumns = 2 //includes running estimate and final total
    let maxColumns = 5
    if(this.selectedScope == 'all'){
      numColumns++ //primary initial estimate
      if(this.secIniItem != null) numColumns++
    }else numColumns++ 
    let scrollWidth = `${(maxColumns - numColumns) * columnWidth}px`

    //set scrollbox sizes
    this.scopeScroll.style.width = scrollWidth
    this.discountScroll.style.width = scrollWidth
    this.costScroll.style.width = scrollWidth
    this.marginScroll.style.width = scrollWidth

    //update totals
    this.updateTotals()
  }

  /**
   * Updates the total items
   */
  updateTotals() {
    //TODO final estimate data?

    //initialize total data
    let totalData
    if (this.selectedScope == 'pri' || this.selectedScope == 'all') {
      totalData = JSON.parse(JSON.stringify(this.priData))
      totalData.item = JSON.parse(JSON.stringify(this.priIniItem.data.item))

      //add secondary scope data if the selected scope is all
      if (this.selectedScope == 'all' && this.secIniItem != null) {
        for (const key in this.secIniItem.data.item) {
          if (this.secIniItem.data.item.hasOwnProperty(key)) {
            const value = this.secIniItem.data.item[key]
            if (key == 'num_days' || key == 'num_crew') {
              //we want to find the max for days/crew, not sum them
              if (totalData.item[key] < value) totalData.item[key] = Number(totalData.item[key]) + Number(value)
            } else {
              totalData.item[key] = Number(totalData.item[key]) + Number(value)
            }
          }
        }
      }
    } else {
      //secondary scope data only
      totalData = JSON.parse(JSON.stringify(this.secData))
      totalData.item = JSON.parse(JSON.stringify(this.secIniItem.data.item))
    }

    //add from all change order items
    if (this.selectedScope == 'pri' || this.selectedScope == 'all'){
      for (const k in this.items) {
        if (this.items.hasOwnProperty(k)) {
          const element = this.items[k]
          let jcoData = element.data.item
  
          //sum all fields
          for (const key in jcoData) {
            if (jcoData.hasOwnProperty(key)) {
              const value = jcoData[key]
              if (key == 'num_days' || key == 'num_crew') {
                //we want to find the max for days/crew, not sum them
                if (totalData.item[key] < value) totalData.item[key] = Number(totalData.item[key]) + Number(value)
              } else {
                totalData.item[key] = Number(totalData.item[key]) + Number(value)
              }
            }
          }
        }
      }
    }

    if(this.selectedScope == 'sec' || this.selectedScope == 'all'){
      for (const key in this.secItems) {
        if (this.secItems.hasOwnProperty(key)) {
          const element = this.secItems[key];
          let jcoData = element.data.item

          //sum all fields
          for (const key in jcoData) {
            if (jcoData.hasOwnProperty(key)) {
              const value = jcoData[key];
              if(key == 'num_days' || key == 'num_crew'){
                //we want to find the max for days/crew, not sum them
                if(totalData.item[key] < value) totalData.item[key] = Number(totalData.item[key]) + Number(value)
              }else{
                totalData.item[key] = Number(totalData.item[key]) + Number(value)
              }
            }
          }
        }
      }
    }

    //TODO update totals for final

    //update total item
    this.totalItem.data = totalData
    this.totalItem.initialize()

    //update final estimate data
    if(this.selectedScope == 'pri'){
      this.finalEstItem.setItemData(this.data.primaryActual)
    }else if(this.selectedScope == 'sec'){
      this.finalEstItem.setItemData(this.data.secondaryActual)
    }else{
      let finalTotal = JSON.parse(JSON.stringify(this.data.primaryActual))
      
      //calc total of sec + pri if secondary actual exists
      if(this.data.secondaryActual != null){
        for (const key in this.data.secondaryActual) {
          if (this.data.secondaryActual.hasOwnProperty(key)) {
            const value = this.data.secondaryActual[key];
  
            if(key != 'jfin_num_days' && key != 'jfin_num_crew'){
              finalTotal[key] = Number(finalTotal[key]) + Number(value)
            }
          }
        }
      }

      this.finalEstItem.setItemData(finalTotal)
    }
    this.finalEstItem.populate()
  }

  /**
   * Saves all the change order items
   */
  save() {
    let scheduler = new AsyncScheduler(() => {window.jifController.update()}) //creates a new scheduler that will call jifcontroller when done
    for (const key in this.items) {
      if (this.items.hasOwnProperty(key)) {
        const item = this.items[key]
        scheduler.add(item.save.bind(item)) //add each async function to scheduler queue
      }
    }
    scheduler.execute() //executes the scheduler
  }

  /**
   * Removes an item from the change order list
   * @param {ChangeOrderItem} item
   */
  deleteItem(item) {
    let newAry = [],
      newSec = []

    //check in pri items
    for (const key in this.items) {
      if (this.items.hasOwnProperty(key)) {
        const element = this.items[key]
        if (element.equals(item)) element.delete()
        else newAry.push(element)
      }
    }

    //check in sec items
    for (const key in this.secItems) {
      if (this.secItems.hasOwnProperty(key)) {
        const element = this.secItems[key];
        if(element.equals(item)) element.delete()
        else newSec.push(element)
      }
    }

    this.items = newAry
    this.secItems = newSec
    this.updateTotals()
    window.jifController.update()
  }
}