import { default as formModal } from './modules/ui/formModal.js'
import { default as note } from './modules/lib/note.js'

/**
 * Class for Jail Cell Jobs admin page
 * @author Scott Kiehn
 */

export class JCJob {
  /**
   * Creates a new JCJob class
   */
  constructor() {
    this.dom = {}
  
    // Parent of JC Job DOM
    this.dom.jcj         = document.querySelector('#jcJob')
    
    // DOM for new customer selection
    this.dom.jobYr       = document.querySelector('#jobYr')
    this.dom.jobAlt      = document.querySelector('#jobAlt')
    this.dom.jobPropType = document.querySelector('#jobPropType')
    this.dom.jjclCust    = document.querySelector('#jjclCust')
    
    this.dom.addIcon     = document.querySelector('#add-icon')
    this.dom.sup         = document.querySelector('#spanCrewSup')
    this.dom.tec         = document.querySelector('#spanCrewTec')
    this.dom.crewSize    = document.querySelector('#crewSize')
    this.dom.crewSize2   = document.querySelector('#crewSize2')
    this.dom.crewNames   = document.querySelector('#crewNames')
    this.dom.saveMdl     = document.querySelector('#modal-job-service-form')
    this.dom.saveBtn     = document.querySelector('#modal-save-button')

    this.dom.jcj.addEventListener('click', (ev) => {

      if (ev.target.matches('#add-icon')) {
        if (this.dom.jobYr.value.length == 2 && this.dom.jobAlt.value.length == 4 && this.dom.jjclCust.value > 0) {
          this.addJob(ev.target, this.dom.jobYr.value, this.dom.jobAlt.value, this.dom.jjclCust.value, this.dom.jobPropType.value)
        } else {
          note({
            class: 'error',
            text: 'Please provide complete information.'
          })
        }
      }

      if (ev.target.matches('[data-opts]'))
        formModal(ev.target)

    })

    jQuery('#selDAppt').datepicker();
    jQuery('#selDStart').datepicker();
    jQuery('#selDComp').datepicker();

    this.dom.saveBtn.addEventListener('click', (ev) => this.saveServInfo(ev.target.closest('form')))

    document.addEventListener('crewChangeEvent', (ev) => {

      const crewNames = []

      for (const emp of this.dom.sup.querySelectorAll('option')) {
        if (emp.innerText.length) crewNames.push(emp.innerText)
      }

      for (const emp of this.dom.tec.querySelectorAll('option')) {
        if (emp.innerText.length) crewNames.push(emp.innerText)
      }

      this.dom.crewSize.innerText  = crewNames.length
      this.dom.crewSize2.innerText = crewNames.length
      this.dom.crewNames.innerText = crewNames.sort().join(', ')
    })
  }

  /**
   * Add a new jail cell job
   * @param {Element} form
   */
  addJob = async (el, yr, alt, cust = 0, prop) => {
    const inpData = {
      action:  'insert',
      job_yr:        yr,
      job_alt:       alt,
      jjcl_cust:     cust,
      job_prop_type: prop
    }
    
    const post = await fetch('jcJobPRG.php', {
      method: 'post',
      headers: { 'Content-Type': 'application/json;charset=utf-8' },
      body: JSON.stringify(inpData)
    })

    const res = await post.json()

    if ( res.ok ) {
      location.assign(res.referer)
    } else {
      note({class: 'error', text: res.msg})
    }
  }

  /**
   * Save jail cell service information
   * @param {Element} form
   */
  saveServInfo = async (form) => {

    const inpData = {}

    let errors = 0

    for (const inp of form.querySelectorAll('[name]')) {
      inpData[inp.name] = inp.value
      if (inp.classList.contains('req')) if (inp.value.length == 0) errors++
    }

    if (errors) return this.dom.saveMdl.querySelector('.error').innerHTML = `<p>Values for the required items were not provided.</p>`;

    const post = await fetch('jcJobPRG.php', {
      method: 'post',
      headers: { 'Content-Type': 'application/json;charset=utf-8' },
      body: JSON.stringify(inpData)
    })

    const res = await post.json()

    if ( res.ok ) {
      this.dom.saveMdl.querySelector('.error').innerHTML = '';
      this.dom.saveMdl.querySelector('.message').innerHTML = `<p>${res.msg}</p>`;
      setTimeout(() => {
        location.assign(res.ref)
        if (res.ref.match(/#/)) location.reload()
      }, 750)
    } else {
      const error = []

      if (typeof(res.msg) == 'object') {
        for (const m of res.msg) error.push(m.msg)
      } else {
        error[0] = res.msg
      }

      this.dom.saveMdl.querySelector('.error').innerHTML = `<p>${error.join('<br />')}</p>`;
    }
  }
}
