import {Util} from "../util.js"

export class ServicesModal {
  /**
   * Creates a new Services modal
   * @param {Number} jobID
   * @param {Function} callback
   */
  constructor(jobID, callback) {
    this.jobID = jobID
    this.callback = callback
    
    //create base element
    this.baseElement = document.createElement('div')
    this.baseElement.className = 'modal'
    this.baseElement.style.display = 'block'
    this.baseElement.innerHTML = window.modalTemplate
    this.dom = Util.parseDOM(this.baseElement)

    this.populateAvailableServices()
    this.initializeEvents()

    //append to document
    document.body.appendChild(this.baseElement)
  }

  /**
   * Fetches a list of the available service scopes & offerings and populates the service selector
   */
  async populateAvailableServices(){

    const post = await fetch('servicesPRG.php', {
      method: 'post',
      headers: {'Content-Type': 'application/json;charset=utf8'},
      body: JSON.stringify({action: 'getAvailableServices', jobID: this.jobID})
    }),
    res = await post.json()

    if (res.ok) {
      const servScopes = []

      for (const msgKey in res.msg) {
        const item = res.msg[msgKey]

        if (!servScopes.includes(item.svs_id)) {
          servScopes.push(item.svs_id)
          const optSco = document.createElement('option')
          optSco.innerText = item.svs_dsca
          optSco.setAttribute('data-svs_id', item.svs_id)
          this.dom.jsvc_sco.appendChild(optSco)
        }

        const optOff = document.createElement('option')
        optOff.value = JSON.stringify(item)
        optOff.innerText = `${item.svo_short}: ${item.svo_dsca}`
        optOff.setAttribute('data-svo_svs', item.svo_svs)
        if (Number(item.svo_svs) != Number(servScopes[0])) optOff.style.display = 'none'
        this.dom.jsvc_off.appendChild(optOff)
      }
    }
  }

  /**
   * Initializes the event listeners on the dom
   */
  initializeEvents(){
    this.dom.jsvc_sco.addEventListener('change', this.change.bind(this))
    this.dom.createButton.addEventListener('click', this.create.bind(this))
    this.dom.exitButton.addEventListener('click', this.close.bind(this))
    this.dom.cancelButton.addEventListener('click', this.close.bind(this))
  }
  
  /**
   * Calls the callback with data from this modal
   */
  create(){
    let service = JSON.parse(this.dom.jsvc_off.value)

    this.callback({
      jsvo_job: this.jobID,
      jsvo_svo: service.svo_id,
      svs_dsca: service.svs_dsca,
      svo_dsca: service.svo_dsca,
      svo_short: service.svo_short,
      jsvo_amt_ini: Math.round(Number(this.dom.jsvo_amt_ini.value) * 100),
      jsvo_amt_fin: Math.round(Number(this.dom.jsvo_amt_fin.value) * 100),
      jsvo_note: this.dom.jsvo_note.value
    })
    this.close()
  }
  
  /**
   * Closes this modal
   */
  close(){
    this.baseElement.remove()
    this.callback(null)
    delete this
  }

  /**
   * Alters Offering select options based on Scope select option
   */
  change(){
    const select = this.dom.jsvc_sco,
          selectedSVS = Number(select.options[select.selectedIndex].dataset.svs_id)

    for (const option of this.dom.jsvc_off.querySelectorAll('option')) {
      option.style.display = (Number(option.dataset.svo_svs) == selectedSVS) ? 'block' : 'none'
    }

    this.dom.jsvc_off.querySelector(`[data-svo_svs='${selectedSVS}']`).selected = true
  }
}