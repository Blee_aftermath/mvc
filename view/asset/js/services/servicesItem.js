import { default as note } from '../modules/lib/note.js'
import {Util} from "../util.js"

export class ServicesItem{
  constructor(parent, data) {
    this.parent = parent
    this.data = data
    //console.log(this.data)

    //create base element
    this.baseElement = document.createElement('div')
    this.baseElement.classList.add('service-item')
    this.baseElement.innerHTML = window.itemTemplate
    this.baseElement.style.backgroundColor = 'white'
    // this.baseElement.style.margin = '8px'
    this.dom = Util.parseDOM(this.baseElement)
    this.inputFields = this.baseElement.querySelectorAll("[tag=inputField]")
    
    this.initiateEvents()
    this.populate()

    this.parent.baseElement.appendChild(this.baseElement)
  }
  
  /**
   * Initiates the events for this item
   */
  initiateEvents(){
    //have input items re-enable the save button on change
    for (const inputItemsKey in this.inputFields) {
      if(this.inputFields.hasOwnProperty(inputItemsKey)){
        const item = this.inputFields[inputItemsKey]
        item.addEventListener('change', (() => {
          this.dom.saveButton.disabled = false
        }).bind(this))
      }
    }
    
    this.dom.saveButton.addEventListener('click', this.update.bind(this))
    this.dom.deleteButton.addEventListener('click', this.delete.bind(this))
  }
  
  /**
   * Populates this item from this.data
   */
  populate(){
    this.dom.svo_dsca.innerText = `${this.data.svs_dsca}: ${this.data.svo_short} - ${this.data.svo_dsca}`
    this.dom.jsvo_amt_ini.value = (Number(this.data.jsvo_amt_ini) / 100).toFixed(2)
    this.dom.jsvo_amt_fin.value = (Number(this.data.jsvo_amt_fin) / 100).toFixed(2)
    this.dom.jsvo_note.value    = this.data.jsvo_note
  }
  
  /**
   * Updates this service offering on the database
   */
  update(){
    this.dom.saveButton.disabled = true
    
    //update this.data to reflect what is currently in the fields
    for (const inputFieldsKey in this.inputFields) {
      if(this.inputFields.hasOwnProperty(inputFieldsKey)){
        const item = this.inputFields[inputFieldsKey]
        
        if(item.name.includes('amt'))
          this.data[item.name] = Math.round(Number(item.value) * 100)
        else
          this.data[item.name] = item.value
      }
    }
    
    let data = JSON.parse(JSON.stringify(this.data))
    data.action = 'updateServiceOffering'
    fetch('servicesPRG.php', {
      method: 'post',
      headers: {'Content-Type': 'application/json;charset=utf8'},
      body: JSON.stringify(data)
    })
      .then(res => res.json())
      .then(body => {
        if(body.ok){
          this.populate()
          this.baseElement.classList.remove('box-error')
          document.dispatchEvent(this.parent.eqsServicesEvent) //dispatch the change event
        }else{
          this.dom.saveButton.disabled = false
          this.baseElement.classList.add('box-error')
          note({
            class: 'error',
            text: 'There has been an error. Your updates have not been saved.',
            time: 2500
          })
        }
      })
  }
  
  /**
   * Deletes this service item from the database
   */
  delete(){
    fetch('servicesPRG.php', {
      method: 'post',
      headers: {'Content-Type': 'application/json;charset=utf8'},
      body: JSON.stringify({
        action: 'deleteServiceOffering',
        jsvo_job: this.data.jsvo_job,
        jsvo_svo: this.data.jsvo_svo
      })
    })
      .then(res => res.json())
      .then(body => {
        if(body.ok){
          this.remove()
          document.dispatchEvent(this.parent.eqsServicesEvent) //dispatch the change event
        }
      })
  }
  
  /**
   * removes this element from the dom
   */
  remove(){
    this.baseElement.remove()
    delete this
    //TODO remove from list of items
  }
  
  /**
   * Compares this services item to another
   * @param {ServicesItem} other
   * @returns {boolean} True if the items are equal, false otherwise
   */
  equals(other){
    return this.data.jsvo_job == other.data.jsvo_job
      && this.data.jsvo_svo == other.data.jsvo_svo
  }
}