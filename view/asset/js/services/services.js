import { default as note } from '../modules/lib/note.js'
import {Util} from "../util.js"
import {ServicesItem} from "./servicesItem.js"
import {ServicesModal} from "./servicesModal.js"

export class Services{
  /**
   * Creates a new services page
   * @param {Object} data
   * @param {Number} data.jobID
   * @param {Object} data.items
   */
  constructor(data) {
    this.data = data
    this.items = []
    //console.log(data)
    
    this.baseElement = document.querySelector('#blockServices')
    this.dom = Util.parseDOM(this.baseElement)
    
    this.initiateEvents()
    this.eqsServicesEvent = new CustomEvent('eqsServicesEvent') //an event to dispatch
    this.populate()
  }
  
  /**
   * Initiates the event listeners for this items dom
   */
  initiateEvents(){
    //setup add button
    const addFunc = () => {
      new ServicesModal(this.data.jobID, this.addServiceOffering.bind(this))
    }
    this.dom.addService.addEventListener('click', addFunc.bind(this))
    this.dom.addText.addEventListener('click', addFunc.bind(this))
  }
  
  /**
   * Adds a service offerering to the database and adds an item
   * @param data
   */
  addServiceOffering(data){
    if(data == null) return
      
    data.action = 'createServiceOffering'
    fetch('servicesPRG.php', {
      method: 'post',
      headers: {'Content-Type': 'application/json;charset=utf8'},
      body: JSON.stringify(data)
    })
      .then(res => res.json())
      .then(body => {
        if(body.ok){
          //crew new services item
          data.action = undefined
          this.items.push(new ServicesItem(this, data))
          document.dispatchEvent(this.eqsServicesEvent) //dispatch the change event
        }else{
          note({
            class: 'error',
            text: 'There has been an error. The service offerering has not been added.',
            time: 2500
          })
        }
      })
  }
  
  /**
   * Populates the services page from this.data
   */
  populate(){
    for (const itemsKey in this.data.items) {
      if(this.data.items.hasOwnProperty(itemsKey)){
        this.items.push(new ServicesItem(this, this.data.items[itemsKey]))
      }
    }
  }
  
  /**
   * Clears all of the service items
   */
  clear(){
    for (const itemsKey in this.items) {
      if(this.items.hasOwnProperty(itemsKey)){
        this.items[itemsKey].remove()
      }
    }
    this.items = []
  }
  
}
  
