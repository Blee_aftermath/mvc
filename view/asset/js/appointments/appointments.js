import {AppointmentsItem} from "./appointmentsItem.js"
import {Util} from "../util.js"

/**
 * Class that handles the multi-appointments tab
 * @author Jake Cirino
 */
export class Appointments{
  /**
   * Creates a new appointments tab
   * @param {Object} data
   */
  constructor(data) {
    this.data = data
    this.items = []
    this.baseElement = document.querySelector('[name=tabAppointments]')
    this.dom = Util.parseDOM(this.baseElement)
    
    console.log(this.data)
    window.updateCrew = this.updateCrew.bind(this)
    
    this.populate()
    
    this.dom.addButton.addEventListener('click', this.addItem.bind(this))
    this.dom.addText.addEventListener('click', this.addItem.bind(this))
  }
  
  /**
   * Populates the appointment items
   */
  populate(){
    for (const appointmentsKey in this.data.appointments) {
      const item = this.data.appointments[appointmentsKey]
      
      this.items.push(new AppointmentsItem(this, item))
    }
  }
  
  /**
   * Sorts the appointment items in this list by date
   */
  sort(){
    //sort items in the list
    let sorted = this.items
    sorted.sort((a, b) => {
      let tsA = a.getTimestamp(), tsB = b.getTimestamp()
      if(tsA < tsB) return -1
      else if(tsA > tsB) return 1
      return 0
    })
  
    //remove unsorted from dom
    for (const itemsKey in this.items) {
      this.items[itemsKey].baseElement.remove()
    }
  
    //readd items in sorted order
    for (const sortedKey in sorted) {
      const domItem = sorted[sortedKey].baseElement
      this.baseElement.appendChild(domItem)
    }
  }
  
  /**
   * Queries the data
   * base to add a new item to the appointments
   */
  addItem(){
    fetch('appointmentsPRG.php', {
      method: 'post',
      headers: {'Content-Type': 'application/json;charset=utf8'},
      body: JSON.stringify({
        action: 'createAppt',
        jobID: this.data.jobID
      })
    })
      .then(res => res.json())
      .then(body => {
        if(body.ok){
          this.items.push(new AppointmentsItem(this, body.data))
          this.sort()
        }else{
          //TODO error message
        }
      })
  }
  
  /**
   * Updates the crew for this appointment selector
   */
  updateCrew(){
    fetch('flexTimesheetPRG.php', {
      method: 'post',
      headers: {'Content-Type': 'application/json;charset=utf8'},
      body: JSON.stringify({
        action: 'getJobCrew',
        jobID: this.data.jobID
      })
    })
      .then(res => res.json())
      .then(body => {
        if(body.ok){
          window.crew = body.data
          for (const itemsKey in this.items) {
            this.items[itemsKey].createCrewItems()
          }
        }
      })
  }
}