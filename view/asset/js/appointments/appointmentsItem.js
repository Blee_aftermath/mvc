/**
 * Represents a single appointment in a multi-appointment tab
 * @author Jake Cirino
 */
import {Util} from "../util.js"

export class AppointmentsItem{
  /**
   * Creates a new appointments item
   * @param {Appointments} parent
   * @param {Object} data
   */
  constructor(parent, data) {
    this.data = data
    this.parent = parent
    console.log(this.data)
    
    //create base element
    this.baseElement = document.createElement('div')
    this.baseElement.innerHTML = window.aptItemTemplate
    this.dom = Util.parseDOM(this.baseElement)
    
    //initialize and populate the UI
    this.initialize()
    this.populate()
    
    //append html to parent
    this.parent.baseElement.appendChild(this.baseElement)
  }
  
  /**
   * Initializes UI functionality
   */
  initialize(){
    this.createCrewItems()
    
    this.dom.deleteButton.addEventListener('click', this.delete.bind(this))
    this.dom.saveButton.addEventListener('click', this.save.bind(this))
    
    //initialize datepicker
    const changeFunc = () => {
      this.dom.saveButton.disabled = false
    }
    Util.initiateDatePicker(this.dom.dateAppt, changeFunc.bind(this))
  }
  
  populate(){
    //populate dt
    const startDT = this.data.japt_start_dt.split(' ')
    this.dom.dateAppt.value = startDT[0]
    this.dom.timeAppt.value = Util.parseTime24(startDT[1])
    
    //time
    this.dom.japt_hours.value = Number(this.data.japt_hours).toFixed(2)
    this.dom.japt_info.value = this.data.japt_info
  }
  
  /**
   * Generates the list view of employees
   */
  createCrewItems(){
    this.dom.empList.innerHTML = ''
    for (const crewKey in window.crew) {
      const data = window.crew[crewKey]
      let listItem = document.createElement('li')
      listItem.innerHTML = `
        <input name='${data.crew_emp}' type='checkbox'/>
        ${data.emp_fname} ${data.emp_lname}`
      
      this.dom.empList.appendChild(listItem)
    }
    
    this.empDom = Util.parseDOM(this.dom.empList)
  
    //initialize change functions
    const changeFunc = () => {
      this.dom.saveButton.disabled = false
    }
    const inputItems = this.baseElement.querySelectorAll('input')
    for (const inputItemsKey in inputItems) {
      if(inputItems.hasOwnProperty(inputItemsKey)){
        inputItems[inputItemsKey].addEventListener('change', changeFunc)
      }
    }
  
    //set emps checked
    for (const empsKey in this.data.emps) {
      if(this.data.emps.hasOwnProperty(empsKey)){
        const empID = this.data.emps[empsKey]
        try{
          this.empDom[empID].checked = true
        }catch(ex){}
      }
    }
  }
  
  /**
   * Gets the MS timestamp of the currently set date/time
   * @returns {number}
   */
  getTimestamp(){
    return Util.parseDate(this.dom.dateAppt.value, this.dom.timeAppt.value).getTime()
  }
  
  /**
   * Saves changes for this object in the database
   */
  save(){
    this.dom.saveButton.disabled = true
    
    //prepare appt/appt emp data
    let saveData = {
      action: 'updateAppt',
      aptID: this.data.japt_id,
      japt_start_dt: Util.dateToString(Util.parseDate(this.dom.dateAppt.value, this.dom.timeAppt.value)),
      japt_hours: Number(this.dom.japt_hours.value).toFixed(2),
      japt_info: this.dom.japt_info.value
    }
    
    let saveList = [], delList = []
    for (const empDomKey in this.empDom) {
      if(this.empDom.hasOwnProperty(empDomKey)){
        const item = this.empDom[empDomKey]
        if(item.checked) saveList.push(item.name)
        else delList.push(item.name)
      }
    }
    let empData = {
      action: 'updateEmps',
      aptID: this.data.japt_id,
      save: saveList,
      del: delList
    }
    
    //run fetch requests
    fetch('appointmentsPRG.php', {
      method: 'post',
      headers: {'Content-Type': 'application/json;charset=utf8'},
      body: JSON.stringify(saveData)
    })
      .then(res => res.json())
      .then(body => {
        if(body.ok){
          fetch('appointmentsPRG.php', {
            method: 'post',
            headers: {'Content-Type': 'application/json;charset=utf8'},
            body: JSON.stringify(empData)
          })
            .then(res => res.json())
            .then(body => {
              if(body.ok){
                //TODO
                this.parent.sort()
              }else{
                //TODO error msg
                this.dom.saveButton.disabled = false
              }
            })
        }else{
          //TODO error
          this.dom.saveButton.disabled = false
        }
      })
    
  }
  
  /**
   * Deletes this item from the database then removes it from the dom
   */
  delete(){
    fetch('appointmentsPRG.php', {
      method: 'post',
      headers: {'Content-Type': 'application/json;charset=utf8'},
      body: JSON.stringify({
        action: 'deleteAppt',
        aptID: this.data.japt_id
      })
    })
      .then(res => res.json())
      .then(body => {
        if(body.ok){
          this.remove()
        }else{
          //TODO show error
        }
      })
  }
  
  /**
   * Removes this item from the dom
   */
  remove(){
    this.baseElement.remove()
    delete this
  }
}