
var arrOrlnOb   = []; // Array of Orln objects
var arrOrlnID   = []; // Array of Orln IDs
var arrOrlnEr   = []; // Array of errored fields to tag in red

function fnAddOrln() {
  document.getElementById("spanOrlnHead").innerHTML     = "Add Orln";
  document.getElementById("hidOrlnID").disabled         = true;
  document.getElementById("subAddOrln").style.display   = "inline";
  document.getElementById("subChgOrln").style.display   = "none";
  document.getElementById("butConOrln").style.display   = "none";
  document.getElementById("myModalOrln").style.display  = "block";
}

function fnChgOrln(id) {
  jsIndex = arrOrlnID.indexOf(id);
  document.getElementById("spanOrlnHead").innerHTML     = "Edit Orln";
  document.getElementById("hidOrlnID").disabled         = false;
  document.getElementById("subAddOrln").style.display   = "none";
  document.getElementById("subChgOrln").style.display   = "inline";
  document.getElementById("butConOrln").style.display   = "inline";
  document.getElementById("myModalOrln").style.display  = "block";
  fnFillOrln(jsIndex);
}

function fnFillOrln(jsIndex) {
  var radID = "radOrlnSku_" + arrOrlnOb[jsIndex].sku;
  document.getElementById("hidOrlnID").value            = arrOrlnID[jsIndex];
  document.getElementById("inpOrlnQtyReq").value        = arrOrlnOb[jsIndex].qty;
  document.getElementById(radID).checked                = true;
}

function fnConfirmDelOrln() {
  strConfirm  = "<div class='content alert'>";
  strConfirm += "Delete this item?&nbsp;&nbsp;";
  strConfirm += "<input class='inp08' type='submit' name='subDelOrlnY' value='Yes' />";
  strConfirm += "<input class='inp08' type='button' name='butDelOrlnN' value='No' onclick='fnDelOrlnN()' />";
  strConfirm += "</div>";
  document.getElementById("spanOrlnConfirm").innerHTML = strConfirm;
}

function fnDelOrlnN() {
  document.getElementById("spanOrlnConfirm").innerHTML = "";
}

function fnCloseModalOrln() {
  document.getElementById("hidOrlnID").value            = "";
  document.getElementById("inpOrlnQtyReq").value        = "";
  document.getElementById("myModalOrln").style.display  = "none";
  for(var i=0; i<arrOrlnEr.length; i++) {
    document.getElementById(arrOrlnEr[i]).classList.remove("err");
  }
  checkboxes = document.getElementsByName('radOrlnSku[]');
  for (var i = 0; i < checkboxes.length; i++) {
    checkboxes[i].checked = false;
  }
}