<?php
// ==================================================================================================== //
// === FUNCTIONS === //
// ==================================================================================================== //

function fnFillRefJS($myRes) {

    echo "\n\n<script language=\"JavaScript\">";
    echo "\n<!-- Hide the script from old browsers --";

    echo "\nvar objRef = {};"; // Object of Job fields.  Can also = new Object();

    echo "\nobjRef.id            = \"" . $myRes['job_id']                    . "\";";
    echo "\nobjRef.ref           = \"" . $myRes['ref_id']                    . "\";";
    echo "\nobjRef.contact       = \"" . $myRes['job_ref_contact']           . "\";";
    echo "\nobjRef.company       = \"" . $myRes['job_ref_company']           . "\";";
    echo "\nobjRef.telephone     = \"" . $myRes['job_ref_telp']              . "\";";
    echo "\nobjRef.other         = \"" . $myRes['job_ref_other']                 . "\";";

    echo "\narrRefID.push(" . $myRes['job_id'] . ");";
    echo "\narrRefOb.push(objRef);"; // Push this object into the array of Objects
    echo "\n// --End Hiding Here -->";
    echo "\n</script>\n";
  }

  function fnFillRefPost() {

    echo "\n<script language=\"JavaScript\">";
    echo "\n<!-- Hide the script from old browsers --";

    echo "\nvar objRef = {};"; // Create a new object to hold the posted values.

    if (isset($_SESSION['inpValue']['hidRefID'])) {
      echo "\narrRefID.push(\"" . prepareJS($_SESSION['inpValue']['hidJobID']) . "\");";
      echo "\nobjRef.id     = \"" . prepareJS($_SESSION['inpValue']['hidJobID']) . "\";";
    }

    echo "\nobjRef.ref           = \"" . prepareJS($_SESSION['inpValue']['selRefSource'])    . "\";";
    echo "\nobjRef.contact       = \"" . prepareJS($_SESSION['inpValue']['inpRefName'])      . "\";";
    echo "\nobjRef.company       = \"" . prepareJS($_SESSION['inpValue']['inpRefCompany'])   . "\";";
    echo "\nobjRef.telephone     = \"" . prepareJS($_SESSION['inpValue']['inpRefTel'])       . "\";";
    echo "\nobjRef.other         = \"" . prepareJS($_SESSION['inpValue']['inpRefOther'])     . "\";";
    
    echo "\narrRefOb.push(objRef);"; // Tack this onto the end of the array of Referral objects.

    foreach($_SESSION['errField'] AS $field) {
      echo "\narrRefEr.push(\"" . $field    . "\");";
      echo "\ndocument.getElementById(\"" . $field . "\").classList.add(\"err\");";
    }

    // Show the modal dialog box

    if (isset($_SESSION['inpValue']['subChgRef'])) echo "\nfnChgReferral(" . $_SESSION['inpValue']['hidJobID'] . ");";
    echo "\nfnFillReferral(arrRefOb.length-1);";

    echo "// --End Hiding Here -->\n";
    echo "\n</script>\n";

  }
  // ==================================================================================================== //
// === MODAL === //
// ==================================================================================================== //

  echo "<div class='modal' id='myModalReferral'>";
  echo "<div class='fixed'>";
  echo "<div class='body_stripe'></div>";

  echo "<div class='head'>";
  echo "<div class='content'>";

  echo "Referral Information <span id='spanAddressID'></span>";
  echo "<span style='float:right;'><a href='javascript:fnCloseModalReferral();'><img class='icon16' src='icons/no16.png' alt='Close' title='Close' /></a></span>";

  echo "</div>"; // content
  echo "</div>"; // head

  echo "<span id='spanRefMessages'>";
  fnModalMessagesBox('Referral');
  echo "</span>";

  echo "<div class='body'>";
  echo "\n<form method='post' name='frmReferral' action='jifPRG.php'>";
  echo "<div class='content'>";

   if (isset($valJobID)) {
    $inpField = "hidJobID";
    $defValue = $valJobID;
    echo "<input type='hidden' name='$inpField' id='$inpField' value='" . $defValue . "' />";
  }
  echo "<table class='info'>";

  $defValue = ""; // set by JS for all fields in the modal table
  $checked  = ""; // set by JS for all fields in the modal table


  $sqlSelRefSource = "SELECT * FROM ref_source";
  $resSelRefSource = $mysqli->query($sqlSelRefSource);

  $inpField = "selRefSource";
  $defValue = "";
  $defClass = "req";
  echo "<tr class='content'><td class='l'><label for='$inpField'>How did you hear about Aftermath Services ? :</label></td>";
  echo "<td class='r'><select class='$defClass' name='$inpField' id='$inpField' onchange='fnSelectOther(this.value)'>";
  while ($mySelRefSource = $resSelRefSource->fetch_assoc()) {
    echo "<option value='" . $mySelRefSource['ref_id'] . "'";
    if ($mySelRefSource['ref_id'] == $defValue) { echo " selected='selected'"; }
     echo ">" . prepareWeb($mySelRefSource['ref_name']) . "</option>";
  }
  echo "</select><br>";
 
  $inpField = "inpRefOther";
  $defClass = "opt Jifother";
  echo "<input class='" . $defClass . "' type='text' name='" . $inpField . "' id='" . $inpField . "' value='" . $defValue . "' maxlength='$maxName' />";

  echo "</td></tr>";
 
 
  $inpField = "inpRefName";
  $defValue = "";
  $defClass = "opt";
  echo "<tr class='content'><td class='l'>Name of person whol referred :</td>";
  echo "<td class='r'><input class='" . $defClass . "' type='text' name='" . $inpField . "' id='" . $inpField . "' value='" . $defValue . "' maxlength='$maxName' />";
  echo "</td></tr>";   

  $inpField = "inpRefCompany";
  $inpLabel = "Police Department / Company who referred: ";
  $defValue = "";
  $defClass = "opt";

  echo "<tr class='content'><td class='l'>$inpLabel</td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' value='$defValue' autocomplete='off'/>";

  echo "<tr class='content'><td class='l'>Telephone: </td>";
  $inpField = "inpRefTel";
  $defValue = "";
  $defClass = "opt";
  echo "<td class='r'><input class='" . $defClass . "' type='text' id='" . $inpField . "' name='" . $inpField . "' value='" . $defValue . "' maxlength='$maxName' />";
  echo "</td></tr>";
 

  $arrButtonRow[] = "<input class='inp12' type='submit' name='subChgRef' id='subChgRef' value='Save' />";
  $arrButtonRow[] = "<input class='inp08' type='button' name='subCan' id='subCan' value='Cancel' onclick='fnCloseModalReferral();' />";

  echo "<tr><td></td><td>" . implode("", $arrButtonRow) . "</td></tr>";

  echo "</table>";

  echo "</div>"; // content
  echo "<span id='spanConfirm'></span>";
  echo "</form>";
  echo "</div>"; // body

  echo "<div class='body_stripe'></div>";
  echo "</div>"; // fixed
  echo "</div>"; // modal

  unset($arrButtonRow);

?>