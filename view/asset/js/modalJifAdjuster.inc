<?php
 echo "\n\n<script language=\"JavaScript\">";
  echo "\n<!-- Hide the script from old browsers --";

  echo "\nvar arrAdjuster = [];"; // Array of adjuster objects for this job
  echo "\nvar arrError   = [];"; // Array of errored fields to tag in red
  
  echo "\nfunction fnFillFormAdjuster(id) {";
  echo "\n  document.getElementById(\"inpCFName\").value    = arrAdjuster[id].fname;";
  echo "\n  document.getElementById(\"inpCLName\").value    = arrAdjuster[id].lname;";
  echo "\n  document.getElementById(\"inpCTitle\").value    = arrAdjuster[id].title;";
  echo "\n  document.getElementById(\"inpCEmail\").value    = arrAdjuster[id].email;";
  echo "\n  document.getElementById(\"inpCTelp\").value     = arrAdjuster[id].telp;";
  echo "\n  document.getElementById(\"inpCTelc\").value     = arrAdjuster[id].telc;";
  echo "\n  document.getElementById(\"inpCTelf\").value     = arrAdjuster[id].telf;";
  echo "\n  document.getElementById(\"inpCTelpX\").value    = arrAdjuster[id].telpx;";
  echo "\n  document.getElementById(\"inpCInfo\").value     = arrAdjuster[id].info;";
  echo "\n  document.getElementById(\"selInsco\").value     = arrAdjuster[id].insco;";
  echo "\n}";
 

  echo "\nfunction fnEditAdjuster(id) {";
  echo "\n  fnFillFormAdjuster(id);";
  echo "\n  document.getElementById(\"myModalAdjuster\").style.display = \"block\";";
  echo "\n  document.getElementById(\"spanAdjusterID\").innerHTML = arrAdjuster[id].id + \" " . $delim . " Edit\";";
  echo "\n  document.getElementById(\"hidContactID\").value       = arrAdjuster[id].id;";
  echo "\n}";

  echo "\nfunction fnCloseAdjuster() {";
  echo "\n  document.getElementById(\"inpCFName\").value          = \"\";";
  echo "\n  document.getElementById(\"inpCLName\").value          = \"\";";
  echo "\n  document.getElementById(\"inpCTitle\").value          = \"\";";
  echo "\n  document.getElementById(\"inpCEmail\").value          = \"\";";
  echo "\n  document.getElementById(\"inpCTelp\").value           = \"\";";
  echo "\n  document.getElementById(\"inpCTelc\").value           = \"\";";
  echo "\n  document.getElementById(\"inpCTelf\").value           = \"\";";
  echo "\n  document.getElementById(\"inpCTelpX\").value          = \"\";";
  echo "\n  document.getElementById(\"inpCInfo\").value           = \"\";";
  echo "\n  document.getElementById(\"selInsco\").value           = \"\";";
  echo "\n  document.getElementById(\"myModalAdjuster\").style.display   = \"none\";";
  echo "\n}";

  echo "\n// --End Hiding Here -->";
  echo "\n</script>\n";

  if (isset($valJobID)) {
    $sqlContact  = "SELECT * FROM job_con";
    $sqlContact .= " LEFT JOIN contact ON jc_con = con_id";
    $sqlContact .= " WHERE jc_job = $valJobID";

       if ($resContact = $mysqli->query($sqlContact)) {
          while ($myContact = $resContact->fetch_assoc()) {

            echo "\n<script type=\"text/javascript\">";
            echo "\n<!-- Hide the script from old browsers --";

            echo "\nvar objAdjuster = {};"; // Object of adjuster fields for this contact.  Can also = new Object();
            echo "\nobjAdjuster.id      = \"" .prepareJS($myContact['con_id']) . "\";";
            echo "\nobjAdjuster.fname   = \"" . prepareJS($myContact['con_fname'])    . "\";";
            echo "\nobjAdjuster.lname   = \"" . prepareJS($myContact['con_lname'])    . "\";";
            echo "\nobjAdjuster.title   = \"" . prepareJS($myContact['con_title'])    . "\";";
            echo "\nobjAdjuster.email   = \"" . prepareJS($myContact['con_email'])    . "\";";
            echo "\nobjAdjuster.telp    = \"" . prepareJS($myContact['con_telp'])     . "\";";
            echo "\nobjAdjuster.telc    = \"" . prepareJS($myContact['con_telc'])     . "\";";
            echo "\nobjAdjuster.telf    = \"" . prepareJS($myContact['con_telf'])     . "\";";
            echo "\nobjAdjuster.telpx   = \"" . prepareJS($myContact['con_telpx'])    . "\";";
            echo "\nobjAdjuster.info    = \"" . prepareJS($myContact['con_info'])     . "\";";
            echo "\nobjAdjuster.insco   = \"" . prepareJS($myContact['con_insco'])     . "\";";


           /* $valInsco      = $myContact['con_insco'];
            $webTelp       = formatPhone($myContact['con_telp']);
            $webTelc       = formatPhone($myContact['con_telc']);
            $webTelf       = formatPhone($myContact['con_telf']);
            $webTelpX      = prepareWeb($myContact['con_telpx']);
            $webInscoName  = prepareWeb($myContact['insco_name']);
            $frmInfo       = prepareFrm($myContact['con_info']);
            $webInfo       = prepareWeb($myContact['con_info']);
            $webDTAdd      = (is_null($myContact['con_dt_add']) ? "" : formatDTText(strtotime($myContact['con_dt_add'])));
            $valPreNumInv  = $myContact['con_pre_num_inv'];
            $penPreAmtInv  = $myContact['con_pre_amt_inv'];
            $webPreAmtInv  = "$" . number_format($myContact['con_pre_amt_inv']/100, 2);
            $penPreAmtCol  = $myContact['con_pre_amt_col'];
            $webPreAmtCol  = "$" . number_format($myContact['con_pre_amt_col']/100, 2);
            $valPrePerReal = $myContact['con_pre_per_real'];*/

            echo "\narrAdjuster[" . $myContact['con_id'] . "] = objAdjuster;"; // Push this object into the array of adjusters

            echo "\n// --End Hiding Here -->";
            echo "\n</script>\n";

          } 
    } else { $_SESSION['errInfo'] = "Query error: Contact."; }
  } // if ($valJobID)
 
  echo "<div class='modal' id='myModalAdjuster'>";
  echo "<div class='fixed'>";
  echo "<div class='body_stripe'></div>";

  echo "<div class='head'>";
  echo "<div class='content'>";

  echo "Contact <span id='spanAdjusterID'></span>";
  echo "<span style='float:right;'><a href='javascript:fnCloseAdjuster();'><img class='icon16' src='icons/no16.png' alt='Close' title='Close' /></a></span>";

  echo "</div>"; // content
  echo "</div>"; // head

  echo "<span id='spanMessages'>";
  fnModalMessagesBox('Contact');
  echo "</span>";

  echo "<div class='body'>";
  echo "\n<form method='post' name='frmContact' action='jifPRG.php'>";
  echo "<div class='content'>";

  $inpField = "hidContactID";
  $defValue = ""; // set by JS
  echo "<input type='hidden' name='$inpField' id='$inpField' value='" . $defValue . "' />";

  if (isset($valJobID)) {
    $inpField = "hidJobID";
    $defValue = $valJobID;
    echo "<input type='hidden' name='$inpField' id='$inpField' value='" . $defValue . "' />";
  }

    echo "<table class='info info3'>";
    echo "<tr class='content'><td>Name:</td>";
    echo "<td class='r'>";
    $inpField = "inpCFName";
    $defClass = "inp08 " . (isset($_SESSION['errField']) ? (in_array($inpField, $_SESSION['errField']) ? "err" : "req") : "req");
    $defValue = "";
    echo "<input class='" . $defClass . "' type='text' name='" . $inpField . "'  id='" . $inpField . "'  value='" . $defValue . "' maxlength='$maxName' />";

    $inpField = "inpCLName";
    $defClass = "inp08 " . (isset($_SESSION['errField']) ? (in_array($inpField, $_SESSION['errField']) ? "err" : "req") : "req");
    $defValue = "";
    echo "<input class='" . $defClass . "' type='text' name='" . $inpField . "'  id='" . $inpField . "'  value='" . $defValue . "' maxlength='$maxName' />";
    echo "</td></tr>";

    echo "<tr class='content'><td>Title:</td>";
    echo "<td class='r'>";
    $inpField = "inpCTitle";
    $defClass = (isset($_SESSION['errField']) ? (in_array($inpField, $_SESSION['errField']) ? "err" : "opt") : "opt");
    $defValue = "";
    echo "<input class='" . $defClass . "' type='text' name='" . $inpField . "' id='" . $inpField . "' value='" . $defValue . "' maxlength='$maxName' />";
    echo "</td></tr>";

    echo "<tr class='content'><td>Email:</td>";
    $inpField = "inpCEmail";
    $defClass = (isset($_SESSION['errField']) ? (in_array($inpField, $_SESSION['errField']) ? "err" : "opt") : "opt");
    $defValue = "";
    echo "<td class='r'><input class='$defClass' type='text' name='$inpField' id='" . $inpField . "' value='$defValue' maxlength='$maxEmail' />";
    echo "</td></tr>";

    echo "<tr class='content'><td class='l'>Phone / Ext:</td>";
    echo "<td class='r'>";

    $inpField = "inpCTelp";
    $defClass = (isset($_SESSION['errField']) ? (in_array($inpField, $_SESSION['errField']) ? "err" : "opt") : "opt");
    $defValue = "";
    echo "<input class='" . $defClass . "' type='text' name='" . $inpField . "'  id='" . $inpField . "' value='" . $defValue . "' maxlength='$maxTel' />";

    $inpField = "inpCTelpX";
    $defClass = "inp12 " . (isset($_SESSION['errField']) ? (in_array($inpField, $_SESSION['errField']) ? "err" : "opt") : "opt");
    $defValue = "";
    echo "<input class='" . $defClass . "' style='margin-left:9px;' type='text' name='" . $inpField . "' id='" . $inpField . "' value='" . $defValue . "' maxlength='$maxExt' />";

    echo "</td></tr>";

    echo "<tr class='content'><td>Cell:</td>";
    $inpField = "inpCTelc";
    $defClass = (isset($_SESSION['errField']) ? (in_array($inpField, $_SESSION['errField']) ? "err" : "opt") : "opt");
    $defValue = "";
    echo "<td class='r'><input class='" . $defClass . "' type='text' name='" . $inpField . "' id='" . $inpField . "' value='" . $defValue . "' maxlength='$maxTel' />";
    echo "</td></tr>";

    echo "<tr class='content'><td>Fax:</td>";
    $inpField = "inpCTelf";
    $defClass = (isset($_SESSION['errField']) ? (in_array($inpField, $_SESSION['errField']) ? "err" : "opt") : "opt");
    $defValue = "";
    echo "<td class='r'><input class='" . $defClass . "' type='text' name='" . $inpField . "' id='" . $inpField . "' value='" . $defValue . "' maxlength='$maxTel' />";
    echo "</td></tr>";

    $sqlSelect = "SELECT * FROM insco ORDER BY insco_name";
    $resSelect = $mysqli->query($sqlSelect);

    $inpField = "selInsco";
    $defClass = (isset($_SESSION['errField']) ? (in_array($inpField, $_SESSION['errField']) ? "err" : "opt") : "opt");
    $defValue = "";
    echo "<tr class='content'><td class='l'><label for='$inpField'>Carrier:</label></td>";
    echo "<td class='r'><select class='$defClass' name='$inpField' id='$inpField'>";
    echo "<option value='' selected='selected'>--- Select Carrier ---</option>";
    while ($mySelect = $resSelect->fetch_assoc()) {
      echo "<option value='" . $mySelect['insco_id'] . "'";
      if ($mySelect['insco_id'] == $defValue) { echo " selected='selected'"; }
      echo ">" . prepareWeb($mySelect['insco_name']) . "</option>";
    }
    echo "</select>";
    echo "</td></tr>";

    $inpField = "inpCInfo";
    $defClass = (isset($_SESSION['errField']) ? (in_array($inpField, $_SESSION['errField']) ? "err" : "opt") : "opt");
    $defValue = "";
    echo "<tr class='content'><td class='l'>Additional<br />Information:</td>";
    echo "<td class='r'><textarea class='$defClass' name='$inpField' id='$inpField' rows='6' cols='48'>$defValue</textarea>";
    echo "</td></tr>";

    echo "<tr class='content'><td>&nbsp;</td><td>";
    echo "<input class='inp12' type='submit' name='subChgContact' value='Save' />";
    echo "&nbsp;<input class='inp12' type='button' name='subCanContact' value='Cancel' onclick='fnCloseAdjuster();' />";
    echo "</td></tr>";

    echo "</table>";

  echo "</div>"; // content
  echo "<span id='spanConfirm'></span>";
  echo "</form>";
  echo "</div>"; // body

  echo "<div class='body_stripe'></div>";
  echo "</div>"; // fixed
  echo "</div>"; // modal
 
  if (isset($_SESSION['inpValue']['subChgContact'])) {
    echo "\n<script language=\"JavaScript\">";
    echo "\n<!-- Hide the script from old browsers --";

    echo "\nvar objAdjuster = {};"; // Create a new object to hold the posted values, otherwise it updates the last one displayed earlier

    if (isset($_SESSION['inpValue']['hidContactID'])) {
      echo "\nobjAdjuster.id     = \"" . prepareJS($_SESSION['inpValue']['hidContactID']) . "\";";
    }

    echo "\nobjAdjuster.fname    = \"" . prepareJS($_SESSION['inpValue']['inpCFName'])    . "\";";
    echo "\nobjAdjuster.lname    = \"" . prepareJS($_SESSION['inpValue']['inpCLName'])    . "\";";
    echo "\nobjAdjuster.title    = \"" . prepareJS($_SESSION['inpValue']['inpCTitle'])    . "\";";
    echo "\nobjAdjuster.email    = \"" . prepareJS($_SESSION['inpValue']['inpCEmail'])    . "\";";
    echo "\nobjAdjuster.telp     = \"" . formatPhone($_SESSION['inpValue']['inpCTelp'])   . "\";";
    echo "\nobjAdjuster.telc     = \"" . formatPhone($_SESSION['inpValue']['inpCTelc'])   . "\";";
    echo "\nobjAdjuster.telf     = \"" . formatPhone($_SESSION['inpValue']['inpCTelf'])   . "\";";
    echo "\nobjAdjuster.telpx    = \"" . prepareJS($_SESSION['inpValue']['inpCTelpX'])    . "\";";
    echo "\nobjAdjuster.info     = \"" . prepareJS($_SESSION['inpValue']['inpCInfo'])     . "\";";
    echo "\nobjAdjuster.insco    = \"" . prepareJS($_SESSION['inpValue']['selInsco'])     . "\";";

    foreach($_SESSION['errField'] AS $field) {

      echo "\narrError.push(\"" . $field    . "\");";
      echo "\ndocument.getElementById(\"" . $field . "\").classList.add(\"err\");";

    }

     echo "\narrAdjuster[0] = objAdjuster;"; // Put this object into the array of adjuster

     if (isset($_SESSION['inpValue']['subChgContact'])) { echo "\nfnEditAdjuster(0);"; }

    echo "// --End Hiding Here -->\n";
    echo "\n</script>\n";
  }
  
 unset($arrButtonRow);

?>