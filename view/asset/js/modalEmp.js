
var arrEmpOb   = []; // Array of Emp objects
var arrEmpID   = []; // Array of Emp IDs
var arrEmpEr   = []; // Array of errored fields to tag in red

function fnAddEmp() {
  document.getElementById("spanEmpHead").innerHTML     = "Add Employee";
  document.getElementById("rowEmpID").hidden           = true;
  document.getElementById("reoEmpID").disabled         = true;
  document.getElementById("subEmp").value              = "Add";
//  document.getElementById("subAddEmp").style.display   = "inline";
//  document.getElementById("subChgEmp").style.display   = "none";
  document.getElementById("butConEmp").style.display   = "none";
  document.getElementById("myModalEmp").style.display  = "block";
}

function fnChgEmp(id) {
  jsIndex = arrEmpID.indexOf(id);
  document.getElementById("spanEmpHead").innerHTML     = "Edit Employee";
  document.getElementById("rowEmpID").hidden           = false;
  document.getElementById("reoEmpID").disabled         = false;
  document.getElementById("subEmp").value              = "Save";
//  document.getElementById("subAddEmp").style.display   = "none";
//  document.getElementById("subChgEmp").style.display   = "inline";
  document.getElementById("butConEmp").style.display   = "inline";
  document.getElementById("myModalEmp").style.display  = "block";
  fnFillEmp(jsIndex);
}

function fnFillEmp(jsIndex) {
  document.getElementById("reoEmpID").value            = arrEmpID[jsIndex];
  document.getElementById("inpEmpFName").value         = arrEmpOb[jsIndex].fname;
  document.getElementById("inpEmpLName").value         = arrEmpOb[jsIndex].lname;
  document.getElementById("selEmpTitle").value         = arrEmpOb[jsIndex].title;
  document.getElementById("selEmpOffice").value        = arrEmpOb[jsIndex].office;
  document.getElementById("selEmpPay").value           = arrEmpOb[jsIndex].pay;
  document.getElementById("selEmpCCStatus").value      = arrEmpOb[jsIndex].ccstatus;
  document.getElementById("inpEmpPayID").value         = arrEmpOb[jsIndex].pay_id;
  document.getElementById("inpEmpUser").value          = arrEmpOb[jsIndex].user;
  document.getElementById("inpEmpEmail").value         = arrEmpOb[jsIndex].email;
  document.getElementById("inpEmpEmailAlt").value      = arrEmpOb[jsIndex].email_alt;
  document.getElementById("inpEmpTelc").value          = arrEmpOb[jsIndex].telc;
  document.getElementById("inpEmpTelp").value          = arrEmpOb[jsIndex].telp;
  document.getElementById("inpDHire").value            = arrEmpOb[jsIndex].d_hire;
  document.getElementById("inpDRehire").value          = arrEmpOb[jsIndex].d_rehire;
  document.getElementById("inpDHepv").value            = arrEmpOb[jsIndex].d_hepv;
  document.getElementById("inpDPPTrain").value         = arrEmpOb[jsIndex].d_pptrain;
  document.getElementById("inpDCETrain").value         = arrEmpOb[jsIndex].d_cetrain;
  document.getElementById("inpDERailSafe").value       = arrEmpOb[jsIndex].d_erailsafe;
  document.getElementById("inpDReady").value           = arrEmpOb[jsIndex].d_ready;
  document.getElementById("inpDTerm").value            = arrEmpOb[jsIndex].d_term;
  document.getElementById("inpEmpInfo").value          = arrEmpOb[jsIndex].info;
  document.getElementById("chkEmpIsIICRC").checked     = (arrEmpOb[jsIndex].is_iicrc == 1 ? true : false);
  document.getElementById("chkEmpIsAct").checked       = (arrEmpOb[jsIndex].is_act == 1 ? true : false);
  document.getElementById("chkEmpIsEna").checked       = (arrEmpOb[jsIndex].is_ena == 1 ? true : false);
  document.getElementById("chkEmpIsTrn").checked       = (arrEmpOb[jsIndex].is_trn == 1 ? true : false);
  document.getElementById("chkEmpNotify").checked      = (arrEmpOb[jsIndex].notify == 1 ? true : false);
}

function fnConfirmDelEmp() {
  strConfirm  = "<div class='content alert'>";
  strConfirm += "Delete this Employee?&nbsp;&nbsp;";
  strConfirm += "<input class='inp08' type='submit' name='subDelEmpY' value='Yes' />";
  strConfirm += "<input class='inp08' type='button' name='butDelEmpN' value='No' onclick='fnDelEmpN()' />";
  strConfirm += "</div>";
  document.getElementById("spanEmpConfirm").innerHTML = strConfirm;
}

function fnDelEmpN() {
  document.getElementById("spanEmpConfirm").innerHTML = "";
}

function fnCloseModalEmp() {
  document.getElementById("reoEmpID").value            = "";
  document.getElementById("inpEmpFName").value         = "";
  document.getElementById("inpEmpLName").value         = "";
  document.getElementById("selEmpTitle").value         = "";
  document.getElementById("selEmpOffice").value        = "";
  document.getElementById("selEmpPay").value           = "";
  document.getElementById("selEmpCCStatus").value      = "";
  document.getElementById("inpEmpPayID").value         = "";
  document.getElementById("inpEmpUser").value          = "";
  document.getElementById("inpEmpEmail").value         = "";
  document.getElementById("inpEmpEmailAlt").value      = "";
  document.getElementById("inpEmpTelc").value          = "";
  document.getElementById("inpEmpTelp").value          = "";
  document.getElementById("inpDHire").value            = "";
  document.getElementById("inpDRehire").value          = "";
  document.getElementById("inpDHepv").value            = "";
  document.getElementById("inpDPPTrain").value         = "";
  document.getElementById("inpDCETrain").value         = "";
  document.getElementById("inpDERailSafe").value       = "";
  document.getElementById("inpDReady").value           = "";
  document.getElementById("inpDTerm").value            = "";
  document.getElementById("inpEmpInfo").value          = "";
  document.getElementById("chkEmpIsIICRC").checked     = false;
  document.getElementById("chkEmpIsAct").checked       = false;
  document.getElementById("chkEmpIsEna").checked       = false;
  document.getElementById("chkEmpIsTrn").checked       = false;
  document.getElementById("chkEmpNotify").checked       = false;
  document.getElementById("myModalEmp").style.display  = "none";
  for(var i=0; i<arrEmpEr.length; i++) {
    document.getElementById(arrEmpEr[i]).classList.remove("err");
  }
  delete(arrEmpEr);
  document.getElementById("spanEmpMessages").innerHTML = "";
}
