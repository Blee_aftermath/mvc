<?php
  echo "<script type='module'>\n";
  echo "import {default as formModal} from './scripts/modules/ui/formModal.js';\n";
  echo "window.jsClkEvt['formModal'] = formModal;\n";
  echo "</script>\n";

  echo "<div class='modal modal-close' id='contactInfoModal' title='Contact Information'>\n";

  echo "<form action='contactPRG.php' method='post'>\n";

  $inpField = "subChgContact";
  echo "<input type='hidden' name='$inpField' />\n";

  $inpField = "subChgContactModal";
  echo "<input type='hidden' name='$inpField' />\n";

  $inpField = "hidContact";
  echo "<input type='hidden' name='$inpField' />\n";

  echo "<table class='info info3'>";

  echo "<tr class='content'><td>Name:</td>";
  echo "<td class='r'>";
  $inpField = "inpFName";
  $defClass = "inp08 req";
  echo "<input class='" . $defClass . "' type='text' name='" . $inpField . "' maxlength='$maxName' />";

  $inpField = "inpLName";
  $defClass = "inp08 req";
  echo "<input class='" . $defClass . "' style='margin-left:9px;' type='text' name='" . $inpField . "' maxlength='$maxName' />";
  echo "</td></tr>";

  echo "<tr class='content'><td>Title:</td>";
  echo "<td class='r'>";
  $inpField = "inpTitle";
  $defClass = "opt";
  echo "<input class='" . $defClass . "' type='text' name='" . $inpField . "' maxlength='$maxName' />";
  echo "</td></tr>";

  echo "<tr class='content'><td>Email:</td>";
  $inpField = "inpEmail";
  $defClass = "opt";
  echo "<td class='r'><input class='$defClass' type='text' name='$inpField' maxlength='$maxEmail' />";
  echo "</td></tr>";

  echo "<tr class='content'><td class='l'>Phone / Ext:</td>";
  echo "<td class='r'>";

  $inpField = "inpTelp";
  $defClass = "opt";
  echo "<input class='" . $defClass . "' type='text' name='" . $inpField . "' maxlength='$maxTel' />";

  $inpField = "inpTelpX";
  $defClass = "inp12 opt";
  echo "<input class='" . $defClass . "' style='margin-left:9px;' type='text' name='" . $inpField . "' maxlength='$maxExt' />";

  echo "</td></tr>";

  echo "<tr class='content'><td>Cell:</td>";
  $inpField = "inpTelc";
  $defClass = "opt";
  echo "<td class='r'><input class='" . $defClass . "' type='text' name='" . $inpField . "' maxlength='$maxTel' />";
  echo "</td></tr>";

  echo "<tr class='content'><td>Fax:</td>";
  $inpField = "inpTelf";
  $defClass = "opt";
  echo "<td class='r'><input class='" . $defClass . "' type='text' name='" . $inpField . "' maxlength='$maxTel' />";
  echo "</td></tr>";

  $sqlSelect = "SELECT * FROM insco ORDER BY insco_name";
  $resSelect = $mysqli->query($sqlSelect);

  $inpField = "selInsco";
  $defClass = "opt";
  echo "<tr class='content'><td class='l'><label for='$inpField'>Carrier:</label></td>";
  echo "<td class='r'><select class='$defClass' name='$inpField'>";
  echo "<option value='' selected='selected'>--- Select Carrier ---</option>";
  while ($mySelect = $resSelect->fetch_assoc()) {
    echo "<option value='" . $mySelect['insco_id'] . "'>" . prepareWeb($mySelect['insco_name']) . "</option>";
  }
  echo "</select>";
  echo "</td></tr>";

  $inpField = "inpInfo";
  $defClass = "opt";
  echo "<tr class='content'><td class='l'>Additional<br />Information:</td>";
  echo "<td class='r'><textarea class='$defClass' name='$inpField' rows='6' cols='48'></textarea>";
  echo "</td></tr>";

  $arrButtonRow   = [];
  $arrButtonRow[] = "<input class='inp08' type='submit' value='Save' />";
  $arrButtonRow[] = "<input class='inp08 modal-close' type='button' value='Cancel' />";
  echo "<tr class='content'><td></td><td>" . implode("", $arrButtonRow) . "</td></tr>";

  echo "</table>";

  echo "</form>\n";

  echo "</div>\n";
?>