<?php

// *************************** MODAL ***************************

  echo "<div class='modal' id='myModalRtl'>";
  echo "<div class='fixed'>";
  echo "<div class='body_stripe'></div>";

  echo "<div class='head'>";
  echo "<div class='content'>";

  echo "<span id='spanRtlHead'></span>";
  echo "<span style='float:right;'><a href='javascript:fnCloseModalRtl();'><img class='icon16' src='icons/no16.png' alt='Close' title='Close' /></a></span>";

  echo "</div>"; // content
  echo "</div>"; // head

  echo "<span id='spanRtlMessages'>";
  fnModalMessages();
  echo "</span>";

  echo "<div class='body'>";
  echo "\n<form method='post' name='frmModal' action='modalPRG.php'>";
  echo "<div class='content'>";

  echo "<input type='hidden' name='hidRtlID' id='hidRtlID' />";

  echo "<table class='info' id='tblRtl' style='display:none;'>";

  $inpField = "reoSkuNum";
  $inpLabel = "SKU";
  $defValue = "$frmSkuNum";
  $defClass = "inp16 reo";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' value='" . $defValue . "' readonly />";
  echo "</td></tr>";

  $inpField = "inpAmtCost";
  $inpLabel = "Retail";
  $defValue = "";
  $defClass = "inp08 req";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td>";
  echo "$&nbsp;<input class='$defClass' type='text' name='$inpField' id='$inpField' value='$defValue' maxlength='" . $maxAmt . "' />";
  echo "</td></tr>";

  $inpField = "inpDEff";
  $inpLabel = "Effective";
  $defValue = "";
  $defClass = "inp16 req";
  $inpHelp  = "Enter the date this retail price will take effect.";
  echo "<tr class='content'><td><label for='$inpField'>$inpLabel:</label></td><td class='r'>";
  echo "<input class='$defClass' type='text' name='$inpField' id='$inpField' value='" . $defValue . "' maxlength='$maxName' list='off' />";
  echo "&nbsp;<img class='icon16' title='$inpHelp' alt='Help' src='icons/question16.png' />";
  echo "</td></tr>";

  echo "<script>$(function() { $('#".  $inpField . "').datepicker(); }); </script>";

  $arrButtonRowRtl[] = "<input class='inp12' type='submit' name='subAddRtl' id='subAddRtl' value='Add' />";
  $arrButtonRowRtl[] = "<input class='inp12' type='submit' name='subChgRtl' id='subChgRtl' value='Save' />";
  $arrButtonRowRtl[] = "<input class='inp12' type='button' name='butConRtl' id='butConRtl' value='Delete' onclick='fnConfirmDelRtl();' />";
  $arrButtonRowRtl[] = "<input class='inp12' type='button' name='butCanRtl' id='butCanRtl' value='Cancel' onclick='fnCloseModalRtl();' />";

  echo "<tr class='content'><td></td><td>" . implode("", $arrButtonRowRtl) . "</td></tr>";

  echo "</table>";

  echo "</div>"; // content
  echo "<span id='spanRtlConfirm'></span>";
  echo "</form>";
  echo "</div>"; // body

  echo "<div class='body_stripe'></div>";
  echo "</div>"; // fixed
  echo "</div>"; // modal

// *************************** LOAD ***************************

  if (isset($_SESSION['inpValue']['subAddRtl']) || isset($_SESSION['inpValue']['subChgRtl'])) {

    echo "\n<script language=\"JavaScript\">";

    echo "\nvar objRtl = {};"; // Create a new object to hold the posted values.

    if (isset($_SESSION['inpValue']['hidRtlID'])) {
      echo "\narrRtlID.push(" . $_SESSION['inpValue']['hidRtlID'] . ");";
      echo "\nobjRtl.id     = \"" . $_SESSION['inpValue']['hidRtlID'] . "\";";
    }

    echo "\nobjRtl.skuNum  = \"" . prepareJS($_SESSION['inpValue']['reoSkuNum'])  . "\";";
    echo "\nobjRtl.penCost = \"" . prepareJS($_SESSION['inpValue']['inpAmtCost']) . "\";";
    echo "\nobjRtl.dtEff   = \"" . prepareJS($_SESSION['inpValue']['inpDEff'])    . "\";";

    foreach($_SESSION['errField'] AS $field) {

      echo "\narrRtlEr.push(\"" . $field    . "\");";
      echo "\ndocument.getElementById(\"" . $field . "\").classList.add(\"err\");";

    }

    echo "\narrRtlOb.push(objRtl);"; // Tack this onto the end of the array of SKU objects.

    if (isset($_SESSION['inpValue']['subAddRtl'])) echo "\nfnAddRtl();";
    if (isset($_SESSION['inpValue']['subChgRtl'])) echo "\nfnChgRtl(" . $_SESSION['inpValue']['hidRtlID'] . ");";
    echo "\nfnFillRtl(arrRtlOb.length-1);";

    echo "\n</script>\n";
  }

?>