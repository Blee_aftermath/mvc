<?php echo $header; ?>
            
            <div class='body_fixed bgcolor1'>
               <div class='body_stripe'></div>
               <div class='flexbox'>
                  <div class='body_content_float' style='width:180px;'>
                     <table class='info'>
                        <tr class='content'>
                           <td class='l' colspan='2' style='font-weight:bold;'>JOB INFORMATION FILE</td>
                        </tr>
                        <tr class='content'>
                           <td class='l'>Job:  </td>
                           <td class='r' style='font-weight:bold;'><a href='job.php?job=84072'>2110023</a></td>
                        </tr>
                        <tr class='content'>
                           <td class='l'>Class:</td>
                           <td class='r' style='font-weight:bold;'>Aftermath</td>
                        </tr>
                     </table>
                  </div>
                  <div class='body_content_float' style='width:240px;'>
                     <table class='info'>
                        <tr class='content'>
                           <td class='l'>CM: </td>
                           <td class='r' style='font-weight:bold;'>K.Brown</td>
                           <td style='white-space:nowrap'>(630) 701-5407</td>
                        </tr>
                        <tr class='content'>
                           <td class='l'>PM: </td>
                           <td class='r' style='font-weight:bold;'>K.Brown</td>
                           <td style='white-space:nowrap'>(630) 701-5407</td>
                        </tr>
                        <tr class='content'>
                           <td class='l'>Sup:</td>
                           <td class='r' style='font-weight:bold;'>M.Sexton</td>
                           <td style='white-space:nowrap'>(916) 225-0766</td>
                        </tr>
                     </table>
                  </div>
                  <div class='body_content_float' style='width:200px;'>
                     <table class='info'>
                        <tr class='content'>
                           <td class='l'>Location:</td>
                           <td class='r' style='font-weight:bold;'>Chico, CA</td>
                        </tr>
                        <tr class='content'>
                           <td class='l'>Office:  </td>
                           <td class='r' style='font-weight:bold;'><a href='office.php?office=24'>Sacramento, CA</a></td>
                        </tr>
                        <tr class='content'>
                           <td class='l'>         </td>
                           <td class='r' style='font-weight:bold;'></td>
                        </tr>
                     </table>
                  </div>
                  <div class='body_content_float' style='width:180px;'>
                     <table class='info'>
                        <tr class='content'>
                           <td class='l'>Job Type: </td>
                           <td class='r' style='font-weight:bold;'>Human Fluids</td>
                        </tr>
                        <tr class='content'>
                           <td class='l'>Prop Type:</td>
                           <td class='r' style='font-weight:bold;'>Outdoors</td>
                        </tr>
                        <tr class='content'>
                           <td class='l'>Category: </td>
                           <td class='r' style='font-weight:bold;'>Non</td>
                        </tr>
                     </table>
                  </div>
                  <div class='body_content_float' style='width:200px;'>
                     <table class='info'>
                        <tr class='content'>
                           <td class='l'>Decedent:  </td>
                           <td class='r' style='font-weight:bold;'>N/E</td>
                        </tr>
                        <tr class='content'>
                           <td class='l'>Cause:     </td>
                           <td class='r' style='font-weight:bold;'>N/E</td>
                        </tr>
                        <tr class='content'>
                           <td class='l'>Unattended:</td>
                           <td class='r' style='font-weight:bold;'>N/E</td>
                        </tr>
                     </table>
                  </div>
               </div>
               <div class='body_stripe'></div>
               <div class='body_content teal' phaselist-insert=1></div>
            </div>
            <div class='body_fixed bgcolor2'>
               <div class='body_stripe'></div>
               <div class='flexbox' jif-bottom='1'>
                  <div name='tab_body' class='body_jif_l' insert-comm='1'>
                     <div class='tabs_fixed'>
                        <div id='tabs'>
                           <ul>
                              <li><a href='/jif.php?job=84072&phase=fin&tab=finalEstimate'>Final Est</a></li>
                              <li><a href='/jif.php?job=84072&phase=fin&tab=decline'>Declined</a></li>
                              <li id = 'current'><a href='/jif.php?job=84072&phase=fin&tab=postJob'>Post Job</a></li>
                              <li><a href='/jif.php?job=84072&phase=fin&tab=contacts'>Contacts</a></li>
                              <li><a href='/jif.php?job=84072&phase=fin&tab=comm'>Comm</a></li>
                              <li><a href='/jif.php?job=84072&phase=fin&tab=doc'>Docs</a></li>
                              <li><a href='/jif.php?job=84072&phase=fin&tab=photo'>Photos</a></li>
                              <li><a href='/jif.php?job=84072&phase=fin&tab=restoration'>Restoration</a></li>
                              <li><a href='/jif.php?job=84072&phase=fin&tab=verify'>Verify</a></li>
                           </ul>
                        </div>
                     </div>
                     <br />
                     <div id='jifLock'></div>
                     
                     <div name="postJob" style="background-color: white;padding-bottom: 15px">
                        <div class="block-header">Post Job Recommendation</div>
                        <p style="padding:10px">Select all applicable recommendations for additional work needed after completing our scope. <br/><span style="font-style: italic">Write one in Other if not on the list.</span></br> Customer must sign that they understand post job recommendation(s).</p>
                        <div>
                           <table class="data data-alt1" width="100%">
                              <thead>
                                 <tr>
                                    <th style="width:25%">Recommendation</th>
                                    <th style="width:3%"></th>
                                    <th style="width:66%;">Notes</th>
                                    <th style="width:6%;"></th>
                                 </tr>
                              </thead>
                              <tbody name="postJobBody">
                              </tbody>
                           </table>
                        </div>
                     </div>

                     <script type='module'>
                           import {PostJob} from '/view/asset/js/jif/postJob.js?$scriptUUID';
                           new PostJob(<?php echo $postJobs ?>)
                     </script>


                  </div>
                  <div class='body_vstripe'></div>
               </div>
            </div>
<?php echo $footer; ?>