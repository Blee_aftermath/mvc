<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.88.1">
    <title>Dashboard Template · Bootstrap v5.1</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/5.1/examples/dashboard/">

    <!-- 
    Bootstrap core CSS 
    TODO. move later
    -->
    <link href="http://127.0.0.1:85/assets/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel='stylesheet' type='text/css' href='/view/asset/js/jquery/jquery-ui-1.11.4/jquery-ui.css' />

    <!-- awesome font than SVG vector graphic -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" rel="stylesheet">
    
    <!-- Custom styles for this template -->
    <link href="/view/asset/css/style.css" rel="stylesheet">

    <script>window.aftermath = {};</script>

    <script src="/view/asset/js/jquery/jquery-ui-1.11.4/external/jquery/jquery.js"></script>
    <script src="/view/asset/js/jquery/jquery-ui-1.11.4/jquery-ui.js"></script>

  </head>

  <body data-version="<?php echo T ?>">

    <header class="p-3 bg-dark text-white sticky-top">

    <div class="container">
      <div class="d-flex flex-wrap align-items-center justify-content-center justify-content-lg-start">
        <a href="/" class="d-flex align-items-center mb-2 mb-lg-0 text-white text-decoration-none">
          <svg class="bi me-2" width="40" height="32" role="img" aria-label="Bootstrap"><use xlink:href="#bootstrap"></use></svg>
        </a>

        <ul class="nav col-12 col-lg-auto me-lg-auto mb-2 justify-content-center mb-md-0">
          <?php          
          foreach($arrMenu AS $key => $arrValue) {
            // TODO. remembered link and perm check later
            /*
            if (isset($_SESSION['perm']) && in_array($arrValue[0], $_SESSION['perm'])) { 
              if ($topMenu == $arrValue[1]) {
                //$arrMenuString[] = "<li><a href='" . $arrValue[3] . "' class='nav-link px-2 text-white' title='" . $arrValue[4] . "'>" . $arrValue[2] . "</span></a></li>";
              } else {
                $arrMenuString[] = "<li><a href='" . $arrValue[3] . "' class='nav-link px-2 text-secondary' title='" . $arrValue[4] . "'>" . $arrValue[2] . "</a></li>";
              }
            }
            */
            echo "<li><a href='" . $arrValue[3] . "' class='nav-link px-2 text-white' title='" . $arrValue[4] . "'>" . $arrValue[2] . "</a></li>";
          }
          ?>
        </ul>

        <form class="col-12 col-lg-auto mb-3 mb-lg-0 me-lg-3">
          <input type="search" class="form-control form-control-dark" placeholder="Search..." aria-label="Search">
        </form>

        <div class="text-end">
          <?php if (isset($_SESSION['emp'])) { ?>
            <button type="button" class="btn btn-warning">Profile</button>
            <button type="button" class="btn btn-outline-light me-2" onclick="location.href='/auth/logout'">Logout</button>
          <?php } else { ?>
            <button type="button" class="btn btn-outline-light me-2" onclick="location.href='/auth/login'">Login</button>
            <!--button type="button" class="btn btn-warning">Sign-up</button-->
          <?php } ?>

        </div>
      </div>
    </div>
  </header>
