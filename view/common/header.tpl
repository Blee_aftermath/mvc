<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
   <head>
      <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
      <meta http-equiv="content-language" content="en" />
      <meta name="author" content="Fuzzy Logic" />
      <meta name="description" content="Aftermath Employee Portal" />
      <meta name="keywords" content="aftermath, biometric, crime scene, cleanup" />
      <meta name="robots" content="index, follow">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
      
      <!-- Added 180424 to make tip css show correctly on Android -->
      <link rel='stylesheet' type='text/css' href='/view/asset/css/styles.css?v=<?php echo time() ?>' />
      <?php if (isset($incJQueryCSS))   echo "<link rel='stylesheet' type='text/css' href='$incJQueryCSS' />"; ?>
      <?php if (isset($incJifStyles))   echo "<link rel='stylesheet' type='text/css' href='$incJifStyles'"; ?>
      <?php if (isset($incStateStyles)) echo "<link rel='stylesheet' type='text/css' href='$incStateStyles'"; ?>
      <?php if (isset($docStyles))      echo "<link rel='stylesheet' type='text/css' href='$docStyles'"; ?>
      <link rel='stylesheet' type='text/css' href='/view/asset/css/localStyles.css?v=<?php echo time() ?>' />


      <link rel="apple-touch-icon-precomposed" sizes="57x57" href="/view/asset/icons/favicon/apple-touch-icon-57x57.png" />
      <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/view/asset/icons/favicon/apple-touch-icon-114x114.png" />
      <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/view/asset/icons/favicon/apple-touch-icon-72x72.png" />
      <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/view/asset/icons/favicon/apple-touch-icon-144x144.png" />
      <link rel="apple-touch-icon-precomposed" sizes="60x60" href="/view/asset/icons/favicon/apple-touch-icon-60x60.png" />
      <link rel="apple-touch-icon-precomposed" sizes="120x120" href="/view/asset/icons/favicon/apple-touch-icon-120x120.png" />
      <link rel="apple-touch-icon-precomposed" sizes="76x76" href="/view/asset/icons/favicon/apple-touch-icon-76x76.png" />
      <link rel="apple-touch-icon-precomposed" sizes="152x152" href="/view/asset/icons/favicon/apple-touch-icon-152x152.png" />
      <link rel="icon" type="image/png" href="/view/asset/icons/favicon/favicon-196x196.png" sizes="196x196" />
      <link rel="icon" type="image/png" href="/view/asset/icons/favicon/favicon-96x96.png" sizes="96x96" />
      <link rel="icon" type="image/png" href="/view/asset/icons/favicon/favicon-32x32.png" sizes="32x32" />
      <link rel="icon" type="image/png" href="/view/asset/icons/favicon/favicon-16x16.png" sizes="16x16" />
      <link rel="icon" type="image/png" href="/view/asset/icons/favicon/favicon-128.png" sizes="128x128" />
      <meta name="application-name" content="&nbsp;"/>
      <meta name="msapplication-TileColor" content="#FFFFFF" />
      <meta name="msapplication-TileImage" content="mstile-144x144.png" />
      <meta name="msapplication-square70x70logo" content="mstile-70x70.png" />
      <meta name="msapplication-square150x150logo" content="mstile-150x150.png" />
      <meta name="msapplication-wide310x150logo" content="mstile-310x150.png" />
      <meta name="msapplication-square310x310logo" content="mstile-310x310.png" />
      <script>
         window.aftermath = {};
      </script>
      <script src='scripts/modules/app.js?v" . time() . "' type='module'></script>
      <title><?php echo $title ?></title>
      <!-- biometric, aftermath, suicide, biological remediation -->
   </head>
   <body data-version='1630598492'>
      <!-- <script type="text/javascript">
         var
             snow_size_min = 5,
             snow_size_max = 10,
             snow_num_flakes = 20,
             snow_vert_start = 0,
             snow_vert_end = 80,
             snow_flake_color = '#fff',
             snow_fall_speed = 20;
         </script>
         <script type="text/javascript" src="/view/asset/js/jquery/snow.js"></script> -->
      <script src="/view/asset/js/jquery/jquery-ui-1.11.4/external/jquery/jquery.js"></script>
      <script src="/view/asset/js/jquery/jquery-ui-1.11.4/jquery-ui.js"></script>
      <script>
         window.aftermath.hasPerm = perm => {
           const perms = ["ADMIN","ASSIGN","AUD_A","COMMON","FIN_A","FIN_E","FIN_P","HR_E","IV_A","IV_E","JIF_A","JIF_D","JIF_E","JIF_M","JIF_R","JOB_A","JOB_CF","JOB_CO","JOB_E","JOB_PG","JOB_PW","JOB_ST","JOB_V1","JOB_V2","MASTER","MKT_A","MKT_E","RDB","RES_A","RPT","SCH_E","SCH_R","TEAM","TIM_A","TIM_E","DEV","EXEC","FDB","RGN"];
           return perms.includes(perm.toUpperCase()) ? true : false;
         }
      </script>

      
      
      <!-- 
      Jif header 
      TODO. only JIF use module javascript ? Need to partitioning neatly
      -->
      <script type='module'>
         import {JobVerify} from '/view/asset/js/jobVerify.js?1630598492';
         new JobVerify({job: 84072, class: 1, state: 'CA'});
      </script>
      <script type='module'>
         import {JifController} from '/view/asset/js/jif/jifController.js?1630598492';
         import {TemplateController} from '/view/asset/js/templateController.js?1630598492';
         window.templateController = new TemplateController();
         window.jifController = new JifController();
      </script>
      <script type='module'>
         import {default as formModal} from '/view/asset/js/modules/ui/formModal.js?1630598492';
         document.querySelector('.body_jif_l').addEventListener('click', (ev) => {
           if (ev.target.matches('[data-form]')) {
             formModal(ev.target);
           }
         });
      </script>
      <script type='module'>import {JifSidebar} from '/view/asset/js/jif/jifSidebar.js?1630598492';let jifSidebar = new JifSidebar({"template":"<h1 name='checklist'><\/h1>\r\n<!--Do we need to convert the rest of sidebar to use this? Hide for now: <h1 name='documents'><\/h1>-->","checklistTemplate":"<div class=\"jif-checklist-item\" style=\"cursor: pointer\">\r\n  <img src='/view/asset/icons\/dn16.png' title='expand' class='jif-expander' style='transform: rotate(-90deg);'>\r\n  <span name='title'><\/span>\r\n  <img name='statusIcon' src='/view/asset/icons\/no16.png' title='Incomplete' class='jif-check-red'>\r\n<\/div>\r\n<form class='jif-dropdown'>\r\n  <textarea rows='5' maxlength='90'><\/textarea>\r\n  <input name='overrideButton' type='submit' style='display:inline-block;margin:auto'>\r\n<\/form>","hoursCheckTemplate":"<h1 name='expanderHeader' style='user-select: none;cursor: pointer;position: relative'>\r\n  <img name='expander' src=\"icons\/dn16.png\" title=\"Expand\" class=\"jif-expander-white\" style=\"transform: rotate(-90deg);float: left\">\r\n  HOURS CHECK\r\n<\/h1>\r\n<div name='content-collapse' class='content-collapse' style='width:254px;'>\r\n  <table name='checkTable' class='data data-alt1'>\r\n    <thead>\r\n      <th style='width:25%'>\r\n        <select class='inp08' name='scopeSelector' value = '1' style='position: inherit;'>\r\n        <\/select>\r\n      <\/th>\r\n      <th class='r' style='position: inherit'>Est<\/th>\r\n      <th class='r'>Act<\/th>\r\n      <th class='r'>Diff<\/th>\r\n    <\/thead>\r\n    <tbody>\r\n      <tr>\r\n        <td>Hazard<\/td>\r\n        <td class='r' name='jcur_hr_haz'>1<\/td>\r\n        <td class='r' name='jfin_hr_haz'>1<\/td>\r\n        <td class='r' name='diff_hr_haz'>1<\/td>\r\n      <\/tr>\r\n      <tr>\r\n        <td>Setup<\/td>\r\n        <td class='r' name='jcur_hr_set'>1<\/td>\r\n        <td class='r' name='jfin_hr_set'>1<\/td>\r\n        <td class='r' name='diff_hr_set'>1<\/td>\r\n      <\/tr>\r\n      <tr>\r\n        <td>Bio Rem<\/td>\r\n        <td class='r' name='jcur_hr_bio'>1<\/td>\r\n        <td class='r' name='jfin_hr_bio'>1<\/td>\r\n        <td class='r' name='diff_hr_bio'>1<\/td>\r\n      <\/tr>\r\n      <tr>\r\n        <td>Content<\/td>\r\n        <td class='r' name='jcur_hr_cnt'>1<\/td>\r\n        <td class='r' name='jfin_hr_cnt'>1<\/td>\r\n        <td class='r' name='diff_hr_cnt'>1<\/td>\r\n      <\/tr>\r\n      <tr>\r\n        <td>Surf Prep<\/td>\r\n        <td class='r' name='jcur_hr_srf'>1<\/td>\r\n        <td class='r' name='jfin_hr_srf'>1<\/td>\r\n        <td class='r' name='diff_hr_srf'>1<\/td>\r\n      <\/tr>\r\n      <tr>\r\n        <td>Biowash<\/td>\r\n        <td class='r' name='jcur_hr_cln'>1<\/td>\r\n        <td class='r' name='jfin_hr_cln'>1<\/td>\r\n        <td class='r' name='diff_hr_cln'>1<\/td>\r\n      <\/tr>\r\n      <tr>\r\n        <td>Extract<\/td>\r\n        <td class='r' name='jcur_hr_poe'>1<\/td>\r\n        <td class='r' name='jfin_hr_poe'>1<\/td>\r\n        <td class='r' name='diff_hr_poe'>1<\/td>\r\n      <\/tr>\r\n      <tr>\r\n        <td>Fin Insp<\/td>\r\n        <td class='r' name='jcur_hr_fin'>1<\/td>\r\n        <td class='r' name='jfin_hr_fin'>1<\/td>\r\n        <td class='r' name='diff_hr_fin'>1<\/td>\r\n      <\/tr>\r\n      <tr>\r\n        <td>Pers Prop<\/td>\r\n        <td class='r' name='jcur_hr_ppr'>1<\/td>\r\n        <td class='r' name='jfin_hr_ppr'>1<\/td>\r\n        <td class='r' name='diff_hr_ppr'>1<\/td>\r\n      <\/tr>\r\n      <tr>\r\n        <th>Total<\/th>\r\n        <th class='r' name='estimateTotal'><\/th>\r\n        <th class='r' name='actualTotal'><\/th>\r\n        <th class='r' name='diffTotal'><\/th>\r\n      <\/tr>\r\n    <\/tbody>\r\n  <\/table>\r\n<\/div>","costCheckTemplate":"<h1 name='expanderHeader' style='user-select: none;cursor: pointer;position: relative'>\r\n  <img name='expander' src=\"icons\/dn16.png\" title=\"Expand\" class=\"jif-expander-white\" style=\"transform: rotate(-90deg);float: left\">\r\n  COST CHECK\r\n<\/h1>\r\n<div name='content-collapse' class='content-collapse' style='width:254px;'>\r\n  <table class='data data-alt1'>\r\n    <thead>\r\n      <th style='width: 14%;'>\r\n        <select class='inp08' name='scopeSelector' value = '1' style='position: inherit;'>\r\n        <\/select>\r\n      <\/th>\r\n      <th class='r' style='position: inherit'>Est<\/th>\r\n      <th class='r'>Act<\/th>\r\n      <th class='r'>O[U]<\/th>\r\n    <\/thead>\r\n    <tbody>\r\n      <tr>\r\n        <td>Labor<\/td>\r\n        <td class='r' name='jcur_calc_labor'>1<\/td>\r\n        <td class='r' name='jfin_calc_labor'>1<\/td>\r\n        <td class='r' name='diff_calc_labor'>1<\/td>\r\n      <\/tr>\r\n      <tr>\r\n        <td>Waste<\/td>\r\n        <td class='r' name='jcur_calc_sub_waste'>1<\/td>\r\n        <td class='r' name='jfin_calc_sub_waste'>1<\/td>\r\n        <td class='r' name='diff_calc_sub_waste'>1<\/td>\r\n      <\/tr>\r\n      <tr>\r\n        <td>Equip<\/td>\r\n        <td class='r' name='jcur_calc_eqp'>1<\/td>\r\n        <td class='r' name='jfin_calc_eqp'>1<\/td>\r\n        <td class='r' name='diff_calc_eqp'>1<\/td>\r\n      <\/tr>\r\n      <tr>\r\n        <td>Supply<\/td>\r\n        <td class='r' name='jcur_calc_sup'>1<\/td>\r\n        <td class='r' name='jfin_calc_sup'>1<\/td>\r\n        <td class='r' name='diff_calc_sup'>1<\/td>\r\n      <\/tr>\r\n      <tr>\r\n        <td>Other<\/td>\r\n        <td class='r' name='jcur_calc_other'>1<\/td>\r\n        <td class='r' name='jfin_calc_other'>1<\/td>\r\n        <td class='r' name='diff_calc_other'>1<\/td>\r\n      <\/tr>\r\n      <tr>\r\n        <th>Rack<\/th>\r\n        <th class='r' name='jcur_calc_sub_full'>1<\/th>\r\n        <th class='r' name='jfin_calc_sub_full'>1<\/th>\r\n        <th class='r' name='diff_calc_sub_full'>1<\/th>\r\n      <\/tr>\r\n      <tr>\r\n        <td>Disc<\/td>\r\n        <td class='r' name='jcur_calc_disc'>1<\/td>\r\n        <td class='r' name='jfin_calc_disc'>1<\/td>\r\n        <td class='r' name='diff_calc_disc'>1<\/td>\r\n      <\/tr>\r\n      <tr>\r\n        <th>Sub<\/th>\r\n        <th class='r' name='jcur_calc_sub'>1<\/th>\r\n        <th class='r' name='jfin_calc_sub'>1<\/th>\r\n        <th class='r' name='diff_calc_sub'>1<\/th>\r\n      <\/tr>\r\n      <tr>\r\n        <td>Tax<\/td>\r\n        <td class='r' name='jcur_calc_tax'>1<\/td>\r\n        <td class='r' name='jfin_calc_tax'>1<\/td>\r\n        <td class='r' name='diff_calc_tax'>1<\/td>\r\n      <\/tr>\r\n      <tr>\r\n        <td>3P\/Oth<\/td>\r\n        <td class='r' name='jcur_calc_3rd_other'>1<\/td>\r\n        <td class='r' name='jfin_calc_3rd_other'>1<\/td>\r\n        <td class='r' name='diff_calc_3rd_other'>1<\/td>\r\n      <\/tr>\r\n      <tr>\r\n        <th>Total<\/th>\r\n        <th class='r' name='jcur_calc_tot'><\/th>\r\n        <th class='r' name='jfin_calc_tot'><\/th>\r\n        <th class='r' name='diff_calc_tot'><\/th>\r\n      <\/tr>\r\n    <\/tbody>\r\n  <\/table>\r\n\r\n  <!--Differences-->\r\n  <table class='data data-alt1'>\r\n    <thead>\r\n      <th style='padding-left: 10%;'>Cost Center<\/th>\r\n      <th style='padding-right: 10%;' class='r' style>Over[Under]<\/th>\r\n    <\/thead>\r\n    <tbody>\r\n      <tr>\r\n        <td style='padding-left: 10%;'>Labor<\/td>\r\n        <td style='padding-right: 10%;' class='r' name='laborDiff'><\/td>\r\n      <\/tr>\r\n      <tr>\r\n        <td style='padding-left: 10%;'>Waste<\/td>\r\n        <td style='padding-right: 10%;' class='r' name='wasteDiff'><\/td>\r\n      <\/tr>\r\n      <tr>\r\n        <td style='padding-left: 10%;'>Equipment<\/td>\r\n        <td style='padding-right: 10%;' class='r' name='equipDiff'><\/td>\r\n      <\/tr>\r\n      <tr>\r\n        <td style='padding-left: 10%;'>Supply<\/td>\r\n        <td style='padding-right: 10%;' class='r' name='supplyDiff'><\/td>\r\n      <\/tr>\r\n      <tr class='bb'>\r\n        <td style='padding-left: 10%;'>Other<\/td>\r\n        <td style='padding-right: 10%;' class='r' name='otherDiff'><\/td>\r\n      <\/tr>\r\n      <tr>\r\n        <td style='padding:12px;padding-left:10%;font-size:16px'>Subtotal<\/td>\r\n        <td style='padding:12px;padding-right:10%;font-size:16px' class='r' name='subtotalDiff'><\/td>\r\n      <\/tr>\r\n    <\/tbody>\r\n  <\/table>\r\n<\/div>","sidebarAdminTemplate":"<h1>Admin<\/h1>\r\n<div name='adminPanel' style='text-align: center;padding: 4px;'>\r\n  <input name='lockButton' type='button' value='Lock'>\r\n  <div name='unlockedText'>WARNING: This job is unlocked, all fields are editable<\/div>\r\n<\/div>","items":[{"checkID":"1037286","name":"Confirm final estimate","reason":null,"skippable":false,"isPass":"1"}],"primaryJini":{"jini_id":"10564","jini_jif":"1009549","jini_job":"84072","jini_scope":"1","jini_seq":"1","jini_is_iicrc":"0","jini_pricelist_used":"3","jini_num_zone":"1","jini_num_days":"1","jini_num_crew":"1","jini_num_truck":"1","jini_num_rental":"0","jini_num_hotel":"0","jini_rate_iicrc":"22500","jini_rate_sup":"22500","jini_rate_tec":"22500","jini_wknd_iicrc":"22500","jini_wknd_sup":"22500","jini_wknd_tec":"22500","jini_price_wm_fee":"0","jini_price_transport":"0","jini_price_disposal":"0","jini_price_dispatch":"0","jini_price_supply_fee":"0","jini_price_atp_test":"29500","jini_price_asb_test":"69700","jini_price_asb_vial":"7036","jini_price_srf_swab":"0","jini_price_srf_test":"0","jini_price_demob":"0","jini_price_disinfect":"0","jini_price_phodoc":"0","jini_price_digrep":"0","jini_price_dumpster":"100000","jini_price_bagster":"40000","jini_price_mattress":"0","jini_price_sqft":"0","jini_price_ent":"0","jini_per_dis":"0.0","jini_per_eqp":"30.0","jini_per_sup":"50.0","jini_per_ovh":"0.0","jini_yn_include":"1","jini_yn_atp_testing":"0","jini_yn_pp_w_structure":"0","jini_yn_state_tax":"0","jini_yn_guarantee":"0","jini_hr_haz":"0.00","jini_hr_set":"0.00","jini_hr_bio":"3.00","jini_hr_cnt":"0.00","jini_hr_srf":"0.00","jini_hr_cln":"0.00","jini_hr_poe":"0.00","jini_hr_fin":"0.00","jini_hr_ppr":"0.00","jini_hr_cov":"0.00","jini_hr_wknd":"0.00","jini_sum_hr":"3.00","jini_num_box_suits":"1","jini_num_box_debris":"0","jini_num_dumpster":"0","jini_num_bagster":"0","jini_num_mattress":"0","jini_num_asb_vial":"0","jini_num_srf_swab":"0","jini_num_sqft":"0","jini_num_ent":"0","jini_amt_est":"0","jini_amt_other":"40000","jini_amt_3rd_party":"0","jini_amt_int_exp":"0","jini_disc":null,"jini_disc_per":"0","jini_disc_amt_other":"0","jini_disc_emp_appr":null,"jini_mgn_emp_appr":null,"jini_tax_type":"1","jini_tax_rate":"9.00","jini_calc_haz":"0","jini_calc_set":"0","jini_calc_bio":"67500","jini_calc_cnt":"0","jini_calc_srf":"0","jini_calc_cln":"0","jini_calc_poe":"0","jini_calc_fin":"0","jini_calc_ppr":"0","jini_calc_cov":"0","jini_calc_labor":"67500","jini_calc_waste":"0","jini_calc_dis":"0","jini_calc_ovh":"0","jini_calc_dispatch":"0","jini_calc_atp_test":"0","jini_calc_asb_test":"0","jini_calc_asb_vial":"0","jini_calc_demob":"0","jini_calc_disinfect":"0","jini_calc_phodoc":"0","jini_calc_digrep":"0","jini_calc_dumpster":"0","jini_calc_eqp":"20250","jini_calc_sup":"33750","jini_calc_other":"54000","jini_calc_sub_full":"121500","jini_calc_disc":"0","jini_calc_sub":"121500","jini_calc_tax":"0","jini_calc_tot":"161500","jcur_hr_haz":"0.00","jcur_hr_set":"0.00","jcur_hr_bio":"3.00","jcur_hr_cnt":"0.00","jcur_hr_srf":"0.00","jcur_hr_cln":"0.00","jcur_hr_poe":"0.00","jcur_hr_fin":"0.00","jcur_hr_ppr":"0.00","jcur_hr_cov":"0.00","jcur_hr_wknd":"0.00","jcur_sum_hr":"3.00","jcur_num_box_suits":"1","jcur_num_box_debris":"0","jcur_num_dumpster":"0","jcur_num_bagster":"0","jcur_num_mattress":"0","jcur_num_asb_vial":"0","jcur_amt_other":"40000","jcur_amt_3rd_party":"0","jcur_amt_int_exp":"0","jcur_calc_haz":"0","jcur_calc_set":"0","jcur_calc_bio":"67500","jcur_calc_cnt":"0","jcur_calc_srf":"0","jcur_calc_cln":"0","jcur_calc_poe":"0","jcur_calc_fin":"0","jcur_calc_ppr":"0","jcur_calc_cov":"0","jcur_calc_labor":"67500","jcur_calc_waste":"0","jcur_calc_dis":"0","jcur_calc_ovh":"0","jcur_calc_dispatch":"0","jcur_calc_atp_test":"0","jcur_calc_asb_test":"0","jcur_calc_asb_vial":"0","jcur_calc_demob":"0","jcur_calc_disinfect":"0","jcur_calc_phodoc":"0","jcur_calc_digrep":"0","jcur_calc_dumpster":"0","jcur_calc_eqp":"20250","jcur_calc_sup":"33750","jcur_calc_other":"0","jcur_calc_sub_full":"121500","jcur_calc_disc":"0","jcur_calc_sub":"121500","jcur_calc_tax":"0","jcur_calc_tot":"161500","jini_dt_add":"2021-08-30 13:44:13","jini_dt_last":"2021-08-30 13:44:13","jini_yn_wknd_ovrd":"0","x_jini_rate_tax_sup":"0.00","x_jini_rate_tax_lab":"0.00","x_jcur_calc_other":"0","scope_id":"1","scope_code":"LS","scope_inv_type":"6","scope_short":"Limited","scope_name":"Limited Scope","scope_pricelist":"3","scope_billable_entity":"1","scope_def_yn_atp_testing":"0","scope_def_yn_guarantee":"0","scope_allow_sec_scope":"0","scope_blurb":null,"jif_id":"1009549","jif_job":"84072","jif_truck":"715","jif_scope_rec":"1","jif_scope_pri":"1","jif_scope_sec":null,"jif_num_days":"1","jif_num_crew":"1","jif_is_after_hours":"0","jif_pass_tsol":"1","jif_ini_is_confirmed":"1","jif_ini_emp_confirmed":"295","jif_ini_dt_confirmed":"2021-09-01 18:13:51","jif_fin_is_confirmed":"1","jif_fin_emp_confirmed":"295","jif_fin_dt_confirmed":"2021-09-02 07:58:09","jif_is_locked":"1","jif_emp_locked":"295","jif_per_real_est":"90.0","jif_dt_locked":"2021-09-02 07:58:09","jif_dt_dispatch":"2021-08-31 10:00:00","jif_dt_arrive":"2021-08-31 12:00:00","jif_calc_hr_transit":"2.00","jif_affected_pri":"outdoor job","jif_affected_sec":null,"jif_affected_pp":null,"jif_service_notes":null,"jif_yn_deconstruction":"0","jini_disc_emp_name":null},"primaryInvType":"Limited","primaryFinal":{"jfin_id":"4697","jfin_jif":"1009549","jfin_job":"84072","jfin_seq":"1","jfin_num_days":"1","jfin_num_crew":"1","jfin_num_truck":"1","jfin_num_rental":"0","jfin_num_hotel":"0","jfin_hr_sup_drv":"0.00","jfin_hr_sup_haz":"0.00","jfin_hr_sup_set":"0.00","jfin_hr_sup_bio":"2.00","jfin_hr_sup_cnt":"0.00","jfin_hr_sup_srf":"0.00","jfin_hr_sup_cln":"0.00","jfin_hr_sup_poe":"0.00","jfin_hr_sup_fin":"0.00","jfin_hr_sup_ppr":"0.00","jfin_hr_sup_cov":"0.00","jfin_hr_sup_off":"0.00","jfin_hr_sup_brk":"0.00","jfin_hr_tec_drv":"0.00","jfin_hr_tec_haz":"0.00","jfin_hr_tec_set":"0.00","jfin_hr_tec_bio":"0.00","jfin_hr_tec_cnt":"0.00","jfin_hr_tec_srf":"0.00","jfin_hr_tec_cln":"0.00","jfin_hr_tec_poe":"0.00","jfin_hr_tec_fin":"0.00","jfin_hr_tec_ppr":"0.00","jfin_hr_tec_cov":"0.00","jfin_hr_tec_off":"0.00","jfin_hr_tec_brk":"0.00","jfin_hr_nb_sup_drv":"5.50","jfin_hr_nb_sup_haz":"0.00","jfin_hr_nb_sup_set":"0.00","jfin_hr_nb_sup_bio":"0.00","jfin_hr_nb_sup_cnt":"0.00","jfin_hr_nb_sup_srf":"0.00","jfin_hr_nb_sup_cln":"0.00","jfin_hr_nb_sup_poe":"0.00","jfin_hr_nb_sup_fin":"0.00","jfin_hr_nb_sup_ppr":"0.00","jfin_hr_nb_sup_cov":"0.00","jfin_hr_nb_sup_off":"1.00","jfin_hr_nb_sup_brk":"0.50","jfin_hr_nb_tec_drv":"0.00","jfin_hr_nb_tec_haz":"0.00","jfin_hr_nb_tec_set":"0.00","jfin_hr_nb_tec_bio":"0.00","jfin_hr_nb_tec_cnt":"0.00","jfin_hr_nb_tec_srf":"0.00","jfin_hr_nb_tec_cln":"0.00","jfin_hr_nb_tec_poe":"0.00","jfin_hr_nb_tec_fin":"0.00","jfin_hr_nb_tec_ppr":"0.00","jfin_hr_nb_tec_cov":"0.00","jfin_hr_nb_tec_off":"0.00","jfin_hr_nb_tec_brk":"0.00","jfin_hr_bb_sup_wkdy":"2.00","jfin_hr_bb_sup_wknd":"0.00","jfin_hr_bb_sup":"2.00","jfin_hr_bb_tec_wkdy":"0.00","jfin_hr_bb_tec_wknd":"0.00","jfin_hr_bb_tec":"0.00","jfin_hr_bb":"2.00","jfin_sum_hr_sup":"2.00","jfin_sum_hr_tec":"0.00","jfin_sum_hr_nb_sup":"0.00","jfin_sum_hr_nb_tec":"0.00","jfin_amt_other":"40000","jfin_amt_3rd_party":"0","jfin_amt_int_exp":"0","jfin_amt_tax_final":null,"jfin_amt_addl_final":"0","jfin_num_box_suits":"0","jfin_num_box_debris":"0","jfin_num_dumpster":"0","jfin_num_bagster":"0","jfin_num_mattress":"0","jfin_num_asb_vial":"0","jfin_num_srf_swab":"0","jfin_disc":"0","jfin_disc_per":"0","jfin_disc_amt_other":"0","jfin_disc_amt_addl":"0","jfin_disc_addl_emp_appr":null,"jfin_calc_sup_haz":"0","jfin_calc_sup_set":"0","jfin_calc_sup_bio":"0","jfin_calc_sup_cnt":"0","jfin_calc_sup_srf":"0","jfin_calc_sup_cln":"0","jfin_calc_sup_poe":"0","jfin_calc_sup_fin":"0","jfin_calc_sup_ppr":"0","jfin_calc_sup_cov":"0","jfin_calc_tec_haz":"0","jfin_calc_tec_set":"0","jfin_calc_tec_bio":"0","jfin_calc_tec_cnt":"0","jfin_calc_tec_srf":"0","jfin_calc_tec_cln":"0","jfin_calc_tec_poe":"0","jfin_calc_tec_fin":"0","jfin_calc_tec_ppr":"0","jfin_calc_tec_cov":"0","jfin_calc_labor_sup":"45000","jfin_calc_labor_tec":"0","jfin_calc_labor":"45000","jfin_calc_sup_labor":"0","jfin_calc_tec_labor":"0","jfin_calc_sub_labor":"45000","jfin_calc_sub_waste":"0","jfin_calc_sub_srf":"0","jfin_calc_ovh":"0","jfin_calc_dispatch":"0","jfin_calc_atp_test":"0","jfin_calc_asb_test":"0","jfin_calc_asb_vial":"0","jfin_calc_demob":"0","jfin_calc_disinfect":"0","jfin_calc_phodoc":"0","jfin_calc_digrep":"0","jfin_calc_dumpster":"0","jfin_calc_eqp_use":"13500","jfin_calc_eqp_dis":"0","jfin_calc_eqp":"13500","jfin_calc_supply_fee":"0","jfin_calc_sup":"22500","jfin_calc_other":"0","jfin_calc_sub_full":"81000","jfin_calc_disc_ini":"0","jfin_calc_disc_max":"0","jfin_calc_disc":"0","jfin_calc_sub":"81000","jfin_calc_3rd_other":"40000","jfin_calc_tax":"0","jfin_calc_tot":"121000","jfin_dt_add":"2021-09-01 18:13:51","jfin_info":null,"jfin_hr_bio":"2.00","jfin_hr_brk":"0.00","jfin_hr_cln":"0.00","jfin_hr_cnt":"0.00","jfin_hr_drv":"0.00","jfin_hr_fin":"0.00","jfin_hr_haz":"0.00","jfin_hr_off":"0.00","jfin_hr_poe":"0.00","jfin_hr_ppr":"0.00","jfin_hr_set":"0.00","jfin_hr_srf":"0.00"},"secondaryJini":null,"secondaryInvType":null,"secondaryFinal":null,"phaseData":[{"phase_id":"1","phase_code":"PRE","phase_dsca":"Pre-Arrival"},{"phase_id":"2","phase_code":"INI","phase_dsca":"Initial Estimate"},{"phase_id":"3","phase_code":"MGT","phase_dsca":"Job Management"},{"phase_id":"4","phase_code":"FIN","phase_dsca":"Final Estimate"},{"phase_id":"5","phase_code":"POS","phase_dsca":"Post-Job"}],"jobInfo":{"jif_num_days":"1","jif_num_crew":"1","jif_calc_hr_transit":"2.00"},"jobID":"84072","jobClass":"1","phaseID":4,"jifLocked":"1","iniConfirmed":"1","finConfirmed":"1","tab":"postJob","perms":["ADMIN","ASSIGN","AUD_A","COMMON","FIN_A","FIN_E","FIN_P","HR_E","IV_A","IV_E","JIF_A","JIF_D","JIF_E","JIF_M","JIF_R","JOB_A","JOB_CF","JOB_CO","JOB_E","JOB_PG","JOB_PW","JOB_ST","JOB_V1","JOB_V2","MASTER","MKT_A","MKT_E","RDB","RES_A","RPT","SCH_E","SCH_R","TEAM","TIM_A","TIM_E","DEV","EXEC","FDB","RGN"]});</script><script type='module'>import {PhaseList} from '/view/asset/js/jif/phaseList.js?1630598492';var phaseList = new PhaseList({"jobID":"84072","phases":[{"phaseID":"1","phaseCode":"pre","title":"Pre-Arrival","checksFailed":"0"},{"phaseID":"2","phaseCode":"ini","title":"Initial Estimate","checksFailed":"0"},{"phaseID":"3","phaseCode":"mgt","title":"Job Management","checksFailed":"0"},{"phaseID":"4","phaseCode":"fin","title":"Final Estimate","checksFailed":"0"},{"phaseID":"5","phaseCode":"pos","title":"Post-Job","checksFailed":"1"}],"phase":"fin"});</script>
      <div class='aura'>
         <div class='wrap'>
            <!--div class='no-print'>
               <table style='color:white;background-color:#446;text-align:left;'>
                  <tr>
                     <td style='padding-right:30px;'>SESSION: <span style='color:#ddf;'>ctle28anlvnpi98p4r0i6qlp8a</span></td>
                     <td style='padding-right:30px;'>GET</td>
                     <td style='padding-right:30px;'>POST</td>
                  </tr>
                  <tr>
                     <td style='padding-right:30px;'>
                        <p>timeout => <span style='color:#ddf;'>1630598492</span><br />emp => <span style='color:#ddf;'>1478</span><br />user => <span style='color:#ddf;'>blee</span><br />email => <span style='color:#ddf;'>blee@aftermath.com</span><br />office => <span style='color:#ddf;'>1</span><br />region => <span style='color:#ddf;'>0</span><br />fname => <span style='color:#ddf;'>Besso</span><br />lname => <span style='color:#ddf;'>Lee</span><br />role => <span style='color:#ddf;'>Array
                           (
                           [0] => ADM
                           [1] => DEV
                           [2] => EXE
                           )
                           </span><br />perm => <span style='color:#ddf;'>Array
                           (
                           [0] => ADMIN
                           [1] => ASSIGN
                           [2] => AUD_A
                           [3] => COMMON
                           [4] => FIN_A
                           [5] => FIN_E
                           [6] => FIN_P
                           [7] => HR_E
                           [8] => IV_A
                           [9] => IV_E
                           [10] => JIF_A
                           [11] => JIF_D
                           [12] => JIF_E
                           [13] => JIF_M
                           [14] => JIF_R
                           [15] => JOB_A
                           [16] => JOB_CF
                           [17] => JOB_CO
                           [18] => JOB_E
                           [19] => JOB_PG
                           [20] => JOB_PW
                           [21] => JOB_ST
                           [22] => JOB_V1
                           [23] => JOB_V2
                           [24] => MASTER
                           [25] => MKT_A
                           [26] => MKT_E
                           [27] => RDB
                           [28] => RES_A
                           [29] => RPT
                           [30] => SCH_E
                           [31] => SCH_R
                           [32] => TEAM
                           [33] => TIM_A
                           [34] => TIM_E
                           [35] => DEV
                           [36] => EXEC
                           [37] => FDB
                           [38] => RGN
                           )
                           </span><br />last_login_date => <span style='color:#ddf;'>08/31/21</span><br />last_login_time => <span style='color:#ddf;'>8:56</span><br />inactivity => <span style='color:#ddf;'>2662</span><br />job.php => <span style='color:#ddf;'>Array
                           (
                           [status] => 5
                           )
                           </span><br />jif.php => <span style='color:#ddf;'>Array
                           (
                           [tab] => postJob
                           )
                           </span><br />
                        </p>
                     </td>
                     <td>
                        <p>job => <span style='color:#ddf;'>84072</span><br />phase => <span style='color:#ddf;'>fin</span><br /></p>
                     </td>
                     <td>
                        <p></p>
                     </td>
                  </tr>
               </table>
            </div-->
            <div class='head_fixed no-print'>
               <div class='head_left'>
                  <div class='head_left_content'>
                     <a href='index.php'>logo</a>
                  </div>
               </div>
               <div class='head_right'>
                  <div class='head_right_content'>

                     <p>&nbsp;</p>
                                     
<?php
                     echo "<p class='head_menu' style='display:inline;padding-left:10px;'>";
                     if (isset($_SESSION['emp'])) {
                        echo $_SESSION['fname'] . " " . $_SESSION['lname'];
                        echo "&nbsp;";
                        echo "<a href='account.php'><img class='icon16' title='Account' alt='Account' src='icons/account16.png' /></a>";

                        echo "&nbsp;";
                        echo "<a href='/auth/logout'>Logout</a>";
                     } else {
                        echo "<a href='/auth/login'>Login</a>&nbsp;";
                     }
                     echo "&nbsp;</p>";
?>


                     <p class='head_menu'><a href='admin.php' title='Administration'>Adm</a> &#183; <a href='my.php' title='My Status'>My</a> &#183; <a href='call.php' title='Call Leads'>Call</a> &#183; <a href='lgen.php' title='General Calls'>Gen</a> &#183; <a href='lead.php' title='Website Leads'>Web</a> &#183; <a href='ldir.php' title='Direct Leads'>Dir</a> &#183; <a href='job.php' title='Jobs'><span class='highlighted'>Job</span></a> &#183; <a href='jcJob.php' title='Jail Cell Jobs'>JC</a> &#183; <a href='resDB.php' title='Restoration Jobs'>RJ</a> &#183; <a href='dbEnv.php' title='Environmental Jobs'>EQ</a> &#183; <a href='invoice.php' title='Invoices'>Inv</a> &#183; <a href='csr.php' title='Invoices'>CSR</a> &#183; <a href='cash.php' title='Collections'>Collect</a> &#183; <a href='team.php' title='Teams'>Team</a> &#183; <a href='insurance.php' title='Insurance Companies'>Ins</a> &#183; <a href='partnerIns.php' title='Partners'>Cont</a> &#183; <a href='inventory.php' title='Inventory'>Invy</a> &#183; <a href='opportunity.php' title='Opportunity Logger'>Opp</a> &#183; <a href='resources.php' title='Resources'>Res</a> &#183; <a href='report.php' title='Reports'>Rpt</a> &#183; <a href='db1.php' title='Dashboard 1'>DB1</a> &#183; <a href='dbAR.php' title='AR Dashboard'>DB2</a> &#183; <a href='cm4.php' title='CM Schedule'>CM</a> &#183; <a href='help.php' title='Help'>Help</a>&nbsp;</p>
                  </div>
               </div>
            </div>
            <!-- end of jif header -->